import { ApiLookup, ApiResourceVersion } from './Api/index'
import { ZenLookup } from './modules/ComponentInfo/Dictionary/ZenLookup'
import { GlobalStorage, KeyStorage } from "./utils/storage";

function cacheData(notify, isLogin, callback) {
    // notify : { code_name, version}
    // get thông tin version
    ApiResourceVersion.get(res => {
        if (res.status === 200) {
            let dataCache = GlobalStorage.get(KeyStorage.CacheData) || {};
            const versionInfo = res.data.data

            // Mới đăng nhập hoặc chưa có cache data
            if (isLogin || Object.keys(dataCache).length === 0) {
                tableCacheData.forEach(code_name => {
                    const newVersion = versionInfo.find(t => t.code_name.toLowerCase() == code_name.toLowerCase()) || {}
                    dataCache[code_name.toLowerCase()] = {
                        version: newVersion.version || 0,
                        data: []
                    }
                })
                getdata(tableCacheData.toString(), dataCache, callback)
            } else {
                if (notify.code_name) {
                    // Nhận thông báo từ hubs
                    dataCache[notify.code_name.toLowerCase()].version = notify.version
                    getdata(notify.code_name, dataCache)
                } else {
                    // Đã đăng nhập
                    try {
                        let strCode = "";
                        const newDataCache = {}

                        tableCacheData.forEach(code_name => {
                            const lowerCodeName = code_name.toLowerCase();
                            newDataCache[lowerCodeName] = dataCache[lowerCodeName] || { version: 0, data: [] }

                            const newVersion = versionInfo.find(t => t.code_name.toLowerCase() == lowerCodeName) || {}
                            const localVersion = dataCache[lowerCodeName]

                            if (!dataCache[lowerCodeName]
                                || newVersion.version > localVersion.version) {
                                strCode += `,${lowerCodeName}`
                            }
                        })

                        strCode = strCode.replace(",", "")
                        if (strCode) {
                            getdata(strCode, newDataCache)
                        } else {
                            GlobalStorage.set(KeyStorage.CacheData, newDataCache)
                        }
                    } catch (error) {
                        console.log(error)
                    }
                }
            }
        }
    })
}

function getdata(strCode, dataCache, callback) {
    if (strCode) {
        ApiLookup.getMulti(strCode, res => {
            if (res.status === 200) {
                strCode.split(",").forEach((code_name, index) => {
                    dataCache[code_name.toLowerCase()].data = res.data.data[index]
                })
                GlobalStorage.set(KeyStorage.CacheData, dataCache)
                callback && callback()
            }
        })
    }
}

// khai báo các bảng cần cache
const tableCacheData = [
    ZenLookup.TK.code,
    ZenLookup.Ma_nt.code,
    ZenLookup.Ma_dvt.code,
    ZenLookup.Ma_phi.code,
    ZenLookup.Ma_bp.code,
    ZenLookup.Ma_thue.code,
    ZenLookup.Ma_httt.code,
    ZenLookup.Ma_nvkd.code,
    ZenLookup.Ma_nhkh.code,
    ZenLookup.Ma_plkh.code,
    ZenLookup.Ma_tt.code,
    ZenLookup.Ma_cp.code,
    ZenLookup.Ma_loai_vt.code,
    ZenLookup.Ma_nhvt.code,
    ZenLookup.Ma_plvt.code,
    ZenLookup.Ma_loai_giaton.code,
    ZenLookup.Ma_kho.code,
    ZenLookup.Ma_nhts.code,
    ZenLookup.Ma_ldtg.code,
    ZenLookup.Ma_ct.code,
    ZenLookup.Nam_tc.code,// năm tài chính
    ZenLookup.SIDmloai.code,
    ZenLookup.Ma_nhhd.code,
    ZenLookup.SIDmQuocGia.code,
    ZenLookup.SIDmTinh.code,
    ZenLookup.SIDmHuyen.code,
    ZenLookup.HrDmKhac.code,
    ZenLookup.HRDmcctc.code,
    ZenLookup.MA_BC.code, // Bằng cấp
    ZenLookup.MA_DAC_DIEM_GV.code, // đặc điểm gv
    ZenLookup.MA_CVCM.code, // Chức vụ vị trí
    ZenLookup.MA_LOAI_HDLD.code, // Mã loại hdld
    ZenLookup.MA_NN.code, // Ngoại ngữ
    ZenLookup.MA_TTHN.code, // tình trạng hôn nhân
    ZenLookup.Ma_Dan_Toc.code, // dân tộc
    ZenLookup.MA_TON_GIAO.code, // tôn giáo
    ZenLookup.MA_TDCM.code, // trình độ chuyên môn
    ZenLookup.MA_TDDT.code, // trình độ đào tạo
    ZenLookup.MA_TDNN.code, // trình độ ngoại ngữ
    ZenLookup.MA_LOAI_TAI_LIEU.code, // loại TÀI LIỆU
    ZenLookup.Ma_ngh.code, // Ngân hàng
    ZenLookup.Ma_kh.code,
    ZenLookup.ID_NV.code,
    ZenLookup.SIDmForm.code,
    ZenLookup.Ma_phi.code,
    ZenLookup.Ma_ngh.code,
    ZenLookup.Ma_linh_vuc.code,
    ZenLookup.Ma_nguon_kh.code,
    ZenLookup.Ma_hoat_dong.code,
    ZenLookup.CRMDeal.code,
    ZenLookup.Ma_giai_doan.code,
    ZenLookup.Ma_uu_tien.code,
    ZenLookup.PMDmNhomDa.code,
    ZenLookup.PMDmGiaiDoanDa.code,
    ZenLookup.PMDuAn.code,
    ZenLookup.WMDmLoaiCV.code,
    ZenLookup.WMDmUuTienCV.code,
    ZenLookup.WMDmGiaiDoanCV.code,
    ZenLookup.Ma_vt.code,
    ZenLookup.PAKyHieuChamCong.code,
    ZenLookup.Ma_gd.code,
    "sidmtgnt",     // Danh mục tỷ giá ngoại tệ
    "settinggrid",  // setting grid
    "z00report",    // report info
    "dmmagd",       // config/ma_gd
    "ZeInvoiceSetting", // Thông tin hóa đơn điện tử
    "fasetup",
    "sisetup",
    "insetup",
    "posetup",
    "sosetup",
]

export { cacheData }