import React from "react";
export { Login, LogOutModal, ForgotPassword, ForgotPasswordTks, Register, RegisterTks, ActivatedForm } from "./Account";

export { default as Home } from "./Home/Home";
export { default as SystemInfo } from "./SystemInfo/SystemInfo/SystemInfo";
// export { default as Dashboard } from "./Dashboard/Main/Main";
//export const Dashboard = React.lazy(() => import('./Dashboard/Main/Main'));

export const Dashboard = React.lazy(() => import('./Dashboard/DashletDynamic/Main'));
// export { default as Setting } from "./Setting/Main/Setting";
export const Setting = React.lazy(() => import('./Setting/Main/Setting'));

export { User } from './User/index';
export { Role, RoleInfo, RoleDashlet } from "./Role/index"
export { Permission } from "./Permission/Permission/index"
export { default as ChangePassword } from './User/Component/ChangePassword';

export { default as NotFound } from "../components/PrivateRoute/NotFound";
export { default as NotPermission } from "../components/PrivateRoute/NotPermission";

export { ZenDictionaryList } from "../components/Control/zDictionary/index";
export { ReportForm } from "../components/Control/zReport/index";
export { ZenForm } from "../components/Control/zEmptyForm/index";
export { ZenVoucherList, ZenVoucherForm } from "../components/Control/zVoucher/index";

export { default as MaintenaceClearCache } from "./Maintenace/Component/MaintenaceClearCache"