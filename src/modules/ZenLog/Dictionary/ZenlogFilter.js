import React, { useEffect, useMemo, useRef, useState } from "react";
import { Accordion, Button, Form, Icon } from "semantic-ui-react";
import { ref } from "yup";
import { ApiZenlogs } from "../Api";
import { ZenButton,ZenFormik, ZenFieldSelect, ZenFieldSelectApi, InputDatePeriod, ZenDatePeriod, ZenField ,ZenMessageToast,ZenMessageAlert } from "../../../components/Control/index";
import { zzControlHelper } from "../../../components/Control/zzControlHelper";
import { ZenHelper } from "../../../utils";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";

export const ZenlogFilter = (props) => {
    const { onLoadData, fieldCode } = props;
    const refForm = useRef();
    const [expand, setExpand] = useState(0);
    const [cg, setCg] = useState(false);

    // const memoSIDmLoai = useMemo(() => {
    //     const data = zzControlHelper.getDataFromLocal(ZenLookup.SIDmloai)
    //     return {
    //         TenPlkh: data?.filter(t => t.ma_nhom === "PLKH")
    //             ?.reduce((obj, item) => Object.assign(obj, { [item.ma]: item.ten }), {}),
    //         optionLoaiKh: zzControlHelper.convertToOptionsSemantic(data.filter(t => t.ma_nhom === "LOAI_KH"), true, "ma", "ma", "ten")
    //     }
    // }, [])

    useEffect(() => {
        onLoadData(``);
    }, []);


    const handleSubmit = (item, { name }) => {
        //console.log(item.target.value,name)
        const newItem = { ...refForm.current.values,  [name] : item.value,}
        const strSql = createStrSql(newItem)
        onLoadData(strSql, newItem);
        setCg(false)
    }

    const handleChangeDate = ({ startDate, endDate }) => {
        if(startDate) {
            refForm.current.setFieldValue('ngay1', startDate)
        } 
        if(endDate) {
            refForm.current.setFieldValue('ngay2', endDate)
        }

        const newItem = { ...refForm.current.values, ngay1: startDate, ngay2: endDate}
        const strSql = createStrSql(newItem)
        onLoadData(strSql, newItem);
    }

    const handleChangeRequest = (e,data) => {
        if(data.value){
            refForm.current.setValues({
                ...refForm.current.values,
                request_path: data.value
            })
            setCg(true)
        } else {
            setCg(false)
        }
    }

    function createStrSql(item = {}) {
        let _sql = "";
        if (item.ngay1) _sql += ` AND TimeStamp >= '${item.ngay1}'`;
        if (item.ngay2) _sql += ` AND TimeStamp <= '${item.ngay2}'`;
        if (item.request_path) _sql += `AND request_path LIKE '%${item.request_path}%'`;

        return _sql.replace("AND", "")
    }
    const handleDelete = ()  => {
        const {ngay1,ngay2} = refForm.current.values
        if(ngay1 && ngay2) {
            ApiZenlogs.delete({ngay1 : ngay1 , ngay2: ngay2}, res => {
                if(res.status >= 200 && 204 >= res.status) {
                    ZenMessageToast.success()
                    window.location.reload()
                }
            })
        } else {
            ZenMessageAlert.warning("Vui lòng chọn khoảng thời gian cần xóa")
        }
    }
    return <>
        <Accordion fluid styled>
            <Accordion.Title
                active={expand === 0}
                index={0}
                onClick={(e, { index }) => setExpand(expand === index ? -1 : index)}
            >
                <Icon name="dropdown" />
                Điều kiện lọc
            </Accordion.Title>
            <Accordion.Content active={expand === 0}>
        <ZenFormik form={"Zenlog-filter"}
            ref={refForm}
            validation={[]}
            initItem={initItem}
            onSubmit={handleSubmit}
            onClick={handleDelete}
            onReset={(item) => {onLoadData(""),setCg(false)}}
        >
            {
                formik => {
                    const { values } = formik
                    return <Form size="small">
                        <Form.Group widths="4">
                            <ZenDatePeriod
                                onChange={handleChangeDate}
                                value={[values.ngay1, values.ngay2]}
                                textLabel="Thời gian"
                                defaultPopupYear={ZenHelper.getFiscalYear()}
                            />
                            <ZenField
                                 name="request_path"
                                 props={formik}
                                 label="zenlog.url" defaultlabel="Request Path"
                                 onChange={(e,data) => handleChangeRequest(e,data)}
                            />
                            { cg &&
                            <Form.Field>
                                <label>&nbsp;</label>
                            <div style={{position:"relative",display:"inline-block"}}>
                                <ZenButton btnType="cancel"
                                    size="small"
                                    content="Hủy"
                                    color="red"
                                    onClick={(e) => {
                                        refForm.current.setValues({
                                            ...refFormik.current.values,
                                            request_path: "",
                                        })
                                        refForm.current.handleSubmit(e)
                                    }}
                                />
                                <ZenButton 
                                    primary
                                    size="small"
                                    type="submit"
                                    content="Lọc"
                                    icon="search"
                                    onClick={(e) => refForm.current.handleSubmit(e)}
                                    style={{ margin: "0px" }}
                                />
                            </div>
                            </Form.Field>
                        }
                        </Form.Group>
                    </Form>
                }
            }
        </ZenFormik>
        <Button primary
            content="Reset"
            size="small" icon="refresh"
            onClick={(e) => refForm.current.handleReset(e)}
        />
        <Button primary
            content="Xóa Log"
            size="small" icon="trash"
            onClick={(e) => {handleDelete()}}
        />
        </Accordion.Content>
        </Accordion>
    </>
}

const initItem = {
    ngay1:"",
    ngay2:"",
    request_path: "",
}