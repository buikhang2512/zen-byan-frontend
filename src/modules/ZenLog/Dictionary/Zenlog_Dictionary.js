import React from "react"
import { Table } from "semantic-ui-react";
import { zzControlHelper } from "../../../components/Control/zzControlHelper";
import * as routes from "../../../constants/routes";
import { IntlFormat } from "../../../utils/intlFormat";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { ApiZenlogs } from "../Api";
import { ZenlogFilter } from "./ZenlogFilter";

export const ZenLog_Dictionary = {
    Zenlogs: {
        route: routes.ZenLogs,

        action: {
            view: { visible: true, permission: "" },
            add: { visible: false, permission: "" },
            edit: { visible: false, permission: "" },
            del: { visible: false, permission: "" }
        },
        linkHeader: {
            id: "zenlog-header",
            defaultMessage: "Nhật ký hệ thống",
            active: true,
            isSetting: true,
        },

        tableList: {
            keyForm: "zenlog-form",
            //formModal: ZenLookup.SIDmloai,
            fieldCode: "id",
            upPagination: false,
            changeCode: false,
            api: {
                url: ApiZenlogs,
                type: "sql",
            },

            ContainerTop: ZenlogFilter,

            columns: [
                {
                    id: "zenlog.id", defaultMessage: "id",
                    fieldName: "Id", type: "string", filter: "string", sorter: true,
                },
                {
                    id: "zenlog.time", defaultMessage: "Time",
                    fieldName: "TimeStamp", type: "date", sorter: true, custom: true
                },
                {
                    id: "zenlog.message", defaultMessage: "Message",
                    fieldName: "Message", type: "string", filter: "string", sorter: true, 
                },
                {
                    id: "zenlog.level", defaultMessage: "Level",
                    fieldName: "Level", type: "string", filter: "string", sorter: true,
                },
                {
                    id: "zenlog.properties", defaultMessage: "Properties",
                    fieldName: "Properties", type: "string", filter: "string", sorter: true, 
                },
                {
                    id: "zenlog.request", defaultMessage: "Request Path",
                    fieldName: "request_path", type: "string", filter: "string", sorter: true,
                },
                {
                    id: "zenlog.userid", defaultMessage: "UserId",
                    fieldName: "userid", type: "string", filter: "string", sorter: true,
                },
                //{ id: "ksd", defaultMessage: "Không sử dụng", fieldName: "ksd", type: "bool", filter: "", sorter: true },
            ],

            customCols: (data, item, fieldName, { onEdit, ...restCol }) => {
                function isValidDate(d) {
                   return new Date(d).getFullYear()
                }
                
                let customCell = undefined
    
                if (fieldName === 'TimeStamp') {
                   customCell = <Table.Cell key={fieldName} collapsing>
                      {isValidDate(item.TimeStamp) > 1900 &&
                          `${zzControlHelper.formatDateTime(item.TimeStamp, 'DD/MM/YYYY HH:mm:ss')}`
                      }
                   </Table.Cell>
                }
                return customCell
             }
        }
    },
}