import axios from '../../../Api/axios';

const ExtName = "zenlog"

export const ApiZenlogs = {
   get(callback, filter = {}, pagination = {}) {
      axios.get(`${ExtName}`, {
         params: {
            qf: filter.qf,
            keyword: filter.keyword,
            sort: filter.sort,
            page: pagination.page,
            pageSize: pagination.pageSize
         },
      })
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   getByCode(code, callback) {
      axios.get(`${ExtName}/` + code)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   delete(params, callback) {
      let query = ""
      if(params?.ngay1) query += `ngay1=${params.ngay1}`;
      if(params?.ngay2) query += `&ngay2=${params.ngay2}`;
      axios.delete(`${ExtName}?${query}`)
      .then(res => {
         callback(res)
      })
      .catch(err => {
         callback(err)
      });
   }
}