import React, { Component } from 'react';
import { Card, Image, Icon, Segment, Label, Modal, Button, Form, Message, Popup, Breadcrumb } from 'semantic-ui-react';
import { SMTP, SendGrid, ZenTech } from 'resources'
import { EmailSMTP, EmailSendGrid } from './index';
import { FormattedMessage } from 'react-intl';

import { ButtonCancel, ConfirmDelete, HeaderLink, Helmet, SegmentHeader, ZenMessageAlert } from '../../../components/Control';
import { ApiEmail } from '../../../Api/ApiEmail';
import * as routes from '../../../constants/routes';
import * as permissions from "../../../constants/permissions"
// import '../config.css';

class EmailComponent extends Component {
   constructor(props) {
      super(props);
      this.state = {
         error: false,
         errorList: [],

         btnSendLoading: false,
         typeEmailEdit: "",
         openTestMail: false,
         typeTestMail: "",

         fromMail: { ...initItems },

         errors: {
            fromMail: "",
         },

         active: {
            provider: "",
            isinactive: false
         }
      }
   }

   componentDidMount() {
      ApiEmail.get(res => {
         if (res.status === 200) {
            const temp = res.data.data.find(x => x.isinactive === true)
            this.setState({
               active: {
                  provider: temp ? temp.provider : "",
                  isinactive: temp ? temp.isinactive : false
               }
            })
         } else {
            const errorList = []
            errorList.push('' + res)
            this.setState({ error: true, errorList: errorList })
         }
      })
   }

   handleOpenFormConfig = (type) => {
      this.setState({ typeEmailEdit: type })
   }

   handleAfterSaveData = (item) => {
      if (item.isinactive === true) {
         this.setState({
            active: { provider: item.provider, isinactive: item.isinactive },
            typeEmailEdit: ""
         })
      } else {
         this.setState({
            typeEmailEdit: ""
         })
      }
   }

   handleTestMailSend = () => {
      const { typeTestMail, fromMail, errors } = this.state
      if (fromMail.fromMail) {
         this.setState({ btnSendLoading: true })

         ApiEmail.sendMailTest(typeTestMail, fromMail, res => {
            if (res.status === 200) {
               ZenMessageAlert.success('success', "Gửi thành công")
               this.setState({ openTestMail: false, typeTestMail: "", btnSendLoading: false })
            } else {
               ZenMessageAlert.error("Gửi thất bại")
               this.setState({ btnSendLoading: false, openTestMail: false, typeTestMail: "" })
            }
         })
      } else {
         errors['fromMail'] = "Không được để trống trường này"
         this.setState({ errors })
      }
   }

   handleTestMailOpen = (type) => {
      if (type) {
         ApiEmail.get(res => {
            if (res.status === 200) {
               if (res.data.data.length > 0) {
                  if (type) {
                     this.setState({
                        openTestMail: true,
                        typeTestMail: type,
                        fromMail: { ...initItems },
                        errors: { fromMail: "" }
                     })
                  } else {
                     this.setState({
                        openTestMail: false,
                        typeTestMail: ""
                     })
                  }
               } else {
                  ZenMessageAlert.error("Chưa khai báo thông tin gửi mail")
                  this.setState({ btnSendLoading: false })
               }
            } else {
               const errorList = []
               errorList.push('' + res)
               this.setState({ error: true, errorList: errorList, btnSendLoading: false })
            }
         }, `provider = "${type}"`)
      } else {
         this.setState({
            openTestMail: false,
            typeTestMail: ""
         })
      }
   }

   handleTestMailChangeInput = (e, { name, value }) => {
      const { fromMail, errors } = this.state
      fromMail[name] = value

      if (name === 'fromMail' && value) {
         errors[name] = undefined
      } else if (name === 'fromMail' && !value) {
         errors[name] = "Không được để trống trường này"
      }

      this.setState({ fromMail, errors })
   }

   handleUseMailZenTech = () => {
      ApiEmail.get(res => {
         if (res.status === 200) {
            const item = { provider: setting.ZENTECH, isinactive: true }

            if (res.data.data.length > 0) {
               ApiEmail.update(item, res => {
                  if (res.status >= 200 && res.status <= 204) {
                     this.state.active.provider = setting.ZENTECH
                     this.state.active.isinactive = true
                     this.handleOpenFormConfig("")
                  } else {
                     const errorList = []
                     errorList.push('' + res)
                     this.setState({ error: true, errorList: errorList })
                  }
               })
            } else {
               ApiEmail.insert(item, res => {
                  if (res.status >= 200 && res.status <= 204) {
                     this.state.active.provider = setting.ZENTECH
                     this.state.active.isinactive = true
                     this.handleOpenFormConfig("")
                  } else {
                     const errorList = []
                     errorList.push('' + res)
                     this.setState({ error: true, errorList: errorList })
                  }
               })
            }

         } else {
            const errorList = []
            errorList.push('' + res)
            this.setState({ error: true, errorList: errorList })
         }
      }, `provider = "${setting.ZENTECH}"`)
   }

   render() {
      const { error, errorList, typeEmailEdit, openTestMail, errors, fromMail,
         btnSendLoading, typeTestMail, active } = this.state

      return (
         <React.Fragment>
            <PageHeader />

            <Segment>
               {error && <Message negative list={errorList} />}

               <Card.Group stackable>
                  <Card>
                     <Card.Content>
                        <a onClick={() => this.handleOpenFormConfig(setting.SMTP)}>
                           <Image className="mouse_over" src={SMTP} size="big" />
                        </a>
                     </Card.Content>
                     <Card.Content>
                        <Label as="a" onClick={() => this.handleTestMailOpen(setting.SMTP)}>
                           <Icon name="mail" />
                           Thử gửi mail với SMTP
                        </Label>
                        {(active.provider === setting.SMTP && active.isinactive) &&
                           <Popup content="Đang sử dụng" trigger={<Icon size="large" name="check" color="green" style={{ float: "right" }} />} />
                        }
                     </Card.Content>
                  </Card>

                  <Card>
                     <Card.Content>
                        <a onClick={() => this.handleOpenFormConfig(setting.SENDGRID)}>
                           <Image className="mouse_over" src={SendGrid} size="big" />
                        </a>
                     </Card.Content>
                     <Card.Content>
                        <Label as="a" onClick={() => this.handleTestMailOpen(setting.SENDGRID)}>
                           <Icon name="mail" />
                           Thử gửi mail với SendGrid
                        </Label>

                        {(active.provider === setting.SENDGRID && active.isinactive) &&
                           <Popup content="Đang sử dụng" trigger={<Icon size="large" name="check" color="green" style={{ float: "right" }} />} />
                        }
                     </Card.Content>
                  </Card>

                  <Card>
                     <Card.Content>
                        <a onClick={() => this.handleOpenFormConfig(setting.ZENTECH)}>
                           <Image className="mouse_over" src={ZenTech} size="big" />
                        </a>
                     </Card.Content>
                     <Card.Content>
                        <Label as="a" onClick={() => this.handleTestMailOpen(setting.ZENTECH)}>
                           <Icon name="mail" />
                           Thử gửi mail với dịch vụ của Zentech
                        </Label>
                        {(active.provider === setting.ZENTECH && active.isinactive) &&
                           <Popup content="Đang sử dụng" trigger={<Icon size="large" name="check" color="green" style={{ float: "right" }} />} />
                        }
                     </Card.Content>
                  </Card>
               </Card.Group>
            </Segment>

            {openTestMail && <Modal size='tiny' closeOnEscape closeIcon closeOnDimmerClick={false}
               open={openTestMail} onClose={() => this.handleTestMailOpen()}>
               <Modal.Header>
                  <Icon name="mail" />
                  {typeTestMail !== setting.ZENTECH ? < FormattedMessage id="tab_email.test_header" />
                     : "Bạn đang sử dụng dịch vụ gửi Mail của ZenTech"}
               </Modal.Header>
               <Modal.Content>
                  <Form>
                     <Form.Input required name="fromMail"
                        label={"Email nhận"}
                        placeholder={"Email nhận"}
                        onChange={this.handleTestMailChangeInput} value={fromMail.fromMail}
                        error={errors.fromMail ? errors.fromMail : undefined}
                     />

                     <Form.Input name="subject"
                        label={"Tiêu đề"}
                        placeholder={"Tiêu đề"}
                        onChange={this.handleTestMailChangeInput} value={fromMail.subject} />

                     <Form.TextArea name="body"
                        label={"Nội dung"}
                        placeholder={"Nội dung"}
                        onChange={this.handleTestMailChangeInput} value={fromMail.body} />
                  </Form>
               </Modal.Content>
               <Modal.Actions>
                  <ButtonCancel disabled={btnSendLoading} size="small"
                     onClick={() => this.handleTestMailOpen()} />

                  <Button loading={btnSendLoading} disabled={btnSendLoading} size="small" primary
                     content={"Gửi"} icon="send"
                     onClick={this.handleTestMailSend}
                  />
               </Modal.Actions>
            </Modal>
            }

            {(typeEmailEdit === setting.SMTP) && <EmailSMTP provider={setting.SMTP}
               //editItem={currentItem}
               open={typeEmailEdit === setting.SMTP}
               onAfterSave={this.handleAfterSaveData}
               onClose={() => this.handleOpenFormConfig("")} />
            }

            {(typeEmailEdit === setting.SENDGRID) && <EmailSendGrid provider={setting.SENDGRID}
               open={typeEmailEdit === setting.SENDGRID}
               onAfterSave={this.handleAfterSaveData}
               onClose={() => this.handleOpenFormConfig("")} />
            }

            {typeEmailEdit === setting.ZENTECH && <ConfirmDelete open={typeEmailEdit === setting.ZENTECH}
               header={"Xác nhận sử dụng dịch vụ gửi Mail của Zentech"}
               onConfirm={this.handleUseMailZenTech}
               onCancel={() => this.handleOpenFormConfig("")}
               content={"Bạn có muốn sử dụng dịch vụ gửi mail của Zentech không?"} />
            }
         </React.Fragment >
      );
   }
}

const PageHeader = () => {
   return <>
      <Helmet idMessage={"setting-mail"}
         defaultMessage={"Thiết lập mail"} />

      <SegmentHeader>
         <HeaderLink listHeader={listHeader} isSetting />
      </SegmentHeader>
      <br />
   </>
}
const listHeader = [{
   id: "setting-mail",
   defaultMessage: "Thiết lập mail",
   route: routes.MailSetting,
   active: true
}]
const setting = {
   SMTP: "Smtp",
   SENDGRID: "SendGrid",
   ZENTECH: "ZenTech"
}
const initItems = {
   fromMail: "",
   subject: "",
   body: ""
}

export const SetupMail = {
   route: routes.MailSetting,
   Zenform: EmailComponent,
   action: {
      view: { visible: true, permission: permissions.Mail },
   },
}