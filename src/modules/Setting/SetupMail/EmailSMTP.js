import React, { Component } from 'react';
import { withFormik } from 'formik';
import * as Yup from 'yup';
import { FormattedMessage } from 'react-intl';

import { Icon, Modal, Message, Form, Dimmer, Loader, Button, Label, Checkbox } from 'semantic-ui-react';
import { ZenField, ZenFieldCheckbox, ButtonCancel, ButtonSave, ZenMessageToast } from '../../../components/Control';
import { ApiEmail } from '../../../Api/ApiEmail';

class EmailSMTP extends Component {
   constructor(props) {
      super(props);
      this.state = {
         open: false,
         loadingForm: true,
         error: false,
         errorList: [],
         result: {},
         hiddenPassword: "password",
         isinactive: false
      }
   }


   loadData() {
      const { provider } = this.props
      const query = `provider = "${provider}"`

      ApiEmail.get(res => {
         if (res.status === 200) {
            var temp = {}, result = {}
            if (res.data.data.length > 0) {
               result = res.data.data[0]
               temp = result.setting ? JSON.parse(result.setting) : initItem
            } else {
               temp = initItem
            }

            this.setState({
               loadingForm: false,
               error: false,
               errorList: [],
               result: result,
               isinactive: result ? result.isinactive : false
            })

            this.props.setValues(temp)
         } else {
            this.state.errorList.push('' + res)
            this.setState({
               loadingForm: false,
               error: true,
            })
         }
      }, query)
   }

   validData() {
      const { errors } = this.props
      if (errors.name) {
         return false;
      }
      return true;
   }

   saveData = (item) => {
      const { result, isinactive } = this.state
      // convert to string
      const itemSave = {
         provider: this.props.provider,
         setting: JSON.stringify(item),
         isinactive: isinactive
      }

      if (result.id) {
         itemSave.id = result.id

         ApiEmail.update(itemSave, res => {
            if (res.status >= 200 && res.status <= 204) {
               ZenMessageToast.success();
               this.afterSave(itemSave)
            } else {
               this.state.errorList.push("" + res)
               this.setState({
                  loadingForm: false,
                  error: true,
               })
            }
         })
      } else {
         ApiEmail.insert(itemSave, res => {
            if (res.status >= 200 && res.status <= 204) {
               ZenMessageToast.success();
               this.afterSave(res.data.data)
            } else {
               this.state.errorList.push("" + res)
               this.setState({
                  loadingForm: false,
                  error: true,
               })
            }
         })
      }
   }

   afterSave = (item) => {
      this.props.onAfterSave ? this.props.onAfterSave(item) : this.handleCloseForm()
   }

   // ------------------------   component
   componentDidMount() {
      if (this.props.onRef) {
         this.props.onRef(this)
      }

      this.loadData()
   }

   componentWillUnmount() {
      if (this.props.onRef) {
         this.props.onRef(undefined)
      }
   }

   static getDerivedStateFromProps(props, state) {
      if (props.open !== state.open) {
         return {
            open: props.open,
            loadingForm: true
         };
      }
      return null;
   }

   componentDidUpdate(prevProps) {
      if (prevProps.open !== this.props.open && this.props.open) {
         this.loadData()
      }
   }

   // ------------------------   handle
   handleSaveForm = (e, submit) => {
      e.preventDefault();
      if (e.target.id === submit.id) {
         if (this.validData()) {
            this.saveData(this.props.values)
         }
      }
   }

   handleCloseForm = () => {
      this.props.onClose && this.props.onClose()
   }

   handleEnablePassword = (enable) => {
      this.setState({ hiddenPassword: enable })
   }

   render() {
      const { provider, open, onCloseForm, onAfterSave, header, editItem, ...rest } = this.props
      const { loadingForm, error, errorList, hiddenPassword, isinactive } = this.state

      return (
         <React.Fragment>
            <Modal size='small' closeOnEscape closeIcon closeOnDimmerClick={false}
               open={open} onClose={this.handleCloseForm}>
               <Modal.Header>
                  <Icon name="folder open outline" />
                  Sử dụng dịch vụ gửi mail SMTP
               </Modal.Header>

               <Modal.Content>
                  {error && <Message negative list={errorList} />}
                  {loadingForm && <Dimmer inverted active={loadingForm}>
                     <Loader inverted />
                  </Dimmer>}

                  <Form id="modal-smtp" onSubmit={this.handleSaveForm}>
                     <Form.Group>
                        <ZenField width="13"
                           label={"tab_email.smtp_server"} defaultlabel="SMTP Server"
                           name="smtp_server" props={rest}
                        />

                        <ZenField
                           label={"tab_email.port"} defaultlabel="Port"
                           name="port" props={rest}
                        />
                     </Form.Group>
                     <Form.Group widths="equal">
                        <ZenField
                           label={"tab_email.email_account"} defaultlabel="Email Account"
                           name="email_account" props={rest}
                        />

                        <ZenField type={hiddenPassword}
                           label={"tab_email.email_password"} defaultlabel="Password"
                           name="email_password" props={rest}
                           icon={{
                              name: hiddenPassword ? 'eye' : 'eye slash', link: true,
                              onClick: () => this.handleEnablePassword(hiddenPassword ? "" : "password")
                           }}
                        />
                     </Form.Group>

                     <ZenFieldCheckbox label={"tab_email.enable_ssl"} defaultlabel="Enable SSL"
                        name="enable_ssl" props={rest} style={{ fontWeight: "bold" }} />
                  </Form>
               </Modal.Content>

               <Modal.Actions>
                  <Checkbox style={{ float: "left" }} name="isinactive"
                     checked={isinactive} label={"Sử dụng gửi mail với SMTP"}
                     onChange={(e, { checked }) => this.setState({ isinactive: checked })}
                  />

                  <ButtonCancel type="button" size="small" onClick={this.handleCloseForm} />
                  <ButtonSave type='submit' size="small" form="modal-smtp"
                     onClick={(e) => this.props.handleSubmit()} />
               </Modal.Actions>
            </Modal>
         </React.Fragment>
      );
   }
}

export default withFormik({
   mapPropsToValues(props) { // Init form field
      return Object.assign({}, { ...initItem });
   },

   validationSchema: (props) => Yup.object().shape({ // Validate form field
      // smtp_server: Yup.string()
      //    .required(ZenHelper.GetMessage(MESSAGES.Required))
      // ,
   }),

   handleSubmit: (values, { setSubmitting }) => {
      setTimeout(() => {

      }, 1000);
   },
   enableReinitialize: true,
   isInitialValid: false

})(EmailSMTP)

EmailSMTP.defaultProps = {
   onRef: undefined,    // call handle from parent component
   provider: "",       // type Email
   open: false,         // close - open modal
   header: undefined,   // header modal
   isView: false,       // only view
   onClose: undefined,  // event close modal
   onAfterSave: undefined  // event after save data
};

const initItem = {
   smtp_server: "",
   port: "",
   enable_ssl: true,
   email_account: "",
   email_password: ""
}