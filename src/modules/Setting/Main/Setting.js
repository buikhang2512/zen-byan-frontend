import React, { PureComponent } from "react";
import { Helmet, SegmentHeader, HeaderLink } from "../../../components/Control";
import auth from "../../../utils/auth";
import * as routes from "../../../constants/routes";
import SettingSystem from "./SettingSystem";
import { ZenDictionaryInfo } from "../../ComponentInfo/Dictionary/ZenDictionaryInfo";


class Setting extends PureComponent {
  constructor(props) {
    super();
    this.userInfo = auth.getUserInfo();
    this.state = {
      menu: [],
      error: false,
      errorList: [],
      userId: 2, // admin
      dictionaryForm: [],
    };
  }

  componentDidMount() {
    var dictionary = Object.keys(ZenDictionaryInfo).map(function (
      key
    ) {
      return ZenDictionaryInfo[key];
    });

    this.setState({
      dictionaryForm: dictionary,
    });
  }
  render() {
    const { dictionaryForm } = this.state;

    return (
      <React.Fragment>
        <Helmet idMessage="setting.header_page" defaultMessage="Settings" />
        <SegmentHeader>
          <HeaderLink listHeader={linkHeader} />
        </SegmentHeader>

        <SettingSystem />


      </React.Fragment>
    );
  }
}
export default Setting;

const linkHeader = [
  {
    id: "setting.header_page",
    defaultMessage: "Settings",
    route: routes.Settings,
    active: true,
  },
];
