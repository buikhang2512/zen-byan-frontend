import React, { useContext, useEffect, useState } from 'react';
import {
    Header,
    Divider,
    Segment,
    Card
} from "semantic-ui-react";
import {
    FormattedMessage
} from "react-intl";
import { useHistory } from "react-router-dom";
import { AppContext } from "../../../AppContext";
import { ZenApp } from "../../../utils";

const SettingSystemAll = (props) => {
    const history = useHistory();
    const globalContext = useContext(AppContext)
    const [menuSystem, setMenuSystem] = useState()

    useEffect(() => {
        const itemSystem = globalContext.store.menuSys;
        setMenuSystem(itemSystem);
    }, [globalContext.store.menuSys])

    const handleClickMenu = (menu) => {
        history.push(menu.url)
    }

    return (
        <>
            <TitleHeader menus={menuSystem} onClick={handleClickMenu} />
        </>
    )
}

const TitleHeader = ({ menus = [], onClick }) => {
    return menus.map(menu => {
        return (
            (menu.childs.length > 0) && <React.Fragment key={menu.menu_id}>
                <Header as="h4">
                    {menu.name}
                </Header>
                <Segment>
                    <SettingCard
                        menus={menu}
                        onClick={onClick}
                        styleCard={{ maxWidth: "160px" }}
                    />
                </Segment>
            </React.Fragment>
        )
    }
    )
}

const SettingCard = ({ menus = [], onClick, styleCard, ...rest }) => {
    return <Card.Group //centered={true}
        {...rest}
        style={{ marginTop: '1em' }}
    >
        {
            menus.childs.length > 0 && menus.childs.map((item) => {
                return item.url && <MenuCard key={item.menu_id} item={item} onClick={onClick} styleCard={styleCard} />
            })
        }
    </Card.Group>

}

const MenuCard = ({ item, onClick, styleCard, ...rest }) => {
    return <Card link onClick={() => onClick(item)} style={{ textDecoration: "none", ...styleCard }}>
        <Card.Content textAlign="center">
            <Card.Header>
                <img src={`${ZenApp.baseUrl}${item.icon}`}
                    style={{ marginBottom: "12px", borderRadius: "3px", height: "64px" }} />
                <br />
            </Card.Header>

            <div style={{ fontSize: "14px", color: "black" }}>
                <strong>{item.name}</strong>
            </div>
        </Card.Content>

        {item.description &&
            <Card.Content extra style={{ minHeight: '90px', textAlign: 'center' }} >
                {item.description}
            </Card.Content>}
    </Card>
}

export default SettingSystemAll