import React, { Component } from 'react'
import { Dimmer, Grid, Icon, Loader, Menu, Message } from 'semantic-ui-react';
import memoize from 'memoize-one';
import * as routes from "../../../constants/routes";
import SetupContent from './SetupContent';
import { ApiConfig } from '../../../Api';
import { ZenHelper } from '../../../utils/global';
import './SetupModules.css'
import { ContainerScroll } from '../../../components/Control/ContainerScroll';
import * as permissions from "../../../constants/permissions"

class Main extends Component {
   constructor(props) {
      super();
      this.idElementContainer = "setup-grid"
      this.state = {
         error: false,
         errorList: {},
         isLoading: true,
         modules: [],
         activeModule: "",
      }
   }

   // ============================ REACT COMPONENT
   componentDidMount() {
      // ẩn scroll browser
      document.body.style.overflow = "hidden"
      ApiConfig.getModuleForSetup(res => {
         if (res.status === 200) {
            const data = res.data.data.filter(t => t.has_setup === 1 || t.has_setup === undefined)
            this.setState({
               isLoading: false,
               modules: data,
               activeModule: data.length > 0 ? data[0].id : ""
            })
         } else {
            this.setState({
               isLoading: false,
               error: true,
               errorList: ZenHelper.getResponseError(res)
            })
         }
      })
   }

   componentWillUnmount() {
      document.body.style.overflow = ""
   }

   // ============================ EVENT HANDLE
   handleChangeTab = (e, { name }) => {
      if (name === this.state.activeModule)
         return
      this.setState({ activeModule: name })
   }

   // =========================== memozie
   mapModule = memoize(
      (activeModule) => {
         return <Menu fluid pointing vertical size="small">
            {this.state.modules.map(module => {
               const isActive = activeModule === module.id
               return <Menu.Item key={module.id} content={<>
                  {isActive ?
                     <span><Icon name={"caret right"} /></span> : undefined}
                  {module.id} - {module.name}
               </>}
                  name={module.id} onClick={this.handleChangeTab}
                  active={isActive} header={isActive}
               />
            })}
         </Menu>
      }
   )

   render() {
      const { activeModule, isLoading, error, errorList } = this.state
      const renderMenu = this.mapModule(activeModule)

      return <>
         {isLoading && <Dimmer inverted active={isLoading}>
            <Loader inverted content='Loading...' />
         </Dimmer>}

         <Grid id={this.idElementContainer}>
            <Grid.Column width={4}>
               {error && <Message list={errorList} negative />}
               {renderMenu}
            </Grid.Column>

            <Grid.Column stretched width={12}>
               <ContainerScroll isSegment={false}
                  idElementContainer={this.idElementContainer}
                  id={"right-content"}
                  border
               >
                  {activeModule && <SetupContent module={activeModule} />}
               </ContainerScroll>
            </Grid.Column>
         </Grid>
      </>
   }
}

export const SetupModules = {
   SetupModule: {
      route: routes.SetupModule,
      Zenform: Main,
      linkHeader: {
         id: "setup.module",
         defaultMessage: "Thiết lập tham số",
         active: true,
         isSetting: true
      },
      action: {
         view: { visible: true, permission: permissions.Setup_Module },
      },
   },
}