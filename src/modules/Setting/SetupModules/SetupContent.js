import React, { memo, useEffect, useMemo, useState } from 'react'
import { FormattedMessage } from 'react-intl';
import { Dimmer, Icon, Loader, Message, Select, Table } from 'semantic-ui-react'
import { ApiConfig } from '../../../Api/index';
import { FormatDate, FormatNumber, InputDate, InputNumber, ZenMessageToast } from '../../../components/Control';
import { ZenHelper } from '../../../utils/global';

const SetupContent = ({ module }) => {
   // state
   const [error, setError] = useState()
   const [state, setState] = useState(initState)
   const { data, isLoading, rowActive } = state

   useEffect(() => {
      if (module) {
         setState({ ...state, isLoading: true })
         ApiConfig.getModuleSetupMeta(module, res => {
            if (res.status === 200) {
               setState({
                  ...state,
                  data: res.data.data,
                  isLoading: false,
               })
            }
         })
      }
   }, [module])

   useEffect(() => {
      if (rowActive) {
         document.body.addEventListener('click', handleClickPosition, true);
      }
      return () => {
         document.body.removeEventListener('click', handleClickPosition, true);
      }
   }, [rowActive])

   const handleClickRow = (e, item) => {
      const { target } = e
      if (target?.getAttribute("name") === "cancel") {
         setState({ ...state, rowActive: null })
      } else if (!rowActive) {
         setState({ ...state, rowActive: item })
      }
   }

   const handleSave = (newItem) => {
      const updField = {
         var_name: newItem.varName,
         var_value: newItem.varValue
      }
      ApiConfig.updModuleSetup(module, updField, res => {
         if (res.status === 200) {
            if (res.data.data === true) {
               const newRowItem = { ...rowActive, varValue: newItem.varValue }
               const newData = data.map(t => t.varName === newItem.varName ? newRowItem : t)
               setState({ ...state, data: newData, rowActive: null })
               ZenMessageToast.success()
            }
         } else {
            setError(ZenHelper.getResponseError(res))
         }
      })
   }

   function handleClickPosition(e) {
      const isContains = document.getElementById("row-active")?.contains(e.target)
      if (!isContains && rowActive) {
         setState({ ...state, rowActive: null })
      }
   }

   return <>
      {isLoading && <Dimmer inverted active={isLoading}>
         <Loader inverted content='Loading...' />
      </Dimmer>}
      {error && <Message negative list={error} />}

      <Table celled striped compact fixed size="small" definition selectable
         className="hover-edit"
      >
         <Table.Header fullWidth>
            <Table.Row>
               <Table.HeaderCell width="1" className="sticky-top" />
               <Table.HeaderCell width="4" className="sticky-top">
                  <FormattedMessage id="setup.h_name" defaultMessage="Tên tham số" />
               </Table.HeaderCell>
               <Table.HeaderCell width="5" className="sticky-top">
                  <FormattedMessage id="setup.h_value" defaultMessage="Giá trị" />
               </Table.HeaderCell>
               <Table.HeaderCell width="6" className="sticky-top">
                  <FormattedMessage id="setup.h_description" defaultMessage="Mô tả" />
               </Table.HeaderCell>
            </Table.Row>
         </Table.Header>
         <Table.Body>
            {data && data.map((item, index) => {
               const isEdit = rowActive && rowActive.isEdit && rowActive.varName === item.varName
               const isActive = rowActive && rowActive.varName === item.varName

               return <Table.Row key={index} onClick={(e) => handleClickRow(e, item)}
                  id={isActive ? "row-active" : undefined}
                  className={isActive ? "sc-row-active" : ""}
                  style={{ cursor: "pointer" }}
               >
                  <Table.Cell content={index + 1} textAlign="center" collapsing />
                  <Table.Cell>
                     {item.varDescrpt}
                  </Table.Cell>
                  <Table.Cell className="edit">
                     {!isEdit && item.isEdit == 1 && <div className="hover-td">
                        <Icon name="pencil" />
                     </div>}

                     {isEdit ? <RowActive rowActive={{ ...rowActive }} onSave={handleSave} />
                        : <FormView item={item} />}
                  </Table.Cell>
                  <Table.Cell style={{
                     whiteSpace: 'pre-line',
                     wordWrap: 'break-word'
                  }}>
                     {item.Description}
                  </Table.Cell>
               </Table.Row>
            })}
         </Table.Body>
      </Table>
   </>
}

const RowActive = ({ rowActive, onSave }) => {
   const [item, setItem] = useState(rowActive)
   const [isLoadingSave, setIsLoadingSave] = useState(false)

   const handleChangeValue = (values) => {
      const { value, floatValue } = values
      var newItem = { ...item }
      if (item.varType === "N") {
         newItem.varValue = floatValue
      } else {
         newItem.varValue = value
      }
      setItem({ ...newItem })
   }

   const handleSave = (e) => {
      if (item.varValue === rowActive.varValue) { return }
      setIsLoadingSave(true)
      onSave(item)
   }

   return <FormEdit rowActive={item} onChange={handleChangeValue} onSave={handleSave}
      isLoadingSave={isLoadingSave}
   />
}

const FormEdit = ({ rowActive, isLoadingSave, onChange, onSave }) => {
   return <div
      className="ui action fluid input sc-button-save" style={{ width: "100%", zIndex: 0 }}
   >
      {
         rowActive.varType === "D" ? <InputDate
            autoFocus isChangeBlur hiddenCalendar
            selected={rowActive.varValue}
            onChange={(date) => onChange(date)} style={{ borderRadius: "unset" }}
         />
            : rowActive.varType === "N" ?
               <InputNumber
                  autoFocus isChangeBlur
                  style={{ width: "100%", borderRadius: "unset", minWidth: "" }}
                  value={rowActive.varValue}
                  onValueChange={(values) => onChange(values)}
               />
               : rowActive.varType === "combobox" ? <InputSelect rowActive={rowActive} onChange={onChange} />
                  : <input autoFocus
                     type={rowActive.varType === "password" ? "password" : "text"}
                     value={rowActive.varValue || ""}
                     onChange={(e) => onChange(e.target)}
                  />
      }
      <button type="button" onClick={onSave} name="save"
         className={`ui icon button primary basic ${isLoadingSave ? "loading disabled" : ""}`}
      >
         <i aria-hidden="true" className="save icon" />
      </button>
      <button type="button" name="cancel"
         className={`ui icon button basic red ${isLoadingSave ? "disabled" : ""}`}
      >
         <i aria-hidden="true" className="cancel icon" name="cancel" />
      </button>
   </div>
}

const FormView = memo(({ item }) => {
   return item.varType === "D" ? <FormatDate value={item.varValue} />
      : item.varType === "N" ? <FormatNumber value={item.varValue} isZero />
         : item.varType === "password" ? convertToPassword(item.varValue)
            : item.varValue
})

const InputSelect = ({ rowActive, onChange }) => {
   const options = JSON.parse(rowActive.input_options)
   return options && <select
      style={{ width: "100%" }}
      onChange={(e) => onChange(e.target)}>
      {options.map(option => {
         return <option
            key={option.key}
            value={option.value}
            selected={option.value === rowActive.varValue}>
            {option.text}
         </option>
      })}
   </select>
}

function convertToPassword(str) {
   const newStr = new Array(str.length + 1).join("*");
   return str.replace(str, newStr)
}

const initState = {
   data: [],
   isLoading: true,
   rowActive: null,
}

export default memo(SetupContent);