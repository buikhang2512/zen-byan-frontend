import React from "react";
import { Link } from "react-router-dom";
import { Card, Icon } from "semantic-ui-react";
import * as styles from "./SettingCard.less"
import { FormattedMessage } from "react-intl";

export function SettingCard(props) {
  return (
    <Card className={styles.settingCard}>
      <Card.Content textAlign="center">
        <Link to={props.route}>
          <Card.Header>
            <br />
            <Icon name={props.icon} size="huge" />
            <br />
            <br />
          </Card.Header>
          <Card.Header>
            <strong>
              {/* <FormattedMessage {...props.text} /> */}
              {props.text}
            </strong>
          </Card.Header>
        </Link>
      </Card.Content>

      {props.description &&
        <Card.Content extra style={{minHeight:'90px', textAlign:'center'}} >
          {/* <FormattedMessage {...props.description} /> */}
          {props.description}
        </Card.Content>}
    </Card >
  );
}
