import React, { memo } from 'react'
import * as routes from "../../../constants/routes";
import { ApiEODmLoaiPost } from "../Api/index";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import * as permissions from "../../../constants/permissions";
import { auth, ZenHelper } from "../../../utils";
import { Label, Table } from 'semantic-ui-react';
import { GlobalStorage, KeyStorage } from '../../../utils/storage';

const optionBoPhan = ZenHelper.translateListToSelectOptions(GlobalStorage.getByField(KeyStorage.CacheData, 'id_cctc')?.data, false, 'id_cctc', 'id_cctc', 'id_cctc', [], 'ten_cctc')

const EO_Dictionary = {
   EODmLoaiPost: {
      route: routes.EODmLoaiPost,
      action: {
         view: { visible: true, permission: permissions.EODmLoaiPostXem },
         add: { visible: true, permission: permissions.EODmLoaiPostThem },
         edit: { visible: true, permission: permissions.EODmLoaiPostSua },
         del: { visible: true, permission: permissions.EODmLoaiPostXoa },
      },

      linkHeader: {
         id: "eodmloaipost",
         defaultMessage: "Danh mục loại thông báo",
         active: true,
         isSetting: false,
      },

      tableList: {
         keyForm: "EODmLoaiPost",
         formModal: ZenLookup.EODmLoaiPost,
         fieldCode: "ma_loai_post",
         unPagination: false,
         duplicate: true,
         showFilterLabel: true,

         api: {
            url: ApiEODmLoaiPost,
            type: "sql",
         },
         columns: [
            { id: "EODmLoaiPost.ma_loai_post", defaultMessage: "Mã loại", fieldName: "ma_loai_post", filter: "string", sorter: true, editForm: true },
            { id: "EODmLoaiPost.ten_loai_post", defaultMessage: "Tên loại", fieldName: "ten_loai_post", filter: "string", sorter: true, },
            { id: "EODmLoaiPost.ten_user_duyets", defaultMessage: "User Duyệt", fieldName: "ten_user_duyets", filter: "string", sorter: true, custom: true },
            { id: "EODmLoaiPost.id_cctcs", defaultMessage: "Bộ phận áp dụng", fieldName: "id_cctcs", filter: "list", sorter: true, custom: true, listFilter: optionBoPhan },
            { id: "EODmLoaiPost.sua_sau_ph", defaultMessage: "Cho phép sửa sau phát hành", fieldName: "sua_sau_ph", type: "bool", filter: "", sorter: true, collapsing: true },
            { id: "EODmLoaiPost.trao_doi_sau_ph", defaultMessage: "Cho phép trao đổi", fieldName: "trao_doi_sau_ph", type: "bool", filter: "", sorter: true, collapsing: true },
            { id: "EODmLoaiPost.ghi_chu", defaultMessage: "Ghi chú", fieldName: "ghi_chu", type: "string", filter: "", sorter: true, collapsing: true },
            { id: "ksd", defaultMessage: "Không sử dụng", fieldName: "ksd", type: "bool", filter: "", sorter: true, collapsing: true },
         ],

         customCols: (data, item, fieldName, infoCol) => {
            let customCell = undefined
            if (fieldName === 'ten_user_duyets') {
               customCell = <Table.Cell key={fieldName}>
                  <Listuserduyet users={item[fieldName]} />
               </Table.Cell>
            }
            if (fieldName === 'id_cctcs') {
               customCell = <Table.Cell key={fieldName}>
                  <Listuserduyet users={item[fieldName]} />
               </Table.Cell>
            }
            return customCell
         }
      },
   },
}

const ListParticipant = memo(({ users }) => (
   users && users.map(fname => {
      return <Label key={fname.full_name} as="a" content={fname.full_name} />
   })
))

const Listuserduyet = memo(({ users }) => (
   users && users.split(',').map(fname => {
      return <Label key={fname} as="a" content={fname} />
   })
))

export default EO_Dictionary