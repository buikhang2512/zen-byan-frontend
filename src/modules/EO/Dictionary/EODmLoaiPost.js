import React, { useEffect, useState } from "react";
import { ZenField, ZenFieldSelectApi, ZenFieldSelectApiMulti, ZenFieldTextArea, ZenFieldNumber, ZenFieldCheckbox } from "../../../components/Control/index";
import { FormMode, ZenHelper } from "../../../utils";
import { Form, Dropdown } from "semantic-ui-react";
import { ApiEODmLoaiPost } from "../Api";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import * as permissions from "../../../constants/permissions";
import { GlobalStorage, KeyStorage } from "../../../utils/storage";
import _ from "lodash";

const EODmLoaiPostModal = (props) => {
    const { formik, permission, mode } = props
    const [optionbophan, setOptionbophan] = useState([])

    useEffect(() => {
        const bophan = GlobalStorage.getByField(KeyStorage.CacheData, "id_cctc")?.data
        setOptionbophan(ZenHelper.translateListToSelectOptions(bophan, false, 'id_cctc', 'id_cctc', 'id_cctc', [], 'ten_cctc'))
    }, [])

    const handleSelectIDcctc = (item) => {
        if (_.join(item.value, ',') !== ', ') {
            let next_stt = _.join(item.value, ',')
            formik.setFieldValue('id_cctcs', next_stt)
        }
    }

    return <React.Fragment>
        <ZenField required readOnly={!permission}
            label={"EODmLoaiPost.ma_loai_post"} defaultlabel="Mã loại thông báo"
            name="ma_loai_post" props={formik}
        />
        <ZenField required readOnly={!permission}
            label={"EODmLoaiPost.ten_loai_post"} defaultlabel="Tên loại thông báo"
            name="ten_loai_post" props={formik}
        />
        <ZenFieldSelectApiMulti lookup={ZenLookup.User}
            readOnly={!permission}
            name="user_duyets" formik={formik}
            label={"EODmLoaiPost.user_duyets"} defaultlabel="Người duyệt"
            onSelectedItem={(e, i) => {
                let items = []
                for (let item of e) {
                    items.push(item.id)
                }
                formik.setFieldValue('user_ids', items)
            }}
        />
        <Form.Group>
            <Form.Field width="16">
                <label>Bộ phận áp dụng</label>
                <Dropdown error={formik && (formik.touched['id_cctcs'] && formik.errors['id_cctcs'])} fluid multiple selection options={optionbophan} value={formik.values.id_cctcs?.split(',')}
                    readOnly={!permission}
                    onChange={(e, item) => { handleSelectIDcctc(item) }}
                />
            </Form.Field>
        </Form.Group>
        <ZenFieldCheckbox readOnly={!permission}
            name="sua_sau_ph" props={formik}
            label={"EODmLoaiPost.sua_sau_ph"} defaultlabel="Cho phép sửa sau khi phát hành"
        />
        <ZenFieldCheckbox readOnly={!permission}
            name="trao_doi_sau_ph" props={formik}
            label={"EODmLoaiPost.trao_doi_sau_ph"} defaultlabel="Cho phép trao đổi sau khi phát hành"
        />
        <ZenFieldTextArea readOnly={!permission}
            name="ghi_chu" props={formik}
            label={"EODmLoaiPost.ghi_chu"} defaultlabel="Ghi chú"
        />
    </React.Fragment>
}

const onBeforeSave = (item) => {
    if(item.user_duyets) {
        item.user_duyets = _.join(item.user_ids, ',')
        delete item.user_ids
    } else {
        item.user_duyets = ""
    }

    return item
}

const EODmLoaiPost = {
    FormModal: EODmLoaiPostModal,
    onBeforeSave: onBeforeSave,
    api: {
        url: ApiEODmLoaiPost,
    },
    permission: {
        view: permissions.EODmLoaiPostXem,
        add: permissions.EODmLoaiPostThem,
        edit: permissions.EODmLoaiPostSua
    },
    formId: "EODmLoaiPost-form",
    size: "tiny",
    isScrolling: false,
    initItem: {
        ma_gdoan_cv: "",
        ten_gdoan_cv: "",
        ordinal: "",
        ghi_chu: "",
        isclosed: false,
        ksd: false
    },
    formValidation: [
        {
            id: "ma_loai_post",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
                // {
                //    type: "max",
                //    params: [8, "Không được vượt quá 8 kí tự"]
                // },
            ]
        },
        {
            id: "ten_loai_post",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ]
        }
    ]
}

export { EODmLoaiPost };