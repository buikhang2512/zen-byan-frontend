import React, { Component, Suspense } from "react";
import { Switch, Redirect } from "react-router-dom";
import { Navbar, PrivateRoute } from "../../components";
import * as components from "../index";
import * as routes from "../../constants/routes";
import * as permissions from "../../constants/permissions";
import { auth, navs, ZenHelper } from "../../utils";
import { ApiMenu, ApiModule } from "../../Api";
import { GlobalStorage, KeyStorage } from "../../utils/storage";
import { Message } from "semantic-ui-react";
import { ZenLoading } from "../../components/Control";
import { AppContext } from "../../AppContext";

import {
  InitPropsComponent, InitPropsZenVoucherForm,
  InitPropsReportForm, InitPropsZenEmptyForm,
  InitPropsDictionaryList
} from "./InitPropsComponent";
import { ZenVoucherInfo, ZenVoucherInfoEdit } from "../ComponentInfo/Voucher/ZenVoucherInfo";
import { ReportInfo } from "../ComponentInfo/Report/ZenReportInfo";
import { ZenFormInfo } from "../ComponentInfo/ZenForm/ZenFormInfo";
import { ZenDictionaryInfo } from "../ComponentInfo/Dictionary/ZenDictionaryInfo";

class Home extends Component {
  constructor(props) {
    super();
    InitPropsComponent();
    this.initRoutes();
    this.userInfo = auth.getUserInfo();
    this.today = new Date();
    this.state = {
      error: false,
      errorList: [],
      modules: [],
      navItems: [], //navs.getMenusDemo(),
      menus: [],
      dictionaryForm: [],
      voucherForm: [],
      voucherFormEdit: [],
      zenForm: [],
      reportForm: [],
      ...this.initRoutes()
    };
  }

  componentDidMount() {
    this.initMenu();
  }

  initMenu() {
    let global = GlobalStorage.get(KeyStorage.Global);

    const navItemsPromise = new Promise((resolve, reject) => {
      ApiMenu.getNav((res) => {
        if (res.status === 200) {
          resolve(res.data.data);
        } else {
          reject(res);
        }
      });
    });

    /* Chungtv - Lấy navbar theo API khác quan hệ Tree
    const menus = new Promise((resolve, reject) => {
      ApiMenu.get(res => {
        if (res.status === 200) {
          var temp
          if (Boolean(this.userInfo.admin) === true) {
            temp = res.data.data
          } else {
            const permissionUser = this.userInfo.permisson.split(",")
            temp = res.data.data.filter(t => permissionUser.includes(t.permission_id))
          }
          resolve(temp.filter(t => t.url.includes('/dm/') === false))
        } else {
          reject(res)
        }
      })
    })
*/
    const modules = new Promise((resolve, reject) => {
      ApiModule.get((res) => {
        if (res.status === 200) {
          resolve(res.data.data);
        } else {
          reject(res);
        }
      });
    });

    const namTC = new Promise((resolve, reject) => {
      resolve()
      // ApiSIDmNamTC.getByCode(
      //   global && global.financial_year ? global.financial_year : this.today.getFullYear(),
      //   (res) => {
      //     if (res.status === 200) {
      //       resolve(res.data.data);
      //     } else {
      //       resolve();
      //     }
      //   }
      // );
    });

    const filterMenuSys = (menus = []) => {
      var rs = []
      menus.map(menu => {
        const id = menu.menu_id;
        const a = id.split('.').join('');
        if (+a >= 900000) {
          rs.push(menu)
        }
      })
      return rs;
    }

    Promise.all([modules, navItemsPromise, namTC])
      .then((values) => {
        // set năm TC vào localstorage
        this.initGlobalStorage(values[2]);
        // set menu vào global context
        this.context.actions.setMenu(values[1])

        const menusys = filterMenuSys(values[1]);
        this.context.actions.setMenuSys(menusys);

        this.setState({
          modules: values[0],
          navItems: values[1]
        });
      })
      .catch((err) => {
        this.setState({
          error: true,
          errorList: ZenHelper.getResponseError(err),
        });
      });
  }

  initRoutes() {
    var dictionary = Object.keys(ZenDictionaryInfo).map(function (
      key
    ) {
      return ZenDictionaryInfo[key];
    });

    var voucher = Object.keys(ZenVoucherInfo).map(function (key) {
      return ZenVoucherInfo[key];
    });

    var voucherEdit = Object.keys(ZenVoucherInfoEdit).map(function (
      key
    ) {
      return ZenVoucherInfoEdit[key];
    });

    var reportForm = Object.keys(ReportInfo).map(function (key) {
      return ReportInfo[key];
    });

    var zenForm = Object.keys(ZenFormInfo).map(function (key) {
      return ZenFormInfo[key];
    });
    return {
      dictionaryForm: dictionary,
      voucherForm: voucher,
      voucherFormEdit: voucherEdit,
      reportForm: reportForm,
      zenForm: zenForm,
    }
  }

  initGlobalStorage(namTC = {}) {
    let global = GlobalStorage.get(KeyStorage.Global);
    if (!global) {
      global = {
        language: "vi-VN",
        financial_year: this.today.getFullYear(),
        from_date: ZenHelper.formatDateTime(
          ZenHelper.getfirstDayOfCurrentMonth(),
          "YYYY-MM-DD"
        ),
        to_date: ZenHelper.formatDateTime(
          ZenHelper.getLastDayOfCurrentMonth(),
          "YYYY-MM-DD 23:59:00"
        ),
        ngay_dntc: namTC.ngay_dntc,
        ngay_cntc: namTC.ngay_cntc,
      };
    } else {
      global.ngay_dntc = namTC.ngay_dntc;
      global.ngay_cntc = namTC.ngay_cntc;
    }
    GlobalStorage.set(KeyStorage.Global, global);
  }

  render() {
    var isloged = auth.checkLogin();
    if (!isloged) {
      return <Redirect to="/login" />;
    } else {
      auth.saveCurrentSession();
    }

    /**
     * Menu phải
     */
    const rightItems = navs.rightItems();

    const {
      error,
      errorList,
      modules,
      menus,
      navItems,
      dictionaryForm,
      voucherForm,
      voucherFormEdit,
      reportForm,
      zenForm,
    } = this.state;

    return (
      <div>
        <Navbar
          modules={modules}
          navItems={navItems}
          leftItems={menus}
          rightItems={rightItems}
        >
          {error && <Message negative list={errorList} />}
          <Suspense fallback={<ZenLoading loading size="big" inline={"centered"} indeterminate />}>
            <Switch>
              <PrivateRoute exact
                path="/"
                component={components.Dashboard}
                notPermission={true}
              />

              <PrivateRoute
                exact
                path={routes.Settings}
                component={components.Setting}
                notPermission={true}
              />

              <PrivateRoute
                exact
                path={routes.Home}
                component={components.Dashboard}
                notPermission={true}
              />

              <PrivateRoute
                exact
                path={routes.SystemInfo}
                component={components.SystemInfo}
                notPermission={true}
              />

              <PrivateRoute
                exact
                path={routes.Permission}
                component={components.Permission}
              />

              <PrivateRoute
                exact
                path={routes.Role}
                component={components.Role}
                permission={permissions.RoleXem}
              />
              <PrivateRoute
                exact
                path={routes.RoleInfo()}
                component={components.RoleInfo}
                permission={permissions.RoleXem}
              />
              <PrivateRoute
                exact
                path={routes.RoleInfoDashlet()}
                component={components.RoleDashlet}
                permission={permissions.RoleXem}
              />

              <PrivateRoute
                exact
                path={routes.User}
                component={components.User}
                permission={permissions.UserXem}
              />

              {dictionaryForm &&
                dictionaryForm.map((dic, idx) => {
                  const { view } = dic.action;
                  return (
                    view.visible && (
                      <PrivateRoute
                        key={idx}
                        exact
                        form={dic}
                        path={dic.route}
                        component={components.ZenDictionaryList}
                        permission={view.permission}
                        notPermission={view.notPermission}
                        initProps={InitPropsDictionaryList}
                      />
                    )
                  );
                })}

              {voucherForm &&
                voucherForm.map((item, idx) => {
                  const { view } = item.action;
                  return (
                    view.visible && (
                      <PrivateRoute
                        key={idx}
                        exact
                        form={item}
                        path={item.route}
                        component={components.ZenVoucherList}
                        permission={view.permission}
                        notPermission={view.notPermission}
                      />
                    )
                  );
                })}

              {zenForm &&
                zenForm.map((item, idx) => {
                  const { view } = item.action;
                  return (
                    view.visible && (
                      <PrivateRoute
                        key={idx}
                        exact
                        form={item}
                        path={item.route}
                        component={components.ZenForm}
                        permission={view.permission}
                        notPermission={view.notPermission}
                        initProps={InitPropsZenEmptyForm}
                      />
                    )
                  );
                })}

              {voucherFormEdit &&
                voucherFormEdit.map((item, idx) => {
                  const { view, add, edit } = item.action;

                  const routeNew = add.visible ? (
                    <PrivateRoute
                      key={"A" + idx}
                      exact
                      form={item}
                      path={item.route.add}
                      component={components.ZenVoucherForm}
                      permission={add.permission}
                      initProps={InitPropsZenVoucherForm}
                    />
                  ) : undefined;

                  const routeEdit = edit.visible ? (
                    <PrivateRoute
                      key={"E" + idx}
                      exact
                      form={item}
                      path={item.route.edit}
                      component={components.ZenVoucherForm}
                      permission={edit.permission}
                      initProps={InitPropsZenVoucherForm}
                    />
                  ) : undefined;

                  return [routeNew, routeEdit];
                })}

              {reportForm &&
                reportForm.map((rpt, idx) => {
                  return (
                    rpt.visible && (
                      <PrivateRoute
                        key={idx}
                        exact
                        form={rpt}
                        path={rpt.route}
                        component={components.ReportForm}
                        permission={rpt.permission}
                        initProps={InitPropsReportForm}
                      />
                    )
                  );
                })}

              <PrivateRoute
                exact
                path={routes.NotPermission}
                component={components.NotPermission}
                notPermission={true}
              />

              <PrivateRoute
                exact
                path={"*"}
                component={components.NotFound}
                notPermission={true}
              />

            </Switch>
          </Suspense>
        </Navbar>
      </div>
    );
  }
}

Home.contextType = AppContext;
export default Home;
