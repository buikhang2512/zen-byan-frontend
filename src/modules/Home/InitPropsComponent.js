import {
   ZenTableList, ZenFieldSelectApi, ZenDatePeriod, ZenLink, ZenFileViewer,
   ZenLookupDropdown, HeaderLink, SegmentFooter, InputDate, ZenModalLookup, ZenLookupSearch,
   InputDateMY, ZenTableBody, ZenFormJsonSchema, FileAttachment, ErrorBoundary, DiscussComment, ZenSelectSearch, ZenTableHeader
} from "../../components/Control";
import { ZenLookup } from "../ComponentInfo/Dictionary/ZenLookup";
import { getModal } from "../ComponentInfo/Dictionary/ZenGetModal";
import { ZenApp, auth, ZenHelper } from "../../utils";
import { ApiConfig, ApiFileAttachment, ApiImportExcel, ApiLookup, ApiReport, ApiSettingGrid, ApiStored, ApiZenLog } from "../../Api";
import * as routes from "../../constants/routes";
import { GlobalStorage, KeyStorage } from "../../utils/storage";
import ZenTableListMobile from "../../components/Control/ZenTableListMobile";
import ZenFieldSelectApiMulti from "../../components/Control/ZenFieldSelectApiMulti";
import ErrorPage from "../../components/PrivateRoute/ErrorPage";
import { ApiCRMDiscuss } from "../CRM/Api";
import { apiForCustomHook } from "../../components/Control/zzCustomHook";
import { searchCodeReport } from "../ComponentInfo/Report/ZenReportInfo";

// import { ApiSIDMCT } from "../SI/Api";

// ko import trực tiếp các file vào trong Component
// khởi tạo Props default cố định cho component
// Được gọi trong Home khi đăng nhập
export function InitPropsComponent() {
   ZenTableList.defaultProps = {
      ...ZenTableList.defaultProps,
      getModal: getModal,
      baseUrl: ZenApp.baseUrl,
      hiddenSetting: false, // ẩn/hiện thiết lập table góc trái trên
      apiChangeCode: ApiConfig.changeCode,
      checkPermission: (permis) => auth.checkPermission(permis),
      checkNgayKs: ZenHelper.checkNgayKs,
      dataCaching: {
         // key local storage
         keyCache: KeyStorage.CacheData,
         // code data cache, tương ứng với code trong cacheData.js
         settingGrid: "settinggrid",
         sisetup: "sisetup",
      },
      apiSettingGrid: {
         url: ApiSettingGrid,
         // default: get, getByCode, update
         // tên api khác default thì khai báo thêm, VD: get:"getAll", getByCode:"getSingle",... 
      },
   }
   ZenTableHeader.defaultProps = {
      defaultStyle: {
         filterStyle: {
            color: '#005c66',
         }
      },
   }
   ZenTableListMobile.defaultProps = {
      ...ZenTableListMobile.defaultProps,
      checkPermission: (permis) => auth.checkPermission(permis),
      colorItem: "blue"
   }
   ZenTableBody.defaultProps = {
      ...ZenTableBody.defaultProps,
      checkPermission: (permis) => auth.checkPermission(permis),
   }
   ZenFieldSelectApi.defaultProps = {
      ...ZenFieldSelectApi.defaultProps,
      keyStorageGlobal: KeyStorage.Global,
      api: { url: ApiLookup },
      getModal: getModal,
      checkPermission: (permis) => auth.checkPermission(permis),
      popupLookup: true,
   }
   ZenSelectSearch.defaultProps = {
      api: { url: ApiLookup },
   }
   ZenFieldSelectApiMulti.defaultProps = {
      ...ZenFieldSelectApiMulti.defaultProps,
      api: { url: ApiLookup },
      getModal: getModal,
      popupLookup: false,
   }
   ZenLookupSearch.defaultProps = {
      ...ZenLookupSearch.defaultProps,
      api: { url: ApiLookup },
      popupLookup: true
   }
   ZenLookupDropdown.defaultProps = {
      ...ZenLookupDropdown.defaultProps,
      api: { url: ApiLookup },
      popupLookup: true
   }
   ZenModalLookup.defaultProps = {
      ...ZenModalLookup.defaultProps,
      getModal: getModal,
      api: { url: ApiLookup },
      checkPermission: (permis) => auth.checkPermission(permis),
   }
   HeaderLink.defaultProps = {
      ...HeaderLink.defaultProps,
      routes: {
         home: routes.Home,
         settings: routes.Settings,
         reportCenter: routes.ReportModule,
      }
   }
   SegmentFooter.defaultProps = {
      color: "teal"
   }
   InputDate.defaultProps = {
      ...InputDate.defaultProps,
      financialYear: GlobalStorage.getByField(KeyStorage.Global, 'financial_year')
   }
   InputDateMY.defaultProps = {
      ...InputDateMY.defaultProps,
      financialYear: GlobalStorage.getByField(KeyStorage.Global, 'financial_year')
   }
   ZenDatePeriod.defaultProps = {
      ...ZenDatePeriod.defaultProps,
      keyLS: KeyStorage.Global   // key localstorage: dùng để set/get năm tài chính nếu có:{..., ngay_dntc, ngay_cntc}
   }
   ZenFormJsonSchema.defaultProps = {
      ...ZenFormJsonSchema.defaultProps,
      lookupInfo: ZenLookup
   }
   ZenLink.defaultProps = {
      ...ZenLink.defaultProps,
      checkPermission: (permis) => auth.checkPermission(permis),
   }
   FileAttachment.defaultProps = {
      ...FileAttachment,
      apiFileAttachment: { url: ApiFileAttachment },
      baseUrl: ZenApp.baseUrl,
      userInfo: auth.getUserInfo(),
   }
   DiscussComment.defaultProps = {
      ...DiscussComment,
      apiFileAttachment: { url: ApiFileAttachment },
      apiCRMDiscuss: { url: ApiCRMDiscuss },
      baseUrl: ZenApp.baseUrl,
      userInfo: auth.getUserInfo(),
   }
   ZenFileViewer.defaultProps = {
      ...ZenFileViewer.defaultProps,
      baseUrl: ZenApp.baseUrl,
      folderPath: "FileAttach"
   }
   ErrorBoundary.defaultProps = {
      ...ErrorBoundary.defaultProps,
      apiZenLog: ApiZenLog,
      Content: ErrorPage
   }

   apiForCustomHook.lookup = ApiLookup.get
}

export const InitPropsZenVoucherForm = {
   // required
   apiReport: { url: ApiReport },
   apiStored: { url: ApiStored },
   // apiSIDMCT: { url: ApiSIDMCT },
   checkPermission: (permis) => auth.checkPermission(permis),
   checkNgayKs: ZenHelper.checkNgayKs,
   financialYear: GlobalStorage.getByField(KeyStorage.Global, 'financial_year'),
   settingInvoice: {
      keyLS: KeyStorage.CacheData,
      table: "zeinvoicesetting"
   },
   zenlookup: {
      Ma_ct: ZenLookup.Ma_ct,
      Ma_cp: ZenLookup.Ma_cp,
      Ma_bp: ZenLookup.Ma_bp,
      Ma_phi: ZenLookup.Ma_phi,
      Ma_spct: ZenLookup.Ma_spct,
      Ma_lo: ZenLookup.Ma_lo,
      Ma_hd: ZenLookup.Ma_hd,
      Ma_ncc: ZenLookup.Ma_ncc,
      TK: ZenLookup.TK,
      Ma_nt: ZenLookup.Ma_nt
   },
   keyCacheData: KeyStorage.CacheData
}

export const InitPropsDictionaryList = {
   apiImportExcel: { url: ApiImportExcel },
}

export const InitPropsReportForm = {
   apiReport: { url: ApiReport },
   apiSettingGrid: { url: ApiSettingGrid },
   searchCodeReport: searchCodeReport,
}

export const InitPropsZenEmptyForm = {
   checkPermission: (permis) => auth.checkPermission(permis),
}
