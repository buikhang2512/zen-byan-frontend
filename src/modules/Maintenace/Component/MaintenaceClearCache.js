import React, { useContext } from "react";
import { Modal, Message, Button, Icon, Divider } from "semantic-ui-react";
import { Redirect } from 'react-router-dom';
import { AppContext } from "../../../AppContext";
import { GlobalStorage, KeyStorage } from "../../../utils/storage";
import { auth } from "../../../utils";
import { cacheData } from "../../../cacheData";
import { isBuffer } from "lodash-es";
import { ZenLoading, ZenMessageAlert, } from "../../../components/Control";

const MaintenanceClearCache = (props) => {
    const [isConfirmed, setConfirmed] = React.useState(false)
    const [loadingForm, setLoading] = React.useState(false)
    const { open, onClose, ...rest } = props
    const ctx = useContext(AppContext)

    async function clearcache () {
        await GlobalStorage.clear(KeyStorage.CacheData)
        await GlobalStorage.clear(KeyStorage.Global)
    }

    const handleSubmit = (e) => {
        setLoading(true)
        GlobalStorage.clear(KeyStorage.CacheData)
        GlobalStorage.clear(KeyStorage.Global)

        setTimeout(() => {
            cacheData(null,true,res => {
                if(res.status === 204) {
                    ZenMessageAlert.success()
                }
            })
            window.location.reload()
            setLoading(false)
            setConfirmed(true)
            onClose()
        },2000)
    }

    return (
        <React.Fragment>
            <Modal size='tiny' closeOnEscape
                open={open} onClose={onClose || undefined}
                >
                {
                    !isConfirmed
                        ? <>
                            <Message floating style={{ margin: "0px",textAlign:"center"}}
                                onDismiss={onClose}
                            >
                                <Message.Content >
                                    <Message.Header style={{ padding: "10px 0px 20px 0px", textAlign:"center", fontSize:"23px" }}>
                                    Bảo trì dữ liệu
                                    </Message.Header>
                                    { loadingForm && <Icon name={'circle notched'} loading={loadingForm} size="big" />}
                                    <p>Để tối ưu hóa hiệu xuất sử dụng, hệ thống thực hiện cache (lưu trữ tạm) các dữ liệu dùng chung tại trình duyệt. Việc bảo trì dữ liệu sẽ xóa bộ nhớ tạm lại trình duyệt để thực hiện lấy lại từ server.</p>
                                    <Divider clearing />

                                    <div style={{ textAlign: "right" }}>
                                        <Button icon="cancel" size="small" //labelPosition="right"
                                            content="Bỏ qua" onClick={onClose || undefined} />

                                        <Button icon="checkmark" size="small" primary //labelPosition="right"
                                            content="Thực hiện" onClick={handleSubmit} style={{ marginRight: "0px" }} />
                                    </div>
                                </Message.Content>
                            </Message>
                        </>
                        : <Redirect to="/" />
                }
            </Modal>
        </React.Fragment>
    );
}

export default React.memo(MaintenanceClearCache)