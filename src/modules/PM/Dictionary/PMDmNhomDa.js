import React, { useEffect } from "react";
import { ZenField, ZenFieldSelectApi, ZenFieldTextArea,ZenFieldNumber } from "../../../components/Control/index";
import { FormMode } from "../../../utils";
import { Form } from "semantic-ui-react";
import { ApiPMDmnhomda } from "../Api";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import * as permissions from "../../../constants/permissions";

const PMDmNhomDaModal = (props) => {
   const { formik, permission, mode } = props

   return <React.Fragment>
      <Form.Group widths="equal">
        <ZenField  required autoFocus readOnly={!permission || (mode === FormMode.ADD ? false : true)}
            label={"PMDmNhomDa.ma_nhda"} defaultlabel="Mã nhóm dự án"
            name="ma_nhda" props={formik}
            isCode
        />
        <ZenFieldNumber
            readOnly={!permission}
            label={"PMDmGiaiDoanDa.ordinal"} defaultlabel="Thứ tự hiển thị"
            name="ordinal" props={formik}
         />
        </Form.Group>
        <ZenField required readOnly={!permission}
            label={"PMDmNhomDa.ten_nhda"} defaultlabel="Tên nhóm dự án"
            name="ten_nhda" props={formik}
        />
      <ZenFieldTextArea 
         readOnly={!permission}
         label={"PMDmNhomDa.ghi_chu"} defaultlabel="Ghi Chú"
         name="ghi_chu" props={formik} 
      />
   </React.Fragment>
}

const PMDmNhomDa = {
   FormModal: PMDmNhomDaModal,
   api: {
      url: ApiPMDmnhomda,
   },
   permission: {
      view: permissions.PMDmNhomDaXem,
      add: permissions.PMDmNhomDaThem,
      edit: permissions.PMDmNhomDaSua
   },
   formId: "PMDmNhomDa-form",
   size: "tiny",
   isScrolling: false,
   initItem: {
      ma_nhda: "",
      ten_nhda: "",
      ghi_chu: "",
      ordinal: 0,
      ksd: false
   },
   formValidation: [
      {
         id: "ma_nhda",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
            // {
            //    type: "max",
            //    params: [8, "Không được vượt quá 8 kí tự"]
            // },
         ]
      },
      {
         id: "ten_nhda",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
         ]
      },
   ]
}

export { PMDmNhomDa };