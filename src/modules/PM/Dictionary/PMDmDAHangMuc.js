import React, { useEffect } from "react";
import { ZenField, ZenFieldSelectApi, ZenFieldTextArea, ZenFieldNumber } from "../../../components/Control/index";
import { FormMode } from "../../../utils";
import { Form } from "semantic-ui-react";
import { ApiPMDmDAHangMuc } from "../Api";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";

const PMDmDAHangMucModal = (props) => {
   const { formik, permission, mode } = props
   return <React.Fragment>
         <Form.Group >
         <ZenField width={10}  required autoFocus readOnly={!permission || (mode === FormMode.ADD ? false : true)}
               label={"PMDmDAHangMuc.ma_nhda"} defaultlabel="Mã hạng mục"
               name="ma_hang_muc" props={formik}
               isCode
         />
         
         <ZenFieldNumber 
         readOnly={!permission} width={6}
         label={"PMDmDAHangMuc.ordinal"} defaultlabel="Thứ tự hiển thị"
         name="ordinal" props={formik} 
      />
        </Form.Group>
        <ZenField required readOnly={!permission}
               label={"PMDmDAHangMuc.ten_nhda"} defaultlabel="Tên hạng mục"
               name="ten_hang_muc" props={formik}
         />
         <ZenFieldTextArea 
         readOnly={!permission}
         label={"PMDmNhomDa.ghi_chu"} defaultlabel="Ghi Chú"
         name="ghi_chu" props={formik} 
      />
   </React.Fragment>
}

const customHeader = (props, state) => {
   const { id, header, fieldCode, formMode, currentItem, intl } = props
   const { valueFieldKey } = state
   const txtHeader = !id ? ('Thêm mới hạng mục')
       : ('Hạng mục' + ": " + id)
   return txtHeader
 }

const PMDmDAHangMuc = {
   FormModal: PMDmDAHangMucModal,
   customHeader,
   api: {
      url: ApiPMDmDAHangMuc,
   },
   permission: {
      view: "",
      add: "",
      edit: ""
   },
   formId: "PMDmDAHangMuc-form",
   size: "tiny",
   isScrolling: false,
   initItem: {
      ma_hang_muc: "",
      ten_hang_muc: "",
      ma_du_an:"",
      ghi_chu:"",
      ordinal: "",
      ksd: false
   },
   formValidation: [
      {
         id: "ma_hang_muc",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
            // {
            //    type: "max",
            //    params: [8, "Không được vượt quá 8 kí tự"]
            // },
         ]
      },
      {
         id: "ten_hang_muc",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
         ]
      },
      {
         id: "ordinal",
         validationType: "number",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
         ]
      },
   ]
}

export { PMDmDAHangMuc };