import React, { useEffect, useState } from "react";
import { ZenField, ZenFieldSelectApi, ZenFieldTextArea, ZenFieldNumber, ZenFieldDate, ZenFieldSelect } from "../../../components/Control/index";
import { FormMode, ZenHelper } from "../../../utils";
import { Form } from "semantic-ui-react";
import { ApiPMDuAn } from "../Api";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { zzControlHelper } from "../../../components/Control/zzControlHelper";
import { GlobalStorage, KeyStorage } from "../../../utils/storage";
import _ from "lodash";
import * as permissions from "../../../constants/permissions";

const PMDuAnModal = (props) => {
   const { formik, permission, mode } = props
   const [kh, setKH] = useState([]);
   const [nv, setNV] = useState([]);

   useEffect(() => {
      const _kh = GlobalStorage.getByField(KeyStorage.CacheData, "ma_kh")?.data
      const _nv = GlobalStorage.getByField(KeyStorage.CacheData, "ma_nvns")?.data
      setKH(ZenHelper.translateListToSelectOptions(_kh, false, 'ma_kh', 'ma_kh', 'ma_kh', [], 'ten_kh'))
      setNV(ZenHelper.translateListToSelectOptions(_nv, false, 'id_nv', 'id_nv', 'id_nv', [], 'ho_ten'))
   }, [])

   const handleItemSelected = (option, { name }) => {
      const item = {
         [name]: option.value
      }
      if (name === "ma_gdoan_da") {
         item.ten_gdoan_da = option.ten_gdoan_da
      } else if (name === "ma_nhda") {
         item.ten_nhda = option.ten_nhda
      } else if (name === "id_nv_pm") {
         item.ten_nv_pm = option.ho_ten
      }
      formik.setValues({ ...formik.values, ...item })
   }
   const handlemultiSelected = (e, i) => {
      if (i.name === "members") {
         formik.setFieldValue('members', _.join(i.value, ','))
      } else if (i.name === "khs") {
         formik.setFieldValue('khs', _.join(i.value, ','))
      }
   }

   return <React.Fragment>
      <Form.Group>
         <ZenField width={8} required autoFocus readOnly={!permission || (mode === FormMode.ADD ? false : true)}
            label={"PMDuAnDa.ma_du_an"} defaultlabel="Mã dự án"
            name="ma_du_an" props={formik}
         />
         <ZenFieldSelectApi width={8}
            required
            lookup={ZenLookup.PMDmNhomDa}
            formik={formik}
            name="ma_nhda"
            label={"PMDuAnDa.ma_nhda"} defaultlabel="Mã nhóm dự án"
            onItemSelected={handleItemSelected}
         />
      </Form.Group>
      <ZenField required readOnly={!permission}
         label={"PMDuAnDa.ten_du_an"} defaultlabel="Tên dự án"
         name="ten_du_an" props={formik}
      />
      <Form.Group>
         <ZenFieldDate width={8}
            name="ngay_bd"
            props={formik}
            label={"PMDuAnDa.ngay_bd"} defaultlabel="Ngày bắt đầu"
         />
         <ZenFieldDate width={8}
            name="ngay_kt"
            props={formik}
            label={"PMDuAnDa.ngay_kt"} defaultlabel="Ngày kết thúc"
         />
      </Form.Group>
      <ZenFieldSelectApi
         lookup={ZenLookup.PMDmGiaiDoanDa}
         formik={formik}
         name="ma_gdoan_da"
         label={"PMDuAnDa.ma_gdoan_da"} defaultlabel="Giai đoạn"
         onItemSelected={handleItemSelected}
      />

      <ZenFieldSelectApi
         lookup={ZenLookup.ID_NV}
         formik={formik}
         name="id_nv_pm"
         label={"PMDuAnDa.id_nv_pm"} defaultlabel="Quản trị dự án"
         onItemSelected={handleItemSelected}
      />

      <ZenFieldSelect
         multiple
         options={nv}
         props={formik}
         value={formik.values.id_nv?.split(',')}
         name="members"
         label={"PMDuAnDa.members"} defaultlabel="Thành viên dự án"
         onChange={handlemultiSelected}
      />
      <ZenFieldSelect
         multiple
         options={kh}
         props={formik}
         value={formik.values.ma_kh?.split(',')}
         name="khs"
         label={"PMDuAnDa.khs"} defaultlabel="Khách hàng"
         onChange={handlemultiSelected}
      />

      <ZenFieldTextArea
         readOnly={!permission}
         label={"PMDuAnDa.ghi_chu"} defaultlabel="Ghi chú"
         name="ghi_chu" props={formik}
      />
   </React.Fragment>
}

const onValidate = (item) => {
   const errors = {};
   const { ngay_bd, ngay_kt } = item
   const ngay1 = parseInt(ngay_bd) > 1900 ? ngay_bd : null
   const ngay3 = parseInt(ngay_kt) > 1900 ? ngay_kt : null
   if (ngay1 > ngay3) {
      errors.ngay_bd = 'Ngày bắt đầu không được lớn hơn ngày kết thúc';
   }
   return errors
}

const PMDuAn = {
   FormModal: PMDuAnModal,
   onValidate: onValidate,
   api: {
      url: ApiPMDuAn,
   },
   permission: {
      view: permissions.PMDuAnXem,
      add: permissions.PMDuAnThem,
      edit: permissions.PMDuAnXoa
   },
   formId: "PMDuAnDa-form",
   size: "large",
   isScrolling: true,
   initItem: {
      ma_du_an: "",
      ten_du_an: "",
      loai_du_an: "",
      ma_nhda: "",
      id_nv_pm: "",
      ma_gdoan_da: "",
      ngay_bd: "",
      ngay_kt: "",
      ghi_chu: ""
   },
   formValidation: [
      {
         id: "ma_du_an",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
            // {
            //    type: "max",
            //    params: [8, "Không được vượt quá 8 kí tự"]
            // },
         ]
      },
      {
         id: "ten_du_an",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
         ]
      },
      {
         id: "ma_nhda",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
         ]
      },
      {
         id: "ma_gdoan_da",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
         ]
      },
   ]
}

export { PMDuAn };