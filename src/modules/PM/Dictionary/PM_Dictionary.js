import * as routes from '../../../constants/routes';
import { ZenLookup } from '../../ComponentInfo/Dictionary/ZenLookup';
import { ApiPMDmGiaiDoanDa, ApiPMDmnhomda, ApiPMDuAn } from '../Api';
import * as permissions from "../../../constants/permissions";

const PM_Dictionary = {
    PMDmNhDa : {
        route: routes.PMDMNhDa,

        action: {
            view: { visible: true, permission: permissions.PMDmNhomDaXem },
            add: { visible: true, permission: permissions.PMDmNhomDaThem },
            edit: { visible: true, permission: permissions.PMDmNhomDaSua },
            del: { visible: true, permission: permissions.PMDmNhomDaXoa },
            dup: { visible: true, permission: "" }
         },

         linkHeader: {
             id: 'pmdmnhomda',
             defaultMessage: "Danh mục nhóm dự án",
             active: true,
             isSetting: true,
         },

         tableList: {
             keyForm: "PMDMNHOMDA",
             formModal: ZenLookup.PMDmNhomDa,
             fieldCode: "ma_nhda",
             unPagination: false,
             duplicate: true,
             showFilterLabel: true,
            
             api: {
                 url: ApiPMDmnhomda,
                 type: "sql",
             },
            columns: [
                {id: "pmdmnhomda.ma_nhda", defaultMessage: "Mã nhóm DA", fieldName: "ma_nhda", filter: "string", sorter: true, editForm: true},
                {id: "pmdmnhomda.ten_nhda", defaultMessage: "Tên nhóm DA", fieldName: "ten_nhda", filter: "string", sorter: true, },
                {id: "PMDmGiaiDoanDa.ordinal", defaultMessage: "Thứ tự hiển thị", fieldName: "ordinal", filter: "number", sorter: true, },
                {id: "pmdmnhomda.ghi_chu", defaultMessage: "Ghi chú", fieldName: "ghi_chu", filter: "string", sorter: true, },
                { id: "ksd", defaultMessage: "Không sử dụng", fieldName: "ksd", type: "bool", filter: "", sorter: true, collapsing: true },
            ]
         }
    },

    PMDmGiaiDoanDa : {
        route: routes.PMDMGiaiDoanDa,

        action: {
            view: { visible: true, permission: permissions.PMDmGiaiDoanDaXem },
            add: { visible: true, permission: permissions.PMDmGiaiDoanDaThem },
            edit: { visible: true, permission: permissions.PMDmGiaiDoanDaSua },
            del: { visible: true, permission: permissions.PMDmGiaiDoanDaXoa },
         },

         linkHeader: {
             id: 'pmdmgiaidoanda',
             defaultMessage: "Danh mục giai đoạn dự án",
             active: true,
             isSetting: true,
         },

         tableList: {
             keyForm: "PMDMGiaiDoanDa",
             formModal: ZenLookup.PMDmGiaiDoanDa,
             fieldCode: "ma_gdoan_da",
             unPagination: false,
             duplicate: true,
             showFilterLabel: true,
            
             api: {
                 url: ApiPMDmGiaiDoanDa,
                 type: "sql",
             },
            columns: [
                {id: "PMDmGiaiDoanDa.ma_gdoan_da", defaultMessage: "Mã giai đoạn", fieldName: "ma_gdoan_da", filter: "string", sorter: true, editForm: true},
                {id: "PMDmGiaiDoanDa.ten_gdoan_da", defaultMessage: "Tên giai đoạn", fieldName: "ten_gdoan_da", filter: "string", sorter: true, },
                {id: "PMDmGiaiDoanDa.ordinal", defaultMessage: "Thứ tự hiển thị", fieldName: "ordinal", filter: "string", sorter: true, },
                { id: "ksd", defaultMessage: "Không sử dụng", fieldName: "ksd", type: "bool", filter: "", sorter: true, collapsing: true },
            ]
         }
    },
    PMDuAn : {
        route: routes.PMDuAn,

        action: {
            view: { visible: true, permission: permissions.PMDuAnXem },
            add: { visible: true, permission: permissions.PMDuAnThem },
            edit: { visible: true, permission: permissions.PMDuAnSua },
            del: { visible: true, permission: permissions.PMDuAnXoa },
         },

         linkHeader: {
             id: 'pmduan',
             defaultMessage: "Dự án",
             active: true,
             isSetting: true,
         },

         tableList: {
             keyForm: "PMDuAn",
             formModal: ZenLookup.PMDuAn,
             fieldCode: "ma_du_an",
             unPagination: false,
             duplicate: true,
             showFilterLabel: true,
            
             api: {
                 url: ApiPMDuAn,
                 type: "sql",
             },
            columns: [
                {id: "PMDuAn.ma_du_an", defaultMessage: "Mã dự án", fieldName: "ma_du_an", filter: "string", sorter: true, link: { route: routes.PMDuAnDetail(), params: "ma_du_an"}},
                {id: "PMDuAn.ten_du_an", defaultMessage: "Tên dự án", fieldName: "ten_du_an", filter: "string", sorter: true, },
                {id: "PMDuAn.ten_gdoan_da", defaultMessage: "Giai đoạn", fieldName: "ten_gdoan_da", filter: "string", sorter: true, },
                {id: "PMDuAn.ten_nhda", defaultMessage: "Tên nhóm DA", fieldName: "ten_nhda", filter: "string", sorter: true, },
                {id: "PMDuAn.ten_nv_pm", defaultMessage: "Nhân viên phụ trách", fieldName: "ten_nv_pm", filter: "string", sorter: true, },
            ]
         }
    }
}

export default PM_Dictionary