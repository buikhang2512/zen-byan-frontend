import React, { useEffect } from "react";
import { ZenField, ZenFieldSelectApi, ZenFieldTextArea, ZenFieldNumber } from "../../../components/Control/index";
import { FormMode } from "../../../utils";
import { Form } from "semantic-ui-react";
import { ApiPMDmGiaiDoanDa } from "../Api";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import * as permissions from "../../../constants/permissions";

const PMDmGiaiDoanDaModal = (props) => {
   const { formik, permission, mode } = props

   return <React.Fragment>
        <ZenField width={8}  required autoFocus readOnly={!permission || (mode === FormMode.ADD ? false : true)}
            label={"PMDmGiaiDoanDa.ma_nhda"} defaultlabel="Mã giai đoạn"
            name="ma_gdoan_da" props={formik}
            isCode
        />
        <ZenField required readOnly={!permission}
            label={"PMDmGiaiDoanDa.ten_nhda"} defaultlabel="Tên giai đoạn"
            name="ten_gdoan_da" props={formik}
        />
      <ZenFieldNumber 
         readOnly={!permission} width={6}
         label={"PMDmGiaiDoanDa.ordinal"} defaultlabel="Thứ tự hiển thị"
         name="ordinal" props={formik} 
      />
   </React.Fragment>
}

const PMDmGiaiDoanDa = {
   FormModal: PMDmGiaiDoanDaModal,
   api: {
      url: ApiPMDmGiaiDoanDa,
   },
   permission: {
      view: permissions.PMDmGiaiDoanDaXem,
      add: permissions.PMDmGiaiDoanDaThem,
      edit: permissions.PMDmGiaiDoanDaSua
   },
   formId: "PMDmGiaiDoanDa-form",
   size: "tiny",
   isScrolling: false,
   initItem: {
      ma_gdoan_da: "",
      ten_gdoan_da: "",
      ordinal: "",
      ksd: false
   },
   formValidation: [
      {
         id: "ma_gdoan_da",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
            // {
            //    type: "max",
            //    params: [8, "Không được vượt quá 8 kí tự"]
            // },
         ]
      },
      {
         id: "ten_gdoan_da",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
         ]
      },
      {
         id: "ordinal",
         validationType: "number",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
         ]
      },
   ]
}

export { PMDmGiaiDoanDa };