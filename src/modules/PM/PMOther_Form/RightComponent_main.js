import React, { useContext, useEffect, useMemo, useRef, useState } from "react";
import {
   Button,
   Card, Comment, Form,
   Menu, Message, Segment, TextArea, Modal, Table, Icon, Grid,
} from "semantic-ui-react";
import { ApiStored } from "../../../Api";
import { NoImage } from "../../../resources/index";
import {
   ZenMessageAlert, ZenButton, DiscussComment,
   FormatDate, ConfirmDelete, ZenMessageToast,
   ZenFieldSelectApi, ZenField, ZenFormik, ZenLoading, ContainerScroll,
   ButtonDelete, ButtonEdit, ZenFieldTextArea, ZenLink, ZenFieldDate,
} from "../../../components/Control/index";
import { FormMode, zzControlHelper } from "../../../components/Control/zzControlHelper";
import { ZenHelper } from "../../../utils";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { getModal } from "../../ComponentInfo/Dictionary/ZenGetModal";
//import { ApiCRMDeal, ApiCRMDiscuss, ApiCRMHoatDong, ApiCRMNote, ApiSOVchSO0 } from "../../../CRM/Api";
import { IntlFormat } from "../../../utils/intlFormat";
import { ValidError } from "../../../utils/language/variable";
// import { ApiArDmKh } from "../../Api";
import { ContextPMDuAnDetail } from "./PMDuAnDetail";
import { FormattedMessage } from "react-intl";
import { Link, useHistory, } from "react-router-dom";
import * as routes from "../../../constants/routes"
import { auth } from "../../../utils";
import _ from "lodash";
import { ApiPMDmDAHangMuc, ApiPMDuAn } from "../Api";

const ContextMenu = React.createContext({})

export const RightComponentMain = ({ itemDuAn, keyData, type, idElementContainer, ...rest }) => {
   return <MenuBar idElementContainer={idElementContainer} typeForm={type}>
      {
         ({ activeMenu }) => {
            return <>
               {menuBar.DAInfo.visible && menuBar.DAInfo.name === activeMenu && <TabDAInfo itemDuAn={itemDuAn} type={type} keyData={keyData} {...rest} />}
               {menuBar.HangMuc.visible && menuBar.HangMuc.name === activeMenu && <TabHangMuc data={itemDuAn} type={type} keyData={keyData} {...rest} />}
               {menuBar.ThanhVien.visible && menuBar.ThanhVien.name === activeMenu && <TabNs data={itemDuAn} type={type} keyData={keyData} {...rest} />}
            </>
         }
      }
   </MenuBar>
}

const MenuBar = ({ children, typeForm, idElementScroll = "content-right", idElementContainer }) => {
   // ************ MEMO
   const memoizeMenu = useMemo(() => getMenu(), [""])
   // ************ STATE
   const [loadingForm, setLoadingForm] = useState(true)
   const [activeMenu, setActiveMenu] = useState(memoizeMenu.defaultMenu)

   useEffect(() => {
      return () => {
         memoryStore.set(typeForm, null)
      }
   }, [])

   // ************ HANDLE
   const handleChangeTab = (e, { name }) => {
      if (name !== activeMenu) {
         setActiveMenu(name)
         setLoadingForm(true)
         memoryStore.set(typeForm, { activeMenu: name })
      }
   }

   // ************ FUNCTION
   function getMenu() {
      let defaultMenu;
      const menus = Object.keys(menuBar).map(menu => {
         const itemMenu = menuBar[menu]
         if (itemMenu.default) {
            // default tab theo khai báo
            defaultMenu = itemMenu.name
         }
         return itemMenu
      })
      // get activeMenu in memory
      const tempActiveMenu = memoryStore.get(typeForm)
      defaultMenu = tempActiveMenu && tempActiveMenu.activeMenu ? tempActiveMenu.activeMenu : defaultMenu
      return {
         menus: menus,
         defaultMenu: defaultMenu
      }
   }

   return <ContextMenu.Provider
      value={{
         setLoadingForm: (isLoad) => setLoadingForm(isLoad),
         loadingForm: loadingForm,
      }}
   >
      <Segment style={{height:"100%"}}>
         <Menu pointing secondary>
            {memoizeMenu.menus.map(menu => {
               const { visible, ...restMenu } = menu
               return visible ? <Menu.Item key={menu.name}
                  {...restMenu}
                  active={activeMenu === menu.name}
                  onClick={handleChangeTab}
               /> : undefined
            })}
         </Menu>

         <ContainerScroll
            id={idElementScroll} isSegment={false}
            idElementContainer={idElementContainer}
            heightSub={28}
         >
            {children({ activeMenu: activeMenu })}
            <ZenLoading loading={loadingForm} inline="centered" />
         </ContainerScroll>
      </Segment>
   </ContextMenu.Provider>
}

const TabDAInfo = (props) => {
   const { itemDuAn, type, keyData } = props;
   const { setLoadingForm, loadingForm } = useContext(ContextMenu);
   const { setitemDuAn } = useContext(ContextPMDuAnDetail);
   const [permission, setPermission] = useState(false);
   const [loadingSave, setLoadingSave] = useState(false);
   const [dataDA, setDataDA] = useState();
   const refForm = useRef();

   useEffect(() => {
      setLoadingForm(false)
   }, [])

   useEffect(() => {
      if (itemDuAn?.khs) {
         delete itemDuAn.khs
      }
      if (itemDuAn?.members) {
         delete itemDuAn.members
      }
      if (itemDuAn?.followers) {
         delete itemDuAn.followers
      }
      setDataDA({ ...itemDuAn })
   }, [itemDuAn])

   const memoSiDmLoai = useMemo(() => {
      const listSiDmLoai = ZenHelper.getDataLocal(ZenLookup.SIDmloai.code)
      const data = listSiDmLoai?.data.filter(t => t.ma_nhom === "PLKH")
      var result = data.reduce(
         (obj, item) => Object.assign(obj, { [item.ma]: item.ten }), {}
      );
      return result
   }, [])

   const handleEdit = () => {
      if (permission) {
         refForm.current.handleReset()
      }
      setPermission(!permission)
   }

   const handleSubmit = (values) => {
      setLoadingSave(true)
      ApiPMDuAn.update(values, res => {
         if (res.status === 204) {
            setitemDuAn({ ...itemDuAn, ...values })
            ZenMessageToast.success();
         } else {
            ZenMessageAlert.error(zzControlHelper.getResponseError(res))
         }
         setPermission(!permission)
         setLoadingSave(false)
      })
   }

   const handleItemSelected = (option, { name }) => {
      if (name === "ma_gdoan_da") {
         refForm.current.setFieldValue("ten_gdoan_da", option.ten_gdoan_da)
      } else if (name === "ma_nhda") {
         refForm.current.setFieldValue("ten_nhda", option.ten_nhda)
      } else if (name === "id_nv_pm") {
         refForm.current.setFieldValue("ten_nv_pm", option.ho_ten)
      }
   }

   return <>
      <ZenFormik form={"TabDAInfo"}
         ref={refForm}
         validation={formValidationDA}
         initItem={dataDA}
         onSubmit={handleSubmit}
      >
         {
            formik => {
               return <Form>
                  <Form.Group>
                     <ZenField width={8} required autoFocus readOnly={true}
                        label={"PMDuAnDa.ma_du_an"} defaultlabel="Mã dự án"
                        name="ma_du_an" props={formik}
                     />
                     <ZenFieldSelectApi width={8}
                        required
                        lookup={ZenLookup.PMDmNhomDa}
                        formik={formik}
                        name="ma_nhda"
                        label={"PMDuAnDa.ma_nhda"} defaultlabel="Mã nhóm dự án"
                        onItemSelected={handleItemSelected}
                        readOnly={!permission}
                     />
                  </Form.Group>
                  <ZenField required readOnly={!permission}
                     label={"PMDuAnDa.ten_du_an"} defaultlabel="Tên dự án"
                     name="ten_du_an" props={formik}
                     readOnly={!permission}
                  />
                  <Form.Group>
                     <ZenFieldDate width={8}
                        name="ngay_bd"
                        props={formik}
                        label={"PMDuAnDa.ngay_bd"} defaultlabel="Ngày bắt đầu"
                        readOnly={!permission}
                     />
                     <ZenFieldDate width={8}
                        name="ngay_kt"
                        props={formik}
                        label={"PMDuAnDa.ngay_kt"} defaultlabel="Ngày kết thúc"
                        readOnly={!permission}
                     />
                  </Form.Group>
                  <ZenFieldSelectApi
                     lookup={ZenLookup.PMDmGiaiDoanDa}
                     formik={formik}
                     name="ma_gdoan_da"
                     label={"PMDuAnDa.ma_gdoan_da"} defaultlabel="Giai đoạn"
                     onItemSelected={handleItemSelected}
                     readOnly={!permission}
                  />

                  <ZenFieldSelectApi
                     lookup={ZenLookup.ID_NV}
                     formik={formik}
                     name="id_nv_pm"
                     label="pmdans.id_nv_pm" defaultlabel="Quản trị dự án"
                     readOnly={!permission}
                  />

                  <ZenFieldTextArea readOnly={!permission}
                     label={"ardmkh.ghi_chu"} defaultlabel="Ghi chú"
                     readOnly={!permission}
                     name="ghi_chu" props={formik} />
               </Form>
            }
         }
      </ZenFormik>
      <div style={{ paddingTop: "14px" }}>
         {permission ? <>
            <ZenButton btnType="save"
               size="small"
               loading={loadingSave}
               onClick={() => refForm.current.handleSubmit()}
            />
            <ZenButton btnType="cancel"
               size="small"
               loading={loadingSave}
               onClick={handleEdit}
            />
         </>
            : <Button primary
               size="small"
               icon="edit"
               content={"Sửa thông tin chung"}
               onClick={handleEdit}
            />}
      </div>
   </>
}

const TabHangMuc = ({ data, keyData = "id", type }) => {
   const { setLoadingForm, loadingForm } = useContext(ContextMenu);
   const _isMounted = useRef(true);
   const [err, setErr] = useState();
   const [items, setItems] = useState([]);
   const [dt, setData] = useState([]);
   const [infoModal, setInfoModal] = useState({});

   useEffect(() => {
      if (data) setData(data)
      const info = getModal(ZenLookup.PMDmDAHangMuc)
      setInfoModal(info)

      return () => {
         _isMounted.current = false
      }
   }, [])

   useEffect(() => {
      getItems()
   }, [data])

   // ********************** HANDLE
   const handleOpenCloseModal = (formMode, item) => {
      setInfoModal({
         ...infoModal,
         info: {
            ...infoModal.info,
            initItem: { ...infoModal.info.initItem, [keyData]: data[keyData] },
         },
         id: item ? item.id : "",
         formMode: formMode,
         open: true,
         other: {
            id: data[keyData],
            keyData: keyData,
            type: type,
         }
      })
   }

   const handleAfterSaveModal = (newItem, { mode }) => {
      // change
      if (mode === FormMode.ADD) {
         getItems()
         //setItems(items.concat(newItem))
      } else if (mode === FormMode.EDIT) {
         setItems(items.map(t => t.id === newItem.id ? newItem : t))
      }
      // close modal
      setInfoModal({
         ...infoModal,
         id: "",
         formMode: FormMode.NON,
         open: false,
      })
   }

   const handleDeleteItem = (item) => {
      ZenMessageAlert.question("Bạn có chắc muốn xóa không?")
         .then(success => {
            if (success == 1) {
               ApiPMDmDAHangMuc.delete(item.id, res => {
                  if (res.status >= 200 && res.status <= 204) {
                     setItems(items.filter(t => t.ma_hang_muc != item.ma_hang_muc))
                     ZenMessageToast.success()
                  } else {
                     ZenMessageAlert.error(res)
                  }
               })
            }
         })
   }

   // ********************** FUNCTION
   function getItems() {
      if (data) {
         ApiPMDmDAHangMuc.get(res => {
            if (_isMounted.current) {
               if (res.status === 200) {
                  setItems(res.data.data)
               } else {
                  setErr(ZenHelper.getResponseError(res))
               }
               loadingForm && setLoadingForm(false)
            }
         }, { qf: `a.${[keyData]} = '${data[keyData]}'` })
         return
      }
      if (dt) {
         ApiPMDmDAHangMuc.get(res => {
            if (_isMounted.current) {
               if (res.status === 200) {
                  setItems(res.data.data)
               } else {
                  setErr(ZenHelper.getResponseError(res))
               }
               loadingForm && setLoadingForm(false)
            }
         }, { qf: `a.${[keyData]} = '${dt[keyData]}'` })
         return
      }
   }

   function renderList() {
      return <Table compact selectable striped
         className="crm-sticky-table"
      >
         <Table.Header>
            <Table.Row>
               <Table.HeaderCell content={"Mã hạng mục"} />
               <Table.HeaderCell content={"Tên hạng mục"} />
               <Table.HeaderCell content={"Thứ tự"} />
               <Table.HeaderCell content={"Ghi Chú"} />
               <Table.HeaderCell content="" />
            </Table.Row>
         </Table.Header>
         <Table.Body>
            {

               items.sort((a,b) => a.ordinal > b.ordinal ? 1 : -1).map(item => {
                  return <Table.Row key={item["ma_hang_muc"]}
                     onDoubleClick={() => handleOpenCloseModal(FormMode.VIEW, item)}
                  >
                     <Table.Cell>
                        {/* <ZenLink style={{ fontWeight: "bold" }}
                           to={routes.SIDmHdDetail(zzControlHelper.btoaUTF8(item.ma_hd))}
                        >  
                           {item.ma_hd}
                        </ZenLink> */}
                        <a style={{ fontWeight: "bold", cursor: "pointer" }}
                           onClick={() => handleOpenCloseModal(FormMode.EDIT, item)}
                        >
                           {item.ma_hang_muc}
                        </a>
                     </Table.Cell>
                     <Table.Cell content={item.ten_hang_muc} />
                     <Table.Cell content={item.ordinal} />
                     <Table.Cell content={item.ghi_chu} />
                     <Table.Cell collapsing>
                        <Button.Group size="small" style={{ float: "right" }}>
                           <ButtonEdit
                              onClick={() => handleOpenCloseModal(FormMode.EDIT, item)}
                           />
                           <ButtonDelete
                              onClick={() => handleDeleteItem(item)}
                           />
                        </Button.Group>
                     </Table.Cell>
                  </Table.Row>
               })
            }
         </Table.Body>
      </Table>
   }

   return <>
      <ZenButton btnType="add" size="tiny" floated="right"
         onClick={() => handleOpenCloseModal(FormMode.ADD)}
      />
      <div style={{ clear: "both" }} />

      {err && <Message list={err} negative header="Error" />}

      {renderList()}

      {zzControlHelper.openModalComponent(infoModal,
         {
            onClose: () => handleOpenCloseModal(FormMode.NON),
            onAfterSave: handleAfterSaveModal,
         }
      )}
   </>
}

const TabNs = ({ data, keyData = "id", type }) => {
   const { setLoadingForm, loadingForm } = useContext(ContextMenu);
   const _isMounted = useRef(true);
   const [err, setErr] = useState();
   const [dataNs, setDataNs] = useState()
   const [itemsNs, setItemsNs] = useState([]);
   const [itemsFl, setItemsFl] = useState([]);
   const [infoModal, setInfoModal] = useState({});
   const refFormikNs = useRef();
   const refFormikFl = useRef();

   const initItemNs = { id_nv: "", ho_ten: "", loai: "1", [keyData]: data[keyData], ksd: false }
   const formValidationNs = []

   const initItemFl = { id_nv: "", ho_ten: "", loai: "2", [keyData]: data[keyData], ksd: false }
   const formValidationFl = []

   useEffect(() => {
      getItems()
   }, [])

   // ********************** HANDLE

   const handleDeleteItem = (item) => {
      ZenMessageAlert.question("Bạn có chắc muốn xóa không?")
         .then(success => {
            if (success == 1) {
               ApiPMDuAn.deleteNS(data[keyData], item.id_nv, item.loai, res => {
                  if (res.status >= 200 && res.status <= 204) {
                     if (item.loai == "1") {
                        setItemsNs(itemsNs.filter(t => t.id != item.id))
                     }
                     if (item.loai == "2") {
                        setItemsFl(itemsFl.filter(t => t.id != item.id))
                     }
                     ZenMessageToast.success()
                  } else {
                     ZenMessageAlert.error(res)
                  }
               })
            }
         })
   }

   // ********************** FUNCTION
   function getItems() {
      ApiPMDuAn.getNSByCode(data[keyData], "1", res => {
         if (_isMounted.current) {
            if (res.status === 200) {
               setItemsNs(res.data.data)
            } else {
               setErr(ZenHelper.getResponseError(res))
            }
            loadingForm && setLoadingForm(false)
         }
      })

      ApiPMDuAn.getNSByCode(data[keyData], "2", res => {
         if (_isMounted.current) {
            if (res.status === 200) {
               setItemsFl(res.data.data)
            } else {
               setErr(ZenHelper.getResponseError(res))
            }
            loadingForm && setLoadingForm(false)
         }
      })
   }

   function renderList(items) {
      return <Table compact selectable striped
         className="crm-sticky-table"
      >
         <Table.Header>
            <Table.Row>
               <Table.HeaderCell content={"ID"} />
               <Table.HeaderCell content={"Họ và tên"} />
               <Table.HeaderCell content="" />
            </Table.Row>
         </Table.Header>
         <Table.Body>
            {

               items.map(item => {
                  return <Table.Row key={item["id_nv"]}
                     onDoubleClick={() => handleOpenCloseModal(FormMode.VIEW, item)}
                  >
                     <Table.Cell>
                        {item.id_nv}
                     </Table.Cell>
                     <Table.Cell content={item.ten_nv} />
                     <Table.Cell collapsing>
                        <Icon name="x" link color="red"
                           onClick={() => handleDeleteItem(item)} />
                     </Table.Cell>
                  </Table.Row>
               })
            }
         </Table.Body>
      </Table>
   }

   const handleChangeNs = (option, { name }) => {
      if (name === "id_nv") {
         refFormikNs.current.setFieldValue('ten_nv', option?.ho_ten)
      }
   }

   const handleChangeFl = (option, { name }) => {
      if (name === "id_nv") {
         refFormikFl.current.setFieldValue('ten_nv', option?.ho_ten)
      }
   }

   const handleAdd = (params) => {
      if (params.id_nv) {
         ApiPMDuAn.insertNS(data[keyData], params, res => {
            if (res.status === 204) {
               if (params.loai === "1") {
                  setItemsNs(itemsNs?.concat(params))
                  refFormikNs.current.setValues(initItemNs)
               }
               if (params.loai === "2") {
                  setItemsFl(itemsFl?.concat(params))
                  refFormikFl.current.setValues(initItemFl)
               }
               ZenMessageToast.success()
            } else {
               setErr(zzControlHelper.getResponseError(res))
            }
         })
      } else {
         ZenMessageAlert.warning("Vui lòng chọn nhân viên")
      }
   }

   return <>
      {err && <Message list={err} negative header="Error" />}
      <Grid columns={2}>
         <Grid.Row>
            <Grid.Column>
               <ZenFormik form={"pmother"} ref={refFormikNs}
                  validation={formValidationNs}
                  initItem={initItemNs}
                  onSubmit={handleAdd}
               >
                  {
                     formik => {
                        return <Form>
                           <Form.Group>
                              <ZenFieldSelectApi
                                 width={12}
                                 lookup={ZenLookup.ID_NV}
                                 name="id_nv"
                                 formik={formik}
                                 label="pmdans.id_nv" defaultlabel="Thành viên"
                                 onItemSelected={handleChangeNs}
                              />
                              <Form.Field width={4}>
                                 <label>&nbsp;</label>
                                 <Button icon="level down"
                                    floated="right"
                                    primary content="Add"
                                    onClick={() => refFormikNs.current.handleSubmit()}
                                 />
                              </Form.Field>
                           </Form.Group>
                        </Form>
                     }}
               </ZenFormik>

               <div style={{ clear: "both" }} />

               {renderList(itemsNs)}
            </Grid.Column>
            <Grid.Column>
               <ZenFormik form={"pmother"} ref={refFormikFl}
                  validation={formValidationFl}
                  initItem={initItemFl}
                  onSubmit={handleAdd}
               >
                  {
                     formik => {
                        return <Form>
                           <Form.Group>
                              <ZenFieldSelectApi
                                 width={12}
                                 lookup={ZenLookup.ID_NV}
                                 name="id_nv"
                                 formik={formik}
                                 label="pmdans.id_nv" defaultlabel="Thành viên"
                                 onItemSelected={handleChangeFl}
                              />
                              <Form.Field width={4}>
                                 <label>&nbsp;</label>
                                 <Button icon="level down"
                                    floated="right"
                                    primary content="Add"
                                    onClick={() => refFormikFl.current.handleSubmit()}
                                 />
                              </Form.Field>
                           </Form.Group>
                        </Form>
                     }}
               </ZenFormik>

               <div style={{ clear: "both" }} />

               {renderList(itemsFl)}
            </Grid.Column>
         </Grid.Row>
      </Grid>
   </>
}

const TabFileAttachment = ({ data, keyData = "id", fileCodeName }) => {
   const { setLoadingForm } = useContext(ContextMenu)
   const refFileAttachment = useRef();

   useEffect(() => {
      setLoadingForm(false)
   }, [])

   return <>
      <ZenButton btnType="add" size="tiny" floated="right"
         onClick={() => refFileAttachment.current.openModal()}
      />
      <div style={{ clear: "both" }} />
      <FileAttachment codeName={fileCodeName}
         docKey={data[keyData]}
         ref={refFileAttachment}
         tableProps={{ className: "crm-sticky-table" }}
      />
   </>
}

const formValidationDA = [
   {
      id: "ten_du_an",
      validationType: "string",
      validations: [
         {
            type: "required",
            params: ["Không được bỏ trống trường này"]
         },
      ]
   },
   {
      id: "ma_nhda",
      validationType: "string",
      validations: [
         {
            type: "required",
            params: ["Không được bỏ trống trường này"]
         },
      ]
   },
   {
      id: "ma_gdoan_da",
      validationType: "string",
      validations: [
         {
            type: "required",
            params: ["Không được bỏ trống trường này"]
         },
      ]
   },
]

const menuBar = {
   DAInfo: { name: "DAInfo", content: "Thông tin", visible: true, default: true },
   HangMuc: { name: "hangmuc", content: "Hạng Mục", visible: true, },
   ThanhVien: { name: "thanhvien", content: "Thành Viên", visible: true, },
}

const styles = {
   hd_card: {
      borderLeft: "solid 1px rgba(34, 36, 38, 0.1)",
      borderRight: "solid 1px rgba(34, 36, 38, 0.1)",
      borderBottom: "solid 1px rgba(34, 36, 38, 0.1)",
   }
}

/** Ghi nhớ vị trí của tab */
const memoryStore = {
   _data: new Map(),
   get(key) {
      if (!key) {
         return null
      }

      return this._data.get(key) || null
   },
   set(key, data) {
      if (!key) {
         return
      }
      return this._data.set(key, data)
   }
}
