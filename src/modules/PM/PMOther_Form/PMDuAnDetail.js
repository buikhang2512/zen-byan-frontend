import React from "react";
import { Breadcrumb, Grid, Button, Icon } from "semantic-ui-react";
import { ContainerScroll, HeaderLink, Helmet, SegmentHeader, ZenLoading } from "../../../components/Control";
import { zzControlHelper } from "../../../components/Control/zzControlHelper";
import * as routes from "../../../constants/routes";
import { DuAnInfo, CKhs, CPhuTrachTheoDoi, CUserAccount } from "./LeftComponent";
import { ApiPMDuAn } from "../Api/index";
import { ZenHelper } from "../../../utils/global";
import { RightComponentMain } from "./RightComponent_main";
import { RightComponentWork } from "./RightComponent_work";
import { RightComponentDiscussions } from "./RightComponent_discussions";

export const ContextPMDuAnDetail = React.createContext();

class PMDuAnDetailComponent extends React.Component {
    constructor(props) {
        super()
        this._isMounted = true
        this.idElementContainer = "PMDuAn-container"
        this.maduan = zzControlHelper.atobUTF8(props.match.params.id)

        this.state = {
            error: null,
            loading: true,
            itemDuAn: null,
            activeContent: "info",
        }
    }

    componentDidMount() {
        ApiPMDuAn.getByCode(this.maduan, res => {
            if (this._isMounted) {
                if (res.status === 200)
                    var temp = { itemDuAn: res.data.data }
                else
                    temp = { error: ZenHelper.getResponseError(res) }

                this.setState({
                    loading: false,
                    ...temp
                })
            }
        })
    }

    componentWillUnmount() {
        this._isMounted = false
    }

    render() {
        const { error, itemDuAn, loading, activeContent } = this.state

        const handleChangeTab = (e, { name }) =>{
            if (name !== activeContent) {
               this.setState({ activeContent: name})
            }
         }

        return <ContextPMDuAnDetail.Provider value={{
            setitemDuAn: (newItem) => this.setState({ itemDuAn: newItem })
        }}>
            <div style={{ display: "flex", justifyContent:"space-between", marginBottom: "10px" }}>
                <PageHeader item={itemDuAn} />
                <div style={{ alignSelf: "center" }}>
                    <Button.Group size="tiny">
                        <Button icon primary={activeContent == "info" ? true : false} className={activeContent == "info" ? "" : "basic primary"}  name="info" onClick={handleChangeTab}>
                            <Icon name={"info circle"} />
                        </Button>
                        <Button icon primary={activeContent == "discussions" ? true : false} className={activeContent == "discussions" ? "" : "basic primary"}  name="discussions" onClick={handleChangeTab}>
                            <Icon name={"discussions"} />
                        </Button>
                        <Button icon primary={activeContent == "work" ? true : false} className={activeContent == "work" ? "" : "basic primary"} name="work" onClick={handleChangeTab}>
                            <Icon name={"sidebar"} />
                        </Button>
                    </Button.Group>
                </div>
            </div>
            <ZenLoading loading={loading} dimmerPage />

            <Grid columns={2} id={this.idElementContainer}>
                <Grid.Column width="6">
                    <ContainerScroll isSegment={false}
                        idElementContainer={this.idElementContainer}
                        id={"left-content"}
                    >
                        <DuAnInfo item={itemDuAn} loading={loading}
                            title={"Thông tin cơ bản"}
                            valueKey={this.maduan}
                            viewObject={[
                                { name: "{ma_du_an}", text: "Mã dự án" },
                                { name: "{ten_du_an}", text: "Tên dự án" },
                                { name: "{ten_gdoan_da}", text: "Giai đoạn" },
                                { name: "{ten_nhda}", text: "Nhóm dự án" },
                                { name: "{ngay_bd}", text: "Ngày bắt đầu", type: "date" },
                                { name: "{ngay_kt}", text: "Ngày kết thúc", type: "date" },
                            ]}
                        />

                        <CKhs
                            title={"Khách hàng"}
                            itemDuAn={itemDuAn}
                            valueKey={this.maduan}
                        />

                        {/* <CPhuTrachTheoDoi
                            title={"Phụ trách/theo dõi"}
                            itemDuAn={itemDuAn}
                            valueKey={this.maKH}
                        />

                        <CUserAccount
                            itemDuAn={itemDuAn}
                            valueKey={this.maKH}
                        /> */}
                    </ContainerScroll>
                </Grid.Column>

                <Grid.Column width="10">
                    { activeContent == "info" && <RightComponentMain
                        idElementContainer={this.idElementContainer}
                        itemDuAn={itemDuAn}
                        valueKey={this.maduan}
                        type={"MA_DU_AN"}
                        keyData="ma_du_an"
                    />}
                     { activeContent == "work" && <RightComponentWork
                        idElementContainer={this.idElementContainer}
                        itemDuAn={itemDuAn}
                        valueKey={this.maduan}
                        type={"MA_DU_AN"}
                        keyData="ma_du_an"
                    />}
                    {   activeContent == 'discussions' && <RightComponentDiscussions
                        idElementContainer={this.idElementContainer}
                        itemDuAn={itemDuAn}
                        valueKey={this.maduan}
                        type={"MA_DU_AN"}
                        keyData="ma_du_an"
                    />}
                </Grid.Column>
            </Grid>
        </ContextPMDuAnDetail.Provider>
    }
}

const PageHeader = ({ item }) => {
    return <div style={{}}>
        <Helmet idMessage={"PMDuAn.detail"}
            defaultMessage={"Dự án"} />

        <SegmentHeader>
            <HeaderLink listHeader={[{
                id: "PMDuAn",
                defaultMessage: "Dự án",
                route: routes.PMDuAn,
                active: false,
                isSetting: false,
            }]}>
                <Breadcrumb.Divider icon='right chevron' />
                <Breadcrumb.Section active>
                    {item?.ten_du_an || "Chi tiết"}
                </Breadcrumb.Section>
            </HeaderLink>
        </SegmentHeader>
        <br />
    </div>
}

export const PMDuAnDetail = {
    PMDuAn: {
        route: routes.PMDuAnDetail(),
        Zenform: PMDuAnDetailComponent,
        action: {
            view: { visible: true, permission: "", notPermission: true },
        },
    },
}