import React, { useEffect, useMemo, useState } from "react";
import {
   Icon,
   Label,
   Menu, Message, Segment
} from "semantic-ui-react";
import {
   ZenLoading, ContainerScroll, FormatDate, ZenSelectSearch
} from "../../../components/Control/index";
import { ApiPMDmDAHangMuc, ApiPMDuAn } from "../Api/index"
import * as routes from "../../../constants/routes"
import { useFetchData } from "../../../components/Control/zzCustomHook";
import { FormMode, zzControlHelper } from "../../../components/Control/zzControlHelper";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { getModal } from "../../ComponentInfo/Dictionary/ZenGetModal";
import { ModalCongViecDetail } from "../../AR/Dictionary/ARDmKhDetail.js/CongViecComponent";


const ContextMenu = React.createContext({})

export const RightComponentWork = ({ itemDuAn, keyData, type, idElementContainer, ...rest }) => {
   return <MenuBar idElementContainer={idElementContainer} typeForm={type}>
      {
         ({ activeMenu }) => {
            return <>
               <CongViec itemDuAn={itemDuAn} type={type} keyData={keyData} {...rest} />
            </>
         }
      }
   </MenuBar>
}

const MenuBar = ({ children, typeForm, idElementScroll = "content-right", idElementContainer }) => {
   // ************ MEMO
   const memoizeMenu = useMemo(() => getMenu(), [""])
   // ************ STATE
   const [loadingForm, setLoadingForm] = useState(false)
   const [activeMenu, setActiveMenu] = useState(memoizeMenu.defaultMenu)

   useEffect(() => {
      return () => {
         memoryStore.set(typeForm, null)
      }
   }, [])

   // ************ HANDLE
   const handleChangeTab = (e, { name }) => {
      if (name !== activeMenu) {
         setActiveMenu(name)
         setLoadingForm(true)
         memoryStore.set(typeForm, { activeMenu: name })
      }
   }

   // ************ FUNCTION
   function getMenu() {
      let defaultMenu;
      const menus = Object.keys(menuBar).map(menu => {
         const itemMenu = menuBar[menu]
         if (itemMenu.default) {
            // default tab theo khai báo
            defaultMenu = itemMenu.name
         }
         return itemMenu
      })
      // get activeMenu in memory
      const tempActiveMenu = memoryStore.get(typeForm)
      defaultMenu = tempActiveMenu && tempActiveMenu.activeMenu ? tempActiveMenu.activeMenu : defaultMenu
      return {
         menus: menus,
         defaultMenu: defaultMenu
      }
   }

   return <ContextMenu.Provider
      value={{
         setLoadingForm: (isLoad) => setLoadingForm(isLoad),
         loadingForm: loadingForm,
      }}
   >
      <Segment>
         {memoizeMenu.menus?.length > 0 && <Menu pointing secondary>
            {memoizeMenu.menus.map(menu => {
               const { visible, ...restMenu } = menu
               return visible ? <Menu.Item key={menu.name}
                  {...restMenu}
                  active={activeMenu === menu.name}
                  onClick={handleChangeTab}
               /> : undefined
            })}
         </Menu>}

         <ContainerScroll
            id={idElementScroll} isSegment={false}
            idElementContainer={idElementContainer}
            heightSub={28}
         >
            {children({ activeMenu: activeMenu })}
            <ZenLoading loading={loadingForm} inline="centered" />
         </ContainerScroll>
      </Segment>
   </ContextMenu.Provider>
}

const CongViec = ({ itemDuAn, valueKey, type, keyData }) => {
   const [modalInfoCV, setModalInfoCV] = useState(getModal(ZenLookup.WMCongViec));
   const [filter, setFilter] = useState({ ma_gdoan_cvs: [], id_nv: "", loading_gdoan: "" });
   const [detailCV, setDetailCV] = useState();
   const [expands, setExpands] = useState([]);

   const { data, loading, error, dispatch, fetchData, firstFetched } = useFetchData(loadData, false)

   useEffect(() => {
      fetchData()
   }, [filter.ma_gdoan_cvs, filter.id_nv])

   const handleExpands = (idHangMuc) => {
      if (expands.includes(idHangMuc)) {
         setExpands(expands.filter(id => id !== idHangMuc))
      } else {
         setExpands(expands.concat(idHangMuc))
      }
   }

   // ********************** HANDLE
   const handleOpenCloseModal = (e, formMode, hangMuc) => {
      e?.preventDefault()

      setModalInfoCV({
         ...modalInfoCV,
         info: {
            ...modalInfoCV.info,
            initItem: {
               ...modalInfoCV.info.initItem,
               [keyData]: valueKey,
               id_hang_muc: hangMuc?.id
            },
         },
         formMode: formMode,
         open: true,
         other: {
            type: type
         }
      })
   }

   const handleAfterSaveModal = (newItem, { mode }) => {
      // change
      if (mode === FormMode.ADD) {
         const listTemp = data?.listCongViec?.concat(newItem) || [newItem];
         dispatch({ type: 'FETCHED', payload: { listCongViec: listTemp } })
      } else if (mode === FormMode.EDIT) {
         const listTemp = data.listCongViec.map(t => t[keyData] === newItem[keyData] ? newItem : t);
         dispatch({ type: 'FETCHED', payload: { listCongViec: listTemp } })
      }
      // close modal
      setModalInfoCV({
         ...modalInfoCV,
         id: "",
         formMode: FormMode.NON,
         open: false,
      })
   }

   const handleChangeFilter = (e, { name, value, option, checked }) => {
      if (filter[name] === value) return
      if (name === "ma_gdoan_cvs") {
         if (checked) {
            setFilter({
               ...filter,
               ma_gdoan_cvs: filter?.ma_gdoan_cvs?.concat(option) || [option],
               loading_gdoan: option.ma_gdoan_cv
            })
         } else {
            setFilter({
               ...filter,
               ma_gdoan_cvs: filter?.ma_gdoan_cvs?.filter(t => t.ma_gdoan_cv != option.ma_gdoan_cv),
               loading_gdoan: option.ma_gdoan_cv
            })
         }
      } else if (name === "id_nv") {
         setFilter({
            ...filter,
            [name]: value
         })
      }
   }

   const handleRemoveFilter = (item) => {
      setFilter({
         ...filter,
         ma_gdoan_cvs: filter?.ma_gdoan_cvs?.filter(t => t.ma_gdoan_cv != item.ma_gdoan_cv)
      })
   }

   // ************************ CHI TIET CV
   const handleOpenDetailCV = (e, item) => {
      e.preventDefault();
      setDetailCV({ open: true, item: item })
   }

   const handleCloseCVDetail = (newItem) => {
      if (data?.listCongViec?.find(item => item.id == newItem.id)) {
         const listTemp = data.listCongViec.map(item => item.id == newItem.id ? newItem : item)
            .filter(item => item[keyData] === itemDuAn[keyData]);
         dispatch({ type: 'FETCHED', payload: { listCongViec: listTemp } })
      } else {
         const listTemp = data?.listCongViec?.concat(newItem) || [newItem];
         dispatch({ type: 'FETCHED', payload: { listCongViec: listTemp } })
      }
      setDetailCV()
   }

   function loadData() {
      const listCongViec = new Promise((resolve, reject) => {
         ApiPMDuAn.getCongViecByDuAn(valueKey, res => {
            if (res.status === 200) {
               // response 2 array: Danh sach cong việc, danh sách nhân sự
               const listNS = res.data.data[1]
               const result = res.data.data[0].map(item => {
                  const assignees = listNS.filter(t => item.id == t.id_cviec && t.loai == "1");
                  return { ...item, assignees: assignees }
               })

               resolve(result)
            } else {
               reject(res)
            }
         }, { id_nv: filter?.id_nv, str_ma_gdoan_cv: filter?.ma_gdoan_cvs?.map(t => t.ma_gdoan_cv)?.toString() })
      })

      const listHangMuc = new Promise((resolve) => {
         ApiPMDmDAHangMuc.get(res => {
            if (res.status === 200) {
               resolve(res.data.data)
            } else {
               resolve()
            }
         }, { qf: `ma_du_an = '${valueKey}'` })
      })

      if (firstFetched === true) {
         return { listCongViec: listCongViec }
      } else {
         return { listCongViec: listCongViec, listHangMuc: listHangMuc }
      }
   }

   const listCVEmpty = data?.listCongViec?.filter(item => !item.id_hang_muc)

   return <>
      <ZenLoading loading={loading} />
      {error && <Message negative list={error}
         onDismiss={() => dispatch({ type: 'FETCH_ERROR', payload: null })} />}

      <div style={{ textAlign: "right", padding: "0 0 14px 0" }}>
         <span>
            <ZenSelectSearch
               lookup={ZenLookup.ID_NV}
               name="id_nv"
               value={filter?.id_nv}
               onChange={handleChangeFilter}
               style={{ marginRight: "14px" }}
               direction="left"
               textEmpty="Tất cả thành viên"
            />
         </span>

         <span>
            Giai đoạn:
            <ZenSelectSearch floating
               lookup={ZenLookup.WMDmGiaiDoanCV}
               name="ma_gdoan_cvs"
               value={filter.ma_gdoan_cvs?.map(t => t.ma_gdoan_cv) || []}
               onChange={handleChangeFilter}
               multiple required
               direction="left"
               hiddenText timeOut={1}
               icon={{ name: "filter", color: "teal" }}
            />

            {filter?.ma_gdoan_cvs?.map(item => {
               return <Label key={item.ma_gdoan_cv} size="small" as='a' color="teal">
                  {item.ten_gdoan_cv}
                  <Icon name="delete" onClick={() => handleRemoveFilter(item)} />
                  {/* {filter?.loading_gdoan == item.ma_gdoan_cv ? <Icon name="spinner" loading />
                     : <Icon name="delete" onClick={(e,) => handleRemoveFilter(item)} />} */}
               </Label>
            })}
         </span>
      </div>

      <div>
         <div style={{
            fontSize: "1.2em",
            padding: "7px 0",
            margin: "7px 0",
            borderBottom: "2px solid rgba(34, 36, 38, 0.15)",
         }}>
            <span style={{ fontWeight: "bold" }}>
               Không thuộc hạng mục nào
            </span>

            <a href="#"
               style={{ paddingLeft: "14px" }}
               onClick={(e) => handleOpenCloseModal(e, FormMode.ADD)}
            >
               Thêm công việc
               <Icon name="plus" />
            </a>

            <span style={{ float: "right" }}>
               {listCVEmpty?.length > 0 && <Icon name={expands.includes("zzz") ? "plus" : "minus"}
                  link
                  onClick={() => handleExpands("zzz")}
               />}
            </span>
         </div>

         {!expands.includes("zzz") && <div>
            {
               listCVEmpty?.filter(item => !item.id_hang_muc)
                  .map(item => {
                     return <div key={item.id}
                        style={{
                           padding: "7px", margin: "0 14px",
                           borderBottom: "1px solid rgba(34, 36, 38, 0.1)",
                           display: "flex", columnGap: "14px"
                        }}
                     >
                        <div style={{ minWidth: "40%" }}>
                           <a href="#" onClick={(e) => handleOpenDetailCV(e, item)}>
                              {item.ten_cong_viec}
                           </a>
                        </div>

                        <div style={{ width: "100%", textAlign: "right" }}>
                           <span style={{ marginRight: "14px" }}>
                              <Icon name="users" />
                              {item.assignees?.map(t => t.ho_ten + ",")}
                           </span>

                           <span style={{ float: "right" }}>
                              <Icon name="clock outline" />
                              <FormatDate value={item.ngay_bd} /> - <FormatDate value={item.ngay_kt} />
                           </span>
                        </div>
                     </div>
                  })
            }
         </div>}
      </div>

      {
         data?.listHangMuc?.map(hangMuc => {
            const cvsRender = data?.listCongViec?.filter(item => item.id_hang_muc == hangMuc.id)
            return <div key={hangMuc.id}>
               <div style={{
                  fontSize: "1.2em",
                  padding: "7px 0",
                  margin: "7px 0",
                  borderBottom: "2px solid rgba(34, 36, 38, 0.15)",
               }}>
                  <span style={{ fontWeight: "bold" }}>
                     {hangMuc.ten_hang_muc}
                  </span>

                  <a href="#"
                     style={{ paddingLeft: "14px" }}
                     onClick={(e) => handleOpenCloseModal(e, FormMode.ADD, hangMuc)}
                  >
                     Thêm công việc
                     <Icon name="plus" />
                  </a>

                  <span style={{ float: "right" }}>
                     {cvsRender?.length > 0 && <Icon name={expands.includes(hangMuc.id) ? "plus" : "minus"}
                        link
                        onClick={() => handleExpands(hangMuc.id)}
                     />}
                  </span>
               </div>

               {!expands.includes(hangMuc.id) && <div>
                  {
                     cvsRender?.map(item => {
                        return <div key={item.id}
                           style={{
                              padding: "7px", margin: "0 14px",
                              borderBottom: "1px solid rgba(34, 36, 38, 0.1)",
                              display: "flex", columnGap: "14px"
                           }}
                        >
                           <div style={{ minWidth: "40%" }}>
                              <a href="#" onClick={(e) => handleOpenDetailCV(e, item)}
                                 style={item.color_gdoan_cv ? { color: item.color_gdoan_cv } : undefined}>
                                 {item.ten_cong_viec}
                              </a>
                           </div>

                           <div style={{ width: "100%", textAlign: "right" }}>
                              <span style={{ marginRight: "14px" }}>
                                 <Icon name="users" />
                                 {item.assignees?.map(t => t.ho_ten + ",")}
                              </span>

                              <span style={{ float: "right" }}>
                                 <Icon name="clock outline" />
                                 <FormatDate value={item.ngay_bd} /> - <FormatDate value={item.ngay_kt} />
                              </span>
                           </div>
                        </div>
                     })
                  }
               </div>}
            </div>
         })
      }

      {detailCV?.open && <ModalCongViecDetail
         open={detailCV.open}
         cvItem={detailCV.item}
         onClose={handleCloseCVDetail}
         modalInfoCV={modalInfoCV}
         keyData={keyData}
         valueKey={valueKey}
         type={type}
         fileCodeName={"CRM"}
      />}

      {zzControlHelper.openModalComponent(modalInfoCV,
         {
            onClose: (e) => handleOpenCloseModal(e, FormMode.NON),
            onAfterSave: handleAfterSaveModal,
         }
      )}
   </>
}

const menuBar = {}

/** Ghi nhớ vị trí của tab */
const memoryStore = {
   _data: new Map(),
   get(key) {
      if (!key) {
         return null
      }

      return this._data.get(key) || null
   },
   set(key, data) {
      if (!key) {
         return
      }
      return this._data.set(key, data)
   }
}
