import React, { useContext, useEffect, useMemo, useRef, useState } from "react";
import {
   Button,
   Card, Comment, Form,
   Menu, Message, Segment, TextArea, Modal, Table, Icon, Grid,
} from "semantic-ui-react";
import { ApiStored } from "../../../Api";
import { NoImage } from "../../../resources/index";
import {
   ZenMessageAlert, ZenButton, DiscussComment,
   FormatDate, ConfirmDelete, ZenMessageToast,
   ZenFieldSelectApi, ZenField, ZenFormik, ZenLoading, ContainerScroll,
   ButtonDelete, ButtonEdit, ZenFieldTextArea, ZenLink, ZenFieldDate,
} from "../../../components/Control/index";
import { FormMode, zzControlHelper } from "../../../components/Control/zzControlHelper";
import { ZenHelper } from "../../../utils";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { getModal } from "../../ComponentInfo/Dictionary/ZenGetModal";
//import { ApiCRMDeal, ApiCRMDiscuss, ApiCRMHoatDong, ApiCRMNote, ApiSOVchSO0 } from "../../../CRM/Api";
import { IntlFormat } from "../../../utils/intlFormat";
import { ValidError } from "../../../utils/language/variable";
// import { ApiArDmKh } from "../../Api";
import { ContextPMDuAnDetail } from "./PMDuAnDetail";
import { FormattedMessage } from "react-intl";
import { Link, useHistory, } from "react-router-dom";
import * as routes from "../../../constants/routes"
import { auth } from "../../../utils";
import _ from "lodash";
import { ApiPMDmDAHangMuc, ApiPMDuAn } from "../Api";

const ContextMenu = React.createContext({})

export const RightComponentDiscussions = ({ itemDuAn, keyData, type, idElementContainer, ...rest }) => {
   return <MenuBar idElementContainer={idElementContainer} typeForm={type}>
      {
         ({ activeMenu }) => {
            return <>
              {menuBar.ThaoLuan.visible && menuBar.ThaoLuan.name === activeMenu && <TabThaoLuan itemKH={itemDuAn} type={type} keyData={keyData} {...rest} />}
            </>
         }
      }
   </MenuBar>
}

const MenuBar = ({ children, typeForm, idElementScroll = "content-right", idElementContainer }) => {
   // ************ MEMO
   const memoizeMenu = useMemo(() => getMenu(), [""])
   // ************ STATE
   const [loadingForm, setLoadingForm] = useState(false)
   const [activeMenu, setActiveMenu] = useState(memoizeMenu.defaultMenu)

   useEffect(() => {
      return () => {
         memoryStore.set(typeForm, null)
      }
   }, [])

   // ************ HANDLE
   const handleChangeTab = (e, { name }) => {
      if (name !== activeMenu) {
         setActiveMenu(name)
         setLoadingForm(true)
         memoryStore.set(typeForm, { activeMenu: name })
      }
   }

   // ************ FUNCTION
   function getMenu() {
      let defaultMenu;
      const menus = Object.keys(menuBar).map(menu => {
         const itemMenu = menuBar[menu]
         if (itemMenu.default) {
            // default tab theo khai báo
            defaultMenu = itemMenu.name
         }
         return itemMenu
      })
      // get activeMenu in memory
      const tempActiveMenu = memoryStore.get(typeForm)
      defaultMenu = tempActiveMenu && tempActiveMenu.activeMenu ? tempActiveMenu.activeMenu : defaultMenu
      return {
         menus: menus,
         defaultMenu: defaultMenu
      }
   }

   return <ContextMenu.Provider
      value={{
         setLoadingForm: (isLoad) => setLoadingForm(isLoad),
         loadingForm: loadingForm,
      }}
   >
      <Segment>
         <Menu pointing secondary>
            {memoizeMenu.menus.map(menu => {
               const { visible, ...restMenu } = menu
               return visible ? <Menu.Item key={menu.name}
                  {...restMenu}
                  active={activeMenu === menu.name}
                  onClick={handleChangeTab}
               /> : undefined
            })}
         </Menu>

         <ContainerScroll
            id={idElementScroll} isSegment={false}
            idElementContainer={idElementContainer}
            heightSub={28}
         >
            {children({ activeMenu: activeMenu })}
            <ZenLoading loading={loadingForm} inline="centered" />
         </ContainerScroll>
      </Segment>
   </ContextMenu.Provider>
}

const TabThaoLuan = (props) => {
    const { type, valueKey } = props
    const { setLoadingForm, loadingForm } = useContext(ContextMenu);

    useEffect(() => {
       setLoadingForm(false)
    }, [])
    return <DiscussComment codeName="CRM" type={type} keyValue={valueKey} />
 }

const menuBar = {
    ThaoLuan: { name: "thaoluan", content: "Thảo luận", visible: true, default: true},
}

const styles = {
   hd_card: {
      borderLeft: "solid 1px rgba(34, 36, 38, 0.1)",
      borderRight: "solid 1px rgba(34, 36, 38, 0.1)",
      borderBottom: "solid 1px rgba(34, 36, 38, 0.1)",
   }
}

/** Ghi nhớ vị trí của tab */
const memoryStore = {
   _data: new Map(),
   get(key) {
      if (!key) {
         return null
      }

      return this._data.get(key) || null
   },
   set(key, data) {
      if (!key) {
         return
      }
      return this._data.set(key, data)
   }
}
