import React, { useContext, useEffect, useLayoutEffect, useMemo, useRef, useState } from "react";
import { useIntl } from "react-intl";
import {
    Button, Form, Icon,
    Message, Modal, Segment, Table
} from "semantic-ui-react";
import {
    FormatDate,
    ZenButton,
    ZenField,
    ZenFieldSelectApi, ZenFieldSelectApiMulti,
    ZenFormik,
    ZenLink,
    ZenLoading, ZenMessageAlert, ZenMessageToast, ZenModal
} from "../../../components/Control";
import { FormMode, zzControlHelper } from "../../../components/Control/zzControlHelper";
// import { ApiArDmKh } from "../../Api";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import UserFormAdd from "../../User/UserFormAdd"
import { ContextPMDuAnDetail } from "./PMDuAnDetail";
import { getModal } from "../../ComponentInfo/Dictionary/ZenGetModal";
import { ApiPMDuAn } from "../Api";
import * as routes from '../../../constants/routes'
import _ from "lodash";
//import { ApiCRMitemDuAnt } from "../../../CRM/Api";

const extractStr = zzControlHelper.extractString(["{", "}"]);

export const DuAnInfo = ({ title, loading, item, viewObject, onSetItem }) => {
    // ************ STATE
    const intl = useIntl()
    const [collapse, setCollapse] = useState(false)
    const [loadingSave, setLoadingSave] = useState("")

    // ************ MEMO
    const memoizedView = useMemo(() => getBasicInfo(), [item]);

    // ************ HANDLE
    const onItemLookup = (option, { api, fieldUpdate, lookup, renderView, arrName }) => {
        if (option[lookup.value] == item[fieldUpdate]) {
            return
        }

        setLoadingSave(fieldUpdate)
        // PATCH
        const patchData = [
            {
                value: option[lookup.value],
                path: `/${fieldUpdate}`,
                op: "replace",
                operationType: 0,
            }
        ]

        api["updatePatch"](item.id, patchData, res => {
            if (res.status >= 200 && res.status <= 204) {
                const newItem = {}, valueRender = renderView.split(",")

                arrName.forEach((itemField, index) => {
                    newItem[itemField] = option[valueRender[index]]
                });
                onSetItem(newItem)
                setLoadingSave()
                ZenMessageToast.success();
            } else {
                setLoadingSave()
                ZenMessageAlert.error(ZenHelper.getResponseError(res))
            }
        })
    };

    // ************ FUNCTION
    function getBasicInfo() {
        if (!item) return []

        return viewObject.map(({ name, text, ...rest }) => {
            const arrName = extractStr(name);
            let value = name;

            for (let index = 0; index < arrName.length; index++) {
                const field = arrName[index];
                let valueFormat = item[field]

                if (rest.type === "date") {
                    valueFormat = item[field] && new Date(item[field]).getFullYear() > 1900 ? intl.formatDate(item[field], {
                        year: 'numeric',
                        month: 'numeric',
                        day: 'numeric',
                    }) : ""
                } else if (rest.type === "number") {
                    valueFormat = item[field] ? intl.formatNumber(item[field], {}) : ""
                }

                value = value.replace(`{${field}}`, valueFormat || "")
            }

            return {
                text: text,
                value: value,
                arrName: arrName,
                ...rest
            }
        })
    }

    function tableUI() {
        return <Table basic='very'>
            <Table.Body>
                {
                    memoizedView.map((t, index) => {
                        const isLoadingBtn = t.edit ? loadingSave === t.edit.fieldUpdate : false
                        let viewValue = t.value
                        // route
                        if (t.route) {
                            viewValue = <ZenLink to={t.route(zzControlHelper.btoaUTF8(item.ma_kh))}>
                                {t.value}
                            </ZenLink>
                        }
                        return <Table.Row key={index}>
                            <Table.Cell textAlign="right" collapsing>{t.text}</Table.Cell>
                            <Table.Cell style={{ fontWeight: "bold", whiteSpace: t.type == "textarea" ? "pre-wrap" : "" }}>
                                {viewValue}
                            </Table.Cell>
                            <Table.Cell textAlign="right" collapsing>
                                {t.edit && t.edit.lookup && <>
                                    <ZenModalLookup trigger={<Icon link
                                        name={isLoadingBtn ? "spinner" : "pencil"}
                                        loading={isLoadingBtn ? true : false} />}

                                        lookup={t.edit.lookup}
                                        value={item[t.edit.fieldUpdate]}
                                        onItemLookup={(option) => onItemLookup(option, { ...t.edit, arrName: t.arrName })}
                                    />
                                </>}
                            </Table.Cell>
                        </Table.Row>
                    })
                }
            </Table.Body>
        </Table>
    }

    return <Segment loading={loading}>
        {title && <div className={"ar-lf-header"}>
            {title}
            <Icon name={collapse ? "plus" : "minus"}
                style={{ float: "right" }} link
                onClick={() => setCollapse(!collapse)}
            />
        </div>}
        {!collapse && tableUI()}
    </Segment>
}

export const CPhuTrachTheoDoi = (props) => {
    const { title, itemDuAn } = props
    const { setitemDuAn } = useContext(ContextPMDuAnDetail)

    // *************** STATE
    const _isMounted = useRef(true)
    const _Contructor = useRef(true)
    const [followList, setFollowList] = useState();
    const [error, setError] = useState();
    const [loading, setLoading] = useState(false);
    const [stateModal, setStateModal] = useState({ open: false });

    useEffect(() => {
        if (_Contructor.current === true && itemDuAn) {
            setLoading(true)
            let strSql = zzControlHelper.replaceAll(itemDuAn.id_nv_follow, ",", `','`);
            ApiHrHsNs.get(res => {
                if (res.status === 200) {
                    setFollowList(res.data.data)
                } else {
                    setError(zzControlHelper.getResponseError(res))
                }
                setLoading(false)
            }, {
                qf: `id_nv IN ('${strSql}')`
            })
            _Contructor.current = false;
        }

        return () => {
            _isMounted.current = false
        }
    }, [itemDuAn])

    const handleOpenModal = () => {
        setStateModal({ ...stateModal, open: !stateModal.open })
    }

    const handleSaveModal = (newitemDuAn) => {
        const id_nv_followStr = newitemDuAn.arrIdNvFollow?.toString() || ""
        const patchData = [
            {
                value: newitemDuAn.ma_nvkd,
                path: `/ma_nvkd`,
                op: "replace",
                operationType: 0,
            },
            {
                value: id_nv_followStr,
                path: `/id_nv_follow`,
                op: "replace",
                operationType: 0,
            }
        ]

        ApiArDmKh.updatePatch(newitemDuAn.ma_kh, patchData, res => {
            if (res.status >= 200 && res.status <= 204) {
                setitemDuAn({ ...newitemDuAn, id_nv_follow: id_nv_followStr })
                setFollowList(newitemDuAn.fllowList)
                setStateModal({ open: false })
                ZenMessageToast.success();
            } else {
                ZenMessageAlert.error(zzControlHelper.getResponseError(res))
            }
        })
    }

    return <>
        <Segment>
            <div className={"ar-lf-header"}>
                {title + ` (${followList?.length || 0})`}

                <div style={{ float: "right" }}>
                    <a style={{ cursor: "pointer" }} onClick={handleOpenModal}>
                        <Icon name="edit" />
                        Sửa thông tin
                    </a>
                </div>
            </div>

            <ZenLoading loading={loading} inline="centered" />
            {error && <Message negative list={error} />}

            {itemDuAn && !loading && <>
                <p>Phụ trách chính: <span style={{ fontWeight: "bold" }}>{itemDuAn.ten_nvkd}</span></p>
                <p>Theo dõi:</p>
                <ul>
                    {followList && followList.length > 0 &&
                        followList.map(item => {
                            return <li key={item.id_nv}>{item.ho_ten}</li>
                        })}
                </ul>
            </>}
        </Segment>

        {stateModal.open && <ModalPhuTrachTheoDoi
            itemDuAn={itemDuAn}
            stateModal={stateModal}
            onOpenModal={handleOpenModal}
            onSaveModal={handleSaveModal}
        />}
    </>
}

export const CUserAccount = (props) => {
    const { itemDuAn } = props
    const { setitemDuAn } = useContext(ContextARDmKhDetail)
    const [stateModal, setStateModal] = useState({ open: false });

    const handleOpenModal = () => {
        setStateModal({ ...stateModal, open: !stateModal.open })
    }

    const handleAfterSaveModal = (newUser) => {
        const patchData = [
            {
                value: newUser.email,
                path: `/user_account`,
                op: "replace",
                operationType: 0,
            },
        ]

        ApiArDmKh.updatePatch(itemDuAn.ma_kh, patchData, res => {
            if (res.status >= 200 && res.status <= 204) {
                setitemDuAn({ user_account: newUser.email })
                setStateModal({ open: false })
            } else {
                ZenMessageAlert.error(zzControlHelper.getResponseError(res))
            }
        })
    }

    function noAccountUI() {
        return <>
            <p>
                Khách hàng này chưa được cấp tài khoản đăng nhập
            </p>
            <Button
                primary fluid
                size="small"
                content="Tạo tài khoản cho khách hàng"
                icon="user plus"
                onClick={handleOpenModal}
            />
        </>
    }

    function hasAccountUI() {
        return <>
            <p>
                Tài khoản đăng nhập của khách hàng là:
            </p>
            <p style={{ fontWeight: "bold" }}>
                {itemDuAn.user_account}
            </p>
        </>
    }

    function renderUI() {
        return <>
            {itemDuAn.user_account ? hasAccountUI() : noAccountUI()}

            {stateModal.open && <UserFormAdd
                id={itemDuAn.id}
                initItem={{
                    email: itemDuAn.email,
                    full_name: itemDuAn.ten_kh
                }}
                header={"Tạo tài khoản truy cập"}
                open={stateModal.open}
                onAfterSave={handleAfterSaveModal}
                onClose={handleOpenModal}

                createFromDetail={true}
            />}
        </>
    }

    return <Segment>
        <div className={"ar-lf-header"}>
            {"Tài khoản đăng nhập"}
        </div>
        {itemDuAn && renderUI()}
    </Segment>
}

export const CKhs = (props) => {
    const { itemDuAn, valueKey, title } = props
    const [itemKhs, setItemKhs] = useState();
    const [error, setError] = useState();
    const [loading, setLoading] = useState(true);
    const [collapse, setCollapse] = useState(false)
    const [stateModal, setStateModal] = useState({ open: false });

    useEffect(() => {
        ApiPMDuAn.getKHByCode(valueKey, res => {
            if (res.status === 200) {
                setItemKhs(res.data.data)
            } else {
                setError(zzControlHelper.getResponseError(res))
            }
            loading && setLoading(false)
        })
    }, [])

    // const modal = useMemo(() => {
    //     return getModal(ZenLookup.CrmContact)
    // }, [])

    const handleOpenModal = (mode, item) => {
        if (mode === FormMode.DEL) {
            ZenMessageAlert.question("Bạn có chắc muốn xóa không?")
                .then(success => {
                    if (success == 1) {
                        removeKhs(item)
                    }
                })
        } else {
            setStateModal({ ...stateModal, open: !stateModal.open })
        }
    }

    const handleSaveModal = (newItem) => {

        setItemKhs(itemKhs?.concat(newItem))

        setStateModal({ open: !stateModal.open, formMode: FormMode.NON })
    }

    function removeKhs(item) {
        ApiPMDuAn.deleteKH(valueKey, item.ma_kh, res => {
            if (res.status === 204) {
                setItemKhs(itemKhs.filter(t => t.ma_kh != item.ma_kh))
                ZenMessageToast.success();
            } else {
                ZenMessageAlert.error(zzControlHelper.getResponseError(res))
            }
        })
    }

    function listDataUI() {
        return itemKhs?.map((obj, index) => {
            return <React.Fragment key={obj.id || `c${index}`}>
                <div className="ar-lf-row ar-select-hover">
                    <div style={{ width: "100%" }}>
                        <div style={{ marginBottom: "7px" }}>
                            <a href={`#${routes.ARDmKhDetail(zzControlHelper.btoaUTF8(obj.ma_kh))}`}>
                                <Icon name="building outline" />
                                <strong>{`${obj.ten_kh}`}</strong>
                            </a>
                        </div>
                        <div style={{ display: "flex" }}>
                            <div style={{paddingLeft: "20px"}}>
                                {/* <Icon name="mail outline" /> */}
                                {obj.vai_tro || "..."}
                            </div>
                        </div>
                    </div>
                    <div className="sub-hover">
                        {/* <Icon name="edit" link onClick={() => handleOpenModal(FormMode.EDIT, obj)} style={{ marginBottom: "7px" }} /> */}
                        <Icon name="trash" link onClick={() => handleOpenModal(FormMode.DEL, obj)} />
                    </div>
                </div>
                <div className="ar-hr" />
            </React.Fragment>
        })
    }

    return <>
        <Segment>
        {title && <div className={"ar-lf-header"}>
            {title}
            <Icon name={collapse ? "plus" : "minus"}
                style={{ float: "right" }} link
                onClick={() => setCollapse(!collapse)}
            />
        </div>}

            <ZenLoading loading={loading} inline="centered" />
            {error && <Message negative list={error} />}

            {!collapse && listDataUI()}
            <div style={{ display: "flex", justifyContent: "center", padding: "15px 0px 0px" }}>
                <a style={{ cursor: "pointer" }} onClick={() => handleOpenModal(FormMode.ADD)} >
                    <Icon name="attach" />
                    Gán khách hàng
                </a>
            </div>
        </Segment>

        {stateModal.open && <ModalKhs
            itemDuAn={itemDuAn}
            maduan={valueKey}
            stateModal={stateModal}
            onOpenModal={handleOpenModal}
            onSaveModal={handleSaveModal}
        />}
    </>
}

const ModalKhs = (props) => {
    const { itemDuAn, maduan, stateModal, onOpenModal, onSaveModal } = props;
    const [loading, setLoading] = useState(false)
    const [error, setError] = useState();
    const refFormik = useRef();
    const initItem = {
        ma_du_an: maduan,
        ma_kh: "",
        ten_kh: "",
        vai_tro:"",
        ksd: false,
    }
    const formValidation = []
    useEffect(() => {
        // setTimeout(() => {
        //     setLoading(false)
        // }, 300);
        // setData({})
    }, [])

    const handleChange = (option, {name}) => {
        if (name === "ma_kh") {
            refFormik.current.setFieldValue('ten_kh',option?.ten_kh)
        }
    }
    const handleSubmit = (params,formId) => {
        ApiPMDuAn.insertKH(maduan,params, res => {
            if(res.status === 204) {
                onSaveModal(params)
                ZenMessageToast.success()
            } else {
                ZenMessageAlert.warning(zzControlHelper.getResponseError(res))
                setError(zzControlHelper.getResponseError(res))
            }
        })
    }

    return <>
        <ZenModal
            open={stateModal.open}
            onClose={onOpenModal}
            size="tiny"
            header="Thêm khách hàng"

            actions={
                <ZenButton btnType="save"
                    onClick={(e) => refFormik.current.handleSubmit(e)}
                    size="small"
                />
            }
            scrolling={false}
        >
            <Modal.Content>
                <ZenFormik form={"pmother"} ref={refFormik}
                    validation={formValidation}
                    initItem={initItem}
                    onSubmit={handleSubmit}
                    //onValidate={onValidate}
                >
                    {
                        formik => {
                            return <Form>
                                <ZenFieldSelectApi
                                    //loadApi
                                    lookup={ZenLookup.Ma_kh}
                                    formik={formik}
                                    name="ma_kh" 
                                    label={"ardmkh.ma_kh"} defaultlabel="Khách hàng"
                                    onItemSelected={handleChange}
                                />
                                <ZenField
                                    name="vai_tro"
                                    label={"ardmkh.vai_tro"} defaultlabel="Vai trò"
                                    props={formik}
                                />
                            </Form>
                        }
                    }
                </ZenFormik>

                <ZenLoading loading={loading} />

            </Modal.Content>
        </ZenModal>
    </>
}

const ModalPhuTrachTheoDoi = (props) => {
    const { itemDuAn, stateModal, onOpenModal, onSaveModal } = props;
    const [data, setData] = useState({ ma_nvkd: "", arrIdNvFollow: [] })
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        setTimeout(() => {
            setLoading(false)
        }, 300);
        setData({ ...itemDuAn, arrIdNvFollow: itemDuAn?.id_nv_follow ? itemDuAn?.id_nv_follow.split(",") : [] })
    }, [])

    const handleChange = (e, { name, value, option, options }) => {
        if (name === "ma_nvkd") {
            setData({ ...data, [name]: value, ten_nvkd: option?.ho_ten || "" })
        } else if (name === "arrIdNvFollow") {
            const arraySelected = value.map(id_nv => {
                return options.find(t => t.id_nv == id_nv)
            })
            setData({ ...data, [name]: value, fllowList: arraySelected })
        }
    }

    return <>
        <ZenModal
            open={stateModal.open}
            onClose={onOpenModal}
            size="tiny"
            header="Sửa người phụ trách"

            actions={
                <ZenButton btnType="save"
                    onClick={() => onSaveModal(data)}
                    size="small"
                />
            }
            scrolling={false}
        >
            <Modal.Content>
                <Form>
                    <ZenFieldSelectApi
                        //loadApi
                        lookup={ZenLookup.ID_NV}
                        name="ma_nvkd" value={data.ma_nvkd}
                        label={"ardmkh.nguoi_phu_trach"} defaultlabel="Người phụ trách chính"
                        onChange={handleChange}
                    />

                    <ZenFieldSelectApiMulti
                        search={true}
                        lookup={ZenLookup.ID_NV}
                        name="arrIdNvFollow" value={data.arrIdNvFollow || []}
                        label={"ardmkh.nv_follow"} defaultlabel="Người theo dõi"
                        onChange={handleChange}
                    />
                </Form>

                <ZenLoading loading={loading} />

            </Modal.Content>
        </ZenModal>
    </>
}
