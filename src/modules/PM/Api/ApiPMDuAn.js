import axios from '../../../Api/axios';

const ExtName = "PMDmDA"

export const ApiPMDuAn = {
   get(callback, filter = {}, pagination = {}) {
      axios.get(`${ExtName}`, {
         params: {
            qf: filter.qf,
            keyword: filter.keyword,
            sort: filter.sort,
            page: pagination.page,
            pageSize: pagination.pageSize
         },
      })
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   getByCode(code, callback) {
      axios.get(`${ExtName}/` + code)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   insert(data, callback) {
      axios.post(`${ExtName}`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   update(data, callback) {
      axios.put(`${ExtName}`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   updatePatch(code, data, callback) {
      axios.patch(`${ExtName}/${code}`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   delete(code, callback) {
      axios.delete(`${ExtName}/` + code)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   getKHByCode(code, callback) {
      axios.get(`${ExtName}/${code}/kh `)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   insertKH(code, data, callback) {
      axios.post(`${ExtName}/${code}/kh`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   deleteKH(code, makh, callback) {
      axios.delete(`${ExtName}/${code}/kh/${makh}`)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   getNSByCode(code, type, callback) {
      axios.get(`${ExtName}/${code}/ns?loai=${type}`)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   insertNS(code, data, callback) {
      axios.post(`${ExtName}/${code}/ns`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   deleteNS(code, idnv, type, callback) {
      axios.delete(`${ExtName}/${code}/ns/${idnv}?loai=${type}`)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   getCongViecByDuAn(code, callback, filter = {}) {
      // id_nv: lọc theo Người thực hiện, hrhsns.loai = 1
      // str_ma_gdoan_cv: chuỗi giai đoạn: gd1,gd2,....
      axios.get(`${ExtName}/${code}/congviec`, {
         params: {
            id_nv: filter.id_nv,
            str_ma_gdoan_cv: filter.str_ma_gdoan_cv
         }
      })
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },
}