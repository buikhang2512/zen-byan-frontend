import { IntlFormat, LanguageFormat } from "../../../utils/intlFormat";
import { Mess_Dashboard } from "./variable";

const DashBoard_VI = {
   ...IntlFormat.setMessageLanguage(Mess_Dashboard),
}
const DashBoard_EN = {
   ...IntlFormat.setMessageLanguage(Mess_Dashboard, LanguageFormat.en),
}

export { DashBoard_VI, DashBoard_EN }