import { BienDongSoDu } from "../DashLet/BienDongSoDu";
import { DoanhThuChiPhi } from "../DashLet/DoanhThuChiPhi";
import { DongTien } from "../DashLet/DongTien";
import { LichSuDoanhThu } from "../DashLet/LichSuDoanhThu";
import { PhanTichChiPhi } from "../DashLet/PhanTichChiPhi";
import { TongHopYeuCau } from "../DashLet/TongHopYeuCau";
import { TopCongNoPhaiThu } from "../DashLet/TopCongNoPhaiThu";
import { TopCongNoPhaiTra } from "../DashLet/TopCongNoPhaiTra";
import { TopNVKD } from "../DashLet/TopNVKD";
import { ViecChuaHoanThanh } from "../DashLet/ViecChuaHoanThanh";
import { NewFeeds } from "../DashLet/Newfeeds";

const dashletInfo = {
   //functionId: "cong_no_phai_thu",
   "BDSD": BienDongSoDu,
   "doanh_thu": DoanhThuChiPhi,
   "DongTien": DongTien,
   "lic_su_doanh_thu": LichSuDoanhThu,
   "chi_phi_theo_km": PhanTichChiPhi,
   "cong_no_phai_thu": TopCongNoPhaiThu,
   "TopCongNoPhaiTra": TopCongNoPhaiTra,
   "top_nvkd": TopNVKD,
   "tong_hop_yeu_cau": TongHopYeuCau,
   "viec_chua_hoan_thanh": ViecChuaHoanThanh,
   "new_feeds": NewFeeds
}

const dashletHardcode = [
   
   {
      i: "BDSD",
      x: 0,
      y: 0,
      w: 8,
      h: 10,
      visible: true,
      component: BienDongSoDu,
   },
   {
      i: "PTCP",
      x: 0,
      y: 8,
      w: 8,
      h: 10,
      visible: true,
      component: PhanTichChiPhi,
   },
   {
      i: "DongTien",
      x: 8,
      y: 0,
      w: 8,
      h: 8,
      visible: true,
      component: DongTien,
   },
   {
      i: "LichSuDoanhThu",
      x: 8,
      y: 8,
      w: 8,
      h: 8,
      visible: true,
      component: LichSuDoanhThu,
   },
   {
      i: "TopCongNoPhaiThu",
      x: 0,
      y: 16,
      w: 8,
      h: 8,
      visible: true,
      component: TopCongNoPhaiThu,
   },
   {
      i: "TopCongNoPhaiTra",
      x: 8,
      y: 16,
      w: 8,
      h: 8,
      visible: true,
      component: TopCongNoPhaiTra,
   },
   {
      i: "DoanhThuChiPhi",
      x: 0,
      y: 24,
      w: 8,
      h: 8,
      visible: true,
      component: DoanhThuChiPhi,
   },
];

export { dashletInfo , dashletHardcode }

// khai báo thông tin dashlet: default số cột = 16, height mỗi dòng = 30px
// x: cột, y: dòng, w: chiều rộng(số cột), h chiều cao(số dòng), i: key
// position, size lưu trong localstorage
//
// const DashboardInfo = [
//   {
//     i: "3",
//     x: 12,
//     y: 0,
//     w: 4,
//     h: 4,
//     minH: 2,
//     minW: 4,
//     maxH: 6,
//     maxW: 6
//     visible: true,
//     component: HrCheckIn,
//   },
// .......
// ];
