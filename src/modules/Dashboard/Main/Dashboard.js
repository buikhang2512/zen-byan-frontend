import { isBuffer } from "lodash-es";
import React, { useCallback, useEffect, useReducer, useRef, useState } from "react";
import { Button, Menu, Sidebar, Segment, Icon, Header, Modal, Form } from "semantic-ui-react";
import { ApiDashboard, ApiDashLet, ApiRole, ApiRoleDashlet, ApiStored } from "../../../Api";
import { ZenFormik, ZenGridLayout, ZenButton, ZenField, ZenFieldTextArea, ZenMessageToast, ZenMessageAlert } from "../../../components/Control";
import { zzControlHelper } from "../../../components/Control/zzControlHelper";
import { auth, ZenHelper } from "../../../utils";
import { dashletHardcode, dashletInfo } from "./DashboardInfo";
import './Dashboardnav.css'
import { GlobalStorage, KeyStorage } from "../../../utils/storage";
import _ from "lodash";

export const Dashboard = (props) => {
   const _isMounted = useRef(true);
   const refZGL = useRef();
   const [dashboard, setDashboard] = useState()
   const [availablelets, setAvailablelets] = useState([])
   const [availableletsOrdinal, setAvailableletsOrdinal] = useState([])
   const [layoutDashboard, setLayoutDashBoard] = useState({})
   const [forceRerender, forceUpdate] = useReducer((x) => x + 1, 0);

   useEffect(() => {
      const result = [{}]
      ApiDashboard.getByUser(res => {
         if (!_isMounted.current) return
         if (res.status === 200 && res.data.data.length > 0) {
            setDashboard(res.data.data[0])
            const dashlets = res.data.data[0].lets
            dashlets.forEach(t => {
               if (dashletInfo[t.dashlet_id]) {
                  result.push({
                     ...t,
                     x: t.x, y: t.y,
                     w: t.w, h: t.h,
                     component: dashletInfo[t.dashlet_id],
                     visible: true,
                     i: t.dashlet_id
                  })
               }
            })
            ApiDashboard.getDasletByUser(res => {
               if (res.status === 200) {
                  setAvailableletsOrdinal(res.data.data)
                  let _lets = res.data.data
                  dashlets.find(item => {
                     _lets = removeItem(_lets, item.dashlet_id)
                  })
                  console.log(_lets)
                  setAvailablelets(_lets)
               }
            })
         }
         setLayoutDashBoard({ lg: result })
      })

      return () => {
         _isMounted.current = false
      }
   }, [])

   function removeItem(arr, num) {
      return arr.filter(el => el.id !== num);
   }

   const handleChangeMode = ({ name }) => {
      if (name === btnName.Save) {
         // lưu vào local storge
         refZGL.current.saveToLocalStorage();
         ApiDashboard.insert(dashboard, res => {
            if (res.status === 200) {
               ZenMessageToast.success()
            }
         })

      } else if (name === btnName.Cancel) {
         refZGL.current.onResetLayout();
      }
   }

   const handleAfterSave = (item) => {
      setDashboard(item)
   }

   const handleAddDashlet = (item) => {
      let checkhaveitem = dashboard.lets.filter(t => t.dashlet_id == item.id)
      if (checkhaveitem.length > 0) {
         ZenMessageAlert.warning("Dashlet này đã có trong dashboard")
      } else {
         if (dashletInfo[item.id]) {
            dashboard.lets.push({
               dashlet_id: item.id,
               title: item.ten_dashlet,
               dashboard_id: dashboard.id,
               visible: true,
               x: 0,
               y: 0,
               w: 8,
               h: 10,
            })

            if (dashletInfo[item.id]) {
               layoutDashboard.lg.push({
                  x: 0, y: 0,
                  w: 8, h: 10,
                  component: dashletInfo[item.id],
                  visible: true,
                  i: item.id
               })
            }
            setAvailablelets(availablelets.filter(t => t.id !== item.id))
            setLayoutDashBoard({ lg: [].concat(layoutDashboard.lg) },)
            refZGL.current.onLayoutChange(null, layoutDashboard)

         } else {
            ZenMessageAlert.warning("Dashlet này chưa được khai báo")
         }
         setDashboard(dashboard)
      }
      forceUpdate()
   }

   const callbackFunction = (id) => {
      const newLets_Dashboarddashboard = dashboard.lets.filter(t => t.dashlet_id !== id)
      const newlayoutDashboard = layoutDashboard.lg.filter(t => t.i !== id)
      dashboard.lets = newLets_Dashboarddashboard
      setLayoutDashBoard({ lg: [].concat(newlayoutDashboard) },)
      setDashboard(dashboard)
      let availablelet = availableletsOrdinal.filter(t => t.id == id)
      setAvailablelets(availablelets.concat(availablelet))
   }


   return layoutDashboard.lg?.length > 0 ? <>
      <DropDownSetting
         onChangeMode={handleChangeMode}
         dashboard={dashboard}
         availablelets={availablelets}
         handleAfterSave={handleAfterSave}
         handleAddDashlet={handleAddDashlet}
         layoutDashboard={layoutDashboard}
      >
         {
            ({ isView }) => <ZenGridLayout ref={refZGL}
               keyLS="home_dashboard"
               useLocalStorage={true}
               isView={isView}
               initLayout={layoutDashboard}
               parentCallback={callbackFunction}
            />
         }
      </DropDownSetting>
   </> : <></>
};


const DropDownSetting = ({ children, onChangeMode, dashboard, handleAfterSave, availablelets, handleAddDashlet, layoutDashboard }) => {
   const [isView, setIsView] = useState(true);
   const [visible, setVisible] = useState(false)
   const [openDashboardInfo, setOpenDashboardInfo] = useState(false)
   const handleSetting = (e, { name }) => {
      setIsView(!isView)
      onChangeMode({ name: name })
   }

   // useEffect(() => {

   // },[])

   const handleShowMenu = () => {
      const $ = document.querySelector.bind(document)
      const $$ = document.querySelectorAll.bind(document)
      var x = document.getElementById('dshbrd-menu-hd')
      var y = document.getElementById('dshbrd-nav-overlay')
      x.style.right = '0%'
      y.style.right = '0%'
   }
   const handleHiddenMenu = () => {
      var x = document.getElementById('dshbrd-menu-hd')
      var y = document.getElementById('dshbrd-nav-overlay')
      x.style.right = '-100%'
      y.style.right = '-100%'
   }
   return <>
      <div style={{ padding: "0 10px 14px 10px", display: 'flex', justifyContent: 'space-between' }}>
         <div style={{ display: "flex" }}>
            <span style={styles.fontMenu}>{dashboard?.title}</span>
            {!isView && <div className="dshbrd-edit-info">
               <Icon size="large" name="pencil" onClick={(e) => setOpenDashboardInfo(true)} />
            </div>}
         </div>
         <div>
            {!isView && <Button size="mini" icon="plus"
               primary type="button"
               onClick={(e) => handleShowMenu()} name={btnName.Add}
            />
            }

            <Button.Group size="mini" floated="right">
               {isView && <Button icon="setting" basic
                  primary type="button"
                  onClick={handleSetting} name={btnName.Edit}
               />}
               {!isView && <>
                  <Button icon="check"
                     primary type="button"
                     onClick={handleSetting} name={btnName.Save}
                  />
                  <Button icon="close" basic
                     primary type="button"
                     onClick={handleSetting} name={btnName.Cancel}
                  />
               </>}
            </Button.Group>
         </div>
      </div>

      {children({ isView: isView })}
      <div className="dshbrd-hd-bgtrang">
         <div className="dshbrd-nav-overlay" id="dshbrd-nav-overlay" onClick={(e) => handleHiddenMenu()}>

         </div>
         <div className="dshbrd-container" id="dshbrd-menu-hd">
            <div className="dshbrd-inner-wrapper-hd">
               <div style={{ display: "flex" }}>
                  <div className="dshbrd-btn-close">
                     <Icon name="close" onClick={(e) => handleHiddenMenu()} />
                  </div>
                  <div><span className="dshbrd-title-header-nav">Thêm thành phần cho dashboard</span></div>
               </div>
               {
                  availablelets && availablelets.map((item, index) => {
                     return <div className="dshbrd-sys-dashboard">
                        <div className={"dshbrd-item"} style={{ height: 'auto', display: "flex", justifyContent: "space-between" }}>
                           <div className="dshbrd-title-card-nav">
                              {item.ten_dashlet}
                              <div>
                                 <i className="PADMLBL-mota-km">{item.ghi_chu}</i>
                              </div>
                           </div>
                           <div className="dshbrd-btn-close">
                              <Icon name="plus" onClick={(e) => handleAddDashlet(item)} />
                           </div>
                        </div>
                     </div>
                  })
               }
               {
                  availablelets.length == 0 && <>
                     <div className="dshbrd-sys-dashboard">
                        <div className={"dshbrd-item"} style={{ height: 'auto', display: "flex", justifyContent: "space-between" }}>
                           <div className="dshbrd-title-card-nav">
                              {"Không có dashlet nào !"}
                              <div>
                                 <i className="PADMLBL-mota-km">{"Dashlet có thể đã được thêm hết vào dashboard hoặc bạn chưa được cấp quyền sử dụng"}</i>
                              </div>
                           </div>
                        </div>
                     </div>
                  </>
               }
               {/* <ul class="dshbrd-hd-menu-nav">
                  <li class="dshbrd-menu-item dshbrd-current-menu-item">
                     <a href="#">Công Ty</a>
                  </li>
                  <li class="dshbrd-menu-item">
                     <a href="#">sản phẩm</a>
                  </li>
                  <li class="dshbrd-menu-item">
                     <a href="#">dịch vụ</a>
                  </li>
                  <li class="dshbrd-menu-item">
                     <a href="#">khách hàng</a>
                  </li>
                  <li class="dshbrd-menu-item">
                     <a href="#">tin tức</a>
                  </li>
                  <li class="dshbrd-sub-menu menu-item">
                     <a href="#">tuyển dụng <i class="fas fa-caret-down icon-dropdownMB"></i></a>
                     <ul class="dshbrd-dropdown">
                        <li><a href="#">Văn phòng Hồ Chí Minh</a></li>
                        <li><a href="#">Văn phòng Hà Nội</a></li>
                        <li><a href="#">Văn phòng Đà Nẵng</a></li>
                     </ul>
                  </li>
               </ul> */}
            </div>
         </div>
      </div>
      {openDashboardInfo && <ModalDashboardInfo
         open={openDashboardInfo}
         item={dashboard}
         onAfterSave={handleAfterSave}
         onClose={() => setOpenDashboardInfo(false)}
      />}
   </>
}

const ModalDashboardInfo = ({ open, id, idnv, onClose, onAfterSave, item }) => {
   const _isMounted = useRef(true);
   const refFormik = useRef();
   const [error, setError] = useState();
   const [roles, setRoles] = useState([]);
   const [btnLoading, setBtnLoading] = useState();

   useEffect(() => {
      zzControlHelper.dragElement(document.getElementById('dashboardinfo-form'), 'header-dashboardinfo-form')
   })

   // useEffect(() => {

   // }, [])


   const handleSubmit = (params, formId) => {
      setBtnLoading(true)
      ApiDashboard.insert({ ...item, ...params }, res => {
         if (res.status === 200) {
            ZenMessageToast.success()
            { onAfterSave && onAfterSave({ ...item, ...params }) }
         } else {
            setError(ZenHelper.getResponseError(res))
         }
         onClose()
         if (btnLoading) setBtnLoading()
      })
   }

   return <>
      <Modal id="dashboardinfo-form"
         closeOnEscape closeIcon closeOnDimmerClick={false}
         onClose={() => onClose()}
         //onOpen={() => onClose()}
         open={open}
         size={"tiny"}
      >
         <Modal.Header id='header-dashboardinfo-form' style={{ cursor: "grabbing" }}>
            Thông tin dashboard
         </Modal.Header>
         <Modal.Content>
            <ZenFormik form={"hrdmother"} ref={refFormik}
               validation={formValidation}
               initItem={{ ...initItem, title: item.title, description: item.description }}
               onSubmit={handleSubmit}
            >
               {
                  formik => {
                     return <Form>
                        <Form.Group>
                           <ZenField width={16} name="title" props={formik}
                              label="dashboard.title" defaultlabel="Title"
                           />
                        </Form.Group>
                        <Form.Group>
                           <ZenFieldTextArea width={16} name="description" props={formik}
                              label="dashboard.description" defaultlabel="Description"
                           />
                        </Form.Group>
                     </Form>
                  }
               }
            </ZenFormik>
         </Modal.Content>
         <Modal.Actions>
            <ZenButton btnType={"cancel"} size="small" onClick={() => onClose()} />
            <ZenButton btnType="save" size="small"
               loading={btnLoading} type="submit"
               onClick={(e) => refFormik.current.handleSubmit(e)}
            />
         </Modal.Actions>
      </Modal>
   </>
}

const formValidation = []
const initItem = {
   title: "",
   description: "",
}

const styles = {
   fontMenu: {
      fontWeight: "bold",
      fontSize: "18px",
   }
}

const btnName = {
   Add: "add",
   Save: "save",
   Cancel: "cancel",
   Edit: "edit"
}