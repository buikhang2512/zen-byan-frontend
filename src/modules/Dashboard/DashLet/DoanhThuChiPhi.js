import React, { useEffect, useRef, useState } from "react";
import { ApiDashboard } from "../../../Api";
import { Chart, ZenGridDashlet } from "../../../components/Control";
import { ZenHelper } from "../../../utils/global";

export const DoanhThuChiPhi = (props) => {
    const _isMounted = useRef(true)
    const [data, setData] = useState()
    const [error, setError] = useState([])
    const [isLoading, setIsLoading] = useState(true)

    useEffect(() => {
        loadData()
        return () => {
            _isMounted.current = false
        }
    }, [])

    function loadData(params) {
        ApiDashboard.doanhThuChiPhi(params, res => {
            if (_isMounted.current) {
                if (res.status === 200) {
                    const temp = res.data.data[0].map(t => ({ ...t, thang_view: ZenHelper.formatDateTime(t.thang, "MM/YY") }))
                    setData(temp)
                } else {
                    setError(ZenHelper.getResponseError(res))
                }
                setIsLoading(false)
            }
        })
    }

    return <ZenGridDashlet loading={isLoading}
        isScrolling={false} error={error}
    >
        <ZenGridDashlet.Top>
            <h4>Phân tích danh thu theo loại</h4>
        </ZenGridDashlet.Top>

        <ZenGridDashlet.Content >
            <Chart data={data} isFormDashlet={true}
                chartType={"Bar"}
                dataKey="thang_view"
                dataY={[
                    { dataKey: "doanh_thu", name: "Doanh thu" },
                    { dataKey: "chi_phi", name: "Chi phí" },
                ]}
            />
        </ZenGridDashlet.Content>
    </ZenGridDashlet>
}