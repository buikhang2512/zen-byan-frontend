import React, { useEffect, useRef, useState } from "react";
import { ApiDashboard } from "../../../Api";
import { Chart, ZenGridDashlet } from "../../../components/Control";
import { ZenHelper } from "../../../utils/global";

export const LichSuDoanhThu = (props) => {
    const _isMounted = useRef(true)
    const [data, setData] = useState()
    const [error, setError] = useState([])
    const [isLoading, setIsLoading] = useState(true)

    useEffect(() => {
        loadData()
        return () => {
            _isMounted.current = false
        }
    }, [])

    function loadData(params) {
        ApiDashboard.lichSuDoanhThu(params, res => {
            if (_isMounted.current) {
                if (res.status === 200) {
                    const temp = res.data.data[0].map(t => ({ ...t, thang_view: ZenHelper.formatDateTime(t.thang, "MM/YY") }))
                    setData(temp)
                } else {
                    setError(ZenHelper.getResponseError(res))
                }
                setIsLoading(false)
            }
        })
    }

    return <ZenGridDashlet loading={isLoading}
        isScrolling={false} error={error}
    >
        <ZenGridDashlet.Top>
            <h4>So sánh doanh số cùng kỳ</h4>
        </ZenGridDashlet.Top>

        <ZenGridDashlet.Content >
            <Chart data={data} isFormDashlet={true}
                chartType={"Bar"}
                dataKey="thang_view"
                dataY={[
                    { dataKey: "ky_truoc", name: "Kỳ trước" },
                    { dataKey: "ky_nay", name: "Kỳ này" },
                ]}
            />
        </ZenGridDashlet.Content>
    </ZenGridDashlet>
}