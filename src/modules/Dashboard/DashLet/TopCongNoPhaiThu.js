import React, { useEffect, useRef, useState } from "react";
import { Table } from "semantic-ui-react";
import { ApiDashboard } from "../../../Api";
import { FormatNumber, ZenGridDashlet } from "../../../components/Control";
import { ZenHelper } from "../../../utils/global";
import _ from "lodash";

export const TopCongNoPhaiThu = (props) => {
    const _isMounted = useRef(true)
    const [error, setError] = useState([])
    const [isLoading, setIsLoading] = useState(true)

    const [state, dispatch] = React.useReducer(exampleReducer, {
        column: null,
        data: [],
        direction: null,
    })
    const { column, data, direction } = state

    useEffect(() => {
        loadData()
        return () => {
            _isMounted.current = false
        }
    }, [])

    const handleClickHeader = (columnName) => dispatch({ type: 'CHANGE_SORT', column: columnName })

    function loadData(params) {
        ApiDashboard.noPhaiThu(params, res => {
            if (_isMounted.current) {
                if (res.status === 200) {
                    dispatch({
                        type: "SET_DATA",
                        data: res.data.data[0]
                    })
                } else {
                    setError(ZenHelper.getResponseError(res))
                }
                setIsLoading(false)
            }
        })
    }

    return <ZenGridDashlet loading={isLoading}
        isScrolling={true} error={error}
    >
        <ZenGridDashlet.Top>
            <h4>Top công nợ phải thu</h4>
        </ZenGridDashlet.Top>

        <ZenGridDashlet.Content >
            <Table striped selectable sortable>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell content="Mã KH"
                            sorted={column === 'ma_kh' ? direction : null}
                            onClick={() => handleClickHeader("ma_kh")}
                        />
                        <Table.HeaderCell content="Tên KH"
                            sorted={column === 'ten_kh' ? direction : null}
                            onClick={() => handleClickHeader("ten_kh")}
                        />
                        <Table.HeaderCell content="Dư nợ" textAlign="right"
                            sorted={column === 'so_du' ? direction : null}
                            onClick={() => handleClickHeader("so_du")}
                        />
                    </Table.Row>
                </Table.Header>

                <Table.Body>
                    {
                        data && data.map(item => {
                            return <Table.Row key={item.ma_kh}>
                                <Table.Cell>{item.ma_kh}</Table.Cell>
                                <Table.Cell>{item.ten_kh}</Table.Cell>
                                <Table.Cell textAlign="right">
                                    <FormatNumber value={item.so_du} />
                                </Table.Cell>
                            </Table.Row>
                        })
                    }
                </Table.Body>
            </Table>
        </ZenGridDashlet.Content>
    </ZenGridDashlet>
}

function exampleReducer(state, action) {
    switch (action.type) {
        case 'CHANGE_SORT':
            if (state.column === action.column) {
                return {
                    ...state,
                    data: state.data.slice().reverse(),
                    direction:
                        state.direction === 'ascending' ? 'descending' : 'ascending',
                }
            }

            return {
                column: action.column,
                data: _.sortBy(state.data, [action.column]),
                direction: 'ascending',
            }
        case "SET_DATA":
            return { ...state, data: action.data }
        default:
            throw new Error()
    }
}