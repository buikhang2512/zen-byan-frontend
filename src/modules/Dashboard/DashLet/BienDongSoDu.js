import React, { useEffect, useRef, useState } from "react";
import { useIntl } from "react-intl";
import { ApiDashboard } from "../../../Api";
import * as routes from "../../../constants/routes";
import { Chart, ChartSelect, FormatDate, FormatNumber, ZenFieldSelectApi, ZenGridDashlet } from "../../../components/Control";
import { ZenHelper } from "../../../utils/global";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { Link, useHistory, } from "react-router-dom";

export const BienDongSoDu = (props) => {
    const _isMounted = useRef(true)
    const intl = useIntl();
    const [data, setData] = useState([])
    const [filter, setFilter] = useState({ tk: "111" })

    const [error, setError] = useState([])
    const [isLoading, setIsLoading] = useState(true)

    useEffect(() => {
        loadData(filter)
        return () => {
            _isMounted.current = false
        }
    }, [])

    function loadData(params) {
        ApiDashboard.bienDongSoSu(params, res => {
            if (_isMounted.current) {
                if (res.status === 200) {
                    const temp = res.data.data
                    setData(temp.map(t => {
                        return { ...t, ngay_view: ZenHelper.formatDateTime(t.ngay, "DD/MM") }
                    }))
                    setFilter(params)
                } else {
                    setError(ZenHelper.getResponseError(res))
                }
                setIsLoading(false)
            }
        })
    }

    function getLastSoDu() {
        if (data && data.length > 0) {
            return <div style={{ paddingTop: "14px" }}>
                Số dư tại ngày
                <span> <FormatDate value={data[data.length - 1]?.ngay} />:</span>
                <span style={{ fontWeight: "bold" }}>&emsp;<FormatNumber value={data[data.length - 1]?.so_du} /> VNĐ</span>
                <Link style={{ float: "right" }}
                    target="_blank"
                    to={getUrlChiTiet()}
                    >
                    Xem sổ chi tiết
                </Link>
            </div>
        } else {
            return <></>
        }
    }

    function getUrlChiTiet() {
        if (filter.tk.startsWith("111")) {
            return {
                pathname: `${routes.RptCARptTMNH01}`,
                search: `?`+ new URLSearchParams({tk: filter.tk,}).toString(),
                state: { tk: filter.tk },
            }
        } else {
            return {
                pathname: `${routes.RptCARptTMNH02}`,
                search: `?tk=${filter.tk}`,
                state: { tk: filter.tk }
            }
        }
    }

    function getUrlChiTieta() {
        if (filter.tk.startsWith("111")) {
            this.props.location
            //return "#" + routes.RptCARptTMNH01 + `?tk=:${filter.tk}` // so quy tien mat
        } else {
            //return "#" + routes.RptCARptTMNH02 // so tien gui ngan hang
        }
    }

    return <ZenGridDashlet loading={isLoading}
        isScrolling={false} error={error}
    >
        <ZenGridDashlet.Top>
            <h4>Biến động số dư</h4>
        </ZenGridDashlet.Top>

        <ZenGridDashlet.SubTop>
            <ZenFieldSelectApi
                lookup={{
                    ...ZenLookup.TK,
                    onLocalWhere: (items) => {
                        return items?.filter(t => t.tk.startsWith("111") || t.tk.startsWith("112")) || []
                    },
                    // where: 'loai = 1'
                }}
                isIntl={false}
                name="tk"
                value={filter.tk}
                onChange={(e, propsData) => {
                    loadData({ tk: propsData.value })
                }}
            />
            {getLastSoDu()}
            {/* <ChartSelect
                value={chartType}
                onChange={(e, { value }) => setChartType(value)} /> */}
        </ZenGridDashlet.SubTop>

        <ZenGridDashlet.Content >
            <Chart
                data={data} chartType={"Line"} isFormDashlet={true}
                dataKey="ngay_view"
                dataY={[
                    { dataKey: "so_du", name: "Số dư", color: "#82ca9d" },
                ]}
            />
        </ZenGridDashlet.Content>
    </ZenGridDashlet>
}