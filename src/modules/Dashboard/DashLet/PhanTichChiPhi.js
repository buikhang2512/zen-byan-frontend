import React, { useEffect, useRef, useState } from "react";
import { useIntl } from "react-intl";
import { ApiDashboard } from "../../../Api";
import * as routes from "../../../constants/routes";
import { Chart, ChartPie, ChartSelect, FormatDate, FormatNumber, ZenFieldSelectApi, ZenGridDashlet } from "../../../components/Control";
import { ZenHelper } from "../../../utils/global";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { Statistic } from "semantic-ui-react";

export const PhanTichChiPhi = (props) => {
    const _isMounted = useRef(true)
    const [data, setData] = useState({ data: [], total: 0 })
    const [error, setError] = useState([])
    const [isLoading, setIsLoading] = useState(true)

    useEffect(() => {
        loadData()
        return () => {
            _isMounted.current = false
        }
    }, [])

    function loadData(params) {
        ApiDashboard.phanTichChiPhi(params, res => {
            if (_isMounted.current) {
                if (res.status === 200) {
                    const temp = res.data.data[0]
                    setData({ data: temp, total: temp.reduce((a, b) => a + (b["gia_tri"] || 0), 0) })
                } else {
                    setError(ZenHelper.getResponseError(res))
                }
                setIsLoading(false)
            }
        })
    }

    return <ZenGridDashlet loading={isLoading}
        isScrolling={false} error={error}
    >
        <ZenGridDashlet.Top>
            <h4>Chi phí</h4>
        </ZenGridDashlet.Top>

        <ZenGridDashlet.SubTop>
            <label style={{ fontSize: "1.2em" }}>Chi phí ghi nhận gần nhất trong 30 ngày</label>
            <Statistic.Group size="small">
                <Statistic>
                    <Statistic.Value>
                        <FormatNumber value={data.total} />
                    </Statistic.Value>
                </Statistic>
            </Statistic.Group>
        </ZenGridDashlet.SubTop>

        <ZenGridDashlet.Content >
            <ChartPie data={data.data} isFormDashlet //chartType={chartType}
                dataKey={"gia_tri"}
                nameKey="ten_phi"
                customLegend={{
                    layout: "vertical",
                    align: "right",
                    verticalAlign: "top",
                }}
            />
        </ZenGridDashlet.Content>
    </ZenGridDashlet>
}