import { BienDongSoDu } from "./BienDongSoDu";
import { DoanhThuChiPhi } from "./DoanhThuChiPhi";
import { DongTien } from "./DongTien";
import { LichSuDoanhThu } from "./LichSuDoanhThu";
import { NewFeeds } from "./Newfeeds";
import { PhanTichChiPhi } from "./PhanTichChiPhi";
import { TongHopYeuCau } from "./TongHopYeuCau";
import { TopCongNoPhaiThu } from "./TopCongNoPhaiThu";
import { TopCongNoPhaiTra } from "./TopCongNoPhaiTra";
import { TopNVKD } from "./TopNVKD";
import { ViecChuaHoanThanh } from "./ViecChuaHoanThanh";

export const dashletInfo = {
    //functionId: "cong_no_phai_thu",
    "BDSD": BienDongSoDu,
    "doanh_thu": DoanhThuChiPhi,
    "DongTien": DongTien,
    "lic_su_doanh_thu": LichSuDoanhThu,
    "chi_phi_theo_km": PhanTichChiPhi,
    "cong_no_phai_thu": TopCongNoPhaiThu,
    "TopCongNoPhaiTra": TopCongNoPhaiTra,
    "top_nvkd": TopNVKD,
    "tong_hop_yeu_cau": TongHopYeuCau,
    "viec_chua_hoan_thanh": ViecChuaHoanThanh,
    "new_feeds": NewFeeds,
}