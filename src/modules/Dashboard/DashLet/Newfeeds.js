import React, { useEffect, useRef, useState } from "react";
import { useIntl } from "react-intl";
import { ApiDashboard, ApiStored } from "../../../Api";
import * as routes from "../../../constants/routes";
import { Chart, ChartSelect, FormatDate, FormatNumber, ZenFieldSelectApi, ZenGridDashlet, ZenLink, ZenMessageAlert } from "../../../components/Control";
import { ZenHelper, auth } from "../../../utils";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { Link, useHistory, } from "react-router-dom";
import { Button, Divider, Feed, Header, Icon, ItemDescription, List, Segment } from "semantic-ui-react";
import { FormMode, zzControlHelper } from "../../../components/Control/zzControlHelper";
import _ from "lodash";
import { getModal } from "../../ComponentInfo/Dictionary/ZenGetModal";
import { ApiWMCongViec } from "../../WM/Api";
import { ModalCongViecDetail } from "../../AR/Dictionary/ARDmKhDetail.js/CongViecComponent";
import { ApiHrLichDay } from "../../HrDmOther/Api";

export const NewFeeds = (props) => {
    const _isMounted = useRef(true)
    const intl = useIntl();
    const [data, setData] = useState([])
    const [filter, setFilter] = useState({})
    // Công việc
    const [itemcv, setItemcv] = useState()
    const [modalInfoCV, setModalInfoCV] = useState(getModal(ZenLookup.WMCongViec));
    const [openModalDetailCongviec, setOpenModalDetailCongViec] = useState()
    //Lịch dạy
    const [itemLichDay, setItemLichDay] = useState()
    const [modalInfoLichDay, setModalInfoLichDay] = useState(getModal(ZenLookup.HrLichDay));
    const [openModalDetailLichDay, setOpenModalDetailLichDay] = useState()

    const [error, setError] = useState([])
    const [isLoading, setIsLoading] = useState(true)


    const timer = useRef(null);

    useEffect(() => {
        loadData(filter)
        return () => {
            _isMounted.current = false
        }
    }, [])

    useEffect(
        () => {
            let delay = 300000;
            if (delay) {
                timer.current = setInterval(() => {
                    loadData()
                }, delay);
                return () => {
                    clearInterval(timer.current);
                };
            }
        },
        []
    );

    function loadData(params) {

        setIsLoading(true)
        ApiDashboard.getDashlet('newfeeds', params, res => {
            if (res.status === 200) {
                setData(res.data.data[0])
            } else {
                setError(ZenHelper.getResponseError(res))
            }
            setIsLoading(false)
        })

    }

    const handleOpenCloseModalCV = (formMode, item) => {
        if (!modalInfoCV) {
            ZenMessageAlert.error("Chưa khai báo thông tin modal")
            return
        }

        setModalInfoCV({
            ...modalInfoCV,
            info: {
                ...modalInfoCV.info,
                initItem: { ...modalInfoCV.info.initItem, id: item?.id_cv },
            },
            id: item ? item.id_cv : "",
            formMode: formMode,
            open: true,
            other: {
                type: 'ma_kh'
            }
        })
    }

    const handleOpenCloseModalLD = (formMode, item) => {
        if (!modalInfoLichDay) {
            ZenMessageAlert.error("Chưa khai báo thông tin modal")
            return
        }

        setModalInfoLichDay({
            ...modalInfoLichDay,
            info: {
                ...modalInfoLichDay.info,

                checkPermission: (permis) => auth.checkPermission(permis),
                initItem: {
                    ...modalInfoLichDay.info.initItem,
                    id: item ? item.id : "",
                    id_nv: item ? item.id_nv : "",
                    ma_kh: item ? item.ma_kh : "",
                    ngay: item ? item.ngay : "",
                },
                noChange: item ? item?.id_nv ? 'nv' : 'kh' : false,
            },
            id: item ? item.id : "",
            formMode: formMode,
            open: true,
            other: {
                id: item ? item.id : "",
                type: 'id',
            }
        })
    }

    const handleAfterSaveModalCV = (newItem, { mode }) => {
        // change
        ApiWMCongViec.getByCode(newItem.id, res => {
            loadData()
            // if (mode === FormMode.ADD) {
            //     setData(data.concat(res.data.data))
            // } else if (mode === FormMode.EDIT) {
            //     setData(data.map(t => t.id === newItem.id ? res.data.data : t))
            // }
            // close modal
            setModalInfoCV({
                ...modalInfoCV,
                id: "",
                formMode: FormMode.NON,
                open: false,
            })
        })
    }

    const handleAfterSaveModalLD = (newItem, { mode }) => {
        // change
        ApiHrLichDay.getByCode(newItem.id, res => {
            loadData()
            setModalInfoLichDay({
                ...modalInfoLichDay,
                id: "",
                formMode: FormMode.NON,
                open: false,
            })
        })
    }

    function getListparticipant(str) {
        if (!str) {
            return;
        }
        var list = _.split(str, ',');
        if (list) {
            return <List bulleted>{
                list.map(x => {
                    if (x) {
                        return <List.Item>{x} </List.Item>
                    }
                })
            }
            </List>
        }

    }

    const handleOpenDetail = (type, item) => {
        if (type === 'WmCongViec') {
            setItemcv(item)
            setOpenModalDetailCongViec(true)
        }
        if (type === 'HRLichDay') {
            handleOpenCloseModalLD(FormMode.EDIT, item)
        }
    }

    const handleClickLink = (e, type, id) => {
        if (!id) {
            e.preventDefault()
            ZenMessageAlert.warning('Nội dung này không có đối tượng liên kết')
        }
        if (type === 'WmCongViec') {
            e.preventDefault()
            ApiWMCongViec.getByCode(id, res => {
                if (res.status === 200) {
                    handleOpenDetail(type, res.data.data)
                }
            })
        }

        if (type === 'HRLichDay') {
            e.preventDefault()
            ApiHrLichDay.getByCode(id, res => {
                if (res.status === 200) {
                    handleOpenDetail(type, res.data.data)
                }
            })
        }
    }

    return <ZenGridDashlet loading={isLoading}
        isScrolling={false} error={error}
    >
        <ZenGridDashlet.Top>
            <h4>{"New feeds"}</h4>
        </ZenGridDashlet.Top>

        <ZenGridDashlet.SubTop>
        </ZenGridDashlet.SubTop>

        <ZenGridDashlet.Content >
            <Segment vertical >
                {data && data.map((item, index) => {
                    return <>
                        <Feed size='large'>
                            <Feed.Event>
                                <Feed.Label icon={item.feed_icon} />
                                <Feed.Content>
                                    <Feed.Summary>
                                        <Feed.User>{item.feed_user_name}</Feed.User>&nbsp;&nbsp;&nbsp; {item.feed_event}
                                        <Feed.Date>
                                            lúc{" "}
                                            {ZenHelper.formatDateTime(item.feed_time, "DD/MM/YYYY HH:mm")}
                                        </Feed.Date>
                                    </Feed.Summary>
                                    <Feed.Extra text style={{
                                        overflow: "auto",
                                        width: "auto",
                                        maxWidth: "100%",
                                        whiteSpace: "pre-line",
                                        overflowWrap: "break-word"
                                    }}
                                    >
                                        {item.feed_text}

                                    </Feed.Extra>
                                    <div style={{ paddingTop: "0.5em" }}>
                                        {item.feed_object_type !== "" ? (
                                            <Link as="a" to={`${item.feed_object_type}/${zzControlHelper.btoaUTF8(item.feed_object_id)}`} onClick={(e) => handleClickLink(e, item.feed_object_type, item.feed_object_id)}>
                                                <span style={{ fontSize: "0.8em", fontWeight: "bold" }}>{item.feed_object_name}</span>
                                            </Link>
                                        ) : null}
                                    </div>
                                </Feed.Content>
                            </Feed.Event>
                            <Divider fitted style={{ marginRight: "100px", marginLeft: "100px" }} />
                        </Feed>
                    </>
                })
                }
            </Segment>
            {zzControlHelper.openModalComponent(modalInfoCV,
                {
                    onClose: () => handleOpenCloseModalCV(FormMode.NON),
                    onAfterSave: handleAfterSaveModalCV,
                }
            )}

            {zzControlHelper.openModalComponent(modalInfoLichDay,
                {
                    onClose: () => handleOpenCloseModalLD(FormMode.NON),
                    onAfterSave: handleAfterSaveModalLD,
                }
            )}
            {openModalDetailCongviec && <ModalCongViecDetail
                open={openModalDetailCongviec}
                modalInfoCV={modalInfoCV}
                onClose={() => { setOpenModalDetailCongViec(false); setItemcv() }}
                cvItem={{ ...itemcv }}
                type={'MA_KH'}
                valueKey={itemcv.ma_kh}
                keyData={'ma_kh'}
                fileCodeName={"CRM"}
            />}
        </ZenGridDashlet.Content>
    </ZenGridDashlet>
}