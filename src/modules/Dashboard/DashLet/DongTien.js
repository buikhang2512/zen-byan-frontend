import React, { useEffect, useRef, useState } from "react";
import { ApiDashboard } from "../../../Api";
import { Chart, FormatNumber, ZenGridDashlet } from "../../../components/Control";
import { ZenHelper } from "../../../utils/global";
import { Statistic } from "semantic-ui-react";

export const DongTien = (props) => {
    const _isMounted = useRef(true)
    const [data, setData] = useState()
    const [error, setError] = useState([])
    const [isLoading, setIsLoading] = useState(true)

    useEffect(() => {
        loadData()
        return () => {
            _isMounted.current = false
        }
    }, [])

    function loadData(params) {
        ApiDashboard.dongTien(params, res => {
            if (_isMounted.current) {
                if (res.status === 200) {
                    const temp = res.data.data.map(t => ({ ...t, thang_view: ZenHelper.formatDateTime(t.thang, "MM/YY") }))
                    setData(temp)
                } else {
                    setError(ZenHelper.getResponseError(res))
                }
                setIsLoading(false)
            }
        })
    }

    return <ZenGridDashlet loading={isLoading}
        isScrolling={false} error={error}
    >
        <ZenGridDashlet.Top>
            <h4>Dòng tiền 12 tháng gần nhất</h4>
        </ZenGridDashlet.Top>

        <ZenGridDashlet.Content >
            <Chart data={data} isFormDashlet={true}
                chartType={"Area"}
                dataKey="thang_view"
                dataY={[
                    { dataKey: "thu", name: "Tiền thu" },
                    { dataKey: "chi", name: "Tiền chi" },
                ]}
                customLegend={{
                    // align: "right",
                    // verticalAlign: "top",
                }}
            />
        </ZenGridDashlet.Content>
    </ZenGridDashlet>
}