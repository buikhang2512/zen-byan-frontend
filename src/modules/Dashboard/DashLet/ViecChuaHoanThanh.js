import React, { useEffect, useRef, useState } from "react";
import { useIntl } from "react-intl";
import { ApiDashboard, ApiStored } from "../../../Api";
import * as routes from "../../../constants/routes";
import { Chart, ChartSelect, FormatDate, FormatNumber, ZenFieldSelectApi, ZenGridDashlet, ZenLink } from "../../../components/Control";
import { ZenHelper } from "../../../utils/global";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { Link, useHistory, } from "react-router-dom";
import { Button, Divider, Header, Icon, ItemDescription, List, Segment } from "semantic-ui-react";
import { FormMode, zzControlHelper } from "../../../components/Control/zzControlHelper";
import _ from "lodash";
import { getModal } from "../../ComponentInfo/Dictionary/ZenGetModal";
import { ApiWMCongViec } from "../../WM/Api";
import { ModalCongViecDetail } from "../../AR/Dictionary/ARDmKhDetail.js/CongViecComponent";
import { auth } from "../../../utils";

export const ViecChuaHoanThanh = (props) => {
    const _isMounted = useRef(true)
    const intl = useIntl();
    const [data, setData] = useState([])
    const [filter, setFilter] = useState({})
    const [modalInfoCV, setModalInfoCV] = useState(getModal(ZenLookup.WMCongViec));
    const [error, setError] = useState([])
    const [isLoading, setIsLoading] = useState(true)
    const [openModalDetailCongviec, setOpenModalDetailCongViec] = useState()
    const [itemcv, setItemcv] = useState()
    const timer = useRef(null);

    useEffect(() => {
        loadData(filter)
        return () => {
            _isMounted.current = false
        }
    }, [])

    useEffect(
        () => {
            let delay = 300000;
            if (delay) {
                timer.current = setInterval(() => {
                    loadData()
                }, delay);
                return () => {
                    clearInterval(timer.current);
                };
            }
        },
        []
    );

    function loadData(params) {

        setIsLoading(true)
        ApiDashboard.getDashlet('viec_chua_hoan_thanh', params, res => {
            if (res.status === 200) {
                setData(res.data.data[0])
            } else {
                setError(ZenHelper.getResponseError(res))
            }
            setIsLoading(false)
        })

    }

    const handleOpenCloseModal = (formMode, item) => {
        if (!modalInfoCV) {
            ZenMessageAlert.error("Chưa khai báo thông tin modal")
            return
        }

        setModalInfoCV({
            ...modalInfoCV,
            info: {
                ...modalInfoCV.info,
                initItem: { ...modalInfoCV.info.initItem, id: item?.id_cv },
                checkPermission: (permiss) => auth.checkPermission(permiss)
            },
            id: item ? item.id_cv : "",
            formMode: formMode,
            open: true,
            other: {
                type: 'ma_kh'
            }
        })
    }

    const handleAfterSaveModal = (newItem, { mode }) => {
        // change
        ApiWMCongViec.getByCode(newItem.id, res => {
            loadData()
            // if (mode === FormMode.ADD) {
            //     setData(data.concat(res.data.data))
            // } else if (mode === FormMode.EDIT) {
            //     setData(data.map(t => t.id === newItem.id ? res.data.data : t))
            // }
            // close modal
            setModalInfoCV({
                ...modalInfoCV,
                id: "",
                formMode: FormMode.NON,
                open: false,
            })
        })
    }

    function getListparticipant(str) {
        if (!str) {
            return;
        }
        var list = _.split(str, ',');
        if (list) {
            return <List bulleted>{
                list.map((x, index) => {
                    if (x) {
                        return <List.Item key={index}>{x} </List.Item>
                    }
                })
            }
            </List>
        }

    }

    const handleOpenCVDetail = (e, item) => {
        e.preventDefault();
        setOpenModalDetailCongViec(true)
        setItemcv(item)
    }

    return <ZenGridDashlet loading={isLoading}
        isScrolling={false} error={error}
    >
        {/* <ZenGridDashlet.Top>
            <h4>&nbsp;</h4>
        </ZenGridDashlet.Top>

        <ZenGridDashlet.SubTop>
        </ZenGridDashlet.SubTop> */}

        <ZenGridDashlet.Content >
            <Segment vertical >
                <Header as="h3" floated="left">
                    {" "}
                    Việc chưa hoàn thành
                </Header>

                <Button
                    basic
                    color={"green"}
                    floated="right"
                    icon
                    size="mini"
                    loading={isLoading}
                    onClick={() => loadData()}
                >
                    <Icon name="refresh" />
                </Button>
                <Button
                    color="teal"
                    basic
                    floated="right"
                    size="mini"
                    icon
                    onClick={() => handleOpenCloseModal(FormMode.ADD)}
                >
                    <Icon name="plus" />
                </Button>
                <Divider clearing />

                {data.length > 0 && <List relaxed> {data.map(item => {
                    return <List.Item key={item.id}>
                        <Segment
                        //color={this.getColorName(item.date_due)}
                        >
                            <List.Content>

                                <List.Header as='h3'>
                                    <a href="#" style={{ fontWeight: 'bold' }} onClick={(e) => handleOpenCVDetail(e, item)}>{item.ten_cong_viec}</a>
                                    &nbsp;
                                    <Button basic size='mini' color='blue' icon floated='right'
                                        onClick={() => handleOpenCloseModal(FormMode.EDIT, item)}
                                    >
                                        <Icon name="edit" /></Button>
                                </List.Header>

                                <List.Description>
                                    <div><strong>Khách hàng:</strong> <ZenLink to={`/ardmkh/${zzControlHelper.btoaUTF8(item.ma_kh)}`} >{item.ten_kh}</ZenLink></div>

                                    <Divider hidden style={{ margin: '0.25rem 0rem' }} />
                                    <div><strong>Ngày dự kiến:</strong> {ZenHelper.formatDateTime(item.ngay_kt, 'DD/MM/YYYY')} - <strong>Trạng
                                        thái:</strong> {item.trang_thai}</div>
                                    <div><strong>Phụ trách:</strong> {item.phu_trach}</div>
                                    <div><strong>Tham gia:</strong> {<label>{getListparticipant(item.tham_gia)}</label>}</div>
                                </List.Description>
                            </List.Content></Segment>
                    </List.Item>
                })}
                </List>}
            </Segment>
            {zzControlHelper.openModalComponent(modalInfoCV,
                {
                    onClose: () => handleOpenCloseModal(FormMode.NON),
                    onAfterSave: handleAfterSaveModal,
                }
            )}
            {openModalDetailCongviec && <ModalCongViecDetail
                open={openModalDetailCongviec}
                modalInfoCV={modalInfoCV}
                onClose={() => { setOpenModalDetailCongViec(false); setItemcv() }}
                cvItem={{ ...itemcv, id: itemcv.id_cv }}
                type={'MA_KH'}
                valueKey={itemcv.ma_kh}
                keyData={'ma_kh'}
                fileCodeName={"CRM"}
            />}
        </ZenGridDashlet.Content>
    </ZenGridDashlet>
}