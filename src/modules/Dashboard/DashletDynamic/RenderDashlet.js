import React, { useEffect, useRef, useState } from "react";
import { useIntl } from "react-intl";
import { ApiDashboard, ApiDashLet } from "../../../Api";
import * as routes from "../../../constants/routes";
import { Chart, ChartPie, ChartSelect, FormatDate, FormatNumber, ZenFieldSelectApi, ZenGridDashlet, ZenMessageAlert } from "../../../components/Control/index";
import { ZenHelper } from "../../../utils/global";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { Link, useHistory, } from "react-router-dom";
import NavbarDesktop from "../../../components/Navbar/NavbarDesktop";
import { Divider, Feed, Icon, Label, Segment, Statistic, Table } from "semantic-ui-react";
import { zzControlHelper } from "../../../components/Control/zzControlHelper";
import _ from "lodash";

export const RenderDashlet = (props) => {
    const { ContainerTop, i, web_layout, title, onAfterLoadData } = props
    const _isMounted = useRef(true)
    const timer = useRef(null);
    const intl = useIntl();

    const [error, setError] = useState([])
    const [isLoading, setIsLoading] = useState(true)
    const [layout, setLayout] = useState()
    const [data, setData] = useState()
    const [newDataY, setNewDataY] = useState()
    const [state, dispatch] = React.useReducer(exampleReducer, {
        column: null,
        _data: [],
        direction: null,
    })
    const { column, _data, direction } = state
    useEffect(() => {
        loadData()
        return () => {
            _isMounted.current = false
        }
    }, [])

    useEffect(
        () => {
            let delay;
            let _layout = JSON.parse(web_layout)
            if (_layout?.listtotal?.interval_reload) {
                delay = JSON.parse(web_layout)['listtotal']['interval_reload']
            } else if (_layout?.listview?.interval_reload) {
                delay = JSON.parse(web_layout)['listview']['interval_reload']
            } else if (_layout?.chart?.interval_reload) {
                delay = JSON.parse(web_layout)['chart']['interval_reload']
            } else if (_layout?.piechart?.interval_reload) {
                delay = JSON.parse(web_layout)['piechart']['interval_reload']
            }
            if (delay) {
                timer.current = setInterval(() => {
                    loadData()
                }, delay);
                return () => {
                    clearInterval(timer.current);
                };
            }
        },
        []
    );

    function loadData() {
        fetchData(i, {}).then(res => {
            let _layout = JSON.parse(web_layout)
            onAfterLoadData && (res = onAfterLoadData(i, res))
            if (_layout.listview || _layout.listtotal) {
                dispatch({
                    type: "SET_DATA",
                    _data: _layout.sortby ? _.sortBy(res[0], _layout.sortby) : res[0]
                })
            }
            // let newDataY = [];
            // if (_layout?.chart) {
            //     if (_layout?.chart?.template?.custom_data_y && _layout?.chart?.template?.custom_data_y == true) {
            //         res[0].map((item, index) => {
            //             if (item[_layout?.chart?.template?.data_y[0].name]) {
            //                 newDataY.push({ dataKey: 'gia_tri', name: item[_layout?.chart?.template?.data_y[0].name] })
            //             }
            //         })
            //         setNewDataY(newDataY)
            //     }
            // }

            setData(res[0])

            setLayout(_layout)
            setIsLoading(false)
        }).catch(err => {
            setError(ZenHelper.getResponseError(err))
        })
    }

    function fetchData(id, params) {
        var result = new Promise((resolve, reject) => {
            ApiDashboard.getDashlet(id, params, res => {
                if (_isMounted.current) {
                    if (res.status >= 200 && 204 >= res.status) {
                        resolve(res.data.data)
                    } else {
                        reject(res)
                    }
                }
            })
        })
        return result
    }

    const handleClickHeader = (columnName) => {
        dispatch({ type: 'CHANGE_SORT', column: columnName })
    }


    // const COLORS = ["#0088FE", "#00C49F", "#FFBB28", "#FF8042"];
    return <ZenGridDashlet loading={isLoading}
        isScrolling={false} error={error}
    >
        <ZenGridDashlet.Top>
            <h4>{title} &nbsp;</h4>
        </ZenGridDashlet.Top>

        <ZenGridDashlet.SubTop>
            {ContainerTop && <ContainerTop loadData={loadData} />}
            {layout?.statistic && <>
                {/* <div>
                <label style={{ fontSize: "1.2em" }}>{layout?.statistic?.title_statistic}</label>
                </div> */}
                <Table basic="very" style={{ fontSize: "1.2em", margin: " 7px 0px" }}>
                    <Table.HeaderCell textAlign="left">
                        {layout?.statistic?.title_statistic}
                    </Table.HeaderCell>
                    <Table.HeaderCell textAlign="left">
                        <FormatNumber value={data.reduce((a, b) => a + (b[layout?.statistic?.template.fieldname] || 0), 0)} />
                    </Table.HeaderCell>
                </Table>
                <Divider />
                <div>
                    <label style={{ fontSize: "1.2em", fontWeight: "bold" }}>{layout?.statistic.template.label}</label>
                </div>
            </>}
        </ZenGridDashlet.SubTop>

        <ZenGridDashlet.Content >
            {
                layout?.chart && <Chart
                    data={data} chartType={layout?.chart?.template?.chart_type} isFormDashlet={layout?.chart?.template?.isformdashlet}
                    dataKey={layout?.chart?.template?.name_key}
                    dataY={newDataY || layout?.chart?.template?.data_y}
                />
            }

            {
                layout?.piechart && <ChartPie
                    data={data} isFormDashlet={true}
                    chartType={layout?.piechart?.template?.chart_type || 'Simple'}
                    dataKey={layout?.piechart?.template?.data_key}
                    nameKey={layout?.piechart?.template?.name_key}
                    customLegend={{ ...layout?.piechart?.template.custom_legend }}
                />
            }

            {
                layout?.listview && <Table striped selectable sortable>
                    <Table.Header>
                        <Table.Row>
                            {
                                layout?.listview.listitem.map(col => {
                                    return <Table.HeaderCell content={col.text}
                                        sorted={column === col.fieldname ? direction : null}
                                        textAlign={col.type === "string" ? "left" : "right"}
                                        onClick={() => handleClickHeader(col.fieldname)}
                                    />
                                })
                            }
                        </Table.Row>
                    </Table.Header>

                    <Table.Body>
                        {
                            _data && _data.map((item, index) => {
                                let cell = []
                                layout?.listview.listitem.forEach(col => {
                                    cell.push(
                                        <Table.Cell textAlign={col.type === "string" ? "left" : "right"}>
                                            {col.type === "string" ? item[col.fieldname]
                                                : col.type === "number" ? <FormatNumber value={item[col.fieldname]} />
                                                    : col.type === "date" && parseInt(item[col.fieldname]) > 1900 ? ZenHelper.formatDateTime(item[col.fieldname], 'DD/MM/YYYY')
                                                        : ""
                                            }
                                        </Table.Cell>
                                    )
                                })
                                return <Table.Row>{cell}</Table.Row>
                            })
                        }
                    </Table.Body>
                </Table>
            }

            {
                layout?.listtotal && <>
                    <Table basic="very" celled>
                        <Table.Header>
                            {(_data && _data.length > 0)
                                ? _data.map((item, idx) => {
                                    let cell = []
                                    layout?.listtotal.listitem.forEach(col => {
                                        cell.push(<>
                                            <Table.HeaderCell textAlign="left">{item[col.fieldtext]}
                                            </Table.HeaderCell>
                                            <Table.HeaderCell textAlign="right">
                                                <FormatNumber value={ZenHelper.formatNumber(item[col.fieldvalue] ? item[col.fieldvalue] : 0, 0)} />
                                            </Table.HeaderCell>
                                        </>)
                                    })
                                    return <Table.Row key={idx}>{cell}</Table.Row>
                                })
                                : null
                            }

                        </Table.Header>
                    </Table>
                </>
            }
            
        </ZenGridDashlet.Content>
    </ZenGridDashlet>
}

function exampleReducer(state, action) {
    switch (action.type) {
        case 'CHANGE_SORT':
            if (state.column === action.column) {
                return {
                    ...state,
                    _data: state._data.slice().reverse(),
                    direction:
                        state.direction === 'ascending' ? 'descending' : 'ascending',
                }
            }

            return {
                column: action.column,
                _data: _.sortBy(state._data, [action.column]),
                direction: 'ascending',
            }
        case "SET_DATA":
            return { ...state, ...action, _data: action._data, }
        default:
            throw new Error()
    }
}