import React, { useEffect, useState } from 'react';
import * as routes from "../../../constants/routes";
import * as permissions from "../../../constants/permissions";
import { ApiDashLet, ApiDashBoardTemplate } from "../../../Api/index";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { ZenField, ZenLink, ZenMessageAlert } from "../../../components/Control";
import { Table } from "semantic-ui-react";
import { getModal } from "../../ComponentInfo/Dictionary/ZenGetModal";
import { FormMode, zzControlHelper } from "../../../components/Control/zzControlHelper"
import { auth } from '../../../utils';

const Dash_Dictionary = {
   Dashlet: {
      route: routes.SysDashLet,

      action: {
         view: { visible: true, permission: "" },
         add: { visible: true, permission: "" },
         edit: { visible: true, permission: "" },
         del: { visible: true, permission: "" },
      },

      linkHeader: {
         id: "DashLet",
         defaultMessage: "DashLet",
         active: true,
         isSetting: true,
      },

      tableList: {
         keyForm: "DashLet",
         formModal: ZenLookup.SysDashLet,
         fieldCode: "id",
         unPagination: false,
         duplicate: true,

         //  hasChecked: true,
         // onUpdateBulk: (items) => {

         // },

         api: {
            url: ApiDashLet,
            type: "sql",
         },
         columns: [
            { id: "DashLet.id", defaultMessage: "Dashlet ID", fieldName: "id", type: "string", filter: "string", sorter: true, editForm: true },
            { id: "DashLet.ten_dashlet", defaultMessage: "Title", fieldName: "ten_dashlet", type: "string", filter: "string", sorter: true, },
            { id: "DashLet.sp_name", defaultMessage: "Tên Store Procedure", fieldName: "sp_name", type: "string", filter: "string", sorter: true, },
            { id: "ksd", defaultMessage: "Không sử dụng", fieldName: "ksd", type: "bool", filter: "", sorter: true, collapsing: true, propsCell: { textAlign: "center" } },
         ],
      },
   },
   DashboardTemplate: {
      route: routes.SysDashBoardTemplate,

      action: {
         view: { visible: true, permission: permissions.DashBoardTemplate },
         add: { visible: true, permission: permissions.DashBoardTemplate },
         edit: { visible: false, permission: permissions.DashBoardTemplate },
         del: { visible: false, permission: permissions.DashBoardTemplate },
      },

      linkHeader: {
         id: "DashBoardTemplate",
         defaultMessage: "Dashboard mẫu",
         active: true,
         isSetting: false,
      },

      tableList: {
         keyForm: "DashBoardTemplate",
         formModal: ZenLookup.SysDashBoardTemplate,
         fieldCode: "id",
         unPagination: false,
         duplicate: true,

         //  hasChecked: true,
         // onUpdateBulk: (items) => {

         // },

         api: {
            url: ApiDashBoardTemplate,
            type: "sql",
         },
         columns: [
            { id: "DashBoardTemplate.vai_tro", defaultMessage: "Vai trò", fieldName: "rolename", type: "string", filter: "string", sorter: true, },
            { id: "DashBoardTemplate.title", defaultMessage: "Tên Dashboard", fieldName: "title", type: "string", filter: "string", sorter: true, },
            { id: "DashBoardTemplate.custom_dashboard", defaultMessage: "...", fieldName: "custom_dashboard", type: "string", custom: true },
            // { id: "ksd", defaultMessage: "Không sử dụng", fieldName: "ksd", type: "bool", filter: "", sorter: true, collapsing: true, propsCell: { textAlign: "center" } },
         ],
         customCols: (data, item, fieldName, infoCol) => {
            const [modalInfoDashboardTemplate, setModalInfoDashboardTemplate] = useState(getModal(ZenLookup.SysDashBoardTemplate))
            let customCell = undefined
            const handleOpenCloseModal = (e, formMode, item) => {
               if (e) e.preventDefault()
               if (!modalInfoDashboardTemplate) {
                  ZenMessageAlert.error("Chưa khai báo thông tin modal")
                  return
               }
               setModalInfoDashboardTemplate({
                  ...modalInfoDashboardTemplate,
                  info: {
                     ...modalInfoDashboardTemplate.info,
                     initItem: { ...modalInfoDashboardTemplate.info.initItem, id: item?.id },
                     checkPermission: (permis) => auth.checkPermission(permis),
                  },
                  id: item ? item.id : "",
                  formMode: formMode,
                  open: true,
                  other: {
                     type: 'id'
                  }
               })
            }

            function findIndex (item) {
               return data.findIndex(x => x.id === item.id)
            }

            const handleAfterSaveModal = (newItem, { mode }) => {
               // change
               let index = findIndex(newItem)
               data[index] = newItem
               item = newItem
               setModalInfoDashboardTemplate({
                  ...modalInfoDashboardTemplate,
                  info: {
                     ...modalInfoDashboardTemplate.info,
                     initItem: { ...newItem },
                 },
                  id: "",
                  formMode: FormMode.NON,
                  open: false,
               })
            }
            if (fieldName === 'custom_dashboard') {
               customCell = <Table.Cell key={fieldName}>
                  <ZenLink style={{ fontWeight: 'bold' }} to={`/sys-dashboardtemplate/${zzControlHelper.btoaUTF8(item.id)}`} >{'Customize Dashboard '}</ZenLink>
                  &nbsp;
                  <a style={{ fontWeight: 'bold' }} href="#" onClick={(e) => handleOpenCloseModal(e, FormMode.EDIT, item)}>{'Sửa tên'}</a>
                  {zzControlHelper.openModalComponent(modalInfoDashboardTemplate,
                     {
                        onClose: () => handleOpenCloseModal(null, FormMode.NON),
                        onAfterSave: handleAfterSaveModal,
                     }
                  )}
               </Table.Cell>
            }
            return customCell
         }
      },
   },
}

export { Dash_Dictionary }