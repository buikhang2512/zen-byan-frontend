import React, { useCallback, useEffect, useState } from "react";
import { Form } from "semantic-ui-react";
import { ZenField, ZenFieldDate, ZenFieldNumber, ZenFieldSelectApi, ZenFieldTextArea, ZenButton, ZenFieldCheckbox } from "../../../components/Control/index";
import { FormMode, zzControlHelper } from "../../../components/Control/zzControlHelper";
import { ZenHelper } from "../../../utils";
import { IntlFormat } from "../../../utils/intlFormat";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { ApiDashLet } from "../../../Api/index";
import * as routes from '../../../constants/routes';

const DashLetModal = (props) => {
    const { formik, permission, mode } = props
    return <>
        <Form.Group>
            <ZenField required readOnly={!permission}
                label="DashLet.ma_dashlet" defaultlabel="Mã dashlet"
                name="id"
                props={formik}
            />
            <Form.Field>
                <label>&nbsp;</label>
                <ZenFieldCheckbox readOnly={!permission} style={{ paddingTop: "8px" }}
                    label="DashLet.isbasic" defaultlabel="Đây là dashlet basic"
                    name="isbasic"
                    props={formik}
                />
            </Form.Field>
        </Form.Group>
        <Form.Group>
            <ZenField required readOnly={!permission} width={16}
                label="DashLet.ten_Dashlet" defaultlabel="Tên dashlet"
                name="ten_dashlet"
                props={formik}
            />
        </Form.Group>
        <Form.Group>
            <ZenFieldCheckbox readOnly={!permission}
                label="DashLet.iswin" defaultlabel="Show in Winform"
                name="iswin"
                props={formik}
            />
            <ZenFieldCheckbox readOnly={!permission}
                label="DashLet.isweb" defaultlabel="Show in Web App"
                name="isweb"
                props={formik}
            />
            <ZenFieldCheckbox readOnly={!permission}
                label="DashLet.ismobile" defaultlabel="Show in Mobile App"
                name="ismobile"
                props={formik}
            />
        </Form.Group>
        <Form.Group widths="equal">
            <ZenFieldNumber readOnly={!permission}
                label="DashLet.x" defaultlabel="Cột (x)"
                name="x"
                props={formik}
            />
            <ZenFieldNumber readOnly={!permission}
                label="DashLet.y" defaultlabel="Dòng (y)"
                name="y"
                props={formik}
            />
            <ZenFieldNumber readOnly={!permission}
                label="DashLet.w" defaultlabel="Chiều rộng (w)"
                name="w"
                props={formik}
            />
            <ZenFieldNumber readOnly={!permission}
                label="DashLet.h" defaultlabel="Chiều cao (h)"
                name="h"
                props={formik}
            />
        </Form.Group>
        <Form.Group>
            <ZenField readOnly={!permission} width={16}
                label="DashLet.sp_name" defaultlabel="Store Procedure"
                name="sp_name"
                props={formik}
            />
        </Form.Group>
        <Form.Group>
            <ZenFieldTextArea style={{ minHeight: 250 }} width={16} name="web_layout" props={formik}
                label="dashboard.web_layout" defaultlabel="Web layout"
            />
        </Form.Group>
        <Form.Group>
            <ZenFieldTextArea readOnly={!permission} width={16}
                label="DashLet.ghi_chu" defaultlabel="Ghi chú"
                name="ghi_chu"
                props={formik}
            />
        </Form.Group>
    </>
}

export const DashLet = {
    FormModal: DashLetModal,

    api: {
        url: ApiDashLet,
    },

    permission: {
        view: "",
        add: "",
        edit: ""
    },
    fieldCode: "id",
    formId: "DashLet-form",
    size: "small",
    initItem: {
        id: "",
        ten_dashlet: "",
        isbasic: false,
        iswin: false,
        isweb: false,
        ismobile: false,
        sp_name: "",
        ksd: false,
        ghi_chu: "",
        web_layout: "",
        x:0,
        y:0,
        h: 0,
        w: 0,
    },
    formValidation: [
        {
            id: "id",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ]
        },
        {
            id: "ten_dashlet",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ]
        },
    ]
}