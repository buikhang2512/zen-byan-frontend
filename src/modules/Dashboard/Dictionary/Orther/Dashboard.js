import React, { useCallback, useEffect, useReducer, useRef, useState } from "react";
import { Button, Menu, Sidebar, Segment, Icon, Header, Modal, Form, HeaderContent, Breadcrumb } from "semantic-ui-react";
import { ApiDashboard, ApiDashBoardTemplate, ApiDashLet, ApiRole, ApiRoleDashlet, ApiStored } from "../../../../Api";
import { ZenFormik, ZenGridLayout, ZenButton, ZenField, ZenFieldTextArea, ZenMessageToast, ZenMessageAlert, ZenFieldSelect, HeaderLink, Helmet, SegmentHeader } from "../../../../components/Control";
import { zzControlHelper } from "../../../../components/Control/zzControlHelper";
import { auth, ZenHelper } from "../../../../utils";
import '../../DashletDynamic/Dashboardnav.css'
import { GlobalStorage, KeyStorage } from "../../../../utils/storage";
import _ from "lodash";
import { RenderDashlet } from "../../DashletDynamic/RenderDashlet";
import { dashletInfo } from "../../DashLet";
import * as routes from "../../../../constants/routes"
import * as permissions from "../../../../constants/permissions"

const CustomDashboardForm = (props) => {
   const _isMounted = useRef(true);
   const refZGL = useRef();
   const [dashboard, setDashboard] = useState()
   const [availablelets, setAvailablelets] = useState([])
   const [availableletsOrdinal, setAvailableletsOrdinal] = useState([])
   const [layoutDashboard, setLayoutDashBoard] = useState({})

   function AfterLoadData(i, data) {
      if (i == 'cong_no') {
         return [data[1]]
      }
      return data
   }

   useEffect(() => {
      let id = zzControlHelper.atobUTF8(props.match.params.id)
      loadData(id)
      return () => {
         _isMounted.current = false
      }
   }, [])

   function loadData(id) {
      const result = [{}]
      ApiDashBoardTemplate.getByCode(id, res => {
         if (!_isMounted.current) return
         if (res.status === 200) {
            setDashboard(res.data.data)
            const dashlets = res.data.data.lets

            dashlets.forEach(t => {
               let strobj = t.web_layout;
               let obj = JSON.parse(strobj)
               // delete t['web_layout']
               result.push({
                  ...t,
                  x: t.x, y: t.y,
                  w: t.w, h: t.h,
                  component: obj.id_component ? dashletInfo[obj.id_component] : RenderDashlet,
                  visible: true,
                  i: t.dashlet_id,
                  title: t.ten_dashlet,
                  infodashlet: {
                     web_layout: strobj,
                     onAfterLoadData: AfterLoadData,
                  },
               })
            })

            ApiDashBoardTemplate.getAvailablelets(res.data.data.roleid, res => {
               if (res.status === 200) {
                  // Lọc phần tử bị trùng
                  var uniqueArray = res.data.data
                     .map(v => v['id'])
                     .map((v, i, array) => array.indexOf(v) === i && i)
                     .filter(v => res.data.data[v])
                     .map(v => res.data.data[v]);

                  setAvailableletsOrdinal(uniqueArray)
                  let _lets = uniqueArray
                  dashlets.find(item => {
                     _lets = removeItem(_lets, item.dashlet_id)
                  })
                  setAvailablelets(_lets)
               }
            })
         }
         setLayoutDashBoard({ lg: result })
      })
   }

   function removeItem(arr, num) {
      return arr.filter(el => el.id !== num);
   }

   const handleChangeMode = ({ name }) => {
      if (name === btnName.Save) {
         // refZGL.current.saveToLocalStorage();
         let  result = []
         refZGL.current.state.layouts.lg.forEach(t => {
            let letitem = dashboard.lets.filter(x => x.dashlet_id == t.i)
            result.push({
               ...letitem[0],
               x: t.x, y: t.y,
               w: t.w, h: t.h,
               
            })
         })
         dashboard.lets = result
         ApiDashBoardTemplate.insert(dashboard, res => {
            if (res.status === 200) {
               ZenMessageToast.success()
            }
         })

      } else if (name === btnName.Cancel) {
         refZGL.current.onResetLayout();
      }
   }

   const handleAfterSave = (item) => {
      if (item) {
         setDashboard(item)
      } else {
         loadData()
      }
   }

   const handleAddDashlet = (item) => {
      let checkhaveitem = dashboard.lets.filter(t => t.dashlet_id == item.id)
      if (checkhaveitem.length > 0) {
         ZenMessageAlert.warning("Dashlet này đã có trong dashboard")
      } else {
         if (item.web_layout) {
            let strobj = item.web_layout;
            let obj = JSON.parse(strobj)
            dashboard.lets.push({
               dashlet_id: item.id,
               // title: item.ten_dashlet,
               dashboard_id: dashboard.id,
               visible: true,
               x: item.x,
               y: item.y,
               w: item.w,
               h: item.h,
            })

            layoutDashboard.lg.push({
               x: item.x, y: item.y,
               w: item.w, h: item.h,
               component: obj.id_component ? dashletInfo[obj.id_component] : RenderDashlet,
               infodashlet: {
                  web_layout: item.web_layout,
               },
               title: item.ten_dashlet,
               visible: true,
               i: item.id
            })
            setAvailablelets(availablelets.filter(t => t.id !== item.id))
            setLayoutDashBoard({ lg: [].concat(layoutDashboard.lg) },)
            refZGL.current.onLayoutChange(null, layoutDashboard)
         } else {
            ZenMessageAlert.warning("Dashlet này chưa được khai báo web layout")
         }
         setDashboard(dashboard)
      }
   }

   const callbackFunction = (id) => {
      const newLets_Dashboarddashboard = dashboard.lets.filter(t => t.dashlet_id !== id)
      const newlayoutDashboard = layoutDashboard.lg.filter(t => t.i !== id)
      dashboard.lets = newLets_Dashboarddashboard
      setLayoutDashBoard({ lg: [].concat(newlayoutDashboard) },)
      setDashboard(dashboard)
      let availablelet = availableletsOrdinal.filter(t => t.id == id)
      setAvailablelets(availablelets.concat(availablelet))
   }


   return layoutDashboard.lg?.length > 0 ? <>
      <DropDownSetting
         onChangeMode={handleChangeMode}
         dashboard={dashboard}
         availablelets={availablelets}
         handleAfterSave={handleAfterSave}
         handleAddDashlet={handleAddDashlet}
         layoutDashboard={layoutDashboard}
      >
         {
            ({ isView }) => <ZenGridLayout ref={refZGL}
               keyLS="home_dashboard"
               useLocalStorage={false}
               isView={isView}
               initLayout={layoutDashboard}
               parentCallback={callbackFunction}
            />
         }
      </DropDownSetting>
   </> : <></>
};

const PageHeader = ({ item }) => {
   return <>
      <Helmet idMessage={"hrhsns.detail"}
         defaultMessage={"Danh mục loại bảng lương"} />

      <SegmentHeader style={{width:"auto"}}>
         <HeaderLink listHeader={[{
            id: "padmloaibangluong",
            defaultMessage: "Dashboard mẫu",
            route: routes.SysDashBoardTemplate,
            active: false,
            isSetting: false,
         }]}>
            <Breadcrumb.Divider icon='right chevron' />
            <Breadcrumb.Section active>
               {`${item?.title} - ${item?.rolename}`}
            </Breadcrumb.Section>
         </HeaderLink>
      </SegmentHeader>
      <br />
   </>
}


const DropDownSetting = ({ children, onChangeMode, dashboard, dashboardOriginal, handleAfterSave, availablelets, handleAddDashlet, layoutDashboard }) => {
   const [isView, setIsView] = useState(true);
   const [visible, setVisible] = useState(false)
   const [openDashboardInfo, setOpenDashboardInfo] = useState(false)
   const [openRoleInfo, setOpenRoleInfo] = useState(false)
   const handleSetting = (e, { name }) => {
      setIsView(!isView)
      onChangeMode({ name: name })
   }

   const handleShowMenu = () => {
      const $ = document.querySelector.bind(document)
      const $$ = document.querySelectorAll.bind(document)
      var x = document.getElementById('dshbrd-menu-hd')
      var y = document.getElementById('dshbrd-nav-overlay')
      x.style.right = '0%'
      y.style.right = '0%'
   }
   const handleHiddenMenu = () => {
      var x = document.getElementById('dshbrd-menu-hd')
      var y = document.getElementById('dshbrd-nav-overlay')
      x.style.right = '-100%'
      y.style.right = '-100%'
   }
   return <>
      <div style={{ padding: "0 10px 14px 10px", display: 'flex', justifyContent: 'space-between' }}>
         {/* <div style={{ display: "flex" }}>
            <span style={styles.fontMenu}>{dashboard?.title}</span>
            {!isView && <div className="dshbrd-edit-info">
               <Icon size="large" name="pencil" onClick={(e) => setOpenDashboardInfo(true)} />
            </div>}
         </div> */}
         <PageHeader item={dashboard} />
         <div style={{ display: 'flex', }}>

            {/* <Button.Group size="mini" floated="right"> */}
            {isView && <Button icon="setting" basic size="mini"
               primary type="button"
               onClick={handleSetting} name={btnName.Edit}
            />}
            {!isView && <>
               <ZenButton icon="close" basic size="mini"
                  primary type="button"
                  content="Bỏ qua"
                  onClick={handleSetting} name={btnName.Cancel}
               />
               <ZenButton icon="check" size="mini"
                  primary
                  content="Lưu"
                  onClick={handleSetting} name={btnName.Save}
               />
            </>}
            {/* </Button.Group> */}
            {!isView && <Button icon="plus" size="mini"
               primary type="button"
               onClick={(e) => handleShowMenu()} name={btnName.Add}
            />}
         </div>
      </div>
      {children({ isView: isView })}
      <div className="dshbrd-hd-bgtrang">
         <div className="dshbrd-nav-overlay" id="dshbrd-nav-overlay" onClick={(e) => handleHiddenMenu()}>

         </div>
         <div className="dshbrd-container" id="dshbrd-menu-hd">
            <div className="dshbrd-inner-wrapper-hd">
               <div style={{ display: "flex" }}>
                  <div className="dshbrd-btn-close">
                     <Icon name="close" onClick={(e) => handleHiddenMenu()} />
                  </div>
                  <div><span className="dshbrd-title-header-nav">Thêm thành phần cho dashboard</span></div>
               </div>
               {
                  availablelets && availablelets.map((item, index) => {
                     return <div className="dshbrd-sys-dashboard">
                        <div className={"dshbrd-item"} style={{ height: 'auto', display: "flex", justifyContent: "space-between" }}>
                           <div className="dshbrd-title-card-nav">
                              {item.ten_dashlet}
                              <div>
                                 <i className="PADMLBL-mota-km">{item.ghi_chu}</i>
                              </div>
                           </div>
                           <div className="dshbrd-btn-close">
                              <Icon name="plus" onClick={(e) => handleAddDashlet(item)} />
                           </div>
                        </div>
                     </div>
                  })
               }
               {
                  availablelets.length == 0 && <>
                     <div className="dshbrd-sys-dashboard">
                        <div className={"dshbrd-item"} style={{ height: 'auto', display: "flex", justifyContent: "space-between" }}>
                           <div className="dshbrd-title-card-nav">
                              {"Không có dashlet nào !"}
                              <div>
                                 <i className="PADMLBL-mota-km">{"Dashlet có thể đã được thêm hết vào dashboard hoặc bạn chưa được cấp quyền sử dụng"}</i>
                              </div>
                           </div>
                        </div>
                     </div>
                  </>
               }
            </div>
         </div>
      </div>
      {openDashboardInfo && <ModalDashboardInfo
         open={openDashboardInfo}
         item={dashboard}
         onAfterSave={handleAfterSave}
         onClose={() => setOpenDashboardInfo(false)}
      />}
      {openRoleInfo && <ModalRoleInfo
         open={openRoleInfo}
         item={dashboard}
         onAfterSave={handleAfterSave}
         onClose={() => setOpenRoleInfo(false)}
      />}
   </>
}

const ModalDashboardInfo = ({ open, id, idnv, onClose, onAfterSave, item }) => {
   const _isMounted = useRef(true);
   const refFormik = useRef();
   const [error, setError] = useState();
   const [roles, setRoles] = useState([]);
   const [btnLoading, setBtnLoading] = useState();

   useEffect(() => {
      zzControlHelper.dragElement(document.getElementById('dashboardinfo-form'), 'header-dashboardinfo-form')
   })

   // useEffect(() => {

   // }, [])


   const handleSubmit = (params, formId) => {
      setBtnLoading(true)
      ApiDashBoardTemplate.insert({ ...item, ...params }, res => {
         if (res.status === 200) {
            ZenMessageToast.success()
            { onAfterSave && onAfterSave({ ...item, ...params }) }
         } else {
            setError(ZenHelper.getResponseError(res))
         }
         onClose()
         if (btnLoading) setBtnLoading()
      })
   }

   return <>
      <Modal id="dashboardinfo-form"
         closeOnEscape closeIcon closeOnDimmerClick={false}
         onClose={() => onClose()}
         //onOpen={() => onClose()}
         open={open}
         size={"tiny"}
      >
         <Modal.Header id='header-dashboardinfo-form' style={{ cursor: "grabbing" }}>
            Thông tin dashboard
         </Modal.Header>
         <Modal.Content>
            <ZenFormik form={"hrdmother"} ref={refFormik}
               validation={formValidation}
               initItem={{ ...initItem, title: item.title, description: item.description }}
               onSubmit={handleSubmit}
            >
               {
                  formik => {
                     return <Form>
                        <Form.Group>
                           <ZenField width={16} name="title" props={formik}
                              label="hdld.title" defaultlabel="Title"
                           />
                        </Form.Group>
                        <Form.Group>
                           <ZenFieldTextArea width={16} name="description" props={formik}
                              label="hdld.description" defaultlabel="Description"
                           />
                        </Form.Group>
                     </Form>
                  }
               }
            </ZenFormik>
         </Modal.Content>
         <Modal.Actions>
            <ZenButton btnType={"cancel"} size="small" onClick={() => onClose()} />
            <ZenButton btnType="save" size="small"
               loading={btnLoading} type="submit"
               onClick={(e) => refFormik.current.handleSubmit(e)}
            />
         </Modal.Actions>
      </Modal>
   </>
}

const formValidation = []
const initItem = {
   title: "",
   description: "",
}

const ModalRoleInfo = ({ open, id, idnv, onClose, onAfterSave, item }) => {
   const _isMounted = useRef(true);
   const refFormik = useRef();
   const [error, setError] = useState();
   const [roles, setRoles] = useState([]);
   const [btnLoading, setBtnLoading] = useState();

   useEffect(() => {
      zzControlHelper.dragElement(document.getElementById('dashboardinfo-form'), 'header-dashboardinfo-form')
   })

   function loadRole() {
      return new Promise((resolve, reject) => {
         ApiRole.get(res => {
            if (res.status === 200) {
               resolve(ZenHelper.translateListToSelectOptions(res.data.data, false))
            } else {
               reject(res)
            }
         })
      })
   }

   useEffect(() => {
      const roles = loadRole();
      Promise.all([roles])
         .then(values => {
            setRoles(values[0])
            setError(false)
         }).catch(err => {
            setError(ZenHelper.getResponseError(err))
         });
   }, [])


   const handleSubmit = (params, formId) => {
      setBtnLoading(true)
      ApiDashboard.insert({ ...item, ...params }, res => {
         if (res.status === 200) {
            ZenMessageToast.success()
            { onAfterSave && onAfterSave() }
         } else {
            setError(ZenHelper.getResponseError(res))
         }
         onClose()
         if (btnLoading) setBtnLoading()
      })
   }

   return <>
      <Modal id="dashboardinfo-form"
         closeOnEscape closeIcon closeOnDimmerClick={false}
         onClose={() => onClose()}
         open={open}
         size={"tiny"}
      >
         <Modal.Header id='header-dashboardinfo-form' style={{ cursor: "grabbing" }}>
            Chọn vai trò bạn muốn lưu template
         </Modal.Header>
         <Modal.Content>
            <ZenFormik form={"hrdmother"} ref={refFormik}
               validation={formValidationRole}
               initItem={{ ...initItemRole }}
               onSubmit={handleSubmit}
            >
               {
                  formik => {
                     return <Form>
                        <Form.Group>
                           <ZenFieldSelect width={16} required
                              options={roles}
                              name="roleid"
                              label={"role.name"} defaultlabel="Vai trò"
                              props={formik}
                           />
                        </Form.Group>
                     </Form>
                  }
               }
            </ZenFormik>
         </Modal.Content>
         <Modal.Actions>
            <ZenButton btnType={"cancel"} size="small" onClick={() => onClose()} />
            <ZenButton content="Xác nhận" icon="checkmark"
               size="small" primary
               loading={btnLoading} type="submit"
               onClick={(e) => refFormik.current.handleSubmit(e)}
            />
         </Modal.Actions>
      </Modal>
   </>
}

const formValidationRole = [
   {
      id: "roleid",
      validationType: "string",
      validations: [
         {
            type: "required",
            params: ["Không được bỏ trống trường này"]
         },
      ]
   }
]
const initItemRole = {
   istemplate: true,
   roleid: "",
}

const styles = {
   fontMenu: {
      fontWeight: "bold",
      fontSize: "18px",
   }
}

const btnName = {
   Add: "add",
   Save: "save",
   Cancel: "cancel",
   Edit: "edit"
}

export const CustomDashboard = {
   route: routes.SysDashBoardTemplateCustom(),
   Zenform: CustomDashboardForm,
   action: {
      view: { visible: true, permission: permissions.DashBoardTemplate },
   },
}

