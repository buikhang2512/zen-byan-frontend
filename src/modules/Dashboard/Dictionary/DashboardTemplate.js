import React, { useCallback, useEffect, useState } from "react";
import { Form } from "semantic-ui-react";
import { ZenField, ZenFieldDate, ZenFieldNumber, ZenFieldSelectApi, ZenFieldTextArea, ZenButton, ZenFieldCheckbox } from "../../../components/Control/index";
import { FormMode, zzControlHelper } from "../../../components/Control/zzControlHelper";
import { ZenHelper } from "../../../utils";
import { IntlFormat } from "../../../utils/intlFormat";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { ApiDashBoardTemplate } from "../../../Api/index";
import * as routes from '../../../constants/routes';
import * as permissions from '../../../constants/permissions';

const DashBoardTemplateModal = (props) => {
    const { formik, permission, mode } = props
    return <>
        <ZenField required readOnly={!permission}
            label="DashBoardTemplate.title" defaultlabel="Title"
            name="title"
            props={formik}
        />
        <ZenField readOnly={!permission} width={16}
            label="DashBoardTemplate.description" defaultlabel="Description"
            name="description"
            props={formik}
        />
    </>
}

export const DashBoardTemplate = {
    FormModal: DashBoardTemplateModal,

    api: {
        url: ApiDashBoardTemplate,
    },

    permission: {
        view: permissions.DashBoardTemplate,
        add: permissions.DashBoardTemplate,
        edit: permissions.DashBoardTemplate,
    },
    fieldCode: "id",
    formId: "DashBoardTemplate-form",
    size: "small",
    initItem: {
        id: 0,
        ksd: true,
        username: "",
        title: "",
        description: "",
        iswin: true,
        isweb: true,
        ismobile: true,
        istemplate: true,
        roleid: 0,
        lets: [
          {
            id: 0,
            dashboard_id: 0,
            dashlet_id: "",
            title: "",
            x: 0,
            y: 0,
            w: 0,
            h: 0
          }
        ]
    },
    formValidation: [
        {
            id: "title",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ]
        },
    ]
}