import { PARptThChotCong } from "./PARptThChotCong"
import { PARptThChotCongdrilldown } from "./PARptThChotCong_DrillDown"

export const PA_Report = {
    PARptThChotCong: PARptThChotCong,
    PARptThChotCongdrilldown: PARptThChotCongdrilldown,
}