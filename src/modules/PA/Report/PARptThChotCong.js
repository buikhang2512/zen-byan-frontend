import React from "react";
import { Table, Form } from "semantic-ui-react";
import { ZenFieldSelectApi, ZenDatePeriod, ZenFieldNumber, ZenFieldCheckbox, ZenFieldDate, ZenFieldSelect } from "../../../components/Control";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { RptHeader, RptTable, RptTableCell, RptTableRow } from "../../../components/Control/zReport";
import * as routes from "../../../constants/routes";

const FilterForm = ({ formik }) => {
   const handleItemSelected = (item, { name }) => {
      if (name === "tk") {
         formik.setFieldValue("ten_tk", item.ten_tk);
      }
   };
   return <>
      <ZenFieldSelect
         options={[
            { key: 0, value: "NV", text: "Giáo viên" },
            { key: 1, value: "KH", text: "Khách hàng" },
         ]}
         props={formik}
         name="nhom"
         label="hrlichday.type" defaultlabel="Nhóm"
         onSelectedItem={handleItemSelected}
         clearable={false}
      />
      {formik.values.nhom === "KH" && <ZenFieldSelectApi
         lookup={{ ...ZenLookup.Ma_kh, format: `{ten_kh}` }}
         label={"rpt.loai_tai_lieu"} defaultlabel="Khách hàng"
         name="ma_kh" formik={formik}
         onItemSelected={handleItemSelected}
      />}
      {formik.values.nhom === "NV" && <ZenFieldSelectApi
         lookup={{ ...ZenLookup.ID_NV, format: `{ho_ten}` }}
         label={"rpt.loai_tai_lieu"} defaultlabel="Giáo viên"
         name="id_nv" formik={formik}
         onItemSelected={handleItemSelected}
      />}
   </>
}

const TableForm = ({ data = [], filter = {} }) => {
   return <>
      <RptTable maNt={filter.Ma_Nt}>
         <Table.Header fullWidth>
            <RptHeader maNt={filter.Ma_Nt}
               header={[
                  { text: ["rpt.ma", "Mã"] },
                  { text: ["rpt.ten", "Tên"] },
                  { text: ["rpt.so_luong", "Số lượng"] },
                  { text: ["rpt.gia_tri", "Giá trị"] },
                  { text: ["rpt.so_luong_chu_chot", "Số lượng chưa chốt"] },
               ]}
            />
         </Table.Header>
         <Table.Body>
            {data.length > 0 ? data.map((item, index) => {
               const isNt = filter.Ma_Nt && filter.Ma_Nt !== 'VND' ? true : false
               return <RptTableRow key={index} isBold={item.Bold} index={index} item={item}>
                  <RptTableCell value={item.ma} />
                  <RptTableCell value={item.ten} />
                  <RptTableCell value={item.so_luong} type="number" />
                  <RptTableCell value={item.gia_tri} type="number" />
                  <RptTableCell value={item.so_luong_chua_chot} type="number" />
               </RptTableRow>
            }) : undefined}
         </Table.Body>
      </RptTable>
   </>
}

export const PARptThChotCong = {
   FilterForm: FilterForm,
   TableForm: TableForm,
   permission: "",
   visible: true,    // hiện/ẩn báo cáo
   route: routes.PARptTHChotCong,

   period: {
      fromDate: "ngay1",
      toDate: "ngay2"
   },

   linkHeader: {
      id: "PARptThChotCong",
      defaultMessage: "Báo cáo Tổng hợp công giáo viên",
      active: true
   },

   info: {
      code: "THCGV",
   },
   initFilter: {
      nhom: "KH",
      ma_kh: "",
      id_nv: "",
   },
   columns: [
      { id: "PARptThChotCong.id", defaultMessage: "Mã", fieldName: "ma", type: "string", sorter: true, },
      { id: "PARptThChotCong.ho_ten", defaultMessage: "Tên", fieldName: "ten", type: "string", sorter: true },
      { id: "PARptThChotCong.so_luong", defaultMessage: "Số lượng", fieldName: "so_luong", type: "number", sorter: true, },
      { id: "PARptThChotCong.gia_tri", defaultMessage: "Giá trị", fieldName: "gia_tri", type: "number", sorter: true, },
      { id: "PARptThChotCong.so_luong_chua_chot", defaultMessage: "Số lượng (chưa chốt)", fieldName: "so_luong_chua_chot", type: "number", sorter: true, },
   ],
   formValidation: []
}
