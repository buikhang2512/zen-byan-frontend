
import axios from '../../../Api/axios';

const ExtName = "PA1"

export const ApiPAVchPA1 = {
    get(callback, filter = {}, pagination = {}) {
        axios.get(`${ExtName}`, {
            params: {
                qf: filter.qf,
                keyword: filter.keyword,
                sort: filter.sort,
                page: pagination.page,
                pageSize: pagination.pageSize
            },

        })
            .then(res => {
                callback(res)
            })
            .catch(err => {
                callback(err)
            });
    },

    getByCode(stt_rec,callback) {
        axios.get(`${ExtName}/${stt_rec}`)
            .then(res => {
                callback(res)
            })
            .catch(err => {
                callback(err)
            });
    },

    insert(data, callback) {
        axios.post(`${ExtName}`, data)
            .then(res => {
                callback(res)
            })
            .catch(err => {
                callback(err)
            });
    },

    update(data, callback) {
        axios.put(`${ExtName}`, data)
            .then(res => {
                callback(res)
            })
            .catch(err => {
                callback(err)
            });
    },

    delete(stt_rec, callback) {
        axios.delete(`${ExtName}/${stt_rec}`)
            .then(res => {
                callback(res)
            })
            .catch(err => {
                callback(err)
            });
    },

    report(stt_rec,qf, callback) {
        axios.get(`${ExtName}/${stt_rec}/rpt`, {
            params: {
                // report_id trong bảng z00Report
                codeReport: qf.codeReport,
                // tham số trong stored
                parameters: qf.params,
            },
        })
        .then(res => {
            callback(res)
        })
        .catch(err => {
                callback(err)
            });
    },

    tonghop(data, callback) {
        axios.post(`${ExtName}/tonghop`, data)
            .then(res => {
                callback(res)
            })
            .catch(err => {
                callback(err)
            });
    },

    tonghoplist(data, callback) {
        axios.post(`${ExtName}/tonghoplist`, data)
            .then(res => {
                callback(res)
            })
            .catch(err => {
                callback(err)
            });
    },

    trangthai(data, callback) {
        axios.post(`${ExtName}/trangthai`, data)
            .then(res => {
                callback(res)
            })
            .catch(err => {
                callback(err)
            });
    },

    trangthailist(data, callback) {
        axios.post(`${ExtName}/trangthailist`, data)
            .then(res => {
                callback(res)
            })
            .catch(err => {
                callback(err)
            });
    },
}