import axios from '../../../Api/axios';

const ExtName = "PAHangSo"

export const ApiPAHangSo = {
   get(callback, filter = {}, pagination = {}) {
      axios.get(`${ExtName}`, {
         params: {
            qf: filter.qf,
            keyword: filter.keyword,
            sort: filter.sort,
            page: pagination.page,
            pageSize: pagination.pageSize
         },
      })
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   getByCode(code, callback, params) {
      // K có api get single -- Fake tạm
      callback({
         status: 200,
         data: {
            data: {
               ...params,
            }
         }
      })
      // axios.get(`${ExtName}/`,{ params: { ...params } })
      //    .then(res => {
      //       callback(res)
      //    })
      //    .catch(err => {
      //       callback(err)
      //    });
   },

   getHangSo(params, callback) {
      axios.get(`${ExtName}/${params?.ma_ky_luong}`, {
         params: {
            //ma_ky_luong: params?.ma_ky_luong,
            id_nv: params?.id_nv,
         },
      })
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   insert(data, callback) {
      axios.post(`${ExtName}`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   update(data, callback) {
      axios.post(`${ExtName}`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   updatePatch(code, data, callback) {
      axios.patch(`${ExtName}/${code}`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   delete(code, callback, params) {
      axios.delete(`${ExtName}/`, { data: { ...params } })
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   duplicate(data, callback) {
      axios.post(`${ExtName}/duplicate`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   }
}