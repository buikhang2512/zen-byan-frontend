import axios from '../../../Api/axios';

const ExtName = "PABangLuong"

export const ApiPABangLuong = {
   get(callback, filter = {}, pagination = {}) {
      axios.get(`${ExtName}`, {
         params: {
            qf: filter.qf,
            keyword: filter.keyword,
            sort: filter.sort,
            page: pagination.page,
            pageSize: pagination.pageSize
         },
      })
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   getByCode(code, callback) {
      axios.get(`${ExtName}/` + code)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   insert(data, callback) {
      axios.post(`${ExtName}`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   update(data, callback) {
      axios.put(`${ExtName}`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   updatePatch(code, data, callback) {
      axios.patch(`${ExtName}/${code}`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   delete(code, callback) {
      axios.delete(`${ExtName}/` + code)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   tinhluong(data, callback) {
      axios.post(`${ExtName}/tinhluong`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   duyetbangluong(data, callback) {
      // id,trang_thai
      axios.patch(`${ExtName}/duyet`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   export(code, params, callback) {
      axios.get(`${ExtName}/export/${code}`, {
         params: {
            type: params.type,
            template: params.template, 
         },
         responseType: 'blob'
      })
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   }
}