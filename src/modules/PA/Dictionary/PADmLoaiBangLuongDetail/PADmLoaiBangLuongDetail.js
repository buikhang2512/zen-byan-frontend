import React, { createRef, memo, useEffect, useMemo, useRef, useState, } from "react";
import {
    Dimmer,
    Grid,
    Icon,
    Loader,
    Menu,
    Message,
    Input,
    GridColumn,
    Segment,
    Image,
    Header,
    Divider,
    Form,
    Button,
    Popup,
    Table,
    Breadcrumb,
    Modal,
    TextArea,
} from "semantic-ui-react";
import * as routes from "../../../../constants/routes";
import { ContainerScroll, ScrollManager, ZenButton, ZenField, ZenFieldCheckbox, ZenFieldSelectApi, ZenFormik, ZenMessageAlert, ZenMessageToast, ZenModal, ZenFieldSelect, Helmet, SegmentHeader, HeaderLink, FormatDate, ZenFieldTextArea } from "../../../../components/Control";
import { ZenHelper } from '../../../../utils';
import { ApiPADmLoaiBangLuong } from "../../Api";
import { zzControlHelper } from "../../../../components/Control/zzControlHelper";
import './PADMLBL.css'
import { Formik } from "formik"; import _ from "lodash";
import { ZenLookup } from "../../../ComponentInfo/Dictionary/ZenLookup";
import { Apihrdmkhac } from "../../../HrDmOther/Api";
import { height } from "dom-helpers";

const PADmLoaiBangLuongDetail_form = memo((props) => {
    const [isLoading, setIsloading] = useState(false);
    const [data, setData] = useState();
    const [cts, setCts] = useState([]);
    const [CtsOriginal, setCtsOriginal] = useState([]);
    const [error, setError] = useState();
    const [imgInfo, setImgInfo] = useState();
    const [activeItem, setActiveitem] = useState('ttc');
    const [open, setOpen] = useState(false)
    const [accout, setAccout] = useState();
    const [readonly, setReadonly] = useState(true);
    const refForm = useRef();
    const [btnLoading, setBtnLoading] = useState();
    const newItem = {
        ma_loai_bang_luong: data?.ma_loai_bang_luong,
        ma_km: "",
        ten_km: "",
        mo_ta: "",
        cach_tinh: "",
        cong_thuc: "",
        gia_tri: 0
    };

    function fetchRequestByCode() {
        const id = zzControlHelper.atobUTF8(props.match.params.id)
        var result = new Promise((resolve, reject) =>
            ApiPADmLoaiBangLuong.getByCode(id, res => {
                if (res.status >= 200 && 204 >= res.status) {
                    resolve(res.data.data)
                } else {
                    reject(res)
                }
            })
        )
        return result
    }

    useEffect(() => {
        fetchRequestByCode().then(res => {
            setData(res)
            setCtsOriginal(_.cloneDeep(res.cts))
            setCts(_.cloneDeep(res.cts))
        }).catch(err => {
            setError(ZenHelper.getResponseError(err))
        })

    }, [])

    const handleAddKM = () => {
        //const { cts } = refForm.current.values
        cts.push({ ..._.cloneDeep(newItem), ordinal: cts.length })
        setCts([].concat(cts))
        // refForm.current.setValues({
        //     ...refForm.current.values,
        // })
    }

    const handleRemoveKM = (item, idx) => {
        _.remove(item, (i, x) => { return x == idx });
        setCts([].concat(item))
        // refForm.current.setValues({
        //     ...refForm.current.values,
        //     cts: item,
        // })
    }

    const onRefreshForm = () => {
        // refForm.current.setValues({
        //     ...refForm.current.values,
        //     cts: CtsOriginal,
        // })
        setCts(CtsOriginal)
        setCtsOriginal(_.cloneDeep(CtsOriginal))
        setReadonly(true)
    }

    const handleChangeKM = (item, idx, e, dt) => {
        item[idx][dt.name] = dt.value;
        setCts([].concat(item))
    }

    const handleSort = (dispatch) => {
        const { oldName, index, action } = dispatch
        if (action === "MOVE_UP") {
            if (index > 0) {
                const newCts = zzControlHelper.arrayMoveElement(cts, index, index - 1)
                newCts[index - 1]['ordinal'] = index - 1
                newCts[index]['ordinal'] = index
                setCts([].concat(newCts))
            }
        } else if (action === "MOVE_DOWN") {
            if (index < cts.length - 1) {
                const newCts = zzControlHelper.arrayMoveElement(cts, index, index + 1)
                newCts[index + 1]['ordinal'] = index + 1
                newCts[index]['ordinal'] = index
                setCts([].concat(newCts))
            }
        }
        setReadonly(false)
    }

    const handleSubmit = (params) => {
        let result = true
        let chk_makm = false
        let chk_tenkm = false
        let chk_cachtinh = false
        if (params.cts) {
            if (cts.length > 0) {
                const makm = params.cts.filter(function (i) { return i.ma_km == null || i.ma_km == "" })
                const tenkm = params.cts.filter(function (i) { return i.ten_km == null || i.ten_km == "" })
                const cachtinh = params.cts.filter(function (i) { return i.cach_tinh == null || i.cach_tinh == "" })

                if (makm.length > 0) {
                    chk_makm = true
                } else { chk_makm = false }
                if (tenkm.length > 0) {
                    chk_tenkm = true
                } else { chk_tenkm = false }
                if (cachtinh.length > 0) {
                    chk_cachtinh = true
                } else { chk_cachtinh = false }
                if (chk_makm == true || chk_tenkm == true || chk_cachtinh == true) {
                    result = false
                } else {
                    result = true
                }
            } else {
                ZenMessageAlert.warning('Danh sách khoản mục lương')
                return
            }
        } else {
            ZenMessageAlert.warning('Danh sách khoản mục lương')
            return
        }

        if (result) {
            setBtnLoading(true)
            ApiPADmLoaiBangLuong.update({ ...params, cts: cts }, res => {
                if (res.status >= 200 && 204 >= res.status) {
                    ZenMessageToast.success();
                    setReadonly(true)
                } else {
                    setError(zzControlHelper.getResponseError(res))
                    ZenMessageAlert.error(zzControlHelper.getResponseError(res))
                }
                setBtnLoading(false)
            })
        } else {
            if (chk_makm) {
                ZenMessageAlert.warning('Mã khoản mục không được để trống')
                return
            }
            if (chk_tenkm) {
                ZenMessageAlert.warning('tên khoản mục không được để trống')
                return
            }
            if (chk_cachtinh) {
                ZenMessageAlert.warning('Cách tính không được để trống')
                return
            }
        }
    }

    const onValidCT = (data, ct, currentRowCT) => {
        const { item, index } = currentRowCT
        const errorField = {}

        if (!item.ma_km) {
            errorField.ma_km = ["Mã khoản mục không được để trống"]
        }
        if (!item.ten_km) {
            errorField.ten_km = ["Tên khoản mục không được để trống"]
        }
        if (!item.cach_tinh) {
            errorField.cach_tinh = ["Cách tính không được để trống"]
        }
        return Object.keys(errorField).length > 0 ? errorField : undefined
    }

    const onValidate = (item) => {
        const errors = []

        return errors
    }

    function getNameCongthuc(value) {
        var _switch = {
            1: 'Từ hệ thống',
            2: 'Công thức',
            3: 'Hằng số',
        };
        return _switch[value]
    }

    return <>
        <PageHeader item={data} />
        <Segment>
            <div className="PADMLBL-group">
                <div className="PADMLBL-field">
                    <label>Mã loại :</label>
                    <label>{data?.ma_loai_bang_luong}</label>
                </div>
                <div className="PADMLBL-field">
                    <label>Tên loại :</label>
                    <label>{data?.ten_loai_bang_luong}</label>
                </div>
                <div className="PADMLBL-field">
                    <label>Bộ phận áp dụng :</label>
                    <label>{data?.id_cctcs}</label>
                </div>
            </div>
            <Header content="Danh sách khoản mục lương" size="medium" />
            <ZenFormik form={"padmloaibangluong"}
                ref={refForm}
                initItem={data}
                onSubmit={handleSubmit}
            //onValidate={onValidate}
            >
                {/* <Formik
                innerRef={refForm}
                initialValues={{}}
                onSubmit={(values, action) => {
                    handleSubmit(values, action)
                }}
            > */}
                {
                    formik => {
                        return <Form>
                            <Form.Group>
                                <Form.Field width={2}>
                                    <label>Mã Khoản mục</label>
                                </Form.Field>
                                <Form.Field width={3}>
                                    <label>Tên Khoản mục</label>
                                </Form.Field>
                                <Form.Field width={2}>
                                    <label>Cách tính</label>
                                </Form.Field>
                                <Form.Field width={3}>
                                    <label>Công thức tính/giá trị</label>
                                </Form.Field>
                                <Form.Field width={4}>
                                    <label>Mô tả thêm</label>
                                </Form.Field>
                            </Form.Group>
                            {
                                cts && cts.map((item, index) => {
                                    return <Form.Group key={`loaibangluong${index}`}>
                                        <Form.Field width={2}>
                                            <Form.Input readOnly={readonly}
                                                placeholder='Mã khoản mục'
                                                name='ma_km'
                                                value={item['ma_km']}
                                                onChange={(e, data) => {
                                                    const { value } = data
                                                    var regex = new RegExp(`[~!@#$%^&*();:'"|/?><,]`, "gm");
                                                    data.value = value.replace(regex, "");
                                                    handleChangeKM(cts, index, e, data)
                                                }}
                                            />
                                        </Form.Field>
                                        <Form.Field width={3}>
                                            <Form.Input readOnly={readonly}
                                                placeholder='Tên khoản mục'
                                                name='ten_km'
                                                value={item['ten_km']}
                                                onChange={(e, data) => handleChangeKM(cts, index, e, data)}
                                            />
                                        </Form.Field>
                                        <div>
                                            <Form.Field width={2}>
                                                <Form.Select
                                                    open={readonly ? false : undefined}
                                                    placeholder='Cách tính'
                                                    options={[
                                                        { key: '1', value: '1', text: "Từ hệ thống" },
                                                        { key: '2', value: '2', text: "Công thức" },
                                                        { key: '3', value: '3', text: "Hằng số" },
                                                    ]}
                                                    name='cach_tinh'
                                                    value={item['cach_tinh']}
                                                    onChange={(e, data) => handleChangeKM(cts, index, e, data)}
                                                />
                                                {/* <ErrorPopup error={formik.errors} name={`cach_tinh`} /> */}
                                            </Form.Field>
                                        </div>
                                        <Form.Field width={3}>
                                            {cts[index].cach_tinh == '3' &&
                                                <Popup trigger={
                                                    <Form.Input readOnly={readonly}
                                                        type="number"
                                                        placeholder='Công thức tính/giá trị'
                                                        name='gia_tri'
                                                        value={item['gia_tri'] ? item['gia_tri'] : null}
                                                        onKeyPress={(event) => {
                                                            if (!/[0-9]/.test(event.key)) {
                                                                event.preventDefault();
                                                            }
                                                        }}
                                                        onChange={(e, data) => handleChangeKM(cts, index, e, data)}
                                                    />
                                                }>
                                                    {cts[index].cach_tinh ? (<>
                                                        Cách tính :  {getNameCongthuc(cts[index].cach_tinh)} < br />
                                                        {cts[index].cach_tinh == '3' ? `Giá trị : ${cts[index].gia_tri}` : `Công thức : ${cts[index].cong_thuc}`}
                                                    </>) : <>Mã khoản mục : {cts[index].ma_km}</>
                                                    }
                                                </Popup>
                                            }
                                            {cts[index].cach_tinh == '2' &&

                                                <div className="field">
                                                    <div style={{ display: "flex" }}>
                                                        <Popup trigger={
                                                            <div style={{ width: "100%" }}>
                                                                <Form.Input readOnly={readonly}
                                                                    placeholder='Công thức tính/giá trị'
                                                                    name='cong_thuc'
                                                                    value={item['cong_thuc']}
                                                                    onChange={(e, data) => handleChangeKM(cts, index, e, data)}
                                                                />
                                                            </div>}>
                                                            {cts[index].cach_tinh ? (<>
                                                                Cách tính :  {getNameCongthuc(cts[index].cach_tinh)} < br />
                                                                {cts[index].cach_tinh == '3' ? `Giá trị : ${cts[index].gia_tri}` : `Công thức : ${cts[index].cong_thuc}`}
                                                            </>) : <>Mã khoản mục : {cts[index].ma_km}</>
                                                            }
                                                        </Popup>
                                                        <ModalThietlapCongthuc readOnly={readonly} index={index} item={item} cts={cts} handleChange={handleChangeKM} />
                                                    </div>
                                                </div>

                                            }
                                            {cts[index].cach_tinh == '1' &&
                                                <Popup key={index + "lookup"} trigger={
                                                    <ZenFieldSelectApi readOnly={readonly}
                                                        lookup={{ ...ZenLookup.Ma_Km_luong, format: "{ten}" }}
                                                        name="cong_thuc"
                                                        value={item['cong_thuc']}
                                                        onChange={(e, data) => { handleChangeKM(cts, index, null, { value: data.value, name: data.name }) }}
                                                        placeholder="Công thức tính/giá trị"
                                                    />}>
                                                    {cts[index].cach_tinh ? (<>
                                                        Cách tính :  {getNameCongthuc(cts[index].cach_tinh)} < br />
                                                        {cts[index].cach_tinh == '3' ? `Giá trị : ${cts[index].gia_tri}` : `Công thức : ${cts[index].cong_thuc}`}
                                                    </>) : <>Mã khoản mục : {cts[index].ma_km}</>
                                                    }
                                                </Popup>
                                            }
                                        </Form.Field>

                                        <Form.Field width={3}>
                                            <Form.Input readOnly={readonly}
                                                placeholder='Mô tả thêm'
                                                name='mo_ta'
                                                value={item['mo_ta']}
                                                onChange={(e, data) => handleChangeKM(cts, index, e, data)}
                                            />
                                        </Form.Field>

                                        <Form.Field>
                                            <Button icon='trash' color='red' basic
                                                disabled={readonly}
                                                onClick={() => {
                                                    handleRemoveKM(cts, index);
                                                }}
                                            />
                                        </Form.Field>
                                        <Form.Field>
                                            <Button icon={"caret square up outline"}
                                                basic
                                                primary
                                                onClick={() => handleSort({ action: "MOVE_UP", index: index })}
                                                disabled={index === 0 ? true : false}
                                            />
                                        </Form.Field>
                                        <Form.Field>
                                            <Button icon={"caret square down outline"}
                                                basic
                                                primary
                                                onClick={() => handleSort({ action: "MOVE_DOWN", index: index })}
                                                disabled={index === cts.length - 1 ? true : false}
                                            />
                                        </Form.Field>
                                    </Form.Group>
                                })
                            }
                            {!readonly && <a href="#" onClick={(e) => {
                                e.preventDefault();
                                handleAddKM();
                            }}>
                                <Icon style={{ marginBottom: "20px" }} name="add" />
                                Thêm khoản mục
                            </a>}
                        </Form>
                    }
                }
            </ZenFormik>
            {/* </Formik> */}
            <div className="btn-form-right">
                {!readonly ?
                    <div style={{ position: "relative", display: "inline-block" }}>
                        <ZenButton btnType="cancel"
                            size="small"
                            content="Hủy"
                            color="red"
                            onClick={() => onRefreshForm()}
                        />
                        <ZenButton btnType="save"
                            size="small"
                            type="submit"
                            onClick={(e) => refForm.current.handleSubmit(e)}
                            style={{ margin: "0px" }}
                            loading={btnLoading}
                            disabled={btnLoading}
                        />
                    </div>
                    : <Button style={{ margin: 0 }}
                        type={"button"}
                        size="small"
                        primary
                        icon="edit"
                        content={"Sửa danh sách khoản mục"}
                        onClick={() => setReadonly(false)}
                    />
                }
            </div>
        </Segment>
    </>
})

const PageHeader = ({ item }) => {
    return <>
        <Helmet idMessage={"hrhsns.detail"}
            defaultMessage={"Danh mục loại bảng lương"} />

        <SegmentHeader>
            <HeaderLink listHeader={[{
                id: "padmloaibangluong",
                defaultMessage: "Danh mục loại bảng lương",
                route: routes.PADmLoaiBangLuong,
                active: false,
                isSetting: false,
            }]}>
                <Breadcrumb.Divider icon='right chevron' />
                <Breadcrumb.Section active>
                    {item?.ten_loai_bang_luong || "Chi tiết"}
                </Breadcrumb.Section>
            </HeaderLink>
        </SegmentHeader>
        <br />
    </>
}

const ModalThietlapCongthuc = ({ readOnly, index, item, cts, handleChange }) => {
    const refForm = useRef()
    const refCongthuc = useRef()
    const [open, setOpen] = useState(false)
    const [kmluong, setKmluong] = useState([])

    useEffect(() => {
        zzControlHelper.dragElement(document.getElementById('thietlapcongthuc-form'), 'header-thietlapcongthuc-form')
    })

    useEffect(() => {
        getKmluong()
            .then(data =>
                setKmluong(data)
            )
            .catch(err => {
                setError(ZenHelper.getResponseError(err))
            })
    }, [])

    function getKmluong() {
        var result = new Promise((resolve, reject) =>
            Apihrdmkhac.getList('ma_km_luong', res => {
                if (res.status >= 200 && 204 >= res.status) {
                    resolve(res.data.data)
                } else {
                    reject(res)
                }
            })
        )
        return result
    }

    function insertAtCursor(myField, myValue) {
        let ctt = document.getElementById('cong_thuc_text');
        let startPos = ctt.selectionStart;
        let endPos = ctt.selectionStart;
        //IE support
        if (document.selection) {
            myField.focus();
            sel = document.selection.createRange();
            sel.text = myValue;
        }
        //MOZILLA and others
        else if (startPos || endPos == '0') {
            myField.value = myField.value.substring(0, startPos)
                + myValue
                + myField.value.substring(endPos, myField.value.length);
        } else {
            myField.value += myValue;
        }
        refForm.current.setFieldValue('cong_thuc', myField.value)
    }

    function insertCharactor(e, c) {
        refCongthuc.current.focus()
        refCongthuc.current.Character = `${c}`
        refCongthuc.current.handleChange(e)
    }

    const handleSubmit = (params) => {
        handleChange(cts, index, null, { value: params.cong_thuc, name: "cong_thuc" })
        setOpen(false)
    }

    return <>
        <Modal closeOnEscape closeIcon closeOnDimmerClick={false} style={{ zIndex: 9999999 }}
            id="thietlapcongthuc-form"
            onClose={() => setOpen(false)}
            onOpen={() => setOpen(true)}
            open={open}
            trigger={<Button
                icon="ellipsis horizontal"
                primary
                basic
                disabled={readOnly}
                style={{ marginLeft: "5px", }}
            />}

        >
            <Modal.Header id="header-thietlapcongthuc-form">
                Thiết lập công thức
            </Modal.Header>
            <Modal.Content>
                <ZenFormik form={"thietlapcongthuc"}
                    initItem={{ cong_thuc: item.cong_thuc }}
                    validation={[]}
                    onSubmit={handleSubmit}
                    ref={refForm}

                >
                    {formik => {
                        return <Form>
                            <Form.Field>
                                <label>{`Công thức tính khoản mục : ( ${item.ma_km} - ${item.ten_km} )`} </label>
                                <TextArea id="cong_thuc_text"
                                    placeholder="Công thức tính khoản mục"
                                    ref={refCongthuc}
                                    value={formik.values['cong_thuc']}
                                    onChange={(e, eleInput) => {
                                        const { value } = eleInput
                                        formik.setFieldValue('cong_thuc', value)
                                        refCongthuc.current?.Character && insertAtCursor(e.target, refCongthuc.current.Character)
                                        refCongthuc.current.Character = ""
                                    }}
                                />
                            </Form.Field>
                            <Button
                                content="+"
                                value={formik.values['cong_thuc']}
                                onClick={(e) => {
                                    insertCharactor(e, '+')
                                }}
                            />
                            <Button
                                content="-"
                                value={formik.values['cong_thuc']}
                                onClick={(e) => {
                                    insertCharactor(e, '-')
                                }}
                            />
                            <Button
                                content="*"
                                value={formik.values['cong_thuc']}
                                onClick={(e) => {
                                    insertCharactor(e, '*')
                                }}
                            />
                            <Button
                                content="/"
                                value={formik.values['cong_thuc']}
                                onClick={(e) => {
                                    insertCharactor(e, '/')
                                }}
                            />
                            <Button
                                content="("
                                value={formik.values['cong_thuc']}
                                onClick={(e) => {
                                    insertCharactor(e, '(')
                                }}
                            />
                            <Button
                                content=")"
                                value={formik.values['cong_thuc']}
                                onClick={(e) => {
                                    insertCharactor(e, ')')
                                }}
                            />
                            <Grid style={{ paddingTop: "10px" }}>
                                <Grid.Row columns={2}>
                                    <Grid.Column>
                                        <Header content="Các khoản mục hệ thống" />
                                        <div style={{ overflow: 'hidden auto', height: '300px' }}>
                                            {
                                                kmluong && kmluong.map((item, index) => {
                                                    return <div index={index} key={`kmht${index}card`} className={"ar-lf-header"} style={{ height: 'auto', display: "flex", justifyContent: "space-between" }}>
                                                        <div>
                                                            {item.ma} : {item.ten}
                                                            <div>
                                                                <i className="PADMLBL-mota-km">{item.mo_ta}</i>
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <Button index={index} key={`kmht${index}btn`}
                                                                icon
                                                                onClick={(e) => {
                                                                    e.target.value = formik.values['cong_thuc'] ? formik.values['cong_thuc'] : ""
                                                                    insertCharactor(e, item.ma)
                                                                }}
                                                            >
                                                                <Icon name="plus" />
                                                            </Button>
                                                        </div>
                                                    </div>
                                                })
                                            }
                                        </div>
                                    </Grid.Column>
                                    <Grid.Column>
                                        <Header content="Từ danh sách khoản mục" />
                                        <div style={{ overflow: 'hidden auto', height: '300px' }}>
                                            {
                                                cts && cts.filter(t => t.ordinal < item.ordinal).map((item, index) => {

                                                    return <div key={`dskm${index}card`} className={"ar-lf-header"} style={{ height: 'auto', display: "flex", justifyContent: "space-between" }}>
                                                        <div>
                                                            {item.ma_km} : {item.ten_km}
                                                            <div>
                                                                <i className="PADMLBL-mota-km">{item.mo_ta}</i>
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <Button index={index} key={`kmht${index}btn`}
                                                                icon
                                                                onClick={(e) => {
                                                                    e.target.value = formik.values['cong_thuc'] ? formik.values['cong_thuc'] : ""
                                                                    insertCharactor(e, item.ma_km)
                                                                }}
                                                            >
                                                                <Icon name="plus" />
                                                            </Button>
                                                        </div>
                                                    </div>
                                                })
                                            }
                                        </div>
                                    </Grid.Column>
                                </Grid.Row>
                            </Grid>
                        </Form>
                    }}
                </ZenFormik>

            </Modal.Content>
            <Modal.Actions>
                <ZenButton btnType={"cancel"} size="small" onClick={() => setOpen(false)} />
                <ZenButton btnType="save" size="small"
                    type="submit"
                    onClick={(e) => refForm.current.handleSubmit(e)}
                />
            </Modal.Actions>
        </Modal>
    </>
}

export const PADmLoaiBangLuongDetail = {
    PADmLoaiBangLuongDetail: {
        route: routes.PADmLoaiBangLuongDetail(),
        Zenform: PADmLoaiBangLuongDetail_form,
        action: {
            view: { visible: true, permission: "", notPermission: true },
        },
    },
}