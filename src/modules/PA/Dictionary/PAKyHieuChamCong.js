import React, { useEffect } from "react";
import { ZenField, ZenFieldNumber } from "../../../components/Control/index";
import { FormMode } from "../../../utils";
import { Form } from "semantic-ui-react";
import { ApiPAKyHieuChamCong } from "../Api";
import * as permissions from "../../../constants/permissions";

const PAKyHieuChamCongModal = (props) => {
    const { formik, permission, mode,infoModal } = props

    return <>
        <ZenField required autoFocus readOnly={!permission || (mode === FormMode.ADD ? false : true)}
            label={"pakyhieuchamcong.ky_hieu"} defaultlabel="Ký hiệu"
            name="ky_hieu" props={formik}
            maxLength={20}
            isCode
        />
        <ZenField required autoFocus readOnly={!permission}
            label={"pakyhieuchamcong.mo_ta"} defaultlabel="Mô tả"
            name="mo_ta" props={formik}
            maxLength={20}
        />
        <ZenFieldNumber required autoFocus readOnly={!permission} decimalScale={2}
            label={"pakyhieuchamcong.ty_le"} defaultlabel="Tỷ lệ" 
            name="ty_le" props={formik}
            maxLength={20}
        />
    </>
}

const PAKyHieuChamCong = {
    FormModal: PAKyHieuChamCongModal,
    api: {
        url: ApiPAKyHieuChamCong,
    },
    permission: {
        view: permissions.PAKyHieuChamCongXem,
        add: permissions.PAKyHieuChamCongThem,
        edit: permissions.PAKyHieuChamCongSua
    },
    formId: "PAKyHieuChamCong-form",
    size: "tiny",
    isScrolling: false,
    initItem: {
        ksd: 0,
        ky_hieu: "",
        mo_ta: ""
    },
    formValidation: [
        {
            id: "ky_hieu",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ]
        },
        {
            id: "mo_ta",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ]
        }
    ]
}

export { PAKyHieuChamCong };