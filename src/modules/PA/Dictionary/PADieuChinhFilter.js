import React, { useEffect, useMemo, useRef, useState } from "react";
import { Accordion, Button, Form, Icon } from "semantic-ui-react";
import { ZenFormik, ZenFieldSelect, ZenFieldSelectApi } from "../../../components/Control/index";
import { zzControlHelper } from "../../../components/Control/zzControlHelper";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { GlobalStorage, KeyStorage } from "../../../utils/storage";


const styles = {
    label: {
        marginTop: "18px",
        marginBottom: "2px"
    }
}

export const PADieuChinhFilter = (props) => {
    const { onLoadData, fieldCode, onAfterLoadData } = props;
    const refForm = useRef();
    const [expand, setExpand] = useState(0);

    function getfianceyear () {
        return GlobalStorage.getByField(KeyStorage.Global,'financial_year')
    }

    const memoSIDmLoai = useMemo(() => {
        const data = zzControlHelper.getDataFromLocal(ZenLookup.SIDmloai)
        return {
            TenPlkh: data?.filter(t => t.ma_nhom === "PLKH")
                ?.reduce((obj, item) => Object.assign(obj, { [item.ma]: item.ten }), {}),
            optionLoaiKh: zzControlHelper.convertToOptionsSemantic(data.filter(t => t.ma_nhom === "LOAI_KH"), true, "ma", "ma", "ten")
        }
    }, [])

    useEffect(() => {
        onLoadData(``);
    }, []);


    const handleSubmit = async (item, { name }) => {
        const newItem = { ...refForm.current.values, [name]: item.value, }
        const strSql = createStrSql(newItem)
        onLoadData(strSql, newItem, true);
        /* setLoadingForm(false)*/
    }

    function createStrSql(item = {}) {
        let _sql = "";
        if (item.ma_ky_luong) _sql += ` AND a.ma_ky_luong = '${item.ma_ky_luong}'`;
        if (item.loai_dc) _sql += ` AND a.loai_dc = '${item.loai_dc}'`;

        return _sql.replace("AND", "")
    }

    return <>
        <ZenFormik form={"HrLichDay-filter"}
            ref={refForm}
            validation={[]}
            initItem={initItem}
            onSubmit={handleSubmit}
            onReset={(item) => onLoadData("")}
        >
            {
                formik => {
                    const { values } = formik
                    return <Form size="small">
                        <Form.Group widths="4">
                            <ZenFieldSelectApi
                                lookup={{
                                    ...ZenLookup.Ma_ky_luong,
                                    where: `nam = '${getfianceyear()}'`,
                                    onLocalWhere: (items) => {
                                        return items.filter(t => t.nam == getfianceyear())
                                    }
                                }}
                                label="padieuchinh.ma_ky_luong" defaultlabel="Kỳ lương"
                                name="ma_ky_luong"
                                formik={formik}
                                onItemSelected={handleSubmit}
                            />
                            <ZenFieldSelect
                                options={[
                                    { key: 0, value: "T", text: "Tăng" },
                                    { key: 1, value: "G", text: "Giảm" },
                                ]}
                                label="padieuchinh.loai_dc" defaultlabel="Loại điều chỉnh"
                                name="loai_dc"
                                props={formik} clearable={false}
                                onSelectedItem={handleSubmit}
                            />
                            <Button primary style={styles.label}
                                content="Reset"
                                size="small" icon="refresh"
                                onClick={(e) => refForm.current.handleReset(e)}
                            />
                        </Form.Group>
                    </Form>
                }
            }
        </ZenFormik>
    </>
}

const initItem = {
    ma_ky_luong: "",
    loai_dc: "",
}