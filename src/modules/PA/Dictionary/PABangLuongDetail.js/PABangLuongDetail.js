import React, { createRef, memo, useEffect, useMemo, useRef, useState, useImperativeHandle } from "react";
import { Segment, Button, Table, Breadcrumb, Message, Dropdown, Icon } from "semantic-ui-react";
import * as routes from "../../../../constants/routes";
import {
    ContainerScroll, ScrollManager, ZenButton, ZenField, ZenFieldCheckbox,
    ZenFieldSelectApi, ZenFormik, ZenMessageAlert, ZenMessageToast, ZenModal,
    ZenFieldSelect, Helmet, SegmentHeader, HeaderLink, FormatDate, ZenFieldTextArea,
    ZenLoading,
    FormatNumber
} from "../../../../components/Control";
import { ZenHelper } from '../../../../utils';
import { ApiPABangLuong, ApiPADmLoaiBangLuong } from "../../Api";
import { zzControlHelper } from "../../../../components/Control/zzControlHelper";
import _ from "lodash";
import { ZenLookup } from "../../../ComponentInfo/Dictionary/ZenLookup";
import './PABL.css'
import * as permissions from "../../../../constants/permissions";
import { useReactToPrint } from 'react-to-print';
import { ZenVoucherReport } from "../../../../components/Control/zVoucher";

const PABangLuongDetail_form = (props) => {
    const [isLoading, setIsloading] = useState(false);
    const [data, setData] = useState();
    const [kmluong, setKmluong] = useState();
    const [CtsOriginal, setCtsOriginal] = useState([]);
    const [error, setError] = useState();
    const [imgInfo, setImgInfo] = useState();
    const [activeItem, setActiveitem] = useState('ttc');
    const [open, setOpen] = useState(false)
    const [accout, setAccout] = useState();
    const [readonly, setReadonly] = useState(true);
    const refForm = useRef();
    const [loadingForm, setLoadingForm] = useState(false)
    const [btnLoading, setBtnLoading] = useState();
    const [tongCong, setTongCong] = useState();

    const [widthtennv, setWidthtennv] = useState()
    const [widthkh, setWidthkh] = useState()
    const [widthbp, setWidthbp] = useState()
    const [heighttennv, setHeihgttennv] = useState()

    const refPrintingPDf = useRef();
    const [loading, setLoading] = useState(false)

    useEffect(() => {
        const elementtennv = document.getElementById('getwidthtennv-id')
        const elementkh = document.getElementById('getwidthkh-id')
        const elementbp = document.getElementById('getwidthbp-id')
        const widthtennv = elementtennv.getBoundingClientRect().width
        const heighttennv = elementtennv.getBoundingClientRect().height
        const widthkh = elementkh.getBoundingClientRect().width
        const widthbp = elementbp.getBoundingClientRect().width
        setWidthtennv(widthtennv)
        setWidthkh(widthkh)
        setWidthbp(widthbp)
        setHeihgttennv(heighttennv)
    })

    function fetchRequestByCode() {
        setLoadingForm(true)
        const id = zzControlHelper.atobUTF8(props.match.params.id)
        var result = new Promise((resolve, reject) =>
            ApiPABangLuong.getByCode(id, res => {
                if (res.status >= 200 && 204 >= res.status) {
                    resolve(res.data.data)
                } else {
                    reject(res)
                }
                setLoadingForm(false)
            })
        )
        return result
    }

    function fetchRequestKMLuong(data) {
        var result = new Promise((resolve, reject) =>
            ApiPADmLoaiBangLuong.getByCode(data?.ma_loai_bang_luong, res => {
                if (res.status >= 200 && 204 >= res.status) {
                    resolve(res.data.data)
                } else {
                    reject(res)
                }
            })
        )
        return result
    }

    useEffect(() => {
        fetchRequestByCode().then(res => {
            setData(res)
            setCtsOriginal(_.cloneDeep(res.cts))
            fetchRequestKMLuong(res).then(reskm => {
                setKmluong(reskm)
                let tongcong = [];
                res?.cts.map(item => {
                    if (item.data) {
                        reskm?.cts.map((it, index) => {
                            if (!tongcong[it.ma_km]) {
                                tongcong[it.ma_km] = 0
                            }
                            tongcong[it.ma_km] += JSON.parse(item?.data)[it.ma_km]
                        })
                    }
                })
                setTongCong(tongcong)
                //setCtsOriginal(_.cloneDeep(res.cts))
            }).catch(err => {
                setError(ZenHelper.getResponseError(err))
            })
        }).catch(err => {
            setError(ZenHelper.getResponseError(err))
        })

    }, [])

    const PageHeader = ({ item }) => {
        return <>
            <Helmet idMessage={"hrhsns.detail"}
                defaultMessage={"Bảng lương"} />

            <SegmentHeader style={{ width: "auto" }}>
                <HeaderLink listHeader={[{
                    id: "pabangluong",
                    defaultMessage: "Bảng lương",
                    route: routes.PABangLuong,
                    active: false,
                    isSetting: false,
                }]}>
                    <Breadcrumb.Divider icon='right chevron' />
                    <Breadcrumb.Section active>
                        {`${item?.ten_loai_bang_luong} - ${item?.ten_ky_luong}` || "Chi tiết"}
                    </Breadcrumb.Section>
                </HeaderLink>
            </SegmentHeader>
        </>
    }

    function getCachtinh(value) {
        var _switch = {
            1: 'Từ hệ thống',
            2: 'Công thức',
            3: 'Hằng số',
        };
        return _switch[value]
    }

    const handleTinhLuong = () => {
        setLoadingForm(true)
        ApiPABangLuong.tinhluong({ id: data.id, ma_loai_bang_luong: data.ma_loai_bang_luong, ma_ky_luong: data.ma_ky_luong }, res => {
            if (res.status >= 200 && 204 >= res.status) {
                ZenMessageToast.success()
                fetchRequestByCode().then(res => {
                    setData(res)
                    setCtsOriginal(_.cloneDeep(res.cts))
                    fetchRequestKMLuong(res).then(res => {
                        setKmluong(res)
                        //setCtsOriginal(_.cloneDeep(res.cts))
                    }).catch(err => {
                        setError(ZenHelper.getResponseError(err))
                    })
                }).catch(err => {
                    setError(ZenHelper.getResponseError(err))
                })
            } else {
                setError(ZenHelper.getResponseError(res))
            }
            setLoadingForm(false)
        })
    }

    const handleDuyetBangLuong = (params) => {
        ApiPABangLuong.duyetbangluong(params, res => {
            if (res.status >= 200 && 204 >= res.status) {
                ZenMessageToast.success()
                fetchRequestByCode().then(res => {
                    setData(res)
                    setCtsOriginal(_.cloneDeep(res.cts))
                    fetchRequestKMLuong(res).then(res => {
                        setKmluong(res)
                        //setCtsOriginal(_.cloneDeep(res.cts))
                    }).catch(err => {
                        setError(ZenHelper.getResponseError(err))
                    })
                }).catch(err => {
                    setError(ZenHelper.getResponseError(err))
                })
            } else {
                setError(ZenHelper.getResponseError(res))
            }
        })
    }
    //In Bảng lương
    const RefPrinting = useRef();
    const handlePrint = useReactToPrint({
        content: () => RefPrinting.current,
    });

    const btnType = [
        { key: 'excel', icon: 'file excel outline', text: 'Tải bảng lương (excel)', value: 'xls' },
        { key: 'pdf', icon: 'file pdf outline', text: 'Tải phiếu lương (pdf)', value: 'pdf' },
        { key: 'zip', icon: 'download', text: 'Tải phiếu lương (zip)', value: 'zip' },
        { key: 'fastprint', icon: 'print', text: 'In nhanh bảng lương', value: 'fastprint' },
    ]
    const handleDowload = (i) => {
        const { value } = i
        setBtnLoading(true)
        if (value === 'xls') {
            ApiPABangLuong.export(data.id, { type: value, template: data.ma_loai_bang_luong }, res => {
                if (res.status === 200) {
                    const url = window.URL.createObjectURL(new Blob([res.data]));
                    const link = document.createElement('a');
                    link.href = url;
                    link.setAttribute('download', `report.xlsx`);
                    document.body.appendChild(link);
                    link.click();
                    document.body.removeChild(link);
                } else {
                    ZenMessageAlert.error(zzControlHelper.getResponseError(res))
                }
                setBtnLoading(false)
            })
        } else if (value === 'pdf') {
            ApiPABangLuong.export(data.id, { type: value, template: "plgv" }, res => {
                if (res.status === 200) {
                    // const url = window.URL.createObjectURL(new Blob([res.data]));
                    // const link = document.createElement('a');
                    // link.href = url;
                    // link.setAttribute('download', `${zzControlHelper.formatDateTime(new Date(), 'YYYYMMDD_HHMMSS')}_${data.id}.pdf`);
                    // document.body.appendChild(link);
                    // link.click();
                    // document.body.removeChild(link);
                    zzControlHelper.openPdfPrintFile(res.data)
                    // var file = new Blob([data], { type: 'application/pdf' });
                    // var fileURL = URL.createObjectURL(file);
                    // refPrintingPDf.current.reportPrint(res.data)
                } else {
                    ZenMessageAlert.error(zzControlHelper.getResponseError(res))
                }
                setBtnLoading(false)
            })
        } else if (value === 'zip') {
            ApiPABangLuong.export(data.id, { type: value, template: "plgv" }, res => {
                if (res.status === 200) {
                    const url = window.URL.createObjectURL(new Blob([res.data]));
                    const link = document.createElement('a');
                    link.href = url;
                    link.setAttribute('download', `${zzControlHelper.formatDateTime(new Date(), 'YYYYMMDD_HHMMSS')}_${data.id}.zip`);
                    document.body.appendChild(link);
                    link.click();
                    document.body.removeChild(link);
                } else {
                    ZenMessageAlert.error(zzControlHelper.getResponseError(res))
                }
                setBtnLoading(false)
            })
        } else if (value === 'fastprint') {
            handlePrint()
            setBtnLoading(false)
        }
    }

    return <>
        <div style={{ display: "flex", justifyContent: "space-between", marginBottom: "10px" }}>
            <PageHeader item={data} />
            <div style={{ alignSelf: "center" }}>
                <ZenButton
                    btnType="refresh"
                    size="small"
                    onClick={() => {
                        fetchRequestByCode().then(res => {
                            setData(res)
                            setCtsOriginal(_.cloneDeep(res.cts))
                            fetchRequestKMLuong(res).then(res => {
                                setKmluong(res)
                                //setCtsOriginal(_.cloneDeep(res.cts))
                            }).catch(err => {
                                setError(ZenHelper.getResponseError(err))
                            })
                        }).catch(err => {
                            setError(ZenHelper.getResponseError(err))
                        })
                    }}
                />
                <Button.Group>
                    <ZenButton permission={permissions.PABangLuongTaoVaTinhLuong}
                        icon="calculator"
                        content="Tính lương"
                        primary //type="submit"
                        onClick={(e) => { handleTinhLuong() }}
                    >
                    </ZenButton>
                    <Dropdown
                        loading={btnLoading}
                        style={{ borderLeft: "1px solid rgb(65, 108, 218)" }}
                        className='button icon primary'
                        trigger={<></>}
                    >
                        <Dropdown.Menu className="left">
                            {btnType && btnType.map((item, index) => {
                                return <>
                                    <Dropdown.Item value={item.value} onClick={(e, i) => handleDowload(i)}>
                                        <Icon name={item.icon} />
                                        {item.text}
                                    </Dropdown.Item>
                                </>
                            })}
                        </Dropdown.Menu>
                    </Dropdown>
                </Button.Group>
                <PrintReportPDF
                    ref={refPrintingPDf}
                    onLoaded={() => setLoading(false)}
                />
            </div>
        </div>
        <Segment>
            {error && <Message negative list={error} />}
            <div className="PADMLBL-group" style={{ paddingBottom: "20px" }}>
                <div className="PADMLBL-field">
                    <label>Kỳ lương : </label>
                    <label>{data?.ten_ky_luong}</label>
                </div>
                <div className="PADMLBL-field">
                    <label>Loại bảng lương : </label>
                    <label>{data?.ten_loai_bang_luong}</label>
                </div>
                <div className="PADMLBL-field">
                    <label>Diễn giải : </label>
                    <label>{data?.dien_giai || " . . . "}</label>
                </div>
                <div className="PADMLBL-field">
                    <label>Trạng thái : </label>
                    <label>{data?.ten_trang_thai || " . . . "}</label>
                </div>
                <div className="PADMLBL-field">
                    {data?.trang_thai !== '5' && <Button size="small" primary content="Duyệt bảng lương" onClick={e => handleDuyetBangLuong({ id: data.id, trang_thai: 5 })} />}
                    {data?.trang_thai == '5' && <Button size="small" primary content="Chuyển chờ duyệt" onClick={e => handleDuyetBangLuong({ id: data.id, trang_thai: 0 })} />}
                </div>
            </div>
            <ZenLoading loading={loadingForm} inline="centered" dimmerPage={false} />
            <div style={{ overflowX: 'auto', overFlowY: 'auto', height: 'auto', maxHeight: '542px' }}>
                <div ref={RefPrinting}>
                    <Table celled compact selectable size="small" unstackable >
                        <Table.Header>
                            <Table.HeaderCell content="Mã NV" textAlign="left" className="sticky-left HRCCGV-sticky-top" id="getwidthtennv-id" style={{ zIndex: '2' }} width={1} />
                            <Table.HeaderCell content="Họ và tên" textAlign="left" className="HRCCGV-sticky-left HRCCGV-sticky-top" id="getwidthkh-id" style={{ left: `${widthtennv}px`, zIndex: '2' }} width={1} />
                            <Table.HeaderCell content="Bộ phận" textAlign="left" className="HRCCGV-sticky-left HRCCGV-sticky-top" id="getwidthbp-id" style={{ left: `${widthtennv + widthkh}px`, zIndex: '2' }} width={1} />
                            {/* <Table.HeaderCell content={data?.ten_loai_bang_luong} textAlign="left" className="sticky-top" /> */}
                            {kmluong && kmluong?.cts.map((item, index) => {
                                return <Table.HeaderCell className="HRCCGV-sticky-top" content={item.ten_km} />
                            })}
                        </Table.Header>
                        <Table.Body>
                            {tongCong && <Table.Row>
                                <Table.Cell colSpan={3} style={{ whiteSpace: 'nowrap' }} className="sticky-left PADMLBL-sticky-top" style={{ top: `${heighttennv}px`, zIndex: "2" }} ><strong>Tổng cộng</strong></Table.Cell>
                                {
                                    kmluong && kmluong?.cts.map((it, index) => {
                                        return <Table.Cell textAlign="right" className="sticky-left PADMLBL-sticky-top" style={{ top: `${heighttennv}px` }} > <strong><FormatNumber value={tongCong[it.ma_km]} /></strong> </Table.Cell>
                                    })
                                }
                            </Table.Row>}
                            {
                                data && data?.cts.map((item, index) => {

                                    return <Table.Row>
                                        <Table.Cell style={{ whiteSpace: 'nowrap' }} className="sticky-left">{item.id_nv} </Table.Cell>
                                        <Table.Cell style={{ whiteSpace: 'nowrap', left: `${widthtennv}px` }} className="HRCCGV-sticky-left">{item.ten_nv}</Table.Cell>
                                        <Table.Cell style={{ whiteSpace: 'nowrap', left: `${widthtennv + widthkh}px` }} className="HRCCGV-sticky-left">{item.ten_cctc} </Table.Cell>
                                        {
                                            kmluong && kmluong?.cts.map((it, index) => {
                                                return item.data ? <Table.Cell textAlign="right"> <FormatNumber value={JSON.parse(item?.data)[it.ma_km]} /> </Table.Cell> : <Table.Cell />
                                            })
                                        }
                                    </Table.Row>
                                })

                            }
                        </Table.Body>
                    </Table>
                </div>
            </div>
        </Segment>
    </>

}

const PrintReportPDF = React.forwardRef(({ data }, ref) => {

    function f_reportPrint() {
        zzControlHelper.openPdfPrintFile(data);
    }

    useImperativeHandle(ref, () => ({
        reportPrint: f_reportPrint
    }));

    return <></>
})

export const PABangLuongDetail = {
    PABangLuongDetail: {
        route: routes.PABangLuongDetail(),
        Zenform: PABangLuongDetail_form,
        action: {
            view: { visible: true, permission: "", notPermission: true },
        },
    },
}