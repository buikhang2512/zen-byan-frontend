import React, { useEffect, useMemo, useRef, useState } from "react";
import { Accordion, Button, Form, Icon, Modal, label } from "semantic-ui-react";
import { ZenFormik, ZenFieldSelect, ZenFieldSelectApi, ZenButton, ZenMessageAlert, ZenMessageToast } from "../../../components/Control/index";
import { zzControlHelper } from "../../../components/Control/zzControlHelper";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { GlobalStorage, KeyStorage } from "../../../utils/storage";
import { ApiPAHangSo } from "../Api";

const ModalDulicate = () => {
    const refFormik = useRef();
    const [value, setValue] = useState()
    const [open, setOpen] = useState(false)
    const [btnLoading, setBtnLoading] = useState();
    const [error, setError] = useState();

    function getfianceyear() {
        return GlobalStorage.getByField(KeyStorage.Global, 'financial_year')
    }

    const handleSubmit = (params, formId) => {
        ApiPAHangSo.duplicate({ ...params, period_from: params.period_from, period_to: params.period_to }, res => {
            if (res.status >= 200 && 204 >= res.status) {
                ZenMessageToast.success();
            } else {
                setError(zzControlHelper.getResponseError(res))
                ZenMessageAlert.error(zzControlHelper.getResponseError(res))
            }
        })
        setOpen(false)
    }

    const onValidate = (item) => {
        const errors = {};
        const { period_from, period_to } = item

        if (period_from == period_to ) {
            errors.period_from = 'hai kỳ lương không được trùng nhau !'
            errors.period_to = 'hai kỳ lương không được trùng nhau !'
        }

        return errors
    }

    return <Modal id="dulicate-form" style={{ paddingTop: "10px" }}
        closeOnEscape closeIcon closeOnDimmerClick={false}
        onClose={() => setOpen(false)}
        onOpen={() => setOpen(true)}
        open={open} size={"tiny"}
        trigger={<Button size="small" icon="copy" primary content="Copy dữ liệu từ kỳ lương trước" style={{ marginTop: "19px", height: "fit-content" }}/>}>

        <Modal.Header id='header-dulicate-form' style={{ cursor: "grabbing" }}>
            Copy dữ liệu
        </Modal.Header>

        <Modal.Content>
            <ZenFormik ref={refFormik} onSubmit={handleSubmit} onValidate={onValidate} >
                {
                    formik => {
                        return <Form>
                            <ZenFieldSelectApi required loadApi
                                tabIndex={1}
                                lookup={{
                                    ...ZenLookup.Ma_ky_luong,
                                    where: `nam = '${getfianceyear()}'`,
                                    onLocalWhere: (items) => {
                                        return items.filter(t => t.nam == getfianceyear())
                                    }
                                }}
                                label={"ardmkh.header"} defaultlabel="Kỳ lương copy"
                                name="period_from" formik={formik}
                                //onItemSelected={handleSelectedLookup}
                              
                            />
                            <ZenFieldSelectApi required loadApi
                                tabIndex={1}
                                lookup={{
                                    ...ZenLookup.Ma_ky_luong,
                                    where: `nam = '${getfianceyear()}'`,
                                    onLocalWhere: (items) => {
                                        return items.filter(t => t.nam == getfianceyear())
                                    }
                                }}
                                label={"ardmkh.header"} defaultlabel="Kỳ lương mới"
                                name="period_to" formik={formik}
                                //onItemSelected={handleSelectedLookup}
                                
                            />
                            <label style={{ paddingTop: "10px" }}>Hệ thống sẽ sao chép dữ liệu đã nhập thủ công từ <b>Kỳ lương copy</b> đến kỳ lương <b>Kỳ lương mới</b></label>
                        </Form>
                    }
                }
            </ZenFormik>
        </Modal.Content>

        <Modal.Actions>
            <ZenButton btnType={"cancel"} size="small" onClick={() => setOpen(false)} />
            <ZenButton btnType="save" size="small"
                loading={btnLoading} type="submit"
                onClick={(e) => refFormik.current.handleSubmit(e)}
            />
        </Modal.Actions>
    </Modal>
}

const styles = {
    label: {
        marginTop: "18px",
        marginBottom: "2px"
    }
}

export const PAHangSoFilter = (props) => {
    const { onLoadData, fieldCode, onAfterLoadData } = props;
    const refForm = useRef();
    const [expand, setExpand] = useState(0);

    function getfianceyear () {
        return GlobalStorage.getByField(KeyStorage.Global,'financial_year')
    }

    const memoSIDmLoai = useMemo(() => {
        const data = zzControlHelper.getDataFromLocal(ZenLookup.SIDmloai)
        return {
            TenPlkh: data?.filter(t => t.ma_nhom === "PLKH")
                ?.reduce((obj, item) => Object.assign(obj, { [item.ma]: item.ten }), {}),
            optionLoaiKh: zzControlHelper.convertToOptionsSemantic(data.filter(t => t.ma_nhom === "LOAI_KH"), true, "ma", "ma", "ten")
        }
    }, [])

    useEffect(() => {
        onLoadData(``);
    }, []);


    const handleSubmit = async (item, { name }) => {
        const newItem = { ...refForm.current.values, [name]: item.value, }
        const strSql = createStrSql(newItem)
        onLoadData(strSql, newItem, true);
        /* setLoadingForm(false)*/
    }

    function createStrSql(item = {}) {
        let _sql = "";
        if (item.ma_ky_luong) _sql += ` AND a.ma_ky_luong = '${item.ma_ky_luong}'`;
        if (item.loai_dc) _sql += ` AND a.loai_dc = '${item.loai_dc}'`;

        return _sql.replace("AND", "")
    }

    return <>
        <ZenFormik form={"HrLichDay-filter"}
            ref={refForm}
            validation={[]}
            initItem={initItem}
            onSubmit={handleSubmit}
            onReset={(item) => onLoadData("")}
        >
            {
                formik => {
                    const { values } = formik
                    return <Form size="small">
                        <Form.Group widths="4">
                            <ZenFieldSelectApi
                                lookup={{
                                    ...ZenLookup.Ma_ky_luong,
                                    where: `nam = '${getfianceyear()}'`,
                                    onLocalWhere: (items) => {
                                        return items.filter(t => t.nam == getfianceyear())
                                    }
                                }}
                                label="paHangSo.ma_ky_luong" defaultlabel="Kỳ lương"
                                name="ma_ky_luong"
                                formik={formik}
                                onItemSelected={handleSubmit}
                            />
                            <ModalDulicate />
                        </Form.Group>
                    </Form>
                }
            }
        </ZenFormik>
    </>
}

const initItem = {
    ma_ky_luong: "",
}