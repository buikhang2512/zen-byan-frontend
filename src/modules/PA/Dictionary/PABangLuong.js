import React, { useEffect, useState } from "react";
import { ZenField, ZenFieldSelectApi, ZenFieldNumber, ZenFieldSelect, ZenFieldSelectApiMulti } from "../../../components/Control/index";
import { FormMode } from "../../../utils";
import { Form, Dropdown } from "semantic-ui-react";
import { ApiPABangLuong } from "../Api";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import DatePicker from "react-datepicker";
import { zzControlHelper } from "../../../components/Control/zzControlHelper";
import moment from "moment";
import { GlobalStorage, KeyStorage } from "../../../utils/storage";
import { ZenHelper } from "../../../utils";
import _ from "lodash";
import * as routes from '../../../constants/routes';
import * as permissions from "../../../constants/permissions";

const PABangLuongModal = (props) => {
    const { formik, permission, mode, infoModal } = props
    const [optionbophan, setOptionbophan] = useState([])

    function getfianceyear() {
        return GlobalStorage.getByField(KeyStorage.Global, 'financial_year')
    }

    const handleselect = (item, { name }) => {
        if (name == 'ma_ky_luong') {
            formik.setFieldValue('ten_ky_luong', item.ten_ky_luong)
        } else if (name == 'ma_loai_bang_luong') {
            formik.setFieldValue('ten_loai_bang_luong', item.ten_loai_bang_luong)
        }
    }

    useEffect(() => {
        const bophan = GlobalStorage.getByField(KeyStorage.CacheData, "id_cctc")?.data
        setOptionbophan(ZenHelper.translateListToSelectOptions(bophan.filter(x => x.chi_tiet == 1) || [], false, 'id_cctc', 'id_cctc', 'id_cctc', [], 'ten_cctc'))
    }, [])

    const handleSelectIDcctc = (item) => {
        if (_.join(item.value, ',') !== ', ') {
            let next_stt = _.join(item.value, ',')
            formik.setFieldValue('id_cctcs', next_stt)
        }
    }

    return <>
        <ZenFieldSelectApi required
            readOnly={!permission}
            lookup={{
                ...ZenLookup.Ma_ky_luong,
                where: `nam = '${getfianceyear()}'`,
                onLocalWhere: (items) => {
                    return items.filter(t => t.nam == getfianceyear())
                }
            }}
            label="PABangLuong.ma_ky_luong" defaultlabel="Kỳ lương"
            name="ma_ky_luong"
            formik={formik}
            onItemSelected={(item, { name }) => handleselect(item, { name })}
        />
        <ZenFieldSelectApi required
            readOnly={!permission}
            lookup={{
                ...ZenLookup.PADmLoaiBangLuong,
                format: '{ten_loai_bang_luong}',
            }}
            label="PABangLuong.ma_loai_bang_luong" defaultlabel="Loại bảng lương"
            name="ma_loai_bang_luong"
            formik={formik}
            onItemSelected={(item, { name }) => handleselect(item, { name })}
        />
        <Form.Group>
            <Form.Field width="16" required>
                <label>Bộ phận</label>
                <Dropdown error={formik && (formik.touched['id_cctcs'] && formik.errors['id_cctcs'])} fluid multiple selection options={optionbophan} value={formik.values.id_cctcs?.split(',')}
                    readOnly={!permission}
                    onChange={(e, item) => { handleSelectIDcctc(item) }}
                />
                {formik && (formik.touched['id_cctcs'] && formik.errors['id_cctcs']) ? <div class="ui pointing above prompt label" role="alert" aria-atomic="true">Không được bỏ trống trường này</div> : undefined}
            </Form.Field>
        </Form.Group>
        <ZenField readOnly={!permission}
            label={"PABangLuong.dien_giai"} defaultlabel="Diễn giải"
            name="dien_giai" props={formik}
        />
    </>
}

const onBeforeSave = (item) => {
    if (item.id_cctcs !== "") {
        item.id_cctcs = item.id_cctcs.replace(',','')
        return item
    }
    else {
        return
    }
}

const PABangLuong = {
    FormModal: PABangLuongModal,
    api: {
        url: ApiPABangLuong,
    },
    onBeforeSave: onBeforeSave,
    saveAndRedirect: {
        linkto: (item) => {
            const id = zzControlHelper.btoaUTF8(item['id'])
            window.location.assign(`#${routes.PABangLuongDetail(id)}`)
        }
    },
    permission: {
        view: "",
        add: permissions.PABangLuongTaoVaTinhLuong,
        edit: ""
    },
    formId: "PABangLuong-form",
    size: "tiny",
    isScrolling: false,
    initItem: {
        ma_ky_luong: "",
        ma_loai_bang_luong: "",
        id_cctcs: "",
        dien_gia: "",
        trang_thai: ""
    },
    formValidation: [
        {
            id: "ma_ky_luong",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ]
        },
        {
            id: "ma_loai_bang_luong",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ]
        },
        {
            id: "id_cctcs",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ]
        }
    ]
}

export { PABangLuong };