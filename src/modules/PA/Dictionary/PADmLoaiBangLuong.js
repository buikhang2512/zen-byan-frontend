import React, { useEffect, useState } from "react";
import { ZenField, ZenFieldDate, ZenFieldSelect } from "../../../components/Control/index";
import { FormMode, ZenHelper } from "../../../utils";
import { ApiPADmLoaiBangLuong } from "../Api";
import { Form, Dropdown } from "semantic-ui-react";
import { GlobalStorage, KeyStorage } from "../../../utils/storage";
import _ from "lodash";
import * as permissions from "../../../constants/permissions";

const PADmLoaiBangLuongModal = (props) => {
   const { formik, permission, mode } = props
   const [optionbophan, setOptionbophan] = useState([])

   useEffect(() => {
      const bophan = GlobalStorage.getByField(KeyStorage.CacheData, "id_cctc")?.data
      setOptionbophan(ZenHelper.translateListToSelectOptions(bophan.filter(x => x.chi_tiet == 1) || [], false, 'id_cctc', 'id_cctc', 'id_cctc', [], 'ten_cctc'))
   }, [])

   const handleSelectIDcctc = (item) => {
       if(_.join(item.value, ',') !== ', ' ) {
           let next_stt = _.join(item.value, ',')
           formik.setFieldValue('id_cctcs',next_stt)
       }  
   }

   return <React.Fragment>
      <ZenField required autoFocus readOnly={!permission || (mode === FormMode.ADD ? false : true)}
         label={"padmloaibangluong.ma_loai_bang_luong"} defaultlabel="Mã loại bảng lương"
         name="ma_loai_bang_luong" props={formik}
         isCode
      />
      <ZenField required readOnly={!permission}
         label={"padmloaibangluong.ten_loai_bang_luong"} defaultlabel="Tên loại bảng lương"
         name="ten_loai_bang_luong" props={formik}
      />
      <Form.Group>
         <Form.Field width="16">
            <label>Bộ phận</label>
            <Dropdown error={formik && (formik.touched['id_cctcs'] && formik.errors['id_cctcs'])} fluid multiple selection options={optionbophan} value={formik.values.id_cctcs?.split(',')}
               readOnly={!permission}
               onChange={(e, item) => { handleSelectIDcctc(item) }}
            />
         </Form.Field>
      </Form.Group>
   </React.Fragment>
}

const onBeforeSave = (item) => {
   if(item.id_cctcs) {
      item.id_cctcs = item.id_cctcs.replace(',','')
   }
   return item
}

const PADmLoaiBangLuong = {
   FormModal: PADmLoaiBangLuongModal,
   onBeforeSave: onBeforeSave,
   api: {
      url: ApiPADmLoaiBangLuong,
   },
   permission: {
      view: permissions.PADmLoaiBangLuongXem,
      add: permissions.PADmLoaiBangLuongThem,
      edit: permissions.PADmLoaiBangLuongSua
   },
   formId: "padmloaibangluong-form",
   size: "tiny",
   initItem: {
      ma_loai_bang_luong: "",
      ten_loai_bang_luong: "",
      id_cctcs: "",
      cts: [],
   },
   formValidation: [
      {
         id: "ma_loai_bang_luong",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
            {
               type: "max",
               params: [20, "Không được vượt quá 20 kí tự"]
            },
         ]
      },
      {
         id: "ten_loai_bang_luong",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
         ]
      },
   ]
}

export { PADmLoaiBangLuong };