import React, { useEffect, useState } from 'react';
import * as routes from "../../../constants/routes";
import { ApiPADmKyLuong, ApiPADieuChinh, ApiPAKyHieuChamCong, ApiPADmLoaiBangLuong, ApiPABangLuong, ApiPAHangSo } from "../Api/index";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { ZenField, ZenLink } from "../../../components/Control";
import { Table } from "semantic-ui-react";
import { PADieuChinhFilter } from './PADieuChinhFilter';
import { PAHangSoFilter } from './PAHangSoFilter';
import { zzControlHelper } from '../../../components/Control/zzControlHelper';
import * as permissions from "../../../constants/permissions";
import { ZenHelper } from '../../../utils';
import { GlobalStorage, KeyStorage } from '../../../utils/storage';

const optionBoPhan = ZenHelper.translateListToSelectOptions(GlobalStorage.getByField(KeyStorage.CacheData, 'id_cctc')?.data, false, 'id_cctc', 'id_cctc', 'id_cctc', [], 'ten_cctc')


const PA_Dictionary = {
   PADmKyLuong: {
      route: routes.PADmKyLuong,

      action: {
         view: { visible: true, permission: permissions.PADmKyLuongXem },
         add: { visible: true, permission: permissions.PADmKyLuongThem },
         edit: { visible: true, permission: permissions.PADmKyLuongSua },
         del: { visible: true, permission: permissions.PADmKyLuongXoa },
      },

      linkHeader: {
         id: "PADmKyLuong",
         defaultMessage: "Danh mục kỳ lương",
         active: true,
         isSetting: true,
      },

      tableList: {
         keyForm: "PADmKyLuong",
         formModal: ZenLookup.Ma_ky_luong,
         fieldCode: "ma_ky_luong",
         unPagination: false,
         duplicate: true,
         showFilterLabel: true,

         hasChecked: true,
         onDeleteBulk: (items, onAfterExec) => {
            setTimeout(() => {
               // items: các item đã chọn
               // onAfterExec(mảng các mã đã chọn: [code1,code2])
               onAfterExec(items.map(t => t.Ma_ky_luong))
            }, 500);
         },
         // onUpdateBulk: (items) => {

         // },

         api: {
            url: ApiPADmKyLuong,
            type: "sql",
         },
         columns: [
            { id: "padmkyluong.ma_ky_luong", defaultMessage: "Mã kỳ lương", fieldName: "ma_ky_luong", type: "string", filter: "string", sorter: true, editForm: true },
            { id: "padmkyluong.ten_ky_luong", defaultMessage: "Tên kỳ lương", fieldName: "ten_ky_luong", type: "string", filter: "string", sorter: true, },
            { id: "padmkyluong.nam", defaultMessage: "Năm", fieldName: "nam", type: "int", filter: "number", sorter: true, },
            { id: "padmkyluong.ngay_bd", defaultMessage: "Ngày bắt đầu", fieldName: "ngay_bd", filter: "date", sorter: true, },
            { id: "padmkyluong.ngay_kt", defaultMessage: "Ngày kết thúc", fieldName: "ngay_kt", filter: "date", sorter: true, },
            { id: "padmkyluong.ghi_chu", defaultMessage: "Ghi chú", fieldName: "ghi_chu", type: "string", filter: "string", sorter: true, },
         ],
      },
   },

   PADieuChinh: {
      route: routes.PADieuChinh,
      action: {
         view: { visible: true, permission: permissions.PADieuChinhXem },
         add: { visible: true, permission: permissions.PADieuChinhThem },
         edit: { visible: true, permission: permissions.PADieuChinhSua },
         del: { visible: true, permission: permissions.PADieuChinhXoa },
      },

      linkHeader: {
         id: "padieuchinh",
         defaultMessage: "Điều chỉnh lương",
         active: true,
         isSetting: false,
      },

      tableList: {
         ContainerTop: PADieuChinhFilter,
         keyForm: "PADieuChinh",
         formModal: ZenLookup.PADieuChinh,
         fieldCode: "id",
         unPagination: false,
         changeCode: false,
         showFilterLabel: true,

         api: {
            url: ApiPADieuChinh,
            type: "sql"
         },

         columns: [
            { id: "PADieuChinh.ten_ky_luong", defaultMessage: "kỳ lương", fieldName: "ten_ky_luong", filter: "string", sorter: true, editForm: true, },
            { id: "PADieuChinh.ten_nv", defaultMessage: "Nhân viên", fieldName: "ten_nv", filter: "string", sorter: true, },
            { id: "PADieuChinh.loai_dc", defaultMessage: "Loai", fieldName: "loai_dc", filter: "string", sorter: true, },
            { id: "PADieuChinh.gia_tri", defaultMessage: "Giá trị", fieldName: "gia_tri", filter: "number", sorter: true, },
            { id: "PADieuChinh.ly_do", defaultMessage: "Lý do", fieldName: "ly_do", filter: "string", sorter: false, },
            { id: "PADieuChinh.ghi_chu", defaultMessage: "Ghi chú", fieldName: "ghi_chu", filter: "string", sorter: true, },
         ],
      },
   },

   PAKyHieuChamCong: {
      route: routes.PAKyHieuChamCong,
      action: {
         view: { visible: true, permission: permissions.PAKyHieuChamCongXem },
         add: { visible: true, permission: permissions.PAKyHieuChamCongThem },
         edit: { visible: true, permission: permissions.PAKyHieuChamCongSua },
         del: { visible: true, permission: permissions.PAKyHieuChamCongXoa },
      },

      linkHeader: {
         id: "pakyhieuchamcong",
         defaultMessage: "Ký hiệu chấm công",
         active: true,
         isSetting: false,
      },

      tableList: {
         keyForm: "PAKyHieuChamCong",
         formModal: ZenLookup.PAKyHieuChamCong,
         fieldCode: "ky_hieu",
         unPagination: false,
         changeCode: false,
         showFilterLabel: true,

         api: {
            url: ApiPAKyHieuChamCong,
            type: "sql"
         },

         columns: [
            { id: "PAKyHieuChamCong.ky_hieu", defaultMessage: "Ký hiệu", fieldName: "ky_hieu", filter: "string", sorter: true, editForm: true, },
            { id: "PAKyHieuChamCong.mo_ta", defaultMessage: "Mô tả", fieldName: "mo_ta", filter: "string", sorter: true, },
            { id: "PAKyHieuChamCong.ty_le", defaultMessage: "Tỷ lệ", fieldName: "ty_le", filter: "number", sorter: true, },
            { id: "ksd", defaultMessage: "Không sử dụng", fieldName: "ksd", type: "bool", filter: "", sorter: true }
         ],
      },
   },

   PADmLoaiBangLuong: {
      route: routes.PADmLoaiBangLuong,
      action: {
         view: { visible: true, permission: permissions.PADmLoaiBangLuongXem },
         add: { visible: true, permission: permissions.PADmLoaiBangLuongThem },
         edit: { visible: true, permission: permissions.PADmLoaiBangLuongSua },
         del: { visible: true, permission: permissions.PADmLoaiBangLuongXoa },
      },

      linkHeader: {
         id: "padmloaibangluong",
         defaultMessage: "Danh mục loại bảng lương",
         active: true,
         isSetting: false,
      },

      tableList: {
         keyForm: "PADmLoaiBangLuong",
         formModal: ZenLookup.PADmLoaiBangLuong,
         fieldCode: "ma_loai_bang_luong",
         unPagination: false,
         changeCode: false,
         showFilterLabel: true,

         api: {
            url: ApiPADmLoaiBangLuong,
            type: "sql"
         },

         columns: [
            { id: "PAKyHieuChamCong.ma_loai_bang_luong", defaultMessage: "Mã loại bảng lương", fieldName: "ma_loai_bang_luong", filter: "string", sorter: true, editForm: true, custom: true },
            { id: "PAKyHieuChamCong.ten_loai_bang_luong", defaultMessage: "Tên loại bảng lương", fieldName: "ten_loai_bang_luong", filter: "string", sorter: true, },
            { id: "PAKyHieuChamCong.id_cctcs", defaultMessage: "Bộ phận áp dụng", fieldName: "id_cctcs", filter: "list", sorter: true, custom: true, listFilter: optionBoPhan },
            //{ id: "ksd", defaultMessage: "Không sử dụng", fieldName: "ksd", type: "bool", filter: "", sorter: true }
         ],
         customCols: (data, item, fieldName, { onEdit, ...restCol }) => {
            let customCell = undefined

            const linkto = (a) => {
               let paramsUrl = zzControlHelper.btoaUTF8(a.params)
               const route = a.route.replace(":id", paramsUrl)
               return route
            }
            if (fieldName === 'ma_loai_bang_luong') {
               customCell = <Table.Cell key={fieldName} singleLine>
                  <ZenLink style={{ ...restCol.style }}
                     to={linkto({ route: routes.PADmLoaiBangLuongDetail(), params: item.ma_loai_bang_luong })}
                     permission=""
                  >
                     {item.ma_loai_bang_luong}
                  </ZenLink>
               </Table.Cell>;
            }

            if (fieldName === 'id_cctcs') {
               const loai = optionBoPhan.find(t => t.value == item["id_cctcs"]) || {}
               customCell = <Table.Cell key={fieldName}>
                  {loai.text || item["id_cctcs"]}
               </Table.Cell>
            }
            return customCell
         }
      },
   },

   PABangLuong: {
      route: routes.PABangLuong,
      action: {
         view: { visible: true, permission: "" },
         add: { visible: true, permission: permissions.PABangLuongTaoVaTinhLuong },
         edit: { visible: true, permission: "" },
         del: { visible: true, permission: "" },
      },

      linkHeader: {
         id: "pabangluong",
         defaultMessage: "Bảng lương",
         active: true,
         isSetting: false,
      },

      tableList: {
         keyForm: "PABangLuong",
         formModal: ZenLookup.PABangLuong,
         fieldCode: "id",
         unPagination: false,
         duplicate: true,
         changeCode: false,
         showFilterLabel: true,

         api: {
            url: ApiPABangLuong,
            type: "sql"
         },

         columns: [
            { id: "pabangluong.ten_ky_luong", defaultMessage: "Kỳ lương", fieldName: "ten_ky_luong", filter: "string", sorter: true, editForm: true, custom: true },
            { id: "pabangluong.ten_loai_bang_luong", defaultMessage: "Loại bảng lương", fieldName: "ten_loai_bang_luong", filter: "string", sorter: true, },
            { id: "pabangluong.id_cctcs", defaultMessage: "Bộ phận", fieldName: "id_cctcs", filter: "string", sorter: true, },
            { id: "pabangluong.dien_giai", defaultMessage: "Diễn giải", fieldName: "dien_giai", filter: "string", sorter: true, },
            { id: "pabangluong.trang_thai", defaultMessage: "Trạng thái", fieldName: "ten_trang_thai", filter: "string", sorter: true, },
            // { id: "ksd", defaultMessage: "Không sử dụng", fieldName: "ksd", type: "bool", filter: "", sorter: true }
         ],

         customCols: (data, item, fieldName, { onEdit, ...restCol }) => {
            let customCell = undefined

            const linkto = (a) => {
               let paramsUrl = zzControlHelper.btoaUTF8(a.params)
               const route = a.route.replace(":id", paramsUrl)
               return route
            }
            if (fieldName === 'ten_ky_luong') {
               customCell = <Table.Cell key={fieldName} singleLine>
                  <ZenLink style={{ ...restCol.style }}
                     to={linkto({ route: routes.PABangLuongDetail(), params: item.id })}
                     permission=""
                  >
                     {item.ten_ky_luong}
                  </ZenLink>
               </Table.Cell>;
            }
            return customCell
         }
      },
   },

   PAHangSo: {
      route: routes.PAHangSo,
      action: {
         view: { visible: true, permission: permissions.PAHangSoXem },
         add: { visible: true, permission: permissions.PAHangSoThem },
         edit: { visible: true, permission: permissions.PAHangSoSua },
         del: { visible: true, permission: permissions.PAHangSoXoa },
      },

      linkHeader: {
         id: "pahangso",
         defaultMessage: "Cập nhật hằng số",
         active: true,
         isSetting: false,
      },

      tableList: {
         ContainerTop: PAHangSoFilter,
         keyForm: "PAHangSo",
         formModal: ZenLookup.PAHangSo,
         fieldCode: "ma_ky_luong",
         multiKey: ["ma_ky_luong", "id_nv"],
         unPagination: false,
         changeCode: false,
         showFilterLabel: true,
         
         api: {
            url: ApiPAHangSo,
            type: "sql",
            params: ["ma_ky_luong", "id_nv"],
         },

         columns: [
            { id: "PAHangSo.ten_ky_luong", defaultMessage: "kỳ lương", fieldName: "ten_ky_luong", filter: "string", sorter: true, editForm: true, },
            { id: "PAHangSo.ma_nv", defaultMessage: "Mã nhân viên", fieldName: "id_nv", filter: "string", sorter: true, },
            { id: "PAHangSo.ho_ten", defaultMessage: "Tên nhân viên", fieldName: "ho_ten", filter: "string", sorter: true, },
         ],
      },
   },
}

export default PA_Dictionary