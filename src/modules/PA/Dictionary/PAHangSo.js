import React, { useEffect, useState } from "react";
import { ZenField, ZenFieldSelectApi, ZenFieldNumber, ZenFieldSelect } from "../../../components/Control/index";
import { FormMode } from "../../../utils";
import { Form } from "semantic-ui-react";
import { ApiPADmLoaiBangLuongKm, ApiPAHangSo } from "../Api";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import DatePicker from "react-datepicker";
import { zzControlHelper } from "../../../components/Control/zzControlHelper";
import moment from "moment";
import { GlobalStorage, KeyStorage } from "../../../utils/storage";
import * as permissions from "../../../constants/permissions";
import NumberFormat from "react-number-format";

const PAHangSoModal = (props) => {
    const { formik, permission, mode, infoModal } = props
    const [kms, setKms] = useState()

    useEffect(() => {
        if (mode === 1) {
            ApiPADmLoaiBangLuongKm.getByCode(formik.values.id_nv, res => {
                if (res.status === 200) {
                    setKms(res.data.data)
                }
            })
        } else {
            ApiPAHangSo.getHangSo(formik.values, res => {
                if (res.status === 200) {
                    setKms(res.data.data)
                }
            })
        }
    }, [formik.values.id_nv])

    useEffect(() => {
        if (kms) {
            formik.setFieldValue("kms", kms)
        }
    }, [kms])

    function getfianceyear() {
        return GlobalStorage.getByField(KeyStorage.Global, 'financial_year')
    }

    const handleselect = (item, { name }) => {
        if (name == 'ma_ky_luong') {
            formik.setFieldValue('ten_ky_luong', item.ten_ky_luong)
        } else if (name == 'id_nv') {
            formik.setFieldValue('ho_ten', item.ho_ten)
        }
    }

    const handleChangeKM = (item, idx, e) => {
        const { value, name } = e.target
        item[idx][name] = value;
        formik.setValues({
            ...formik.values,
            kms: item
        })
    }

    return <>
        <ZenFieldSelectApi
            readOnly={mode == 1 ? !permission : true}
            lookup={{
                ...ZenLookup.Ma_ky_luong,
                where: `nam = '${getfianceyear()}'`,
                onLocalWhere: (items) => {
                    return items.filter(t => t.nam == getfianceyear())
                }
            }}
            label="paHangSo.ma_ky_luong" defaultlabel="Kỳ lương"
            name="ma_ky_luong"
            formik={formik}
            onItemSelected={(item, { name }) => handleselect(item, { name })}
        />
        <ZenFieldSelectApi
            readOnly={mode == 1 ? !permission : true}
            lookup={ZenLookup.ID_NV}
            label="paHangSo.id_nv" defaultlabel="Nhân sự"
            name="id_nv"
            formik={formik}
            onItemSelected={(item, { name }) => handleselect(item, { name })}
        />
        {
            formik.values && formik.values.kms && formik.values?.kms.map((item, index) => {
                return <Form.Group widths={"equal"}>
                    <Form.Field fixedDecimalScale
                        style={{ textAlign: 'right' }}
                        label={item['ten_km'] || "Tên Khoản mục"}
                        control={NumberFormat}
                        placeholder='Giá trị'
                        name='gia_tri'
                        value={item['gia_tri'] ? item['gia_tri'] : null}
                        onKeyPress={(event) => {
                            if (!/[0-9]/.test(event.key)) {
                                event.preventDefault();
                            }
                        }}
                        onChange={(e) => {
                            let newValue = e.target.value
                            newValue = zzControlHelper.replaceAll(newValue, ".", '')
                            newValue = zzControlHelper.replaceAll(newValue, ",", '.')
                            // newValue = zzControlHelper.replaceAll(newValue, rest.suffix, '')
                            let floatValue = newValue ? Number(newValue) : null
                            e.target.value = floatValue
                            handleChangeKM(formik.values.kms, index, e)
                        }}
                        thousandSeparator="."
                        decimalSeparator=","
                        decimalScale={0}
                        maxLength={25}
                    >
                    </Form.Field>
                    <Form.Field>
                        <label>Ghi chú</label>
                        <Form.Input readOnly={!permission}
                            placeholder='Ghi chú'
                            name='ghi_chu'
                            value={item['ghi_chu']}
                            onChange={(e, data) => handleChangeKM(formik.values.kms, index, e, data)}
                        />
                    </Form.Field>
                </Form.Group>
            })
        }
    </>
}

const PAHangSo = {
    FormModal: PAHangSoModal,
    api: {
        url: ApiPAHangSo,
    },
    permission: {
        view: permissions.PAHangSoXem,
        add: permissions.PAHangSoThem,
        edit: permissions.PAHangSoSua
    },
    formId: "PAHangSo-form",
    size: "tiny",
    isScrolling: false,
    initItem: {
        id_nv: "",
        ma_ky_luong: "",
        kms: [],
    },
    formValidation: [
        {
            id: "id_nv",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ]
        },
        {
            id: "ma_ky_luong",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ]
        }
    ]
}

export { PAHangSo };