import React, { useEffect } from "react";
import { ZenField, ZenFieldSelectApi, ZenFieldNumber, ZenFieldSelect } from "../../../components/Control/index";
import { FormMode } from "../../../utils";
import { Form } from "semantic-ui-react";
import { ApiPADieuChinh } from "../Api";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import DatePicker from "react-datepicker";
import { zzControlHelper } from "../../../components/Control/zzControlHelper";
import moment from "moment";
import { GlobalStorage, KeyStorage } from "../../../utils/storage";
import * as permissions from "../../../constants/permissions";

const PADieuChinhModal = (props) => {
    const { formik, permission, mode,infoModal } = props

    function getfianceyear () {
        return GlobalStorage.getByField(KeyStorage.Global,'financial_year')
    }

    const handleselect = (item, { name }) => {
        if (name == 'ma_ky_luong') {
            formik.setFieldValue('ten_ky_luong', item.ten_ky_luong)
        } else if (name == 'id_nv') {
            formik.setFieldValue('ten_nv', item.ho_ten)
        }
    }

    return <>
        <ZenFieldSelectApi
            readOnly={!permission}
            lookup={{...ZenLookup.Ma_ky_luong,
                where:`nam = '${getfianceyear()}'`,
                onLocalWhere: (items) => {
                    return items.filter(t => t.nam == getfianceyear())
                }
            }}
            label="padieuchinh.ma_ky_luong" defaultlabel="Kỳ lương"
            name="ma_ky_luong"
            formik={formik}
            onItemSelected={(item, { name }) => handleselect(item, { name })}
        />
        <ZenFieldSelectApi
            readOnly={!permission}
            lookup={ZenLookup.ID_NV}
            label="padieuchinh.id_nv" defaultlabel="Nhân sự"
            name="id_nv"
            formik={formik}
            onItemSelected={(item, { name }) => handleselect(item, { name })}
        />
        <Form.Group>
            <ZenFieldSelect width={8}
                readOnly={!permission}
                options={[
                    { key: 0, value: "T", text: "Tăng" },
                    { key: 1, value: "G", text: "Giảm" },
                ]}
                label="padieuchinh.loai_dc" defaultlabel="Loại điều chỉnh"
                name="loai_dc"
                props={formik} clearable={false}
            />
            <ZenFieldNumber width={8} name="gia_tri" readOnly={!permission}
                decimalScale={2} props={formik}
                label={"padieuchinh.gia_tri"} defaultlabel="Giá trị"
            />
        </Form.Group>
        <ZenField required autoFocus readOnly={!permission}
            label={"padieuchinh.ly_do"} defaultlabel="Lý do"
            name="ly_do" props={formik}
            maxLength={20}
        />
        <ZenField required autoFocus readOnly={!permission}
            label={"padieuchinh.ghi_chu"} defaultlabel="Ghi chú"
            name="ghi_chu" props={formik}
            maxLength={20}
        />
    </>
}

const PADieuChinh = {
    FormModal: PADieuChinhModal,
    api: {
        url: ApiPADieuChinh,
    },
    permission: {
        view: permissions.PADieuChinhXem,
        add: permissions.PADieuChinhThem,
        edit: permissions.PADieuChinhSua
    },
    formId: "PADieuChinh-form",
    size: "tiny",
    isScrolling: false,
    initItem: {
        id_nv: "",
        ma_ky_luong: "",
        ly_do: "",
        loai_dc: "",
        gia_tri: 0,
        ghi_chu: ""
    },
    formValidation: [
        {
            id: "id_nv",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ]
        },
        {
            id: "ma_ky_luong",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ]
        }
    ]
}

export { PADieuChinh };