import React from "react";
import { ZenField, ZenFieldDate, ZenFieldSelectApi, ZenFieldNumber } from "../../../components/Control/index";
import { FormMode } from "../../../utils";
import { ApiPADmKyLuong } from "../Api";
import { Form } from "semantic-ui-react";
import { ZenLookup } from '../../ComponentInfo/Dictionary/ZenLookup'
import * as permissions from "../../../constants/permissions";

const PADmKyLuongModal = (props) => {
   const { formik, permission, mode } = props

   return <React.Fragment>
      <ZenField required autoFocus readOnly={!permission || (mode === FormMode.ADD ? false : true)}
         label={"padmkyluong.ma_ky_luong"} defaultlabel="Mã kỳ lương"
         name="ma_ky_luong" props={formik} maxLength={20}
         isCode
      />
      <ZenField required readOnly={!permission}
         label={"padmkyluong.ten_ky_luong"} defaultlabel="Tên kỳ lương"
         name="ten_ky_luong" props={formik}
      />
      <ZenFieldSelectApi required readOnly={!permission}
         loadApi lookup={ZenLookup.Nam_tc}
         name="nam" formik={formik}
         label={"padmkyluong.nam"} defaultlabel="Năm"
      />
      <Form.Group>
         <ZenFieldDate required width={8} label="padmkyluong.ngay_bd" 
            defaultlabel="Ngày bắt đầu" name="ngay_bd" props={formik} />
         <ZenFieldDate required width={8} label="padmkyluong.ngay_kt" 
            defaultlabel="Ngày kết thúc" name="ngay_kt" props={formik} />
      </Form.Group>
      <ZenFieldNumber required autoFocus readOnly={!permission} decimalScale={1}
           label={"padmkyluong.ngay_chuan"} defaultlabel="Ngày công chuẩn"
           name="ngay_chuan" props={formik}
           maxLength={20}
      />
      <ZenField readOnly={!permission}
         label={"padmkyluong.ghi_chu"} defaultlabel="Ghi chú"
         name="ghi_chu" props={formik}
      />
   </React.Fragment>
}

const PADmKyLuong = {
   FormModal: PADmKyLuongModal,
   api: {
      url: ApiPADmKyLuong,
   },
   permission: {
      view: permissions.PADmKyLuongXem,
      add: permissions.PADmKyLuongThem,
      edit: permissions.PADmKyLuongSua
   },
   formId: "padmkyluong-form",
   size: "tiny",
   initItem: {
      ma_ky_luong: "",
      ten_ky_luong: "",
      ksd: false,
      nam: "",
      ngay_bd:"",
      ngay_kt: "",
      ngay_chuan: 0
   },
   formValidation: [
      {
         id: "ma_ky_luong",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
            {
               type: "max",
               params: [20, "Không được vượt quá 20 kí tự"]
            },
         ]
      },
      {
         id: "ten_ky_luong",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
         ]
      },
      {
         id: "ngay_bd",
         validationType: "date",
         validations: [
            {
                type: "required",
                params: ["Không được bỏ trống trường này"]
            },
         ]
      },
      {
         id: "ngay_kt",
         validationType: "date",
         validations: [
            {
                type: "required",
                params: ["Không được bỏ trống trường này"]
            },
         ]
      },
      {
         id: "nam",
         validationType: "string",
         validations: [
            {
                type: "required",
                params: ["Không được bỏ trống trường này"]
            },
         ]
      },
   ]
}

export { PADmKyLuong };