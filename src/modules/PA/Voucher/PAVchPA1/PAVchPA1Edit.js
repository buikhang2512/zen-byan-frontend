import React, { useContext, useEffect, useState, useRef } from 'react';
import { Form, Grid, Table, Button, Modal } from 'semantic-ui-react';
import { ButtonAdd, ZenField, ZenFieldDate, ZenFieldSelectApi, ZenMessageAlert, ZenMessageToast, ZenButton, ZenFormik, FormatNumber } from '../../../../components/Control';
import { ZenHelper } from '../../../../utils';
import { ApiPAVchPA1 } from '../../Api';
import {
    ContextVoucher, VoucherHelper,
    RowItemPH, NumberCell, DeleteCell, RowHeaderCell,
    ActionType, SelectCell, TableScroll, TableTotalPH, RowTotalPH
} from "../../../../components/Control/zVoucher";
import { ZenLookup } from '../../../ComponentInfo/Dictionary/ZenLookup';
import * as routes from '../../../../constants/routes';
import { useLocation } from "react-router-dom";
import { date } from 'yup';
import { zzControlHelper } from "../../../../components/Control/zzControlHelper";
import { GlobalStorage, KeyStorage } from "../../../../utils/storage";
import { FormMode } from "../../../../utils";
import * as permissions from "../../../../constants/permissions";
import { ApiReport } from '../../../../Api';
import { FormattedMessage, useIntl } from "react-intl";
import { auth } from "../../../../utils";

const PHForm = ({ formik, permission, modeForm }) => {
    const { ct, ph, onUpdatePHCT } = useContext(ContextVoucher)
    const [ngayct, setNgayct] = useState()
    let isReadOnly = VoucherHelper.f_ReadOnly(modeForm, permission)
    const location = useLocation()
    const [open, setOpen] = useState(false)
    useEffect(() => {
        // let tk = new URLSearchParams(location.state).get("tk")
        let makh = location.state
        if (makh) formik.setFieldValue("ma_kh", makh.ma_kh);
    }, [formik.values]);

    const handleChangeDatect = (e) => {
        formik.setFieldValue('ngay_ct', e.value)
        setNgayct(e.value)
    }

    const handleSelectedLookup = (itemSelected, { name }) => {
        if (name === 'ma_ky_luong') {
            ph.ma_ky_luong = itemSelected.ma_ky_luong;
            ph.ten_ky_luong = itemSelected.ten_ky_luong;
            ph.ngay_bd = itemSelected.ngay_bd;
        }
        if (name === 'ma_kh') {
            ph.ma_kh = itemSelected.ma_kh;
            ph.ten_kh = itemSelected.ten_kh;
            formik.setFieldValue('ma_kh', itemSelected.ma_kh);
        }
        onUpdatePHCT(ph, ct)
    }

    let questBefore = (mess, callBack) => {
        let result = ZenMessageAlert.question(mess);
        Promise.all([result])
            .then(values => {
                if (values[0]) {
                    callBack()
                }
            }).catch(err => {

            });
    }

    const handleChoChotCong = () => {
        ph.trang_thai = "0"
        ph.Cts = ct
        ApiPAVchPA1.trangthai({ stt_rec: ph.stt_rec, trang_thai: ph.trang_thai }, res => {
            if (res.status >= 200 && res.status <= 204) {
                ZenMessageToast.success();
                onUpdatePHCT(ph)
                formik.setFieldValue("trang_thai", "0")
            } else {
                ZenMessageAlert.error(zzControlHelper.getResponseError(res))
            }
        })
    }

    const handleChotCong = () => {
        ph.trang_thai = "5"
        ph.Cts = ct
        ApiPAVchPA1.trangthai({ stt_rec: ph.stt_rec, trang_thai: ph.trang_thai }, res => {
            if (res.status >= 200 && res.status <= 204) {
                ZenMessageToast.success();
                onUpdatePHCT(ph)
                formik.setFieldValue("trang_thai", "5")
            } else {
                ZenMessageAlert.error(zzControlHelper.getResponseError(res))
            }
        })
    }

    const handleTongHopChamCong = () => {
        if (ct && ct.length > 0) {
            questBefore("Hệ thống sẽ thực hiện tổng hợp dữ liệu từ bảng chấm công và thay thế dữ liệu đang có, bạn chắc chắn thực hiện không?", res => {
                TongHopChamCong(ct, ph)
            })
        } else {
            TongHopChamCong(ct, ph)
        }
    }

    function TongHopChamCong(ct, ph) {

        if (ph.ma_ky_luong == null || ph.ma_ky_luong == "") {
            ZenMessageAlert.warning('Vui lòng chọn kỳ công/lương trước khi thực hiện')
            return
        }
        if (ph.ma_kh == null || ph.ma_kh == "") {
            ZenMessageAlert.warning('Vui lòng chọn mã khách hàng trước khi thực hiện')
            return
        }
        ApiPAVchPA1.tonghop({ ma_kh: ph.ma_kh, ma_ky_luong: ph.ma_ky_luong }, res => {

            if (res.status >= 200 && res.status <= 204) {
                ZenMessageToast.success();
                ph.t_tong_gio = VoucherHelper.f_SumArray(res.data.data, 'tong_gio');
                onUpdatePHCT(ph, res.data.data)
            } else {
                ZenMessageAlert.error(zzControlHelper.getResponseError(res))
            }
        })
    }

    let globalStorage = GlobalStorage.get(KeyStorage.Global);

    return <React.Fragment>
        <Grid columns="3">
            <RowItemPH content={[
                {
                    width: 6,
                    input: <ZenFieldSelectApi required loadApi
                        tabIndex={1}
                        lookup={ZenLookup.Ma_kh}
                        label={"ardmkh.header"} defaultlabel="Khách hàng"
                        name="ma_kh" formik={formik}
                        onItemSelected={handleSelectedLookup}
                        readOnly={isReadOnly}
                    />
                },
                {
                    width: 6,
                    input: <ZenFieldSelectApi required loadApi
                        tabIndex={2}
                        lookup={{ ...ZenLookup.Ma_ky_luong, where: "nam = " + globalStorage.financial_year }}
                        label={"pa1.ma_ky_luong"} defaultlabel="Kỳ"
                        name="ma_ky_luong" formik={formik}
                        onItemSelected={handleSelectedLookup}
                        readOnly={isReadOnly}
                    />
                },
                {
                    width: 4,
                    input: <>
                        <Form.Group>
                            <ZenField width={8} readOnly={isReadOnly} tabIndex={3}
                                label={"pa1.so_ct"} defaultlabel="Số CT"
                                name="so_ct" props={formik}
                            />
                            <ZenFieldDate width={8} required readOnly={isReadOnly} tabIndex={3}
                                label={"pa1.ngay_ct"} defaultlabel="Ngày chứng từ"
                                name="ngay_ct" props={formik}
                                onChange={handleChangeDatect}
                            />
                        </Form.Group>
                    </>
                },
            ]} />
            <RowItemPH content={[
                {
                    width: 12,
                    input: <ZenField readOnly={isReadOnly} tabIndex={1}
                        name="dien_giai" props={formik}
                        label={"pa1.dien_giai"} defaultlabel="Diễn giải/ghi chú" />
                },
                {
                    width: 4,
                    input: <ZenFieldSelectApi required loadApi
                        tabIndex={2}
                        lookup={{ ...ZenLookup.trang_thai, where: "ma_ct = 'PA1'" }}
                        label={"pa1.trang_thai"} defaultlabel="Trạng thái"
                        name="trang_thai" formik={formik}
                        onItemSelected={handleSelectedLookup}
                        readOnly={(modeForm === FormMode.EDIT ? false : true)}
                    />
                },
            ]} />
            <RowItemPH content={[
                {
                    width: 16,
                    input: <Form.Group>
                        <ZenField readOnly={isReadOnly} tabIndex={1} width={4}
                            name="nguoi_nhan" props={formik}
                            label={"pa1.nguoi_nhan"} defaultlabel="Người nhận" />
                        <ZenFieldSelectApi required
                            tabIndex={1} width={4}
                            lookup={{ ...ZenLookup.ContactKHEmail, where: `ma_kh = '${ph.ma_kh}'` }}
                            label={"pa1.email_nhan"} defaultlabel="Email nhận"
                            name="email_nhan" formik={formik}
                            readOnly={isReadOnly}
                        />
                        <ZenFieldDate readOnly={true} tabIndex={1} width={4}
                            name="tg_gui_email" props={formik}
                            label={"pa1.tg_gui_email"} defaultlabel="Thời gian gửi email" />
                        <ZenFieldDate readOnly={true} tabIndex={1} width={4}
                            name="tg_gui_tt" props={formik}
                            label={"pa1.tg_gui_tt"} defaultlabel="Thời gian gửi HSTT" />
                    </Form.Group>
                },
            ]} />
            <RowItemPH content={[
                {
                    width: 12,
                    input: <div style={{ float: "right", padding: '0px 0px 7px 0px' }}> </div>
                },
                {
                    width: 4, input: modeForm === FormMode.VIEW ? (ph.trang_thai == "5" ?
                        (auth.checkPermission(permissions.PA1ChoChotCong) ? <Button content="Chờ chốt công" primary type="submit" onClick={handleChoChotCong} /> : undefined) :
                        <Button content="Chốt công" primary type="submit" onClick={handleChotCong} />) :
                        <Button icon="calculator" content="Tổng hợp từ chấm công" primary type="submit" onClick={handleTongHopChamCong} />
                },
            ]} />

        </Grid>
    </React.Fragment>
}

const CTForm = ({ ph, ct, permission, modeForm }) => {
    const { onChangeCT, errorCT } = useContext(ContextVoucher)
    let isReadOnly = VoucherHelper.f_ReadOnly(modeForm, permission)

    if (ph.ngay_bd === '' || ph.ngay_bd == undefined) {
        ph.ngay_bd = new Date();
    }
    var date = new Date(ph.ngay_bd);

    const switchAction = async (propsElement, type, itemCurrent, index) => {
        const { name, value, itemSelected } = propsElement
        switch (type) {
            case ActionType.TEXT_CHANGE:
                const { lookup } = propsElement
                itemCurrent[name] = value
                if (itemSelected.id_nv) {
                    itemCurrent.ho_ten = itemSelected.ho_ten
                }
                const _totalPH = calcTotalPH(itemCurrent, index)
                onChangeCT(itemCurrent, index, type, _totalPH, propsElement)
                break;

            case ActionType.NUMBER_CHANGE:
                itemCurrent[name] = value
                f_calcNumber(propsElement, type, itemCurrent, index)
                break;

            case ActionType.ADD_ROW:
                let newCT = ({
                    id_nv: value,
                    ho_ten: itemSelected.ho_ten,
                    tong_gio: 0,
                })
                onChangeCT(newCT, null, type)
                break;

            case ActionType.DELETE_ROW:
                // xóa dòng CT, tính lại PH
                const ctDel = ct.filter((item, idx) => idx !== index)
                const totalPH = {
                    t_tong_gio: VoucherHelper.f_SumArray(ctDel, 'tong_gio')
                }
                onChangeCT(itemCurrent, index, type, totalPH)
                break;
            default:
                break;
        }
    }

    function f_calcNumber(propsElement, type, itemCurrent, index) {

        itemCurrent.tong_gio =
            (itemCurrent.ngay1_gio != null ? itemCurrent.ngay1_gio : 0) +
            (itemCurrent.ngay2_gio != null ? itemCurrent.ngay2_gio : 0) +
            (itemCurrent.ngay3_gio != null ? itemCurrent.ngay3_gio : 0) +
            (itemCurrent.ngay4_gio != null ? itemCurrent.ngay4_gio : 0) +
            (itemCurrent.ngay5_gio != null ? itemCurrent.ngay5_gio : 0) +
            (itemCurrent.ngay6_gio != null ? itemCurrent.ngay6_gio : 0) +
            (itemCurrent.ngay7_gio != null ? itemCurrent.ngay7_gio : 0) +
            (itemCurrent.ngay8_gio != null ? itemCurrent.ngay8_gio : 0) +
            (itemCurrent.ngay9_gio != null ? itemCurrent.ngay9_gio : 0) +
            (itemCurrent.ngay10_gio != null ? itemCurrent.ngay10_gio : 0) +
            (itemCurrent.ngay11_gio != null ? itemCurrent.ngay11_gio : 0) +
            (itemCurrent.ngay12_gio != null ? itemCurrent.ngay12_gio : 0) +
            (itemCurrent.ngay13_gio != null ? itemCurrent.ngay13_gio : 0) +
            (itemCurrent.ngay14_gio != null ? itemCurrent.ngay14_gio : 0) +
            (itemCurrent.ngay15_gio != null ? itemCurrent.ngay15_gio : 0) +
            (itemCurrent.ngay16_gio != null ? itemCurrent.ngay16_gio : 0) +
            (itemCurrent.ngay17_gio != null ? itemCurrent.ngay17_gio : 0) +
            (itemCurrent.ngay18_gio != null ? itemCurrent.ngay18_gio : 0) +
            (itemCurrent.ngay19_gio != null ? itemCurrent.ngay19_gio : 0) +
            (itemCurrent.ngay20_gio != null ? itemCurrent.ngay20_gio : 0) +
            (itemCurrent.ngay21_gio != null ? itemCurrent.ngay21_gio : 0) +
            (itemCurrent.ngay22_gio != null ? itemCurrent.ngay22_gio : 0) +
            (itemCurrent.ngay23_gio != null ? itemCurrent.ngay23_gio : 0) +
            (itemCurrent.ngay24_gio != null ? itemCurrent.ngay24_gio : 0) +
            (itemCurrent.ngay25_gio != null ? itemCurrent.ngay25_gio : 0) +
            (itemCurrent.ngay26_gio != null ? itemCurrent.ngay26_gio : 0) +
            (itemCurrent.ngay27_gio != null ? itemCurrent.ngay27_gio : 0) +
            (itemCurrent.ngay28_gio != null ? itemCurrent.ngay28_gio : 0) +
            (itemCurrent.ngay29_gio != null ? itemCurrent.ngay29_gio : 0) +
            (itemCurrent.ngay30_gio != null ? itemCurrent.ngay30_gio : 0) +
            (itemCurrent.ngay31_gio != null ? itemCurrent.ngay31_gio : 0)

        //cập nhật PH, CT
        VoucherHelper.f_Timeout(() => {
            const totalPH = calcTotalPH(itemCurrent, index)
            onChangeCT(itemCurrent, index, type, totalPH, propsElement)
            tongConggiocong(ct)
        })
    }

    function calcTotalPH(itemCurrent, index) {
        // tạo CT mới
        const newCT = ct.map((item, idx) => idx === index ? itemCurrent : item)
        // tính tổng PH
        const totalPH = {
            t_tong_gio: VoucherHelper.f_SumArray(newCT, 'tong_gio'),
        }
        return totalPH
    }

    function getDayofWeek(day) {
        var _switch = {
            0: 'CN',
            1: 'T2',
            2: 'T3',
            3: 'T4',
            4: 'T5',
            5: 'T6',
            6: 'T7',
        };
        return _switch[day]
    }

    function renderHeaderTable(date) {
        let result = [];
        const countDay = ZenHelper.getLastDayOfMonth(date.getMonth(), date.getFullYear())
        for (let i = 1; i <= countDay.getDate(); i++) {
            result.push(<Table.HeaderCell textAlign="center" className="sticky-left HRCCGV-sticky-top">{
                `${getDayofWeek(new Date(date.getFullYear(), date.getMonth(), i).getDay())}`}
                <br />
                {`${zzControlHelper.formatDateTime(new Date(date.getFullYear(), date.getMonth(), i), 'DD/MM')}`}
            </Table.HeaderCell>)
        }
        return result
    }
    const [tongcong, setTongcong] = useState([]);
    useEffect(() => {
        tongConggiocong(ct)
    }, [ct])

    function tongConggiocong(data) {
        let tongcong = [];
        data.map(item => {
            if (!tongcong[`tong_gio`]) {
                tongcong[`tong_gio`] = 0
            }
            tongcong[`tong_gio`] += item[`tong_gio`]
            for (let i = 1; i <= 31; i++) {
                if (!tongcong[`ngay${i}_gio`]) {
                    tongcong[`ngay${i}_gio`] = 0
                }
                if (i == 31) {
                    if (tongcong[`ngay${i}_gio`] >= 0) {
                        tongcong[`ngay${i}_gio`] += item[`ngay${i}_gio`]
                    }
                    return
                } else {
                    tongcong[`ngay${i}_gio`] += item[`ngay${i}_gio`]
                }
            }
        })
        setTongcong(tongcong)
    }

    function renderRowCT(item, index, error, date) {
        return <>
            <SelectCell lookup={{ ...ZenLookup.ID_NV, formatInput: `{id_nv} - {ho_ten}` }} error={error}
                name="id_nv" rowItem={item} index={index}
                placeholder={['pa1.id_nv', 'Mã id_nv']}
                readOnly={isReadOnly}
                onChange={switchAction}
            />

            <NumberCell name="tong_gio" decimalScale={2} width="60px"
                rowItem={item} index={index}
                placeholder={["pa1.tong_gio", "Tổng"]}
                onChange={switchAction} error={error}
                readOnly="true"
            />

            {renderCellngayX_gio(item, index, error, date)}

            {isReadOnly === false && <DeleteCell collapsing
                rowItem={item} index={index}
                onClick={switchAction} />}
        </>
    }

    function renderCellngayX_gio(item, index, error, date) {
        let result = [];
        const countDay = ZenHelper.getLastDayOfMonth(date.getMonth(), date.getFullYear())
        for (let i = 1; i <= countDay.getDate(); i++) {
            var name = "ngay" + i + "_gio";
            if (i < 10) i = "0" + i
            result.push(
                <NumberCell name={name} decimalScale={2} width="60px"
                    rowItem={item} index={index}
                    placeholder={[`ngay${i}_kh`, `${i}`]}
                    onChange={switchAction} error={error}
                    readOnly={isReadOnly}
                />
            )
        }
        return result
    }

    return <>
        {/* {reserr && <Message negative list={reserr} onDismiss={() => setReserr()} />} */}
        <TableScroll>
            <Table.Header>
                <Table.HeaderCell content="Giáo viên" textAlign="center" id="id_nv" className="sticky-left HRCCGV-sticky-top" />
                <Table.HeaderCell content="Tổng" textAlign="center" id="tong_gio" className="sticky-left HRCCGV-sticky-top" />
                {renderHeaderTable(date)}
            </Table.Header>
            <Table.Body>
                {tongcong && <Table.Row>
                    <Table.Cell style={{ whiteSpace: 'nowrap' }} className="sticky-left PADMLBL-sticky-top" ><strong>Tổng cộng</strong></Table.Cell>
                    <Table.Cell textAlign="center" className="HRCCGV-sticky-left PADMLBL-sticky-top" > <strong><FormatNumber value={tongcong[`tong_gio`]} /></strong> </Table.Cell>
                    {
                        (() => {
                            let tablecell = [];
                            for (let i = 1; i <= 31; i++) {
                                if (i == 31) {
                                    if (tongcong[`ngay${i}_gio`]) {
                                        tablecell.push(
                                            <Table.Cell textAlign="center" className="sticky-left PADMLBL-sticky-top" > <strong><FormatNumber value={tongcong[`ngay${i}_gio`]} /></strong> </Table.Cell>
                                        )
                                    }
                                } else {
                                    tablecell.push(
                                        <Table.Cell textAlign="center" className="sticky-left PADMLBL-sticky-top" > <strong><FormatNumber value={tongcong[`ngay${i}_gio`]} /></strong> </Table.Cell>
                                    )
                                }
                            }
                            return tablecell
                        })()
                    }
                </Table.Row>}
                {
                    ct && ct.map((item, index) => {
                        const error = errorCT ? errorCT.find(x => x.index === index) : undefined

                        return <Table.Row key={item.stt_rec0 ? item.stt_rec0 : ('REC' + index)}>
                            {renderRowCT(item, index, error, date)}
                        </Table.Row>
                    })
                }

                {isReadOnly === false && <Table.Row>
                    <SelectCell isAdd clearable={false} name="addRow"
                        lookup={{ ...ZenLookup.ID_NV, formatInput: `{id_nv} - {ho_ten}` }}
                        placeholder={['pa1.id_nv', 'Mã nhân viên']}
                        onChange={switchAction}
                    />
                    <Table.Cell colSpan={32} />
                </Table.Row>}
            </Table.Body>
        </TableScroll>
    </>
}

const CTFormTotal = ({ ph }) => {
    return <Grid columns="2">
        <Grid.Column width="8" />
        <Grid.Column width="8" textAlign="right">
            <TableTotalPH >
                <RowTotalPH text="Tổng giờ công" value={ph.t_tong_gio} />
            </TableTotalPH>
        </Grid.Column>
    </Grid>
}

const onValidCT = (ph, ct, currentRowCT) => {
    const { item, index } = currentRowCT
    const errorField = {}

    if (!item.id_nv) {
        errorField.id_nv = ["Mã nhân viên không được để trống"]
    }

    return Object.keys(errorField).length > 0 ? errorField : undefined
}

const handleBeforeEditItem = async (ph) => {
    if (ph.trang_thai == "5") {
        ZenMessageAlert.warning('Phiếu ở trạng thái chốt công không thể sửa')
    } else {
        return ph;
    }
}

const ModalNgayGuiHSTT = () => {
    const { ph, ct, onUpdatePHCT } = useContext(ContextVoucher);
    const refFormik = useRef();
    const [value, setValue] = useState()
    const [open, setOpen] = useState(false)

    const handleUpdate = (values) => {
        ph.tg_gui_tt = values.ngay_gui
        ph.Cts = ct
        ApiPAVchPA1.update(ph, res => {
            if (res.status >= 200 && res.status <= 204) {
                ZenMessageToast.success();
                onUpdatePHCT(ph)
            } else {
                ZenMessageAlert.error(zzControlHelper.getResponseError(res))
            }
        })
        setOpen(false)
    }

    return <Modal id="Filter-form"
        closeOnEscape closeIcon closeOnDimmerClick={false}
        onClose={() => setOpen(false)}
        onOpen={() => setOpen(true)}
        open={open} size={"tiny"}
        trigger={<Button size="small" icon="edit" primary content="Cập nhật ngày gửi HSTT" />}
    >
        <Modal.Header id='header-Filter-form' style={{ cursor: "grabbing" }}>
            Ngày gửi hồ sơ thanh toán
        </Modal.Header>

        <Modal.Content>
            <ZenFormik ref={refFormik} >
                {
                    formikProps => {
                        const { values } = formikProps
                        setValue(values)
                        return <Form>
                            <ZenFieldDate width={8} name="ngay_gui" props={formikProps}
                                required
                                label="ngay_gui" defaultlabel="Ngày gửi"
                            />
                        </Form>
                    }
                }
            </ZenFormik>
        </Modal.Content>

        <Modal.Actions>
            <ZenButton btnType={"cancel"} size="small" onClick={() => setOpen(false)} />
            <Button content="Cập nhật" size="small" primary onClick={() => handleUpdate(value)} />
        </Modal.Actions>
    </Modal>
}

const buttoncustom = () => {
    const { ph } = useContext(ContextVoucher);
    return <>
        {ph.trang_thai != "5" ? <ModalNgayGuiHSTT /> : undefined}
        <SendMailButton />
    </>
}

const SendMailButton = () => {
    const { ph, ct, onUpdatePHCT } = useContext(ContextVoucher);
    const [isLoading, setLoading] = useState(false);
    const htmlView = <div>
        <p style={{ fontWeight: "bold" }}>{`Bạng đang thực hiện gửi email chốt công?`}</p>
        <p>{`Khách hàng: ${ph.ma_kh}`}</p>
        <p>{`Người nhân: ${ph.nguoi_nhan}`}</p>
        <p>{`Email: ${ph.email_nhan}`}</p>
    </div>

    const handleUpdateTGGuiMail = () => {
        var currentdate = new Date();
        ph.tg_gui_email = currentdate;
        ph.Cts = ct
        ApiPAVchPA1.update(ph, res => {
            if (res.status >= 200 && res.status <= 204) {
                onUpdatePHCT(ph)
            } else {
                ZenMessageAlert.error(zzControlHelper.getResponseError(res))
            }
        })
    }

    const handleSendmail = async () => {
        if (ph.trang_thai == "5") {
            ZenMessageAlert.question(htmlView, true)
                .then(success => {
                    if (success == 1) {
                        if (ph.email_nhan != "" || ph.email_nhan != null) {
                            const params = { pMa_ct: "PA1", pStt_rec: ph.stt_rec, pEmail: ph.email_nhan }
                            setLoading(true);
                            if (params) {
                                const data = { codeReport: "PA1", params: params }
                                ApiReport.getReportSendMail(data, res => {
                                    if (res.status === 204) {
                                        ZenMessageAlert.success("Gửi mail kèm file báo cáo thành công");
                                        handleUpdateTGGuiMail();
                                        setLoading(false);
                                    } else {
                                        ZenMessageAlert.error(zzControlHelper.getResponseError(res)[0])
                                        setLoading(false);
                                    }
                                })
                            }
                            else {
                                ZenMessageAlert.error("Chưa khai báo code report hoặc params null")
                                setLoading(false);
                            }
                        }
                        else {
                            ZenMessageAlert.error("Chưa có địa chỉ email nhận")
                        }
                    }
                })
        } else {
            ZenMessageAlert.error("Chỉ gửi bảng chốt công khi phiếu ở trạng thái Đã chốt")
        }

    };

    return (<Button basic size="small" primary content="Gửi mail" icon="mail" onClick={handleSendmail} loading={isLoading} />);
};

export const PAVchPA1Edit = {
    PHForm,
    CTForm,
    CTFormTotal,
    onValidCT,
    ma_ct: 'PA1',
    formId: 'PAVchPA1',
    route: {
        add: routes.PAVchPA1New,
        edit: routes.PAVchPA1Edit()
    },
    ActionFooter: buttoncustom,
    linkHeader: {
        id: "pavchPA1",
        defaultMessage: "Bảng chốt giờ công với trường",
        route: routes.PAVchPA1,
        active: false,
    },
    onBeforeEditItem: handleBeforeEditItem,
    formId: "PAVchPA1",
    api: {
        url: ApiPAVchPA1,
    },
    action: {
        view: { visible: true, permission: permissions.PA1Xem },
        add: { visible: true, permission: permissions.PA1Them },
        edit: { visible: true, permission: permissions.PA1Sua },
        del: { visible: true, permission: permissions.PA1Xoa }
    },
    initItem: {
        stt_rec: "", ma_ct: "PA1", trang_thai: "0",
        ngay_ct: ZenHelper.formatDateTime(new Date(), "YYYY-MM-DD"),
        ngay_lct: ZenHelper.formatDateTime(new Date(), "YYYY-MM-DD"),
        ma_kh: "", ma_ky_luong: "", dien_giai: "", t_tong_gio: 0
    },
    formValidation: [
        {
            id: "ngay_ct",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ]
        },
        {
            id: "so_ct",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ],
        },
        {
            id: "ma_kh",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ]
        },
        {
            id: "ma_ky_luong",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ]
        },
    ]
}

