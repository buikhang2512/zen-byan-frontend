import React, { useEffect, useRef, useState } from "react";
import { Button, Form, Icon, Label, Modal, Table, Checkbox, Dropdown } from "semantic-ui-react";
import { ContextZenTableList, ZenButton, ZenDatePeriod, ZenField, ZenFieldSelectApi, ZenFormik, ZenMessageAlert, ZenLink, ZenMessageToast } from "../../../../components/Control";
import { ZenHelper } from "../../../../utils/global";
import * as routes from '../../../../constants/routes';
import { ZenLookup } from "../../../ComponentInfo/Dictionary/ZenLookup";
import { zzControlHelper } from "../../../../components/Control/zzControlHelper";
import { GlobalStorage, KeyStorage } from "../../../../utils/storage";
import { ApiPAVchPA1 } from "../../Api";
import { useContext } from "react";
import * as permissions from "../../../../constants/permissions";

const styles = {
    label: {
        marginTop: "0.14285714em",
        marginBottom: "0.14285714em"
    }
}

const ContainerFilter = (props) => {
    const [open, setOpen] = useState(false)
    const [data, setData] = useState()
    const [value, setValue] = useState()
    const [sql, setSql] = useState("1=1")
    const refFormik = useRef();
    const { onLoadData, fieldCode } = props;
    const globalStorage = GlobalStorage.get(KeyStorage.Global) || {};
    const initItem = {
        ngay1: globalStorage.from_date,
        ngay2: globalStorage.to_date,
    }
    // Set drag drop
    useEffect(() => {
        zzControlHelper.dragElement(document.getElementById('Filter-form'), 'header-Filter-form');
    })
    // Lắng nghe sự kiện khi có thay đổi query
    useEffect(() => {
        onLoadData(sql)
    }, [sql]);

    // Bắt sk mở lại modal setdata
    useEffect(() => {
        if (open) {
            refFormik.current.setValues({
                ...refFormik.current.values,
                ...data,
            })
        }
    }, [open])

    const handleSearch = (values) => {
        let _sql = "1=1";
        if (values.ngay1) _sql += ` AND ngay_ct >= '${values.ngay1}'`;
        if (values.ngay2) _sql += ` AND ngay_ct <= '${values.ngay2}'`;
        if (values.so_ct1) _sql += ` AND so_ct >= '${values.so_ct1}'`;
        if (values.so_ct2) _sql += ` AND so_ct <= '${values.so_ct2}'`;
        if (values.ma_kh) _sql += ` AND ma_kh = '${values.ma_kh}'`;
        if (values.ma_ky_luong) _sql += ` AND ma_ky_luong = '${values.ma_ky_luong}'`;
        if (values.ma_so_thue) _sql += ` AND ma_so_thue = '${values.ma_so_thue}'`;
        if (values.ma_httt) _sql += ` AND ma_httt = '${values.ma_httt}'`;
        if (values.dien_giai) _sql += ` AND dien_giai LIKE '%${values.dien_giai}%'`;
        if (_sql == '1=1') {
            setData(null)
        } else {
            setData(values)
        }
        setSql(_sql)
        setOpen(false)
    }

    const handleChangeDate = ({ startDate, endDate }) => {
        refFormik.current.setValues({
            ...refFormik.current.values,
            ngay1: startDate,
            ngay2: endDate
        })
    }

    const handleSelectKH = (itemSelected, { name }) => {
        refFormik.current.setValues({
            ...refFormik.current.values,
            ma_kh: itemSelected.ma_kh,
        })
    }


    const handleSelectKL = (itemSelected, { name }) => {
        refFormik.current.setValues({
            ...refFormik.current.values,
            ma_ky_luong: itemSelected.ma_ky_luong,
        })
    }

    const handleDelete = (e, i) => {
        if (i == 'all') {
            handleSearch("")
        } else {
            delete data[i]
            handleSearch(data)
        }
    }

    return <>
        <Modal id="Filter-form"
            closeOnEscape closeIcon closeOnDimmerClick={false}
            onClose={() => setOpen(false)}
            onOpen={() => setOpen(true)}
            open={open}
            size={"tiny"}
            trigger={<div style={{ display: "inline-block"}}><Button basic icon><Icon name="filter" link /></Button></div>}
        >
            <Modal.Header id='header-Filter-form' style={{ cursor: "grabbing" }}>
                Tìm kiếm
            </Modal.Header>

            <Modal.Content>
                <ZenFormik ref={refFormik}
                    initItem={initItem}
                >
                    {
                        formikProps => {
                            const { values } = formikProps
                            setValue(values)

                            return <Form>
                                <ZenDatePeriod
                                    onChange={handleChangeDate}
                                    value={[values.ngay1, values.ngay2]}
                                    textLabel="Từ ngày - đến ngày"
                                    defaultPopupYear={ZenHelper.getFiscalYear()}
                                />

                                <Form.Group widths="equal">
                                    <ZenField label={"pa1.so_ct1"} defaultlabel="Số chứng từ"
                                        name="so_ct1" props={formikProps}
                                    />
                                    <ZenField label={"pa1.so_ct2"} defaultlabel="Đến số"
                                        name="so_ct2" props={formikProps}
                                    />
                                </Form.Group>
                                <Form.Group>
                                    <ZenFieldSelectApi width={16} loadApi
                                        lookup={ZenLookup.Ma_kh}
                                        label={"pa1.ma_kh"} defaultlabel="Khách hàng"
                                        name="ma_kh" formik={formikProps}
                                        onItemSelected={handleSelectKH}
                                    />
                                </Form.Group>
                                <Form.Group>
                                    <ZenFieldSelectApi width={16} loadApi
                                        lookup={ZenLookup.Ma_ky_luong}
                                        label={"pa1.ma_ky_luong"} defaultlabel="Kỳ lương"
                                        name="ma_ky_luong" formik={formikProps}
                                        onItemSelected={handleSelectKL}
                                    />
                                </Form.Group>
                                <ZenField
                                    label={"pa1.dien_giai"} defaultlabel="Diễn giải"
                                    name="dien_giai" props={formikProps} />
                            </Form>
                        }
                    }
                </ZenFormik>
            </Modal.Content>

            <Modal.Actions>
                <ZenButton btnType={"cancel"} size="small" onClick={() => setOpen(false)} />
                <Button content="Tìm" icon="search" size="small"
                    primary
                    onClick={() => handleSearch(value)}
                />
            </Modal.Actions>
        </Modal>
        {data && data.ngay1 && <Label as='a' color={'grey'} style={styles.label}>Từ ngày : {ZenHelper.formatDateTime(data.ngay1,'DD/MM/YYYY')}<Icon name='delete' onClick={(e) => handleDelete(e, 'ngay1')} /></Label>}
        {data && data.ngay2 && <Label as='a' color={'grey'} style={styles.label}>Đến ngày : {ZenHelper.formatDateTime(data.ngay2,'DD/MM/YYYY')}<Icon name='delete' onClick={(e) => handleDelete(e, 'ngay2')} /></Label>}
        {data && data.so_ct1 && <Label as='a' color={'grey'} style={styles.label}>Từ số : {data.so_ct1}<Icon name='delete' onClick={(e) => handleDelete(e, 'so_ct1')} /></Label>}
        {data && data.so_ct2 && <Label as='a' color={'grey'} style={styles.label}>Đến số : {data.so_ct2}<Icon name='delete' onClick={(e) => handleDelete(e, 'so_ct2')} /></Label>}
        {data && data.ma_kh && <Label as='a' color={'grey'} style={styles.label}>Khách hàng : {data.ma_kh}<Icon name='delete' onClick={(e) => handleDelete(e, 'ma_kh')} /></Label>}
        {data && data.ma_ky_luong && <Label as='a' color={'grey'} style={styles.label}>Kỳ lương : {data.ma_ky_luong}<Icon name='delete' onClick={(e) => handleDelete(e, 'ma_ky_luong')} /></Label>}
        {data && data.dien_giai && <Label as='a' color={'grey'} style={styles.label}>Diễn giải : {data.dien_giai}<Icon name='delete' onClick={(e) => handleDelete(e, 'dien_giai')} /></Label>}
        {data && JSON.stringify(data) !== '{}' && <a style={{ cursor: "pointer", fontWeight: "bold", margin: "5px" }} onClick={(e) => handleDelete(e, 'all')}>Xóa điều kiện lọc</a>}
    </>
}

const AddActionBulk = () => {
    const { data, onSetState, onCloseCheck, onLoadData } = useContext(ContextZenTableList)

    const handleClick = (e, { value }) => {
        const dataSelected = data.filter(t => t.checked)

        if (dataSelected.length == 0) {
            ZenMessageAlert.info("Vui lòng chọn dòng cần thực hiện")
        }
        else {
            if (value === "CHOT") {
                var datachot = []
                dataSelected.map((item) => { datachot.push({ stt_rec: item.stt_rec, trang_thai: "5" }) });
                ApiPAVchPA1.trangthailist(datachot, res => {
                    if ((res.status >= 200 && res.status <= 204)) {
                        onLoadData({});
                        if (res.data.data != "")
                            ZenMessageAlert.error(res.data.data, true)
                        else
                            ZenMessageAlert.success()
                    }
                    onSetState({ loadingForm: false });
                })
            }
            if (value === "CHOCHOT") {
                var datachochot = []
                dataSelected.map((item) => { datachochot.push({ stt_rec: item.stt_rec, trang_thai: "0" }) });
                ApiPAVchPA1.trangthailist(datachochot, res => {
                    if ((res.status >= 200 && res.status <= 204)) {
                        onLoadData({});
                        if (res.data.data != "")
                            ZenMessageAlert.error(res.data.data, true)
                        else
                            ZenMessageAlert.success()
                    }
                    onSetState({ loadingForm: false });
                })
            }
            if (value === "TONGHOP") {
                var datatonghop = []
                dataSelected.map((item) => { datatonghop.push({ stt_rec: item.stt_rec, ma_kh: item.ma_kh, ma_ky_luong: item.ma_ky_luong }) });
                ApiPAVchPA1.tonghoplist(datatonghop, res => {
                    if ((res.status >= 200 && res.status <= 204)) {
                        if (res.data.data != "")
                            ZenMessageAlert.error(res.data.data, true)
                        else
                            ZenMessageAlert.success()
                    }
                    onSetState({ loadingForm: false });
                })
            }

            onCloseCheck();
            onSetState({ loadingForm: true });
        } 
    }

    return <>
        <Dropdown.Item value="CHOT" icon="check" content="Chốt công" onClick={handleClick} />
        <Dropdown.Item value="CHOCHOT" icon="check" content="Chờ Chốt công" onClick={handleClick} />
        <Dropdown.Item value="TONGHOP" icon="check" content="Tổng hợp giờ công" onClick={handleClick} />
    </>
}

export const PAVchPA1List = {
    route: routes.PAVchPA1,

    action: {
        view: {visible:true, permission: permissions.PA1Xem},
        add: {visible:true, permission: permissions.PA1Them, link: {route: routes.PAVchPA1New}},
        edit: {visible: true, permission: permissions.PA1Sua, link: {route: routes.PAVchPA1Edit(), params: "stt_rec"}},
        del: {visible: true, permission: permissions.PA1Xoa},
    },

    linkHeader: {
        id: "PAvchpa1",
        defaultMessage: "Bảng chốt giờ công với trường",
        active: true
    },

    tableList: {
        hasChecked: true,
        AddActionBulk: AddActionBulk,
        unPagination: false, // Phân trang
        fieldCode: "stt_rec", // khóa chính
        ma_ct: 'PA1', //Mã chứng từ 
        ContainerTop: ContainerFilter,

        api: {
            url: ApiPAVchPA1,
            type: "sql",
            sort: 'ngay_ct DESC'
        },

        columns: [
            { id: "pa1.ma_ky_luong", defaultMessage: "Mã kỳ", fieldName: "ma_ky_luong", filter: "string", sorter: true, link: { route: routes.PAVchPA1Edit(), params: "stt_rec" } },
            { id: "pa1.ten_ky_luong", defaultMessage: "Tên kỳ", fieldName: "ten_ky_luong", filter: "string", sorter: true, link: { route: routes.PAVchPA1Edit(), params: "stt_rec" } },
            { id: "pa1.ngay_bd", defaultMessage: "Ngày bắt đầu", fieldName: "ngay_bd", filter: "date", sorter: true, },
            { id: "pa1.ngay_kt", defaultMessage: "Ngày kết thúc", fieldName: "ngay_kt", filter: "date", sorter: true, },
            { id: "pa1.ngay_ct", defaultMessage: "Ngày lập", fieldName: "ngay_ct", filter: "date", sorter: true, },
            { id: "pa1.ma_kh", defaultMessage: "Khách hàng", fieldName: "ma_kh", filter: "string", sorter: true, custom: true },
            { id: "pa1.t_tong_gio", defaultMessage: "Tổng giờ công", fieldName: "t_tong_gio", filter: "number", sorter: true, },
            { id: "pa1.ten_trang_thai", defaultMessage: "Trạng thái", fieldName: "ten_trang_thai", filter: "string", sorter: true, },
        ],

        customCols: (data, item, fieldName, { onEdit, ...restCol }) => {
            let customCell = undefined

            const linkto = (a) => {
                let paramsUrl = zzControlHelper.btoaUTF8(a.params)
                const route = `${a.route}/${paramsUrl}`
                return route
            }

            if (fieldName === 'ma_kh') {
                customCell = <Table.Cell key={fieldName} singleLine>
                    <ZenLink style={{ ...restCol.style }}
                        to={linkto({ route: routes.ARDmkh, params: item.ma_kh })}
                        permission=""
                    >
                        {item.ma_kh}
                    </ZenLink>
                </Table.Cell>;
            }
            return customCell
        }

    }
}