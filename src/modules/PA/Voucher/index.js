import { PAVchPA1List, PAVchPA1Edit } from './PAVchPA1/index'

export const PA_Voucher = {
    PA1: PAVchPA1List,
}

export const PA_VoucherEdit = {
    PA1: PAVchPA1Edit,
}