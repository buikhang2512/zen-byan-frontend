import React, { Component } from "react";
import _ from "lodash";
import { axios } from "../../../Api";
import { ZenApp } from "../../../utils";
import { Helmet } from "../../../components/Control";
import { Header, Segment, Message } from "semantic-ui-react";
import { SystemAbout, SystemReleases } from "./index";

function getJson(url, callback) {
   return fetch(url, {
      headers: {
         // 'Authorization': 'bearer ' + auth.getToken(),
         'Access-Control-Allow-Credentials': true,
         'Access-Control-Allow-Origin': '*',
         'Content-Type': 'application/json'
      }
   })
      .then((response) => response.json())
      .then((responseJson) => callback({ success: true, data: responseJson }))
      .catch((error) => callback({ success: false, data: error }));
}

class SystemInfo extends Component {
   constructor(props) {
      super();
      this.urlAbout = ZenApp.baseUrl + 'about.json'
      this.urlReleases = ZenApp.baseUrl + 'releases.json'

      this.state = {
         loading: false,
         error: false,
         errorList: [],

         about: {},
         releases: []
      }
   }

   // componentDidMount() {
   //    const about = new Promise((resolve, reject) => {
   //       getJson(this.urlAbout, res => {
   //          if (res.success) {
   //             resolve(res.data)
   //          } else {
   //             reject(res)
   //          }
   //       })
   //    })

   //    const releases = new Promise((resolve, reject) => {
   //       getJson(this.urlReleases, res => {
   //          if (res.success) {
   //             resolve(res.data)
   //          } else {
   //             reject(res)
   //          }
   //       })
   //    })

   //    Promise.all([about, releases])
   //       .then(values => {
   //          this.setState({
   //             loading: false,
   //             error: false,
   //             about: values[0],
   //             releases: _.orderBy(values[1], ['date'], ['desc'])
   //          })
   //       })
   //       .catch(err => {
   //          this.setState({
   //             loading: false,
   //             error: true,
   //             errorList: [].concat('' + err.data)
   //          })
   //       })
   // }

   render() {
      return (
         <React.Fragment>
            <Helmet idMessage="system.info" defaultMessage="Thông tin hệ thống" />
            <Header as={"h2"} content="Thông tin hệ thống" />
            {this.state.error && <Message negative list={this.state.errorList} />}
            <Segment>
               <Header as={"h3"} content="Thông tin tổ chức" />
               <p>ID công ty: </p>
               <p>Tên công ty: </p>
            </Segment>

            <SystemAbout data={this.state.about} />
            <SystemReleases data={this.state.releases} />
         </React.Fragment>
      );
   }
}

export default SystemInfo