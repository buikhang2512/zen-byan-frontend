import React from "react";
import { Segment, Header } from "semantic-ui-react";

const SystemAbout = props => {
   const { data } = props
   return <React.Fragment>
      <Segment>
         <Header as={"h3"} content="Thông tin hỗ trợ" />

         <p>
            Tài liệu hướng dẫn sử dụng: <a href={data.support_center}> {data.support_center}</a>
         </p>
         <p>
            Email: <a href="#"> {data.support_email}</a>
         </p>
         <p>
            Điện thoại: <b> {data.support_tel}</b>
         </p>
      </Segment>
   </React.Fragment>
}

export default SystemAbout