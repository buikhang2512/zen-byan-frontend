import React, { useState } from "react";
import { Segment, Header, Accordion, Icon, List } from "semantic-ui-react";
import { ZenHelper } from "../../../utils";

const SystemReleases = props => {
   const [activeIndex, setActiveIndex] = useState();
   const { data } = props
   const newData = (data && data.length > 0) ? data[0] : {}

   const handleClick = (e, titleProps) => {
      const { index } = titleProps
      const newIndex = (activeIndex === index) ? -1 : index
      setActiveIndex(newIndex)
   }

   return <React.Fragment>
      <Segment>
         <Header as={"h3"} content="Thông tin phần mềm" />
         <div>
            <p>
               Phiên bản hiện tại: {newData.version}
               <span style={{ paddingLeft: "200px" }}>Ngày phát hành: {ZenHelper.formatDateTime(newData.date, 'DD/MM/YYYY')}</span>
            </p>
         </div>

         <Header as={"h3"} content="Lịch sử phát hành" />

         {data && data.length > 0 && <Accordion fluid styled>
            {
               data.map((item, index) => {
                  return <React.Fragment key={item.date}>
                     <Accordion.Title
                        active={activeIndex === index}
                        index={index}
                        onClick={handleClick}
                     >
                        <Icon name='dropdown' />
                        Version: {item.version} <span style={{ padding: "20px" }}>Ngày phát hành: {ZenHelper.formatDateTime(item.date, 'DD/MM/YYYY')}</span>
                     </Accordion.Title>

                     <Accordion.Content active={activeIndex === index}>
                        <p><b>Tính năng mới:</b></p>
                        <List bulleted>
                           {
                              item.new.map((text, idx) => {
                                 return <List.Item key={idx}>{text}</List.Item>
                              })
                           }
                        </List>

                        <p><b>Cập nhật:</b></p>
                        <List bulleted>
                           {
                              item.update.map((text, idx) => {
                                 return <List.Item key={idx}>{text}</List.Item>
                              })
                           }
                        </List>

                        <p><b>Sửa lỗi:</b></p>
                        <List bulleted>
                           {
                              item.bug.map((text, idx) => {
                                 return <List.Item key={idx}>{text}</List.Item>
                              })
                           }
                        </List>
                     </Accordion.Content>
                  </React.Fragment>
               })
            }
         </Accordion>}
      </Segment>
   </React.Fragment >
}

export default SystemReleases