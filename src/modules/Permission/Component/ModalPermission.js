import React from "react";
import { Form, Modal, Message, Dimmer, Loader, Icon, Popup, Button } from "semantic-ui-react";
import { FormattedMessage } from 'react-intl';

import { MESSAGES } from '../../../utils/Messages';
import { ZenHelper, FormMode } from '../../../utils/global'
import { ButtonSave, ButtonCancel, ZenField, ZenFormik, ZenMessageToast, ZenFieldSelect, ZenFieldNumber, ZenFieldCheckbox } from "../../../components/Control/index";
import { ApiPermission } from "../api/ApiPermission";
import { ApiModule, ApiStored } from "../../../Api";

const btnSave = {
   Save: "save",
   SaveBasic: "saveBasic"
}

class ModalPermission extends React.PureComponent {
   isMounted = false
   constructor(props) {
      super();
      this.formRef = React.createRef();
      this.btnSave = ""

      this.state = {
         open: false,
         loadingForm: true,
         btnLoadingSave: false,
         error: false,
         errorList: [],
         items: initItem,
         modules: []
      }
   }

   loadModules() {
      return new Promise((resolve, reject) => {
         ApiModule.get(res => {
            if (res.status === 200) {
               resolve(ZenHelper.translateListToSelectOptions(res.data.data, false, 'id', 'id', 'id', null, "name"))
            } else {
               reject(res)
            }
         })
      })
   }

   loadData() {
      const { id, formMode } = this.props
      const modules = this.loadModules();

      const permission = new Promise((resolve, reject) => {
         ApiPermission.getByID(id, res => {
            if (res.status === 200) {
               resolve(res.data)
            } else {
               reject(res)
            }
         })
      })

      Promise.all([modules, permission])
         .then(values => {
            if (this.isMounted) {
               this.setState({
                  loadingForm: false,
                  error: false,
                  errorList: [],
                  items: values[1].data,
                  modules: values[0]
               })
            }
         }).catch(err => {
            this.state.errorList.push('' + err)
            this.setState({
               error: true,
               loadingForm: false
            })
         });
   }

   saveData = (item) => {
      const { formMode } = this.props

      if (formMode === FormMode.EDIT) {
         ApiPermission.update(item, res => {
            if (res.status >= 200 && res.status <= 204) {
               ZenMessageToast.success();
               this.afterSave(item)
            } else {
               this.state.errorList.push("" + res)
               this.setState({
                  loadingForm: false,
                  error: true,
               })
            }
         })
      } else {
         if (this.btnSave === btnSave.SaveBasic) {
            const qf = {
               sqlCommand: "zusCreatePermission",
               parameters: {
                  zId: item.id,
                  zModule_id: item.module_id,
                  zOrdinal: item.ordinal ? item.ordinal : 0,
                  zName: item.name
               }

            }
            ApiStored.get(qf, res => {
               if (res.status === 200) {
                  this.handleCloseForm();
                  ZenMessageToast.success();
               } else {
                  this.state.errorList.push("" + res)
                  this.setState({
                     loadingForm: false,
                     error: true,
                  })
               }
            })
         } else {
            ApiPermission.insert(item, res => {
               if (res.status >= 200 && res.status <= 204) {
                  ZenMessageToast.success();
                  this.afterSave(res.data.data)
               } else {
                  this.state.errorList.push("" + res)
                  this.setState({
                     loadingForm: false,
                     error: true,
                  })
               }
            })
         }

      }
   }

   afterSave = (item) => {
      this.props.onAfterSave ? this.props.onAfterSave(item) : this.handleCloseForm()
   }

   // ------------------------   component
   static getDerivedStateFromProps(props, state) {
      if (props.open !== state.open) {
         return {
            open: props.open,
            loadingForm: props.id ? true : false,
            items: initItem
         };
      }
      return null;
   }

   componentDidMount() {
      this.isMounted = true
      if (this.props.onRef) {
         this.props.onRef(this)
      }

      if (this.props.formMode === FormMode.EDIT || this.props.formMode === FormMode.DUPLICATE) {
         this.loadData()
      } else {
         const modules = this.loadModules();
         Promise.all([modules])
            .then(values => {
               if (this.isMounted) {
                  this.setState({
                     loadingForm: false,
                     error: false,
                     errorList: [],
                     modules: values[0]
                  })
               }
            }).catch(err => {
               this.state.errorList.push('' + err)
               this.setState({
                  error: true,
                  loading: false
               })
            });
      }
   }

   componentWillUnmount() {
      this.isMounted = false
      if (this.props.onRef) {
         this.props.onRef(undefined)
      }
   }

   // ------------------------   handle
   handleSaveForm = (values, form) => {
      if (form === 'form-nhomkh')
         this.saveData(values)
   }

   handleCloseForm = () => {
      this.props.onClose && this.props.onClose()
   }

   // ======================== use remote submit, outside Formik 
   handleRefSubmit = (e) => {
      this.btnSave = e.target.name
      this.formRef.current.handleSubmit(e);
   }

   render() {
      const { formMode, id, open, onCloseForm, onAfterSave, header, ...rest } = this.props
      const { loadingForm, error, errorList, items, btnLoadingSave } = this.state

      return (
         <React.Fragment>
            <Modal size='tiny' closeOnEscape closeIcon closeOnDimmerClick={false}
               open={open} onClose={this.handleCloseForm}>

               <Modal.Header>
                  <Icon name="folder open outline" />
                  {!id ? <FormattedMessage id={MESSAGES.CPControlAdd} defaultMessage="Thêm mới"
                     values={{ mess: header }} />
                     : header || ZenHelper.GetMessage(MESSAGES.CPControlEdit)}
               </Modal.Header>

               <Modal.Content>
                  {error && <Message negative list={errorList} />}

                  {loadingForm && <Dimmer inverted active={loadingForm}>
                     <Loader inverted />
                  </Dimmer>}
                  <ZenFormik form="form-nhomkh" ref={this.formRef}
                     validation={formValidation}
                     initItem={items}
                     onSubmit={this.handleSaveForm}
                  >
                     {formikProps => {
                        return <Form ref={this.formRef} id="modal-permission" onSubmit={this.handleSaveForm}>
                           <ZenField
                              label={"permission.id"} defaultlabel="ID"
                              name="id" required props={formikProps}
                              readOnly={formMode === FormMode.EDIT}
                           />

                           <ZenField
                              label={"permission.name"} defaultlabel="Name"
                              name="name" required props={formikProps}
                           />

                           <ZenField
                              label={"permission.parent_id"} defaultlabel="Parent ID"
                              name="parent_id" props={formikProps}
                           />

                           <ZenFieldSelect options={this.state.modules}
                              label={"permission.module_id"} defaultlabel="Module ID"
                              name="module_id" props={formikProps} />

                           <ZenFieldNumber name="ordinal"
                              decimalScale={2} props={formikProps} label="permission.ordinal" defaultlabel="Ordinal"
                           />
                           <ZenFieldCheckbox toggle style={{ zIndex: 0 }}
                              name="isinactive"
                              // onClick={this.handleHasVariant}
                              label={"permission.isinactive"} defaultlabel="In Active"
                              props={formikProps}
                           />
                        </Form>
                     }}
                  </ZenFormik>
               </Modal.Content>

               <Modal.Actions>
                  <ButtonCancel type="button" size="small" onClick={this.handleCloseForm} />
                  <ButtonSave type='submit' size="small" form="form-nhomkh"
                     loading={btnLoadingSave} onClick={this.handleRefSubmit}
                     name={btnSave.Save}
                  />
                  {formMode === FormMode.ADD && <Popup content="Xem, Thêm, Sửa, Xóa"
                     trigger={
                        <Button type='submit' size="small" form="modal-permission"
                           name={btnSave.SaveBasic}
                           content={"Lưu và tự tạo quyền cơ bản"}
                           loading={btnLoadingSave} onClick={this.handleRefSubmit}
                        />
                     }
                  />}
               </Modal.Actions>
            </Modal>
         </React.Fragment>
      );
   }
}
export default ModalPermission

ModalPermission.defaultProps = {
   onRef: undefined,      // call handle from parent component
   id: undefined,    // id of items
   open: false,      // close - open modal
   header: undefined,
   onClose: undefined, // event close modal
   onAfterSave: undefined  // event after save data
};

const initItem = {
   id: "",
   name: "",
   parent_id: "",
   ordinal: undefined,
   module_id: ""
}

const formValidation = [
   {
      id: "id",
      validationType: "string",
      validations: [
         {
            type: "required",
            params: ["Không được bỏ trống trường này"]
         },
      ]
   },
   {
      id: "name",
      validationType: "string",
      validations: [
         {
            type: "required",
            params: ["Không được bỏ trống trường này"]
         },
      ]
   },
]