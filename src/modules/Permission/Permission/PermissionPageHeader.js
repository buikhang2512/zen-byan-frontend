import React, { PureComponent } from 'react';
import * as routes from '../../../constants/routes';
import { Context } from './index';
import { ButtonAdd, HeaderLink, ButtonRefresh } from '../../../components/Control';
import { FormMode } from '../../../utils/global';
import { Button } from 'semantic-ui-react';

class PermissionPageHeader extends PureComponent {
   state = {
      listHeader: [{
         id: "setting.permission",
         defaultMessage: "Permissions",
         route: routes.Permission,
         active: true
      }]
   }

   render() {
      return (
         <Context.Consumer>
            {ctx => (
               <React.Fragment>
                  <HeaderLink listHeader={this.state.listHeader} isSetting />
                  <Button.Group floated="right" size="small">
                     <ButtonAdd onClick={() => ctx.handleAction(FormMode.ADD)} />
                     <ButtonRefresh basic color="green" onClick={ctx.handleRefresh()} />
                  </Button.Group>
                  <div style={{ clear: "both" }} />
               </React.Fragment>
            )}
         </Context.Consumer>
      );
   }
}

export default PermissionPageHeader;