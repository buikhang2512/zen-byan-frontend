import React, { Component } from 'react';
import { Message } from 'semantic-ui-react';
import { Context, PageHeader, TabMenu } from './index';
import { ApiPermission } from '../api/ApiPermission';
import { ConfirmDelete, Helmet, ZenMessageToast, SegmentHeader } from '../../../components/Control/index';
import { FormMode, ZenHelper } from '../../../utils/global';
import ModalPermission from '../Component/ModalPermission';

class Permission extends Component {
   constructor(props) {
      super();
      this.state = {
         activeTab: "list",
         loading: true,
         error: false,
         errorList: [],
         data: { data: [] },
         currentItem: {},
         formMode: FormMode.NON,

         changeTabItem: () => this.changeTabItem,
         handleAction: (action, data) => this.handleAction(action, data),
         handleRefresh: () => this.handleRefresh
      }
   }

   changeTabItem = (e, { name }) => {
      if (name === this.state.activeTab)
         return
      this.setState({ activeTab: name })
   }

   loadData() {
      ApiPermission.get(res => {
         if (res.status === 200) {
            const temp = res.data.data
            res.data.data = temp
            this.setState({
               loading: false,
               data: res.data
            })
         } else {
            this.state.errorList.push('' + res)
            this.setState({
               loading: false,
               error: true,
            })
         }
      })
   }

   componentDidMount() {
      this.loadData();
   }

   handleAction = (action, data = {}) => {
      const { item } = data

      if (action === FormMode.EDIT || action === FormMode.DUPLICATE) {
         this.state.currentItem = Object.assign({}, item)
      } else if (action === FormMode.DEL) {
         this.state.currentItem = Object.assign({}, item)
      } else {
         this.state.currentItem = {}
      }

      this.setState({ formMode: action })
   }

   handleOkDelete = () => {
      const { currentItem, data } = this.state
      ApiPermission.delete(currentItem.id, res => {
         if (res.status >= 200 && res.status <= 204) {
            const idx = data.data.findIndex(x => x.id === currentItem.id)
            if (idx > -1) {
               data.data.splice(idx, 1)
            }

            ZenMessageToast.success();

            this.setState({
               formMode: FormMode.NON,
               loading: false,
               error: false,
               errorList: [],
               data: data,
               currentItem: {}
            })

         } else {
            this.state.errorList.push('' + res)
            this.setState({
               formMode: FormMode.NON,
               loading: false,
               error: true,
               currentItem: {}
            })
         }
      })
   }

   handleAfterSaveData = (item) => {
      const { data, currentItem } = this.state
      if (item.id) {
         const idx = data.data.findIndex(x => x.id === item.id)
         if (idx >= 0)
            data.data[idx] = item
         else
            data.data.push(item)
      } else {
         data.data.push(item)
      }
      this.setState({ formMode: FormMode.NON, data, currentItem: {} })
   }

   handleRefresh = () => {
      this.setState({ loading: true })
      this.loadData();
   }

   render() {
      const { error, errorList, formMode, currentItem } = this.state

      return (
         <Context.Provider value={this.state}>
            <Helmet idMessage="setting.permission" defaultMessage="Permissions" />
            <SegmentHeader>
               <PageHeader />
            </SegmentHeader>

            {error && <Message negative list={errorList} />}

            <TabMenu />

            {(formMode === FormMode.ADD || formMode === FormMode.EDIT || formMode === FormMode.DUPLICATE) &&
               <ModalPermission id={currentItem.id}
                  editItem={currentItem}
                  header={ZenHelper.GetMessage("permission.header")}
                  formMode={formMode} open={formMode === FormMode.ADD || formMode === FormMode.EDIT || formMode === FormMode.DUPLICATE}
                  onAfterSave={this.handleAfterSaveData}
                  onClose={() => this.handleAction(FormMode.NON)} />}

            {formMode === FormMode.DEL &&
               <ConfirmDelete open={formMode === FormMode.DEL}
                  onCancel={() => this.handleAction(FormMode.NON)} onConfirm={this.handleOkDelete} />}
         </Context.Provider>
      );
   }
}

export default Permission;