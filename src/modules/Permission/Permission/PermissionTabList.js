import React from 'react';
import { Table, Button, Checkbox } from 'semantic-ui-react';
import { FormattedMessage } from "react-intl";
import { Context } from './index';
import { ButtonDelete, ButtonEdit } from '../../../components/Control';
import { FormMode } from '../../../utils/global';

const PermissionTabList = (props) => {
   return (
      <Context.Consumer>
         {ctx => (
            <Table celled selectable striped size="small">
               <Table.Header>
                  <Table.Row>
                     <Table.HeaderCell width="1" textAlign="center">
                        <FormattedMessage id="permission.id" defaultMessage="Permission ID" />
                     </Table.HeaderCell>
                     <Table.HeaderCell width="1" textAlign="center">Parent Id</Table.HeaderCell>
                     <Table.HeaderCell width="1" textAlign="center">Ordinal</Table.HeaderCell>
                     <Table.HeaderCell textAlign="center">
                        Name{/* <FormattedMessage id="permission.name" defaultMessage="Tên" /> */}
                     </Table.HeaderCell>
                     <Table.HeaderCell width="1" textAlign="center">
                        Module
                     </Table.HeaderCell>
                     <Table.HeaderCell width="1" textAlign="center">
                        Is Inactive
                     </Table.HeaderCell>
                     <Table.HeaderCell collapsing></Table.HeaderCell>
                  </Table.Row>
               </Table.Header>

               <Table.Body>
                  {ctx.data.data.length > 0 ? ctx.data.data.map((item) => {
                     return <Table.Row key={item.id} active={!item.parent_id}
                        style={!item.parent_id ? { fontWeight: "bold", fontSize: "1.1em" } : undefined}>
                        <Table.Cell textAlign="left">{item.id}</Table.Cell>
                        <Table.Cell>{item.parent_id}</Table.Cell>
                        <Table.Cell textAlign="right">{item.ordinal}</Table.Cell>
                        <Table.Cell>{item.name}</Table.Cell>
                        <Table.Cell>{item.module_id}</Table.Cell>
                        <Table.Cell textAlign="center">
                           <Checkbox checked={item.isinactive} />
                        </Table.Cell>
                        <Table.Cell collapsing>
                           <Button.Group size="small">
                              <Button icon={'copy outline'} basic color={'green'} onClick={() => ctx.handleAction(FormMode.DUPLICATE, { item: item })} />
                              <ButtonEdit onClick={() => ctx.handleAction(FormMode.EDIT, { item: item })} />
                              <ButtonDelete onClick={() => ctx.handleAction(FormMode.DEL, { item: item })} />
                           </Button.Group>
                        </Table.Cell>
                     </Table.Row>
                  }) : undefined}
               </Table.Body>
            </Table>
         )}
      </Context.Consumer>
   );
};

export default PermissionTabList;