import {axios} from 'Api'

export const ApiPermission = {
   get(callback, query = undefined, pagination = {}) {
      axios.get(`permissions`, {
         params: {
            qf: query,
            page: pagination.page,
            pageSize: pagination.pageSize
         },
      })
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   getByID(id, callback) {
      axios.get(`permissions/` + id,
      )
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   insert(data, callback) {
      axios.post(`permissions`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   update(data, callback) {
      axios.put(`permissions`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   delete(id, callback) {
      axios.delete(`permissions/` + id)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   }
}