import React, { PureComponent } from "react";
import * as routes from "constants/routes";
import * as permissions from "../../constants/permissions";
import { Context } from "./index";
import { ButtonAdd, HeaderLink } from "components/Control";
import { FormMode } from "utils/global";

class UserPageHeader extends PureComponent {
  state = {
    listHeader: [
      {
        id: "user.header",
        defaultMessage: "Danh sách người dùng",
        route: routes.User,
        active: true,
      },
    ],
  };

  render() {
    return (
      <Context.Consumer>
        {(ctx) => (
          <React.Fragment>
            <HeaderLink listHeader={this.state.listHeader} isSetting />

            <ButtonAdd
              permission={permissions.UserThem}
              floated="right"
              size="small"
              onClick={() => ctx.handleAction(FormMode.ADD)}
            />
            {this.props.children}
            <div style={{ clear: "both" }} />
          </React.Fragment>
        )}
      </Context.Consumer>
    );
  }
}

export default UserPageHeader;
