import React, { Component } from "react";
import { Form, Modal, Message, Icon } from "semantic-ui-react";
import { FormattedMessage } from 'react-intl';
import { withFormik } from 'formik';
import * as Yup from 'yup';

import { MESSAGES, ZenHelper } from 'utils';
import { Context } from "./index";
import { ButtonSave, ButtonCancel, ZenField } from "components/Control/index";

class UserFormSetPassword extends Component {
   handleSaveForm = (e) => {
      e.preventDefault();
      if (this.validData()) {
         this.context.handleSubmitFormSetPassword(this.props.values);
      }
   }


   validData() {
      const { errors } = this.props
      if (errors.password) {
         return false;
      }
      return true;
   }

   render() {
      const { modalOpen, currentItem } = this.props
      return (
         <Context.Consumer>
            {ctx => (
               <Modal size='tiny' closeOnEscape={true} closeOnDimmerClick={false} open={modalOpen} onClose={ctx.handleCloseFormSetPassword()}>
                  <Modal.Header>
                     <Icon name="key" />
                     {<FormattedMessage id={"user.reset_password"} defaultMessage="Reset Password" />}
                  </Modal.Header>

                  <Modal.Content>
                     {ctx.errorModal && <Message negative list={ctx.errorModalList} />}

                     <Form id="form-reset-password" onSubmit={this.handleSaveForm}>
                        <ZenField name="password" type="password"
                           label={"user.password"} defaultlabel="Mật khẩu"
                           props={this.props}
                           icon="lock" iconPosition='left'
                        />
                     </Form>
                  </Modal.Content>

                  <Modal.Actions>
                     <ButtonCancel type="button" size="small" onClick={ctx.handleCloseFormSetPassword()} />
                     <ButtonSave form="form-reset-password" size="small" type='submit'
                        loading={ctx.loadingResetPass}
                        onClick={(e) => this.props.handleSubmit()}
                     />
                  </Modal.Actions>
               </Modal>
            )}
         </Context.Consumer>
      );
   }
}

UserFormSetPassword.contextType = Context;

export default withFormik({
   mapPropsToValues(props) { // Init form field
      return Object.assign({}, { ...props.currentItem });
   },

   validationSchema: (props) => Yup.object().shape({ // Validate form field
      password: Yup.string()
         .required(ZenHelper.GetMessage(MESSAGES.Required))
      ,
   }),

   handleSubmit: (values, { setSubmitting }) => {
      setTimeout(() => {

      }, 1000);
   },
   enableReinitialize: true,
   isInitialValid: false

})(UserFormSetPassword)

