import { axios } from 'Api'

export const ApiUser = {
   get(callback, query = undefined, pagination = {}) {
      axios.get(`users`, {
         params: {
            keyword: query?.key,
            roleids: query?.roleids,
            page: pagination.page,
            pageSize: pagination.pageSize
         },
      })
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   getByID(id, callback) {
      axios.get(`users/` + id,
      )
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   insert(data, callback) {
      axios.post(`users`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   update(data, callback) {
      axios.put(`users`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   updatePatch(code, data, callback) {
      axios.patch(`users/${code}`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   delete(id, callback) {
      axios.delete(`users/` + id)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   resetPassword(data, callback) {
      axios.post(`users/reset_password/`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   insertSendMail(data, callback) {
      axios.post(`users/send_mail`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },
}