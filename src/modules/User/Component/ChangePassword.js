import React from "react";
import { Form, Modal, Message, Icon } from "semantic-ui-react";
import { FormattedMessage } from 'react-intl';
import { withFormik } from 'formik';
import * as Yup from 'yup';

import { MESSAGES } from '../../../utils/Messages';
import { ZenHelper } from '../../../utils/global'
import { ButtonSave, ButtonCancel, ZenField, ZenMessageToast } from "../../../components/Control/index";
import { ApiUser } from "../api/ApiUser";
import { auth } from "../../../utils";
import { ApiAccount } from "../../Account/Api/ApiAccount";

class ChangePassword extends React.Component {
   constructor(props) {
      super();
      this.userInfo = auth.getUserInfo();
      this.state = {
         error: false,
         errorList: [],
         btnLoadingSave: false,
         items: {},
         showPass: {
            old_pass: false,
            new_pass: false,
            new_pass_re: false,
         }
      }
   }

   validData() {
      const { errors } = this.props

      if (errors.old_pass || errors.new_pass || errors.new_pass_re) {
         return false;
      }
      return true;
   }

   saveData = (item) => {
      this.setState({
         btnLoadingSave: true
      })
      const item_pass = {
         id: this.userInfo.userId,
         password: item.new_pass,
         password_old: item.old_pass
      }

      ApiAccount.changePassword(item_pass, res => {
         if (res.status >= 200 && res.status <= 204) {
            ZenMessageToast.success();
            this.afterSave(item)
         } else {
            if (res.response) {
               if (res.response.data.messages.length > 0) {
                  const errorList = []
                  errorList.push(ZenHelper.GetMessage("user.password_denied"))
                  this.setState({
                     error: true, errorList: errorList, btnLoadingSave: false
                  })
               }
            } else {
               const errorList = []
               errorList.push("" + res)
               this.setState({
                  error: true, errorList: errorList, btnLoadingSave: false
               })
            }
         }
      })
   }

   afterSave = (item) => {
      this.props.onAfterSave ? this.props.onAfterSave(item) : this.handleCloseForm()
   }

   // ------------------------   component
   componentDidMount() {
      if (this.props.onRef) {
         this.props.onRef(this)
      }

      if (this.props.id) {
         this.loadData()
      } else {
         this.setState({ loadingForm: false })
      }
   }

   static getDerivedStateFromProps(props, state) {
      if (props.open !== state.open) {
         return {
            open: props.open,
         };
      }
      return null;
   }

   componentWillUnmount() {
      if (this.props.onRef) {
         this.props.onRef(undefined)
      }
   }

   componentDidUpdate(prevProps) {
      if (prevProps.open !== this.props.open && this.props.open) {
         this.props.setValues(initItem)
      }
   }

   // ------------------------   handle
   handleSaveForm = (e, submit) => {
      e.preventDefault();

      if (e.target.id === submit.id) {
         if (this.validData()) {
            this.saveData(this.props.values)
         }
      }
   }

   handleCloseForm = () => {
      this.props.onClose && this.props.onClose()
   }

   handleVisiblePass = (e, { fieldname, name }) => {
      const { showPass } = this.state
      this.setState({
         showPass: {
            ...showPass,
            [fieldname]: name === "eye" ? true : false
         }
      })
   }

   render() {
      const { id, open, onCloseForm, onAfterSave, header, editItem, ...rest } = this.props
      const { error, errorList, showPass } = this.state

      return (
         <React.Fragment>
            <Modal size='tiny' closeOnEscape closeIcon closeOnDimmerClick={false}
               open={open} onClose={this.handleCloseForm}>

               <Modal.Header>
                  <Icon name="key" />
                  <FormattedMessage id="user.change_password" defaultMessage="Change Password" />
               </Modal.Header>

               <Modal.Content>
                  {error && <Message negative list={errorList} />}

                  <Form id="change-pass" onSubmit={this.handleSaveForm}>
                     <ZenField type={showPass.old_pass ? "" : "password"}
                        icon={{
                           fieldname: "old_pass", link: true, onClick: this.handleVisiblePass,
                           name: showPass.old_pass ? "eye slash" : "eye",
                        }}
                        label={"user.old_password"} defaultlabel="Mật khẩu cũ"
                        name="old_pass" props={rest}
                     />

                     <ZenField type={showPass.new_pass ? "" : "password"}
                        icon={{
                           fieldname: "new_pass", link: true, onClick: this.handleVisiblePass,
                           name: showPass.new_pass ? "eye slash" : "eye",
                        }}
                        label={"user.new_password"} defaultlabel="Mật khẩu mới"
                        name="new_pass" props={rest}
                     />

                     <ZenField type={showPass.new_pass_re ? "" : "password"}
                        icon={{
                           fieldname: "new_pass_re", link: true, onClick: this.handleVisiblePass,
                           name: showPass.new_pass_re ? "eye slash" : "eye",
                        }}
                        label={"user.new_password_re"} defaultlabel="Xác nhận mật khẩu mới"
                        name="new_pass_re" props={rest}
                     />
                  </Form>
               </Modal.Content>

               <Modal.Actions>
                  <ButtonCancel type="button" size="small" onClick={this.handleCloseForm} />
                  <ButtonSave type='submit' size="small" form="change-pass" loading={this.state.btnLoadingSave}
                     onClick={(e) => this.props.handleSubmit()} />
               </Modal.Actions>
            </Modal>

         </React.Fragment>
      );
   }
}

export default withFormik({
   mapPropsToValues(props) { // Init form field
      return Object.assign({}, { ...initItem });
   },

   validationSchema: (props) => Yup.object().shape({ // Validate form field
      old_pass: Yup.string()
         .required(ZenHelper.GetMessage(MESSAGES.Required))
      ,
      new_pass: Yup.string()
         .required(ZenHelper.GetMessage(MESSAGES.Required))
      ,
      new_pass_re: Yup.string()
         .oneOf([Yup.ref('new_pass')], ZenHelper.GetMessage(MESSAGES.ValidPassConfirm))
         .required(ZenHelper.GetMessage(MESSAGES.ValidPassConfirm))
      ,
   }),

   handleSubmit: (values, { setSubmitting }) => {
      setTimeout(() => {

      }, 1000);
   },
   enableReinitialize: true,
})(ChangePassword)

ChangePassword.defaultProps = {
   onRef: undefined,    // call handle from parent component
   id: undefined,       // id of items
   open: false,         // close - open modal
   header: undefined,   // header modal
   editItem: {},        // items edit
   isView: false,       // only view
   onClose: undefined,  // event close modal
   onAfterSave: undefined  // event after save data
};

const initItem = {
   old_pass: "",
   new_pass: "",
   new_pass_re: ""
}