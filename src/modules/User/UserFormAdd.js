import React, { Component } from "react";
import { Form, Modal, Message, Loader, Icon, Dimmer, Checkbox } from "semantic-ui-react";
import { FormattedMessage } from 'react-intl';
import { withFormik } from 'formik';
import * as Yup from 'yup';

import { MESSAGES, ZenHelper } from 'utils';
import { ButtonSave, ButtonCancel, ZenField, ZenMessageToast } from "components/Control/index";
import { ApiUser } from "./api/ApiUser";
import { ApiRole } from "../../Api/index";
import { ZenFieldCheckbox, ZenFieldSelect } from "../../components/Control";

class UserFormAdd extends Component {
   isMounted = false
   constructor(props) {
      super();
      this.state = {
         open: false,
         loadingForm: true,
         btnLoadingSave: false,
         error: false,
         errorList: [],
         items: initItem,
         roles: [],
      }
   }

   loadRole() {
      return new Promise((resolve, reject) => {
         ApiRole.get(res => {
            if (res.status === 200) {
               resolve(ZenHelper.translateListToSelectOptions(res.data.data, false))
            } else {
               reject(res)
            }
         })
      })
   }

   loadData() {
      const { id } = this.props
      const roles = this.loadRole();

      const user = new Promise((resolve, reject) => {
         ApiUser.getByID(id, res => {
            if (res.status === 200) {
               resolve(res.data.data)
            } else {
               reject(res)
            }
         })
      })

      Promise.all([roles, user])
         .then(values => {
            const temp = []
            values[1].userRoles?.forEach(t => temp.push(t.role_id))
            values[1].rolesChose = temp

            if (this.isMounted) {
               this.setState({
                  loadingForm: false,
                  error: false,
                  errorList: [],
                  items: values[1],
                  roles: values[0],
               })
               this.props.setValues(values[1])
            }
         }).catch(err => {

            this.state.errorList.push('' + err)
            this.setState({
               error: true,
               loadingForm: false
            })
         });
   }

   validData() {
      const { errors } = this.props

      if (this.props.id && errors.password)
         delete errors["password"]

      if (errors && Object.keys(errors).length > 0) {
         return false;
      }
      return true;
   }

   saveData = (item) => {
      this.setState({ btnLoadingSave: true })
      if (item.id) {
         item.userRoles = []
         item.rolesChose.map(t => item.userRoles.push({ role_id: t }))
         delete item.password

         ApiUser.update(item, res => {
            if (res.status >= 200 && res.status <= 204) {
               ZenMessageToast.success();

               if (item.rolesChose && item.rolesChose.length > 0) {
                  item.roleNames = []
                  item.rolesChose.map((t, index) => {
                     let temp = this.state.roles.find(x => x.value == t)
                     if (temp) {
                        item.roleNames.push(temp.text)
                        item.userRoles[index].role_name = temp.text
                     }
                  })
               }

               this.afterSave(item)
            } else {
               if (res.response) {
                  if (res.response.data.messages && res.response.data.messages.length > 0) {
                     const errorList = res.response.data.messages

                     this.setState({
                        error: true, errorList: errorList, btnLoadingSave: false
                     })
                  } else {
                     const errorList = []
                     errorList.push("" + res)
                     this.setState({
                        error: true, errorList: errorList, btnLoadingSave: false
                     })
                  }
               } else {
                  const errorList = []
                  errorList.push("" + res)
                  this.setState({
                     error: true, errorList: errorList, btnLoadingSave: false
                  })
               }
            }
         })
      } else {
         item.userRoles = []
         item.rolesChose.map(t => item.userRoles.push({ role_id: t }))

         // kiểm tra tạo user từ detail
         let apiIns = ApiUser.insert
         if (this.props.createFromDetail) {
            apiIns = ApiUser.insertSendMail
         }

         apiIns(item, res => {
            if (res.status >= 200 && res.status <= 204) {
               ZenMessageToast.success();
               let newItem = res.data.data

               if (item.rolesChose && item.rolesChose.length > 0) {
                  newItem.roleNames = []
                  item.rolesChose.map((t, index) => {
                     let temp = this.state.roles.find(x => x.value == t)
                     if (temp) {
                        newItem.roleNames.push(temp.text)
                        newItem.userRoles[index].role_name = temp.text
                     }
                  })
               }

               this.afterSave(newItem)
            } else {
               if (res.response) {
                  if (res.response.data.messages && res.response.data.messages.length > 0) {
                     const errorList = res.response.data.messages
                     //errorList.push(res.response.data.messages)
                     this.setState({
                        error: true, errorList: errorList, btnLoadingSave: false
                     })
                  } else {
                     const errorList = []
                     errorList.push("" + res)
                     this.setState({
                        error: true, errorList: errorList, btnLoadingSave: false
                     })
                  }
               } else {
                  const errorList = []
                  errorList.push("" + res)
                  this.setState({
                     error: true, errorList: errorList, btnLoadingSave: false
                  })
               }
            }
         })
      }
   }

   afterSave = (item) => {
      this.props.onAfterSave ? this.props.onAfterSave(item) : this.handleCloseForm() || undefined
   }

   componentDidMount() {
      this.isMounted = true
      if (this.props.onRef) {
         this.props.onRef(this)
      }

      if (this.props.id) {
         this.loadData()
      } else {
         const roles = this.loadRole();
         Promise.all([roles])
            .then(values => {
               if (this.isMounted) {
                  this.setState({
                     loadingForm: false,
                     error: false,
                     errorList: [],
                     roles: values[0]
                  })
               }
            }).catch(err => {
               this.state.errorList.push('' + err)
               this.setState({
                  error: true,
                  loadingForm: false
               })
            });
      }
   }

   componentWillUnmount() {
      this.isMounted = false
      if (this.props.onRef) {
         this.props.onRef(undefined)
      }
   }

   static getDerivedStateFromProps(props, state) {
      if (props.open !== state.open) {
         return {
            open: props.open,
            loadingForm: props.id ? true : false,
            items: initItem
         };
      }
      return null;
   }

   componentDidUpdate(prevProps) {
      if (prevProps.open !== this.props.open && this.props.open) {
         if (this.props.id) {
            this.loadData()
         } else {
            const roles = this.loadRole();
            Promise.all([roles])
               .then(values => {
                  this.props.setValues(initItem)
                  this.setState({
                     loadingForm: false,
                     error: false,
                     errorList: [],
                     roles: values[0]
                  })
               }).catch(err => {
                  this.state.errorList.push('' + err)
                  this.setState({
                     error: true,
                     loadingForm: false
                  })
               });

         }
      }
   }

   // ------------------------   handle
   handleSaveForm = (e, submit) => {
      e.preventDefault();

      if (e.target.id === submit.id) {
         if (this.validData()) {
            this.saveData(this.props.values)
         }
      }
   }

   handleCloseForm = () => {
      this.props.onClose && this.props.onClose()
   }

   render() {
      const { id, open, onCloseForm, onAfterSave, header, editItem, createFromDetail, ...rest } = this.props
      const { loadingForm, error, errorList } = this.state

      return (
         <Modal size='tiny' closeOnEscape closeIcon closeOnDimmerClick={false} open={open} onClose={this.handleCloseForm}>
            <Modal.Header>
               <Icon name="folder open outline" />
               {!id ? <FormattedMessage id={MESSAGES.CPControlAdd} defaultMessage="Thêm mới"
                  values={{ mess: header }} />
                  : header}
            </Modal.Header>

            <Modal.Content>
               {error && <Message negative list={errorList} />}

               {loadingForm && <Dimmer inverted active={loadingForm}>
                  <Loader inverted />
               </Dimmer>}

               <Form id="form-modal-user" onSubmit={this.handleSaveForm}>
                  <ZenField name="email" required
                     label={MESSAGES.GeneralEmail} defaultlabel="Email"
                     props={rest}
                  />

                  {!id && !createFromDetail &&
                     <ZenField name="password" type="password" required
                        label={"user.password"} defaultlabel="Mật khẩu"
                        props={rest}
                     />}

                  <ZenField name="full_name" required
                     label={"user.full_name"} defaultlabel="Tên đầy đủ"
                     props={rest}
                  />

                  <ZenFieldSelect multiple options={this.state.roles} name="rolesChose"
                     label={"role.name"} defaultlabel="Vai trò"
                     props={rest}
                  />

                  {!createFromDetail && <ZenFieldCheckbox style={{ fontWeight: "bold" }}
                     label={"role.isadmin"} defaultlabel="Là người quản trị"
                     name="isadmin" props={rest} />}
               </Form>
            </Modal.Content>

            <Modal.Actions>
               <div style={{ float: "left" }}>
                  <ZenFieldCheckbox style={{ paddingTop: '7px', paddingLeft: "7px", fontWeight: "bold" }}
                     label={"role.isinactive"} defaultlabel="Inactive"
                     name="isinactive" props={rest}
                     tabIndex={-1}
                  />
               </div>

               <ButtonCancel type="button" size="small" onClick={this.handleCloseForm} />
               <ButtonSave type='submit' size="small" form="form-modal-user" loading={this.state.btnLoadingSave}
                  onClick={(e) => this.props.handleSubmit()} />
            </Modal.Actions>
         </Modal>
      );
   }
}

export default withFormik({
   mapPropsToValues(props) { // Init form field
      return Object.assign({}, { ...initItem, ...props.initItem });
   },

   validationSchema: (props) => {
      const shapeValid = { // Validate form field
         email: Yup.string()
            .required(ZenHelper.GetMessage(MESSAGES.Required))
            .email(),
         full_name: Yup.string()
            .required(ZenHelper.GetMessage(MESSAGES.Required)),
      }

      if (props.createFromDetail) {
         return Yup.object().shape(shapeValid)
      }

      shapeValid.password = Yup.string()
         .required(ZenHelper.GetMessage(MESSAGES.Required))
      return Yup.object().shape(shapeValid)
   },

   handleSubmit: (values, { setSubmitting }) => {
      setTimeout(() => {

      }, 1000);
   },
   enableReinitialize: true,

})(UserFormAdd)

UserFormAdd.defaultProps = {
   onRef: undefined,    // call handle from parent component
   id: undefined,       // id of items
   open: false,         // close - open modal
   header: undefined,   // header modal
   editItem: {},        // items edit
   isView: false,       // only view
   onClose: undefined,  // event close modal
   onAfterSave: undefined  // event after save data
};

const initItem = {
   user_name: "",
   email: "",
   full_name: "",
   password: "",
   rolesChose: []
}