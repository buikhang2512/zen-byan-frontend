import React from "react";
import { Table, Button, Icon, Label } from "semantic-ui-react";
import { FormattedMessage } from "react-intl";
import { Context } from "./index";
import { ButtonDelete, ButtonEdit } from "components/Control";
import { MESSAGES, FormMode } from "utils";
import * as permissions from "../../constants/permissions";
import { ZenButton } from "../../components/Control";

const UserTabList = (props) => {
   return (
      <Context.Consumer>
         {(ctx) => (
            <Table celled selectable striped>
               <Table.Header>
                  <Table.Row>
                     <Table.HeaderCell width="1" textAlign="center">
                        <FormattedMessage
                           id={MESSAGES.GeneralzID}
                           defaultMessage="zID"
                        />
                     </Table.HeaderCell>
                     <Table.HeaderCell textAlign="center">
                        <FormattedMessage id="user.email" defaultMessage="Email" />
                     </Table.HeaderCell>
                     <Table.HeaderCell textAlign="center">
                        <FormattedMessage
                           id="user.full_name"
                           defaultMessage="Tên đầy đủ"
                        />
                     </Table.HeaderCell>
                     <Table.HeaderCell textAlign="center">
                        Vai trò User
                     </Table.HeaderCell>
                     <Table.HeaderCell textAlign="center" collapsing>
                        <FormattedMessage id="user.isadmin" defaultMessage="Người quản trị" />
                     </Table.HeaderCell>
                     <Table.HeaderCell textAlign="center" collapsing>
                        <FormattedMessage id="user.inactive" defaultMessage="Inactive" />
                     </Table.HeaderCell>
                     <Table.HeaderCell collapsing></Table.HeaderCell>
                  </Table.Row>
               </Table.Header>

               {ctx.data && (
                  <Table.Body>
                     {ctx.data.map((item) => {
                        return (
                           <Table.Row key={item.id}>
                              <Table.Cell textAlign="center">{item.id}</Table.Cell>
                              <Table.Cell>{item.email}</Table.Cell>
                              <Table.Cell>{item.full_name}</Table.Cell>
                              <Table.Cell>
                                 {
                                    item.userRoles && item.userRoles?.length > 0 &&
                                    item.userRoles.map((t, idx) => {
                                       return <Label key={t.id} content={t.role_name} />
                                    })
                                 }
                              </Table.Cell>
                              <Table.Cell textAlign="center">
                                 {item.isadmin && (
                                    <Icon color="green" name="checkmark" size="large" />
                                 )}
                              </Table.Cell>

                              <Table.Cell textAlign="center">
                                 {item.isinactive && <Icon name="lock" />}
                              </Table.Cell>

                              <Table.Cell collapsing>
                                 <Button.Group size="small">
                                    <ButtonEdit
                                       permission={permissions.UserSua}
                                       onClick={() =>
                                          ctx.handleAction(FormMode.EDIT, { item: item })
                                       }
                                    />
                                    <ButtonDelete
                                       permission={permissions.UserXoa}
                                       onClick={() =>
                                          ctx.handleAction(FormMode.DEL, { item: item })
                                       }
                                    />
                                    <ButtonEdit permission={permissions.UserSua}
                                       icon={"key"}
                                       basic
                                       color={"black"}
                                       onClick={() => ctx.handleOpenFormSetPassword(item)}
                                    />

                                    <ZenButton content="" basic
                                       color={item.isinactive ? "green" : "red"}
                                       icon={item.isinactive ? "unlock" : "lock"}
                                       permission={permissions.UserSua}
                                       onClick={() =>
                                          ctx.handleAction("ACTIVE", { item: item })
                                       }
                                    />
                                 </Button.Group>
                              </Table.Cell>
                           </Table.Row>
                        );
                     })}
                  </Table.Body>
               )}
            </Table>
         )}
      </Context.Consumer>
   );
};

export default UserTabList;
