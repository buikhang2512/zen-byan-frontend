import React, { Component } from "react";
import { Message, Segment, Input, Icon, Menu, Dropdown } from "semantic-ui-react";
import {
  Context,
  FormAdd,
  FormSetPassword,
  PageHeader,
  TabList,
} from "./index";
import { ApiUser } from "./api/ApiUser";
import {
  ConfirmDelete,
  Helmet,
  ZenMessageToast,
  SegmentHeader,

} from "components/Control/index";
import * as permissions from "../../constants/permissions";
import { MessageNone, ButtonAdd, ZenMessageAlert, ZenFieldSelect } from "../../components/Control";
import { FormMode, ZenHelper } from "../../utils/global";
import { zzControlHelper } from "../../components/Control/zzControlHelper";
import { ApiRole } from "../../Api";
import _ from "lodash";

class User extends Component {
  constructor(props) {
    super();
    this.state = {
      activeTab: "All",
      loadingResetPass: false,
      btnLoadingDel: false,
      loadingSearch: true,
      loading: true,
      error: false,
      errorList: [],
      keyword: "",
      data: [],
      dataOriginal: [],
      currentItem: {},
      roles: [],
      rolechoose: [],
      errorModal: false,
      errorModalList: [],
      formMode: FormMode.NON,

      openFormSetPassword: false,

      changeTabItem: () => this.changeTabItem,
      handleAction: (action, data) => this.handleAction(action, data),

      handleOpenFormSetPassword: (item) => this.handleOpenFormSetPassword(item),
      handleCloseFormSetPassword: () => this.handleCloseFormSetPassword,
      handleSubmitFormSetPassword: (item) =>
        this.handleSubmitFormSetPassword(item),
      handleEnterSearch: () => this.handleEnterSearch,
      handleSearchChange: () => this.handleSearchChange,
      handleClickSearch: (type) => this.handleClickSearch(type),
    };
  }

  createQuery(keyword) {
    var key
    if (keyword) {
      key = keyword.replace(/\\/g, "\\\\");
      key = key.replace(/"/g, `\\"`);
    }
    let roleids = null
    if (_.join(this.state.rolechoose, ',') !== ', ') {
      roleids = _.join(this.state.rolechoose, ',')
    }
    let query = { key: key, roleids: roleids ? `[${roleids}]` : null }
    // `(full_name != null && full_name.Contains("${key}")) ` +
    // ` or (email != null && email.Contains("${key}"))` +
    // ` or (user_name != null && user_name.Contains("${key}"))`
    return query;
  }

  changeTabItem = (e, { name }) => {
    const { dataOriginal } = this.state
    if (name !== this.state.activeTab) {
      let newData = dataOriginal;
      if (name === "Active") {
        newData = dataOriginal.filter(t => t.isinactive === false)
      } else if (name === "Inactive") {
        newData = dataOriginal.filter(t => t.isinactive === true)
      }
      this.setState(prev => {
        return {
          data: newData,
          activeTab: name
        }
      });
    }
  };

  componentDidMount() {
    this.loadData();
    this.loadRole();
  }

  loadRole() {
    ApiRole.get(res => {
      if (res.status === 200) {
        let roles = ZenHelper.translateListToSelectOptions(res.data.data, false)
        this.setState({ roles: roles })
      } else {
        reject(res)
      }
    })
  }

  loadData(keyword) {
    let query = this.createQuery(keyword)

    ApiUser.get((res) => {
      if (res.status === 200) {
        this.setState({
          loadingSearch: false,
          loading: false,
          error: false,
          keyword: keyword,
          data: res.data.data.data,
          dataOriginal: res.data.data.data,
        });
      } else {
        this.state.errorList.push("" + res);
        this.setState({
          loading: false,
          error: false,
          loadingSearch: false,
          error: true,
        });
      }
    }, query);
  }

  // ================================== xoa/sua
  handleAction = (action, data = {}) => {
    const { item } = data;

    if (action === FormMode.EDIT) {
      this.state.currentItem = Object.assign({}, item);
    } else if (action === FormMode.DEL) {
      this.state.currentItem = Object.assign({}, item);
    } else if (action === "ACTIVE") {
      this.lockUser(item)
      return
    }
    else {
      this.state.currentItem = {};
    }

    this.setState({ formMode: action });
  };

  handleOkDelete = () => {
    const { currentItem, data } = this.state;
    this.setState({ btnLoadingDel: true });

    ApiUser.delete(currentItem.id, (res) => {
      if (res.status >= 200 && res.status <= 204) {
        const idx = data.findIndex((x) => x.id === currentItem.id);
        if (idx > -1) {
          data.splice(idx, 1);
        }

        ZenMessageToast.success();

        this.setState({
          formMode: FormMode.NON,
          btnLoadingDel: false,
          loadingSearch: false,
          error: false,
          errorList: [],
          data: data,
          currentItem: {},
        });
      } else {
        this.state.errorList.push("" + res);
        this.setState({
          formMode: FormMode.NON,
          btnLoadingDel: false,
          loadingSearch: false,
          error: true,
          currentItem: {},
        });
      }
    });
  };

  handleAfterSaveData = (item) => {
    const { data, currentItem } = this.state;
    if (currentItem.id) {
      const idx = data.findIndex((x) => x.id === currentItem.id);
      data[idx] = item;
    } else {
      data.push(item);
    }
    this.setState({ formMode: FormMode.NON, data, currentItem: {} });
  };

  // ================================== form set password  
  handleOpenFormSetPassword = (item) => {
    if (item) {
      this.setState({
        openFormSetPassword: true,
        currentItem: Object.assign({}, item),
      });
    }
  };

  handleCloseFormSetPassword = () => {
    this.setState({
      openFormSetPassword: false,
      currentItem: {},
    });
  };

  handleSubmitFormSetPassword = (item) => {
    this.setState({ loadingResetPass: true });

    if (item.id) {
      ApiUser.resetPassword({ id: item.id, password: item.password }, (res) => {
        if (res.status == 204) {
          this.setState({
            openFormSetPassword: false,
            loadingResetPass: false,
          });
          ZenMessageToast.success();
        } else {
          this.state.errorModalList.push("" + res);
          this.setState({
            errorModal: true,
            loadingResetPass: false
          });
        }
      });
    }
  };

  // ================================== search
  handleSearchChange = (e, { value }) => {
    this.setState({ keyword: value });
  };

  handleEnterSearch = (e) => {
    if (e.key === 'Enter') {
      this.setState({ loadingSearch: true });

      clearTimeout(this.timer);
      this.timer = setTimeout(() => {
        this.loadData(this.state.keyword);
      }, 300);
    }
  };

  handleClickSearch = (type) => {
    this.setState({ loadingSearch: true });
    if (type === "refresh") {
      this.loadData("");
    } else {
      // click icon trên input search
      this.loadData(this.state.keyword);
    }
  };

  lockUser(item) {
    const htmlView = <div>
      {`Bạn có ${item.isinactive ? "muốn mở khóa" : " chắc khóa"} tài khoản của`}
      <p style={{ fontWeight: "bold" }}>
        {` ${item.full_name}`}
      </p>
    </div>

    ZenMessageAlert.question(htmlView, true)
      .then(success => {
        if (success == 1) {
          const patchData = [
            {
              value: !item.isinactive,
              path: `/isinactive`,
              op: "replace",
              operationType: 0,
            },
          ]
          ApiUser.updatePatch(item.id, patchData,
            res => {
              if (res.status === 200) {
                this.setState(prev => {
                  return {
                    data: prev.data.map(t => t.id == item.id ? { ...t, isinactive: !item.isinactive } : t)
                  }
                })
                ZenMessageToast.success()
              } else {
                ZenMessageAlert.error(ZenHelper.getResponseError(res))
              }
            })
        }
      })
  }

  render() {
    const {
      error,
      errorList,
      formMode,
      currentItem,
      keyword,
      data,
      loading,
      loadingSearch,
      openFormSetPassword,
      activeTab,
    } = this.state;

    return (
      <Context.Provider Context={Context} value={this.state}>
        <Helmet idMessage="user.header" defaultMessage="Danh sách người dùng" />

        <SegmentHeader>
          <PageHeader>
            <div style={{ float: "right" }}>
              <Input
                size="small"
                value={keyword || ""}
                style={{ float: "right", width: "250px" }}
                placeholder={"Search"}
                loading={loadingSearch}
                onChange={this.handleSearchChange}
                onKeyDown={this.handleEnterSearch}
                icon={
                  <Icon
                    link
                    name="search"
                    onClick={() => this.handleClickSearch("search")}
                  />
                }
              />
              <Dropdown style={{ marginRight: "10px" }}
                size="small"
                search
                selection
                multiple
                placeholder="Vai trò"
                options={this.state.roles} name="rolesChose"
                label={"role.name"} defaultlabel="Vai trò"
                value={this.state.rolechoose}
                onChange={(e, i) => {
                  this.setState({ rolechoose: i.value })
                  this.handleEnterSearch({ key: "Enter" })
                }}
              />
            </div>
          </PageHeader>
        </SegmentHeader>

        {error && <Message negative list={errorList} />}

        <Segment loading={loading}>
          <Menu pointing secondary>
            <Menu.Item
              name='All'
              active={activeTab === 'All'}
              onClick={this.changeTabItem}
            />
            <Menu.Item
              name='Active'
              active={activeTab === 'Active'}
              onClick={this.changeTabItem}
            />
            <Menu.Item
              name='Inactive'
              active={activeTab === 'Inactive'}
              onClick={this.changeTabItem}
            />
          </Menu>

          {data && data.length > 0 ? (
            <TabList />
          ) : (
            <MessageNone>
              <ButtonAdd
                permission={permissions.UserThem}
                size="small"
                onClick={() => this.handleAction(FormMode.ADD)}
              />
            </MessageNone>
          )}
        </Segment>

        {(formMode === FormMode.ADD || formMode === FormMode.EDIT) && (
          <FormAdd
            id={currentItem.id}
            editItem={currentItem}
            header={ZenHelper.GetMessage("user.user", "Người sử dụng")}
            open={formMode === FormMode.ADD || formMode === FormMode.EDIT}
            onAfterSave={this.handleAfterSaveData}
            onClose={() => this.handleAction(FormMode.NON)}
          />
        )}

        {openFormSetPassword && (
          <FormSetPassword
            currentItem={{ id: currentItem.id, password: "" }}
            modalOpen={true}
          />
        )}

        {formMode === FormMode.DEL && (
          <ConfirmDelete
            open={formMode === FormMode.DEL}
            loading={this.state.btnLoadingDel}
            onCancel={() => this.handleAction(FormMode.NON)}
            onConfirm={this.handleOkDelete}
          />
        )}
      </Context.Provider>
    );
  }
}

export default User;
