import React, { Component } from "react";
import {
  Dimmer,
  Grid,
  Icon,
  Loader,
  Menu,
  Message,
  Input,
} from "semantic-ui-react";
import memoize from "memoize-one";
import * as routes from "../../constants/routes";
import ReportContent from "./ReportContent";
import { ApiMenu } from "../../Api";
import { ZenHelper } from "../../utils/global";
import "./ReportModules.css";
import { ContainerScroll, ScrollManager } from "../../components/Control";

class Main extends Component {
  constructor(props) {
    super();
    this.keyActiveModule = "active-module"
    this.keyData = "menu-report"
    this.idElementContainer = "report-center"

    this.state = {
      error: null,
      isLoading: true,
      activeModule: "",
      data: [],
      keyword: "",
    };
  }

  // ============================ REACT COMPONENT
  componentDidMount() {
    // get data
    const dataTemp = memoryReport.get(this.keyData)
    // get active module
    const currentModule = memoryReport.get(this.keyActiveModule)

    if (dataTemp && dataTemp.length > 0) {
      this.setState({
        error: null,
        isLoading: false,
        data: dataTemp,
        activeModule: currentModule ? currentModule.activeModule : "ALL"
      });

    } else {
      ApiMenu.getReport((res) => {
        if (res.status === 200) {
          // lưu data vào biến tạm
          memoryReport.set(this.keyData, res.data.data)

          this.setState({
            error: null,
            isLoading: false,
            data: res.data.data,
            activeModule: currentModule ? currentModule.activeModule : "ALL"
          });
        } else {
          this.setState({
            isLoading: false,
            error: ZenHelper.getResponseError(res),
          });
        }
      });
    }
  }

  componentWillUnmount() {
    memoryReport.set(this.keyActiveModule,
      {
        activeModule: this.state.activeModule
      })
  }

  // ============================ EVENT HANDLE
  handleChangeTab = (e, { name }) => {
    //if (name === this.state.activeModule) return;
    this.setState({ keyword: "", activeModule: name });
  };

  // =========================== memozie
  mapModule = memoize((activeModule) => {
    return (
      <Menu fluid pointing vertical>
        <Menu.Item
          key="ALL"
          content={
            <>
              {" "}
              {(activeModule === "ALL" ? true : false) ? (
                <span>
                  <Icon name={"caret right"} />
                </span>
              ) : undefined}
              Tất cả báo cáo
            </>
          }
          name="ALL"
          onClick={this.handleChangeTab}
          active={activeModule === "ALL" ? true : false}
          header={activeModule === "ALL" ? true : false}
        />
        {this.state.data.map((module) => {
          const isActive = activeModule === module.id;
          return (
            <Menu.Item
              key={module.id}
              content={
                <>
                  {isActive ? (
                    <span>
                      <Icon name={"caret right"} />
                    </span>
                  ) : undefined}{" "}
                  {module.name}{" "}
                </>
              }
              name={module.id}
              onClick={this.handleChangeTab}
              active={isActive}
              header={isActive}
            />
          );
        })}
      </Menu>
    );
  });

  render() {
    const {
      activeModule,
      isLoading,
      error,
      data,
      keyword,
    } = this.state;
    const renderMenu = this.mapModule(activeModule);

    return (
      <React.Fragment>
        <div className="zrc-container-flex">
          <Input
            value={keyword}
            placeholder={"Search..."}
            style={{ width: "250px" }}
            onChange={(e, { value }) => this.setState({ keyword: value })}
          />
        </div>

        <Grid id={this.idElementContainer}>
          <Grid.Column width={4}>
            {isLoading && (
              <Dimmer inverted active={isLoading}>
                <Loader inverted content="Loading" />
              </Dimmer>
            )}
            {error && <Message list={error} negative />}
            {renderMenu}
          </Grid.Column>

          <Grid.Column width={12}>
            <ContainerScroll isSegment={false}
              idElementContainer={this.idElementContainer}
              id={"right-content"}
              border
            >
              <ReportContent
                moduleid={activeModule}
                modules={data}
                keyword={keyword}
              />
            </ContainerScroll>
          </Grid.Column>
        </Grid >
      </React.Fragment >
    );
  }
}

export const memoryReport = {
  _data: new Map(),
  get(key) {
    if (!key) {
      return null
    }

    return this._data.get(key) || null
  },
  set(key, data) {
    if (!key) {
      return
    }
    return this._data.set(key, data)
  }
}

export const ReportCenter = {
  ReportModule: {
    route: routes.ReportModule,
    Zenform: Main,
    linkHeader: {
      id: "report-center",
      defaultMessage: "Report Center",
      active: true,
    },
    action: {
      view: { visible: true, permission: "", notPermission: true },
    },
  },
};
