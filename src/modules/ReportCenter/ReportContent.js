import React, { memo, useEffect, useMemo } from "react";
import { FormattedMessage } from "react-intl";
import { Table } from "semantic-ui-react";
import { ScrollManager, ZenLink } from "../../components/Control";

const ReportContent = (props) => {
  const { modules, moduleid, keyword } = props;

  const itemsMenu = useMemo(() => {
    let result = [];
    if (moduleid === "ALL") {
      modules.forEach((module) => {
        const temp = module.menus
          .filter(
            (menu) =>
              !keyword ||
              (keyword &&
                menu.name.toLowerCase().includes(keyword.toLowerCase()))
          )
          .map((menu) => ({ ...menu, module_name: module.name }));

        result = result.concat([...temp]);
      });
    } else {
      const module = modules.find((module) => module.id === moduleid);
      if (module) {
        result = module.menus
          .filter(
            (menu) =>
              !keyword ||
              (keyword &&
                menu.name.toLowerCase().includes(keyword.toLowerCase()))
          )
          .map((menu) => ({ ...menu, module_name: module.name }));
      }
    }
    return result;
  }, [moduleid, keyword]);

  return (
    <>
      <Table striped definition selectable textAlign="left">
        <Table.Header fullWidth >
          <Table.Row>
            <Table.HeaderCell width="1" className="sticky-top" />
            <Table.HeaderCell className="sticky-top" >
              <FormattedMessage id="report.name" defaultMessage="Báo cáo" />
            </Table.HeaderCell>
            <Table.HeaderCell className="sticky-top">
              <FormattedMessage
                id="report.group"
                defaultMessage="Nhóm báo cáo"
              />
            </Table.HeaderCell>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {itemsMenu.map((item, index) => {
            return (
              <Table.Row key={item.menu_id + index}>
                <Table.Cell content={index + 1} textAlign="center" />
                <Table.Cell>
                  <ZenLink href={"#" + item.url}>{item.name}</ZenLink>
                </Table.Cell>
                <Table.Cell>{item.module_name}</Table.Cell>
              </Table.Row>
            );
          })}
        </Table.Body>
      </Table>
    </>
  );
};

export default memo(ReportContent);
