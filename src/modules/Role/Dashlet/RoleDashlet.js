import React, { Component } from 'react';
import { Message } from 'semantic-ui-react';
import { PageHeader, RoleContent, RoleDashLetContent } from './index';
import { Helmet, SegmentHeader, ZenMessageToast } from '../../../components/Control/index';
import { ApiPermission, ApiRole, ApiModule, ApiRoleDashlet, ApiDashLet } from '../../../Api';
import { ZenHelper } from '../../../utils/global';
import _ from 'lodash';

class RoleDashlet extends Component {
    constructor(props) {
        super();
        this.state = {
            loading: true,
            error: false,
            errorList: [],

            roleInfo: {},
            modules: [],
            modulesFunction: [],
            permission: [],
            viewData: null,
            roleDashlet: [],
            dashlets: [],
            SysDashlets: [],
            checkall: null,

            btnSaveLoading: false,

            handleChecked: this.handleChecked,
            handleSave: this.handleSave,
            handleCheckedDashlets: this.handleCheckedDashlets
        }
    }

    componentDidMount() {
        const { match } = this.props
        const roleInfo = new Promise((resolve, reject) => {
            ApiRole.getByID(match.params.id, res => {
                if (res.status === 200) {
                    resolve(res.data.data)
                } else {
                    reject(res)
                }
            })
        })

        const permission = new Promise((resolve, reject) => {
            ApiPermission.get(res => {
                if (res.status === 200) {
                    resolve(res.data.data)
                } else {
                    reject(res)
                }
            })
        })

        const modules = new Promise((resolve, reject) => {
            ApiModule.get(res => {
                if (res.status === 200) {
                    resolve(res.data.data)
                } else {
                    reject(res)
                }
            })
        })

        const roleDashlet = new Promise((resolve, reject) => {
            ApiRoleDashlet.getListRole(match.params.id, res => {
                if (res.status === 200) {
                    resolve(res.data.data)
                } else {
                    reject(res)
                }
            })
        })

        const Dashlets = new Promise((resolve, reject) => {
            ApiDashLet.get(res => {
                if (res.status === 200) {
                    resolve(res.data.data)
                } else {
                    reject(res)
                }
            },
            {qf: `ksd ='false'`})
        })

        Promise.all([roleInfo, permission, modules, roleDashlet, Dashlets])
            .then(values => {
                // set permission của user vào list permission
                const rolePermisson = values[1]
                    .map(t => { t.check = false; return t })
                    .sort((a, b) => a.ordinal - b.ordinal)

                values[0].rolePermissions.forEach(element => {
                    const idx = rolePermisson.findIndex(x => x.id === element.permission_id)
                    if (idx > -1) {
                        rolePermisson[idx].check = true
                    }
                })
                values[4].forEach(x => x.check = false)
                values[3].forEach(element => {
                    const idx = values[4].findIndex(x => x.id === element.dashlet_id)
                    if (idx > -1) {
                        values[4][idx].check = true
                    }
                })

                let dashlets = Object.keys(values[3].reduce((r, { dashlet_id }) => (r[dashlet_id] = '', r), {}))
                const modules = values[2].sort((a, b) => a.ordinal - b.ordinal);
                const viewData = this.mapToTreeData(modules, rolePermisson)

                let checkall = !values[4].some(el => { return el.check === false })

                this.setState({
                    error: false,
                    loading: false,
                    roleInfo: values[0],
                    permission: rolePermisson,
                    modules: modules,
                    viewData: viewData,
                    roleDashlet: values[3],
                    SysDashlets: values[4],
                    dashlets: dashlets,
                    checkall: checkall,
                })
            }).catch(err => {
                this.setState({
                    error: ZenHelper.getResponseError(err),
                    loading: false
                })
            });
    }

    handleSave = () => {
        const { dashlets } = this.state
        const { match } = this.props
        let str_dashlet;
        if (_.join(dashlets, ',') !== ', ') {
            str_dashlet = _.join(dashlets, ',')
        }
        ApiRoleDashlet.insert({ role_id: match.params.id, dashlets: str_dashlet }, res => {
            if (res.status === 200) {
                ZenMessageToast.success()
            } else {
                this.setState({
                    error: ZenHelper.getResponseError(err),
                })
            }
        })
    }

    handleChecked = ({ checked, name }, item) => {
        const { roleDashlet, dashlets, SysDashlets } = this.state
        const idx = SysDashlets.findIndex(x => x.id === item.id)
        if (idx > -1) {
            SysDashlets[idx].check = checked
        }
        if (checked) {
            const newDashlets = dashlets.concat(item.id)
            let checkall = !SysDashlets.some(el => { return el.check === false })
            this.setState({
                dashlets: newDashlets,
                checkall: checkall,
            })
        } else {
            const newDashlets = dashlets.filter(t => t !== item.id)
            this.setState({
                dashlets: newDashlets,
                checkall: false,
            })
        }
    }

    handleCheckedDashlets = ({ checked }, dashlets) => {
        const { roleDashlet, SysDashlets } = this.state
        if (checked) {
            SysDashlets.forEach(x => x.check = true)
            const dashlets_id = Object.keys(dashlets.reduce((r, { id }) => (r[id] = '', r), {}))
            this.setState({
                dashlets: dashlets_id,
                SysDashlets: SysDashlets,
                checkall: true,
            })
        } else {
            SysDashlets.forEach(x => x.check = false)
            this.setState({
                dashlets: [],
                SysDashlets: SysDashlets,
                checkall: false,
            })
        }
    }

    mapToTreeData(modules, rolePermisson) {
        let viewData = {};
        for (let index = 0; index < modules.length; index++) {
            const module = modules[index]
            const lenCheckedOfModules = rolePermisson.filter(t => t.module_id == module.id && t.parent_id.trim() && t.check == true).length
            const lenPermissionOfModules = rolePermisson.filter(t => t.module_id == module.id && t.parent_id.trim()).length

            const moduleObj = {
                ...module,
                check: lenCheckedOfModules == lenPermissionOfModules ? 1 : (lenCheckedOfModules > 0 ? 2 : 0),
                checkedCurrent: lenCheckedOfModules,
                checkedTotal: lenPermissionOfModules,
                feature: []
            }
            // set permission theo feature
            const arrFeature = rolePermisson.filter(t => t.module_id == module.id && !t.parent_id.trim())

            for (let index1 = 0; index1 < arrFeature.length; index1++) {
                const feature = arrFeature[index1];
                moduleObj.feature.push({
                    ...feature,
                    permission: rolePermisson.filter(t => t.module_id == module.id && t.parent_id.trim() == feature.id),
                })
            }

            // set data theo module
            viewData[module.id] = moduleObj
        }
        return viewData
    }

    render() {
        const { error, errorList, modules, modulesFunction, permission, viewData } = this.state
        return (
            <Context.Provider value={{ ...this.state }}>
                <Helmet idMessage="role.header" defaultMessage="Vai trò" />
                <SegmentHeader style={{ paddingBottom: "14px" }}>
                    <PageHeader />
                </SegmentHeader>

                {error && <Message negative list={errorList} />}

                <RoleDashLetContent />
            </Context.Provider>
        );
    }
}
export const Context = React.createContext();
export default RoleDashlet;

