import React, { useContext, useState } from 'react';
import { Accordion, Icon, Checkbox, Segment, Header, Table } from 'semantic-ui-react';
import { Context } from './index';

const RoleDashletContent = (props) => {
    const [activeIndex, setActiveIndex] = useState();
    const { viewData, handleChecked, handleCheckedDashlets, roleDashlet, SysDashlets, checkall } = useContext(Context)

    const handleClick = (e, propsData, moduleItem) => {
        const { index, active } = propsData
        const newIndex = activeIndex === index ? -1 : index

        // click expand
        if (e.target.tagName == "DIV") {
            setActiveIndex(newIndex)
        } else if (!active) {
            // click checked và ko active
            setActiveIndex(newIndex)
        }
    }

    return (
        <>
            <Segment>
                <Header as="h3" content="Check chọn danh sách Dashlet mà vai trò có quyền truy cập" />
                <Table celled compact selectable size="small" unstackable>
                    <Table.Header>
                        <Table.HeaderCell width="3"> &nbsp;&nbsp; <Checkbox checked={checkall} onChange={(e, propsData) => handleCheckedDashlets(propsData, SysDashlets)} /></Table.HeaderCell>
                        <Table.HeaderCell content="Title" />
                        <Table.HeaderCell content="Ghi chú" />
                    </Table.Header>
                    <Table.Body>
                        {SysDashlets && SysDashlets.length > 0 && SysDashlets.map((item, index) => {
                            return <Table.Row>
                                <Table.Cell>
                                    <Checkbox key={item.id + index}
                                        style={{ padding: "7px" }}
                                        checked={item.check}
                                        // label={item.ma_dashlet}
                                        name={item.id}
                                        onChange={(e, propsData) => handleChecked(propsData, item,)}
                                    />
                                </Table.Cell>
                                <Table.Cell>
                                    {item.ten_dashlet}
                                </Table.Cell>
                                <Table.Cell>
                                    {item.ghi_chu}
                                </Table.Cell>
                            </Table.Row>
                        })}
                    </Table.Body>
                </Table>
                {/* <Accordion fluid styled>
                {
                    viewData && Object.keys(viewData).length > 0 && Object.keys(viewData)
                        .map((moduleId, index) => {
                            const moduleItem = viewData[moduleId];

                            return moduleItem.feature && moduleItem.feature.length > 0 &&
                                <React.Fragment key={moduleId}>
                                    <Accordion.Title
                                        active={activeIndex === index}
                                        index={index}
                                        onClick={(e, propsData) => handleClick(e, propsData, moduleItem)}
                                    >
                                        <Checkbox name={moduleId}
                                            checked={moduleItem.check === 1}
                                            indeterminate={moduleItem.check === 2}
                                            onChange={(e, propsData) => handleCheckedModules(propsData, moduleItem)}
                                        />
                                        &emsp;

                                        <Icon name='dropdown' />
                                        {moduleItem.name}
                                    </Accordion.Title>

                                    <Accordion.Content active={activeIndex === index}>
                                        {
                                            moduleItem.feature.map((item, indexItem) => {
                                                const isLastRow = (moduleItem.feature.length == (indexItem + 1))
                                                return <div key={item.id}
                                                    style={{
                                                        display: "flex",
                                                        padding: "7px",
                                                        borderBottom: isLastRow ? undefined : "1px solid rgba(34, 36, 38, 0.15)"
                                                    }}>
                                                    <div style={{ width: "30%", paddingTop: "7px", fontWeight: "bold" }}>
                                                        {item.name}
                                                    </div>

                                                    <div style={{ width: "70%" }}>
                                                        {item.permission.map(per => {
                                                            return <Checkbox key={per.id}
                                                                style={{ padding: "7px" }}
                                                                checked={per.check}
                                                                label={per.name}
                                                                name={per.id}
                                                                onChange={(e, propsData) => handleChecked(propsData, per, moduleItem)}
                                                            />
                                                        })}
                                                    </div>
                                                </div>
                                            })
                                        }
                                    </Accordion.Content>
                                </React.Fragment>

                        })
                }
            </Accordion> */}
            </Segment>
        </>
    );
};

export default RoleDashletContent;