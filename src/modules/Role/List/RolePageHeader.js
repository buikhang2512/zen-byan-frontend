import React, { PureComponent } from "react";
import * as routes from "constants/routes";
import * as permissions from "../../../constants/permissions";
import { Context } from "./index";
import { ButtonAdd, HeaderLink } from "components/Control";
import { FormMode } from "../../../utils/global";
import { ButtonRefresh } from "../../../components/Control";
import { Button } from "semantic-ui-react";

class RolePageHeader extends PureComponent {
  state = {
    listHeader: [
      {
        id: "role.header",
        defaultMessage: "Vai trò",
        route: routes.Role,
        active: true,
      },
    ],
  };

  render() {
    return (
      <Context.Consumer>
        {(ctx) => (
          <React.Fragment>
            <HeaderLink listHeader={this.state.listHeader} isSetting />
            <ButtonAdd
              permission={permissions.RoleThem}
              floated="right"
              size="small"
              onClick={() => ctx.handleAction(FormMode.ADD)}
            />
            <Button.Group floated="right" size="small">
              <ButtonRefresh onClick={() => ctx.handleClickSearch("refresh")} />
            </Button.Group>
            <div style={{ clear: "both" }} />
          </React.Fragment>
        )}
      </Context.Consumer>
    );
  }
}

export default RolePageHeader;
