import React from "react";
import { Form, Modal, Message, Dimmer, Loader, Icon } from "semantic-ui-react";
import { FormattedMessage } from 'react-intl';
import { withFormik } from 'formik';
import * as Yup from 'yup';

import { MESSAGES } from '../../../utils/Messages';
import { ZenHelper } from '../../../utils/global'
import { ButtonSave, ButtonCancel, ZenField, ZenMessageToast } from "../../../components/Control/index";
import { ApiRole } from '../api/ApiRole';
import * as permissions from "../../../constants/permissions";
import { auth } from "../../../utils";

class RoleFormAdd extends React.Component {
   isMounted = false
   constructor(props) {
      super();

      this.state = {
         open: false,
         loadingForm: true,
         btnLoadingSave: false,
         error: false,
         errorList: [],
         items: initItem,

         permissionEdit: true,
      }
   }

   loadData() {
      const { id } = this.props
      ApiRole.getByID(id, res => {
         if (res.status === 200) {
            if (this.isMounted) {
               this.setState({
                  loadingForm: false,
                  error: false,
                  errorList: [],
                  items: res.data.data,
                  permissionEdit: auth.checkPermission(permissions.RoleSua)
               })

               this.props.setValues(res.data.data)
            }
         } else {
            this.state.errorList.push('' + res)
            this.setState({
               loadingForm: false,
               error: true,
            })
         }
      })
   }

   validData() {
      const { errors } = this.props
      if (errors.name) {
         return false;
      }
      return true;
   }

   saveData = (item) => {
      this.setState({ btnLoadingSave: true })
      if (item.id) {
         ApiRole.update(item, res => {
            if (res.status >= 200 && res.status <= 204) {
               ZenMessageToast.success();
               this.afterSave(item)
            } else {
               this.state.errorList.push("" + res)
               this.setState({
                  btnLoadingSave: false,
                  loadingForm: false,
                  error: true,
               })
            }
         })
      } else {
         ApiRole.insert(item, res => {
            if (res.status >= 200 && res.status <= 204) {
               ZenMessageToast.success();
               this.afterSave(res.data.data)
            } else {
               this.state.errorList.push("" + res)
               this.setState({
                  btnLoadingSave: false,
                  loadingForm: false,
                  error: true,
               })
            }
         })
      }
   }

   afterSave = (item) => {
      this.props.onAfterSave ? this.props.onAfterSave(item) : this.handleCloseForm()
   }

   // ------------------------   component
   componentDidMount() {
      this.isMounted = true
      if (this.props.onRef) {
         this.props.onRef(this)
      }

      if (this.props.id) {
         this.loadData()
      } else {
         this.setState({ loadingForm: false })
      }
   }

   componentWillUnmount() {
      this.isMounted = false
      if (this.props.onRef) {
         this.props.onRef(undefined)
      }
   }

   static getDerivedStateFromProps(props, state) {
      if (props.open !== state.open) {
         return {
            open: props.open,
            loadingForm: props.id ? true : false,
            items: initItem
         };
      }
      return null;
   }

   componentDidUpdate(prevProps) {
      if (prevProps.open !== this.props.open && this.props.open) {
         if (this.props.id) {
            this.loadData()
         } else {
            this.props.setValues(initItem)
         }
      }
   }

   // ------------------------   handle
   handleSaveForm = (e, submit) => {
      e.preventDefault();

      if (e.target.id === submit.id) {
         if (this.validData()) {
            this.saveData(this.props.values)
         }
      }
   }

   handleCloseForm = () => {
      this.props.onClose && this.props.onClose()
   }

   render() {
      const { id, open, onCloseForm, onAfterSave, header, editItem, ...rest } = this.props
      const { loadingForm, error, errorList, permissionEdit } = this.state

      return (
         <Modal size='tiny' closeOnEscape closeIcon closeOnDimmerClick={false}
            open={open} onClose={this.handleCloseForm}>

            <Modal.Header>
               <Icon name="folder open outline" />
               {!id ? <FormattedMessage id={MESSAGES.CPControlAdd} defaultMessage="Thêm mới"
                  values={{ mess: header }} />
                  : header || ZenHelper.GetMessage(MESSAGES.CPControlEdit)}
            </Modal.Header>

            <Modal.Content>
               {error && <Message negative list={errorList} />}

               {loadingForm && <Dimmer inverted active={loadingForm}>
                  <Loader inverted />
               </Dimmer>}

               <Form id="modal-role" onSubmit={this.handleSaveForm}>
                  <ZenField required readOnly={!permissionEdit}
                     label={"role.name"} defaultlabel="Vai trò"
                     name="name" props={rest}
                  />
                  <ZenField readOnly={!permissionEdit}
                     label={"role.description"} defaultlabel="Mô tả"
                     name="description" props={rest}
                  />
               </Form>
            </Modal.Content>

            <Modal.Actions>
               <ButtonCancel type="button" size="small" onClick={this.handleCloseForm} />
               {permissionEdit && <ButtonSave type='submit' size="small" form="modal-role" loading={this.state.btnLoadingSave}
                  onClick={(e) => this.props.handleSubmit()} />}
            </Modal.Actions>
         </Modal>
      );
   }
}

export default withFormik({
   mapPropsToValues(props) { // Init form field
      return Object.assign({}, { ...initItem });
   },

   validationSchema: (props) => Yup.object().shape({ // Validate form field
      name: Yup.string()
         .required(ZenHelper.GetMessage(MESSAGES.Required))
      ,
   }),

   handleSubmit: (values, { setSubmitting }) => {
      setTimeout(() => {

      }, 1000);
   },
   enableReinitialize: true,
   isInitialValid: false

})(RoleFormAdd)

RoleFormAdd.defaultProps = {
   onRef: undefined,    // call handle from parent component
   id: undefined,       // id of items
   open: false,         // close - open modal
   header: undefined,   // header modal
   editItem: {},        // items edit
   isView: false,       // only view
   onClose: undefined,  // event close modal
   onAfterSave: undefined  // event after save data
};

const initItem = {
   name: "",
   description: ""
}