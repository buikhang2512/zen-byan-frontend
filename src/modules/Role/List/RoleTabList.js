import React from "react";
import { Link } from "react-router-dom";
import { Table, Button, Label } from "semantic-ui-react";
import { FormattedMessage } from "react-intl";
import * as routes from "constants/routes";
import * as permissions from "../../../constants/permissions";
import { Context } from "./index";
import { ButtonDelete, ButtonEdit } from "components/Control";
import { MESSAGES } from "utils/Messages";
import { FormMode } from "../../../utils/global";
import { ZenLink } from "../../../components/Control";

const RoleTabList = (props) => {
  return (
    <Context.Consumer>
      {(ctx) => (
        <Table celled selectable striped>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell width="1" textAlign="center">
                <FormattedMessage
                  id={MESSAGES.GeneralzID}
                  defaultMessage="zID"
                />
              </Table.HeaderCell>
              <Table.HeaderCell textAlign="center">
                <FormattedMessage id="role.name" defaultMessage="Vai trò" />
              </Table.HeaderCell>
              <Table.HeaderCell textAlign="center">
                <FormattedMessage
                  id="role.description"
                  defaultMessage="Mô tả"
                />
              </Table.HeaderCell>
              {/* <Table.HeaderCell>
                User
              </Table.HeaderCell> */}
              <Table.HeaderCell collapsing></Table.HeaderCell>
            </Table.Row>
          </Table.Header>

          <Table.Body>
            {ctx.data
              ? ctx.data.data.map((item) => {
                return (
                  <Table.Row key={item.id}>
                    <Table.Cell textAlign="center">{item.id}</Table.Cell>
                    <Table.Cell>
                      <ZenLink
                        permission={permissions.RoleThem}
                        to={routes.RoleInfo(item.id)}
                      >
                        {item.name}
                      </ZenLink>
                    </Table.Cell>
                    <Table.Cell>{item.description}</Table.Cell>
                    {/* <Table.Cell>
                      {item.userNames && item.userNames.map((t, idx) => {
                        return <Label key={idx} content={t} />
                      })}
                    </Table.Cell> */}
                    <Table.Cell collapsing>
                      <Button.Group size="small">
                        <Button
                          basic
                          icon="tasks"
                          color="green"
                          as={Link}
                          to={routes.RoleInfo(item.id)}
                        />
                        <Button
                          basic
                          icon="th"
                          color="green"
                          as={Link}
                          to={routes.RoleInfoDashlet(item.id)}
                        />
                        <ButtonEdit
                          permission={permissions.RoleSua}
                          onClick={() =>
                            ctx.handleAction(FormMode.EDIT, { item: item })
                          }
                        />
                        <ButtonDelete
                          permission={permissions.RoleXoa}
                          onClick={() =>
                            ctx.handleAction(FormMode.DEL, { item: item })
                          }
                        />
                      </Button.Group>
                    </Table.Cell>
                  </Table.Row>
                );
              })
              : undefined}
          </Table.Body>
        </Table>
      )}
    </Context.Consumer>
  );
};

export default RoleTabList;
