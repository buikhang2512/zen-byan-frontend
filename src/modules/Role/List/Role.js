import React, { Component } from "react";
import { Message, Segment, Input, Icon } from "semantic-ui-react";
import { Context, PageHeader, TabList, FormAdd } from "./index";
import { ApiRole } from "../api/ApiRole";
import {
  ConfirmDelete,
  Helmet,
  ZenMessageToast,
  SegmentHeader,
  ButtonAdd,
  MessageNone,
} from "../../../components/Control/index";
import { FormMode, ZenHelper } from "../../../utils/global";
import * as permissions from "../../../constants/permissions";

class Role extends Component {
  constructor(props) {
    super();
    this.state = {
      activeTab: "list",
      btnLoadingDel: false,
      loadingSearch: true,
      loading: true,
      error: false,
      errorList: [],
      data: { data: [] },
      currentItem: {},
      formMode: FormMode.NON,

      changeTabItem: () => this.changeTabItem,
      handleAction: (action, data) => this.handleAction(action, data),
      handleRefresh: () => this.handleRefresh,

      handleEnterSearch: () => this.handleEnterSearch,
      handleSearchChange: () => this.handleSearchChange,
      handleClickSearch: (type) => this.handleClickSearch(type),
    };
  }
  createQuery(keyword) {
    var key = keyword.replace(/\\/g, "\\\\");
    key = key.replace(/"/g, `\\"`);

    let query = ` name.Contains("${key}") `;
    return query;
  }

  changeTabItem = (e, { name }) => {
    if (name === this.state.activeTab) return;
    this.setState({ activeTab: name });
  };

  loadData(keyword) {
    let query = keyword ? this.createQuery(keyword) : null;
    ApiRole.get((res) => {
      if (res.status === 200) {
        this.setState({
          keyword: keyword,
          loading: false,
          loadingSearch: false,
          data: res.data,
        });
      } else {
        this.state.errorList.push("" + res);
        this.setState({
          loadingSearch: false,
          loading: false,
          error: true,
        });
      }
    }, query);
  }

  componentDidMount() {
    this.loadData();
  }

  handleAction = (action, data = {}) => {
    const { item } = data;

    if (action === FormMode.EDIT) {
      this.state.currentItem = Object.assign({}, item);
    } else if (action === FormMode.DEL) {
      this.state.currentItem = Object.assign({}, item);
    } else {
      this.state.currentItem = {};
    }

    this.setState({ formMode: action });
  };

  handleOkDelete = () => {
    const { currentItem, data } = this.state;
    this.setState({ btnLoadingDel: true });

    ApiRole.delete(currentItem.id, (res) => {
      if (res.status >= 200 && res.status <= 204) {
        const idx = data.data.findIndex((x) => x.id === currentItem.id);
        if (idx > -1) {
          data.data.splice(idx, 1);
        }

        ZenMessageToast.success();

        this.setState({
          formMode: FormMode.NON,
          btnLoadingDel: false,
          loadingSearch: false,
          error: false,
          errorList: [],
          data: data,
          currentItem: {},
        });
      } else {
        this.state.errorList.push("" + res);
        this.setState({
          formMode: FormMode.NON,
          btnLoadingDel: false,
          loadingSearch: false,
          error: true,
          currentItem: {},
        });
      }
    });
  };

  handleAfterSaveData = (item) => {
    const { data, currentItem } = this.state;
    if (currentItem.id) {
      const idx = data.data.findIndex((x) => x.id === currentItem.id);
      data.data[idx] = item;
    } else {
      data.data.push(item);
    }
    this.setState({ formMode: FormMode.NON, data, currentItem: {} });
  };

  // ================================== search
  handleSearchChange = (e, { value }) => {
    this.setState({ keyword: value });
  };

  handleEnterSearch = (e) => {
    if (e.key === 'Enter') {
      this.setState({ loadingSearch: true });

      clearTimeout(this.timer);
      this.timer = setTimeout(() => {
        this.loadData(this.state.keyword);
      }, 300);
    }
  };

  handleClickSearch = (type) => {
    this.setState({ loadingSearch: true });
    if (type === "refresh") {
      this.loadData("");
    } else {
      // click icon trên input search
      this.loadData(this.state.keyword);
    }
  };

  render() {
    const {
      error,
      errorList,
      formMode,
      currentItem,
      keyword,
      data,
      loading,
      loadingSearch,
    } = this.state;

    return (
      <Context.Provider value={this.state}>
        <Helmet idMessage="role.header" defaultMessage="Vai trò" />
        <SegmentHeader>
          <PageHeader />
        </SegmentHeader>

        {error && <Message negative list={errorList} />}

        <Segment loading={loading}>
          <div>
            <Input
              size="small"
              value={keyword || ""}
              style={{ float: "right", width: "250px" }}
              placeholder={"Search"}
              loading={loadingSearch}
              onChange={this.handleSearchChange}
              onKeyDown={this.handleEnterSearch}
              icon={
                <Icon
                  link
                  name="search"
                  onClick={() => this.handleClickSearch("search")}
                />
              }
            />
          </div>
          <div style={{ clear: "both" }} />
          {data.data && data.data.length > 0 ? (
            <TabList />
          ) : (
              <MessageNone>
                <ButtonAdd
                  permission={permissions.RoleThem}
                  size="small"
                  onClick={() => this.handleAction(FormMode.ADD)}
                />
              </MessageNone>
            )}
        </Segment>

        {(formMode === FormMode.ADD || formMode === FormMode.EDIT) && (
          <FormAdd
            id={currentItem.id}
            //editItem={currentItem}
            header={ZenHelper.GetMessage("role.header", "Vai trò")}
            open={formMode === FormMode.ADD || formMode === FormMode.EDIT}
            onAfterSave={this.handleAfterSaveData}
            onClose={() => this.handleAction(FormMode.NON)}
          />
        )}

        {formMode === FormMode.DEL && (
          <ConfirmDelete
            open={formMode === FormMode.DEL}
            loading={this.state.btnLoadingDel}
            onCancel={() => this.handleAction(FormMode.NON)}
            onConfirm={this.handleOkDelete}
          />
        )}
      </Context.Provider>
    );
  }
}

export default Role;
