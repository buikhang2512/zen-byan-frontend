import React from 'react';
import { Segment, Menu } from 'semantic-ui-react';
import { FormattedMessage } from "react-intl";
import { Context, TabList } from './index';
import { MESSAGES } from 'utils/Messages';

const RoleTabMenu = () => {
   return (
      <Context.Consumer>
         {ctx => (
            <React.Fragment>
               <Menu pointing secondary>
                  <Menu.Item name="list" as="a" active={ctx.activeTab === "list"} onClick={ctx.changeTabItem()}
                     content={<FormattedMessage id={MESSAGES.GeneralList} defaultMessage="Danh sách" />} />
               </Menu>

               <Segment loading={ctx.loading}>
                  {ctx.activeTab === "list" && <TabList />}
               </Segment>
            </React.Fragment>
         )}
      </Context.Consumer>
   );
}
export default RoleTabMenu;