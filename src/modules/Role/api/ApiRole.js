import { axios } from 'Api';

export const ApiRole = {
   get(callback, query = undefined, pagination = {}) {
      axios.get(`roles`, {
         params: {
            qf: query,
            page: pagination.page,
            pageSize: pagination.pageSize
         },
      })
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   getByID(id, callback) {
      axios.get(`roles/` + id,
      )
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   insert(data, callback) {
      axios.post(`roles`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   addPermission(data, callback) {
      axios.put(`roles/permission`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   update(data, callback) {
      axios.put(`roles`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   delete(id, callback) {
      axios.delete(`roles/` + id)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   }
}