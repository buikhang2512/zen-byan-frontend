import React, { Component } from 'react';
import { Message } from 'semantic-ui-react';
import { PageHeader, RoleContent } from './index';
import { Helmet, SegmentHeader, ZenMessageToast } from '../../../components/Control/index';
import { ApiPermission, ApiRole, ApiModule } from '../../../Api';
import { ZenHelper } from '../../../utils/global';

class RoleEntry extends Component {
    constructor(props) {
        super();
        this.state = {
            loading: true,
            error: false,
            errorList: [],

            roleInfo: {},
            modules: [],
            modulesFunction: [],
            permission: [],
            viewData: null,

            btnSaveLoading: false,

            handleChecked: this.handleChecked,
            handleSave: this.handleSave,
            handleCheckedModules: this.handleCheckedModules
        }
    }

    componentDidMount() {
        const { match } = this.props
        const roleInfo = new Promise((resolve, reject) => {
            ApiRole.getByID(match.params.id, res => {
                if (res.status === 200) {
                    resolve(res.data.data)
                } else {
                    reject(res)
                }
            })
        })

        const permission = new Promise((resolve, reject) => {
            ApiPermission.get(res => {
                if (res.status === 200) {
                    resolve(res.data.data)
                } else {
                    reject(res)
                }
            })
        })

        const modules = new Promise((resolve, reject) => {
            ApiModule.get(res => {
                if (res.status === 200) {
                    resolve(res.data.data)
                } else {
                    reject(res)
                }
            })
        })

        Promise.all([roleInfo, permission, modules])
            .then(values => {
                // set permission của user vào list permission
                const rolePermisson = values[1]
                    .map(t => { t.check = false; return t })
                    .sort((a, b) => a.ordinal - b.ordinal)

                values[0].rolePermissions.forEach(element => {
                    const idx = rolePermisson.findIndex(x => x.id === element.permission_id)
                    if (idx > -1) {
                        rolePermisson[idx].check = true
                    }
                })

                // map thành tree data
                const modules = values[2].sort((a, b) => a.ordinal - b.ordinal);
                const viewData = this.mapToTreeData(modules, rolePermisson)

                this.setState({
                    error: false,
                    loading: false,
                    roleInfo: values[0],
                    permission: rolePermisson,
                    modules: modules,
                    viewData: viewData,
                })
            }).catch(err => {
                this.setState({
                    error: ZenHelper.getResponseError(err),
                    loading: false
                })
            });
    }

    handleSave = () => {
        const { permission, roleInfo } = this.state
        const { match } = this.props
        this.setState({ btnSaveLoading: true })

        roleInfo.rolePermissions = permission.filter(t => t.check === true)
            .map(t => {
                return {
                    role_id: match.params.id,
                    permission_id: t.id
                }
            })

        setTimeout(() => {
            ApiRole.addPermission(roleInfo, res => {
                if (res.status === 204) {
                    ZenMessageToast.success();
                    this.setState({
                        error: false,
                        btnSaveLoading: false,
                    })
                } else {
                    this.state.errorList.push('' + res)
                    this.setState({
                        error: true,
                        btnSaveLoading: false
                    })
                }
            })
        }, 400)
    }

    handleChecked = ({ checked, name }, item, moduleItem) => {
        const { permission, viewData } = this.state
        const idx = permission.findIndex(x => x.id === name)
        if (idx > -1) {
            permission[idx].check = checked
        }
        // Số check hiện tại của module
        const checkedCurrent = moduleItem.checkedCurrent + (checked ? 1 : -1)

        // đánh dấu checked
        viewData[moduleItem.id].checkedCurrent = checkedCurrent;
        viewData[moduleItem.id].check = (moduleItem.checkedTotal == checkedCurrent ? 1 : (checkedCurrent > 0 ? 2 : 0))
        this.setState({ permission, viewData })
    }

    handleCheckedModules = ({ checked }, moduleItem) => {
        const { permission, viewData } = this.state
        const newPermission = permission.filter(t => t.module_id === moduleItem.id && t.parent_id.trim())
            .map(t => {
                t.check = checked
                return t
            })

        permission[moduleItem.id] = newPermission
        viewData[moduleItem.id].checkedCurrent = checked ? moduleItem.checkedTotal : 0;
        viewData[moduleItem.id].check = checked ? 1 : 0;

        this.setState({ permission, viewData })
    }

    mapToTreeData(modules, rolePermisson) {
        let viewData = {};
        for (let index = 0; index < modules.length; index++) {
            const module = modules[index]
            const lenCheckedOfModules = rolePermisson.filter(t => t.module_id == module.id && t.parent_id.trim() && t.check == true).length
            const lenPermissionOfModules = rolePermisson.filter(t => t.module_id == module.id && t.parent_id.trim()).length

            const moduleObj = {
                ...module,
                check: lenCheckedOfModules == lenPermissionOfModules ? 1 : (lenCheckedOfModules > 0 ? 2 : 0),
                checkedCurrent: lenCheckedOfModules,
                checkedTotal: lenPermissionOfModules,
                feature: []
            }
            // set permission theo feature
            const arrFeature = rolePermisson.filter(t => t.module_id == module.id && !t.parent_id.trim())

            for (let index1 = 0; index1 < arrFeature.length; index1++) {
                const feature = arrFeature[index1];
                moduleObj.feature.push({
                    ...feature,
                    permission: rolePermisson.filter(t => t.module_id == module.id && t.parent_id.trim() == feature.id),
                })
            }

            // set data theo module
            viewData[module.id] = moduleObj
        }
        return viewData
    }

    render() {
        const { error, errorList, modules, modulesFunction, permission, viewData } = this.state
        return (
            <Context.Provider value={{ ...this.state }}>
                <Helmet idMessage="role.header" defaultMessage="Vai trò" />
                <SegmentHeader style={{ paddingBottom: "14px" }}>
                    <PageHeader />
                </SegmentHeader>

                {error && <Message negative list={errorList} />}

                {/* <Modules /> */}
                <RoleContent />
            </Context.Provider>
        );
    }
}
export const Context = React.createContext();
export default RoleEntry;