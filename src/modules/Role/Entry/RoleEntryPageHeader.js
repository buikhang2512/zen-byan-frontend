import React, { PureComponent } from 'react';
import * as routes from 'constants/routes';
import { Context } from './index';
import { HeaderLink } from 'components/Control';
import { Breadcrumb } from 'semantic-ui-react';
import { ButtonSave } from '../../../components/Control';

class RoleEntryPageHeader extends PureComponent {
   state = {
      listHeader: [{
         id: "role.header",
         defaultMessage: "Vai trò",
         route: routes.Role,
         active: false
      }]
   }

   render() {
      return (
         <Context.Consumer>
            {ctx => (
               <React.Fragment>
                  <HeaderLink listHeader={this.state.listHeader} isSetting>
                     <Breadcrumb.Divider icon='right chevron' />

                     <Breadcrumb.Section active>
                        {ctx.roleInfo ? ctx.roleInfo.name : undefined}
                     </Breadcrumb.Section>
                  </HeaderLink>

                  <ButtonSave size="small" floated="right" onClick={ctx.handleSave}
                     loading={ctx.btnSaveLoading} disabled={ctx.btnSaveLoading} />
                  <div style={{ clear: "both" }} />
               </React.Fragment>
            )}
         </Context.Consumer>
      );
   }
}

export default RoleEntryPageHeader;