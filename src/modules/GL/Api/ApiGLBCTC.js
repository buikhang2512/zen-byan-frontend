import axios from '../../../Api/axios';

const ExtName = "bctc"

export const ApiGLBCTC = {
   get(callback, filter = {}, pagination = {}, params) {
      axios.get(`${ExtName}`, {
         params: {
            ...params,
            qf: filter.qf,
            keyword: filter.keyword,
            sort: filter.sort,
            page: pagination.page,
            pageSize: pagination.pageSize
         },
      })
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },
   getByCode(code, callback, params) {
      axios.post(`${ExtName}/single`, params)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   insert(data, callback) {
      axios.post(`${ExtName}`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   update(data, callback) {
      axios.put(`${ExtName}`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   updatePatch(code, data, callback, params) {
      axios.patch(`${ExtName}/${params.moduleid}/${params.id}`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   delete(code, callback, params) {
      axios.delete(`${ExtName}`, {data:{ ...params}})
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   }
}