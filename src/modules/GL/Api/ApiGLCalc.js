import axios from '../../../Api/axios';

const ExtName = "glcalc"

export const ApiGLCalc = {
   KetChuyenTuDong(data, callback) {
      axios.post(`${ExtName}/ketchuyentudong`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   XoaKetChuyenTuDong(data, callback) {
      axios.post(`${ExtName}/xoaketchuyentudong`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },
   getKetChuyenTuDong(filter, callback) {
      axios.get(`${ExtName}/ketchuyentudong`, {
         params: {
            module: filter.module
         }
      })
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },
   chuyenSoDuTK(data, callback) {
      axios.post(`${ExtName}/chuyensodutk`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   }
}