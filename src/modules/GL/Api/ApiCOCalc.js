import Integer from 'read-excel-file/commonjs/types/Integer';
import axios from '../../../Api/axios';

const ExtName = "cocalc"

export const ApiCOCalc = {
   getDataTypePB(filter, callback) {
      axios.get(`${ExtName}/datatypepb`, {
         params: {
            ksd: Boolean(filter.ksd),
            module: filter.module,
            language: filter.language
         }
      })
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },
   getDataPbPh(filter, callback) {
      axios.get(`${ExtName}/datapbph`, {
         params: {
            module: filter.module
         }
      })
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },
   getDataPbCt(filter, callback) {
      axios.get(`${ExtName}/datapbct`, {
         params: {
            year: Number(filter.year),
            module: filter.module
         }
      })
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },
   calPB(data, callback) {
      axios.post(`${ExtName}/calpb`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   calPBHS(data, callback) {
      axios.post(`${ExtName}/calpbhs`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },
   updPBHS(data, callback) {
      axios.post(`${ExtName}/updpbhs`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },
   delcalPB(data, callback) {
      axios.post(`${ExtName}/delcalpb`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },
   
}