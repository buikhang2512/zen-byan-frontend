import axios from '../../../Api/axios';

const ExtName = "GlDmKc"

export const ApiGlDmKc = {
   get(callback, filter = {}, pagination = {}) {
      axios.get(`${ExtName}`, {
         params: {
            qf: filter.qf,
            keyword: filter.keyword,
            sort: filter.sort,
            page: pagination.page,
            pageSize: pagination.pageSize
         },
      })
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   getByCode(code, callback, params) {
      axios.get(`${ExtName}/${params.moduleid}/${params.tk}/` + code)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   getDfValue(moduleid, callback) {
      axios.get(`${ExtName}/defaultVlue/${moduleid}`)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   insert(data, callback) {
      axios.post(`${ExtName}`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   update(data, callback) {
      axios.put(`${ExtName}`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   updatePatch(code, data, callback, params) {
      axios.patch(`${ExtName}/${params.moduleid}/${params.tk}/${code}`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   delete(code, callback, params) {
      axios.delete(`${ExtName}/${params.moduleid}/${params.tk}/` + code)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   }
}