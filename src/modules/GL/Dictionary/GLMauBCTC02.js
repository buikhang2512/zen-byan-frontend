import React, { useEffect } from "react";
import { ZenField, ZenFieldSelect, ZenFieldNumber, ZenFieldCheckbox, ZenFieldSelectApi } from "../../../components/Control/index";
import { FormMode, ZenHelper } from "../../../utils";
import { ApiGLMauBCTC02 } from "../Api";
import { Divider, Form } from "semantic-ui-react";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { useRouteMatch, useHistory } from 'react-router-dom';

const CACH_TINH = 'chkcach_tinh';
const GLMauBCTC02Modal = (props) => {
   const { formik, permission, mode } = props
   let match = useRouteMatch();
   // componentdidmount
   useEffect(() => {
      if (mode === FormMode.ADD) {
         formik.setFieldValue("mau", match?.mau ? match?.mau : "01");
      }
      if (mode === FormMode.EDIT) {
         formik.setFieldValue(CACH_TINH, formik.values?.tk ? true : false);
      }
   }, [formik.values.ma_so])

   return <React.Fragment>
      <Form.Group>
         <ZenFieldNumber width='4' required readOnly={!permission}
            label={"GLMauBCTC02.stt"} defaultlabel="Số thứ tự"
            name="stt" props={formik}
         />
         <ZenField width='12' required readOnly={!permission || mode == FormMode.EDIT}
            label={"GLMauBCTC02.ma_so"} defaultlabel="Mã số"
            name="ma_so" props={formik}
         />
      </Form.Group>
      <ZenField required readOnly={!permission}
         label={"GLMauBCTC02.tm"} defaultlabel="Thuyết minh"
         name="tm" props={formik}
      />
      <ZenField required readOnly={!permission}
         label={"GLMauBCTC02.chi_tieu"} defaultlabel="Chỉ tiêu"
         name="chi_tieu" props={formik}
      />
      <Divider />
      <Form.Group>
         <ZenFieldCheckbox readOnly={!permission} tabIndex={-1}
            label={"GLMauBCTC02.in_ck"} defaultlabel="In - In / Không in"
            name="in_ck" props={formik}
         /> 
         <ZenFieldCheckbox readOnly={!permission} tabIndex={-1}
            label={"GLMauBCTC02.bold"} defaultlabel="Kiểu chữ - Đậm / Không đậm"
            name="bold" props={formik}
         />
      </Form.Group>
      <Divider />
      <Form.Group> 
         <ZenFieldCheckbox readOnly={!permission} tabIndex={-1}
            label={"GLMauBCTC02.ts_nv"} defaultlabel="Phân loại - Tài sản / Nguồn vốn"
            name="ts_nv" props={formik} 
         />
         <ZenFieldCheckbox readOnly={!permission} tabIndex={-1}
            label={"GLMauBCTC02.ngoai_bang"} defaultlabel="Ngoại bảng - Ngoại bảng / Trong bảng"
            name="ngoai_bang" props={formik} 
         />
      </Form.Group> 
      <Divider /> 
      <Form.Group> 
         <ZenFieldCheckbox readOnly={!permission} tabIndex={-1}                          
            label={`GLMauBCTC02.${CACH_TINH}`} defaultlabel="Tính - Tính theo số dư tài khoản / tính theo công thức"
            name={CACH_TINH} props={formik}
         />
      </Form.Group>
      <ZenFieldSelectApi required={formik.values[CACH_TINH]}
         readOnly={!permission || (!formik.values[CACH_TINH])}
         loadApi defaultValue={formik.values['tk']}
         lookup={ZenLookup.TK}
         label={"GLMauBCTC02.tk"} defaultlabel="Tài khoản"
         name="tk" formik={formik}
      />
      <Form.Group>
         <ZenFieldCheckbox readOnly={!permission || (!formik.values[CACH_TINH])}
            label={"GLMauBCTC02.cong_no"} defaultlabel="Lấy chi tiết một vế của các đối tượng công nợ"
            name="cong_no" props={formik}
         />
         <ZenFieldCheckbox readOnly={!permission || (!formik.values[CACH_TINH])}
            label={"GLMauBCTC02.sodu_duong"} defaultlabel="Chỉ lấy số dư lớn hơn 0"
            name="sodu_duong" props={formik}
         />
      </Form.Group>
      <Form.Group widths='equal'>
         <ZenField width='12' required={!formik.values[CACH_TINH]}
            readOnly={!permission || (formik.values[CACH_TINH])}
            label={"GLMauBCTC02.cach_tinh"} defaultlabel="Công thức"
            name="cach_tinh" props={formik}
         />
      </Form.Group>
   </React.Fragment>
}

const GLMauBCTC02 = {
   FormModal: GLMauBCTC02Modal,
   api: {
      url: ApiGLMauBCTC02,
      params: ['mau', 'ma_so']
   },
   permission: {
      view: "",
      add: "",
      edit: ""
   },
   formId: "GLMauBCTC02-form",
   size: "tiny",
   initItem: {
      stt: 0,
      tk: "",
      cach_tinh: '',
      [CACH_TINH]: false
   },
   onBeforeSave: (item) => {
      return new Promise(resove =>{
         if (item[CACH_TINH]) {
            item.cach_tinh = '';
         } else {
            item.tk = '';
            item.cong_no = false;
            item.sodu_duong = false;
         }
         resove(item);
      })
   },
   formValidation: [
      {
         id: "ma_so",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
         ]
      },
      // {
      //    id: "cach_tinh",
      //    validationType: "string",
      //    validations: [
      //       {
      //          type: "required",
      //          params: ["Không được bỏ trống trường này"]
      //       },
      //    ]
      // },
      // {
      //    id: "tk",
      //    validationType: "string",
      //    validations: [
      //       {
      //          type: "required",
      //          params: ["Không được bỏ trống trường này"]
      //       },
      //       {
      //          type: "max",
      //          params: [20, "Không được vượt quá 20 kí tự"]
      //       },
      //    ]
      // },
      ZenHelper.genValidNumFormik('stt', 'number'),
   ],
   onValidate: (values, props) => {
      const errors = {};
      if (values[CACH_TINH] && !values['tk']) {
         errors['tk'] = "Không được bỏ trống trường này!";
      }
      if (!values[CACH_TINH] && !values['cach_tinh']) {
         errors['cach_tinh'] = "Không được bỏ trống trường này!";
      }
      return errors;
   }
}

export { GLMauBCTC02 };