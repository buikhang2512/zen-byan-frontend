import React, { useEffect, useState } from 'react';
import * as routes from "../../../constants/routes";
import { ApiGlDmKc, ApiGLDmTk, ApiCODMPB, ApiGLMauBCTC02 } from "../Api/index";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { Table } from "semantic-ui-react";
import { ZenLink } from '../../../components/Control';
import { useRouteMatch, useHistory } from 'react-router-dom';
import * as permissions from "../../../constants/permissions";

const ContainerGlMauBCTC02 = (props) => {
   const { onLoadData, fieldCode } = props;
   let match = useRouteMatch();
   useEffect(() => {
      if (match) {
         var mau = match.params.mau;
         onLoadData(`mau = '${mau}'`, { mau: mau });
      }
   }, []);

   return <>

   </>
}

const GL_Dictionary = {
   // GlDmKc: {
   //    route: routes.GlDmKc,

   //    action: {
   //       view: { visible: true, permission: "02.05.1" },
   //       add: { visible: true, permission: "02.05.1" },
   //       edit: { visible: true, permission: "02.05.1" },
   //       del: { visible: true, permission: "02.05.1" },
   //    },

   //    linkHeader: {
   //       id: "GlDmKc",
   //       defaultMessage: "Bút toán kết chuyển",
   //       active: true,
   //       isSetting: false,
   //    },

   //    tableList: {
   //       keyForm: "GlDmKc",
   //       formModal: ZenLookup.Ma_kc,
   //       fieldCode: "tk_du",
   //       unPagination: false,
   //       changeCode: false,
   //       duplicate: true,
   //       multiKey: ['moduleid', 'tk'],
   //       api: {
   //          url: ApiGlDmKc,
   //          type: "sql",
   //          params: ['moduleid', 'tk'],
   //          qf: `moduleid = 'GL'`
   //       },
   //       columns: [
   //          { id: "GlDmKc.stt", defaultMessage: "Số thứ tự", fieldName: "stt", type: "string", filter: "string", sorter: true, editForm: true },
   //          { id: "GlDmKc.so_ct", defaultMessage: "Số chứng từ", fieldName: "so_ct", type: "string", filter: "string", sorter: true, editForm: true },
   //          { id: "GlDmKc.ten_bt", defaultMessage: "Tên bút toán", fieldName: "ten_bt", type: "string", filter: "string", sorter: true, editForm: true },
   //          { id: "GlDmKc.loai_kc", defaultMessage: "Kiểu kết chuyển", fieldName: "loai_kc", type: "string", filter: "string", sorter: true, },
   //          { id: "GlDmKc.tk", defaultMessage: "Tài khoản", fieldName: "tk", type: "string", filter: "string", sorter: true, },
   //          { id: "GlDmKc.tk_du", defaultMessage: "Tài khoản đối ứng", fieldName: "tk_du", type: "string", filter: "string", sorter: true, },
   //          { id: "ksd", defaultMessage: "Không sử dụng", fieldName: "ksd", type: "bool", filter: "", sorter: true, collapsing: true },
   //       ],
   //    },
   // },
   GlDmTk: {
      route: routes.GlDmTk,

      action: {
         view: { visible: true, permission: permissions.GlDmTkXem },
         add: { visible: true, permission: permissions.GlDmTkThem },
         edit: { visible: true, permission: permissions.GlDmTkSua },
         del: { visible: true, permission: permissions.GlDmTkXoa },
         dup: { visible: true, permission: "" }
      },

      linkHeader: {
         id: "gldmtk",
         defaultMessage: "Danh mục tài khoản",
         active: true,
         isSetting: true,
      },

      tableList: {
         keyForm: "GlDmTk",
         formModal: ZenLookup.TK,
         fieldCode: "tk",
         unPagination: false,
         duplicate: true,
         api: {
            url: ApiGLDmTk,
            type: "sql",
         },
         columns: [
            { id: "gldmtk.tk", defaultMessage: "Mã TK", fieldName: "tk", type: "string", filter: "string", sorter: true, editForm: true, custom: true },
            { id: "gldmtk.ten_tk", defaultMessage: "Tên tài khoản", fieldName: "ten_tk", type: "string", filter: "string", sorter: true, editForm: true, custom: true },
            { id: "gldmtk.ma_nt", defaultMessage: "Mã NT", fieldName: "ma_nt", type: "string", filter: "string", sorter: true },
            { id: "gldmtk.tk_me", defaultMessage: "TK mẹ", fieldName: "tk_me", type: "string", filter: "string", sorter: true, },
            { id: "gldmtk.tk_cn", defaultMessage: "TK công nợ", fieldName: "tk_cn", type: "bool", filter: "", sorter: true, },
            { id: "gldmtk.kieu_sd", defaultMessage: "Kiểu SD", fieldName: "kieu_sd", type: "string", filter: "string", sorter: true, },
            { id: "gldmtk.chi_tiet", defaultMessage: "TK chi tiết", fieldName: "chi_tiet", type: "bool", filter: "", sorter: true, },
            { id: "gldmtk.bac_tk", defaultMessage: "Bậc TK", fieldName: "bac_tk", type: "string", filter: "string", sorter: true, },
            { id: "ksd", defaultMessage: "Không sử dụng", fieldName: "ksd", type: "bool", filter: "", sorter: true, collapsing: true },
         ],
         rowStyle: (item) => {
            // style chung cho dòng
            const rowStyle = {
               fontWeight: !item.chi_tiet ? "bold" : undefined,
               color: item.chi_tiet ? "black" : "#2557b8",
            }
            return {
               style: rowStyle,
               className: ""
            }
         },
         customCols: (data, item, fieldName, { onEdit, ...restCol }) => {
            let customCell = undefined
            if (fieldName === 'tk') {
               customCell = <Table.Cell key={fieldName} singleLine>
                  <ZenLink style={{ ...restCol.style }}
                     onClick={() => onEdit(item, { linkRow: restCol.link, editForm: restCol.editForm })}
                     permission=""
                  >
                     {item.tk}
                  </ZenLink>
               </Table.Cell>
            }
            else if (fieldName === 'ten_tk') {
               customCell = <Table.Cell key={fieldName} singleLine>
                  <ZenLink style={{
                     paddingLeft: `${Number(item.bac_tk - 1) * 14}px`,
                     ...restCol.style,
                  }}
                     onClick={() => onEdit(item, { linkRow: restCol.link, editForm: restCol.editForm })}
                     permission=""
                  >
                     {item.chi_tiet ? " " + item.ten_tk : ("+ " + item.ten_tk)}
                  </ZenLink>
               </Table.Cell>
            }
            return customCell
         }
      },
   },
   // CODMPB: {
   //    route: routes.CODMPB,

   //    action: {
   //       view: { visible: true, permission: "02.05.3" },
   //       add: { visible: true, permission: "02.05.3" },
   //       edit: { visible: true, permission: "02.05.3" },
   //       del: { visible: true, permission: "02.05.3" },
   //       dup: { visible: true, permission: "" }
   //    },

   //    linkHeader: {
   //       id: "CODMPB",
   //       defaultMessage: "Khai báo bút toán phân bổ tự động",
   //       active: true,
   //       isSetting: false,
   //    },

   //    tableList: {
   //       keyForm: "CoDmPb",
   //       formModal: ZenLookup.Ma_pb,
   //       fieldCode: "tk_pb",
   //       unPagination: false,
   //       changeCode: false,
   //       multiKey: ['moduleid', 'id', 'tk_pb'],
   //       api: {
   //          url: ApiCODMPB,
   //          type: "sql",
   //          params: ['moduleid', 'id', 'tk_pb'],
   //          qf: `moduleid = 'GL'`
   //       },
   //       columns: [
   //          { id: "CODMPB.stt", defaultMessage: "Số thứ tự", fieldName: "stt", type: "string", filter: "string", sorter: true, editForm: true },
   //          { id: "CODMPB.ten_bt", defaultMessage: "Tên bút toán", fieldName: "ten_bt", type: "string", filter: "string", sorter: true, editForm: true },
   //          { id: "CODMPB.tk_pb", defaultMessage: "TK phân bổ", fieldName: "tk_pb", type: "string", filter: "string", sorter: true, editForm: true },
   //          { id: "CODMPB.so_ct", defaultMessage: "Số chứng từ", fieldName: "so_ct", type: "string", filter: "string", sorter: true, editForm: true },
   //          { id: "CODMPB.theospct", defaultMessage: "Theo SPCT", fieldName: "theospct", type: "bool", filter: "", sorter: true, },
   //          { id: "CODMPB.theobp", defaultMessage: "Theo BP", fieldName: "theobp", type: "bool", filter: "", sorter: true, },
   //          { id: "CODMPB.theotkdu", defaultMessage: "Theo TKDU", fieldName: "theotkdu", type: "bool", filter: "", sorter: true, },
   //          { id: "ksd", defaultMessage: "Không sử dụng", fieldName: "ksd", type: "bool", filter: "", sorter: true, collapsing: true },
   //       ],
   //    },
   // },
   // GLMAUBCTC02: {
   //    route: routes.GLMAUBCTC02(),

   //    action: {
   //       view: { visible: true, permission: "" },
   //       add: { visible: true, permission: "" },
   //       edit: { visible: true, permission: "" },
   //       del: { visible: true, permission: "" },
   //       dup: { visible: true, permission: "" }
   //    },

   //    linkHeader: {
   //       id: "GLMAUBCTC02",
   //       defaultMessage: "Khai báo công thức - cân đối kế toán",
   //       active: true,
   //       isSetting: false,
   //    },

   //    tableList: {
   //       keyForm: "GLMAUBCTC02",
   //       formModal: ZenLookup.GLBS_MA_SO,
   //       fieldCode: "ma_so",
   //       unPagination: false,
   //       multiKey: ['mau', 'ma_so'],
   //       changeCode: false,
   //       ContainerTop: ContainerGlMauBCTC02,
   //       api: {
   //          url: ApiGLMauBCTC02,
   //          type: "sql",
   //          params: ['mau', 'ma_so'],
   //       },
   //       unPagination: true,
   //       columns: [
   //          { id: "GLMAUBCTC02.stt", defaultMessage: "Số thứ tự", fieldName: "stt", type: "number", filter: "number", sorter: true, editForm: true, collapsing: true },
   //          { id: "GLMAUBCTC02.ma_so", defaultMessage: "Mã số", fieldName: "ma_so", type: "string", filter: "string", sorter: true, editForm: true, collapsing: true },
   //          { id: "GLMAUBCTC02.chi_tieu", defaultMessage: "Chỉ tiêu", fieldName: "chi_tieu", type: "string", filter: "string", sorter: true, editForm: true },
   //          { id: "GLMAUBCTC02.tk", defaultMessage: "TK", fieldName: "tk", type: "string", filter: "string", sorter: true },
   //          { id: "GLMAUBCTC02.cach_tinh", defaultMessage: "Cách tính", fieldName: "cach_tinh", type: "string", filter: "", sorter: true, },
   //          { id: "GLMAUBCTC02.tm", defaultMessage: "Thuyết minh", fieldName: "tm", type: "string", filter: "", sorter: true, },
   //       ],
   //    },
   // },
}

export default GL_Dictionary