import React, { useEffect } from "react";
import { ZenField, ZenFieldSelect, ZenFieldNumber, ZenFieldCheckbox, ZenFieldSelectApi, ButtonDelete, ButtonAdd, ZenMessageAlert } from "../../../components/Control/index";
import { FormMode, ZenHelper } from "../../../utils";
import { ApiCODMPB } from "../Api";
import { Form, Checkbox, Table, Button, Divider } from "semantic-ui-react";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { isArray } from "lodash";
import _ from 'lodash';

const DETAILS = 'details'

const CODMPBModal = (props) => {
   const { formik, permission, mode } = props

   // componentdidmount
   useEffect(() => {
      if (mode === FormMode.ADD) {
         formik.setFieldValue(DETAILS, []);
         formik.setFieldValue("moduleid", props.moduleid ? props.moduleid : "GL");
         ApiCODMPB.getSTTValue(props.moduleid ? props.moduleid : "GL", res => {
            if (res.status == 200) {
               if (!res.data.data) {
                  return;
               }
               formik.setFieldValue("stt", res.data.data.stt);
            }
         }
         )
      }
   }, [])

   let onHandlePBTheo = (e, { name, checked }) => {
      formik.setFieldValue(name, checked)
      switch (name) {
         case "theobp":
            formik.setFieldValue("theotkdu", !checked)
            break;
         case "theotkdu":
            formik.setFieldValue("theobp", !checked)
            break;
         default:
            break;
      }
   }

   let checkTKNhanPh = (item) => {
      var kq = Boolean(_.find(formik.values[DETAILS], x => x.tk_nhan_pb == item.tk));
      if (kq) {
         ZenMessageAlert.warning(ZenHelper.GetMessage('trung_ma', 'TK đã có hoặc lồng nhau!'));
      }
      return kq;
   }

   let onHandleTkNhapPbChanged = (item, idx) => {
      let details = formik.values[DETAILS];
      if (!item || details[idx].tk_nhan_pb == item.tk || checkTKNhanPh(item)) {
         return;
      }
      details[idx].tk_nhan_pb = item.tk;
      details[idx].ten_tk_nha_pb = item.ten_tk;
      formik.setFieldValue(DETAILS, details);
   }

   let onHandleDeleteTKNhapnPh = (item, idx) => {
      let details = formik.values[DETAILS];
      _.remove(details, x => {
         return x.tk_nhan_pb == item.tk_nhan_pb
      });
      formik.setFieldValue(DETAILS, details);
   }

   let onHandleAddTKNhapnPh = () => {
      let details = formik.values[DETAILS];
      details.push({
         tk_nhan_pb: '',
         ten_tk_nha_pb: ''
      })
      formik.setFieldValue(DETAILS, details);
   }

   return <React.Fragment>
      <Form.Group widths='equal'>
         <ZenField required readOnly={!permission} autoFocus
            label={"CODMPB.ten_bt"} defaultlabel="Tên bút toán"
            name="ten_bt" props={formik}
         />
      </Form.Group>
      <Form.Group>
         <ZenFieldSelectApi required width={7}
            readOnly={!permission || (mode === FormMode.ADD ? false : true)}
            loadApi defaultValue={formik.values['tk_pb']}
            lookup={ZenLookup.TK}
            label={"CODMPB.tk_pb"} defaultlabel="TK phân bổ"
            name="tk_pb" formik={formik}
         />
         <ZenFieldNumber width='3' readOnly={!permission}
            label={"CODMPB.stt"} defaultlabel="Số thứ tự"
            name="stt" props={formik}
         />
         <ZenField width='6' required readOnly={!permission}
            label={"CODMPB.so_ct"} defaultlabel="Số chứng từ"
            name="so_ct" props={formik}
         />
      </Form.Group>
      <Form.Group widths='equal'>
         <Form.Field control={Checkbox}
            readOnly={!permission}
            label={ZenHelper.GetMessage('CODMPB.theospct', 'Theo SPCT ')}
            name="theospct" onChange={onHandlePBTheo}
            checked={formik.values['theospct']}
            tabIndex={-1}
         />
         <Form.Field control={Checkbox}
            label={ZenHelper.GetMessage('CODMPB.theobp', 'Theo bộ phận ')}
            name="theobp" onChange={onHandlePBTheo}
            checked={formik.values['theobp']}
            tabIndex={-1}
         />
         <Form.Field control={Checkbox}
            label={ZenHelper.GetMessage('CODMPB.theotkdu', 'Theo TKDU')}
            name="theotkdu" onChange={onHandlePBTheo}
            checked={formik.values['theotkdu']}
            tabIndex={-1}
         />
      </Form.Group>
      <Divider />
      <Table>
         <Table.Header>
            <Table.Row>
               <Table.HeaderCell>TK Nhập phân bổ</Table.HeaderCell>
               {/* <Table.HeaderCell>Tên tài khoản</Table.HeaderCell> */}
               <Table.HeaderCell collapsing>Action</Table.HeaderCell>
            </Table.Row>
         </Table.Header>
         <Table.Body>
            {formik.values[DETAILS] && isArray(formik.values[DETAILS]) && _.map(formik.values[DETAILS], (x, idx) => {
               return <Table.Row>
                  <Table.Cell>
                     <ZenFieldSelectApi required width={16} inline clearable={false}
                        readOnly={!permission}
                        loadApi value={formik.values[DETAILS][idx].tk_nhan_pb}
                        lookup={{
                           ...ZenLookup.TK,
                           where: 'chi_tiet = 1',
                           onLocalWhere: (items) => {
                              return items.filter(t => t.chi_tiet == true)
                           }
                        }}
                        label={" "} defaultlabel=""
                        name="tk_nhan_pb" //formik={formik}
                        onChange={(e, opt, item) => onHandleTkNhapPbChanged(item, idx)}
                     />
                  </Table.Cell>
                  <Table.Cell>
                     <ButtonDelete onClick={() => onHandleDeleteTKNhapnPh(x, idx)}
                     />
                  </Table.Cell>
               </Table.Row>
            })}
            <Table.Row>
               <Table.Cell colSpan={2}>
                  <ButtonAdd basic onClick={onHandleAddTKNhapnPh}
                  />
               </Table.Cell>
            </Table.Row>
         </Table.Body>
      </Table>

   </React.Fragment>
}

const CODMPB = {
   FormModal: CODMPBModal,
   api: {
      url: ApiCODMPB,
      params: ['moduleid', 'id', 'tk_pb']
   },
   permission: {
      view: "02.05.3",
      add: "02.05.3",
      edit: "02.05.3"
   },
   formId: "CODMPB-form",
   size: "tiny",
   initItem: {
      moduleid: 'GL',
      tk_pb: "",
      theo_bpsd: true,
      loai_pb: "",
      so_ct: '',
      stt: 0,
      ten_bt: '',
      ksd: false,
      details: []
   },
   formValidation: [
      {
         id: "moduleid",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
         ]
      },
      {
         id: "ten_bt",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
         ]
      },
      {
         id: "tk_pb",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
         ]
      },
   ]
}

export { CODMPB };