import React from "react";
import { ZenField, ZenFieldCheckbox, ZenFieldSelect, ZenFieldSelectApi } from "../../../components/Control/index";
import { FormMode } from "../../../utils";
import { ApiGLDmTk } from "../Api";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { Form, Label } from "semantic-ui-react";
import * as permissions from "../../../constants/permissions";

const optionKieuSd = [
   { key: "0", value: "0", text: "0. Không có số dư" },
   { key: "1", value: "1", text: "1. Dư nợ" },
   { key: "2", value: "2", text: "2. Dư có" },
   { key: "3", value: "3", text: "3. Lưỡng tính" },
]

const optionCachTinh = [
   { key: "0", value: "0", text: "0. Không tính" },
   { key: "1", value: "1", text: "1. Trung bình tháng" },
   { key: "2", value: "2", text: "2. Nhập trước xuất trước" },
   { key: "3", value: "3", text: "3. Bình quân di động" },
]

const FormModal = (props) => {
   const { formik, permission, mode } = props
   return <>
      <ZenFieldSelectApi loadApi
         lookup={ZenLookup.TK} readOnly={!permission || (mode === FormMode.ADD ? false : true)}
         label={"gldmtk.tk_me"} defaultlabel="TK mẹ"
         name="tk_me" formik={formik}
         onItemSelected={(item, { name }) => {
            const bac_tk = Number(item.bac_tk) + 1
            formik.setFieldValue("bac_tk", item.bac_tk ? bac_tk : 1)
         }}
      />

      <Form.Group>
         <ZenField required autoFocus readOnly={!permission || (mode === FormMode.ADD ? false : true)}
            label={"gldmtk.tk"} defaultlabel="Mã tài khoản"
            name="tk" props={formik} width="12"
         />
         <ZenField readOnly
            label={"gldmtk.bac_tk"} defaultlabel="Cấp"
            name="bac_tk" props={formik} width="4"
         />
      </Form.Group>

      <ZenField required readOnly={!permission}
         label={"gldmtk.ten_tk"} defaultlabel="Tên tài khoản"
         name="ten_tk" props={formik}
      />

      <Form.Group widths="equal">
         <ZenFieldSelectApi
            lookup={ZenLookup.Ma_nt}
            label={"rpt.ma_nt"} defaultlabel="Ngoại tệ"
            name="ma_nt" formik={formik}
         />
         <ZenFieldSelect readOnly={!permission} options={optionKieuSd}
            label={"gldmtk.kieu_sd"} defaultlabel="Kiểu SD"
            name="kieu_sd" props={formik} />
      </Form.Group>

      <Form.Group widths="equal">
         <ZenFieldCheckbox readOnly={!permission} tabIndex={-1}
            label={"gldmtk.tk_cn"} defaultlabel="TK công nợ"
            name="tk_cn" props={formik} />
         <ZenFieldCheckbox readOnly={!permission} tabIndex={-1}
            label={"gldmtk.tk_sc"} defaultlabel="TK sổ cái"
            name="tk_sc" props={formik} />
      </Form.Group>

      <Label size="large" ribbon color="blue" style={{ marginBottom: "7px" }}>
         Thông tin ngân hàng
      </Label>

      <ZenFieldSelectApi
         lookup={ZenLookup.Ma_ngh}
         label={"gldmtk.ma_ngh"} defaultlabel="Ngân hàng"
         name="ma_ngh" formik={formik}
         onItemSelected={(item, { name }) => {
            formik.setFieldValue("ten_ngh", item.ten_ngh)
         }}
      />
      <ZenField readOnly={!permission}
         label={"gldmtk.so_tk"} defaultlabel="Số tài khoản"
         name="so_tk" props={formik}
      />
      <ZenField readOnly={!permission}
         label={"gldmtk.ten_ngh"} defaultlabel="Tại ngân hàng"
         name="ten_ngh" props={formik}
      />
      <ZenField readOnly={!permission}
         label={"gldmtk.tinh_tp"} defaultlabel="Tỉnh/Thành phố"
         name="tinh_tp" props={formik}
      />

      <Label size="large" ribbon color="blue" style={{ marginBottom: "7px" }}>
         Đơn giá chêch lệch tỷ giá
      </Label>

      <Form.Group widths="equal">
         <ZenFieldSelect readOnly={!permission} options={optionCachTinh} upward
            label={"gldmtk.pp_tinh_tggs_no"} defaultlabel="Cách tính tỷ giá ghi sổ bên Nợ"
            name="pp_tinh_tggs_no" props={formik} />
         <ZenFieldSelect readOnly={!permission} options={optionCachTinh} upward
            label={"gldmtk.pp_tinh_tggs_co"} defaultlabel="Cách tính tỷ giá ghi sổ bên Có"
            name="pp_tinh_tggs_co" props={formik} />
      </Form.Group>
   </>
}

const GlDmTk = {
   FormModal: FormModal,
   api: {
      url: ApiGLDmTk,
   },
   permission: {
      view: permissions.GlDmTkXem,
      add: permissions.GlDmTkThem,
      edit: permissions.GlDmTkSua
   },
   formId: "gldmtk-form",
   size: "tiny",
   initItem: {
      tk: "", ten_tk: "", tk_me: "", bac_tk: 1,
      ma_nt: "", kieu_sd: "",
      tk_cn: false, tk_sc: false,
      ma_ngh: "", ten_ngh: "", tinh_tp: "", so_tk: "",
      pp_tinh_tggs_no: "", pp_tinh_tggs_co: "",
      ksd: false
   },
   formValidation: [
      {
         id: "tk",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
            {
               type: "max",
               params: [20, "Không được vượt quá 20 kí tự"]
            },
         ]
      },
      {
         id: "ten_tk",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
         ]
      },
   ]
}

export { GlDmTk };