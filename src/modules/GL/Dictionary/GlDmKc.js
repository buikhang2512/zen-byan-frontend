import React, { useEffect } from "react";
import { ZenField, ZenFieldSelect, ZenFieldNumber, ZenFieldCheckbox, ZenFieldSelectApi } from "../../../components/Control/index";
import { FormMode } from "../../../utils";
import { ApiGlDmKc } from "../Api";
import { Form } from "semantic-ui-react";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";

const options_loai = [
   { key: 1, value: "1", text: "Ghi nợ" },
   { key: 2, value: "2", text: "Ghi có" },
   { key: 3, value: "3", text: "Kết chuyển lãi lỗ" },
   { key: 4, value: "4", text: "Kết chuyển thuế GTGT" }
]

const GlDmKcModal = (props) => {
   const { formik, permission, mode } = props

   // componentdidmount
   useEffect(() => {
      if (mode === FormMode.ADD) {
         formik.setFieldValue("loai_kc", "1");
         formik.setFieldValue("moduleid", props.moduleid ? props.moduleid : "GL");
         ApiGlDmKc.getDfValue(props.moduleid ? props.moduleid : "GL", res => {
            if (res.status == 200) {
               if (!res.data.data) {
                  return;
               }
               formik.setFieldValue("stt", res.data.data.stt);
            }
         }
         )
      }
   }, [])

   return <React.Fragment>
      <Form.Group>
         <ZenField width='3' required readOnly={true}
            label={"GlDmKc.moduleid"} defaultlabel="Moduleid"
            name="moduleid" props={formik}
         />
         <ZenFieldNumber width='3' autoFocus readOnly={!permission}
            label={"GlDmKc.stt"} defaultlabel="Số thứ tự"
            name="stt" props={formik}
         />
         <ZenField width='10' required readOnly={!permission}
            label={"GlDmKc.so_ct"} defaultlabel="Số chứng từ"
            name="so_ct" props={formik}
         />
      </Form.Group>
      <ZenField required readOnly={!permission}
         label={"GlDmKc.ten_bt"} defaultlabel="Tên bút toán"
         name="ten_bt" props={formik}
      />
      <ZenFieldSelect readOnly={!permission} options={options_loai}
         label={"GlDmKc.loai_kc"} defaultlabel="Loại kết chuyển"
         name="loai_kc" props={formik}
         clearable={false}
      />

      <ZenFieldSelectApi required
         readOnly={!permission || (mode === FormMode.ADD ? false : true)}
         loadApi defaultValue={formik.values['tk']}
         lookup={ZenLookup.TK}
         label={"GlDmKc.tk"} defaultlabel="Tài khoản"
         name="tk" formik={formik}
      />
      <ZenFieldSelectApi required
         readOnly={!permission || (mode === FormMode.ADD ? false : true)}
         loadApi defaultValue={formik.values['tk_du']}
         lookup={ZenLookup.TK}
         label={"GlDmKc.tk_du"} defaultlabel="Tài khoản đối ứng"
         name="tk_du" formik={formik}
      />

      <Form.Group widths='equal'>
         <ZenFieldCheckbox readOnly={!permission}
            label={"GlDmKc.kc_hd"} defaultlabel="Kết chuyển theo hợp đồng"
            name="kc_hd" props={formik}
            tabIndex={-1}
         />
         <ZenFieldCheckbox readOnly={!permission}
            label={"GlDmKc.kc_bp"} defaultlabel="Kết chuyển theo bộ phận"
            name="kc_bp" props={formik}
            tabIndex={-1}
         />
      </Form.Group>
      <Form.Group widths='equal'>
         <ZenFieldCheckbox readOnly={!permission} 
            label={"GlDmKc.kc_spct"} defaultlabel="Kết chuyển theo SPCT"
            name="kc_spct" props={formik}
            tabIndex={-1}
         />
         <ZenFieldCheckbox readOnly={!permission}
            label={"GlDmKc.kc_phi"} defaultlabel="Kết chuyển theo phí"
            name="kc_phi" props={formik}
            tabIndex={-1}
         />
      </Form.Group>
      <ZenFieldCheckbox readOnly={!permission}
         label={"GlDmKc.kc_dt_ps"} defaultlabel="Không k/c các giao dịch không theo đối tượng"
         name="kc_dt_ps" props={formik}
         tabIndex={-1}
      />
   </React.Fragment>
}

const GlDmKc = {
   FormModal: GlDmKcModal,
   api: {
      url: ApiGlDmKc,
      params: ['moduleid', 'tk']
   },
   permission: {
      view: "02.05.1",
      add: "02.05.1",
      edit: "02.05.1"
   },
   formId: "GlDmKc-form",
   size: "tiny",
   initItem: {
      tk: "",
      tk_du: "",
      loai_kc: "",
      so_ct: '',
      stt: 0,
      ten_bt: '',
      ksd: false
   },
   formValidation: [
      {
         id: "moduleid",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
         ]
      },
      {
         id: "so_ct",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
         ]
      },
      {
         id: "loai_kc",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
         ]
      },
      {
         id: "ten_bt",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
         ]
      },
      {
         id: "tk",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
            {
               type: "max",
               params: [20, "Không được vượt quá 20 kí tự"]
            },
         ]
      },
      {
         id: "tk_du",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
            {
               type: "max",
               params: [20, "Không được vượt quá 20 kí tự"]
            },
         ]
      },
   ]
}

export { GlDmKc };