import React from "react";
import { Table } from "semantic-ui-react";
import { ZenFieldSelectApi } from "../../../components/Control";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { RptHeader, RptTable, RptTableCell, RptTableRow } from "../../../components/Control/zReport";
import * as routes from "../../../constants/routes";

const FilterForm = ({ formik }) => {
    const handleItemSelected = (item, { name }) => {
        if (name === "tk") {
            formik.setFieldValue("ten_tk", item.ten_tk);
        }
    };
    return <>
        <ZenFieldSelectApi required
            lookup={ZenLookup.TK}
            label={"rpt.tk"} defaultlabel="Tài khoản"
            name="tk" formik={formik}
            onItemSelected={handleItemSelected}
        />

        <ZenFieldSelectApi
            lookup={ZenLookup.Ma_nt}
            label={"rpt.ma_nt"} defaultlabel="Ngoại tệ"
            name="ma_nt" formik={formik}
        />
    </>
}

const TableForm = ({ data = [], filter = {} }) => {
    return <>
        <RptTable maNt={filter.Ma_nt}>
            <Table.Header fullWidth>
                <RptHeader maNt={filter.Ma_nt}
                    header={[
                        { text: ["rpt.tk_du", "TK Đ/Ư"] },
                        { text: ["rpt.ten_tk_du", "Tên TK"] },

                        { text: ["rpt.ps_no_nt", "PS nợ"], isNT: true },
                        { text: ["rpt.ps_co_nt", "PS có"], isNT: true },
                        { text: ["rpt.ma_nt", "Mã NT"], isNT: true, hiddenNT: true },

                        { text: ["rpt.ps_no", "PS nợ"] },
                        { text: ["rpt.ps_co", "PS có"] },
                    ]}
                />
            </Table.Header>
            <Table.Body>
                {data.length > 0 ? data.map((item, index) => {
                    const isNt = filter.Ma_nt && filter.Ma_nt !== 'VND' ? true : false
                    return <RptTableRow key={index} isBold={item.bold} index={index} item={item}>
                        <RptTableCell value={item.tk_du} />
                        <RptTableCell value={item.ten_tk_du} />

                        {isNt && <RptTableCell value={item.ps_no_nt} type="number" />}
                        {isNt && <RptTableCell value={item.ps_co_nt} type="number" />}
                        {isNt && <RptTableCell value={item.ma_nt} />}

                        <RptTableCell value={item.ps_no} />
                        <RptTableCell value={item.ps_co} />
                    </RptTableRow>
                }) : undefined}
            </Table.Body>
        </RptTable>
    </>
}

export const GLRptNKC07 = {
    FilterForm: FilterForm,
    TableForm: TableForm,
    permission: "02.40.8",
    visible: true,    // hiện/ẩn báo cáo
    route: routes.GLRptNKC07,

    period: {
        fromDate: "ngay_ct1",
        toDate: "ngay_ct2"
    },

    linkHeader: {
        id: "GLRptNKC07",
        defaultMessage: "Sổ chữ T của một tài khoản",
        active: true
    },

    info: {
        code: "02.21.26.1",
    },
    initFilter: {
        ngay_ct1: "",
        ngay_ct2: "",
        tk: "",
        ma_nt: "",
    },
    columns: [
        { id: "glnkc07.tk_du", defaultMessage: "Tk nợ", fieldName: "tk_du", type: "string", sorter: true },
        { id: "glnkc07.ten_tk_du", defaultMessage: "Tk có", fieldName: "ten_tk_du", type: "string", sorter: true },

        { id: "glnkc07.ps_no", defaultMessage: "PS nợ", fieldName: "ps_no_nt", type: "number", sorter: true, isNT: true },
        { id: "glnkc07.ps_co", defaultMessage: "PS có", fieldName: "ps_co_nt", type: "number", sorter: true, isNT: true },

        { id: "glnkc07.ps_no", defaultMessage: "PS nợ", fieldName: "ps_no", type: "number", sorter: true },
        { id: "glnkc07.ps_co", defaultMessage: "PS có", fieldName: "ps_co", type: "number", sorter: true },

        { id: "glnkc07.ma_nt", defaultMessage: "Mã NT", fieldName: "ma_nt", type: "string", sorter: true, isNT: true, hiddenNT: true },
    ],
    formValidation: [
        {
            id: "tk",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ]
        },
    ]
}