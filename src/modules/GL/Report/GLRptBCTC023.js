import React from "react";
import { Table, Form } from "semantic-ui-react";
import { ZenFieldSelectApi, ZenFieldDate, ZenFieldNumber, ZenFieldCheckbox } from "../../../components/Control";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { RptHeader, RptTable, RptTableCell, RptTableRow } from "../../../components/Control/zReport";
import * as routes from "../../../constants/routes";

const FilterForm = ({ formik }) => {
   return <>
      <ZenFieldDate
         label={"rpt.ngay"} defaultlabel="Ngày báo cáo"
         name="ngay" props={formik}
      />
      <ZenFieldCheckbox
         label={"rpt.ChiTieuCoSoLieu"} defaultlabel="Chỉ lên cho các chỉ tiêu có số liệu"
         name="chitieucosolieu" props={formik}
      />
   </>
}

const TableForm = ({ data = [], filter = {} }) => {
   return <>
      <RptTable maNt={filter.Ma_Nt}>
         <Table.Header fullWidth>
            <RptHeader maNt={filter.Ma_Nt}
               header={[
                  { text: ["rpt.stt", "STT"] },
                  { text: ["rpt.chi_tieu", "Chỉ tiêu"] },
                  { text: ["rpt.ma_so", "Mã số"] },
                  { text: ["rpt.tk", "Tài khoản"] },
                  { text: ["rpt.tien0", "Số đầu kỳ"] },
                  { text: ["rpt.tien_nt0", "Số đầu kỳ NT"], isNT: true },
                  { text: ["rpt.tien", "Số cuối kỳ"] },
                  { text: ["rpt.tien_nt", "Số cuối kỳ NT"], isNT: true },
                  { text: ["rpt.tm", "TM"] },
                  { text: ["rpt.cach_tinh", "Cách tính"] },
               ]}
            />
         </Table.Header>
         <Table.Body>
            {data.length > 0 ? data.map((item, index) => {
               const isNt = filter.Ma_Nt && filter.Ma_Nt !== 'VND' ? true : false
               return <RptTableRow key={index} isBold={item.bold} index={index} item={item}>
                  <RptTableCell value={item.stt} />
                  <RptTableCell value={item.chi_tieu} />
                  <RptTableCell value={item.ma_so} />
                  <RptTableCell value={item.tk} />
                  <RptTableCell value={item.tien0} type="number" />
                  {isNt && <RptTableCell value={item.tien_nt0} type="number" />}
                  <RptTableCell value={item.tien} type="number" />
                  {isNt && <RptTableCell value={item.tien_nt} type="number" />}
                  <RptTableCell value={item.tm} />
                  <RptTableCell value={item.cach_tinh} />
               </RptTableRow>
            }) : undefined}
         </Table.Body>
      </RptTable>
   </>
}

export const GLRptBCTC023 = {
   FilterForm: FilterForm,
   TableForm: TableForm,
   permission: "02.50.5",
   visible: true,    // hiện/ẩn báo cáo
   route: routes.GLRptBCTC023,
   isPaging: false,
   maubc: { route: routes.GLMAUBCTC02, mau: '03' },
   linkHeader: {
      id: "GLRptBCTC023",
      defaultMessage: "Bảng cân đối phát sinh các tài khoản",
      active: true
   },

   info: {
      code: "02.22.05.3",
   },
   initFilter: {
      ngay: new Date(),
      chitieucosolieu: false,
      mau: '03'
   },
   columns: [
      { id: "glbct021.stt", defaultMessage: "STT", fieldName: "stt", type: "string", sorter: true },
      { id: "glbct021.chi_tieu", defaultMessage: "Chỉ tiêu", fieldName: "chi_tieu", type: "string", sorter: true },
      { id: "glbct021.ma_so", defaultMessage: "Mã số", fieldName: "ma_so", type: "string", sorter: true },
      { id: "glbct021.tk", defaultMessage: "TK", fieldName: "tk", type: "string", sorter: true },

      { id: "glbct021.tien0", defaultMessage: "Đầu kỳ", fieldName: "tien_nt0", type: "number", sorter: true, isNT: true },
      { id: "glbct021.tien", defaultMessage: "Cuối kỳ", fieldName: "tien_nt", type: "number", sorter: true, isNT: true },

      { id: "glbct021.tien0", defaultMessage: "Đầu kỳ", fieldName: "tien0", type: "number", sorter: true },
      { id: "glbct021.tien", defaultMessage: "Cuối kỳ", fieldName: "tien", type: "number", sorter: true },

      { id: "glbct021.tm", defaultMessage: "TM", fieldName: "tm", type: "string", sorter: true },
      { id: "glbct021.cach_tinh", defaultMessage: "Cách tính", fieldName: "cach_tinh", type: "string", sorter: true },
   ],
   formValidation: []
}
