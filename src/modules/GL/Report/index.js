import { GLRptBK01 } from "./GLRptBK01";
import { GLRptTH01 } from "./GLRptTH01";
import { GLRptNKC02 } from "./GLRptNKC02";
import { GLRptNKC03T } from "./GLRptNKC03T";
import { GLRptNKC03C } from "./GLRptNKC03C";
import { GLRptNKC04 } from "./GLRptNKC04";
import { GLRptNKC05 } from "./GLRptNKC05";
import { GLRptNKC06 } from "./GLRptNKC06";
import { GLRptNKC07 } from "./GLRptNKC07";
import { GLRptNKC01All01 } from "./GLRptNKC01All01";
import { GLRptBCTC011 } from "./GLRptBCTC011";
import { GLRptBCTC012 } from "./GLRptBCTC012";
import { GLRptBCTC021 } from "./GLRptBCTC021";
import { GLRptBCTC022 } from "./GLRptBCTC022";
import { GLRptBCTC023 } from "./GLRptBCTC023";

export const GL_Report = {
  GLRptBK01: GLRptBK01,
  GLRptTH01: GLRptTH01,
  GLRptNKC02: GLRptNKC02,
  GLRptNKC03C: GLRptNKC03C,
  GLRptNKC03T: GLRptNKC03T,
  GLRptNKC04: GLRptNKC04,
  GLRptNKC05: GLRptNKC05,
  GLRptNKC06: GLRptNKC06,
  GLRptNKC07: GLRptNKC07,
  GLRptNKC01All01: GLRptNKC01All01,
  GLRptBCTC011: GLRptBCTC011,
  GLRptBCTC012: GLRptBCTC012,
  GLRptBCTC021: GLRptBCTC021,
  GLRptBCTC022: GLRptBCTC022,
  GLRptBCTC023: GLRptBCTC023,
};
