import React from "react";
import { Table } from "semantic-ui-react";
import { ZenFieldSelectApi, ZenFieldSelect, ZenField } from "../../../components/Control";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { RptHeader, RptTable, RptTableCell, RptTableRow } from "../../../components/Control/zReport";
import * as routes from "../../../constants/routes";

const FilterForm = ({ formik }) => {
    const Options_GroupType = [
        { key: "1", value: "MA_KH", text: "Mã khách hàng" },
        { key: "2", value: "TK", text: "Tài khoản" },
        { key: "3", value: "TK_DU", text: "Tài khoản đối ứng" },
        { key: "4", value: "MA_BP", text: "Bộ phận" },
        { key: "5", value: "MA_HD", text: "Hợp đồng" },
        { key: "6", value: "MA_PHI", text: "Phí" },
        { key: "7", value: "MA_SPCT", text: "Sản phẩm" },
    ]
    const Options_No_co = [
        { key: "0", value: "0", text: "Tất cả" },
        { key: "1", value: "1", text: "Nợ" },
        { key: "2", value: "2", text: "Có" },
    ]
    const handleItemSelected = (item, { name }) => {
        if (name === "tk") {
            formik.setFieldValue("ten_tk", item.ten_tk);
        } else if (name === "tk_du") {
            formik.setFieldValue("ten_tk_du", item.ten_tk);
        } else if (name === "ma_kh") {
            formik.setFieldValue("ten_kh", item.ten_kh);
        } else if (name === "ma_nhkh") {
            formik.setFieldValue("ten_nhkh", item.ten_nhkh);
        } else if (name === "ma_bp") {
            formik.setFieldValue("ten_bp", item.ten_bp);
        } else if (name === "ma_phi") {
            formik.setFieldValue("ten_phi", item.ten_phi);
        } else if (name === "ma_hd") {
            formik.setFieldValue("ten_hd", item.ten_hd);
        }
    };
    return <>
        <ZenFieldSelect options={Options_GroupType}
            label={"rpt.GroupType"} defaultlabel="Nhóm theo"
            name="grouptype" props={formik}
        />
        <ZenField
            label={"rpt.so_ct1"} defaultlabel="Chứng từ số"
            name="so_ct1" props={formik}
        />
        <ZenField
            label={"rpt.so_ct2"} defaultlabel="Đến số"
            name="so_ct2" props={formik}
        />
        <ZenFieldSelectApi
            lookup={ZenLookup.TK}
            label={"rpt.Tk"} defaultlabel="Tài khoản"
            name="tk" formik={formik}
            onItemSelected={handleItemSelected}
        />
        <ZenFieldSelect options={Options_No_co}
            label={"rpt.No_co"} defaultlabel="Nợ / Có"
            name="no_co" props={formik}
        />
        <ZenFieldSelectApi
            lookup={ZenLookup.TK}
            label={"rpt.tk_du"} defaultlabel="Tài khoản Đ/Ư"
            name="tk_du" formik={formik}
            onItemSelected={handleItemSelected}
        />
        <ZenFieldSelectApi loadApi
            lookup={ZenLookup.Ma_kh}
            label={"rpt.Ma_kh"} defaultlabel="Mã khách hàng"
            name="ma_kh" formik={formik}
            onItemSelected={handleItemSelected}
        />
        <ZenFieldSelectApi
            lookup={ZenLookup.Ma_nhkh}
            label={"rpt.Ma_Nhkh"} defaultlabel="Mã nhóm khách hàng"
            name="ma_nhkh" formik={formik}
            onItemSelected={handleItemSelected}
        />
        <ZenFieldSelectApi
            lookup={ZenLookup.Ma_bp}
            label={"rpt.Ma_bp"} defaultlabel="Mã bộ phận"
            name="ma_bp" formik={formik}
            onItemSelected={handleItemSelected}
        />
        <ZenFieldSelectApi
            lookup={ZenLookup.Ma_phi}
            label={"rpt.Ma_phi"} defaultlabel="Mã phí"
            name="ma_phi" formik={formik}
            onItemSelected={handleItemSelected}
        />
        <ZenFieldSelectApi
            loadApi
            lookup={ZenLookup.Ma_hd}
            label={"rpt.Ma_hd"} defaultlabel="Mã hợp đồng"
            name="ma_hd" formik={formik}
            onItemSelected={handleItemSelected}
        />
        <ZenField
            label={"rpt.ma_ct"} defaultlabel="Mã CT"
            name="ma_ct" props={formik}
        />
        <ZenFieldSelectApi
            lookup={ZenLookup.Ma_nt}
            label={"rpt.ma_nt"} defaultlabel="Ngoại tệ"
            name="ma_nt" formik={formik}
        />
    </>
}

const TableForm = ({ data = [], filter = {} }) => {
    return <>
        <RptTable maNt={filter.Ma_nt}>
            <Table.Header fullWidth>
                <RptHeader maNt={filter.Ma_nt}
                    header={[
                        { text: ["rpt.ngay_ct", "Ngày CT"] },
                        { text: ["rpt.so_ct", "Số CT"] },
                        { text: ["rpt.nguoi_gd", "Người giao dịch"] },
                        { text: ["rpt.dien_giai", "Diễn giải"] },
                        { text: ["rpt.tk", "TK"] },
                        { text: ["rpt.tk_du", "TK Đ/Ư"] },

                        { text: ["rpt.ps_no_nt", "PS nợ"], isNT: true },
                        { text: ["rpt.ps_co_nt", "PS có"], isNT: true },
                        { text: ["rpt.ma_nt", "Mã NT"], isNT: true, hiddenNT: true },
                        { text: ["rpt.ty_gia", "Tỷ giá"], isNT: true },

                        { text: ["rpt.ps_no", "PS nợ"] },
                        { text: ["rpt.ps_co", "PS có"] },
                        { text: ["rpt.ma_kh", "Mã KH"] },
                        { text: ["rpt.ten_kh", "Tên KH"] },
                        { text: ["rpt.ma_bp", "Mã BP"] },
                        { text: ["rpt.ma_nvkd", "Mã NVKD"] },
                        { text: ["rpt.ma_hd", "Mã HĐ"] },
                        { text: ["rpt.ma_phi", "Mã phí"] },
                        { text: ["rpt.ma_spct", "Mã SPCT"] },
                        { text: ["rpt.ma_ku", "Mã K/Ư"] },
                        { text: ["rpt.ma_ct", "Mã CT"] },
                    ]}
                />
            </Table.Header>
            <Table.Body>
                {data.length > 0 ? data.map((item, index) => {
                    const isNt = filter.Ma_nt && filter.Ma_nt !== 'VND' ? true : false
                    return <RptTableRow key={index} isBold={item.bold} index={index} item={item}>
                        <RptTableCell value={item.ngay_ct} type="date" item={item} />
                        <RptTableCell value={item.so_ct} item={item} />
                        <RptTableCell value={item.nguoi_gd} />
                        <RptTableCell value={item.dien_giai} />
                        <RptTableCell value={item.tk} />
                        <RptTableCell value={item.tk_du} />

                        {isNt && <RptTableCell value={item.ps_no_nt} type="number" />}
                        {isNt && <RptTableCell value={item.ps_co_nt} type="number" />}
                        {isNt && <RptTableCell value={item.ma_nt} />}
                        {isNt && <RptTableCell value={item.ty_gia} type="number" />}

                        <RptTableCell value={item.ps_no} type="number" />
                        <RptTableCell value={item.ps_co} type="number" />
                        <RptTableCell value={item.ma_kh} />
                        <RptTableCell value={item.ten_kh} />
                        <RptTableCell value={item.ma_bp} />
                        <RptTableCell value={item.ma_nvkd} />
                        <RptTableCell value={item.ma_hd} />
                        <RptTableCell value={item.ma_phi} />
                        <RptTableCell value={item.ma_spct} />
                        <RptTableCell value={item.ma_ku} />
                        <RptTableCell value={item.ma_ct} />
                    </RptTableRow>
                }) : undefined}
            </Table.Body>
        </RptTable>
    </>
}

export const GLRptBK01 = {
    FilterForm: FilterForm,
    TableForm: TableForm,
    permission: "02.40.9",
    visible: true,    // hiện/ẩn báo cáo
    route: routes.GLRptBK01,

    period: {
        fromDate: "ngay_ct1",
        toDate: "ngay_ct2"
    },

    linkHeader: {
        id: "GLRptBK01",
        defaultMessage: "Bảng kê chứng từ",
        active: true
    },

    info: {
        code: "02.21.29.1",
    },
    initFilter: {
        ngay_ct1: "",
        ngay_ct2: "",
        tk: "",
        ma_nt: "",
    },
    columns: [
        { id: "glbk01.ngay_ct", defaultMessage: "Ngày ghi sổ", fieldName: "ngay_ct", type: "date", sorter: true },
        { id: "glbk01.so_ct", defaultMessage: "Số ct", fieldName: "so_ct", type: "string", sorter: true },
        { id: "glbk01.ngay_lct", defaultMessage: "Người giao dịch", fieldName: "nguoi_gd", type: "string", sorter: true },
        { id: "glbk01.dien_giai", defaultMessage: "Diễn giải", fieldName: "dien_giai", type: "string", sorter: true },
        { id: "glbk01.dien_giai", defaultMessage: "TK", fieldName: "tk", type: "string", sorter: true },
        { id: "glbk01.dien_giai", defaultMessage: "TK Đ/Ứ", fieldName: "tk_du", type: "string", sorter: true },

        { id: "glbk01.ps_no", defaultMessage: "PS nợ", fieldName: "ps_no_nt", type: "number", sorter: true, isNT: true },
        { id: "glbk01.ps_co", defaultMessage: "PS có", fieldName: "ps_co_nt", type: "number", sorter: true, isNT: true },

        { id: "glbk01.ma_nt", defaultMessage: "Mã NT", fieldName: "ma_nt", type: "string", sorter: true, isNT: true, hiddenNT: true },
        { id: "glbk01.ty_gia", defaultMessage: "Tỷ giá", fieldName: "ty_gia", type: "number", sorter: true, isNT: true },

        { id: "glbk01.ps_no", defaultMessage: "PS nợ", fieldName: "ps_no", type: "number", sorter: true, },
        { id: "glbk01.ps_co", defaultMessage: "PS có", fieldName: "ps_co", type: "number", sorter: true, },

        { id: "glbk01.ma_kh", defaultMessage: "Mã KH", fieldName: "ma_kh", type: "string", sorter: true },
        { id: "glbk01.ma_kh", defaultMessage: "Tên KH", fieldName: "ten_kh", type: "string", sorter: true },
        { id: "glbk01.ma_hd", defaultMessage: "Mã HĐ", fieldName: "ma_hd", type: "string", sorter: true },
        { id: "glbk01.ma_bp", defaultMessage: "Mã BP", fieldName: "ma_bp", type: "string", sorter: true },
        { id: "glbk01.ma_ku", defaultMessage: "Mã K/Ứ", fieldName: "ma_ku", type: "string", sorter: true },
        { id: "glbk01.ma_spct", defaultMessage: "Mã SPCT", fieldName: "ma_spct", type: "string", sorter: true },
        { id: "glbk01.ma_ct", defaultMessage: "Mã CT", fieldName: "ma_ct", type: "string", sorter: true },

    ],
    formValidation: []
}