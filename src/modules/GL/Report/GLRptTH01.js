import React from "react";
import { Table } from "semantic-ui-react";
import { ZenFieldSelectApi, ZenFieldSelect, ZenField } from "../../../components/Control";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { RptHeader, RptTable, RptTableCell, RptTableRow } from "../../../components/Control/zReport";
import * as routes from "../../../constants/routes";

const FilterForm = ({ formik }) => {
    const Options_Group = [
        { key: "1", value: "MA_KH", text: "Mã khách hàng" },
        { key: "2", value: "TK", text: "Tài khoản" },
        { key: "3", value: "TK_DU", text: "Tài khoản đối ứng" },
        { key: "4", value: "MA_BP", text: "Bộ phận" },
        { key: "5", value: "MA_HD", text: "Hợp đồng" },
        { key: "6", value: "MA_PHI", text: "Phí" },
        { key: "7", value: "MA_SPCT", text: "Sản phẩm" },
    ]
    const Options_No_co = [
        { key: "0", value: "0", text: "Tất cả" },
        { key: "1", value: "1", text: "Nợ" },
        { key: "2", value: "2", text: "Có" },
    ]
    const handleItemSelected = (item, { name }) => {
        console.log(item)
        if (name === "tk") {
            formik.setFieldValue("ten_tk", item.ten_tk);
        } else if (name === "tk_du") {
            formik.setFieldValue("ten_tk_du", item.ten_tk);
        } else if (name === "ma_kh") {
            formik.setFieldValue("ten_kh", item.ten_kh);
        } else if (name === "nhom_kh") {
            formik.setFieldValue("ten_nhkh", item.ten_nhkh);
        } else if (name === "ma_plkh1") {
            formik.setFieldValue("ten_plkh1", item.ten_plkh);
        } else if (name === "ma_plkh2") {
            formik.setFieldValue("ten_plkh2", item.ten_plkh);
        } else if (name === "ma_plkh3") {
            formik.setFieldValue("ten_plkh3", item.ten_plkh);
        } else if (name === "ma_phi") {
            formik.setFieldValue("ten_phi", item.ten_phi);
        } else if (name === "ma_hd") {
            formik.setFieldValue("ten_hd", item.ten_hd);
        } else if (name === "ma_nhhd") {
            formik.setFieldValue("ten_nhhd", item.ten_nhhd);
        } else if (name === "ma_bp") {
            formik.setFieldValue("ten_bp", item.ten_bp);
        }
        console.log(formik.values)
    };
    return <>
        <ZenFieldSelect options={Options_Group}
            label={"rpt.Group"} defaultlabel="Nhóm theo"
            name="group" props={formik}
        />
        <ZenField
            label={"rpt.so_ct1"} defaultlabel="Chứng từ số"
            name="so_ct1" props={formik}
        />
        <ZenField
            label={"rpt.so_ct2"} defaultlabel="Đến số"
            name="so_ct2" props={formik}
        />
        <ZenFieldSelectApi
            lookup={ZenLookup.TK}
            label={"rpt.tk"} defaultlabel="Tài khoản"
            name="tk" formik={formik}
            onItemSelected={handleItemSelected}
        />
        <ZenFieldSelect options={Options_No_co}
            label={"rpt.No_co"} defaultlabel="Nợ / Có"
            name="no_co" props={formik}
        />
        <ZenFieldSelectApi
            lookup={ZenLookup.TK}
            label={"rpt.tk_du"} defaultlabel="Tài khoản Đ/Ư"
            name="tk_du" formik={formik}
            onItemSelected={handleItemSelected}
        />
        <ZenFieldSelectApi
            loadApi
            lookup={ZenLookup.Ma_kh}
            label={"rpt.Ma_kh"} defaultlabel="Mã khách hàng"
            name="ma_kh" formik={formik}
            onItemSelected={handleItemSelected}
        />
        <ZenFieldSelectApi
            lookup={ZenLookup.Ma_nhkh}
            label={"rpt.nhom_kh"} defaultlabel="Nhóm khách hàng"
            name="nhom_kh" formik={formik}
            onItemSelected={handleItemSelected}
        />
        <ZenFieldSelectApi
            lookup={{
                ...ZenLookup.Ma_plkh,
                onLocalWhere: (items) => {
                    return items?.filter(t => t.loai == "1") || []
                }
            }}
            label={"rpt.Ma_plkh1"} defaultlabel="Phân loại KH1"
            name="ma_plkh1" formik={formik}
            onItemSelected={handleItemSelected}
        />
        <ZenFieldSelectApi
            lookup={{
                ...ZenLookup.Ma_plkh,
                onLocalWhere: (items) => {
                    return items?.filter(t => t.loai == "2") || []
                }
            }}
            label={"rpt.Ma_plkh2"} defaultlabel="Phân loại KH2"
            name="ma_plkh2" formik={formik}
            onItemSelected={handleItemSelected}
        />
        <ZenFieldSelectApi
            lookup={{
                ...ZenLookup.Ma_plkh,
                onLocalWhere: (items) => {
                    return items?.filter(t => t.loai == "3") || []
                }
            }}
            label={"rpt.Ma_plkh3"} defaultlabel="Phân loại KH3"
            name="ma_plkh3" formik={formik}
            onItemSelected={handleItemSelected}
        />
        <ZenFieldSelectApi
            loadApi
            lookup={ZenLookup.Ma_hd}
            label={"rpt.Ma_hd"} defaultlabel="Mã hợp đồng"
            name="ma_hd" formik={formik}
            onItemSelected={handleItemSelected}
        />
        <ZenFieldSelectApi
            loadApi
            lookup={ZenLookup.Ma_nhhd}
            label={"rpt.Ma_nhhd"} defaultlabel="Mã nhóm hợp đồng"
            name="ma_nhhd" formik={formik}
            onItemSelected={handleItemSelected}
        />
        <ZenFieldSelectApi
            lookup={ZenLookup.Ma_bp}
            label={"rpt.Ma_bp"} defaultlabel="Mã bộ phận"
            name="ma_bp" formik={formik}
            onItemSelected={handleItemSelected}
        />
        <ZenFieldSelectApi
            lookup={ZenLookup.Ma_phi}
            label={"rpt.Ma_phi"} defaultlabel="Mã phí"
            name="ma_phi" formik={formik}
            onItemSelected={handleItemSelected}
        />
        <ZenField
            label={"rpt.ma_ct"} defaultlabel="Mã CT"
            name="ma_ct" props={formik}
        />
        <ZenFieldSelectApi
            lookup={ZenLookup.Ma_nt}
            label={"rpt.ma_nt"} defaultlabel="Ngoại tệ"
            name="ma_nt" formik={formik}
        />
    </>
}

const TableForm = ({ data = [], filter = {} }) => {
    return <>
        <RptTable maNt={filter.Ma_nt}>
            <Table.Header fullWidth>
                <RptHeader maNt={filter.Ma_nt}
                    header={[
                        { text: ["rpt.ma_group", "Mã nhóm"] },
                        { text: ["rpt.ten_group", "Tên nhóm"] },

                        { text: ["rpt.ps_no_nt", "PS nợ"], isNT: true },
                        { text: ["rpt.ps_co_nt", "PS có"], isNT: true },
                        { text: ["rpt.ma_nt", "Mã NT"], isNT: true, hiddenNT: true },

                        { text: ["rpt.ps_no", "PS nợ"] },
                        { text: ["rpt.ps_co", "PS có"] },
                    ]}
                />
            </Table.Header>
            <Table.Body>
                {data.length > 0 ? data.map((item, index) => {
                    const isNt = filter.Ma_nt && filter.Ma_nt !== 'VND' ? true : false
                    return <RptTableRow key={index} isBold={item.bold} index={index} item={item}>
                        <RptTableCell value={item.ma_group} />
                        <RptTableCell value={item.ten_group} />

                        {isNt && <RptTableCell value={item.ps_no_nt} type="number" />}
                        {isNt && <RptTableCell value={item.ps_co_nt} type="number" />}
                        {isNt && <RptTableCell value={item.ma_nt} />}

                        <RptTableCell value={item.ps_no} type="number" />
                        <RptTableCell value={item.ps_co} type="number" />
                    </RptTableRow>
                }) : undefined}
            </Table.Body>
        </RptTable>
    </>
}

export const GLRptTH01 = {
    FilterForm: FilterForm,
    TableForm: TableForm,
    permission: "02.40.a",
    visible: true,    // hiện/ẩn báo cáo
    route: routes.GLRptTH01,

    period: {
        fromDate: "ngay_ct1",
        toDate: "ngay_ct2"
    },

    linkHeader: {
        id: "GLRptTH01",
        defaultMessage: "Tổng hợp phát sinh nhóm theo chỉ tiêu",
        active: true
    },

    info: {
        code: "02.21.35.1",
    },
    initFilter: {
        ngay_ct1: "",
        ngay_ct2: "",
        group: "ma_kh",
        tk: "",
        ma_nt: "",
    },
    columns: [
        { id: "glth01.ma_group", defaultMessage: "Mã nhóm", fieldName: "ma_group", type: "string", sorter: true },
        { id: "glth01.ten_group", defaultMessage: "Tên nhóm", fieldName: "ten_group", type: "string", sorter: true },

        { id: "glth01.ps_no", defaultMessage: "PS nợ", fieldName: "ps_no_nt", type: "number", sorter: true, isNT: true },
        { id: "glth01.ps_co", defaultMessage: "PS có", fieldName: "ps_co_nt", type: "number", sorter: true, isNT: true },

        { id: "glth01.ma_nt", defaultMessage: "Mã NT", fieldName: "ma_nt", type: "string", sorter: true, isNT: true, hiddenNT: true },

        { id: "glth01.ps_no", defaultMessage: "PS nợ", fieldName: "ps_no", type: "number", sorter: true },
        { id: "glth01.ps_co", defaultMessage: "PS có", fieldName: "ps_co", type: "number", sorter: true },
    ],
    formValidation: []
}