import React from "react";
import { Table, Form } from "semantic-ui-react";
import { ZenFieldSelectApi, ZenDatePeriod, ZenField } from "../../../components/Control";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { RptHeader, RptTable, RptTableCell, RptTableRow } from "../../../components/Control/zReport";
import * as routes from "../../../constants/routes";

const FilterForm = ({ formik }) => {
    return <>
        <ZenField
            label={"rpt.Ds_Tk_Dt_Hh"} defaultlabel="TK hàng hóa"
            name="ds_tk_hh" props={formik}
        />
        <ZenField
            label={"rpt.Ds_Tk_Dt_Tp"} defaultlabel="TK NVL"
            name="ds_tk_nvl" props={formik}
        />
        <ZenField
            label={"rpt.Ds_Tk_Dt_Dv"} defaultlabel="TK kho khác"
            name="ds_tk_khac" props={formik}
        />
        <ZenField
            label={"rpt.Ds_Tk_Pt"} defaultlabel="TK phải tra"
            name="ds_tk_pt" props={formik}
        />
        <ZenFieldSelectApi
            lookup={ZenLookup.Ma_nt}
            label={"rpt.Ma_nt"} defaultlabel="Ngoại tệ"
            name="ma_nt" formik={formik}
        />
    </>
}

const TableForm = ({ data = [], filter = {} }) => {
    return <>
        <RptTable maNt={filter.Ma_nt}>
            <Table.Header fullWidth>
                <RptHeader maNt={filter.Ma_nt}
                    header={[
                        { text: ["rpt.ngay_ct", "Ngày ghi sổ"] },
                        { text: ["rpt.so_ct", "Số CT"] },
                        { text: ["rpt.ngay_lct", "Ngày CT"] },
                        { text: ["rpt.dien_giai", "Diễn giải"] },
                        { text: ["rpt.tk_no", "TK nợ(HH & NVL)"] },
                        { text: ["rpt.tk_khac", "TK nợ khác"] },
                        { text: ["rpt.tk_co", "TK có"] },

                        { text: ["rpt.tien_nt_hh", "Tiền HH"], isNT: true },
                        { text: ["rpt.tien_nt_nvl", "Tiền nvl"], isNT: true },
                        { text: ["rpt.tien_nt_khac", "Tiền khác"], isNT: true },
                        { text: ["rpt.tien_nt", "Phải thu"], isNT: true },
                        { text: ["rpt.ma_nt", "Mã NT"], isNT: true, hiddenNT: true },
                        { text: ["rpt.ty_gia", "Tỷ giá"], isNT: true },

                        { text: ["rpt.tien_hh", "Tiền HH"] },
                        { text: ["rpt.tien_nvl", "Tiền NVL"] },
                        { text: ["rpt.tien_khac", "Tiền khác"] },
                        { text: ["rpt.tien", "Phải trả"] },
                        { text: ["rpt.ma_kh", "Mã NCC"] },
                        { text: ["rpt.ten_kh", "Tên NCC"] },
                        { text: ["rpt.ma_hd", "Hợp đồng"] },
                        { text: ["rpt.ma_bp", "Mã BP"] },
                        { text: ["rpt.ma_phi", "Mã phí"] },
                        { text: ["rpt.ma_spct", "Mã SPCT"] },
                        { text: ["rpt.ma_ku", "Mã khế ước"] },
                        { text: ["rpt.ma_ct", "Mã CT"] },
                    ]}
                />
            </Table.Header>
            <Table.Body>
                {data.length > 0 ? data.map((item, index) => {
                    const isNt = filter.Ma_nt && filter.Ma_nt !== 'VND' ? true : false
                    return <RptTableRow key={index} isBold={item.bold} index={index} item={item}>
                        <RptTableCell value={item.ngay_ct} type="date" />
                        <RptTableCell value={item.so_ct} />
                        <RptTableCell value={item.ngay_lct} type="date" />
                        <RptTableCell value={item.dien_giai} />
                        <RptTableCell value={item.tk_no} />
                        <RptTableCell value={item.tk_khac} />
                        <RptTableCell value={item.tk_co} />

                        {isNt && <RptTableCell value={item.tien_nt_hh} type="number" />}
                        {isNt && <RptTableCell value={item.tien_nt_nvl} type="number" />}
                        {isNt && <RptTableCell value={item.tien_nt_khac} type="number" />}
                        {isNt && <RptTableCell value={item.Tien_nt} type="number" />}
                        {isNt && <RptTableCell value={item.ma_nt} />}
                        {isNt && <RptTableCell value={item.ty_gia} type="number" />}

                        <RptTableCell value={item.tien_hh} type="number" />
                        <RptTableCell value={item.tien_nvl} type="number" />
                        <RptTableCell value={item.tien_khac} type="number" />
                        <RptTableCell value={item.Tien} type="number" />
                        <RptTableCell value={item.ma_kh} />
                        <RptTableCell value={item.ten_kh} />
                        <RptTableCell value={item.ma_hd} />
                        <RptTableCell value={item.ma_bp} />
                        <RptTableCell value={item.ma_phi} />
                        <RptTableCell value={item.ma_spct} />
                        <RptTableCell value={item.ma_ku} />
                        <RptTableCell value={item.ma_ct} />
                    </RptTableRow>
                }) : undefined}
            </Table.Body>
        </RptTable>
    </>
}

export const GLRptNKC05 = {
    FilterForm: FilterForm,
    TableForm: TableForm,
    permission: "02.40.6",
    visible: true,    // hiện/ẩn báo cáo
    route: routes.GLRptNKC05,

    period: {
        fromDate: "ngay_ct1",
        toDate: "ngay_ct2"
    },

    linkHeader: {
        id: "GLRptNKC05",
        defaultMessage: "Sổ nhật ký mua hàng",
        active: true
    },

    info: {
        code: "02.21.17.1",
    },
    initFilter: {
        ngay_ct1: "",
        ngay_ct2: "",
        ds_tk_hh: "156",
        ds_tk_nvl: "152",
        ds_tk_khac: "153,151",
        ds_tk_pt: "331,111,112",
        ma_nt: "",
    },
    columns: [
        { id: "glnkc05.ngay_ct", defaultMessage: "Ngày ghi sổ", fieldName: "ngay_ct", type: "date", sorter: true },
        { id: "glnkc05.so_ct", defaultMessage: "Số ct", fieldName: "so_ct", type: "string", sorter: true },
        { id: "glnkc05.ngay_lct", defaultMessage: "Ngày ct", fieldName: "ngay_lct", type: "date", sorter: true },
        { id: "glnkc05.dien_giai", defaultMessage: "Diễn giải", fieldName: "dien_giai", type: "string", sorter: true },
        { id: "glnkc05.tk_no", defaultMessage: "Tk nợ (HH & NVL)", fieldName: "tk_no", type: "string", sorter: true },
        { id: "glnkc05.tk_khac", defaultMessage: "Tk nợ khác", fieldName: "tk_khác", type: "string", sorter: true },
        { id: "glnkc05.tk_co", defaultMessage: "Tk có", fieldName: "tk_co", type: "string", sorter: true },

        { id: "glnkc05.tien_hh", defaultMessage: "Tiền HH", fieldName: "tien_nt_hh", type: "number", sorter: true, isNT: true },
        { id: "glnkc05.tien_nvl", defaultMessage: "Tiền NVL", fieldName: "tien_nt_nvl", type: "number", sorter: true, isNT: true },
        { id: "glnkc05.tien_khac", defaultMessage: "Tiền khác", fieldName: "tien_nt_khac", type: "number", sorter: true, isNT: true },
        { id: "glnkc05.tien", defaultMessage: "Phải thu", fieldName: "tien_nt", type: "number", sorter: true, isNT: true },

        { id: "glnkc05.ma_nt", defaultMessage: "Mã NT", fieldName: "ma_nt", type: "string", sorter: true, isNT: true, hiddenNT: true },
        { id: "glnkc05.ty_gia", defaultMessage: "Tỷ giá", fieldName: "ty_gia", type: "number", sorter: true, isNT: true },

        { id: "glnkc05.tien_hh", defaultMessage: "Tiền HH", fieldName: "tien_hh", type: "number", sorter: true, },
        { id: "glnkc05.tien_nvl", defaultMessage: "Tiền NVL", fieldName: "tien_nvl", type: "number", sorter: true, },
        { id: "glnkc05.tien_khac", defaultMessage: "Tiền khác", fieldName: "tien_khac", type: "number", sorter: true, },
        { id: "glnkc05.tien", defaultMessage: "Phải trả", fieldName: "tien", type: "number", sorter: true, },

        { id: "glnkc05.ma_kh", defaultMessage: "Mã NCC", fieldName: "ma_kh", type: "string", sorter: true },
        { id: "glnkc05.ten_kh", defaultMessage: "Tên NCC", fieldName: "ten_kh", type: "string", sorter: true },
        { id: "glnkc05.ma_hd", defaultMessage: "Mã hợp đồng", fieldName: "ma_hd", type: "string", sorter: true },
        { id: "glnkc05.ma_bp", defaultMessage: "Mã bộ phận", fieldName: "ma_bp", type: "string", sorter: true },
        { id: "glnkc05.ma_phi", defaultMessage: "Mã phí", fieldName: "ma_phi", type: "string", sorter: true },
        { id: "glnkc05.ma_spct", defaultMessage: "Mã SPCT", fieldName: "ma_spct", type: "string", sorter: true },
        { id: "glnkc05.ma_ct", defaultMessage: "Mã khế ước", fieldName: "ma_ku", type: "string", sorter: true, },
        { id: "glnkc05.ma_ct", defaultMessage: "Mã CT", fieldName: "ma_ct", type: "string", sorter: true, },
    ],
    formValidation: []
}
