import React from "react";
import { Table, Form } from "semantic-ui-react";
import {
  ZenFieldSelectApi,
  ZenDatePeriod,
  ZenField,
} from "../../../components/Control";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import {
  RptHeader,
  RptTable,
  RptTableCell,
  RptTableRow,
} from "../../../components/Control/zReport";
import * as routes from "../../../constants/routes";

const FilterForm = ({ formik }) => {
  const handleItemSelected = (item, { name }) => {
    if (name === "ma_bp") {
      formik.setFieldValue("ten_bp", item.ten_bp);
    }
  };
  return (
    <>
      <ZenField
        label={"rpt.Tk_List"}
        defaultlabel="DS tài khoản nợ"
        name="tk_list"
        props={formik}
      />

      <ZenField
        label={"rpt.Tkdu_List"}
        defaultlabel="DS tài khoản có"
        name="tkdu_list"
        props={formik}
      />

      <ZenFieldSelectApi
        lookup={ZenLookup.Ma_bp}
        label={"rpt.Ma_Bp"}
        defaultlabel="Mã bộ phận"
        name="ma_bp"
        formik={formik}
        onItemSelected={handleItemSelected}
      />

      <ZenFieldSelectApi
        lookup={ZenLookup.Ma_nt}
        label={"rpt.ma_nt"}
        defaultlabel="Ngoại tệ"
        name="ma_nt"
        formik={formik}
      />
    </>
  );
};

const TableForm = ({ data = [], filter = {} }) => {
  return (
    <>
      <RptTable maNt={filter.Ma_nt}>
        <Table.Header fullWidth>
          <RptHeader
            maNt={filter.Ma_nt}
            header={[
              { text: ["GLRptNKC03T.ngay_ct", "Ngày CT"] },
              { text: ["rpt.so_ct", "Số CT"] },
              { text: ["rpt.dien_giai", "Diễn giải"] },
              { text: ["GLRptNKC03T.tk_no", "TK nợ"] },
              { text: ["GLRptNKC03T.tk_co", "TK có"] },
              { text: ["GLRptNKC03T.tien_nt", "Tiền NT"] },
              { text: ["GLRptNKC03T.tien", "Phát sinh"] },
              { text: ["rpt.ma_ct", "Mã CT"] },
              { text: ["GLRptNKC03T.ma_nt", "Mã NT"] },
            ]}
          />
        </Table.Header>
        <Table.Body>
          {data.length > 0
            ? data.map((item, index) => {
              const isNt =
                filter.Ma_nt && filter.Ma_nt !== "VND" ? true : false;
              return (
                <RptTableRow
                  key={index}
                  isBold={item.bold}
                  index={index}
                  item={item}
                >
                  <RptTableCell value={item.ngay_ct} type="date" />
                  <RptTableCell value={item.so_ct} />
                  <RptTableCell value={item.dien_giai} />
                  <RptTableCell value={item.tk_no} type="number" />
                  <RptTableCell value={item.tk_co} type="number" />
                  <RptTableCell value={item.Tien_nt} />
                  <RptTableCell value={item.Tien} type="number" />
                  <RptTableCell value={item.ma_ct} />
                  <RptTableCell value={item.ma_nt} />
                </RptTableRow>
              );
            })
            : undefined}
        </Table.Body>
      </RptTable>
    </>
  );
};

export const GLRptNKC03T = {
  FilterForm: FilterForm,
  TableForm: TableForm,
  permission: "02.40.3",
  visible: true, // hiện/ẩn báo cáo
  route: routes.GLRptNKC03T,

  period: {
    fromDate: "ngay_ct1",
    toDate: "ngay_ct2",
  },

  linkHeader: {
    id: "GLRptNKC03T",
    defaultMessage: "Sổ nhật ký thu tiền",
    active: true,
  },

  info: {
    code: "04.20.11",
  },
  initFilter: {
    ngay_ct1: "",
    ngay_ct2: "",
    tk_list: "111,112",
    tkdu_list: "",
    ma_bp: "",
    ma_nt: "",
  },
  columns: [
    {
      id: "glnkc03.ngay_ct",
      defaultMessage: "Ngày ghi sổ",
      fieldName: "ngay_ct",
      type: "date",
      sorter: true,
    },
    {
      id: "glnkc03.so_ct",
      defaultMessage: "Số ct",
      fieldName: "so_ct",
      type: "string",
      sorter: true,
    },
    {
      id: "glnkc03.dien_giai",
      defaultMessage: "Diễn giải",
      fieldName: "dien_giai",
      type: "string",
      sorter: true,
    },
    {
      id: "glnkc03.tk_no",
      defaultMessage: "Tk nợ",
      fieldName: "tk_no",
      type: "string",
      sorter: true,
    },
    {
      id: "glnkc03.tk_co",
      defaultMessage: "Tk có",
      fieldName: "tk_co",
      type: "string",
      sorter: true,
    },
    {
      id: "glnkc03t.ps_no",
      defaultMessage: "Thu",
      fieldName: "ps_no",
      type: "number",
      sorter: true,
      isNT: true,
    },
    {
      id: "glnkc03t.ps_co",
      defaultMessage: "Chi",
      fieldName: "ps_co",
      type: "number",
      sorter: true,
      isNT: true,
    },
    {
      id: "glnkc03t.so_du",
      defaultMessage: "Tồn",
      fieldName: "so_du",
      type: "number",
      sorter: true,
      isNT: true,
    },
    {
      id: "glnkc03.tien_nt",
      defaultMessage: "Phát sinh NT",
      fieldName: "tien_nt",
      type: "number",
      sorter: true,
      isNT: true,
    },
    {
      id: "glnkc03.tien",
      defaultMessage: "Phát sinh",
      fieldName: "tien",
      type: "number",
      sorter: true,
    },
    {
      id: "glnkc03.ma_nt",
      defaultMessage: "Mã NT",
      fieldName: "ma_nt",
      type: "string",
      sorter: true,
      isNT: true,
      hiddenNT: true,
    },
    {
      id: "glnkc03.ma_ct",
      defaultMessage: "Mã CT",
      fieldName: "ma_ct",
      type: "string",
      sorter: true,
    },
  ],
  formValidation: [],
};
