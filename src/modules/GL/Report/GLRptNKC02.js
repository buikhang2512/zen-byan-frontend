import React from "react";
import { Table } from "semantic-ui-react";
import { ZenFieldSelectApi } from "../../../components/Control";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { RptHeader, RptTable, RptTableCell, RptTableRow } from "../../../components/Control/zReport";
import * as routes from "../../../constants/routes";

const FilterForm = ({ formik }) => {
    return <>

        <ZenFieldSelectApi
            lookup={ZenLookup.Ma_nt}
            label={"rpt.ma_nt"} defaultlabel="Ngoại tệ"
            name="ma_nt" formik={formik}
        />
    </>
}

const TableForm = ({ data = [], filter = {} }) => {
    return <>
        <RptTable maNt={filter.Ma_nt}>
            <Table.Header fullWidth>
                <RptHeader maNt={filter.Ma_nt}
                    header={[
                        { text: ["rpt.stt_dong_nkc", "STT dòng"] },
                        { text: ["rpt.ngay_ct", "Ngày ghi sổ"] },
                        { text: ["rpt.so_ct", "Số CT"] },
                        { text: ["rpt.ngay_lct", "Ngày CT"] },
                        { text: ["rpt.dien_giai", "Diễn giải"] },
                        { text: ["rpt.tk", "TK"] },
                        { text: ["rpt.tk_du", "TK Đ/Ư"] },

                        { text: ["rpt.ps_no_nt", "PS nợ"], isNT: true },
                        { text: ["rpt.ps_co_nt", "PS có"], isNT: true },
                        { text: ["rpt.ma_nt", "Mã NT"], isNT: true, hiddenNT: true },
                        { text: ["rpt.ty_gia", "Tỷ giá"], isNT: true },

                        { text: ["rpt.ps_no", "PS nợ"] },
                        { text: ["rpt.ps_co", "PS có"] },
                        { text: ["rpt.ma_ct", "Mã CT"] },
                    ]}
                />
            </Table.Header>
            <Table.Body>
                {data.length > 0 ? data.map((item, index) => {
                    const isNt = filter.Ma_nt && filter.Ma_nt !== 'VND' ? true : false
                    return <RptTableRow key={index} isBold={item.bold} index={index} item={item}>
                        <RptTableCell value={item.Stt_dong_nkc} />
                        <RptTableCell value={item.ngay_ct} type="date" />
                        <RptTableCell value={item.so_ct} />
                        <RptTableCell value={item.ngay_lct} type="date" />
                        <RptTableCell value={item.dien_giai} />
                        <RptTableCell value={item.tk} />
                        <RptTableCell value={item.tk_du} />

                        {isNt && <RptTableCell value={item.ps_no_nt} type="number" />}
                        {isNt && <RptTableCell value={item.ps_co_nt} type="number" />}
                        {isNt && <RptTableCell value={item.ma_nt} />}
                        {isNt && <RptTableCell value={item.ty_gia} type="number" />}

                        <RptTableCell value={item.ps_no} type="number" />
                        <RptTableCell value={item.ps_co} type="number" />
                        <RptTableCell value={item.ma_ct} />
                    </RptTableRow>
                }) : undefined}
            </Table.Body>
        </RptTable>
    </>
}

export const GLRptNKC02 = {
    FilterForm: FilterForm,
    TableForm: TableForm,
    permission: "02.40.2",
    visible: true,    // hiện/ẩn báo cáo
    route: routes.GLRptNKC02,

    period: {
        fromDate: "ngay_ct1",
        toDate: "ngay_ct2"
    },

    linkHeader: {
        id: "GLRptNKC02",
        defaultMessage: "Sổ nhật ký chung",
        active: true
    },

    info: {
        code: "02.21.05.1",
    },
    initFilter: {
        ngay_ct1: "",
        ngay_ct2: "",
        ma_nt: "",
    },
    columns: [
        { id: "glnkc02.ngay_ct", defaultMessage: "Ngày ghi sổ", fieldName: "ngay_ct", type: "date", sorter: true },
        { id: "glnkc02.so_ct", defaultMessage: "Số ct", fieldName: "so_ct", type: "string", sorter: true },
        { id: "glnkc02.ngay_lct", defaultMessage: "Ngày ct", fieldName: "ngay_lct", type: "date", sorter: true },
        { id: "glnkc02.dien_giai", defaultMessage: "Diễn giải", fieldName: "dien_giai", type: "string", sorter: true },
        { id: "glnkc02.tk", defaultMessage: "Tk", fieldName: "tk", type: "string", sorter: true },
        { id: "glnkc02.tk_du", defaultMessage: "Tk Đ/Ứ", fieldName: "tk_du", type: "string", sorter: true },
        { id: "glnkc02.ps_no", defaultMessage: "Ps nợ", fieldName: "ps_no_nt", type: "number", sorter: true, isNT: true },
        { id: "glnkc02.ps_co", defaultMessage: "Ps có", fieldName: "ps_co_nt", type: "number", sorter: true, isNT: true },
        { id: "glnkc02.ma_nt", defaultMessage: "Mã NT", fieldName: "ma_nt", type: "string", sorter: true, isNT: true, hiddenNT: true },
        { id: "glnkc02.ty_gia", defaultMessage: "Tỷ giá", fieldName: "ty_gia", type: "number", sorter: true, isNT: true },
        { id: "glnkc02.ps_no", defaultMessage: "Ps nợ", fieldName: "ps_no", type: "number", sorter: true, },
        { id: "glnkc02.ps_co", defaultMessage: "Ps có", fieldName: "ps_co", type: "number", sorter: true, },
        { id: "glnkc02.ma_ct", defaultMessage: "Mã CT", fieldName: "ma_ct", type: "string", sorter: true, },
    ],
    formValidation: []
}