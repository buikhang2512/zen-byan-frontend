import React from "react";
import { Table, Form } from "semantic-ui-react";
import { ZenFieldSelectApi, ZenDatePeriod, ZenFieldNumber, ZenFieldCheckbox } from "../../../components/Control";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { RptHeader, RptTable, RptTableCell, RptTableRow } from "../../../components/Control/zReport";
import * as routes from "../../../constants/routes";

const FilterForm = ({ formik }) => {
   const handleItemSelected = (item, { name }) => {
      if (name === "tk") {
         formik.setFieldValue("ten_tk", item.ten_tk);
      }
   };
   return <>
      <ZenFieldSelectApi
         lookup={ZenLookup.TK}
         label={"rpt.tk"} defaultlabel="Tài khoản"
         name="tk" formik={formik}
         onItemSelected={handleItemSelected}
      />
      <ZenFieldNumber
         label={"rpt.Bac_tk"} defaultlabel="Bậc tài khoản"
         name="bac_tk" props={formik}
         decimalScale={0}
      />
      <ZenFieldCheckbox
         label={"rpt.Tk_sc"} defaultlabel="Lên cho TK sổ cái"
         name="tk_sc" props={formik}
      />
      <ZenFieldSelectApi
         lookup={ZenLookup.Ma_nt}
         label={"rpt.Ma_Nt"} defaultlabel="Ngoại tệ"
         name="ma_nt" formik={formik}
      />
   </>
}

const TableForm = ({ data = [], filter = {} }) => {
   return <>
      <RptTable maNt={filter.Ma_Nt}>
         <Table.Header fullWidth>
            <RptHeader maNt={filter.Ma_Nt}
               header={[
                  // { text: ["rpt.Ngay_ct", "Ngày ghi sổ"] },
                  // { text: ["rpt.So_ct", "Số CT"] },
                  // { text: ["rpt.Ngay_lct", "Ngày CT"] },
                  // { text: ["rpt.Dien_giai", "Diễn giải"] },
                  { text: ["rpt.TK", "Tài khoản"] },
                  { text: ["rpt.Ten_tk", "Tên tài khoản"] },
                  { text: ["rpt.no_dk", "Nợ ĐK"] },
                  { text: ["rpt.no_dk_nt", "PS có"], isNT: true },
                  { text: ["rpt.co_dk", "Có ĐK"] },
                  { text: ["rpt.co_dk_nt", "PS có"], isNT: true },
                  { text: ["rpt.ps_no", "PS nợ"] },
                  { text: ["rpt.ps_no_nt", "PS có"], isNT: true },
                  { text: ["rpt.ps_co", "PS có"] },
                  { text: ["rpt.Ps_co_nt", "PS có"], isNT: true },
                  { text: ["rpt.lk_no", "Lũy kế PS nợ"] },
                  { text: ["rpt.lk_no_nt", "Lũy kế PS nợ Nt"], isNT: true },
                  { text: ["rpt.lk_co", "Lũy kế PS có"] },
                  { text: ["rpt.lk_co_nt", "Lũy kế PS có Nt"], isNT: true },
                  { text: ["rpt.no_ck", "Nợ CK"] },
                  { text: ["rpt.no_ck_nt", "PS có"], isNT: true },
                  { text: ["rpt.co_ck", "Có CK"] },
                  { text: ["rpt.co_ck_nt", "PS có"], isNT: true },
               ]}
            />
         </Table.Header>
         <Table.Body>
            {data.length > 0 ? data.map((item, index) => {
               const isNt = filter.Ma_Nt && filter.Ma_Nt !== 'VND' ? true : false
               return <RptTableRow key={index} isBold={item.Bold} index={index} item={item}>
                  <RptTableCell value={item.Tk} />
                  <RptTableCell value={item.Ten_tk} />
                  <RptTableCell value={item.no_dk} type="number" />
                  {isNt && <RptTableCell value={item.no_dk_nt} type="number" />}
                  <RptTableCell value={item.co_dk} type="number" />
                  {isNt && <RptTableCell value={item.co_dk_nt} type="number" />}
                  <RptTableCell value={item.ps_no} type="number" />
                  {isNt && <RptTableCell value={item.ps_no_nt} type="number" />}
                  <RptTableCell value={item.ps_co} type="number" />
                  {isNt && <RptTableCell value={item.ps_co_nt} type="number" />}
                  <RptTableCell value={item.lk_no} type="number" />
                  {isNt && <RptTableCell value={item.lk_no_nt} type="number" />}
                  <RptTableCell value={item.lk_co} type="number" />
                  {isNt && <RptTableCell value={item.lk_co_nt} type="number" />}
                  <RptTableCell value={item.no_ck} type="number" />
                  {isNt && <RptTableCell value={item.no_ck_nt} type="number" />}
                  <RptTableCell value={item.co_ck} type="number" />
                  {isNt && <RptTableCell value={item.co_ck_nt} type="number" />}
               </RptTableRow>
            }) : undefined}
         </Table.Body>
      </RptTable>
   </>
}

export const GLRptBCTC012 = {
   FilterForm: FilterForm,
   TableForm: TableForm,
   permission: "02.50.2",
   visible: true,    // hiện/ẩn báo cáo
   route: routes.GLRptBCTC012,

   period: {
      fromDate: "ngay_ct1",
      toDate: "ngay_ct2"
   },

   linkHeader: {
      id: "GLRptBCTC012",
      defaultMessage: "Bảng cân đối phát sinh các tài khoản (mẫu lũy kế)",
      active: true
   },

   info: {
      code: "02.22.02.2",
   },
   initFilter: {
      ngay_ct1: "",
      ngay_ct2: "",
      ma_nt: "",
      bac_tk: 9
   },
   columns: [
      { id: "glbctc012.tk", defaultMessage: "TK", fieldName: "tk", type: "string", sorter: true },
      { id: "glbctc012.ten_tk", defaultMessage: "Tên TK", fieldName: "ten_tk", type: "string", sorter: true },

      { id: "glbctc012.no_dk", defaultMessage: "Nợ Đk", fieldName: "no_dk_nt", type: "number", sorter: true, isNT: true },
      { id: "glbctc012.co_dk", defaultMessage: "Có Đk", fieldName: "co_dk_nt", type: "number", sorter: true, isNT: true },
      { id: "glbctc012.ps_no", defaultMessage: "Ps nợ", fieldName: "ps_no_nt", type: "number", sorter: true, isNT: true },
      { id: "glbctc012.ps_co", defaultMessage: "Ps có", fieldName: "ps_co_nt", type: "number", sorter: true, isNT: true },
      { id: "glbctc012.lk_no", defaultMessage: "Lũy kế PS nợ", fieldName: "lk_no_nt", type: "number", sorter: true, isNT: true },
      { id: "glbctc012.lk_co", defaultMessage: "Lũy kế PS có", fieldName: "lk_co_nt", type: "number", sorter: true, isNT: true },
      { id: "glbctc012.no_ck", defaultMessage: "Nợ Ck", fieldName: "no_ck_nt", type: "number", sorter: true, isNT: true },
      { id: "glbctc012.co_ck", defaultMessage: "Có Ck", fieldName: "co_ck_nt", type: "number", sorter: true, isNT: true },

      { id: "glbctc012.no_dk", defaultMessage: "Nợ Đk", fieldName: "no_dk", type: "number", sorter: true, },
      { id: "glbctc012.co_dk", defaultMessage: "Có Đk", fieldName: "co_dk", type: "number", sorter: true, },
      { id: "glbctc012.ps_no", defaultMessage: "Ps nợ", fieldName: "ps_no", type: "number", sorter: true, },
      { id: "glbctc012.ps_co", defaultMessage: "Ps có", fieldName: "ps_co", type: "number", sorter: true, },
      { id: "glbctc012.lk_no", defaultMessage: "Lũy kế PS nợ", fieldName: "lk_no", type: "number", sorter: true, isNT: true },
      { id: "glbctc012.lk_co", defaultMessage: "Lũy kế PS có", fieldName: "lk_co", type: "number", sorter: true, isNT: true },
      { id: "glbctc012.no_ck", defaultMessage: "Nợ Ck", fieldName: "no_ck", type: "number", sorter: true, },
      { id: "glbctc012.co_ck", defaultMessage: "Có Ck", fieldName: "co_ck", type: "number", sorter: true, },

   ],
   formValidation: []
}
