import React from "react";
import { Table, Form } from "semantic-ui-react";
import { ZenFieldSelectApi, ZenDatePeriod, ZenField, ZenFieldCheckbox } from "../../../components/Control";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { RptHeader, RptTable, RptTableCell, RptTableRow } from "../../../components/Control/zReport";
import * as routes from "../../../constants/routes";

const FilterForm = ({ formik }) => {
   const handleItemSelected = (item, { name }) => {
      if (name === "tk") {
         formik.setFieldValue("ten_tk", item.ten_tk);
      }
   };
   return <>
      <ZenFieldSelectApi loadApi
         lookup={ZenLookup.TK}
         label={"rpt.tk"} defaultlabel="Tài khoản"
         name="tk" formik={formik}
         onItemSelected={handleItemSelected}
      />
      <ZenFieldCheckbox
         label={"rpt.Stt_dong_nkc"} defaultlabel="Đánh STT theo sổ NKC"
         name="stt_dong_nkc" props={formik}
      />
      <ZenFieldSelectApi
         lookup={ZenLookup.Ma_nt}
         label={"rpt.Ma_Nt"} defaultlabel="Ngoại tệ"
         name="ma_nt" formik={formik}
      />
   </>
}

const TableForm = ({ data = [], filter = {} }) => {
   return <>
      <RptTable maNt={filter.Ma_Nt}>
         <Table.Header fullWidth>
            <RptHeader maNt={filter.Ma_Nt}
               header={[
                  { text: ["rpt.Ngay_ct", "Ngày ghi sổ"] },
                  { text: ["rpt.So_ct", "Số CT"] },
                  { text: ["rpt.Ngay_lct", "Ngày CT"] },
                  { text: ["rpt.Dien_giai", "Diễn giải"] },
                  { text: ["rpt.TK", "TK"] },
                  { text: ["rpt.TK_du", "TK Đ/Ứ"] },

                  { text: ["rpt.Ps_no_nt", "PS nợ"], isNT: true },
                  { text: ["rpt.Ps_co_nt", "PS có"], isNT: true },
                  { text: ["rpt.Ma_nt", "Mã NT"], isNT: true, hiddenNT: true },
                  { text: ["rpt.Ty_gia", "Tỷ giá"], isNT: true },

                  { text: ["rpt.Ps_no", "PS nợ"] },
                  { text: ["rpt.Ps_co", "PS có"] },
                  { text: ["rpt.Ma_kh", "Mã KH"] },
                  { text: ["rpt.Ma_hd", "Mã HĐ"] },
                  { text: ["rpt.ma_bp", "Mã BP"] },
                  { text: ["rpt.ma_ku", "Mã K/Ứ"] },
                  { text: ["rpt.ma_spct", "Mã SPCT"] },
                  { text: ["rpt.ma_ct", "Mã CT"] },
                  { text: ["rpt.stt_dong_nkc ", "STT dòng NKC"] },
               ]}
            />
         </Table.Header>
         <Table.Body>
            {data.length > 0 ? data.map((item, index) => {
               const isNt = filter.Ma_Nt && filter.Ma_Nt !== 'VND' ? true : false
               return <RptTableRow key={index} isBold={item.bold} index={index} item={item}>
                  <RptTableCell value={item.Ngay_ct} type="date" />
                  <RptTableCell value={item.So_ct} />
                  <RptTableCell value={item.Ngay_lct} type="date" />
                  <RptTableCell value={item.Dien_giai} />
                  <RptTableCell value={item.Tk} />
                  <RptTableCell value={item.Tk_du} />

                  {isNt && <RptTableCell value={item.Ps_no_nt} type="number" />}
                  {isNt && <RptTableCell value={item.Ps_co_nt} type="number" />}
                  {isNt && <RptTableCell value={item.Ma_nt} />}
                  {isNt && <RptTableCell value={item.Ty_gia} type="number" />}

                  <RptTableCell value={item.Ps_no} type="number" />
                  <RptTableCell value={item.Ps_co} type="number" />
                  <RptTableCell value={item.Ma_kh} />
                  <RptTableCell value={item.Ma_hd} />
                  <RptTableCell value={item.ma_bp} />
                  <RptTableCell value={item.ma_ku} />
                  <RptTableCell value={item.ma_spct} />
                  <RptTableCell value={item.ma_ct} />
                  <RptTableCell value={item.stt_dong_nkc} />
               </RptTableRow>
            }) : undefined}
         </Table.Body>
      </RptTable>
   </>
}

export const GLRptNKC01All01 = {
   FilterForm: FilterForm,
   TableForm: TableForm,
   permission: "02.40.1",
   visible: true,    // hiện/ẩn báo cáo
   route: routes.GLRptNKC01All01,

   period: {
      fromDate: "ngay_ct1",
      toDate: "ngay_ct2"
   },

   linkHeader: {
      id: "GLRptNKC01All01",
      defaultMessage: "Sổ cái tài khoản",
      active: true
   },

   info: {
      code: "02.21.03.1",
      code_group: "02.21.03"
   },
   initFilter: {
      ngay_ct1: "",
      ngay_ct2: "",
      ma_nt: "",
   },
   columns: [
      { id: "glnkc01all.ngay_ct", defaultMessage: "Ngày ghi sổ", fieldName: "ngay_ct", type: "date", sorter: true },
      { id: "glnkc01all.so_ct", defaultMessage: "Số Ct", fieldName: "so_ct", type: "string", sorter: true },
      { id: "glnkc01all.ngay_lct", defaultMessage: "Ngày Ct", fieldName: "ngay_lct", type: "date", sorter: true },
      { id: "glnkc01all.dien_giai", defaultMessage: "Diễn giải", fieldName: "dien_giai", type: "string", sorter: true },
      { id: "glnkc01all.tk", defaultMessage: "TK", fieldName: "tk", type: "string", sorter: true },
      { id: "glnkc01all.tk_du", defaultMessage: "TK Đ/Ứ", fieldName: "tk_du", type: "string", sorter: true },
      { id: "glnkc01all.ps_no", defaultMessage: "Ps nợ", fieldName: "ps_no_nt", type: "number", sorter: true, isNT: true },
      { id: "glnkc01all.ps_co", defaultMessage: "Ps có", fieldName: "ps_co_nt", type: "number", sorter: true, isNT: true },
      { id: "glnkc01all.ma_nt", defaultMessage: "Mã Nt", fieldName: "ma_nt", type: "number", sorter: true, isNT: true, hiddenNT: true },
      { id: "glnkc01all.ty_gia", defaultMessage: "Tỷ giá", fieldName: "ty_gia", type: "number", sorter: true, isNT: true },
      { id: "glnkc01all.ps_no", defaultMessage: "Ps nợ", fieldName: "ps_no", type: "number", sorter: true },
      { id: "glnkc01all.ps_co", defaultMessage: "Ps có", fieldName: "ps_co", type: "number", sorter: true },
      { id: "glnkc01all.ma_kh", defaultMessage: "Mã KH", fieldName: "ma_kh", type: "string", sorter: true },
      { id: "glnkc01all.ma_hd", defaultMessage: "Mã HĐ", fieldName: "ma_hd", type: "string", sorter: true },
      { id: "glnkc01all.ma_bp", defaultMessage: "Mã BP", fieldName: "ma_bp", type: "string", sorter: true },
      { id: "glnkc01all.ma_ku", defaultMessage: "Mã K/Ứ", fieldName: "ma_ku", type: "string", sorter: true },
      { id: "glnkc01all.ma_spct", defaultMessage: "Mã SPCT", fieldName: "ma_spct", type: "string", sorter: true },
      { id: "glnkc01all.ma_ct", defaultMessage: "Mã CT", fieldName: "ma_ct", type: "string", sorter: true },
      { id: "glnkc01all.stt_dong_nkc", defaultMessage: "STT dòng NKC", fieldName: "stt_dong_nkc", type: "string", sorter: true },
   ],
   formValidation: []
}
