import React from "react";
import { Table } from "semantic-ui-react";
import { ZenFieldSelectApi } from "../../../components/Control";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { RptHeader, RptTable, RptTableCell, RptTableRow } from "../../../components/Control/zReport";
import * as routes from "../../../constants/routes";

const FilterForm = ({ formik }) => {
    const handleItemSelected = (item, { name }) => {
        if (name === "tk") {
            formik.setFieldValue("ten_tk", item.ten_tk);
        }
    };
    return <>
        <ZenFieldSelectApi required
            lookup={ZenLookup.TK}
            label={"rpt.tk"} defaultlabel="Tài khoản"
            name="tk" formik={formik}
            onItemSelected={handleItemSelected}
        />

        <ZenFieldSelectApi
            lookup={ZenLookup.Ma_nt}
            label={"rpt.ma_nt"} defaultlabel="Ngoại tệ"
            name="ma_nt" formik={formik}
        />
    </>
}

const TableForm = ({ data = [], filter = {} }) => {
    return <>
        <RptTable maNt={filter.Ma_nt}>
            <Table.Header fullWidth>
                <RptHeader maNt={filter.Ma_nt}
                    header={[
                        { text: ["rpt.ngay_ct", "Ngày ghi sổ"] },
                        { text: ["rpt.so_ct", "Số CT"] },
                        { text: ["rpt.ngay_lct", "Ngày CT"] },
                        { text: ["rpt.dien_giai", "Diễn giải"] },
                        { text: ["rpt.tk_du", "TK Đ / Ư"] },

                        { text: ["rpt.ps_no_nt", "PS nợ"], isNT: true },
                        { text: ["rpt.ps_co_nt", "PS có"], isNT: true },
                        { text: ["rpt.du_no_nt", "PS nợ"], isNT: true },
                        { text: ["rpt.du_co_nt", "PS có"], isNT: true },
                        { text: ["rpt.ma_nt", "Mã NT"], isNT: true, hiddenNT: true },
                        { text: ["rpt.ty_gia", "Tỷ giá"], isNT: true },

                        { text: ["rpt.ps_no", "PS nợ"] },
                        { text: ["rpt.ps_co", "PS có"] },
                        { text: ["rpt.du_no", "Số dư nợ"] },
                        { text: ["rpt.du_co", "Số dư có"] },
                        { text: ["rpt.ma_kh", "Mã KH"] },
                        { text: ["rpt.ten_kh", "Tên KH"] },
                        { text: ["rpt.ma_ct", "Mã CT"] },
                    ]}
                />
            </Table.Header>
            <Table.Body>
                {data.length > 0 ? data.map((item, index) => {
                    const isNt = filter.Ma_nt && filter.Ma_nt !== 'VND' ? true : false
                    return <RptTableRow key={index} isBold={item.bold} index={index} item={item}>
                        <RptTableCell value={item.ngay_ct} type="date" />
                        <RptTableCell value={item.so_ct} />
                        <RptTableCell value={item.ngay_lct} type="date" />
                        <RptTableCell value={item.dien_giai} />
                        <RptTableCell value={item.tk_du} />

                        {isNt && <RptTableCell value={item.ps_no_nt} type="number" />}
                        {isNt && <RptTableCell value={item.ps_co_nt} type="number" />}
                        {isNt && <RptTableCell value={item.du_no_nt} type="number" />}
                        {isNt && <RptTableCell value={item.du_co_nt} type="number" />}
                        {isNt && <RptTableCell value={item.ma_nt} />}
                        {isNt && <RptTableCell value={item.ty_gia} type="number" />}

                        <RptTableCell value={item.ps_no} />
                        <RptTableCell value={item.ps_co} />
                        <RptTableCell value={item.du_no} />
                        <RptTableCell value={item.du_co} />
                        <RptTableCell value={item.ma_kh} />
                        <RptTableCell value={item.ten_kh} />
                        <RptTableCell value={item.ma_ct} />
                    </RptTableRow>
                }) : undefined}
            </Table.Body>
        </RptTable>
    </>
}

export const GLRptNKC06 = {
    FilterForm: FilterForm,
    TableForm: TableForm,
    permission: "02.40.7",
    visible: true,    // hiện/ẩn báo cáo
    route: routes.GLRptNKC06,

    period: {
        fromDate: "ngay_ct1",
        toDate: "ngay_ct2"
    },

    linkHeader: {
        id: "GLRptNKC06",
        defaultMessage: "Sổ chi tiết tài khoản",
        active: true
    },

    info: {
        code: "02.21.23.1",
    },
    initFilter: {
        ngay_ct1: "",
        ngay_ct2: "",
        tk: "",
        ma_nt: "",
    },
    columns: [
        { id: "glnkc06.ngay_ct", defaultMessage: "Ngày ghi sổ", fieldName: "ngay_ct", type: "date", sorter: true },
        { id: "glnkc06.so_ct", defaultMessage: "Số ct", fieldName: "so_ct", type: "string", sorter: true },
        { id: "glnkc06.ngay_lct", defaultMessage: "Ngày ct", fieldName: "ngay_lct", type: "date", sorter: true },
        { id: "glnkc06.dien_giai", defaultMessage: "Diễn giải", fieldName: "dien_giai", type: "string", sorter: true },
        { id: "glnkc06.tk_du", defaultMessage: "Tk Đ/Ứ", fieldName: "tk_du", type: "string", sorter: true },

        { id: "glnkc06.ps_no", defaultMessage: "Ps nợ", fieldName: "ps_no_nt", type: "number", sorter: true, isNT: true },
        { id: "glnkc06.ps_co", defaultMessage: "Ps có", fieldName: "ps_co_nt", type: "number", sorter: true, isNT: true },
        { id: "glnkc06.ps_no", defaultMessage: "Dư nợ", fieldName: "du_no_nt", type: "number", sorter: true, isNT: true },
        { id: "glnkc06.ps_co", defaultMessage: "dư có", fieldName: "du_co_nt", type: "number", sorter: true, isNT: true },

        { id: "glnkc06.ma_nt", defaultMessage: "Mã NT", fieldName: "ma_nt", type: "string", sorter: true, isNT: true, hiddenNT: true },
        { id: "glnkc06.ty_gia", defaultMessage: "Tỷ giá", fieldName: "ty_gia", type: "number", sorter: true, isNT: true },

        { id: "glnkc06.ps_no", defaultMessage: "Ps nợ", fieldName: "ps_no", type: "number", sorter: true, },
        { id: "glnkc06.ps_co", defaultMessage: "Ps có", fieldName: "ps_co", type: "number", sorter: true, },
        { id: "glnkc06.ps_no", defaultMessage: "Dư nợ", fieldName: "du_no", type: "number", sorter: true, },
        { id: "glnkc06.ps_co", defaultMessage: "dư có", fieldName: "du_co", type: "number", sorter: true, },

        { id: "glnkc06.ma_kh", defaultMessage: "Mã KH", fieldName: "ma_kh", type: "string", sorter: true, },
        { id: "glnkc06.ten_kh", defaultMessage: "Tên KH", fieldName: "ten_kh", type: "string", sorter: true, },
        { id: "glnkc06.ma_ct", defaultMessage: "Mã CT", fieldName: "ma_ct", type: "string", sorter: true, },
    ],
    formValidation: [
        {
            id: "tk",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ]
        },
    ]
}