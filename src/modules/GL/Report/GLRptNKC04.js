import React from "react";
import { Table, Form } from "semantic-ui-react";
import { ZenFieldSelectApi, ZenDatePeriod, ZenField } from "../../../components/Control";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { RptHeader, RptTable, RptTableCell, RptTableRow } from "../../../components/Control/zReport";
import * as routes from "../../../constants/routes";

const FilterForm = ({ formik }) => {
    return <>
        <ZenField
            label={"rpt.Ds_Tk_Dt_Hh"} defaultlabel="TKDT hàng hóa"
            name="ds_tk_dt_hh" props={formik}
        />
        <ZenField
            label={"rpt.Ds_Tk_Dt_Tp"} defaultlabel="TKDT thành phẩm"
            name="ds_tk_dt_tp" props={formik}
        />
        <ZenField
            label={"rpt.Ds_Tk_Dt_Dv"} defaultlabel="TKDT dịch vụ"
            name="ds_tk_dt_dv" props={formik}
        />
        <ZenField
            label={"rpt.Ds_Tk_Pt"} defaultlabel="TK phải thu"
            name="ds_tk_pt" props={formik}
        />
        <ZenFieldSelectApi
            lookup={ZenLookup.Ma_nt}
            label={"rpt.Ma_nt"} defaultlabel="Ngoại tệ"
            name="ma_nt" formik={formik}
        />
    </>
}

const TableForm = ({ data = [], filter = {} }) => {
    return <>
        <RptTable maNt={filter.Ma_nt}>
            <Table.Header fullWidth>
                <RptHeader maNt={filter.Ma_nt}
                    header={[
                        { text: ["rpt.ngay_ct", "Ngày ghi sổ"] },
                        { text: ["rpt.so_ct", "Số CT"] },
                        { text: ["rpt.ngay_lct", "Ngày CT"] },
                        { text: ["rpt.dien_giai", "Diễn giải"] },
                        { text: ["rpt.tk_no", "Tk nợ"] },
                        { text: ["rpt.tk_co", "Tk có"] },

                        { text: ["rpt.tien_nt", "Phải thu"], isNT: true },
                        { text: ["rpt.tien_nt_hh", "Tiền HH"], isNT: true },
                        { text: ["rpt.tien_nt_tp", "Tiền TP"], isNT: true },
                        { text: ["rpt.tien_nt_dv", "Tiền DV"], isNT: true },
                        { text: ["rpt.ma_nt", "Mã NT"], isNT: true, hiddenNT: true },
                        { text: ["rpt.ty_gia", "Tỷ giá"], isNT: true },

                        { text: ["rpt.tien", "Phải thu"] },
                        { text: ["rpt.tien_hh", "Hàng hóa"] },
                        { text: ["rpt.tien_tp", "Thành phẩm"] },
                        { text: ["rpt.tien_dv", "Dịch vụ"] },
                        { text: ["rpt.ma_kh", "Mã KH"] },
                        { text: ["rpt.ten_kh", "Tên KH"] },
                        { text: ["rpt.ma_bp", "Bộ phận"] },
                        { text: ["rpt.ma_phi", "Mã phí"] },
                        { text: ["rpt.ma_hd", "Mã hợp đồng"] },
                        { text: ["rpt.ma_spct", "SP / CT"] },
                        { text: ["rpt.ma_ct", "Mã CT"] },
                    ]}
                />
            </Table.Header>
            <Table.Body>
                {data.length > 0 ? data.map((item, index) => {
                    const isNt = filter.Ma_nt && filter.Ma_nt !== 'VND' ? true : false
                    return <RptTableRow key={index} isBold={item.bold} index={index} item={item}>
                        <RptTableCell value={item.ngay_ct} type="date" />
                        <RptTableCell value={item.so_ct} />
                        <RptTableCell value={item.ngay_lct} type="date" />
                        <RptTableCell value={item.dien_giai} />
                        <RptTableCell value={item.tk_no} />
                        <RptTableCell value={item.tk_co} />

                        {isNt && <RptTableCell value={item.Tien_nt} type="number" />}
                        {isNt && <RptTableCell value={item.tien_nt_hh} type="number" />}
                        {isNt && <RptTableCell value={item.tien_nt_tp} type="number" />}
                        {isNt && <RptTableCell value={item.tien_nt_dv} type="number" />}
                        {isNt && <RptTableCell value={item.ma_nt} />}
                        {isNt && <RptTableCell value={item.ty_gia} type="number" />}

                        <RptTableCell value={item.Tien} type="number" />
                        <RptTableCell value={item.tien_hh} type="number" />
                        <RptTableCell value={item.tien_tp} type="number" />
                        <RptTableCell value={item.tien_dv} type="number" />
                        <RptTableCell value={item.ma_kh} />
                        <RptTableCell value={item.ten_kh} />
                        <RptTableCell value={item.ma_bp} />
                        <RptTableCell value={item.ma_phi} />
                        <RptTableCell value={item.ma_hd} />
                        <RptTableCell value={item.ma_spct} />
                        <RptTableCell value={item.ma_ct} />
                    </RptTableRow>
                }) : undefined}
            </Table.Body>
        </RptTable>
    </>
}

export const GLRptNKC04 = {
    FilterForm: FilterForm,
    TableForm: TableForm,
    permission: "02.40.5",
    visible: true,    // hiện/ẩn báo cáo
    route: routes.GLRptNKC04,

    period: {
        fromDate: "ngay_ct1",
        toDate: "ngay_ct2"
    },

    linkHeader: {
        id: "GLRptNKC04",
        defaultMessage: "Sổ nhật ký bán hàng",
        active: true
    },

    info: {
        code: "02.21.14.1",
    },
    initFilter: {
        ngay_ct1: "",
        ngay_ct2: "",
        ds_tk_dt_hh: "5111",
        ds_tk_dt_tp: "5112",
        ds_tk_dt_dv: "5113",
        ds_tk_pt: "131,111,112",
        ma_nt: "",
    },
    columns: [
        { id: "glnkc04.ngay_ct", defaultMessage: "Ngày ghi sổ", fieldName: "ngay_ct", type: "date", sorter: true },
        { id: "glnkc04.so_ct", defaultMessage: "Số ct", fieldName: "so_ct", type: "string", sorter: true },
        { id: "glnkc04.ngay_lct", defaultMessage: "Ngày ct", fieldName: "ngay_lct", type: "date", sorter: true },
        { id: "glnkc04.dien_giai", defaultMessage: "Diễn giải", fieldName: "dien_giai", type: "string", sorter: true },
        { id: "glnkc04.tk_no", defaultMessage: "Tk nợ", fieldName: "tk_no", type: "string", sorter: true },
        { id: "glnkc04.tk_co", defaultMessage: "Tk có", fieldName: "tk_co", type: "string", sorter: true },

        { id: "glnkc04.tien", defaultMessage: "Phải thu", fieldName: "tien_nt", type: "number", sorter: true, isNT: true },
        { id: "glnkc04.tien_hh", defaultMessage: "Tiền HH", fieldName: "tien_nt_hh", type: "number", sorter: true, isNT: true },
        { id: "glnkc04.tien_tp", defaultMessage: "Tiền TP", fieldName: "tien_nt_tp", type: "number", sorter: true, isNT: true },
        { id: "glnkc04.tien_dv", defaultMessage: "Tiền DV", fieldName: "tien_nt_dv", type: "number", sorter: true, isNT: true },

        { id: "glnkc04.ma_nt", defaultMessage: "Mã NT", fieldName: "ma_nt", type: "string", sorter: true, isNT: true, hiddenNT: true },
        { id: "glnkc04.ty_gia", defaultMessage: "Tỷ giá", fieldName: "ty_gia", type: "number", sorter: true, isNT: true },

        { id: "glnkc04.tien", defaultMessage: "Phải thu", fieldName: "tien", type: "number", sorter: true, },
        { id: "glnkc04.tien_hh", defaultMessage: "Tiền HH", fieldName: "tien_hh", type: "number", sorter: true, },
        { id: "glnkc04.tien_tp", defaultMessage: "Tiền TP", fieldName: "tien_tp", type: "number", sorter: true, },
        { id: "glnkc04.tien_dv", defaultMessage: "Tiền DV", fieldName: "tien_dv", type: "number", sorter: true, },

        { id: "glnkc04.ma_kh", defaultMessage: "Mã KH", fieldName: "ma_kh", type: "string", sorter: true },
        { id: "glnkc04.ten_kh", defaultMessage: "Tên KH", fieldName: "ten_kh", type: "string", sorter: true },
        { id: "glnkc04.ma_bp", defaultMessage: "Mã bộ phận", fieldName: "ma_bp", type: "string", sorter: true },
        { id: "glnkc04.ma_phi", defaultMessage: "Mã phí", fieldName: "ma_phi", type: "string", sorter: true },
        { id: "glnkc04.ma_hd", defaultMessage: "Mã hợp đồng", fieldName: "ma_hd", type: "string", sorter: true },
        { id: "glnkc04.ma_spct", defaultMessage: "Mã SPCT", fieldName: "ma_spct", type: "string", sorter: true },
        { id: "glnkc04.ma_ct", defaultMessage: "Mã CT", fieldName: "ma_ct", type: "string", sorter: true, },
    ],
    formValidation: [
        {
            id: "ds_tk_dt_hh",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ]
        },
        {
            id: "ds_tk_dt_tp",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ]
        },
        {
            id: "ds_tk_dt_dv",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ]
        },
        {
            id: "ds_tk_pt",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ]
        },
    ]
}
