import React, { useState, useEffect } from "react";
import { InputDate, ZenField, ZenFieldDate, ZenFieldSelectApi, ZenMessageAlert } from "../../../components/Control/index";
import { FormMode, ZenHelper } from "../../../utils";
import { Form, Segment, Grid, Dropdown, Select, Button, Dimmer, Message, Loader } from "semantic-ui-react";
import _ from 'lodash';
import { ApiGLCalc as Api } from '../Api';
import { injectIntl, FormattedMessage, useIntl } from 'react-intl';
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";

const GLChuyenSoDuTK = (props) => {
   const intl = useIntl();
   const [nam, setNam] = useState(ZenHelper.getFiscalYear());
   const [ngay_cntc, setNgayCNTC] = useState(ZenHelper.getLastDayOfMonth(11, nam));
   const [errorInfo, setErrorInfo] = useState({ messageList: [], isError: false });
   const [loadingForm, setLoadingForm] = useState(false);

   let questBefore = (mess, callBack) => {
      let result = ZenMessageAlert.question(mess);
      Promise.all([result])
         .then(values => {
            if (values[0]) {
               callBack()
            }
         }).catch(err => {

         });
   }

   let clearError = () => {
      setErrorInfo({ isError: false, messageList: [] })
   }

   let getData = () => {
      return {
         ngay_cntc: ngay_cntc
      }
   }

   let thucHien = () => {
      questBefore(getMessage("GLChuyenSoDuTK.chuyen", "Bạn có chắc muốn chuyển dữ liệu sang năm sau!"), () => {
         setLoadingForm(true);
         clearError();

         Api.chuyenSoDuTK(getData(), res => {
            if (res.status == 200) {
               ZenMessageAlert.success();
               setLoadingForm(false);
            } else {
               let mess = ZenHelper.getResponseError(res);
               setErrorInfo({ isError: true, messageList: mess });
               setLoadingForm(false);
            }
         });
      });
   }

   let onFilterChange = (e, { name, value }) => {
      switch (name) {
         case 'ngay_cntc':
            setNgayCNTC(value);
            break;
         default:
            break;
      }
   }

   let getMessage = (id, defaultMessage, values) => {
      return intl.formatMessage({ id: id || " ", defaultMessage: defaultMessage || "unknow" }, values ? { ...values } : undefined)
   }

   return <React.Fragment>
      <Segment >
         <Grid>
            <Grid.Row centered>
               <Grid.Column width={6}>
                  <Form>
                     {loadingForm && <Dimmer active={loadingForm} inverted>
                        <Loader inverted>Loading</Loader>
                     </Dimmer>}

                     {errorInfo.isError && <Message negative list={errorInfo.messageList}
                        onDismiss={() => setErrorInfo({ isError: false, messageList: [] })} />}
                     <Form.Group>
                        <Form.Field width='4'>
                           <label>&nbsp;</label>
                           <label>
                              {getMessage("ngay_cntc", "Ngày cuối năm thực hiện")}
                           </label>
                        </Form.Field>
                        <Form.Field readOnly={true}
                           width={12} inline control={InputDate}
                           selected={ngay_cntc} isChangeBlur
                           onChange={(data) => onFilterChange({}, data)}
                        //label={getMessage("ngay_cntc", "Ngày cuối năm thực hiện")}
                        />
                     </Form.Group>
                     <Form.Group>
                        <Form.Button
                           content={getMessage("GLChuyenSoDuTK.chuyen", "Thực hiện")}
                           positive
                           onClick={thucHien}
                        >
                        </Form.Button>
                     </Form.Group>
                  </Form>
               </Grid.Column>
            </Grid.Row>
         </Grid>
      </Segment>
   </React.Fragment>
}

export default GLChuyenSoDuTK;