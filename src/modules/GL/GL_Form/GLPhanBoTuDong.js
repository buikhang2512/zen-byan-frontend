import React, { useState, useEffect } from "react";
import { FormatNumber, ZenDatePeriod, ZenMessageAlert, InputNumber, ButtonRefresh } from "../../../components/Control/index";
import { FormMode, ZenHelper } from "../../../utils";
import { Form, Segment, Checkbox, Button, Dimmer, Message, Loader, Container, Table, Divider, Input, Dropdown, Icon, DimmerDimmable } from "semantic-ui-react";
import _ from 'lodash';
import { ApiCOCalc as api } from '../Api';
import { injectIntl, useIntl } from 'react-intl';
import { GlobalStorage, KeyStorage } from "../../../utils/storage";

const GLPhanBoTuDong = (props) => {
   const intl = useIntl();
   const [ngay1, setNgay1] = useState(ZenHelper.getfirstDayOfCurrentMonth());
   const [ngay2, setNgay2] = useState(ZenHelper.getLastDayOfCurrentMonth());
   const [checkAll, setCheckAll] = useState(false);
   const MODULE = "GL";
   const MESS = "Bạn chưa chọn bút toán nào để thực hiện!";
   const [errorInfo, setErrorInfo] = useState({ messageList: [], isError: false });
   const [loadingForm, setLoadingForm] = useState(false);
   const [loadingCTForm, setLoadingCTForm] = useState(false);
   const [dataTypePB, setDataTypePB] = useState([]);
   const [dataPHPB, setDataPHPB] = useState({ data: [] });
   const [dataCTPB, setDataCTPB] = useState({ data: [] });
   const [currentId, setCurrentId] = useState(null);
   const [currentHSTheo, setCurrentHSTheo] = useState('1');

   const handleChangeDate = ({ startDate, endDate }) => {
      setNgay1(startDate);
      setNgay2(endDate);
      GlobalStorage.setByField(KeyStorage.Global, 'from_date', startDate)
      GlobalStorage.setByField(KeyStorage.Global, 'to_date', endDate)
   }

   let questBefore = (mess, callBack) => {
      let result = ZenMessageAlert.question(mess);
      Promise.all([result])
         .then(values => {
            if (values[0]) {
               callBack()
            }
         }).catch(err => {

         });
   }

   useEffect(() => {
      getData();
   }, [])

   let clearError = () => {
      setErrorInfo({ isError: false, messageList: [] })
   }

   let getFilter = () => {
      return {
         ksd: false,
         module: MODULE,
         year: ZenHelper.getFiscalYear()
      }
   }

   let getPBItems = (arr) => {
      return _.map(arr, (item) => {
         return {
            ngay1: ngay1,
            ngay2: ngay2,
            id: item.id
         }
      });
   }

   let getPBHSItems = (arr) => {
      return _.map(arr, (item) => {
         return {
            ngay1: ngay1,
            ngay2: ngay2,
            id: item.id,
            pb_theo: (item.theospct && 'MA_SPCT') || (item.TheoBP && 'MA_BP') || (item.TheoTKDU && 'TK_DU') || '',
            hs_theo: currentHSTheo,

         }
      });
   }

   let getUpdHSItems = (arr) => {
      return _.map(arr, (item) => {
         return {
            ngay1: ngay1,
            ngay2: ngay2,
            id: item.id,
            nam: ZenHelper.getFiscalYear(),
            thang: item.thang,
            tk: item.tk_nhan_pb,
            ma_spct: item.ma_spct,
            ma_bp: item.ma_bp,
            tk_du: '',
            hs: item.he_so,
            tien_nhan: item.tien_nhan
         }
      });
   }

   let updPBHS = (items) => {
      if (!items || items.length < 1) {
         ZenMessageAlert.warning(MESS);
         return;
      }
      setLoadingCTForm(true);
      api.updPBHS(getUpdHSItems(items), res => {
         if (res.status == 200) {
            ZenMessageAlert.success();
            getData(false, false, true);
            setLoadingCTForm(false);
         } else {
            let mess = ZenHelper.getResponseError(res);
            setErrorInfo({ isError: true, messageList: mess });
            setLoadingCTForm(false);
         }
      });
   }

   let phanBo = () => {
      if (ZenHelper.checkNgayKsAuto(ngay1)) return

      let items = _.filter(dataPHPB.data, (item) => {
         return item.tag == '1';
      });
      if (items.length < 1) {
         ZenMessageAlert.warning(MESS);
         return;
      }
      questBefore("Bạn có chắc muốn tạo phân bổ tự động không!", () => {
         setLoadingForm(true);
         clearError();

         api.calPB(getPBItems(items), res => {
            if (res.status == 200) {
               ZenMessageAlert.success();
               refreshData();
               setLoadingForm(false);
            } else {
               let mess = ZenHelper.getResponseError(res);
               setErrorInfo({ isError: true, messageList: mess });
               setLoadingForm(false);
            }
         });
      });
   }

   let refreshData = () => {
      dataPHPB.data = _.map(dataPHPB.data, (item) => {
         item.tag = '0';
         return item;
      });
      setDataPHPB({ ...dataPHPB })
   }

   let xoaPhanBo = () => {
      if (ZenHelper.checkNgayKsAuto(ngay1)) return

      let items = _.filter(dataPHPB.data, (item) => {
         return item.tag == '1'
      });
      if (items.length < 1) {
         ZenMessageAlert.warning(MESS);
         return;
      }
      questBefore("Bạn có chắc muốn xóa phân bổ tự động không!", () => {
         setLoadingForm(true);
         clearError();

         api.delcalPB(getPBItems(items), res => {
            if (res.status == 200) {
               ZenMessageAlert.success();
               refreshData();
               setLoadingForm(false);
            } else {
               let mess = ZenHelper.getResponseError(res);
               setErrorInfo({ isError: true, messageList: mess });
               setLoadingForm(false);
            }
         });
      });
   }

   let PhanBoHS = () => {
      if (ZenHelper.checkNgayKsAuto(ngay1)) return

      let items = _.filter(dataPHPB.data, (item) => {
         return item.tag == '1'
      });
      if (items.length < 1) {
         ZenMessageAlert.warning(MESS);
         return;
      }
      questBefore("Bạn có chắc muốn tạo phân bổ tự động theo hệ số không!", () => {
         setLoadingForm(true);
         clearError();

         api.calPBHS(getPBHSItems(items), res => {
            if (res.status == 200) {
               ZenMessageAlert.success();
               refreshData();
               setLoadingForm(false);
            } else {
               let mess = ZenHelper.getResponseError(res);
               setErrorInfo({ isError: true, messageList: mess });
               setLoadingForm(false);
            }
         });
      });
   }

   let getData = (isType = true, isPH = true, isCT = true) => {
      if (!(isType || isPH || isCT)) {
         return;
      }
      let arrP = [];
      if (isType) {
         arrP.push(getTypePBPromise());
      }
      if (isPH) {
         arrP.push(getPHPBPromise());
      }
      if (isCT) {
         arrP.push(getCTPBPromise());
      }
      clearError();
      setLoadingForm(true);
      Promise.all(arrP)
         .then(values => {
            let idx = 0;
            if (isType) {
               setDataTypePB(ZenHelper.convertToSelectOptions(values[idx].data?.data, false, 'id', 'id', 'ten'));
               setCurrentHSTheo(values[idx].data?.data[0].id)
               idx++;
            }
            if (isPH) {
               setDataPHPB(values[idx].data);
               idx++;
            }
            if (isCT) {
               setDataCTPB(values[idx].data);
               idx++;
            }
            setLoadingForm(false);
         }).catch(err => {
            let mess = ZenHelper.getResponseError(err);
            setErrorInfo({ isError: true, messageList: mess });
            setLoadingForm(false);
         });
   };

   let getTypePBPromise = () => {
      return new Promise((resolve, reject) => {
         api.getDataTypePB(getFilter(), res => {
            if (res.status == 200) {
               resolve(res);
            } else {
               reject(res);
            }
         })
      });
   }
   let getPHPBPromise = () => {
      return new Promise((resolve, reject) => {
         api.getDataPbPh(getFilter(), res => {
            if (res.status == 200) {
               resolve(res);
            } else {
               reject(res);
            }
         })
      });
   }
   let getCTPBPromise = () => {
      return new Promise((resolve, reject) => {
         api.getDataPbCt(getFilter(), res => {
            if (res.status == 200) {
               resolve(res);
            } else {
               reject(res);
            }
         })
      });
   }

   let getMessage = (id, defaultMessage, values) => {
      return intl.formatMessage({ id: id || " ", defaultMessage: defaultMessage || "unknow" }, values ? { ...values } : undefined)
   }

   let onPHChecked = (idx, d) => {
      dataPHPB.data[idx].tag = d.checked ? '1' : '0';
      setDataPHPB({ ...dataPHPB });
   }

   let onCheckedAll = (d) => {
      _.forEach(dataPHPB.data, (item) => {
         item.tag = d.checked ? '1' : '0';
      });
      setCheckAll(d.checked);
      setDataPHPB({ ...dataPHPB });
   }

   let onCurrentPHChange = (id) => {
      setCurrentId(id);
   }

   let onCurrentHSChange = (e, data) => {
      setCurrentHSTheo(data.value);
   }

   let getHsName = (id) => {
      let currentItem = _.find(dataTypePB, (item) => {
         return item.key == id;
      });
      if (currentItem) {
         return currentItem.text;
      };
      return '';
   }

   let getCTView = () => {
      let dNgay1 = new Date(ngay1);
      let dNgay2 = new Date(ngay2);
      return _.filter(dataCTPB.data, (item) => {
         return (item.id == currentId && item.thang >= dNgay1.getMonth() + 1 && item.thang <= dNgay2.getMonth() + 1)
      });
   }

   let onHandleNumberChanged = (info, item, type = 1) => {
      item[info.name] = info.floatValue;
      switch (type) {
         case 1:
            setDataPHPB({ ...dataPHPB });
            break;
         case 2:
            item.edited = true;
            setDataCTPB({ ...dataCTPB });
            break;
         default:
            break;
      }
   }

   let onHandleUpdHS = (items) => {
      updPBHS(items);
   }

   let globalStorage = GlobalStorage.get(KeyStorage.Global);

   return <React.Fragment>
      <Segment>
         <Form>
            <Form.Group widths='equal'
               style={{ marginBottom: "0px" }}>
               <ZenDatePeriod
                  width={4}
                  idMessage="GLPBTD.period"
                  defaultMessage="Kỳ thực hiện phân bổ"
                  onChange={handleChangeDate}
                  value={[ngay1, ngay2]}
                  defaultPopupYear={globalStorage.financial_year}
               />
               <Form.Field>
                  <label>Phương pháp tính hệ số</label>
                  <Button.Group color='teal'>
                     <Dropdown
                        className='button'
                        floating
                        options={dataTypePB}
                        trigger={<>{getHsName(currentHSTheo)}</>}
                        value={currentHSTheo}
                        onChange={onCurrentHSChange}
                     />
                     <Button onClick={PhanBoHS} style={{ marginLeft: "5px", marginRight: "5px" }}>Điền hệ số</Button>
                  </Button.Group>
                  <Button
                     content={getMessage("GLPhanBoTuDong.phan_bo", "Phân bổ")}
                     primary
                     onClick={phanBo}
                  ></Button>
                  <Button
                     content={getMessage("GLPhanBoTuDong.xoa_phan_bo", "Xóa phân bổ")}
                     negative
                     onClick={xoaPhanBo}
                  ></Button>
                  {/* <ButtonRefresh
                     // color = 'blue'
                     // floated='right'
                     // icon='refresh'
                     onClick={getData}
                  ></ButtonRefresh> */}
               </Form.Field>
            </Form.Group>
         </Form>
      </Segment>

      <Container fluid>
         <Container>
            {loadingForm && <Dimmer active={loadingForm} inverted>
               <Loader inverted>Loading</Loader>
            </Dimmer>}

            {errorInfo.isError && <Message negative list={errorInfo.messageList}
               onDismiss={() => setErrorInfo({ isError: false, messageList: [] })} />}
         </Container>
         <Divider />
         <strong><Checkbox checked={checkAll}
            label={getMessage("GLPhanBoTuDong.check_all", "Chọn/Hủy chọn tất cả")}
            onChange={(e, d) => onCheckedAll(d)} /></strong>
         <Table>
            <Table.Header>
               <Table.Row>
                  <Table.HeaderCell>{getMessage("GLPhanBoTuDong.tag", "Chọn")}</Table.HeaderCell>
                  <Table.HeaderCell>{getMessage("GLPhanBoTuDong.ten_bt", "Tên bút toán")}</Table.HeaderCell>
                  <Table.HeaderCell>{getMessage("GLPhanBoTuDong.theospct", "Theo SPCT")}</Table.HeaderCell>
                  <Table.HeaderCell>{getMessage("GLPhanBoTuDong.TheoBP", "Theo BP")}</Table.HeaderCell>
                  <Table.HeaderCell>{getMessage("GLPhanBoTuDong.TheoTKDU", "Theo tài khoản Đ/Ư")}</Table.HeaderCell>
                  <Table.HeaderCell>{getMessage("GLPhanBoTuDong.tk_pb", "Tài khoản")}</Table.HeaderCell>
                  <Table.HeaderCell>{getMessage("GLPhanBoTuDong.tien_pb", "Tiền phân bổ")}</Table.HeaderCell>
               </Table.Row>
            </Table.Header>
            <Table.Body>
               {dataPHPB && dataPHPB.data.length > 0 && dataPHPB.data.map((x, idx) => {
                  return <Table.Row active={currentId == x.id} onClick={() => onCurrentPHChange(x.id)}
                     style={{ cursor: 'pointer' }}
                  >
                     <Table.Cell>
                        <Checkbox checked={(x.tag == '1')}
                           onChange={(e, d) => onPHChecked(idx, d)} />
                     </Table.Cell>
                     <Table.Cell>{x.ten_bt}</Table.Cell>
                     <Table.Cell><Checkbox checked={(x.theospct)} /></Table.Cell>
                     <Table.Cell><Checkbox checked={(x.TheoBP)} /></Table.Cell>
                     <Table.Cell><Checkbox checked={(x.TheoTKDU)} /></Table.Cell>
                     <Table.Cell>{x.tk_pb}</Table.Cell>
                     <Table.Cell>
                        <InputNumber
                           name={'tien_pb'}
                           value={x.tien_pb}
                           onValueChange={(value) => { onHandleNumberChanged(value, x) }}
                        />
                     </Table.Cell>
                  </Table.Row>
               })}
            </Table.Body>
         </Table>
         <Divider />
         {/* <Button disabled = {} icon='edit' onClick={() =>onHandleUpdAllHS([x])} /> */}

         <DimmerDimmable>
            {loadingCTForm && <Dimmer active inverted>
               <Loader inverted>Loading</Loader>
            </Dimmer>}
         </DimmerDimmable>
         <Table>
            <Table.Header>
               <Table.Row>
                  <Table.HeaderCell>{getMessage("GLPhanBoTuDong.tk_nhan", "Tài khoản nhận")}</Table.HeaderCell>
                  <Table.HeaderCell>{getMessage("GLPhanBoTuDong.ten_tk", "Tên tài khoản")}</Table.HeaderCell>
                  <Table.HeaderCell>{getMessage("GLPhanBoTuDong.ma_spct", "Mã SPCT")}</Table.HeaderCell>
                  <Table.HeaderCell>{getMessage("GLPhanBoTuDong.ten_spct", "Tên SPCT")}</Table.HeaderCell>
                  <Table.HeaderCell>{getMessage("GLPhanBoTuDong.ma_bp", "Mã bộ phận")}</Table.HeaderCell>
                  <Table.HeaderCell>{getMessage("GLPhanBoTuDong.ten_bp", "Tên bộ phận")}</Table.HeaderCell>
                  <Table.HeaderCell>{getMessage("GLPhanBoTuDong.he_so", "Hệ số")}</Table.HeaderCell>
                  {/* <Table.HeaderCell>{getMessage("GLPhanBoTuDong.pt_pb", "%")}</Table.HeaderCell> */}
                  <Table.HeaderCell>{getMessage("GLPhanBoTuDong.tien_nhan", "Tiền nhận")}</Table.HeaderCell>
                  <Table.HeaderCell>{getMessage("GLPhanBoTuDong.edited", "Cập nhật")}</Table.HeaderCell>
               </Table.Row>
            </Table.Header>
            <Table.Body>
               {getCTView().map((x, idx) => {
                  return <Table.Row>
                     <Table.Cell>{x.tk_nhan_pb}</Table.Cell>
                     <Table.Cell>{x.ten_tk}</Table.Cell>
                     <Table.Cell>{x.ma_spct}</Table.Cell>
                     <Table.Cell>{x.ten_spct}</Table.Cell>
                     <Table.Cell>{x.ma_bp}</Table.Cell>
                     <Table.Cell>{x.ten_bp}</Table.Cell>
                     <Table.Cell> <InputNumber
                        name={'he_so'}
                        value={x.he_so}
                        onValueChange={(value) => { onHandleNumberChanged(value, x, 2) }}
                     /></Table.Cell>
                     {/* <Table.Cell> <InputNumber
                        name={'pt_pb'}
                        value={x.pt_pb}
                        onValueChange={(value) => { onHandleNumberChanged(value, x, 2) }}
                     /></Table.Cell> */}
                     <Table.Cell> <InputNumber
                        name={'tien_nhan'}
                        value={x.tien_nhan}
                        onValueChange={(value) => { onHandleNumberChanged(value, x, 2) }}
                     /></Table.Cell>
                     <Table.Cell><Button disabled={!x.edited} icon='edit' onClick={() => onHandleUpdHS([x])} /></Table.Cell>
                  </Table.Row>
               })}
            </Table.Body>
         </Table>
      </Container>
   </React.Fragment >
}

export default GLPhanBoTuDong;