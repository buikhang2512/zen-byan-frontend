import React, { useState, useEffect } from "react";
import { FormatNumber, ZenDatePeriod, ZenMessageAlert } from "../../../components/Control/index";
import { FormMode, ZenHelper } from "../../../utils";
import { Form, Segment, Checkbox, Button, Dimmer, Message, Loader, Container, Table, Divider, Input } from "semantic-ui-react";
import _ from 'lodash';
import { ApiGLCalc as api } from '../Api';
import { useIntl } from 'react-intl';
import { GlobalStorage, KeyStorage } from "../../../utils/storage";

const GLKetChuyeTuDong = (props) => {
   const intl = useIntl();
   const [ngay1, setNgay1] = useState(ZenHelper.getfirstDayOfCurrentMonth());
   const [ngay2, setNgay2] = useState(ZenHelper.getLastDayOfCurrentMonth());
   const [checkAll, setCheckAll] = useState(false);
   const MODULE = "GL";
   const MESS = "Bạn chưa chọn bút toán nào để thực hiện!";
   const [errorInfo, setErrorInfo] = useState({ messageList: [], isError: false });
   const [loadingForm, setLoadingForm] = useState(false);
   const [dataInfo, setDataInfo] = useState({ data: [] });

   const handleChangeDate = ({ startDate, endDate }) => {
      setNgay1(startDate);
      setNgay2(endDate);
      GlobalStorage.setByField(KeyStorage.Global, 'from_date', startDate)
      GlobalStorage.setByField(KeyStorage.Global, 'to_date', endDate)
   }

   let questBefore = (mess, callBack) => {
      let result = ZenMessageAlert.question(mess);
      Promise.all([result])
         .then(values => {
            if (values[0]) {
               callBack()
            }
         }).catch(err => {

         });
   }

   useEffect(() => {
      getData();
   }, [])

   let clearError = () => {
      setErrorInfo({ isError: false, messageList: [] })
   }

   let getFilter = () => {
      return {
         module: MODULE
      }
   }

   let getItems = (arr) => {
      return _.map(arr, (item) => {
         return {
            ngay1: ngay1,
            ngay2: ngay2,
            tk: item.tk,
            tk_du: item.tk_du
         }
      });
   }

   let ketChuyen = () => {
      if (ZenHelper.checkNgayKsAuto(ngay1)) return

      let items = _.filter(dataInfo.data, (item) => {
         return item.tag == '1'
      });
      if (items.length < 1) {
         ZenMessageAlert.warning(MESS, false);
         return;
      }
      questBefore("Bạn có chắc muốn tạo kết chuyển tự động không!", () => {
         setLoadingForm(true);
         clearError();

         api.KetChuyenTuDong(getItems(items), res => {
            if (res.status == 200) {
               ZenMessageAlert.success();
               refreshData();
               setLoadingForm(false);
            } else {
               let mess = ZenHelper.getResponseError(res);
               setErrorInfo({ isError: true, messageList: mess });
               setLoadingForm(false);
            }
         });
      });
   }

   let refreshData = () => {
      dataInfo.data = _.map(dataInfo.data, (item) => {
         item.tag = '0';
         return item;
      });
      setDataInfo({ ...dataInfo });
   }

   let xoaKetChuyen = () => {
      if (ZenHelper.checkNgayKsAuto(ngay1)) return
      let items = _.filter(dataInfo.data, (item) => {
         return item.tag == '1'
      });
      if (items.length < 1) {
         ZenMessageAlert.warning(MESS);
         return;
      }
      questBefore("Bạn có chắc muốn xóa kết chuyển tự động không!", () => {
         setLoadingForm(true);
         clearError();

         api.XoaKetChuyenTuDong(getItems(items), res => {
            if (res.status == 200) {
               ZenMessageAlert.success();
               refreshData();
               setLoadingForm(false);
            } else {
               let mess = ZenHelper.getResponseError(res);
               setErrorInfo({ isError: true, messageList: mess });
               setLoadingForm(false);
            }
         });
      });
   }

   let getData = () => {
      let data = getKetChuyen();
      clearError();
      setLoadingForm(true);
      Promise.all([data])
         .then(values => {
            setDataInfo(values[0].data);
            setLoadingForm(false);
         }).catch(err => {
            let mess = ZenHelper.getResponseError(err);
            setErrorInfo({ isError: true, messageList: mess });
            setLoadingForm(false);
         });
   };

   let getKetChuyen = () => {
      return new Promise((resolve, reject) => {
         api.getKetChuyenTuDong(getFilter(), res => {
            if (res.status == 200) {
               resolve(res);
            } else {
               reject(res);
            }
         })
      });
   }

   let getMessage = (id, defaultMessage, values) => {
      return intl.formatMessage({ id: id || " ", defaultMessage: defaultMessage || "unknow" }, values ? { ...values } : undefined)
   }

   let onChecked = (idx, d) => {
      dataInfo.data[idx].tag = d.checked ? '1' : '0';
      setDataInfo({ ...dataInfo });
   }

   let onCheckedAll = (d) => {
      _.forEach(dataInfo.data, (item) => {
         item.tag = d.checked ? '1' : '0';
      });
      setCheckAll(d.checked);
      setDataInfo({ ...dataInfo });
   }

   let globalStorage = GlobalStorage.get(KeyStorage.Global);

   return <React.Fragment>
      <Segment>
         <Form>
            <Form.Group widths='equal'
               style={{ marginBottom: "0px" }}>
               <ZenDatePeriod
                  width={4}
                  idMessage="GLKCTD.period"
                  onChange={handleChangeDate}
                  value={[ngay1, ngay2]}
                  defaultPopupYear={globalStorage.financial_year}
               />
               <Form.Field>
                  <label>&nbsp;</label>
                  <Button
                     content={getMessage("GLKetChuyeTuDong.ket_chuyen", "Kết chuyển")}
                     primary
                     onClick={ketChuyen}
                  ></Button>
                  <Button
                     content={getMessage("GLKetChuyeTuDong.xoa_ket_chuyen", "Xóa kết chuyển")}
                     negative
                     onClick={xoaKetChuyen}
                  ></Button>
               </Form.Field>
            </Form.Group>
         </Form>
      </Segment>

      <Container fluid>
         <Container>
            {loadingForm && <Dimmer active={loadingForm} inverted>
               <Loader inverted>Loading</Loader>
            </Dimmer>}

            {errorInfo.isError && <Message negative list={errorInfo.messageList}
               onDismiss={() => setErrorInfo({ isError: false, messageList: [] })} />}
         </Container>
         <Divider />
         <strong><Checkbox checked={checkAll}
            label={getMessage("GLKetChuyeTuDong.check_all", "Chọn/Hủy chọn tất cả")}
            onChange={(e, d) => onCheckedAll(d)} /></strong>
         <Table>
            <Table.Header>
               <Table.Row>
                  <Table.HeaderCell>{getMessage("GLKetChuyeTuDong.chon", "Chọn")}</Table.HeaderCell>
                  <Table.HeaderCell>{getMessage("GLKetChuyeTuDong.stt", "STT")}</Table.HeaderCell>
                  <Table.HeaderCell>{getMessage("GLKetChuyeTuDong.ten_bt", "Tên bút toán")}</Table.HeaderCell>
                  <Table.HeaderCell>{getMessage("GLKetChuyeTuDong.tk", "Tài khoản")}</Table.HeaderCell>
                  <Table.HeaderCell>{getMessage("GLKetChuyeTuDong.ten_loai", "Loại kết chuyển")}</Table.HeaderCell>
                  <Table.HeaderCell>{getMessage("GLKetChuyeTuDong.tk_du", "Tài khoản Đ/Ư")}</Table.HeaderCell>
                  <Table.HeaderCell>{getMessage("GLKetChuyeTuDong.so_ct", "Số chứng từ")}</Table.HeaderCell>
                  <Table.HeaderCell>{getMessage("GLKetChuyeTuDong.kc_hd", "Theo hợp đồng")}</Table.HeaderCell>
                  <Table.HeaderCell>{getMessage("GLKetChuyeTuDong.kc_bp", "Theo bộ phận")}</Table.HeaderCell>
                  <Table.HeaderCell>{getMessage("GLKetChuyeTuDong.kc_spct", "Theo SPCT")}</Table.HeaderCell>
                  <Table.HeaderCell>{getMessage("GLKetChuyeTuDong.kc_phi", "Theo phí")}</Table.HeaderCell>
               </Table.Row>
            </Table.Header>
            <Table.Body>
               {dataInfo && dataInfo.data.length > 0 && dataInfo.data.map((x, idx) => {
                  return <Table.Row>
                     <Table.Cell>
                        <Checkbox checked={(x.tag == '1')}
                           onChange={(e, d) => onChecked(idx, d)} />
                     </Table.Cell>
                     <Table.Cell>{x.stt}</Table.Cell>
                     <Table.Cell>{x.ten_bt}</Table.Cell>
                     <Table.Cell>{x.tk}</Table.Cell>
                     <Table.Cell>{x.ten_loai}</Table.Cell>
                     <Table.Cell>{x.tk_du}</Table.Cell>
                     <Table.Cell>{x.so_ct}</Table.Cell>
                     <Table.Cell><Checkbox checked={(x.kc_hd)} /></Table.Cell>
                     <Table.Cell><Checkbox checked={(x.kc_bp)} /></Table.Cell>
                     <Table.Cell><Checkbox checked={(x.kc_spct)} /></Table.Cell>
                     <Table.Cell><Checkbox checked={(x.kc_phi)} /></Table.Cell>
                  </Table.Row>
               })}
            </Table.Body>
         </Table>
      </Container>
   </React.Fragment>
}

export default GLKetChuyeTuDong;