import * as routes from "../../../constants/routes";
import { ZenHelper } from "../../../utils";
import { GLKetChuyeTuDong, GLPhanBoTuDong, GLChuyenSoDuTK } from "./";

const GL_FormInfo = {
  KETCHUYENTUDONG: {
    route: routes.GLKETCHUYENTUDONG,
    Zenform: GLKetChuyeTuDong,
    linkHeader: {
      id: "GLKETCHUYENTUDONG",
      defaultMessage: "Kết chuyển tự động",
      active: true
    },
    action: {
      view: { visible: true, permission: "02.05.2" },
    },
  },
  PHANBOTUDONG: {
    route: routes.GLPHANBOTUDONG,
    Zenform: GLPhanBoTuDong,
    linkHeader: {
      id: "GLPHANBOTUDONG",
      defaultMessage: "Phân bổ chi phí tự động",
      active: true
    },
    action: {
      view: { visible: true, permission: "02.05.4" },
    },
  },

  GLChuyenSoDuTK: {
    route: routes.GLCHUYENSODUTK,
    Zenform: GLChuyenSoDuTK,
    linkHeader: {
      id: "GLCHUYENSODUTK",
      defaultMessage: ZenHelper.GetMessage(
        "GLCHUYENSODUTK",
        "Chuyển số dư tài khoản sang năm sau"
      ),
      active: true,
    },
    action: {
      view: { visible: true, permission: "90.21.5" },
    },
  },
};

export default GL_FormInfo;
