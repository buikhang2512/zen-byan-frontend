import { GLVchGL1List, GLVchGL1Edit} from './GLVchGL1/index'

export const GL_Voucher = {
    GL1: GLVchGL1List,
}

export const GL_VoucherEdit = {
    GL1: GLVchGL1Edit,
}