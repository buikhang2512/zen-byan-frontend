﻿import React, { useContext, useState } from "react";
import { Grid, Table } from "semantic-ui-react";
import { ZenField, ZenFieldDate } from "../../../../components/Control/index";
import { ZenHelper } from "../../../../utils";
import { ApiGLVchGL1 } from "../../Api/index";
import {
    ContextVoucher, VoucherHelper,
    RowItemPH, NumberCell, TextCell, DeleteCell, RowHeaderCell,
    ActionType, SelectCell, TableScroll, TableTotalPH, RowTotalPH
} from "../../../../components/Control/zVoucher";
import { ZenLookup } from "../../../ComponentInfo/Dictionary/ZenLookup";
import * as routes from "../../../../constants/routes";
import { ConfigBySiDmCt } from "../../../../components/Control/zVoucher/ZenVoucherHelper";
import { GlobalStorage, KeyStorage } from "../../../../utils/storage";

const PHForm = ({ formik, permission, modeForm, FieldNT }) => {
    const { ct, onUpdatePHCT } = useContext(ContextVoucher)
    let isReadOnly = VoucherHelper.f_ReadOnly(modeForm, permission)

    function getTyGia(ma_nt, ngay_ct) {
        let date_ct = ngay_ct ? ngay_ct : null
        let newArr = [];
        const getLocalTyGia = GlobalStorage.getByField(KeyStorage.CacheData, 'sidmtgnt').data
        newArr = getLocalTyGia.filter(i => i.ma_nt === ma_nt)
        if (newArr.length) {
            if (date_ct) {
                var findDateClosest = newArr.filter(i => ZenHelper.formatDateTime(i.ngay_tg, 'YYYY-MM-DD') <= date_ct)
                if (!findDateClosest || findDateClosest.length === 0) return 1;
                if (findDateClosest.length < 2) return findDateClosest[0].ty_gia;
                if (findDateClosest.length > 1) return findDateClosest[findDateClosest.length - 1].ty_gia
            } else { return null }
        }
        return null
    }

    function PHCT(ma_nt, ngay_ct) {
        const tygia = ma_nt === "VND" ? 1 : getTyGia(ma_nt, ngay_ct);
        const totalPH = {
            ma_nt: ma_nt,
            ty_gia: tygia,
            ngay_ct: ngay_ct,
            t_tien: 0,
            t_tien_nt: 0,
        }
        const newCT = ct.map(item => {
            // tính CT
            if (ma_nt === 'VND') {
                item.ps_co = Math.round(item.ps_co_nt)
                item.ps_no = Math.round(item.ps_no_nt)

                item.ps_co_nt = item.ps_co
                item.ps_no_nt = item.ps_no
            } else {
                item.ps_co = Math.round(item.ps_co_nt * tygia)
                item.ps_no = Math.round(item.ps_no_nt * tygia)
            }

            // tính PH
            totalPH.t_tien_nt += VoucherHelper.f_NullToZero(item.ps_co_nt)
            totalPH.t_tien += VoucherHelper.f_NullToZero(item.ps_co)
            return item
        });
        onUpdatePHCT(totalPH, newCT)
    }
    const handleChangeDatect = (e) => {
        const { ma_nt } = formik.values
        PHCT(ma_nt, e.value)
    }

    const handleChangeNT = ({ ma_nt, ty_gia }) => {
        const totalPH = {
            ma_nt: ma_nt,
            ty_gia: ty_gia,
            t_tien: 0,
            t_tien_nt: 0,
        }
        const newCT = ct.map(item => {
            // tính CT
            if (ma_nt === 'VND') {
                item.ps_co = Math.round(item.ps_co_nt)
                item.ps_no = Math.round(item.ps_no_nt)

                item.ps_co_nt = item.ps_co
                item.ps_no_nt = item.ps_no
            } else {
                item.ps_co = Math.round(item.ps_co_nt * ty_gia)
                item.ps_no = Math.round(item.ps_no_nt * ty_gia)
            }

            // tính PH
            totalPH.t_tien_nt += VoucherHelper.f_NullToZero(item.ps_co_nt)
            totalPH.t_tien += VoucherHelper.f_NullToZero(item.ps_co)
            return item
        });
        onUpdatePHCT(totalPH, newCT)
    }

    return <React.Fragment>
        <Grid columns="3">
            <RowItemPH
                content={[
                    {
                        width: 6, input: <ZenField readOnly={isReadOnly}
                            label={"gl1.dien_giai"} defaultlabel="Diễn giải"
                            name="dien_giai" props={formik}
                        />
                    },
                    {
                        width: 6, input: <ZenField required readOnly={isReadOnly}
                            label={"gl1.so_ct"} defaultlabel="Số chứng từ"
                            name="so_ct" props={formik}
                        />
                    },
                    {
                        width: 4, input: <ZenFieldDate required readOnly={isReadOnly}
                            label={"gl1.ngay_ct"} defaultlabel="Ngày chứng từ"
                            name="ngay_ct" props={formik}
                            onChange={handleChangeDatect}
                        />
                    }
                ]} />

            <RowItemPH content={[
                { width: 6, input: null },
                { width: 6, input: null },

            ]} />

            <RowItemPH content={[
                { width: 6, input: null },
                { width: 6, input: null },
                {
                    width: 4, input: <FieldNT formik={formik} onChangeNT={handleChangeNT} />
                }
            ]} />
        </Grid>
    </React.Fragment>

}

const CTForm = ({ ph, ct, permission, modeForm }) => {
    const { onChangeCT, errorCT, getItemFormik, itemSiDmCt } = useContext(ContextVoucher)
    let isReadOnly = VoucherHelper.f_ReadOnly(modeForm, permission)

    const switchAction = (propsElement, type, itemCurrent, index) => {
        const { name, value, itemSelected } = propsElement
        const currentPH = getItemFormik()?.values

        switch (type) {
            case ActionType.TEXT_CHANGE:
                const { lookup } = propsElement
                // set tên nếu là lookup
                // => cần để khởi tạo item cho input lookup, nếu hiển thị nhìu hơn 1 trường mã
                if (lookup) {
                    if (name === 'ma_kh') {
                        itemCurrent['ten_kh'] = itemSelected['ten_kh']
                    } else if (name === 'tk') {
                        itemCurrent['ten_tk'] = itemSelected['ten_tk']
                    }
                }
                // set value 
                itemCurrent[name] = value
                onChangeCT(itemCurrent, index, type, {}, propsElement)
                break;

            case ActionType.NUMBER_CHANGE:
                f_calcNumber(propsElement, type, itemCurrent, index)
                break;

            case ActionType.ADD_ROW:
                let newCT = ({
                    tk: value,
                    ten_tk: itemSelected.ten_tk,
                    ps_co: ct.length === 0 ? "" : f_calcNumberNoCo(ct, false, true),
                    ps_co_nt: ct.length === 0 ? "" : f_calcNumberNoCo(ct, true, true),
                    ps_no: ct.length === 0 ? "" : f_calcNumberNoCo(ct, false, false),
                    ps_no_nt: ct.length === 0 ? "" : f_calcNumberNoCo(ct, true, false),
                    //TODO : Để tạm 
                    opt1: "2020-08-21",
                    opt2: "2020-08-21",
                })
                if (ct.length === 0) {
                    newCT.dien_giai = currentPH.dien_giai
                }
                onChangeCT(newCT, null, type)
                break;

            case ActionType.DELETE_ROW:
                // xóa dòng CT, tính lại PH
                const totalPH = {
                    t_tien: currentPH.t_tien - VoucherHelper.f_NullToZero(itemCurrent.ps_co),
                    t_tien_nt: Number((currentPH.t_tien_nt - VoucherHelper.f_NullToZero(itemCurrent.ps_co_nt)).toFixed(2)),
                    t_tt: currentPH.t_tien - VoucherHelper.f_NullToZero(itemCurrent.ps_co)
                }
                onChangeCT(itemCurrent, index, type, totalPH)
                break;
            default:
                console.log(type)
                break;
        }
    }

    function f_calcNumberNoCo(ct, is_Nt, is_ps_no) {
        let tongno = 0;
        let tongco = 0;
        let giatrino = 0;
        let giatrico = 0;
        ct.map(item => {
            if (is_Nt) {
                tongno += item.ps_no_nt;
                tongco += item.ps_co_nt;
            } else {
                tongno += item.ps_no;
                tongco += item.ps_co;
            }
        })
        if (tongno > tongco)
            giatrino = tongno - tongco;
        else if (tongno < tongco)
            giatrico = tongco - tongno;
        else {
            giatrico = 0;
            giatrino = 0;
        }
        if (is_ps_no)
            return giatrino;
        else
            return giatrico
    }

    function f_calcNumber(propsElement, type, itemCurrent, index) {
        const { name, value } = propsElement
        const currentPH = getItemFormik()?.values

        if (itemCurrent[name] !== value) {
            itemCurrent[name] = value

            if (currentPH.ma_nt !== 'VND') {
                if (name === 'ps_co_nt') {
                    itemCurrent['ps_co'] = Math.round(value * currentPH.ty_gia)
                } else if (name === 'ps_co') {
                    itemCurrent['ps_co_nt'] = Number((value / currentPH.ty_gia).toFixed(2))
                } else if (name === 'ps_no_nt') {
                    itemCurrent['ps_no'] = Math.round(value * currentPH.ty_gia)
                } else if (name === 'ps_no') {
                    itemCurrent['ps_no_nt'] = Number((value / currentPH.ty_gia).toFixed(2))
                }
            } else {
                if (name === 'ps_co') {
                    itemCurrent['ps_co_nt'] = value
                }
                else if (name === 'ps_no') {
                    itemCurrent['ps_no_nt'] = value
                }
            }
            //Cập nhật chỉ nhập nợ hoặc có
            if (name === 'ps_co' || name === 'ps_co_nt') {
                itemCurrent['ps_no'] = 0
                itemCurrent['ps_no_nt'] = 0
            } else if (name === 'ps_no' || name === 'ps_no_nt') {
                itemCurrent['ps_co'] = 0
                itemCurrent['ps_co_nt'] = 0
            }
            // cập nhật PH, CT
            VoucherHelper.f_Timeout(() => {
                const totalPH = {
                    t_tien: 0,
                    t_tien_nt: 0,
                    t_tt: 0,
                }
                ct.map((item, idx) => {
                    if (index === idx) {
                        totalPH.t_tien_nt += VoucherHelper.f_NullToZero(itemCurrent.ps_co_nt)
                        totalPH.t_tien += VoucherHelper.f_NullToZero(itemCurrent.ps_co)
                        totalPH.t_tt = totalPH.t_tien
                    } else {
                        totalPH.t_tien_nt += VoucherHelper.f_NullToZero(item.ps_co_nt)
                        totalPH.t_tien += VoucherHelper.f_NullToZero(item.ps_co)
                        totalPH.t_tt = totalPH.t_tien
                    }
                })
                onChangeCT(itemCurrent, index, type, totalPH, propsElement)
            })
        }
    }

    return <TableScroll>
        <Table.Header>
            <RowHeaderCell colAction={isReadOnly}
                itemSiDmCt={itemSiDmCt}
                header={[
                    { text: ['gl1.tk', 'TK'] },
                    { text: ['gl1.ps_no', 'PS nợ'], maNT: ph.ma_nt, isNT: true },
                    { text: ['gl1.ps_no', 'PS nợ'], maNT: 'VND' },
                    { text: ['gl1.ps_co', 'PS có'], maNT: ph.ma_nt, isNT: true },
                    { text: ['gl1.ps_co', 'PS có'], maNT: 'VND' },
                    { text: ['ardmkh.header', 'Khách hàng'] },
                    { text: ['gl1.dien_giai', 'Diễn giải'] },
                    { text: ['gl1.nh_dk', 'Nhóm DK'] },
                ]}
            />
        </Table.Header>
        <Table.Body>
            {
                ct && ct.map((item, index) => {
                    const error = errorCT ? errorCT.find(x => x.index === index) : undefined

                    return <Table.Row key={item.stt_rec0 ? item.stt_rec0 : ('REC' + index)}>
                        <SelectCell lookup={{
                            ...ZenLookup.TK,
                            where: 'chi_tiet = 1',
                            onLocalWhere: (items) => {
                                return items.filter(t => t.chi_tiet == true)
                            }
                        }} error={error}
                            name="tk" rowItem={item} index={index}
                            placeholder={['gl1.tk', 'TK']}
                            readOnly={isReadOnly}
                            onChange={switchAction}
                        />

                        {ph.ma_nt !== "VND" && <NumberCell name="ps_no_nt"
                            rowItem={item} index={index} decimalScale={2}
                            placeholder={["gl1.ps_no", "PS có", { ma_nt: ph.ma_nt }]}
                            onChange={switchAction} error={error}
                            readOnly={isReadOnly}
                            // set focus khi thêm mới
                            autoFocus={(ph.ma_nt !== 'VND' && item.autoFocus && ct.length === index + 1) ? true : false}
                        />
                        }

                        <NumberCell name="ps_no"
                            rowItem={item} index={index}
                            placeholder={["gl1.ps_no", "PS nợ", { ma_nt: 'VND' }]}
                            onChange={switchAction} error={error}
                            readOnly={isReadOnly}
                            // set focus khi thêm mới
                            autoFocus={(ph.ma_nt === 'VND' && item.autoFocus && ct.length === index + 1) ? true : false}
                        />

                        {ph.ma_nt !== "VND" && <NumberCell name="ps_co_nt"
                            rowItem={item} index={index} decimalScale={2}
                            placeholder={["gl1.ps_co", "PS có", { ma_nt: ph.ma_nt }]}
                            onChange={switchAction} error={error}
                            readOnly={isReadOnly}
                        />
                        }

                        <NumberCell name="ps_co"
                            rowItem={item} index={index}
                            placeholder={["gl1.ps_co", "PS có", { ma_nt: 'VND' }]}
                            onChange={switchAction} error={error}
                            readOnly={isReadOnly}
                        />

                        <SelectCell
                            lookup={{ ...ZenLookup.Ma_kh, formatInput: "{ma_kh}" }}
                            name="ma_kh" rowItem={item} index={index}
                            placeholder={['ardmkh.header', 'Khách hàng']}
                            onChange={switchAction} error={error}
                            readOnly={isReadOnly}
                        />

                        <TextCell name="dien_giai"
                            rowItem={item} index={index}
                            placeholder={['gl1.dien_giai', 'Diễn giải']}
                            onChange={switchAction}
                            readOnly={isReadOnly}
                        />

                        <TextCell name="nh_dk"
                            width="100px"
                            rowItem={item} index={index}
                            placeholder={['gl1.nh_dk', 'Nhóm định khoản']}
                            onChange={switchAction} error={error}
                            readOnly={isReadOnly}
                        />

                        <ConfigBySiDmCt
                            rowItem={item} index={index}
                            itemSiDmCt={itemSiDmCt}
                            readOnly={isReadOnly}
                            switchAction={switchAction}
                        />

                        {isReadOnly === false && <DeleteCell collapsing
                            rowItem={item} index={index}
                            onClick={switchAction} />}
                    </Table.Row>
                })
            }

            {isReadOnly === false && <Table.Row>
                <SelectCell isAdd clearable={false} name="addRow"
                    lookup={{
                        ...ZenLookup.TK,
                        where: 'chi_tiet = 1',
                        onLocalWhere: (items) => {
                            return items.filter(t => t.chi_tiet == true)
                        }
                    }}
                    placeholder={['gl1.tk', 'TK']}
                    onChange={switchAction}
                />
                <Table.Cell colSpan={10} />
            </Table.Row>}
        </Table.Body>
    </TableScroll>
}

const CTFormTotal = ({ ph }) => {
    return <Grid columns="2">
        <Grid.Column width="8" />
        <Grid.Column width="8" textAlign="right">
            <TableTotalPH maNt={ph.ma_nt}>
                <RowTotalPH text="Tổng tiền" value={ph.t_tien} valueNT={ph.t_tien_nt} maNt={ph.ma_nt} />
            </TableTotalPH>
        </Grid.Column>
    </Grid>
}

const onValidCT = (ph, ct, currentRowCT) => {
    const { item, index } = currentRowCT
    const errorField = {}
    var nhom_dk = []
    var dk = false
    if (!item.tk) {
        errorField.tk = ["Tài khoản không được để trống"]
    }
    if (!item.nh_dk) {
        errorField.nh_dk = ["nhóm điều khoản không được để trống"]
    }
    if (VoucherHelper.f_NullToZero(item.ps_co) === 0 && VoucherHelper.f_NullToZero(item.ps_no) === 0) {
        errorField.ps_co = ["Giá trị phải lớn hơn 0"]
        errorField.ps_no = ["Giá trị phải lớn hơn 0"]
    }
    if (ph.ma_nt !== 'VND' && VoucherHelper.f_NullToZero(item.ps_co_nt) === 0 && VoucherHelper.f_NullToZero(item.ps_no_nt) === 0) {
        errorField.ps_co_nt = ["Giá trị phải lớn hơn 0"]
        errorField.ps_no_nt = ["Giá trị phải lớn hơn 0"]
    }

    ct.forEach((itemCT) => {
        nhom_dk.forEach((nhom_ct) => { if (itemCT.nh_dk == nhom_ct.ma_nh_dk) { dk = true } })
        if (dk === false) {
            nhom_dk.push({ ma_nh_dk: itemCT.nh_dk, tong_co: 0, tong_no: 0, so_tk_co: 0, so_tk_no: 0, })
        }

        dk = false
        nhom_dk.forEach((nhom_ct, idx) => {
            if (itemCT.nh_dk == nhom_ct.ma_nh_dk) {
                if (ph.ma_nt === "VND") {
                    nhom_ct.tong_co += VoucherHelper.f_NullToZero(itemCT.ps_co)
                    nhom_ct.tong_no += VoucherHelper.f_NullToZero(itemCT.ps_no)
                } else {
                    nhom_ct.tong_co += VoucherHelper.f_NullToZero(itemCT.ps_co_nt)
                    nhom_ct.tong_no += VoucherHelper.f_NullToZero(itemCT.ps_no_nt)
                }
                VoucherHelper.f_NullToZero(itemCT.ps_co) === 0 ? nhom_ct.so_tk_co : nhom_ct.so_tk_co += 1
                VoucherHelper.f_NullToZero(itemCT.ps_no) === 0 ? nhom_ct.so_tk_no : nhom_ct.so_tk_no += 1
            }
        })
    })

    nhom_dk.forEach((itemDk) => {
        if (itemDk.tong_no != itemDk.tong_co) {
            const messErr = ["Phát sinh nợ khác phát sinh có trong nhóm định khoản: " + itemDk.ma_nh_dk]
            if (ph.ma_nt === "VND") {
                if (item.ps_no) {
                    errorField.ps_no = messErr
                } else {
                    errorField.ps_co = messErr
                }
            } else {
                if (item.ps_no) {
                    errorField.ps_no_nt = messErr
                } else {
                    errorField.ps_co_nt = messErr
                }
            }
        }
        if (itemDk.so_tk_no >= 2 && itemDk.so_tk_co >= 2) {
            errorField.nh_dk = ["Có hạch toán nhiều có/nợ trong nhóm định khoản: " + itemDk.ma_nh_dk]
        }
    })

    return Object.keys(errorField).length > 0 ? errorField : undefined
}

export const GLVchGL1Edit = {
    PHForm: PHForm,
    CTForm: CTForm,
    CTFormTotal: CTFormTotal,
    onValidCT: onValidCT,
    ma_ct: 'GL1',

    route: {
        add: routes.GLVchGL1New,
        edit: routes.GLVchGL1Edit()
    },
    linkHeader: {
        id: "glvchgl1",
        defaultMessage: "Phiếu kế toán tổng hợp",
        route: routes.GLVchGL1,
        active: false,
    },
    formId: "GLVchGL1",
    api: {
        url: ApiGLVchGL1,
    },
    action: {
        view: { visible: true, permission: "02.02.1" },
        add: { visible: true, permission: "02.02.2" },
        edit: { visible: true, permission: "02.02.3" },
        del: { visible: true, permission: "02.02.4" }
    },
    initItem: {
        stt_rec: "", so_ct: "", ma_ct: "GL1", ma_nt: "VND", ty_gia: 1,
        ngay_ct: ZenHelper.formatDateTime(new Date(), "YYYY-MM-DD"),
        ngay_lct: ZenHelper.formatDateTime(new Date(), "YYYY-MM-DD"),
        trang_thai: "", t_tien: null, t_tien_nt: null, t_tt: null,
        //TODO : Để tạm
        opt1: "2020-08-21", opt2: "2020-08-21"
    },
    formValidation: [
        {
            id: "ngay_ct",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ]
        },
        {
            id: "so_ct",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ],
        },
    ]
}
