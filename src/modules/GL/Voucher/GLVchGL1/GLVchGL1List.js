﻿import React, { useEffect, useRef, useState } from "react";
import { Button, Form, Modal } from "semantic-ui-react";
import {
  ZenButton,
  ZenDatePeriod,
  ZenField,
  ZenFormik,
} from "../../../../components/Control";
import * as routes from "../../../../constants/routes";
import { ZenHelper } from "../../../../utils/global";
import { ApiGLVchGL1 } from "../../Api/index";
import { GlobalStorage, KeyStorage } from "../../../../utils/storage";

const globalStorage = GlobalStorage.get(KeyStorage.Global);
const initItem = {
  ngay1: globalStorage?.from_date,
  ngay2: globalStorage?.to_date,
}

const FindAdvanced = ({ onClose, onSearch, initFilter }) => {
  const refFormik = useRef();

  const handleSearch = (values) => {
    const _sql = convertToSql(values);
    onSearch(_sql, values);
  };

  const handleChangeDate = ({ startDate, endDate }) => {
    refFormik.current.setValues({
      ...refFormik.current.values,
      ngay1: startDate,
      ngay2: endDate,
    });
  };

  return (
    <>
      <Modal.Header>Tìm kiếm</Modal.Header>
      <Modal.Content>
        <ZenFormik
          ref={refFormik}
          //validation={[]}
          initItem={{ ...initItem, ...initFilter }}
          onSubmit={handleSearch}
        >
          {(formikProps) => {
            const { values } = formikProps;
            return (
              <Form>
                <ZenDatePeriod
                  onChange={handleChangeDate}
                  value={[values.ngay1, values.ngay2]}
                  textLabel="Từ ngày - đến ngày"
                  defaultPopupYear={ZenHelper.getFiscalYear()}
                />
                <Form.Group widths="equal">
                  <ZenField
                    label={"gl1.so_ct1"}
                    defaultlabel="Số chứng từ"
                    name="so_ct1"
                    props={formikProps}
                  />
                  <ZenField
                    label={"gl1.so_ct2"}
                    defaultlabel="Đến số"
                    name="so_ct2"
                    props={formikProps}
                  />
                </Form.Group>
                <ZenField
                  label={"gl1.dien_giai"}
                  defaultlabel="Diễn giải"
                  name="dien_giai"
                  props={formikProps}
                />
              </Form>
            );
          }}
        </ZenFormik>
      </Modal.Content>
      <Modal.Actions>
        <ZenButton btnType={"cancel"} size="small" onClick={onClose} />
        <Button
          content="Tìm"
          icon="search"
          size="small"
          primary
          onClick={(e) => refFormik.current.handleSubmit(e)}
        />
      </Modal.Actions>
    </>
  );
};

function convertToSql(item) {
  let _sql = "";
  if (item.ngay1) _sql += ` AND ngay_ct >= '${item.ngay1}'`;
  if (item.ngay2) _sql += ` AND ngay_ct <= '${item.ngay2}'`;
  if (item.so_ct1) _sql += ` AND so_ct >= '${item.so_ct1}'`;
  if (item.so_ct2) _sql += ` AND so_ct <= '${item.so_ct2}'`;
  if (item.dien_giai) _sql += ` AND dien_giai LIKE '%${item.dien_giai}%'`;
  return _sql.replace("AND", "");
}

export const GLVchGL1List = {
  route: routes.GLVchGL1,

  action: {
    view: { visible: true, permission: "02.02.1" },
    add: { visible: true, permission: "02.02.2", link: { route: routes.GLVchGL1New } },
    edit: { visible: true, permission: "02.02.3", link: { route: routes.GLVchGL1Edit(), params: "stt_rec" } },
    del: { visible: true, permission: "02.02.4" },
  },

  linkHeader: {
    id: "glvchgl1",
    defaultMessage: "Phiếu kế toán tổng hợp",
    active: true,
  },

  tableList: {
    findComponent: {
      FindForm: FindAdvanced,
      convertToSql: convertToSql,
      labels: [
        { name: "ngay1", text: "Từ ngày", type: "date" },
        { name: "ngay2", text: "Đến ngày", type: "date" },
        { name: "so_ct1", text: "Từ số" },
        { name: "so_ct2", text: "Đến số" },
        { name: "dien_giai", text: "Diễn giải" },
      ],
    },

    unPagination: false,
    fieldCode: "stt_rec",
    ma_ct: "GL1",

    api: {
      url: ApiGLVchGL1,
      type: "sql",
    },

    columns: [
      {
        id: "gl1.ngay_ct",
        defaultMessage: "Ngày CTừ",
        fieldName: "ngay_ct",
        type: "date",
        filter: "date",
        sorter: true,
      },
      {
        id: "gl1.ngay_lct",
        defaultMessage: "Ngày lập",
        fieldName: "ngay_lct",
        type: "date",
        filter: "date",
        sorter: true,
      },
      {
        id: "gl1.so_ct",
        defaultMessage: "Số CTừ",
        fieldName: "so_ct",
        filter: "string",
        sorter: true,
        link: { route: routes.GLVchGL1Edit(), params: "stt_rec" },
      },
      {
        id: "gl1.t_tt",
        defaultMessage: "Tổng tiền",
        fieldName: "t_tt",
        type: "number",
        filter: "number",
        sorter: true,
      },
      {
        id: "gl1.dien_giai",
        defaultMessage: "Diễn giải",
        fieldName: "dien_giai",
        type: "string",
        filter: "string",
        sorter: true,
      },
      {
        id: "gl1.ma_nt",
        defaultMessage: "Mã NT",
        fieldName: "ma_nt",
        filter: "string",
        sorter: true,
      },
    ],
  },
};
