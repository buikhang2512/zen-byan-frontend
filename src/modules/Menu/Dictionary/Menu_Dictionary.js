import React, { useEffect, useState } from 'react';
import * as routes from "../../../constants/routes";
import { ApiMenus} from "../Api/index";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { ZenField, ZenLink } from "../../../components/Control";
import { Table } from "semantic-ui-react";
import { MenuFilter } from './MenuFilter';

const Menus_Dictionary = {
   Menu: {
      route: routes.Menus,

      action: {
         view: { visible: true, permission: "" },
         add: { visible: true, permission: "" },
         edit: { visible: true, permission: "" },
         del: { visible: true, permission: "" },
      },

      linkHeader: {
         id: "menus",
         defaultMessage: "Menu",
         active: true,
         isSetting: true,
      },

      tableList: {
         ContainerTop: MenuFilter,
         keyForm: "menus",
         formModal: ZenLookup.Menus,
         fieldCode: "id",
         unPagination: false,
         duplicate: true,

        //  hasChecked: true,
         // onUpdateBulk: (items) => {

         // },

         api: {
            url: ApiMenus,
            type: "sql",
         },
         columns: [
            { id: "menus.module_id", defaultMessage: "ModuleId", fieldName: "module_id", type: "string", filter: "string", sorter: true, },
            { id: "menus.menu_id", defaultMessage: "MenuId", fieldName: "id", type: "string", filter: "string", sorter: true, editForm: true},
            { id: "menus.parent_id", defaultMessage: "ParentId", fieldName: "parent_id", type: "string", filter: "string", sorter: true, },
            { id: "menus.permission", defaultMessage: "Permission", fieldName: "permission", type: "string", filter: "string", sorter: true, },
            { id: "menus.name", defaultMessage: "Name", fieldName: "name", type: "string", filter: "string", sorter: true, },
            { id: "menus.url", defaultMessage: "Url", fieldName: "url", type: "string", filter: "string", sorter: true, },
            { id: "menus.icon", defaultMessage: "Icon", fieldName: "icon", type: "string", filter: "string", sorter: true, },
            { id: "menus.ordinal", defaultMessage: "Ordinal", fieldName: "ordinal", type: "string", filter: "string", sorter: true, },
            { id: "menus.isinactive", defaultMessage: "Isinactive", fieldName: "isinactive", type: "bool", filter: "bool", sorter: true, },
            { id: "menus.is_group", defaultMessage: "IsGroup", fieldName: "is_group", type: "bool", filter: "bool", sorter: true, },
            { id: "menus.description", defaultMessage: "Descripttion", fieldName: "description", type: "string", filter: "string", sorter: true, },
            //{ id: "ksd", defaultMessage: "Không sử dụng", fieldName: "ksd", type: "bool", filter: "", sorter: true, collapsing: true, propsCell: { textAlign: "center" } },
         ],
      },
   },
}

export {Menus_Dictionary}