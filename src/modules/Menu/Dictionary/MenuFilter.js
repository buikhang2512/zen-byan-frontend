import React, { useEffect, useMemo, useRef, useState } from "react";
import { Accordion, Button, Form, Icon } from "semantic-ui-react";
import { ref } from "yup";
import { ApiModule } from "../../../Api";
import { ZenLoading,ZenFormik, ZenFieldSelect, ZenFieldSelectApi, InputDatePeriod, ZenDatePeriod } from "../../../components/Control/index";
import { zzControlHelper } from "../../../components/Control/zzControlHelper";
import { ZenHelper } from "../../../utils";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";

export const MenuFilter = (props) => {
    const { onLoadData, fieldCode, onAfterLoadData } = props;
    const refForm = useRef();
    const [expand, setExpand] = useState(0);
    const [modulesOpt, setModulesOpt] = useState("");
    const [loadingForm,setLoadingForm] = useState(false);
    const [module, setModule] = useState("");

    useEffect(() => {
        ApiModule.get((res) => {
            if (res.status == 200) {
              setModulesOpt(
                ZenHelper.convertToSelectOptions(
                  res.data.data,
                  true,
                  "id",
                  "id",
                  "name"
                )
              );
            }
          });
        onLoadData(``);
    }, []);


    const handleSubmit = async(item, { name }) => {
        const newItem = { ...refForm.current.values,  [name] : item.value,}
        const strSql = createStrSql(newItem)
        onLoadData(strSql, newItem);
        setLoadingForm(false)
    }

    function createStrSql(item = {}) {
        let _sql = "";
        if (item.ngay1) _sql += ` AND ngay >= '${item.ngay1}'`;
        if (item.ngay2) _sql += ` AND ngay <= '${item.ngay2}'`;
        if (item.id_nv) _sql += `AND id_nv = '${item.id_nv}'`;
        if (item.module_id) _sql += `AND module_id = "${item.module_id}"`;

        return _sql.replace("AND", "")
    }
    
    return <>
        {/* <Accordion fluid styled>
            <Accordion.Title
                active={expand === 0}
                index={0}
                onClick={(e, { index }) => setExpand(expand === index ? -1 : index)}
            >
                <Icon name="dropdown" />
                Điều kiện lọc
            </Accordion.Title>
            <Accordion.Content active={expand === 0}> */}
        <ZenFormik form={"Menu-filter"}
            ref={refForm}
            validation={[]}
            initItem={initItem}
            onSubmit={handleSubmit}
            onReset={(item) => onLoadData("")}
        >
            {
                formik => {
                    const { values } = formik
                    return <Form size="small">
                        <Form.Group widths="4">
                            <ZenFieldSelect
                                options={modulesOpt}
                                label="Menu.id_nv" defaultlabel="Module"
                                name="module_id"
                                props={formik}
                                onSelectedItem={handleSubmit}
                            />
                        </Form.Group>
                    </Form>
                }
            }
        </ZenFormik>
        
        {/* <Button primary
            content="Reset"
            size="small" icon="refresh"
            onClick={(e) => refForm.current.handleReset(e)}
        /> */}
        {/* </Accordion.Content>
        </Accordion> */}
    </>
}

const initItem = {
    module_id: "",
}