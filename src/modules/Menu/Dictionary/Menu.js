import React, { useCallback, useEffect, useState } from "react";
import { Form } from "semantic-ui-react";
import { ZenField, ZenFieldDate, ZenFieldNumber, ZenFieldSelectApi, ZenFieldTextArea, ZenButton, ZenFieldCheckbox } from "../../../components/Control/index";
import { FormMode, zzControlHelper } from "../../../components/Control/zzControlHelper";
import { ZenHelper } from "../../../utils";
import { IntlFormat } from "../../../utils/intlFormat";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { ApiMenus } from "../Api";
import * as routes from '../../../constants/routes';

const MenuModal = (props) => {
    const { formik, permission, mode } = props
    return <>
        <Form.Group>
            <label></label>
            <ZenField required readOnly={!permission}
                label="menu.id" defaultlabel="MenuId"
                name="id"
                props={formik}
            />
            <ZenField readOnly={!permission}
                label="menu.parent_id" defaultlabel="ParentId"
                name="parent_id"
                props={formik}
            />
            <ZenField readOnly={!permission}
                label="menu.permission_id" defaultlabel="PermissionId"
                name="permission_id"
                props={formik}
            />
            <ZenField required readOnly={!permission}
                label="menu.module_id" defaultlabel="ModuleId"
                name="module_id"
                props={formik}
            />
        </Form.Group>
        <Form.Group>
            <ZenField required readOnly={!permission} width={16}
                label="menu.name" defaultlabel="Name"
                name="name"
                props={formik}
            />
        </Form.Group>
        <Form.Group>
            <ZenField readOnly={!permission} width={16}
                label="menu.url" defaultlabel="Url"
                name="url"
                props={formik}
            />
        </Form.Group>
        <Form.Group>
            <ZenField readOnly={!permission} width={16}
                label="menu.icon" defaultlabel="Icon"
                name="icon"
                props={formik}
            />
        </Form.Group>
        <Form.Group>
            <ZenFieldNumber readOnly={!permission}
                label="menu.ordinal" defaultlabel="Ordinal"
                name="ordinal"
                props={formik}
            />
            <Form.Field>
                <label>&nbsp;</label>
                <ZenFieldCheckbox readOnly={!permission}
                    label="menu.is_group" defaultlabel="Is Group"
                    name="is_group"
                    props={formik}
                />
            </Form.Field>
            <Form.Field>
                <label>&nbsp;</label>
                <ZenFieldCheckbox readOnly={!permission}
                    label="menu.isinactive" defaultlabel="Is Inactive"
                    name="isinactive"
                    props={formik}
                />
            </Form.Field>
        </Form.Group>
        <Form.Group>
            <ZenField readOnly={!permission} width={16}
                label="menu.description" defaultlabel="Description"
                name="description"
                props={formik}
            />
        </Form.Group>
    </>
}

export const Menu = {
    FormModal: MenuModal,

    api: {
        url: ApiMenus,
    },

    permission: {
        view: "",
        add: "",
        edit: ""
    },
    fieldCode: "id",
    formId: "menus-form",
    size: "small",
    initItem: {
        id: "",
        permission_id: "",
        ordinal: 0,
        url: "",
        name: "",
        icon: "",
        description: "",
        module_id: "",
        module_name: "",
        module_ordinal: "",
        parent_id: ""
    },
    formValidation: [
        {
            id: "module_id",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ]
        },
        {
            id: "id",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ]
        },
        {
            id: "name",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ]
        },
    ]
}