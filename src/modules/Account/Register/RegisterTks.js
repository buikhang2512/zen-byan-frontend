import React from "react";
import {
   Button,
   Segment,
   Header,
   Grid,
   Image,
} from "semantic-ui-react";
import { Link } from "react-router-dom";
import * as routes from '../../../constants/routes'

const RegisterTks = (props) => {

   return (
      <React.Fragment>
         <Grid
            textAlign="center"
            style={{
               height: "100vh",
               backgroundImage: `url("/static/media/bg-login.jpg")`,
               backgroundSize: "cover",
               backgroundPosition: "left top",
               backgroundRepeat: "no-repeat",
             }}
            verticalAlign="middle"
         >
            <Grid.Column style={{ maxWidth: 600 }}>
            <Image src='/static/media/logo.png' style={{ maxWidth: 250 }} centered />

               <Segment textAlign="left">                  

                  <Header as="h3" textAlign="center" content="Cám ơn bạn đã đăng ký." />

                  <p>Chúng tôi sẽ khởi tạo tài khoản và gửi thông tin truy cập hệ thống thông qua email quý khách đã đăng ký. Vui lòng kiểm tra email để nhận được thông tin cần thiết.</p>
                  <p>Một số liên kết hữu ích cho bạn:</p>
                  <ul>
                     <li><a href="https://zentech.vn/books/help/index.html" target="blank">Hướng dẫn sử dụng phần mềm</a></li>
                     <li>Yêu cầu hỗ trợ:<a href="mailto:support@zentech.vn"> support@zentech.vn</a></li>
                     <li>Gọi điện tư vấn trực tiếp:<a href="tel:02836220101"> 028 3622 0101</a></li>
                  </ul>

                  <div style={{ textAlign: "center" }}>
                     <Button primary icon="sign-in" content="Quay về trang đăng nhập"
                        as={Link} to={routes.Login}
                     />
                  </div>
               </Segment>
            </Grid.Column>
         </Grid>
      </React.Fragment >
   );
}

export default RegisterTks