import React, { Component } from "react";
import {
   Button,
   Form,
   Segment,
   Header,
   Grid,
   Divider,
   Image,
   Message
} from "semantic-ui-react";
import _ from 'lodash'
import { ZenField } from "components/Control";
import { ApiAccount } from "../Api/ApiAccount";
import { Link } from "react-router-dom";
import * as routes from '../../../constants/routes'
import { ZenFormik } from "../../../components/Control";
import { ZenHelper } from "../../../utils";

class Register extends Component {
   constructor(props) {
      super();
      this.refForm = React.createRef();
      this.state = {
         error: null,
         items: initItem,
         btnLoading: false
      };
   }

   handleRegister = (values) => {
      // e.preventDefault();
      this.setState({ btnLoading: true })
      this.accountRegister(values)
   }

   accountRegister(item) {
      ApiAccount.register(item, res => {
         if (res.status === 204) {
            this.props.history.push(routes.RegisterTks)
         } else {
            this.setState({
               btnLoading: false,
               error: ZenHelper.getResponseError(res)
            });
         }
      })
   }

   render() {
      const { error, btnLoading } = this.state;

      return (
         <React.Fragment>
            <Grid
               textAlign="center"
               style={{
                  height: "100vh",
                  backgroundImage: `url("/static/media/bg-login.jpg")`,
                  backgroundSize: "cover",
                  backgroundPosition: "left top",
                  backgroundRepeat: "no-repeat",
                }}
               verticalAlign="middle"
            >
               <Grid.Column style={{ maxWidth: 450 }}>
                  <Image src='/static/media/logo.png' style={{ maxWidth: 250 }} centered />

                  <Segment textAlign="left">
                     <Header as='h3' className="centered" >
                        Đăng ký sử dụng                  
                     </Header>

                     <Divider />
                     {error && <Message negative list={error} />}
                     <ZenFormik form={"register-form"}
                        ref={this.refForm}
                        validation={formValidation}
                        initItem={initItem}
                        onSubmit={this.handleRegister}
                     >
                        {
                           formikProps => {
                              return <Form>
                                 <ZenField
                                    label={"register.full_name"}
                                    defaultlabel="Họ và tên"
                                    name="full_name"
                                    props={formikProps}
                                 />
                                 <ZenField
                                    label={"login.email"}
                                    defaultlabel="Email"
                                    name="email"
                                    props={formikProps}
                                 />

                                 <ZenField
                                    label={"register.tel"}
                                    defaultlabel="Điện thoại"
                                    name="tel"
                                    props={formikProps}
                                 />

                                 <ZenField
                                    label={"register.business_name"}
                                    defaultlabel="Công ty"
                                    name="business_name"
                                    props={formikProps}
                                 />

                                 <ZenField
                                    label={"register.dia_chi"}
                                    defaultlabel="Địa chỉ"
                                    name="dia_chi"
                                    props={formikProps}
                                 />

                                 <ZenField
                                    label={"register.business_name"}
                                    defaultlabel="Mã số thuế"
                                    name="ma_so_thue"
                                    props={formikProps}
                                 />
                              </Form>
                           }
                        }
                     </ZenFormik>

                     <br />
                     <Button form="register-form"
                        loading={btnLoading} disabled={btnLoading}
                        fluid primary type="submit" size="small"
                        onClick={(e) => this.refForm.current.handleSubmit()}
                        content="Đăng ký sử dụng" />

                     <Divider />
                     <div>
                        Nếu đã có tài khoản, thực hiện
                        <Link to={routes.Login}> Đăng nhập vào hệ thống.</Link>
                     </div>
                  </Segment>
               </Grid.Column>
            </Grid>
         </React.Fragment >
      );
   }
}

const initItem = {
   email: "",
   tel: "",
   full_name: "",
   business_name: ""
};


const formValidation = [
   {
      id: "full_name",
      validationType: "string",
      validations: [
         {
            type: "required",
            params: ["Không được bỏ trống trường này"]
         },
      ]
   },
   {
      id: "email",
      validationType: "string",
      validations: [
         {
            type: "required",
            params: ["Không được bỏ trống trường này"]
         },
         {
            type: "email",
            params: ["Email không đúng định dạng"]
         }
      ]
   },
   {
      id: "business_name",
      validationType: "string",
      validations: [
         {
            type: "required",
            params: ["Không được bỏ trống trường này"]
         },
      ]
   },
   {
      id: "tel",
      validationType: "string",
      validations: [
         {
            type: "required",
            params: ["Không được bỏ trống trường này"]
         },
      ]
   },
]

export default Register;

