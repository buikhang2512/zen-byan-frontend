import React, { useContext } from "react";
import { Modal, Message, Button, Icon, Divider } from "semantic-ui-react";
import { Redirect } from 'react-router-dom';
import { AppContext } from "../../../AppContext";

const LogOutModal = (props) => {
   const [isConfirmed, setConfirmed] = React.useState(false)
   const { open, onClose, ...rest } = props
   const ctx = useContext(AppContext)

   const handleLogout = (e) => {
      ctx.actions.logout()
      setConfirmed(true)
   }

   return (
      <React.Fragment>
         <Modal size='tiny' closeOnEscape
            open={open} onClose={onClose || undefined}>
            {
               !isConfirmed
                  ? <>
                     <Message icon floating style={{ margin: "0px" }}
                        onDismiss={onClose}
                     >
                        <Icon name='question' />
                        <Message.Content >
                           <Message.Header style={{ padding: "7px" }}>
                              Bạn chắc chắn muốn đăng xuất?
                           </Message.Header>

                           <Divider clearing />

                           <div style={{ textAlign: "right" }}>
                              <Button icon="reply" size="small" labelPosition="right"
                                 content="Quay lại" onClick={onClose || undefined} />

                              <Button icon="sign-out" size="small" primary labelPosition="right"
                                 content="Đăng xuất" onClick={handleLogout} style={{ marginRight: "0px" }} />
                           </div>
                        </Message.Content>
                     </Message>
                  </>
                  : <Redirect to="/" />
            }
         </Modal>
      </React.Fragment>
   );
}

export default React.memo(LogOutModal)