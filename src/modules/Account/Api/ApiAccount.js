import axios from '../../../Api/axios';

export const ApiAccount = {
   authenticate(data, callback) {
      axios.post(`account/authenticate`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   refreshToken(data, callback) {
      axios.post(`account/refreshtoken`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   register(data, callback) {
      axios.post(`account/register`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   forgotPassword(data, callback) {
      axios.post(`account/forgot_password`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   getRegisterInfo(id, callback) {
      axios.get(`account/register_info/${id}`)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   activated(idRegister, data, callback) {
      axios.post(`account/activated/${idRegister}`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   getTenantsByUser(callback) {
      axios.get(`account/tenants`,
      )
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   changePassword(data, callback) {
      axios.put(`account/change_pass`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },
}