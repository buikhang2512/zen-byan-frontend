import React, { Component, createRef } from "react";
import {
   Button, Form, Segment, Header, Grid, Divider, Image, Message
} from "semantic-ui-react";
import _ from 'lodash'
import { injectIntl } from "react-intl";
import * as Yup from 'yup';

import { ZenFormik, ZenField } from '../../../components/Control/index'
import { ApiAccount } from "../Api/ApiAccount";
import { Link, Redirect } from "react-router-dom";
import * as routes from '../../../constants/routes'
import { ZenHelper, auth } from "../../../utils";


class ActivatedForm extends Component {
   constructor(props) {
      super(props);
      this.refZenFormik = createRef();
      this.formValidation = [
         {
            id: "password",
            validationType: "string",
            validations: [
               {
                  type: "required",
                  params: ["Không được bỏ trống trường này"]
               },
            ]
         },
         {
            id: "re_password",
            validationType: "string",
            validations: [
               {
                  type: "required",
                  params: ["Không được bỏ trống trường này"]
               },
               {
                  type: "oneOf",
                  params: [[Yup.ref('password')], props.intl.formatMessage({ id: "valid.pass_confirm", defaultMessage: "Mật khẩu xác nhận ko đúng" })]
               },
            ]
         },
      ]
      this.state = {
         tokenRegister: "",
         error: false,
         isLoading: true,
         isBtnLoading: false,
         isSuccess: false,
         errorList: [],
         registerInfo: { ...initItem }
      }
   }

   componentDidMount() {
      const { match } = this.props
      const tokenRigister = match.params.id

      ApiAccount.getRegisterInfo(tokenRigister, res => {
         if (res.status === 200) {
            const data = res.data.data

            // status: 1 - đã kích hoạt , 0 - chưa kich hoạt
            if (data.status === 1) {
               // đã kich hoạt => login
               this.props.history.push(routes.Login)
            } else {
               this.setState({
                  tokenRegister: tokenRigister,
                  registerInfo: { ...initItem, ...data },
                  isLoading: false
               })
            }
         } else {
            this.setState({
               isLoading: false,
               error: true,
               errorList: [].concat(ZenHelper.getResponseError(res))
            })
         }
      })
   }

   handleSubmit = (values, form) => {
      this.setState({ isBtnLoading: true })

      const newUser = {
         id: 0,
         full_name: values.full_name,
         email: values.email,
         password: values.password
      }

      ApiAccount.activated(this.state.tokenRegister, newUser, res => {
         if (res.status === 204) {
            // kích hoạt thành công => show SuccessForm
            this.setState({
               isSuccess: true,
               isBtnLoading: false,
            })
         } else {
            this.setState({
               isBtnLoading: false,
               error: true,
               errorList: [].concat(ZenHelper.getResponseError(res))
            })
         }
      })
   }

   render() {
      const { isLoading, error, errorList, isSuccess, registerInfo, isBtnLoading } = this.state;
      if (auth.getUserInfo()) {
         return <Redirect to="/home" />
      }

      return (
         <React.Fragment>
            <Grid
               textAlign="center"
               style={{ height: "100vh" }}
               verticalAlign="middle"
            >
               <Grid.Column style={{ maxWidth: 450 }}>
                  <Image src='/static/media/logo.png' style={{ maxWidth: 250 }} centered />

                  <Segment textAlign="left" loading={isLoading}>
                     {!error && !isSuccess && <MainForm onRef={this.refZenFormik} handleSubmit={this.handleSubmit}
                        formValidation={this.formValidation} registerInfo={registerInfo} isBtnLoading={isBtnLoading}
                     />}
                     {!error && isSuccess && <SuccessForm />}
                     {error && <ErrorForm message={errorList} />}
                  </Segment>
               </Grid.Column>
            </Grid>
         </React.Fragment >
      );
   }
}

export default injectIntl(ActivatedForm)
const initItem = {
   full_name: "",
   business_name: "",
   email: "",
   password: "",
   re_password: "",
}

const MainForm = ({ isBtnLoading, formValidation, registerInfo, handleSubmit, onRef }) => {
   return <>
      <Header textAlign="center">
         Thông tin đăng kí của bạn đã được xác thực. Vui lòng kích hoạt tài khoản để bắt đầu sử dụng phần mềm.
      </Header>

      <Divider />

      <ZenFormik form={"form-activated"} ref={onRef}
         validation={formValidation}
         initItem={registerInfo}
         onSubmit={handleSubmit}
      >
         {formikProps => {
            return <Form>
               <ZenField readOnly
                  label={"register.full_name"}
                  defaultlabel="Họ và tên"
                  name="full_name"
                  props={formikProps}
               />
               <ZenField readOnly
                  label={"register.business_name"}
                  defaultlabel="Công ty"
                  name="business_name"
                  props={formikProps}
               />
               <ZenField readOnly
                  label={"act.email"}
                  defaultlabel="Email"
                  name="email"
                  props={formikProps}
               />
               <ZenField type={'password'}
                  label={"act.password"} defaultlabel="Mật khẩu"
                  name="password"
                  props={formikProps}
               />
               <ZenField type="password"
                  label={"act.re_password"} defaultlabel="Xác nhận mật khẩu"
                  name="re_password" props={formikProps}
               />
            </Form>
         }}
      </ZenFormik>

      <br />
      <Button primary fluid content="Kích hoạt tài khoản"
         loading={isBtnLoading} disabled={isBtnLoading}
         onClick={() => onRef.current.handleSubmit()} />
   </>
}

const SuccessForm = () => {
   return <>
      <Header textAlign="center">
         Chúc mừng bạn đã kích hoạt thành công. Hãy bắt đầu sử dụng phần mềm với Email đã đăng kí.
      </Header>
      <br />
      <Button primary fluid content="Quay lại trang đăng nhập"
         as={Link} to={routes.Login} />
   </>
}

const ErrorForm = ({ message = [] }) => {
   return <>
      <Header textAlign="center">
         {message.length > 0 ? message[0] : "Không tìm thấy thông tin đăng kí"}
      </Header>
      <Divider />
      <div style={{ clear: "both" }}>
         <div style={{ float: "left" }}>
            <Link to={routes.Login}>Quay lại trang đăng nhập</Link>
         </div>
         <div style={{ float: "right" }}>
            <Link to={routes.Register}>Đăng kí tài khoản</Link>
         </div>
      </div>
      <br />
   </>
}