import React, { Component } from "react";
import {
  Button, Form, Segment, Header,
  Grid, Divider, Image, Message,
} from "semantic-ui-react";
import { ApiAccount } from "../Api/ApiAccount";
import { Link } from "react-router-dom";
import * as routes from "../../../constants/routes";
import { ZenField, ZenFormik } from "../../../components/Control";
import { IntlFormat } from "../../../utils/intlFormat";
import { ValidError } from "../../../utils/language/variable";
import { default as LoginSelectTenant } from "./LoginSelectTenant"
import { AppContext } from "../../../AppContext";
import auth from "../../../utils/auth";

class Login extends Component {
  constructor(props) {
    super();
    this.refFormAuthen = React.createRef();

    this.state = {
      openSelectTenant: false,
      authenticating: false,
      tenants: [],
      error: null,
      items: initItem,
    };
  }

  componentDidMount() {
    if (auth.checkLogin()) {
      this.props.history.push("/home")
    }
  }

  handleLogin = (itemForm) => {
    this.authenticate(itemForm);
  };

  authenticate = (item) => {
    var tenantId = auth.getTenantId();

    this.authenticateWithTenant(item, tenantId);
  };

  authenticateWithTenant = (item, tenantId) => {
    const { history } = this.props;

    var loginModel = { ...item, tenant: tenantId };
    this.setState({ authenticating: true });

    ApiAccount.authenticate(loginModel, (res) => {
      if (res.status === 200) {
        //Login thành công
        this.setState({ authenticating: false });

        var { data } = res.data;

        if (data.tenants && data.tenants.length > 1) {
          //Nhiều hơn 1 tenant thì chọn tenants
          // Lưu list tenant vào global state

          this.setState({
            error: null,
            authenticating: false,
            openSelectTenant: true,
            tenants: data.tenants,
          });
        } else {
          // Login
          auth.setToken(data.access_token, true);
          auth.saveCurrentSession();

          // đăng kí hubs và get data
          this.context.actions.startHubs(true)

          this.setState({
            error: null,
            authenticating: false,
            openSelectTenant: false,
          });

          history.push("/");
        }
      } else {
        //Logn in không thành công
        if (
          res.response &&
          res.response.data &&
          res.response.data.success === false
        ) {
          this.setState({
            error: res.response.data.messages,
            authenticating: false,
            openSelectTenant: false,
          });
        } else {
          this.setState({
            error: ["Có lỗi khi thực hiện đăng nhập hệ thống."],
            authenticating: false,
            openSelectTenant: false,
          });
        }
      }
    });
  };

  //Handle select tenant
  handleSelectTenant = (t) => this.authenticateWithTenant(this.refFormAuthen.current.values, t.identifier);
  handleCloseSelectTenant = () => this.setState({ openSelectTenant: false });

  //======
  render() {
    const {
      error,
      authenticating,
      openSelectTenant,
      tenants,
    } = this.state;

    return (
      <React.Fragment>
        <Grid
          textAlign="center"
          style={{
            height: "100vh",
            backgroundImage: `url("/static/media/bg-login.jpg")`,
            backgroundSize: "cover",
            backgroundPosition: "left top",
            backgroundRepeat: "no-repeat",
          }}
          verticalAlign="middle"
        >
          <Grid.Column style={{ maxWidth: 450 }}>            
            <Image src='/static/media/logo.png' style={{ maxWidth: 250 }} centered />
            <Segment textAlign="left">
              <Header as='h3' className="centered" >
                Đăng nhập hệ thống
              </Header>
              <Divider />

              {error && <Message negative list={error} />}

              <ZenFormik form={"login-form"} ref={this.refFormAuthen}
                validation={formValidation}
                initItem={initItem}
                onSubmit={this.handleLogin}
              >
                {
                  formik => {
                    return <Form id={"login-form"}>
                      <ZenField isIntl={false}
                        label={"Email"}
                        name="email"
                        props={formik}
                      />
                      <ZenField isIntl={false}
                        label={"Password"}
                        name="password"
                        props={formik}
                        type={"password"}
                      />
                    </Form>
                  }
                }
              </ZenFormik>

              <Divider hidden />
              <Button
                form="login-form"
                loading={authenticating}
                fluid
                primary
                type="submit"
                size="small"
                onClick={(e) => this.refFormAuthen.current.handleSubmit(e)}
                content="Login"
              />

              <Divider />
              <div>
                <p>Development & support by <a href="https://zentech.vn" target={"_blank"}>ZENTECH</a>. </p>
                <p>Email to <a href="mailto:support@zentech.vn">support@zentech.vn</a> or Hotline <a href="tel:02836220101">0283 622 0101</a>  </p>
                
                {/*<Link to={routes.Register}>Đăng ký sử dụng</Link>*/}
                {/*<span style={{ float: "right" }}>*/}
                {/*  <Link to={routes.ForgotPassword}>Quên mật khẩu</Link>*/}
                {/*</span>*/}
              </div>
            </Segment>
          </Grid.Column>
        </Grid>

        {/* nếu tài khoản sử dụng cho nhiều tổ chức thì show modal */}
        <LoginSelectTenant open={openSelectTenant}
          tenants={tenants}
          onClose={this.handleCloseSelectTenant}
          onSelected={this.handleSelectTenant}
        />
      </React.Fragment>
    );
  }
}


const initItem = {
  email: "",
  password: "",
};

const formValidation = [
  {
    ...ValidError.formik({ name: "email", type: "string" },
      { type: "required", intl: IntlFormat.default(ValidError.Required) },
      { type: "email", intl: IntlFormat.default(ValidError.EmailFormat) }
    )
  },
  {
    ...ValidError.formik({ name: "password", type: "string" },
      { type: "required", intl: IntlFormat.default(ValidError.Required) }
    )
  },
]

Login.contextType = AppContext;
export default Login