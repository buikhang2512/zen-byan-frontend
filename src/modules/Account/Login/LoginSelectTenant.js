import React from "react";
import { Divider, Icon, List, Modal } from "semantic-ui-react";
import { ZenButton } from "../../../components/Control";

const LoginSelectTenant = ({ tenants, tenantCurrent, open, onClose, onSelected }) => (
  <Modal
    size="small"
    open={open} closeIcon
    closeOnDimmerClick={false}
    closeOnEscape
    onClose={onClose}
  >
    <Modal.Header>Chọn tổ chức/doanh nghiệp</Modal.Header>
    <Modal.Content>
      <p>
        Tài khoản hiện tại sử dụng cho nhiều tổ chức, vui lòng chọn tổ chức
        làm việc.
      </p>
      <Divider />
      <List selection verticalAlign="middle" size="large">
        {
          tenants.map(tenant => {
            return <List.Item
              key={tenant.id}
              onClick={() => onSelected(tenant)}
            >
              <Icon name="building" />
              <List.Content>
                <List.Header>{tenant.name}</List.Header>
              </List.Content>
            </List.Item>
          })
        }
      </List>
      <Divider hidden />
    </Modal.Content>
    <Modal.Actions>
      <ZenButton btnType="cancel" size="small"
        onClick={onClose}
      />
    </Modal.Actions>
  </Modal>
);

export default LoginSelectTenant