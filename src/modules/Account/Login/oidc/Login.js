import React, { Component } from 'react';
import { Container, Image, Form, Message, Grid } from 'semantic-ui-react';
import { default as auth } from 'utils/auth';
import { Redirect } from 'react-router-dom';

class Login extends Component {
   constructor(props) {
      super();
      this.state = {
      };
   }
   onSubmit = (e) => {
      auth.login();
   }

   render() {
      const { error, isLoging } = this.state;

      if (auth.isLoggedIn()) {
         return <Redirect to="/" />;
      }
      var styleMain = {
         backgroundColor: 'rgba(0,0,0,2)',
         backgroundSize: 'cover',
         width: "100%",
         height: "100vh"
      };
      var styleContainer = {
         //backgroundImage: `url(${Background})`,
         backgroundRepeat: 'no-repeat',
         backgroundSize: 'cover',
         opacity: 0.5,
         backgroundColor: 'rgba(0,0,0,0.5)',
         width: "100%",
         //display: 'block',
         height: '100%'
      };
      var styleContainerAbsolute = {
         position: 'absolute',
         width: '100%',
         height: '100vh',
         display: 'flex',
         'align-items': 'center',
         'justify-content': 'center',
         'flex-direction': 'column',
         'text-align': 'center',
         //'justify-content': 'center',

      }
      var styleGrid = {
         height: '200px',
         width: '400px',
         border: '1px solid #ccc',
         paddingTop: '10px',
         paddingLeft: '20px',
         paddingRight: '20px',
         opacity: 1,
         zIndex: 2,
         backgroundColor: 'rgba(255,255,255,0.8)',
         borderRadius: 10,
      }
      var login = <Container style={styleContainerAbsolute}>
         <Container
            style={styleGrid}>
            <Grid textAlign='center' style={{ marginTop: '10px' }}>
               {/* <Image src={Logo} height="100px" /> */}
            </Grid>
            <Form
               style={{ marginTop: '20px' }}
               error={error}
               onSubmit={this.onSubmit}
            >
               <Form.Button color='facebook' fluid type="submit" loading={isLoging}>Đăng nhập</Form.Button>
            </Form>
         </Container>
      </Container>
      return (
         <Container style={styleMain}>
            {login}
            {/* <Image src={Background} style={styleContainer} /> */}

         </Container>
      );
   }
}

export default Login;