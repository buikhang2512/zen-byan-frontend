import React, { Component } from 'react';
import { Menu } from 'semantic-ui-react';
import { default as auth } from 'utils/auth';

class LoginItem extends Component {
   constructor(props) {
      super();
      this.state = {
         logged : false
      }
   }
   render() {

      return (
         <React.Fragment>
            <Menu.Item onClick = {auth.login}>Log in</Menu.Item>
         </React.Fragment>
      );
   }
}
export default LoginItem