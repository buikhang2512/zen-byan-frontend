import React, { Component } from "react";
import {
   Button,
   Form,
   Segment,
   Header,
   Grid,
   Divider,
   Image,
   Message
} from "semantic-ui-react";
import _ from 'lodash'
import { withFormik } from "formik";
import * as Yup from "yup";
import { ZenField } from "components/Control";
import { ZenHelper, MESSAGES } from "utils";
import { ApiAccount } from "../Api/ApiAccount";
import { Link } from "react-router-dom";
import * as routes from '../../../constants/routes'
import { ZenFormik } from "../../../components/Control";

class ForgotPassword extends Component {
   constructor(props) {
      super();
      this.refForm = React.createRef();
      this.state = {
         error: false,
         errorText: [],
         items: initItem,
         btnLoading: false
      };
   }

   handleForgotPass = (values) => {
      this.setState({ btnLoading: true })
      this.forgotPassword(values)
   }

   forgotPassword(item) {
      ApiAccount.forgotPassword(item, res => {
         if (res.status === 204) {
            this.props.history.push(routes.ForgotPasswordTks)
         } else {
            if (res.response && res.response.data && res.response.data.success === false) {
               this.setState({
                  btnLoading: false,
                  error: true,
                  errorText: res.response.data.messages[0]
               });
            } else {
               this.setState({
                  btnLoading: false,
                  error: true,
                  errorText: "" + res
               });
            }
         }
      })
   }

   render() {
      const { error, errorText, btnLoading } = this.state;
      return (
         <React.Fragment>
            <Grid
               textAlign="center"
               style={{
                  height: "100vh",
                  backgroundImage: `/static/media/bg-login.jpg`,
                  backgroundSize: "cover",
                  backgroundPosition: "left top",
                  backgroundRepeat: "no-repeat",
                }}
               verticalAlign="middle"
            >
               <Grid.Column style={{ maxWidth: 450 }}>
                  <Image src='/static/media/logo.png' style={{ maxWidth: 250 }} centered />
                  <Segment textAlign="left">
                     <Header as='h3' className="centered" >
                        Bạn đã quên mật khẩu?
                     </Header>
                     <p>Vui lòng nhập Email đã đăng ký của bạn. Chúng tôi sẽ gửi thông tin mật khẩu mới đến email của bạn.</p>
                     <Divider />

                     <ZenFormik form={"forgot-form"}
                        ref={this.refForm}
                        validation={formValidation}
                        initItem={initItem}
                        onSubmit={this.handleForgotPass}
                     >
                        {
                           formikProps => {
                              return <Form>
                                 <ZenField isIntl={false}
                                    name="email"
                                    label="Email"
                                    props={formikProps}
                                 />
                              </Form>
                           }
                        }
                     </ZenFormik>

                     {error && <Message negative content={errorText} />}

                     <Divider hidden />
                     <Button form="forgot-form" icon="paper plane outline"
                        fluid primary type="submit" size="small"
                        loading={btnLoading} disabled={btnLoading}
                        onClick={e => this.refForm.current.handleSubmit(e)}
                        content="Lấy lại mật khẩu" />

                     <Divider />
                     <div style={{ textAlign: "right" }}>
                        Quay lại trang
                        <Link to={routes.Login}> Đăng nhập.</Link>
                     </div>
                  </Segment>
               </Grid.Column>
            </Grid>
         </React.Fragment >
      );
   }
}

const initItem = {
   email: "",
};

const formValidation = [
   {
      id: "email",
      validationType: "string",
      validations: [
         {
            type: "required",
            params: ["Không được bỏ trống trường này"]
         },
         {
            type: "email",
            params: ["Email không đúng định dạng"]
         }
      ]
   },
]
export default ForgotPassword
