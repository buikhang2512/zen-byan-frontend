import React from "react";
import {
   Button,
   Segment,
   Header,
   Grid,
   Image,
} from "semantic-ui-react";
import { Link } from "react-router-dom";
import * as routes from '../../../constants/routes'

const ForgotPasswordTks = props => {
   return (
      <React.Fragment>
         <Grid
            textAlign="center"
            style={{ height: "100vh" }}
            verticalAlign="middle"
         >
            <Grid.Column style={{ maxWidth: 600 }}>
               <Image src='/static/media/logo.png' style={{ maxWidth: 250 }} centered />
               
               <Segment basic textAlign="left">
                  <Header as="h3" textAlign="center"
                     content="Mật khẩu đã được thiết lập lại, vui lòng kiểm tra email của bạn." />

                  <div style={{ textAlign: "center" }}>
                     <Button primary icon="sign-in" content="Quay về trang đăng nhập"
                        as={Link} to={routes.Login}
                     />
                  </div>
               </Segment>
            </Grid.Column>
         </Grid>
      </React.Fragment >
   );
}

export default ForgotPasswordTks