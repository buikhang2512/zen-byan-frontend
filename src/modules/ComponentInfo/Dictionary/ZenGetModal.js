import { ZenDictionaryModal } from "../../../components/Control/zDictionary/index";
import { ZenHelper } from "../../../utils";
import { ArDmKh } from "../../AR/Dictionary/index";
import { ARDmPlkh } from "../../AR/Dictionary/ARDmPlkh";
import { CRMDealGiaiDoan, CRMNguonKh, CRMLinhVuc, CRMLoaiHoatDong, CRMHoatDong, CRMContact, CRMDeal, CRMDealUuTien } from "../../CRM/Dictionary";
import { SIDmHuyen, SIDmLoai, SIDmQuocGia, SIDmTinh, SIDmLoaiTaiLieu, SIDmNamTC, SIDMCT } from "../../SI/Dictionary";
import { ZenLookup } from "./ZenLookup";
import { SIDmNgh } from "../../SI/Dictionary/SIDmNgh";
import { SIDmNhhd } from "../../SI/Dictionary/SIDmNhhd";
import { SIDmPhi } from "../../SI/Dictionary/SIDmPhi"
import { SIDmHd } from "../../SI/Dictionary/SIDmHd";
import { SIDmForm } from "../../SI/Dictionary/SIDmForm";
import { PMDmNhomDa, PMDmGiaiDoanDa, PMDuAn, PMDmDAHangMuc } from "../../PM/Dictionary/index";
import { WMDmGiaiDoanCV, WMDmLoaiCV, WMDmUuTienCV, WMCongViec } from "../../WM/Dictionary/index";
import { INDmDvt, INDmNhvt, INDmPlvt, INDmVT } from "../../IN/Dictionary";
import { PADmKyLuong } from "../../PA/Dictionary/PADmKyLuong";
import { PADieuChinh } from "../../PA/Dictionary/PADieuChinh";
import { PAKyHieuChamCong } from "../../PA/Dictionary/PAKyHieuChamCong";
import { SODmTs } from "../../SO/Dictionary/SODmTs";
import { HRDmNgayNghi, HrDmLyDoNghi, HRHsNs, HRDmCctc, HRLichDay } from "../../HrDmOther/HR_Dictionary/index";
import { Menu } from "../../Menu/Dictionary/Menu";
import { PADmLoaiBangLuong } from "../../PA/Dictionary/PADmLoaiBangLuong";
import { GlDmTk } from "../../GL/Dictionary/index"
import { PABangLuong, PAHangSo } from "../../PA/Dictionary";
import { DashLet } from "../../Dashboard/Dictionary/DashLet"
import { EODmLoaiPost } from "../../EO/Dictionary";
import { DashBoardTemplate } from "../../Dashboard/Dictionary/DashboardTemplate";

export function getModal(lookup) {
  if (!ZenHelper) return;

  if (lookup) {
    switch (lookup.code) {
      // ============================== AR
      case ZenLookup.Ma_plkh.code:
        return {
          info: { ...ARDmPlkh },
          component: ZenDictionaryModal,
          header: ZenHelper.GetMessage("ardmplkh.header", "Phân loại khách hàng"),
        };
      case ZenLookup.Ma_kh.code:
        return {
          info: { ...ArDmKh },
          component: ZenDictionaryModal,
          header: ZenHelper.GetMessage("ardmkh.header", "Khách hàng"),
        };
      // =========== CRM
      case ZenLookup.Ma_nguon_kh.code:
        return {
          info: { ...CRMNguonKh }, component: ZenDictionaryModal,
          header: ZenHelper.GetMessage("CRMNguonKh.header", "Nguồn khách hàng")
        }
      case ZenLookup.Ma_linh_vuc.code:
        return {
          info: { ...CRMLinhVuc }, component: ZenDictionaryModal,
          header: ZenHelper.GetMessage("CRMLinhVuc.header", "Lĩnh vực")
        }
      case ZenLookup.Ma_hoat_dong.code:
        return {
          info: { ...CRMLoaiHoatDong }, component: ZenDictionaryModal,
          header: ZenHelper.GetMessage("CRMHoatDong..header", "Loại hoạt động")
        }
      case ZenLookup.CRMHoatDong.code:
        return {
          info: { ...CRMHoatDong }, component: ZenDictionaryModal,
          header: ZenHelper.GetMessage("CRMHoatDong.header", "Hoạt động")
        }
      case ZenLookup.CrmContact.code:
        return {
          info: { ...CRMContact }, component: ZenDictionaryModal,
          header: ZenHelper.GetMessage("CrmContact.header", "Người liên hệ")
        }
      case ZenLookup.CRMDeal.code:
        return {
          info: { ...CRMDeal }, component: ZenDictionaryModal,
          header: "Cơ hội bán hàng",
        }
      case ZenLookup.Ma_giai_doan.code:
        return {
          info: { ...CRMDealGiaiDoan }, component: ZenDictionaryModal,
          header: "Giai đoạn của cơ hội bán hàng",
        }
      case ZenLookup.Ma_uu_tien.code:
        return {
          info: { ...CRMDealUuTien }, component: ZenDictionaryModal,
          header: "Độ ưu tiên",
        }
      // =========== SI
      case ZenLookup.SIDmloai.code:
        return {
          info: { ...SIDmLoai }, component: ZenDictionaryModal,
          header: ZenHelper.GetMessage("sidmloai.header", "Loại")
        }
      case ZenLookup.SIDmQuocGia.code:
        return {
          info: { ...SIDmQuocGia }, component: ZenDictionaryModal,
          header: ZenHelper.GetMessage("sidmquocgia.header", "Quốc gia")
        }
      case ZenLookup.SIDmTinh.code:
        return {
          info: { ...SIDmTinh }, component: ZenDictionaryModal,
          header: ZenHelper.GetMessage("sidmtinh.header", "Tỉnh/thành phố")
        }
      case ZenLookup.SIDmHuyen.code:
        return {
          info: { ...SIDmHuyen }, component: ZenDictionaryModal,
          header: ZenHelper.GetMessage("sidmhuyen.header", "Quận/huyện")
        }
      case ZenLookup.SIDmloaitailieu.code:
        return {
          info: { ...SIDmLoaiTaiLieu }, component: ZenDictionaryModal,
          header: "Loại tài liệu"
        }
      case ZenLookup.Ma_nhhd.code:
        return {
          info: { ...SIDmNhhd },
          component: ZenDictionaryModal,
          header: ZenHelper.GetMessage("sidmnhhd.header"),
        };
      case ZenLookup.Ma_ngh.code:
        return {
          info: { ...SIDmNgh },
          component: ZenDictionaryModal,
          header: ZenHelper.GetMessage("sidmngh.header"),
        };
      case ZenLookup.Ma_phi.code:
        return {
          info: { ...SIDmPhi },
          component: ZenDictionaryModal,
          header: ZenHelper.GetMessage("sidmphi.header"),
        };
      case ZenLookup.Ma_hd.code:
        return {
          info: { ...SIDmHd },
          component: ZenDictionaryModal,
          header: ZenHelper.GetMessage("SIDmHd.header", "Hợp đồng")
        };
      case ZenLookup.SIDmForm.code:
        return {
          info: { ...SIDmForm },
          component: ZenDictionaryModal,
          header: "Biểu mẫu"
        };
      case ZenLookup.Nam_tc.code:
        return { info: { ...SIDmNamTC }, component: ZenDictionaryModal, header: "Năm tài chính" }
      case ZenLookup.Ma_ct.code:
        return { info: { ...SIDMCT }, component: ZenDictionaryModal, header: "khai báo màn hình nhập chứng từ" }
      // ============= SO
      case ZenLookup.Ma_thue.code:
        return { info: { ...SODmTs }, component: ZenDictionaryModal, header: "Thuế suất" }
      // ============= HR
      case ZenLookup.HRDmcctc.code:
        return {
          info: { ...HRDmCctc }, component: ZenDictionaryModal,
          header: ZenHelper.GetMessage("hrmdcctc.header", "Cơ cấu tổ chức")
        }
      case ZenLookup.HRHsNs.code:
        return {
          info: { ...HRHsNs }, component: ZenDictionaryModal,
          header: ZenHelper.GetMessage("hrhsns.header", "Nhân sự")
        }
      case ZenLookup.HrLichDay.code:
        return {
          info: { ...HRLichDay }, component: ZenDictionaryModal,
          header: "Lịch dạy"
        }
      case ZenLookup.HrDmNgayNghi.code:
        return {
          info: { ...HRDmNgayNghi }, component: ZenDictionaryModal,
          header: "Danh mục ngày nghỉ"
        }
      case ZenLookup.HrDmLyDoNghi.code:
        return {
          info: { ...HrDmLyDoNghi }, component: ZenDictionaryModal,
          header: "Danh mục lý do nghỉ"
        }
      // ============= IN
      case ZenLookup.Ma_nhvt.code:
        return { info: { ...INDmNhvt }, component: ZenDictionaryModal, header: "Nhóm vật tư hàng hóa" }
      case ZenLookup.Ma_plvt.code:
        return { info: { ...INDmPlvt }, component: ZenDictionaryModal, header: "Phân loại vật tư hàng hóa" }
      case ZenLookup.Ma_dvt.code:
        return { info: { ...INDmDvt }, component: ZenDictionaryModal, header: "Danh mục đơn vị tính" }
      case ZenLookup.Ma_vt.code:
        return { info: { ...INDmVT }, component: ZenDictionaryModal, header: "Danh mục vật tư hàng hóa" }
      // ============= PM
      case ZenLookup.PMDmNhomDa.code:
        return {
          info: { ...PMDmNhomDa }, component: ZenDictionaryModal,
          header: "Nhóm dự án"
        }
      case ZenLookup.PMDmGiaiDoanDa.code:
        return {
          info: { ...PMDmGiaiDoanDa }, component: ZenDictionaryModal,
          header: "Giai đoạn dự án"
        }
      case ZenLookup.PMDuAn.code:
        return {
          info: { ...PMDuAn }, component: ZenDictionaryModal,
          header: "Dự án"
        }
      case ZenLookup.PMDmDAHangMuc.code:
        return {
          info: { ...PMDmDAHangMuc }, component: ZenDictionaryModal,
          header: "Hạng mục"
        }
      // =============== WM
      case ZenLookup.WMDmLoaiCV.code:
        return {
          info: { ...WMDmLoaiCV }, component: ZenDictionaryModal,
          header: "Loại công việc"
        }
      case ZenLookup.WMDmGiaiDoanCV.code:
        return {
          info: { ...WMDmGiaiDoanCV }, component: ZenDictionaryModal,
          header: "Giai đoạn công việc"
        }
      case ZenLookup.WMDmUuTienCV.code:
        return {
          info: { ...WMDmUuTienCV }, component: ZenDictionaryModal,
          header: "Mức độ ưu tiên"
        }
      case ZenLookup.WMCongViec.code:
        return {
          info: { ...WMCongViec },
          component: ZenDictionaryModal,
          header: "Công việc",
        };
      case ZenLookup.Ma_ky_luong.code:
        return { info: { ...PADmKyLuong }, component: ZenDictionaryModal, header: "Danh mục kỳ lương" }
      case ZenLookup.PADieuChinh.code:
        return { info: { ...PADieuChinh }, component: ZenDictionaryModal, header: "Điều chỉnh lương" }
      case ZenLookup.PAKyHieuChamCong.code:
        return { info: { ...PAKyHieuChamCong }, component: ZenDictionaryModal, header: "Ký hiệu chấm công" }
      case ZenLookup.PADmLoaiBangLuong.code:
        return { info: { ...PADmLoaiBangLuong }, component: ZenDictionaryModal, header: "Loại bảng lương" }
      case ZenLookup.PABangLuong.code:
        return { info: { ...PABangLuong }, component: ZenDictionaryModal, header: "Bảng lương" }
      case ZenLookup.PAHangSo.code:
        return { info: { ...PAHangSo }, component: ZenDictionaryModal, header: "Hằng số lương" }
      case ZenLookup.Menus.code:
        return { info: { ...Menu }, component: ZenDictionaryModal, header: "Menu" }
      case ZenLookup.SysDashLet.code:
        return { info: { ...DashLet }, component: ZenDictionaryModal, header: "Dashlet" }
      case ZenLookup.EODmLoaiPost.code:
        return { info: { ...EODmLoaiPost }, component: ZenDictionaryModal, header: "Danh mục loại thông báo" }
      case ZenLookup.SysDashBoardTemplate.code:
        return { info: { ...DashBoardTemplate }, component: ZenDictionaryModal, header: "Thông tin dashboard" }
      case ZenLookup.TK.code:
        return {
          info: { ...GlDmTk },
          component: ZenDictionaryModal,
          header: "Danh mục tài khoản",
        };
      default:
        return undefined;
    }
  }
  return undefined;
}
