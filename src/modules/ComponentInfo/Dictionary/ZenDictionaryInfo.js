import AR_Dictionary from "../../AR/Dictionary/AR_Dictionary";
import { CRM_Dictionary } from "../../CRM/Dictionary/CRM_Dictionary";
import { HR_Dictionary } from "../../HrDmOther/HR_Dictionary/HR_Dictionary";
import IN_Dictionary from "../../IN/Dictionary/IN_Dictionary";
import PM_Dictionary from "../../PM/Dictionary/PM_Dictionary";
import { SI_Dictionary } from "../../SI/Dictionary/SI_Dictionary";
import WM_Dictionary from "../../WM/Dictionary/WM_Dictionary";
import PA_Dictionary from "../../PA/Dictionary/PA_Dictionary";
import { ZenLog_Dictionary } from "../../ZenLog/Dictionary/Zenlog_Dictionary";
import SO_Dictionary from "../../SO/Dictionary/SO_Dictionary";
import { Menus_Dictionary } from "../../Menu/Dictionary/Menu_Dictionary";
import GL_Dictionary from "../../GL/Dictionary/GL_Dictionary";
import { Dash_Dictionary } from "../../Dashboard/Dictionary/Dash_Dictionary";
import { PostInfo } from "../../PostMessage/PostMessage"
import EO_Dictionary from "../../EO/Dictionary/EO_Dictionary";

const ZenDictionaryInfo = {
    ...AR_Dictionary,
    ...CRM_Dictionary,
    ...SI_Dictionary,
    ...SO_Dictionary,
    ...IN_Dictionary,
    ...HR_Dictionary,
    ...PM_Dictionary,
    ...WM_Dictionary,
    ...PA_Dictionary,
    ...GL_Dictionary,
    ...ZenLog_Dictionary,
    ...Menus_Dictionary,
    ...Dash_Dictionary,
    ...EO_Dictionary,
    PostInfo: PostInfo,
}

export { ZenDictionaryInfo };
