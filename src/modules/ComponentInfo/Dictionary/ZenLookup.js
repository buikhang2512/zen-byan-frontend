import { KeyStorage } from "../../../utils/storage";

// use: API LOOKUP trong zenbook
// =============================
// cols:        Trường trả về: default trường field_list trong LookupInfo. Có thể khai báo thêm trường tại đây
// value:       Giá trị trường lưu xuống DB
// text:        Trường hiển thị trên input
// format:      Format list item, VD: "{ma_kh} -_- {ten_kh} - {mst}" => các trường tương ứng trong bảng lookup
// formatInput: Format giá trị hiển thị trên input. "{tk_vt}-{ten_tk_vt}"
//                => Trường hợp tên trường cần lưu khác với tên trường trong bảng lookup
// localData: key localstorage dùng search từ local
// onLocalWhere: (items)=>{
//    Dùng khi cần filter
//    return items
// }

// top:         Dùng khi search tại input. Default top = 20, nếu top = null => get all
// where:       Điều kiện lookup. VD: "isKh = 1"
// infoPopup:   Khai báo các trường hiển thị trong lookup modal.
// tableName, showFields: 2 key này ko còn dùng,
export const ZenLookup = {
   // =================================== AR
   CaDmNv: {
      code: "ma_kh_ca",
      codeDb: "ma_kh",
      where: "(isnv = 1 or isnvkd = 1)",
      cols: "ma_kh,ten_kh,dia_chi,ma_so_thue,nguoi_gd",
      value: "ma_kh",
      text: "ten_kh",
      showFields: "ma_kh,ten_kh",
      format: "{ma_kh} - {ten_kh}",
      tableName: "ArDmKh",
      infoPopup: {
         title: "Danh sách nhân viên",
         grid: [
            { col: "ma_kh", name: "Mã", width: 200 },
            { col: "ten_kh", name: "Tên", width: "100%" },
         ],
      },
   },
   TaOut: {
      code: "TaOut",
      where: null,
      cols: 'ma_ct,ten_ct',
      value: 'ma_ct',
      text: "ma_ct",
      tableName: "TaOut",
      showFields: "ma_ct,ten_ct",
      format: "{ma_ct} - {ten_ct}",
      localData: KeyStorage.CacheData,
      infoPopup: {
         title: "Danh mục chi phí mua hàng",
         grid: [
            { col: "ma_ct", name: "Mã CT", width: 200 },
            { col: "ten_ct", name: "Tên CT", width: "100%" },
         ]
      }
   },
   TaIn: {
      code: "TaIn",
      where: null,
      cols: 'ma_ct,ten_ct',
      value: 'ma_ct',
      text: "ma_ct",
      tableName: "TaIn",
      showFields: "ma_ct,ten_ct",
      format: "{ma_ct} - {ten_ct}",
      localData: KeyStorage.CacheData,
      infoPopup: {
         title: "Danh sách hóa đơn GTGT đầu vào",
         grid: [
            { col: "ma_ct", name: "Mã CT", width: 200 },
            { col: "ten_ct", name: "Tên CT", width: "100%" },
         ]
      }
   },
   Ma_kh: {
      code: "ma_kh",
      where: "iskh=1",
      filterType:"include", //include hoặc start, nếu start thì sẽ tìm theo dạng chuỗi bắt đầu, nếu include hoặc không có giá trị thì sẽ tìm theo dạng chứa trong
      cols: "ma_kh,ten_kh,dia_chi,ma_so_thue,nguoi_gd,ma_httt,email",
      value: "ma_kh",
      text: "ten_kh",
      format: "{ma_kh} - {ten_kh}",
      tableName: "ArDmKh",
      localData: KeyStorage.CacheData,
      infoPopup: {
         title: "Danh sách khách hàng",
         grid: [
            { col: "ma_kh", name: "Mã khách hàng", width: 200 },
            { col: "ten_kh", name: "Tên khách hàng", width: "100%" },
         ],
      },
   },

   Ma_nhkh: {
      code: "ma_nhkh",
      where: null,
      cols: "ma_nhkh,ten_nhkh",
      value: "ma_nhkh",
      text: "ten_nhkh",
      tableName: "ArDmNhkh",
      localData: KeyStorage.CacheData,
      infoPopup: {
         title: "Danh sách nhóm khách hàng",
         grid: [
            { col: "ma_nhkh", name: "Mã nhóm", width: 200 },
            { col: "ten_nhkh", name: "Tên nhóm", width: "100%" },
         ],
      },
   },
   Ma_plkh: {
      code: "ma_plkh",
      where: null,
      cols: "ma_plkh,ten_plkh,loai",
      value: "ma_plkh",
      text: "ten_plkh",
      tableName: "ArDmPlkh",
      localData: KeyStorage.CacheData,
      infoPopup: {
         title: "Danh sách loại khách hàng",
         grid: [
            { col: "ma_plkh", name: "Mã loại", width: 200 },
            { col: "ten_plkh", name: "Tên loại", width: "100%" },
         ],
      },
   },

   //===================================== HR
   HRDmcctc: {
      code: "id_cctc",
      where: null,
      cols: "id_cctc,ten_cctc,bac,id_parent,ma_bp",
      value: "id_cctc",
      text: "ten_cctc",
      format: "{id_cctc} - {ten_cctc}",
      tableName: "HRDmcctc",
      localData: KeyStorage.CacheData,
      infoPopup: {
         title: "Danh sách cơ cấu tổ chức",
         grid: [
            { col: "id_cctc", name: "Mã", width: 200 },
            { col: "ten_cctc", name: "Tên", width: "100%" },
         ],
      },
   },

   HRHsNs: {
      code: "id_nv",
      where: null,
      cols: "id_nv,ho_ten,gioi_tinh,ngay_sinh,so_dien_thoai,email,phong_ban",
      value: "id_nv",
      text: "ten_nv",
      format: "{id_nv} - {ten_nv}",
      tableName: "HRHsNs",
      localData: KeyStorage.CacheData,
      // infoPopup: {
      //    title: "Danh sách cơ cấu tổ chức",
      //    grid: [
      //       { col: "id_cctc", name: "Mã", width: 200 },
      //       { col: "ten_cctc", name: "Tên", width: "100%" },
      //    ],
      // },
   },
   HrDmKhac: {
      code: "hrdmkhac",
      where: null,
      cols: "code_name,ma,ten",
      value: "ma",
      text: "ten",
      //showFields: "ma,ten",
      format: "{ten}",
      tableName: "HrDmKhac",
      localData: KeyStorage.CacheData,
      // infoPopup: {
      //    title: "Danh sách loại",
      //    grid: [
      //       { col: "ma", name: "Mã loại", width: 200 },
      //       { col: "ten", name: "Tên loại", width: "100%" },
      //    ],
      // },
   },

   HrLichDay: {
      code: "HrLichDay",
      where: null,
      cols: "id",
      value: "id",
      text: "id",
      // showFields: "ma,ten",
      // format: "{ma} - {ten}",
      tableName: "HrLichDay",
      // infoPopup: {
      //    title: "Danh sách loại",
      //    grid: [
      //       { col: "ma", name: "Mã loại", width: 200 },
      //       { col: "ten", name: "Tên loại", width: "100%" },
      //    ],
      // },
   },

   HrDmNgayNghi: {
      code: "HrDmNgayNghi",
      where: null,
      cols: "id",
      value: "id",
      text: "id",
      tableName: "HrDmNgayNghi",
   },

   HrDmLyDoNghi: {
      code: "ma_ld_nghi",
      where: null,
      cols: "ma_ld_nghi",
      value: "ma_ld_nghi",
      text: "ma_ld_nghi",
      tableName: "HrDmLyDoNghi",
   },

   Ma_Dan_Toc: {
      code: "MA_DAN_TOC",
      where: null,
      cols: "code_name,ma,ten",
      value: "ma",
      text: "ten",
      showFields: "ma,ten",
      format: "{ma} - {ten}",
      tableName: "MA_DAN_TOC",
      localData: KeyStorage.CacheData,
   },
   MA_BC: {
      code: "MA_BC",
      where: null,
      cols: "code_name,ma,ten",
      value: "ma",
      text: "ten",
      showFields: "ma,ten",
      format: "{ma} - {ten}",
      tableName: "MA_BC",
      localData: KeyStorage.CacheData,
   },
   MA_CM: {
      code: "MA_CM",
      where: null,
      cols: "code_name,ma,ten",
      value: "ma",
      text: "ten",
      showFields: "ma,ten",
      format: "{ma} - {ten}",
      tableName: "MA_CM",
      localData: KeyStorage.CacheData,
   },
   MA_CQYT: {
      code: "MA_CQYT",
      where: null,
      cols: "code_name,ma,ten",
      value: "ma",
      text: "ten",
      showFields: "ma,ten",
      format: "{ma} - {ten}",
      tableName: "MA_CQYT",
      localData: KeyStorage.CacheData,
   },

   MA_DAC_DIEM_GV: {
      code: "MA_DAC_DIEM_GV",
      where: null,
      cols: "code_name,ma,ten",
      value: "ma",
      text: "ten",
      showFields: "ma,ten",
      format: "{ma} - {ten}",
      tableName: "MA_DAC_DIEM_GV",
      localData: KeyStorage.CacheData,
   },

   MA_CVCM: {
      code: "MA_CVCM",
      where: null,
      cols: "code_name,ma,ten",
      value: "ma",
      text: "ten",
      showFields: "ma,ten",
      format: "{ma} - {ten}",
      tableName: "MA_CVCM",
      localData: KeyStorage.CacheData,
   },

   MA_LOAI_HDLD: {
      code: "MA_LOAI_HDLD",
      where: null,
      cols: "code_name,ma,ten",
      value: "ma",
      text: "ten",
      showFields: "ma,ten",
      format: "{ma} - {ten}",
      tableName: "MA_LOAI_HDLD",
      localData: KeyStorage.CacheData,
   },

   MA_NN: {
      code: "MA_NN",
      where: null,
      cols: "code_name,ma,ten",
      value: "ma",
      text: "ten",
      showFields: "ma,ten",
      format: "{ma} - {ten}",
      tableName: "MA_NN",
      localData: KeyStorage.CacheData,
   },
   MA_TTHN: {
      code: "MA_TTHN",
      where: null,
      cols: "code_name,ma,ten",
      value: "ma",
      text: "ten",
      showFields: "ma,ten",
      format: "{ma} - {ten}",
      tableName: "MA_TTHN",
      localData: KeyStorage.CacheData,
   },

   MA_TON_GIAO: {
      code: "MA_TON_GIAO",
      where: null,
      cols: "code_name,ma,ten",
      value: "ma",
      text: "ten",
      showFields: "ma,ten",
      format: "{ma} - {ten}",
      tableName: "MA_TON_GIAO",
      localData: KeyStorage.CacheData,
   },
   MA_TDCM: {
      code: "MA_TDCM",
      where: null,
      cols: "code_name,ma,ten",
      value: "ma",
      text: "ten",
      showFields: "ma,ten",
      format: "{ma} - {ten}",
      tableName: "MA_TDCM",
      localData: KeyStorage.CacheData,
   },

   MA_TDDT: {
      code: "MA_TDDT",
      where: null,
      cols: "code_name,ma,ten",
      value: "ma",
      text: "ten",
      showFields: "ma,ten",
      format: "{ma} - {ten}",
      tableName: "MA_TDDT",
      localData: KeyStorage.CacheData,
   },

   MA_TDNN: {
      code: "MA_TDNN",
      where: null,
      cols: "code_name,ma,ten",
      value: "ma",
      text: "ten",
      showFields: "ma,ten",
      format: "{ma} - {ten}",
      tableName: "MA_TDNN",
      localData: KeyStorage.CacheData,
   },
   MA_LOAI_TAI_LIEU: {
      code: "MA_LOAI_TAI_LIEU",
      where: null,
      cols: "code_name,ma,ten",
      value: "ma",
      text: "ten",
      showFields: "ma,ten",
      format: "{ma} - {ten}",
      tableName: "MA_LOAI_TAI_LIEU",
      localData: KeyStorage.CacheData,
   },

   // ==================================== SI
   SIDmloai: {
      code: "ma_loai",
      where: null,
      cols: "ma_loai,ma,ten",
      value: "ma",
      text: "ten",
      showFields: "ma,ten",
      format: "{ma} - {ten}",
      tableName: "SIDmLoai",
      localData: KeyStorage.CacheData,
      // infoPopup: {
      //    title: "Danh sách loại",
      //    grid: [
      //       { col: "ma", name: "Mã loại", width: 200 },
      //       { col: "ten", name: "Tên loại", width: "100%" },
      //    ],
      // },
   },

   SIDmloaitailieu: {
      code: "sidmloaitailieu",
      where: null,
      cols: "ma_loai,ma,ten",
      value: "ma",
      text: "ten",
      showFields: "ma,ten",
      format: "{ma} - {ten}",
      tableName: "SIDmLoai",
      // infoPopup: {
      //    title: "Danh sách loại",
      //    grid: [
      //       { col: "ma", name: "Mã loại", width: 200 },
      //       { col: "ten", name: "Tên loại", width: "100%" },
      //    ],
      // },
   },

   SIDmQuocGia: {
      code: "ma_quoc_gia",
      where: null,
      cols: "ma_quoc_gia",
      value: "ma_quoc_gia",
      text: "ten_quoc_gia",
      format: "{ma_quoc_gia} - {ten_quoc_gia}",
      tableName: "SIDmQuocGia",
      localData: KeyStorage.CacheData,
      infoPopup: {
         title: "Danh sách quốc gia",
         grid: [
            { col: "ma_quoc_gia", name: "Mã quốc gia", width: 200 },
            { col: "ten_quoc_gia", name: "Tên quốc gia", width: "100%" },
         ],
      },
   },

   SIDmTinh: {
      code: "ma_tinh",
      where: null,
      cols: "ma_tinh",
      value: "ma_tinh",
      text: "ten_tinh",
      format: "{ma_tinh} - {ten_tinh}",
      tableName: "SIDmTinh",
      localData: KeyStorage.CacheData,
      infoPopup: {
         title: "Danh sách tỉnh/thành phố",
         grid: [
            { col: "ma_tinh", name: "Mã tỉnh", width: 200 },
            { col: "ten_tinh", name: "Tên tỉnh", width: "100%" },
         ],
      },
   },

   SIDmHuyen: {
      code: "ma_huyen",
      where: null,
      cols: "ma_huyen",
      value: "ma_huyen",
      text: "ten_huyen",
      format: "{ma_huyen} - {ten_huyen}",
      tableName: "SIDmHuyen",
      localData: KeyStorage.CacheData,
      infoPopup: {
         title: "Danh sách quận/huyện",
         grid: [
            { col: "ma_huyen", name: "Mã huyện", width: 200 },
            { col: "ten_huyen", name: "Tên huyện", width: "100%" },
         ],
      },
   },

   SIDmForm: {
      code: "MA_FORM",
      where: null,
      cols: "ma_form",
      value: "ma_form",
      text: "ten_form",
      format: "{ma_form} - {ten_form}",
      localData: KeyStorage.CacheData,
      tableName: "SIDmForm",
      //localData: KeyStorage.CacheData,
      // infoPopup: {
      //    title: "Danh sách quận/huyện",
      //    grid: [
      //       { col: "ma_huyen", name: "Mã huyện", width: 200 },
      //       { col: "ten_huyen", name: "Tên huyện", width: "100%" },
      //    ],
      // },
   },

   Ma_bp: {
      code: "ma_bp",
      where: null,
      cols: "ma_bp,ten_bp",
      value: "ma_bp",
      text: "ten_bp",
      format: "{ma_bp} - {ten_bp}",
      tableName: "SiDmBp",
      localData: KeyStorage.CacheData,
      infoPopup: {
         title: "Danh sách bộ phận",
         grid: [
            { col: "ma_bp", name: "Mã bộ phận", width: 200 },
            { col: "ten_bp", name: "Tên bộ phận", width: "100%" },
         ],
      },
   },
   Ma_hd: {
      code: "ma_hd",
      where: null,
      cols: "ma_hd,ten_hd",
      value: "ma_hd",
      text: "ten_hd",
      format: "{ma_hd} - {ten_hd}",
      tableName: "sidmhd",
      infoPopup: {
         title: "Danh sách hợp đồng",
         grid: [
            { col: "ma_hd", name: "Mã hợp đồng", width: 200 },
            { col: "ten_hd", name: "Tên hợp đồng", width: "100%" },
         ],
      },
   },
   Ma_nhhd: {
      code: "ma_nhhd",
      where: null,
      cols: "ma_nhhd,ten_nhhd",
      value: "ma_nhhd",
      text: "ten_nhhd",
      showFields: "ma_nhhd,ten_nhhd",
      format: "{ma_nhhd} - {ten_nhhd}",
      tableName: "sidmnhhd",
      localData: KeyStorage.CacheData,
      infoPopup: {
         title: "Danh sách nhóm hợp đồng",
         grid: [
            { col: "ma_nhhd", name: "Mã nhóm", width: 200 },
            { col: "ten_nhhd", name: "Tên nhóm", width: "100%" },
         ],
      },
   },
   Ma_nt: {
      code: "ma_nt",
      where: null,
      cols: "ma_nt,ten_nt",
      value: "ma_nt",
      text: "ma_nt",
      tableName: "sidmnt",
      localData: KeyStorage.CacheData,
   },
   Ma_ngh: {
      code: "ma_ngh",
      where: null,
      top: null,
      cols: "ma_ngh,ten_ngh",
      value: "ma_ngh",
      text: "ten_ngh",
      tableName: "sidmngh",
      showFields: "ma_ngh,ten_ngh",
      format: "{ma_ngh} - {ten_ngh}",
      localData: KeyStorage.CacheData,
      infoPopup: {
         title: "Danh sách ngân hàng",
         grid: [
            { col: "ma_ngh", name: "Mã", width: 200 },
            { col: "ten_ngh", name: "Tên", width: "100%" },
         ],
      },
   },
   Ma_phi: {
      code: "ma_phi",
      where: null,
      cols: "ma_phi,ten_phi",
      value: "ma_phi",
      text: "ten_phi",
      tableName: "sidmphi",
      format: "{ma_phi} - {ten_phi}",
      localData: KeyStorage.CacheData,
      infoPopup: {
         title: "Danh sách phí",
         grid: [
            { col: "ma_phi", name: "Mã", width: 200 },
            { col: "ten_phi", name: "Tên", width: "100%" },
         ],
      },
   },
   // ===================================== PM

   PMDmNhomDa: {
      code: "ma_nhda",
      where: null,
      cols: "ma_nhda,ten_nhda,ghi_chu",
      value: "ma_nhda",
      text: "ten_nhda",
      localData: KeyStorage.CacheData,
      tableName: "PMDmNhomDa",
   },

   PMDmGiaiDoanDa: {
      code: "ma_gdoan_da",
      where: null,
      cols: "ma_gdoan_da,ten_gdoan_da,ordinal",
      value: "ma_gdoan_da",
      text: "ten_gdoan_da",
      localData: KeyStorage.CacheData,
      tableName: "PMDmGiaiDoanDa",
   },

   PMDuAn: {
      code: "ma_du_an",
      where: null,
      cols: "ma_du_an,ten_du_an",
      value: "ma_du_an",
      text: "ten_du_an",
      localData: KeyStorage.CacheData,
      tableName: "PMDmDA",
      infoPopup: {
         title: "Danh sách dự án",
         grid: [
            { col: "ma_du_an", name: "Mã", width: 200 },
            { col: "ten_du_an", name: "Tên", width: "100%" },
         ],
      },
   },

   PMDmDAHangMuc: {
      code: "ma_hang_muc",
      where: null,
      cols: "id",
      value: "id",
      text: "id",
      tableName: "PMDmDAHangMuc",
   },

   // ===================================== PO
   Ma_cp: {
      code: "ma_cp",
      where: null,
      cols: "ma_cp,ten_cp,tt_pb",
      value: "ma_cp",
      text: "ten_cp",
      tableName: "PODmCp",
      localData: KeyStorage.CacheData,
      infoPopup: {
         title: "Danh sách chi phí",
         grid: [
            { col: "ma_cp", name: "Mã", width: 200 },
            { col: "ten_cp", name: "Tên", width: "100%" },
         ],
      },
   },

   // ===================================== SO
   Ma_thue: {
      code: "ma_thue",
      where: null,
      cols: "ma_thue,ten_thue,ts_gtgt",
      value: "ma_thue",
      text: "ma_thue",
      tableName: "SoDmTs",
      localData: KeyStorage.CacheData,
      infoPopup: {
         title: "Danh sách thuế",
         grid: [
            { col: "ma_thue", name: "Mã thuế", width: 200 },
            { col: "ten_thue", name: "Tên thuế", width: "100%" },
            { col: "ts_gtgt", name: "Thuế suất(%)", width: 120 },
         ],
      },
   },

   // ===================================== IN
   Ma_dvt: {
      code: "DVT",
      where: null,
      cols: "dvt,ten_dvt",
      value: "dvt",
      text: "ten_dvt",
      tableName: "InDmDvt",
      localData: KeyStorage.CacheData,
      infoPopup: {
         title: "Đơn vị tính",
         grid: [{ col: "ten_dvt", name: "Đơn vị tính", width: "100%" }],
      },
   },
   Ma_nhvt: {
      code: "ma_nhvt",
      where: null,
      cols: "ma_nhvt,ten_nhvt",
      value: "ma_nhvt",
      text: "ten_nhvt",
      showFields: "ma_nhvt,ten_nhvt",
      format: "{ma_nhvt} - {ten_nhvt}",
      tableName: "InDmNhvt",
      localData: KeyStorage.CacheData,
      infoPopup: {
         title: "Danh sách nhóm hàng hóa",
         grid: [
            { col: "ma_nhvt", name: "Mã nhóm", width: 200 },
            { col: "ten_nhvt", name: "Tên nhóm", width: "100%" },
         ],
      },
   },
   Ma_plvt: {
      code: "ma_plvt",
      where: null,
      cols: "ma_plvt,ten_plvt",
      value: "ma_plvt",
      text: "ten_plvt",
      showFields: "ma_plvt,ten_plvt",
      format: "{ma_plvt} - {ten_plvt}",
      tableName: "InDmPlvt",
      localData: KeyStorage.CacheData,
      infoPopup: {
         title: "Danh sách phân loại hàng hóa",
         grid: [
            { col: "ma_plvt", name: "Mã loại", width: 200 },
            { col: "ten_plvt", name: "Tên loại", width: "100%" },
         ],
      },
   },
   Ma_kho: {
      code: "ma_kho",
      where: null,
      cols: "ma_kho,ten_kho",
      value: "ma_kho",
      text: "ten_kho",
      format: "{ma_kho} - {ten_kho}",
      tableName: "InDmKho",
      localData: KeyStorage.CacheData,
      infoPopup: {
         title: "Danh sách kho",
         grid: [
            { col: "ma_kho", name: "Mã kho", width: 200 },
            { col: "ten_kho", name: "Tên kho", width: "100%" },
         ],
      },
   },
   Ma_lo: {
      code: "ma_lo",
      where: null,
      cols: "ma_lo,ten_lo",
      value: "ma_lo",
      text: "ten_lo",
      showFields: "ma_lo,ten_lo",
      format: "{ma_lo} - {ten_lo}",
      tableName: "InDmLo",
      infoPopup: {
         title: "Danh sách lô",
         grid: [
            { col: "ma_lo", name: "Mã lô", width: 200 },
            { col: "ten_lo", name: "Tên lô", width: "100%" },
         ],
      },
   },
   Ma_vitri: {
      code: "ma_vitri",
      where: null,
      cols: "ma_vitri,ten_vitri,ma_kho",
      value: "ma_vitri",
      text: "ten_vitri",
      showFields: "ma_vitri,ten_vitri",
      format: "{ma_vitri} - {ten_vitri}",
      tableName: "InDmViTri",
      infoPopup: {
         title: "Danh sách vị trí",
         grid: [
            { col: "ma_vitri", name: "Mã vị trí", width: 200 },
            { col: "ten_vitri", name: "Tên vị trí", width: "100%" },
            { col: "ma_kho", name: "Mã kho", width: 200 },
         ],
      },
   },

   // =================================== CO
   CoDmSP: {
      code: "ma_spct",
      cols: 'ma_spct,ten_spct,dvt',
      value: 'ma_spct',
      text: "ten_spct",
      showFields: "ma_spct, ten_spct, dvt",
      format: "{ma_spct} - {ten_spct}",
      action: {
         add: false,
         edit: false,
      },
      infoPopup: {
         title: "Danh sách sản phẩm",
         grid: [
            { col: "ma_spct", name: "Mã sản phẩm", width: 200 },
            { col: "ten_spct", name: "Tên sản phẩm", width: "50%" },
            { col: "dvt", name: "Đơn vị tính", width: "50%" },
         ]
      }
   },
   CODmNhSP: {
      code: "ma_nhspct",
      where: null,
      cols: 'ma_nhspct, ten_nhspct',
      value: 'ma_nhspct',
      text: "ma_nhspct",
      tableName: "CODmNhSP",
      showFields: "ma_nhspct",
      format: "{ma_nhspct} - {ten_nhspct}",
      // localData: KeyStorage.CacheData,
      infoPopup: {
         title: "Danh sách nhóm sản phẩm",
         grid: [
            { col: "ma_nhspct", name: "Nhóm mẹ", width: 200 },
            { col: "ten_nhspct", name: "Tên nhóm sản phẩm công trình", width: 250 },
         ]
      }
   },

   // ===================================== CRM
   Ma_nguon_kh: {
      code: "MA_NGUON_KH",
      where: null,
      cols: 'ma_nguon_kh,ten_nguon_kh',
      value: 'ma_nguon_kh',
      text: "ten_nguon_kh",
      tableName: "CRMNguonKh",
      localData: KeyStorage.CacheData,
      infoPopup: {
         title: "Nguồn khách hàng",
         grid: [
            { col: "ma_nguon_kh", name: "Mã nguồn KH", width: 250 },
            { col: "ten_nguon_kh", name: "Tên nguồn KH", width: "100%" },
         ],
      },
   },

   Ma_linh_vuc: {
      code: "MA_LINH_VUC",
      where: null,
      cols: 'ma_linh_vuc,ten_linh_vuc',
      value: 'ma_linh_vuc',
      text: "ten_linh_vuc",
      tableName: "CRMLinhVuc",
      localData: KeyStorage.CacheData,
      infoPopup: {
         title: "Danh sách lĩnh vực",
         grid: [
            { col: "ma_linh_vuc", name: "Mã lĩnh vực", width: 250 },
            { col: "ten_linh_vuc", name: "Tên lĩnh vực", width: "100%" },
         ],
      },
   },

   Ma_hoat_dong: {
      code: "MA_HOAT_DONG",
      where: null,
      cols: "ma_hoat_dong,ten_hoat_dong,ordinal,ket_qua",
      value: 'ma_hoat_dong',
      text: "ten_hoat_dong",
      localData: KeyStorage.CacheData,
      tableName: "CRMSetupHoatDong",
   },

   CRMHoatDong: {
      code: "CRMHoatDong",
      where: null,
      cols: 'id',
      value: 'id',
      text: "id",
      tableName: "CRMHoatDong",
   },
   CRMDeal: {
      code: "MA_DEAL",
      where: null,
      cols: 'id,ma_deal,ten_deal',
      valueName: "id",
      value: 'ma_deal',
      text: "ten_deal",
      localData: KeyStorage.CacheData,
      tableName: "CRMDeal",
      infoPopup: {
         title: "Danh sách cơ hội",
         grid: [
            { col: "ma_deal", name: "Mã", width: 200 },
            { col: "ten_deal", name: "Tên", width: "100%" },
         ],
      },
   },
   Ma_uu_tien: {
      code: "MA_UU_TIEN_DEAL",
      where: null,
      cols: 'ma_uu_tien',
      value: 'ma_uu_tien',
      text: "ten_uu_tien",
      localData: KeyStorage.CacheData,
      tableName: "CRMDealUuTien",
   },
   Ma_giai_doan: {
      code: "MA_GIAI_DOAN",
      where: null,
      cols: 'ma_giai_doan,ten_giai_doan',
      value: 'ma_giai_doan',
      text: "ten_giai_doan",
      localData: KeyStorage.CacheData,
      tableName: "CRMDealGiaiDoan",
   },
   // ===================================== GL
   TK: {
      code: "tk",
      where: null,
      cols: "tk,ten_tk,bac_tk",
      value: "tk",
      text: "ten_tk",
      format: "{tk} - {ten_tk}",
      tableName: "GlDmTk",
      localData: KeyStorage.CacheData,
      infoPopup: {
         title: "Danh sách tài khoản",
         grid: [
            { col: "tk", name: "Mã TK", width: 150 },
            { col: "ten_tk", name: "Tên TK", width: "100%" },
         ],
      },
   },

   Ma_nv: {
      code: "ma_nv",
      where: null,
      cols: "ma_nv,ten_nv",
      value: "ma_nv",
      text: "ten_nv",
      tableName: "FADMNV",
      infoPopup: {
         title: "Danh sách nhân viên",
         grid: [
            { col: "ma_nv", name: "Mã nhân viên", width: 200 },
            { col: "ten_nv", name: "Tên nhân viên", width: "100%" },
         ],
      },
   },

   Ma_nhts: {
      code: "ma_nhts",
      where: null,
      cols: "ma_nhts,ten_nhts",
      value: "ma_nhts",
      text: "ten_nhts",
      tableName: "FADMNHTS",
      localData: KeyStorage.CacheData,
      infoPopup: {
         title: "Danh sách nhóm tài sản",
         grid: [
            { col: "ma_nhts", name: "Mã nhóm", width: 200 },
            { col: "ten_nhts", name: "Tên nhóm", width: "100%" },
         ],
      },
   },

   Ma_ldtg: {
      code: "ma_ldtg",
      where: null,
      cols: "ma_ldtg,ten_ldtg,tg",
      value: "ma_ldtg",
      text: "ten_ldtg",
      tableName: "FADMLDTG",
      localData: KeyStorage.CacheData,
      infoPopup: {
         title: "Danh sách mã tăng/giảm",
         grid: [
            { col: "ma_ldtg", name: "Mã", width: 200 },
            { col: "ten_ldtg", name: "Tên", width: "100%" },
         ],
      },
   },
   Ma_bpsd: {
      code: "ma_bpsd",
      where: null,
      cols: "ma_bpsd,ten_bpsd",
      value: "ma_bpsd",
      text: "ten_bpsd",
      tableName: "FADMBPSD",
      infoPopup: {
         title: "Danh sách BPSD",
         grid: [
            { col: "ma_bpsd", name: "Mã BPSD", width: 200 },
            { col: "ten_bpsd", name: "Tên BPSD", width: "100%" },
         ],
      },
   },

   // not lookup
   Ma_kc: {
      code: "ma_kc",
      where: null,
      cols: "tk",
      value: "tk",
      text: "tk",
      tableName: "GlDmKc",
   },

   Ma_httt_po: {
      code: "ma_httt_po",
      codeDb: "ma_httt",
      where: `moduleid = 'PO'`,
      cols: "ma_httt,ten_httt,moduleid,tk,tk_thue_gtgt_mua",
      value: "ma_httt",
      text: "ten_httt",
      tableName: "SiDmHttt",
      showFields: "ma_httt,ten_httt",
      format: "{ma_httt} - {ten_httt}",
      infoPopup: {
         title: "Phương thức thanh toán",
         grid: [
            { col: "ma_httt", name: "Mã HTTT", width: 200 },
            { col: "ten_httt", name: "Tên HTTT", width: "100%" },
         ],
      },
   },
   Ma_httt: {
      code: "ma_httt",
      where: ``,
      cols: "ma_httt,ten_httt,moduleid,tk,tk_thue_gtgt_ban,tk_thue_gtgt_mua",
      value: "ma_httt",
      text: "ten_httt",
      tableName: "SiDmHttt",
      format: "{ma_httt} - {ten_httt}",
      localData: KeyStorage.CacheData,
      infoPopup: {
         title: "Phương thức thanh toán",
         grid: [
            { col: "ma_httt", name: "Mã HTTT", width: 200 },
            { col: "ten_httt", name: "Tên HTTT", width: "100%" },
         ],
      },
   },
   Ma_httt_so: {
      code: "ma_httt_so",
      codeDb: "ma_httt",
      where: `moduleid = 'SO'`,
      cols: "ma_httt,ten_httt,moduleid,tk",
      value: "ma_httt",
      text: "ten_httt",
      tableName: "SiDmHttt",
      showFields: "ma_httt,ten_httt",
      format: "{ma_httt} - {ten_httt}",
      infoPopup: {
         title: "Phương thức thanh toán",
         grid: [
            { col: "ma_httt", name: "Mã HTTT", width: 200 },
            { col: "ten_httt", name: "Tên HTTT", width: "100%" },
         ],
      },
   },

   Ma_ncc: {
      code: "ma_ncc",
      codeDb: "ma_kh",
      where: "isncc = 1",
      cols: "ma_kh,ten_kh,dia_chi,ma_so_thue,nguoi_gd,ma_httt_po",
      value: "ma_kh",
      text: "ten_kh",
      format: "{ma_kh} - {ten_kh}",
      tableName: "ArDmKh",
      infoPopup: {
         title: "Danh sách nhà cung cấp",
         grid: [
            { col: "ma_kh", name: "Mã NCC", width: 200 },
            { col: "ten_kh", name: "Tên NCC", width: "100%" },
         ],
      },
   },
   Ma_nvkd: {
      code: "ma_nvkd",
      where: null,
      cols: "ma_nvkd,ten_nvkd",
      value: "ma_nvkd",
      text: "ten_nvkd",
      tableName: "SoDmNVKD",
      format: "{ma_nvkd} - {ten_nvkd}",
      localData: KeyStorage.CacheData,
      infoPopup: {
         title: "Danh sách nhân viên kinh doanh",
         grid: [
            { col: "ma_nvkd", name: "Mã NVKD", width: 200 },
            { col: "ten_nvkd", name: "Tên NVKD", width: "100%" },
         ],
      },
   },

   // not lookup
   Ma_gia_ban: {
      code: "ma_gia_ban",
      where: null,
      cols: "ma_vt,gia",
      value: "ma_vt",
      text: "gia",
      tableName: "SoDmGiaBan",
      infoPopup: {
         title: "Danh sách giá bán",
         grid: [
            { col: "ma_vt", name: "Mã hàng hóa", width: 200 },
            { col: "gia", name: "Giá bán", width: "100%", type: "number" },
         ],
      },
   },

   // not lookup
   Ma_glcdtk: {
      code: "GLCDTK",
      where: null,
      cols: "nam,tk",
      value: "nam",
      text: "tk",
      tableName: "GLCDTK",
   },

   // not lookup
   Ma_arcdkh: {
      code: "ARCDKH",
      where: null,
      cols: "nam,tk",
      value: "nam",
      text: "tk",
      tableName: "ARCDKH",
   },

   // not lookup
   Ma_incdvt: {
      code: "INCDVT",
      where: null,
      cols: 'nam,ma_vt',
      value: 'nam',
      text: "ma_vt",
      tableName: "INCDVT",
   },

   Ma_vt: {
      code: "ma_vt",
      where: null,
      cols: 'ts_gtgt,ma_kho,ma_thue,dvt,gia_ton,ton_kho,tk_dt,tk_vt,tk_gv,tk_ck,tk_tl,loai',
      value: 'ma_vt',
      text: "ten_vt",
      tableName: "indmvt",
      format: "{ma_vt} - {ten_vt}",
      formatInput: "{ma_vt}",
      localData: KeyStorage.CacheData,
      infoPopup: {
         title: "Danh sách hàng hóa",
         grid: [
            { col: "ma_vt", name: "Mã hàng hóa", width: 150 },
            { col: "ten_vt", name: "Tên hàng hóa", width: "100%" },
            { col: "dvt", name: "Đvt", width: 100 },
         ]
      }
   },

   Ma_incdfifo: {
      code: "INCDFIFO",
      where: null,
      cols: 'nam, ma_kho',
      value: 'nam',
      text: 'ma_kho',
      tableName: "INCDFIFO"
   },

   Ma_tt: {
      code: "ma_tt",
      where: null,
      cols: 'ma_tt,mo_ta',
      value: 'ma_tt',
      text: "mo_ta",
      tableName: "SODMTT",
      localData: KeyStorage.CacheData,
      infoPopup: {
         title: "Danh sách mã thanh toán",
         grid: [
            { col: "ma_tt", name: "Mã TT", width: 150 },
            { col: "mo_ta", name: "Mô tả", width: "100%" },
         ]
      }
   },
   Ma_loai_vt: {
      code: "MA_LOAI_VT",
      where: null,
      cols: 'id,mo_ta',
      value: 'id',
      text: "mo_ta",
      tableName: "InDmLoaiVt",
      localData: KeyStorage.CacheData,
      infoPopup: {
         title: "Danh sách loại hàng hóa",
         grid: [
            { col: "id", name: "Loại ID", width: 150 },
            { col: "mo_ta", name: "Mô tả", width: "100%" },
         ]
      }
   },

   Ma_loai_giaton: {
      code: "Ma_loai_giaton",
      where: "language = 'vi-VN'",
      cols: 'id,mo_ta',
      value: 'id',
      text: "mo_ta",
      tableName: "InDmLoaiGiaTon",
      localData: KeyStorage.CacheData,
   },
   Ma_ts: {
      code: "ma_ts",
      where: null,
      cols: 'ma_ts,ten_ts,dvt',
      value: 'ma_ts',
      text: "ten_ts",
      tableName: "fadmts",
      showFields: "ma_ts,ten_ts",
      format: "{ma_ts} - {ten_ts}",
      infoPopup: {
         title: "Danh sách tài sản",
         grid: [
            { col: "ma_ts", name: "Mã tài sản", width: 200 },
            { col: "ten_ts", name: "Tên tài sản", width: "100%" },
         ]
      }
   },
   Ma_spct: {
      code: "ma_spct",
      where: null,
      cols: 'ma_spct,ten_spct',
      value: 'ma_spct',
      text: "ten_spct",
      tableName: "CoDmSpct",
      format: "{ma_spct} - {ten_spct}",
      infoPopup: {
         title: "Danh sách sản phẩm",
         grid: [
            { col: "ma_spct", name: "Mã SPCT", width: 200 },
            { col: "ten_spct", name: "Tên SPCT", width: "50%" },
            // { col: "dvt", name: "Đơn vị tính", width: "50%" }
         ]
      }
   },
   Ma_nhspct: {
      code: "ma_nhspct",
      where: null,
      cols: 'ma_nhspct,ten_nhspct',
      value: 'ma_nhspct',
      text: "ten_nhspct",
      tableName: "CoDmSpct",
      format: "{ma_nhspct} - {ten_nhspct}",
      infoPopup: {
         title: "Danh sách Nhóm SPCT",
         grid: [
            { col: "ma_nhspct", name: "Mã nhóm SPCT", width: 200 },
            { col: "ten_nhspct", name: "Tên nhóm SPCT", width: "100%" },
         ]
      }
   },
   // not lookup
   Ma_ts1: {
      code: "ma_ts_tgng",
      where: null,
      cols: 'ma_ts,ma_ts_tgng,dien_giai',
      value: 'ma_ts_tgng',
      text: "dien_giai",
      tableName: "fadmts1",
   },
   // not lookup
   Ma_giam_ts: {
      code: "ma_giam_ts",
      where: 'is_giam = 1',
      cols: 'ma_ts,ten_ts,dvt',
      value: 'ma_ts',
      text: "ten_ts",
      tableName: "fadmts",
   },
   // not lookup
   Ma_thoikhts: {
      code: "MA_THOIKHTS",
      where: null,
      cols: 'ma_ts,ngay_dung_kh,ngay_kh_lai',
      value: 'ngay_dung_kh',
      text: "ngay_kh_lai",
      tableName: "FaDungKh",
   },
   // not lookup
   Ma_fadckh: {
      code: "MA_FADCKH",
      where: null,
      cols: 'ma_ts,nam,thang',
      value: 'ma_ts',
      text: "ma_ts",
      tableName: "fakhts",
   },
   Ma_Km_luong: { 
      code:"Ma_Km_luong",
      where: "",
      cols: "code_name,ma,ten",
      value: "ma",
      text: "ten",
      tableName: "MaKmLuong",
      format: "{ma} - {ten}",
      infoPopup: {
         title: "Khoản mục lương hệ thống",
         grid:
            [
               { col: "ma", name: "Mã khoản mục", width: 200 },
               { col: "ten", name: "Tên khoản mục", width: "100%" },
            ],
      },
   },
   // not lookup
   Nam_tc: {
      code: "SiDmNamTc",
      where: null,
      cols: 'nam,ngay_dntc,ngay_cntc',
      value: 'nam',
      text: "nam",
      tableName: "SIDmNamTC",
      localData: KeyStorage.CacheData,
      infoPopup: {
         title: "Danh sách năm tài chính",
         grid: [
            { col: "nam", name: "Năm", width: 200 },
            { col: "description", name: "Mô tả", width: "100%" },
         ],
      },

   },
   Ma_cc: {
      code: "ma_cc",
      where: "tg = 1",
      cols: 'ma_cc,ten_cc,id,stt_rec',
      value: 'stt_rec',
      text: "ten_cc",
      // showFields: "ma_cc,ten_cc",
      // format: "{ma_cc} - {ten_cc}",
      tableName: "VFADMCC",
      infoPopup: {
         title: "Danh sách công cụ dụng cụ",
         grid: [
            { col: "ma_cc", name: "Mã công cụ", width: 200 },
            { col: "ten_cc", name: "Tên công cụ", width: "100%" },
         ]
      }
   },
   // not lookup
   Ma_bh_cc: {
      code: "ma_bh_cc",
      where: "tg = 0",
      cols: 'ma_cc,ten_cc,id_cc,stt_rec',
      value: 'stt_rec',
      text: "ten_cc",
      tableName: "VFADMCC",
   },
   Ma_ct: {
      code: "ma_ct",
      where: null,
      cols: 'ma_ct,ten_ct',
      value: 'ma_ct',
      text: "ma_ct",
      tableName: "sidmct",
      showFields: "ma_ct,ten_ct",
      format: "{ma_ct} - {ten_ct}",
      localData: KeyStorage.CacheData,
      infoPopup: {
         title: "Danh sách chứng từ",
         grid: [
            { col: "ma_ct", name: "Mã CT", width: 200 },
            { col: "ten_ct", name: "Tên CT", width: "100%" },
         ]
      }
   },
   // not lookup
   Ma_tgnt: {
      code: "MA_TGNT",
      where: null,
      cols: 'ma_nt,ten_nt,ngay_tg',
      value: 'ma_nt',
      text: "ma_nt",
      tableName: "sidmtgnt",
   },
   // not lookup
   Ma_pb: {
      code: "MA_PB",
      where: null,
      cols: 'id,tk_pb,ten_tk_pb',
      value: 'id',
      text: "tk_pb",
      tableName: "vcodmpb",
   },
   // not lookup
   GLBS_MA_SO: {
      code: "GLBS_MA_SO",
      where: null,
      cols: 'stt,ma_so,chi_tieu',
      value: 'ma_so',
      text: "ma_so",
      tableName: "GLMAUBCTC02",
   },
   Ma_sp: {
      code: "MA_SP",
      where: null,
      cols: 'ma_sp,ten_sp',
      value: 'ma_sp',
      text: "ten_sp",
      tableName: "CODMBOM",
   },
   ID_NV: {
      code: "ID_NV",
      where: null,
      cols: "dien_thoai,email,loai_hsns,ma_nvns",
      value: "id_nv",
      text: "ho_ten",
      format: "{id_nv} - {ho_ten}",
      tableName: "HRHsNs",
      localData: KeyStorage.CacheData,
      infoPopup: {
         title: "Danh sách nhân viên",
         grid: [
            { col: "id_nv", name: "Id nv", width: 200 },
            { col: "ho_ten", name: "Tên", width: 500 },
            { col: "dien_thoai", name: "Điện thoại", width: "100%" },
            { col: "ma_nvns", name: "Mã nvns", width: "100%" },
         ],
      },
   },
   CaDmKu: {
      code: "ma_ku",
      where: "",
      cols: "ma_ku,ten_ku",
      value: "ma_ku",
      text: "ten_ku",
      showFields: "ma_ku,ten_ku",
      format: "{ma_ku} - {ten_ku}",
      tableName: "CaDmKu",
      infoPopup: {
         title: "Danh sách khế ước",
         grid: [
            { col: "ma_ku", name: "Mã", width: 200 },
            { col: "ten_ku", name: "Tên", width: "100%" },
         ],
      },
   },
   CaCdKu: {
      code: "ma_sd_ku",
      where: "",
      cols: "ma_ku,nam",
      value: "ma_ku,nam",
      text: "nam",
      format: "{nam} - {ma_ku}",
      infoPopup: {
         title: "Danh sách số dư khế ước",
         grid: [{ col: "ma_ku", name: "Mã", width: 200 }],
      },
   },

   CODMCPTT: {
      code: "ma_cptt",
      where: "",
      cols: "ma_cptt,ten_cptt",
      value: "ma_cptt",
      text: "ten_cptt",
      tableName: "CODMCPTT",
      format: "{ma_cptt} - {ten_cptt}",
      infoPopup: {
         title: "Danh mục chi phí trả trước",
         grid: [
            { col: "ma_cptt", name: "Mã", width: 200 },
            { col: "ten_cptt", name: "Tên", width: "100%" },
         ],
      },
   },
   SoDmMHD: {
      code: "so_seri_mhd",
      where: null,
      cols: 'so_seri_mhd,so_seri',
      value: 'so_seri_mhd',
      text: "so_seri_mhd - so_seri",
      tableName: "SODMMHD",
   },
   CrmContact: {
      code: "CrmContact",
      where: null,
      cols: '',
      value: '',
      text: "",
      tableName: "SODMMHD",
   },
   WMDmLoaiCV: {
      code: "ma_loai_cv",
      where: null,
      cols: 'ma_loai_cv,ten_loai_cv,phai_bc,ma_form_bc,form_schema',
      value: 'ma_loai_cv',
      text: "ten_loai_cv",
      localData: KeyStorage.CacheData,
      tableName: "WMDmLoaiCV",
   },
   WMDmGiaiDoanCV: {
      code: "ma_gdoan_cv",
      where: null,
      cols: 'ma_gdoan_cv,isclose',
      value: 'ma_gdoan_cv',
      text: "ten_gdoan_cv",
      localData: KeyStorage.CacheData,
      tableName: "WMDmGiaiDoanCV",
   },
   WMDmUuTienCV: {
      code: "MA_UU_TIEN_CV",
      where: null,
      cols: 'ma_uu_tien',
      value: 'ma_uu_tien',
      text: "ten_uu_tien",
      localData: KeyStorage.CacheData,
      tableName: "WMDmUuTienCV",
   },
   WMCongViec: {
      code: "WMCongViec",
      where: "",
      cols: "",
      value: "",
      text: "",
      tableName: "WMCongViec",
   },
   Ma_ky_luong: {
      code: "ma_ky_luong",
      where: "",
      cols: "ma_ky_luong,ten_ky_luong,nam,ngay_bd,ngay_kt",
      value: "ma_ky_luong",
      text: "ten_ky_luong",
      tableName: "PADmKyLuong",
      format: "{ma_ky_luong} - {ten_ky_luong}",
      infoPopup: {
         title: "Danh sách kỳ lương",
         grid:
            [
               { col: "ma_ky_luong", name: "Mã kỳ lương", width: 200 },
               { col: "ten_ky_luong", name: "Tên kỳ lương", width: "100%" },
            ],
      },
   },

   Ma_gd: {
      code: "MA_GD",
      where: null,
      cols: "ma_ct,ma_gd,mo_ta,ngam_dinh",
      value: 'ma_gd',
      text: "mo_ta",
      tableName: "dmmagd",
      showFields: "ma_gd,mo_tda",
      format: "{ma_gd} - {mo_ta}",
      localData: KeyStorage.CacheData,
      infoPopup: {
         title: "Danh mục mã giao dịch",
         grid: [
            { col: "ma_gd", name: "Mã GD", width: 200 },
            { col: "mo_ta", name: "Mô tả", width: "100%" },
         ]
      }
   },

   PADieuChinh: {
      code: "PADieuChinh",
      where: null,
      cols: "id",
      value: "id",
      text: "id",
      tableName: "PADieuChinh",
   },
   PAKyHieuChamCong: {
      code: "Ky_hieu",
      where: "",
      cols: "ky_hieu,mo_ta",
      value: "ky_hieu",
      text: "ky_hieu",
      tableName: "PAKyHieuChamCong",
      localData: KeyStorage.CacheData,
   },
   PABangLuong: {
      code: "PABangLuong",
      where: "",
      cols: "id",
      value: "id",
      text: "id",
      tableName: "PABangLuong",
   },
   PADmLoaiBangLuong: {
      code: "MA_LOAI_BANG_LUONG",
      where: "",
      cols: "ma_loai_bang_luong,ten_loai_bang_luong",
      value: "ma_loai_bang_luong",
      text: "ten_loai_bang_luong",
      tableName: "PADmLoaiBangLuong",
      format: "{ma_loai_bang_luong} - {ten_loai_bang_luong}",
      infoPopup: {
         title: "Danh mục loại bảng lương",
         grid:
            [
               { col: "ma_loai_bang_luong", name: "Mã loại bảng lương", width: 200 },
               { col: "ten_loai_bang_luong", name: "Tên loại bảng lương", width: "100%" },
            ],
      },
   },

   Menus: {
      code: "Menus",
      where: null,
      cols: "id",
      value: "id",
      text: "id",
      tableName: "Menus",
   },

   trang_thai: {
      code: "TRANG_THAI",
      where: null,
      cols: "trang_thai,mo_ta,ngam_dinh,ma_ct",
      value: 'trang_thai',
      text: "mo_ta",
      tableName: "vCoreDmTrangThai",
      showFields: "trang_thai,mo_ta",
      format: "{trang_thai} - {mo_ta}",
      infoPopup: {
         title: "Danh mục trạng thái",
         grid: [
            { col: "trang_thai", name: "Trạng thái", width: 200 },
            { col: "mo_ta", name: "Mô tả", width: "100%" },
         ]
      }
   },

   SysDashLet: {
      code: "id",
      where: null,
      cols: "id",
      value: "id",
      text: "id",
      tableName: "SysDashLet",
   },

   SysDashBoardTemplate: {
      code: "SysDashBoardTemplate",
      where: null,
      cols: "id",
      value: "id",
      text: "id",
      tableName: "SysDashBoardTemplate",
   },

   PostMessage: {
      code: "PostMessage",
      where: "",
      cols: "",
      value: "",
      text: "",
      tableName: "",
   },

   User: {
      code: "USER",
      where: null,
      cols: "id,full_name,isadmin,email",
      value: 'id',
      text: "full_name",
      tableName: "user",
   },

   UserName: {
      code: "USER",
      where: null,
      cols: "id,full_name,isadmin,email",
      value: 'full_name',
      text: "full_name",
      tableName: "userid",
   },

   PAHangSo: {
      code: "PAHangSo",
      where: null,
      cols: "id,id_nv,ma_ky_luong,ma_km,gia_tri,ghi_chu",
      value: 'ma_ky_luong,id_nv',
      text: "ma_ky_luong",
      tableName: "Hằng số lương",
   },

   EODmLoaiPost: {
      code: "ma_loai_post",
      where: null,
      cols: "ma_loai_post,ten_loai_post,ten_user_duyets,user_duyets,id_cctcs,sua_sau_ph,trao_doi_sau_ph",
      value: 'ma_loai_post',
      text: "ten_loai_post",
      tableName: "Danh sách loại thông báo",
   },

   ContactKHEmail: {
      code: "ContactKH",
      where: null,
      cols: "ten_lien_he,email,email_others,dien_thoai,dia_chi,ma_kh",
      value: 'email',
      text: "email",
      tableName: "vContactKH",
      showFields: "email,ten_lien_he",
      format: "{ten_lien_he} - {email}",
      infoPopup: {
          title: "Danh sách email liên hệ khách hàng",
          grid: [
              { col: "ten_lien_he", name: "Trạng thái", width: 200 },
              { col: "email", name: "Mô tả", width: "100%" },
          ]
      }
   }
}
