import { IntlFormat, LanguageFormat } from '../../../utils/intlFormat';
import { Mess_Report, Mess_Voucher } from './variable';
import { default as ComponentInfo_VIOld } from './vi-vn.json';
import { default as ComponentInfo_ENOld } from './en-us.json';

// vi
const ComponentInfo_VI = {
   ...ComponentInfo_VIOld,
   ...IntlFormat.setMessageLanguage(Mess_Report),
   ...IntlFormat.setMessageLanguage(Mess_Voucher),
}

// en
const ComponentInfo_EN = {
   ...ComponentInfo_ENOld,
   ...IntlFormat.setMessageLanguage(Mess_Report, LanguageFormat.en),
   ...IntlFormat.setMessageLanguage(Mess_Voucher, LanguageFormat.en),
}

export { ComponentInfo_VI, ComponentInfo_EN }
