const prefixRpt = "report.";
const prefixVch = "voucher.";

export const Mess_Report = {
  Stt: {
    id: prefixRpt + "Stt",
    defaultMessage: "Stt",
    en: "No",
    fr: "",
    ch: "",
  },
  MaKh: {
    id: prefixRpt + "MaKh",
    defaultMessage: "Mã khách hàng",
    en: "Customer code",
    fr: "",
    ch: "",
  },
  TenKh: {
    id: prefixRpt + "TenKh",
    defaultMessage: "Tên khách hàng",
    en: "Customer name",
  },
  MaVt: {
    id: prefixRpt + "MaVt",
    defaultMessage: "Mã VT",
  },
  TenVt: {
    id: prefixRpt + "TenVt",
    defaultMessage: "Tên VT",
  },
  Dvt: {
    id: prefixRpt + "Dvt",
    defaultMessage: "Đvt",
  },
  MaKho: {
    id: prefixRpt + "MaKho",
    defaultMessage: "Mã kho",
  },
  MaLo: {
    id: prefixRpt + "MaLo",
    defaultMessage: "Mã lô",
  },
  MaHd: {
    id: prefixRpt + "MaHd",
    defaultMessage: "Mã hợp đồng",
  },
  TenHd: {
    id: prefixRpt + "TenHd",
    defaultMessage: "Tên hợp đồng",
  },
  MaNvkd: {
    id: prefixRpt + "MaNvkd",
    defaultMessage: "Mã NVKD",
  },
  MaBp: {
    id: prefixRpt + "MaBp",
    defaultMessage: "Mã bộ phận",
  },
  MaNcc: {
    id: prefixRpt + "MaNcc",
    defaultMessage: "Mã NCC",
  },
  TenNcc: {
    id: prefixRpt + "TenNcc",
    defaultMessage: "Tên NCC",
  },
  MaNhvt: {
    id: prefixRpt + "MaNhvt",
    defaultMessage: "Mã nhóm VT",
  },
  MaPhi: {
    id: prefixRpt + "MaPhi",
    defaultMessage: "Mã phí",
  },
  MaKu: {
    id: prefixRpt + "MaKu",
    defaultMessage: "Mã khế ước",
  },
  MaNt: {
    id: prefixRpt + "MaNt",
    defaultMessage: "Mã NT",
  },
  MaCt: {
    id: prefixRpt + "MaCt",
    defaultMessage: "Mã CT",
  },
  MaSPCT: {
    id: prefixRpt + "MaSPCT",
    defaultMessage: "Mã SPCT",
  },
  NgayCt: {
    id: prefixRpt + "NgayCt",
    defaultMessage: "Ngày Ct",
  },
  NgayLct: {
    id: prefixRpt + "NgayLct",
    defaultMessage: "Ngày lập Ct",
  },
  NgayGhiSo: {
    id: prefixRpt + "NgayGhiSo",
    defaultMessage: "Ngày ghi sổ",
  },
  SoCt: {
    id: prefixRpt + "SoCt",
    defaultMessage: "Số Ct",
  },
  DienGiai: {
    id: prefixRpt + "DienGiai",
    defaultMessage: "Diễn giải",
  },
  TaiKhoan: {
    id: prefixRpt + "TaiKhoan",
    defaultMessage: "Tài khoản",
  },
  TkDu: {
    id: prefixRpt + "TkDu",
    defaultMessage: "TK Đ/Ư",
  },
  TkNo: {
    id: prefixRpt + "TkNo",
    defaultMessage: "TK nợ",
  },
  TkCo: {
    id: prefixRpt + "TkCo",
    defaultMessage: "TK có",
  },
  TyGia: {
    id: prefixRpt + "TyGia",
    defaultMessage: "Tỷ giá",
  },
  PhatSinh: {
    id: prefixRpt + "PhatSinh",
    defaultMessage: "Phát sinh",
  },
  PhaiThu: {
    id: prefixRpt + "PhaiThu",
    defaultMessage: "Phải thu",
  },
  PhaiTra: {
    id: prefixRpt + "PhaiTra",
    defaultMessage: "Phải trả",
  },
  DauKy: {
    id: prefixRpt + "DauKy",
    defaultMessage: "Đầu kỳ",
  },
  CuoiKy: {
    id: prefixRpt + "CuoiKy",
    defaultMessage: "Cuối kỳ",
  },
  NoDauky: {
    id: prefixRpt + "NoDauky",
    defaultMessage: "Nợ đầu kỳ",
  },
  NoCuoiky: {
    id: prefixRpt + "NoCuoiky",
    defaultMessage: "Nợ cuối kỳ",
  },
  DuNoDauky: {
    id: prefixRpt + "DuNoDauky",
    defaultMessage: "Dư nợ ĐK",
  },
  DuCoDauKy: {
    id: prefixRpt + "DuCoDauKy",
    defaultMessage: "Dư có ĐK",
  },
  PsNo: {
    id: prefixRpt + "PsNo",
    defaultMessage: "PS nợ",
  },
  PsCo: {
    id: prefixRpt + "PsCo",
    defaultMessage: "PS có",
  },
  SoDu: {
    id: prefixRpt + "SoDu",
    defaultMessage: "Số Dư",
  },
  PsDauKy: {
    id: prefixRpt + "PsDauKy",
    defaultMessage: "PS đầu kỳ",
  },
  PsCuoiKy: {
    id: prefixRpt + "PsCuoiKy",
    defaultMessage: "PS cuối kỳ",
  },
  DuNoCuoiKy: {
    id: prefixRpt + "DuNoCuoiKy",
    defaultMessage: "Dư nợ CK",
  },
  DuCoCuoiKy: {
    id: prefixRpt + "DuCoCuoiKy",
    defaultMessage: "Dư có CK",
  },
  SoLuong: {
    id: prefixRpt + "SoLuong",
    defaultMessage: "Số lượng",
  },
  SoLuongMin: {
    id: prefixRpt + "SoLuongMin",
    defaultMessage: "SL nhỏ nhất",
  },
  SoLuongMax: {
    id: prefixRpt + "SoLuongMax",
    defaultMessage: "SL lớn nhất",
  },
  ThanhTien: {
    id: prefixRpt + "ThanhTien",
    defaultMessage: "Thành tiền",
  },
  DonGia: {
    id: prefixRpt + "DonGia",
    defaultMessage: "Đơn giá",
  },
  ThanhTienNt: {
    id: prefixRpt + "ThanhTien",
    defaultMessage: "Thành tiền NT",
  },
  DonGiaNt: {
    id: prefixRpt + "DonGia",
    defaultMessage: "Đơn giá NT",
  },
  GiaTri: {
    id: prefixRpt + "GiaTri",
    defaultMessage: "Giá trị",
  },
  Thu: {
    id: prefixRpt + "Thu",
    defaultMessage: "Thu(gửi vào)",
  },
  Chi: {
    id: prefixRpt + "Chi",
    defaultMessage: "Chi(rút ra)",
  },
  Ton: {
    id: prefixRpt + "Ton",
    defaultMessage: "Tồn",
  },
  ChiPhi: {
    id: prefixVch + "ChiPhi",
    defaultMessage: "Chi phí",
  },
  ThueNk: {
    id: prefixVch + "ThueNk",
    defaultMessage: "Thuế Nk",
  },
  ThueGtgt: {
    id: prefixVch + "ThueGtgt",
    defaultMessage: "Thuế Gtgt",
  },
  TienHang: {
    id: prefixVch + "TienHang",
    defaultMessage: "Tiền hàng",
  },
  TongTien: {
    id: prefixVch + "TongTien",
    defaultMessage: "Tổng tiền",
  },
  TongThanhToan: {
    id: prefixVch + "TongThanhToan",
    defaultMessage: "Tổng thanh toán",
  },
  MaHttt: {
    id: prefixRpt + "MaHttt",
    defaultMessage: "Mã Httt",
  },
  MaNhom: {
    id: prefixRpt + "MaNhom",
    defaultMessage: "Mã nhóm",
  },
  TenNhom: {
    id: prefixRpt + "TenNhom",
    defaultMessage: "Tên nhóm",
  },
  MaChiTiet: {
    id: prefixRpt + "MaChiTiet",
    defaultMessage: "Mã chi tiết",
  },
  TenChiTiet: {
    id: prefixRpt + "TenChiTiet",
    defaultMessage: "Tên chi tiết",
  },
  TonDau: {
    id: prefixVch + "TonDau",
    defaultMessage: "Tồn đầu",
  },
  DuDau: {
    id: prefixVch + "DuDau",
    defaultMessage: "Dư đầu",
  },
  TonCuoi: {
    id: prefixVch + "TonCuoi",
    defaultMessage: "Tồn cuối",
  },
  DuCuoi: {
    id: prefixVch + "DuCuoi",
    defaultMessage: "Dư cuối",
  },
  SoLuongN: {
    id: prefixVch + "SoLuongN",
    defaultMessage: "SL nhập",
  },
  SoLuongX: {
    id: prefixVch + "SoLuongX",
    defaultMessage: "SL xuất",
  },
  TienN: {
    id: prefixVch + "TienN",
    defaultMessage: "Tiền nhập",
  },
  TienX: {
    id: prefixVch + "TienX",
    defaultMessage: "Xuất",
  },
  VatTu: {
    id: prefixVch + "VatTu",
    defaultMessage: "Vật tư",
  },
  Stt: {
    id: prefixVch + "Stt",
    defaultMessage: "Stt",
  },
  TienVon: {
    id: prefixVch + "TienVon",
    defaultMessage: "Tiền vốn",
  },
  ChietKhau: {
    id: prefixVch + "ChietKhau",
    defaultMessage: "Chiết khấu",
  },
  ChietKhauDS: {
    id: prefixVch + "ChietKhauDS",
    defaultMessage: "Chiết khấu DS",
  },
  TienBan: {
    id: prefixVch + "TienBan",
    defaultMessage: "Tiền bán",
  },
  TienSauCK: {
    id: prefixVch + "TienSauCK",
    defaultMessage: "Tiền sau CK",
  },
  TienLai: {
    id: prefixVch + "TienLai",
    defaultMessage: "Tiền lãi",
  },
  TonSL: {
    id: prefixVch + "TonSL",
    defaultMessage: "Tồn SL",
  },
  TonGiaTri: {
    id: prefixVch + "TonGiaTri",
    defaultMessage: "Ton giá trị",
  },
};

export const Mess_Voucher = {
  ChiTiet: {
    id: prefixVch + "detail",
    defaultMessage: "ChiTiết",
    en: "Detail",
  },
  TabCP: {
    id: prefixVch + "tab_cp",
    defaultMessage: "Chi phí",
  },
  TabTAIN: {
    id: prefixVch + "tab_tain",
    defaultMessage: "Thuế",
  },
  TabTonKho: {
    id: prefixVch + "tab_ton",
    defaultMessage: "Tồn kho",
  },
  TabTAOUT: {
    id: prefixVch + "tab_taout",
    defaultMessage: "Thuế ra",
  },
  In: {
    id: prefixVch + "print",
    defaultMessage: "In",
  },
  SuaPhieu: {
    id: prefixVch + "print",
    defaultMessage: "Sửa phiếu",
  },
  LuuVaDong: {
    id: prefixVch + "print",
    defaultMessage: "Lưu và đóng",
  },
  Valid_MaCp: {
    id: prefixVch + "ma_cp_required",
    defaultMessage: "Mã CP không được để trống",
  },
  Valid_TenCp: {
    id: prefixVch + "ten_cp_required",
    defaultMessage: "Tên CP không được để trống",
  },
  Valid_KiHieu: {
    id: prefixVch + "so_seri0_required",
    defaultMessage: "Kí hiệu không được để trống",
  },
  Valid_SoHD: {
    id: prefixVch + "so_ct0_required",
    defaultMessage: "Số HĐ không được để trống",
  },
  Valid_NgayHd: {
    id: prefixVch + "ngay_ct0_required",
    defaultMessage: "Ngày HĐ không được để trống",
  },
  Valid_TkDu: {
    id: prefixVch + "tk_du_required",
    defaultMessage: "TK Đ/Ư không được để trống",
  },
};
