import React, { memo, useEffect, useState } from "react";
import PropTypes from 'prop-types';
import { Form, Select } from "semantic-ui-react";
import { ApiConfig } from "../../../Api";
import { ZenHelper } from "../../../utils";

const DropdownTrangThai = (props) => {
   const [data, setData] = useState([])
   const [error, setError] = useState()
   const { label, defaultlabel, placeholder, defaultplaceholder,
      clearable, readOnly, maCT, onChange, onItemSelected, formik, ...rest } = props

   useEffect(() => {
      initData()
   }, [])

   useEffect(() => {
      if (data.length > 0) {
         if (formik && !formik.initialValues[rest.name]) {
            const defaultItem = data.find(x => x.value && Boolean(x['ngam_dinh']) === true)
            formik.setFieldValue(rest.name, defaultItem.value)
         }
      }
   }, [data, formik.initialValues[rest.name]])

   function initData() {
      const { required, name } = props

      ApiConfig.getTrangThaiByCT(maCT, res => {
         if (res.status >= 200 && res.status <= 204) {
            let options = ZenHelper.convertToSelectOptions(res.data.data, required ? false : true,
               'trang_thai', 'trang_thai', 'mo_ta', ['ngam_dinh'])
            setData(options)
         } else {
            setError(ZenHelper.getResponseError(res))
         }
      })
   }

   const handleChange = (e, elementInput) => {
      const { name, value, options } = elementInput

      const itemSelected = options.find(x => x.value === value)
      if (formik) {
         formik.setFieldValue(name, value)
         onItemSelected ? onItemSelected(itemSelected || {}, { value: value, name: name }) : undefined
      } else {
         onChange(e, elementInput)
      }
   }

   return <Form.Field control={Select}
      fluid deburr wrapSelection
      options={data} name={rest.name}
      selectOnBlur={false} clearable={clearable ? clearable : readOnly ? false : true}
      selectOnNavigation={false}

      open={readOnly ? false : undefined}
      searchInput={{
         id: rest.name,
         readOnly: readOnly,
      }}

      onChange={handleChange}

      error={error ? error : formik && (formik.touched[rest.name] && formik.errors[rest.name])}
      placeholder={(placeholder || label) ?
         ZenHelper.GetMessage((placeholder || label),
            defaultplaceholder || defaultlabel)
         : undefined}

      label={
         label ? ZenHelper.GetMessage(
            label,
            defaultlabel || defaultplaceholder
         ) : undefined
      }
      {...rest}
      value={formik ? formik.values[rest.name] : rest.value}
   />
}
export default memo(DropdownTrangThai, (prevProps, nextProps) => {
   const { formik, name, value, readOnly } = prevProps
   if (value !== nextProps.value
      || readOnly !== nextProps.readOnly
      || (formik && formik.touched[name] !== nextProps.formik.touched[name])
      || (formik && formik.errors[name] !== nextProps.formik.errors[name])
      || (formik && formik.values[name] !== nextProps.formik.values[name])
   ) {
      return false
   }
   return true
})

DropdownTrangThai.propTypes = {
   clearable: PropTypes.bool,
   readOnly: PropTypes.bool,

   formik: PropTypes.any,
   onItemSelected: PropTypes.func,
   onChange: PropTypes.func,
};