import React, { memo, useEffect, useState } from "react";
import PropTypes from 'prop-types';
import { Form, Select } from "semantic-ui-react";
import { ApiConfig } from "../../../Api";
import { ZenHelper } from "../../../utils";
import { GlobalStorage, KeyStorage } from "../../../utils/storage";

const DropdownMagd = (props) => {
   const [data, setData] = useState([])
   const [error, setError] = useState()
   const { label, defaultlabel, placeholder, defaultplaceholder,
      clearable, readOnly, maCT, onChange, onItemSelected, formik, defaultValue, ...rest } = props

   useEffect(() => {
      initData()
   }, [])

   useEffect(() => {
      if (data.length > 0) {
         if (formik && !formik.initialValues[rest.name]) {
            if(defaultValue == "" || defaultValue) {
               formik.setFieldValue(rest.name, defaultValue)
            } 
            if(JSON.stringify(formik.values) !== '{}'){
               const defaultItem = data.find(x => x.value && Boolean(x['ngam_dinh']) === true)
               formik.setFieldValue(rest.name, formik.values.ma_gd)
            }
            if(JSON.stringify(formik.values) !== '{}' || !defaultValue) {
               const defaultItem = data.find(x => x.value && Boolean(x['ngam_dinh']) === true)
               formik.setFieldValue(defaultItem)
            }
         }
      }
   }, [data, formik.initialValues[rest.name]])

   function initData() {
      const { required, name } = props
      // kiểm tra cacheData
      if (GlobalStorage.checkDataLookup({ localData: KeyStorage.CacheData, code: "dmmagd" })) {
         const dataLocal = GlobalStorage.getByField(KeyStorage.CacheData, "dmmagd")
         const dataByMaCt = dataLocal.data.filter(t => t.ma_ct == maCT)

         let options = ZenHelper.convertToSelectOptions(dataByMaCt, required ? false : true,
            'ma_gd', 'ma_gd', 'mo_ta', ['ngam_dinh'])
         setData(options)
      }
      // get từ api
      else {
         ApiConfig.getMaGdByCT(maCT, res => {
            if (res.status >= 200 && res.status <= 204) {
               let options = ZenHelper.convertToSelectOptions(res.data.data, required ? false : true,
                  'ma_gd', 'ma_gd', 'mo_ta', ['ngam_dinh'])
               setData(options)

            } else {
               setError(ZenHelper.getResponseError(res))
            }
         })
      }
   }

   const handleChange = (e, elementInput) => {
      const { name, value, options } = elementInput

      const itemSelected = options.find(x => x.value === value)
      if (formik) {
         formik.setFieldValue(name, value)
         onItemSelected ? onItemSelected(itemSelected || {}, { value: value, name: name }) : undefined
      } else {
         onChange(e, elementInput)
      }
   }

   return <Form.Field control={Select}
      fluid deburr wrapSelection
      options={data} name={rest.name}
      selectOnBlur={false} clearable={clearable ? clearable : readOnly ? false : true}
      selectOnNavigation={false}
      default
      open={readOnly ? false : undefined}
      searchInput={{
         id: rest.name,
         readOnly: readOnly,
      }}

      onChange={handleChange}

      error={error ? error : formik && (formik.touched[rest.name] && formik.errors[rest.name])}
      placeholder={(placeholder || label) ?
         ZenHelper.GetMessage((placeholder || label),
            defaultplaceholder || defaultlabel)
         : undefined}

      label={
         label ? ZenHelper.GetMessage(
            label,
            defaultlabel || defaultplaceholder
         ) : undefined
      }
      {...rest}
      value={formik ? (formik.values[rest.name] || "") : rest.value}
   />
}
export default memo(DropdownMagd, (prevProps, nextProps) => {
   const { formik, name, value, readOnly } = prevProps
   if (value !== nextProps.value
      || readOnly !== nextProps.readOnly
      || (formik && formik.touched[name] !== nextProps.formik.touched[name])
      || (formik && formik.errors[name] !== nextProps.formik.errors[name])
      || (formik && formik.values[name] !== nextProps.formik.values[name])
   ) {
      return false
   }
   return true
})

DropdownMagd.propTypes = {
   clearable: PropTypes.bool,
   readOnly: PropTypes.bool,

   formik: PropTypes.any,
   onItemSelected: PropTypes.func,
   onChange: PropTypes.func,
};