import { Company } from "../../Company/Company"
import { ReportCenter } from "../../ReportCenter/ReportCenter"
import { SetupModules } from "../../Setting/SetupModules/SetupModules"
import { HrDmOther } from "../../HrDmOther/HrDmOther_Form/HrDmOther"
import { HrHsNsDetail } from "../../HrDmOther/HrDmOther_Form/HrHsNs_Detail"
import { ARDmKhDetail } from "../../AR/Dictionary/ARDmKhDetail.js/ARDmKhDetail"
import { CRM_Other } from "../../CRM/Other"
import { HrHsNsQuery } from "../../HrDmOther/HrDmOther_Form/HRHsNsQuery"
import { PMDuAnDetail } from "../../PM/PMOther_Form/PMDuAnDetail"
import { HrLichDayWeek } from "../../HrDmOther/HrDmOther_Form/HrLichDay/HrLichDayWeek"
import { HrLichDayMonth } from "../../HrDmOther/HrDmOther_Form/HrLichDay/HrLichDayMonth"
import { SIDmFormDetail } from "../../SI/Dictionary/SIDmFormDetail/SIDmFormDetail"
import { HrChamCongGv } from "../../HrDmOther/HrDmOther_Form/HrLichDay/HrChamCongGv"
import { HrChamCong } from "../../HrDmOther/HrDmOther_Form/HrLichDay/HrChamCong"
import { PADmLoaiBangLuongDetail } from "../../PA/Dictionary/PADmLoaiBangLuongDetail/PADmLoaiBangLuongDetail"
import { SetupMail } from "../../Setting/SetupMail/Email"
import { PABangLuongDetail } from "../../PA/Dictionary/PABangLuongDetail.js/PABangLuongDetail"
import { HrLichDayMonthGv } from "../../HrDmOther/HrDmOther_Form/HrLichDay/HrLichDayMonthGv"
import { PostMessageDetail, PostMessageNew } from "../../PostMessage/PostMessageDetail"
import { WMCongViecCal } from "../../WM/Orther/WMCongViecCal"
import { CustomDashboard } from "../../Dashboard/Dictionary/Orther/Dashboard"

const ZenFormInfo = {
    ...SetupModules,
    ...ReportCenter,
    ...HrDmOther,
    ...HrHsNsDetail,
    ...HrHsNsQuery,
    ...CRM_Other,
    ...PMDuAnDetail,
    ...HrLichDayWeek,
    ...HrLichDayMonth,
    ...HrLichDayMonthGv,
    ...HrChamCongGv,
    ...HrChamCong,
    ...PADmLoaiBangLuongDetail,
    ...PABangLuongDetail,
    Company: Company,
    ARDmKhDetail: ARDmKhDetail,
    SIDmFormDetail: SIDmFormDetail,
    SetupMail: SetupMail,
    PostMessageDetail: PostMessageDetail,
    PostMessageNew: PostMessageNew,
    ...WMCongViecCal,
    CustomDashboard: CustomDashboard,
}

export { ZenFormInfo }