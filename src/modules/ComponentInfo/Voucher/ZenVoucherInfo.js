
// import { GL_Voucher, GL_VoucherEdit } from "../../GL/Voucher/index";
import { CA_Voucher, CA_VoucherEdit } from "../../CA/Voucher"
import { SO_Voucher_CRM,SO_VoucherEdit_CRM } from "../../CRM/Voucher"
import { PA_Voucher,PA_VoucherEdit } from "../../PA/Voucher"
import { SO_Voucher,SO_VoucherEdit } from "../../SO/Voucher"

const ZenVoucherInfo = {
    // ...GL_Voucher,
    ...SO_Voucher_CRM,
    ...SO_Voucher,
    ...PA_Voucher,
    ...CA_Voucher,
}

const ZenVoucherInfoEdit = {
    // ...GL_VoucherEdit,
    ...SO_VoucherEdit_CRM,
    ...SO_VoucherEdit,
    ...PA_VoucherEdit,
    ...CA_VoucherEdit,
}
export { ZenVoucherInfo, ZenVoucherInfoEdit }