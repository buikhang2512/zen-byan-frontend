import React, { useEffect, useRef, useState } from "react";
import { Form, Icon, Divider, Button, Label, Breadcrumb, Segment, TextArea, Container, Grid, Checkbox, Header, List, Radio, Input } from "semantic-ui-react";
import _ from "lodash";
import { isEmpty } from "lodash-es";

import { ApiFileAttachment, ApiPostMessage } from "../../Api";
import {
   ZenFieldDate, ZenFileViewer,
   ZenMessageToast, ZenFormik, ZenField, DiscussComment, ZenFieldTextArea, ContainerScroll, ZenFieldSelectApi, ZenFieldCheckbox,
   ZenFieldSelectApiMulti, ZenMessageAlert, ZenLoading, RelativeTime, Helmet, SegmentHeader, HeaderLink, ZenButton, ZenFieldSelect
} from '../../components/Control/index'
import { ZenHelper, auth } from "../../utils";
import { zzControlHelper } from "../../components/Control/zzControlHelper";
import { ZenLookup } from "../ComponentInfo/Dictionary/ZenLookup";
import { IntlFormat } from "../../utils/intlFormat";
import { Mess_Post } from "./zLanguage/variable";
import * as routes from "../../constants/routes";
import { optionsPostStatus } from "./ConstVariable";
import { FormattedMessage } from "react-intl";
import TextDraftEditor from "./DraftEditor/TextDraftEditor"
import * as permissions from "../../constants/permissions";
import { convertFromHTML } from "draft-convert";
import { convertToRaw } from "draft-js";
import draftToHtml from "draftjs-to-html";

class PostMessageDetailForm extends React.Component {
   constructor(props) {
      super();
      this._isMounted = true
      this.refZenFormik = React.createRef();
      this.postId = zzControlHelper.atobUTF8(props.match.params?.id)
      this.userInfo = auth.getUserInfo();

      this.state = {
         permissionView: props.action.view ? auth.checkPermission(props.action.view.permission) : true,
         permissionAdd: props.action.add ? auth.checkPermission(props.action.add.permission) : true,
         permissionEdit: props.action.edit ? auth.checkPermission(props.action.edit.permission) : true,
         permissionDelete: props.action.delete ? auth.checkPermission(props.action.delete.permission) : true,

         loadingForm: this.postId ? true : false,
         btnLoadingSave: false,
         error: "",

         // object post
         items: {
            ma_loai_post: "",
            title: "",
            content: "",
            str_participants: ",", // danh sách người xem thông báo: "user1,user2,..."
            duyet_user: [],
            user_duyet: "",
            polls: [],
            participants: [],
            cau_hoi: "",
            poll_expired: "",
            has_poll: false,
            sua_sau_ph: false,
            trao_doi_sau_ph: false,
         },

         user_ids: [],

         str_participants_Original: "",

         // list comment
         activities: [],

         readOnlyContent: this.postId ? true : false,
         content: "",
         comment: "",
      }
   }

   initState(state) {
      this.setState({
         btnLoadingSave: false,
         loadingForm: false,
         error: "",
         ...state
      })
   }

   loadData() {
      const postData = new Promise((resolve, reject) => {
         ApiPostMessage.getByCode(this.postId, res => {
            if (res.status === 200) {
               resolve(res.data.data)
            } else {
               reject(res)

            }
         })
      })

      const participants = new Promise((resolve, reject) => {
         ApiPostMessage.getParticipants(this.postId, res => {
            if (res.status === 200) {
               resolve(res.data.data)
            } else {
               reject(res)
            }
         })
      })
      // const postComment = new Promise((resolve, reject) => {
      //    ApiPostMessage.getActivities(this.postId, res => {
      //       if (res.status === 200) {
      //          resolve(res.data.data)
      //       } else {
      //          reject(res)
      //       }
      //    })
      // })
      Promise.all([postData, participants])
         .then(values => {
            const temp = this.convertParticipantsToStr(values[0])
            this.initState({
               items: { ...values[0], str_participants: temp },
               participants: values[1],
               content: values[0].content
            })
            let items = []
            for (let item of values[1]) {
               items.push(item.user_name)
            }
            this.setState({ user_ids: items })
         })
         .catch(err => {
            this.initState({
               error: ZenHelper.getResponseError(err),
            })
         })
   }

   stringToHTML = function (str) {
      var dom = document.createElement('div');
      dom.innerHTML = str;
      return dom.firstElementChild;
   };

   saveData = (newItem) => {

      // convert to object
      const participants = this.convertParticipantsToObject(newItem)
      const dataSave = {
         ...newItem,
         // content: this.state.content,
         participants: participants ? participants : [],
      }

      const formData = new FormData()
      if (this.postId) { formData.append('id', dataSave.id) }
      formData.append('title', dataSave.title)
      if (dataSave.content && this.stringToHTML(dataSave.content).innerHTML) {
         formData.append('content', dataSave.content)
      } else {
         ZenMessageAlert.warning("Nội dung thông báo không được để trống")
         return
      }
      formData.append('ma_loai_post', dataSave.ma_loai_post)
      formData.append('status', dataSave.status || 0)
      formData.append('duyet_user', dataSave.duyet_user)
      formData.append('user_duyet', dataSave.user_duyet)
      formData.append('cau_hoi', dataSave.cau_hoi)
      formData.append('poll_expired', dataSave.poll_expired)
      formData.append('str_participants', dataSave.str_participants.replace(',',''))
      formData.append('has_poll', dataSave.has_poll)
      formData.append('participants', JSON.stringify(dataSave.participants))

      formData.append('polls', JSON.stringify(dataSave.polls))
      formData.append('trao_doi_sau_ph', dataSave.trao_doi_sau_ph)
      formData.append('sua_sau_ph', dataSave.sua_sau_ph)

      for (let file of newItem.files) {
         formData.append("files", file);
      }
      this.setState({ btnLoadingSave: true })



      if (this.postId) {
         ApiPostMessage.update(formData, res => {
            if (res.status === 200) {
               this.initState({
                  items: { ...this.state.items, ...dataSave },
                  readOnlyContent: true
               })
               ZenMessageToast.success();
               this.loadData();
            } else {
               ZenMessageAlert.error(ZenHelper.getResponseError(res))
               this.initState({
                  error: ZenHelper.getResponseError(res)
               })
            }
         })
      } else {
         ApiPostMessage.insert(formData, res => {
            if (res.status === 201) {
               this.props.history.push(routes.PostMessageDetail(zzControlHelper.btoaUTF8(res.data.data.id)))
               ZenMessageToast.success();
            } else {
               ZenMessageAlert.error(ZenHelper.getResponseError(res))
               this.initState({
                  error: ZenHelper.getResponseError(res)
               })
            }
         })
      }
   }

   convertParticipantsToStr(item) {
      const { participants, str_participants } = item
      if (participants && participants.length > 0) {
         var result = participants.map(t => t.full_name).toString()
         this.setState({ str_participants_Original: str_participants })
      }
      return result
   }

   convertParticipantsToObject(item) {
      const { str_participants } = item
      if (str_participants) {
         var result = str_participants.split(",").map((userName, index) => {
            return {
               post_id: item.id,
               user_name: this.state.user_ids[index] + "",
            }
         })
      }
      return result;
   }

   // ========================   component react
   componentDidMount() {
      if (this.postId) {
         this.loadData()
      }
   }

   componentWillUnmount() {
      this._isMounted = false
   }

   // ========================   handle
   handleSaveForm = (values, form) => {
      this.saveData(values)
   }

   handleComment = () => {
      if (this.state.comment && this.state.comment != `<p><br></p>`) {
         const data = {
            content: this.state.comment
         }
         ApiPostMessage.insertActivities(this.props.id, data, res => {
            if (res.status === 201) {
               this.setState({
                  activities: this.state.activities.concat(res.data.data),
                  comment: ""
               })
            } else {
               ZenMessageAlert.error(ZenHelper.getResponseError(res))
            }
         })
      }
   }

   handleEditContent = ({ name }) => {
      let result = {
         readOnlyContent: true,
         content: ""
      }
      if (name === IconName.Edit) {
         result.readOnlyContent = false
         result.content = this.state.items.content
      }
      this.setState({ ...result })
   }

   handleRemoveComment = (itemRemove) => {
      ZenMessageAlert.question("Bạn có chắc muốn xóa không?")
         .then(success => {
            if (success == 1) {
               ApiPostMessage.deleteActivities(itemRemove.post_id, itemRemove.id, res => {
                  if (res.status === 204) {
                     this.setState(state => {
                        return {
                           activities: state.activities.filter(t => t.id !== itemRemove.id)
                        }
                     })
                  } else {
                     ZenMessageAlert.error(ZenHelper.getResponseError(res))
                  }
               })
            }
         })
   }

   handleTrinhDuyet = () => {
      const patchData = [
         {
            value: 1,
            path: `/status`,
            op: "replace",
            operationType: 0,
         },
      ]
      ApiPostMessage.updatePatch(this.postId, patchData, res => {
         if (res.status === 200) {
            ZenMessageToast.success()
            this.setState({ items: { ...this.state.items, status: 1 } })
         } else {
            ZenMessageAlert.error(ZenHelper.getResponseError(res))
         }
      })
   }

   handleBanHanh = () => {
      ApiPostMessage.publishPost(this.postId, res => {
         if (res.status === 204) {
            ZenMessageToast.success()
            this.setState({ items: { ...this.state.items, status: 2 } })
         } else {
            ZenMessageAlert.error(ZenHelper.getResponseError(res))
         }
      })
   }

   handleDelete = () => {
      ApiPostMessage.delete(this.postId, res => {
         if (res.status === 204) {
            ZenMessageToast.success()
            this.props.history.push(routes.PostMessageNew)
         } else {
            ZenMessageAlert.error(ZenHelper.getResponseError(res))
         }
      })
   }

   render() {
      const { items, activities, error, loadingForm, btnLoadingSave,
         permissionDelete, permissionEdit, permissionAdd, permissionView, participants,
         readOnlyContent, content } = this.state

      const permission = this.postId ? (permissionEdit ? true : false) : permissionAdd

      return (<>
         <Helmet idMessage={Mess_Post.Detail.id}
            defaultMessage={Mess_Post.Detail.defaultMessage} />

         <SegmentHeader>
            <HeaderLink listHeader={linkHeader}>
               <Breadcrumb.Divider icon='right chevron' />
               <Breadcrumb.Section active>
                  {
                     this.postId ? items.title
                        : IntlFormat.text({
                           id: "component.add",
                           defaultMessage: "Thêm mới",
                           values: { mess: "" }
                        })
                  }
               </Breadcrumb.Section>
            </HeaderLink>
            {permission && readOnlyContent
               && <>
                  <div style={{ float: "right" }} >
                     {(items.status == 1 && (!isEmpty(items.user_duyet) || !items.user_duyet == undefined)) && <ZenButton
                        icon="share"
                        content="Trình duyệt"
                        primary
                        size="small"
                        onClick={(e) => this.handleTrinhDuyet()}
                     />}
                     {((items.status == 1 || items.status == 0) &&
                        ((!isEmpty(items.user_duyet) || !items.user_duyet == undefined) ||
                           !isEmpty(_.split(items.user_duyet, ',').filter(x => { return x === this.userInfo.fullName ? true : false })))) ? <ZenButton
                        icon="send"
                        content="Ban hành"
                        primary
                        size="small"
                        onClick={(e) => this.handleBanHanh()}
                     /> : undefined}

                     {((items.status == 1 || items.status == 0) &&
                        (this.userInfo.userId === items.cuser || !isEmpty(_.split(items.user_duyet, ',').filter(x => { return x === this.userInfo.fullName ? true : false })))) ||
                        ((items.status == 2 && items.sua_sau_ph) &&
                           (this.userInfo.userId === items.cuser || !isEmpty(_.split(items.user_duyet, ',').filter(x => { return x === this.userInfo.fullName ? true : false })))) ? <ZenButton
                        primary
                        size="small"
                        icon="pencil"
                        content="Sửa"
                        onClick={() => this.handleEditContent({ name: IconName.Edit })}
                     /> : undefined}
                     {(items.status == 1 || items.status == 0) && this.userInfo.userId === items.cuser && permissionDelete && <ZenButton
                        size="small"
                        btnType="delete"
                        icon="delete"
                        content="Xóa"
                        onClick={() => this.handleDelete()}
                     />}
                  </div>
               </>}
         </SegmentHeader>


         <ZenLoading loading={loadingForm} />
         <div style={{ paddingTop: "1em" }}>
            {
               !this.postId || (this.postId && !readOnlyContent) ?
                  <>
                     <Segment>
                        <FormAdd refFormik={this.refZenFormik}
                           id={this.postId}
                           items={items}
                           onSubmit={this.handleSaveForm}
                           loadingSave={btnLoadingSave}
                           readOnlyContent={readOnlyContent}
                           content={content}
                           onChangeContent={(newValue) => this.setState({ content: newValue })}
                           userInfo={this.userInfo}
                           setUserIds={(e) => { this.setState({ user_ids: e }) }}
                           formButton={
                              this.postId && !readOnlyContent &&
                              <div style={{ margin: "14px 0", textAlign: "right" }}>
                                 <FormButton loadingSave={btnLoadingSave}
                                    onClose={() => this.handleEditContent({ name: IconName.Cancel })}
                                    onSave={(e) => this.refZenFormik.current.handleSubmit(e)}
                                 />
                              </div>
                           }
                        />
                     </Segment>
                  </>
                  : <FormView id={this.postId} items={items}
                     participants={participants}
                     readOnlyContent={readOnlyContent}
                     permission={permission}
                     onEdit={() => this.handleEditContent({ name: IconName.Edit })} />
            }

            {/* {
               this.postId && <FormListComment activities={activities}
                  onChange={(newValue) => this.setState({ comment: newValue })}
                  value={this.state.comment}
                  readOnlyContent={readOnlyContent}
                  userInfo={this.userInfo}
                  onComment={this.handleComment}
                  onRemoveComment={this.handleRemoveComment}
               />
            } */}
         </div>
      </>
      );
   }
}

const FormAdd = ({ id, items, idForm, refFormik,
   formButton, readOnlyContent, content, onChangeContent, onSubmit, loadingSave, userInfo, setUserIds }) => {
   const [checkCauhoi, setCheckCauhoi] = useState()
   const [userDuyetName, setUserDuyetName] = useState()
   const [filesInfo, setFilesInfo] = useState([])
   const refFileInput = useRef();
   const newItem = { noi_dung: '' };

   const handleAddNoiDung = (item) => {
      const itemarr = item ? _.cloneDeep(item) : []
      itemarr.push(newItem)
      refFormik.current.setValues({
         ...refFormik.current.values,
         polls: itemarr
      })
   }

   useEffect(() => {
      refFormik.current.setFieldValue('files', filesInfo)
   }, [filesInfo])

   const handleChangeNoiDung = (item, idx, e, dt) => {
      const newItem = _.cloneDeep(item)
      newItem[idx][dt.name] = dt.value;
      refFormik.current.setFieldValue('polls', newItem)
      //setCgMXH(true)
   }

   const handleRemoveNoiDung = (item, idx) => {
      const itemarr = _.cloneDeep(item)
      _.remove(itemarr, (i, x) => { return x == idx });
      refFormik.current.setValues({
         ...refFormik.current.values,
         polls: itemarr
      })
   }

   const handleSelectFileInput = (e) => {
      e.preventDefault()
      if (e.target.value && e.target.files?.length > 0) {
         const arrFile = []
         for (let index = 0; index < e.target.files.length; index++) {
            const file = e.target.files[index];
            const fileDuplicate = filesInfo?.filter(t => t.name === file.name)
            if (!fileDuplicate || fileDuplicate.length === 0) {
               arrFile.push(file)
            }
         }

         if (filesInfo) {
            setFilesInfo(filesInfo.concat(arrFile))
         } else {
            setFilesInfo(arrFile)
         }
      }
      e.target.value = ""
   }

   return <>
      <div id={'postmesage-component-left'} >
         {/* <ContainerScroll isSegment={false}
            idElementContainer={'postmesage-component-left'}
            id={"left-content"}
         > */}
         <ZenFormik form={idForm} ref={refFormik}
            validation={formatValidation}
            initItem={items}
            onSubmit={onSubmit}
         >
            {formikProps => {
               return <Form id={"form-postmess"}>
                  <Grid columns={2} >
                     <Grid.Column width="4">
                        <ZenFieldSelectApi
                           lookup={ZenLookup.EODmLoaiPost}
                           formik={formikProps}
                           name="ma_loai_post"
                           onItemSelected={(e, i) => {
                              formikProps.setFieldValue('ten_user_duyet', e.ten_user_duyets)
                              formikProps.setFieldValue('user_duyet', e.user_duyets)
                              formikProps.setFieldValue('sua_sau_ph', e.sua_sau_ph)
                              formikProps.setFieldValue('trao_doi_sau_ph', e.trao_doi_sau_ph)
                           }}
                           label="posts.ma_loai_post" defaultlabel="Loại thông báo"
                        />
                        {/* <ZenFieldSelect options={optionsPostStatus}
                              name="status" clearable={false}
                              props={formikProps}
                              {...IntlFormat.label(Mess_Post.Status)}
                           /> */}
                        {
                           formikProps.values.ten_user_duyet && formikProps.values.ten_user_duyet.split(',')?.map((item, index) => {
                              return <>
                                 <Label style={{ marginBottom: "5px" }} basic>{item}</Label>
                              </>
                           })
                        }
                        <br /> <br />
                        {/* <ZenFieldSelectApiMulti lookup={ZenLookup.User} readOnly={true}
                           name="user_duyet" formik={formikProps}
                           label={"posts.user_duyet"} defaultlabel="Người duyệt"
                           onSelectedItem={(e, i) => formikProps.setFieldValue('user_duyet', userDuyet)}
                        /> */}
                        {console.log(formikProps.values)}
                        <ZenFieldSelectApiMulti lookup={ZenLookup.UserName}
                           name="str_participants" formik={formikProps}
                           onSelectedItem={(e, i) => {
                              let items = []
                              for (let item of e) {
                                 items.push(item.id)
                              }
                              setUserIds(items)
                           }}
                           {...IntlFormat.label(Mess_Post.Participant)}
                        />
                        <Checkbox checked={formikProps.values.has_poll} onClick={(e, i) => { formikProps.setFieldValue('has_poll', i.checked) }} label="Tạo thăm dò ý kiến" />
                        <br /> <br />
                        {formikProps.values.has_poll && <>
                           <ZenField
                              name="cau_hoi" props={formikProps}
                              label={"posts.cau_hoi"} defaultlabel="Câu hỏi thăm dò ý kiến"
                           />
                           <ZenFieldDate
                              name="poll_expired" props={formikProps}
                              label={"posts.poll_expired"} defaultlabel="Thời hạn"
                           />
                           {
                              formikProps.values.polls && formikProps.values.polls.map((item, index) => {
                                 return <>
                                    <Form.Group key={'noidung' + index}>
                                       <Form.Field width={16}>
                                          <Form.Input
                                             placeholder={`Nội dung lựa chọn ${index + 1}`}
                                             name='noi_dung'
                                             value={item['noi_dung']}
                                             onChange={(e, data) => handleChangeNoiDung(formikProps.values.polls, index, e, data)}
                                          />
                                       </Form.Field>
                                       <Form.Field>
                                          <ZenButton icon='remove' color='red' basic
                                             onClick={() => handleRemoveNoiDung(formikProps.values.polls, index)}
                                          />
                                       </Form.Field>
                                    </Form.Group>
                                 </>
                              })
                           }
                           {<a href="#" onClick={(e) => {
                              e.preventDefault();
                              handleAddNoiDung(formikProps.values.polls);
                           }}>
                              <Icon style={{ marginBottom: "20px" }} name="add" />
                              Thêm lựa chọn
                           </a>}
                        </>}
                     </Grid.Column>
                     <Grid.Column width="12">
                        <ZenField name="title" props={formikProps}
                           {...IntlFormat.label(Mess_Post.Title)}
                        />
                        <TextDraftEditor
                           placeholder="Nội dung thông báo"
                           readOnly={readOnlyContent}
                           value={items.content}
                           onChange={(e) => formikProps.setFieldValue('content', e)}
                           editorStyle={{ height: "300px", maxHeight: "auto" }}
                           suggestions={[
                              // { text: 'APPLE', value: 'apple', url: 'apple' },
                              // { text: 'BANANA', value: 'banana', url: 'banana' },
                              // { text: 'CHERRY', value: 'cherry', url: 'cherry' },
                              // { text: 'DURIAN', value: 'durian', url: 'durian' },
                              // { text: 'EGGFRUIT', value: 'eggfruit', url: 'eggfruit' },
                              // { text: 'FIG', value: 'fig', url: 'fig' },
                              // { text: 'GRAPEFRUIT', value: 'grapefruit', url: 'grapefruit' },
                              // { text: 'HONEYDEW', value: 'honeydew', url: 'honeydew' },
                           ]}
                        />
                        <br />
                        <div>
                           {
                              filesInfo && filesInfo?.map((file, index) => {
                                 return <Label key={`fu${index}`} as='a' color="grey">
                                    {file.name}
                                    <Icon name="delete" onClick={() => {
                                       setFilesInfo(filesInfo.filter((t, idx) => idx != index))
                                    }} />
                                 </Label>
                              })
                           }
                        </div>
                        <br />
                        <input hidden type="file" multiple
                           ref={refFileInput}
                           onChange={handleSelectFileInput} />
                        <a href="#"
                           onClick={(e) => {
                              e.preventDefault();
                              refFileInput.current.click(e)
                           }}>
                           <Icon name="attach" />
                           File kèm theo
                        </a>
                        {!id && <div style={{ textAlign: "right", paddingTop: "14px" }}>
                           {(!isEmpty(formikProps.values.user_duyet)) && <ZenButton
                              icon="share"
                              content="Trình duyệt"
                              primary size="small"
                              onClick={(e) => {
                                 formikProps.setFieldValue('status', 1)
                                 refFormik.current.handleSubmit(e)
                              }}
                           />}
                           {((!isEmpty(formikProps.values.user_duyet) || formikProps.values.user_duyet == undefined) &&
                              !isEmpty(_.split(formikProps.values.user_duyet, ',').filter(x => { return x === userInfo.fullName ? true : false }))) ? <ZenButton
                              icon="send"
                              content="Ban hành"
                              primary size="small"
                              onClick={(e) => {
                                 formikProps.setFieldValue('status', 2)
                                 refFormik.current.handleSubmit(e)
                              }}
                           /> : undefined}
                           <ZenButton type="submit" btnType="save" size="small" style={{ marginRight: 0 }}
                              loading={loadingSave}
                              onClick={(e) => refFormik.current.handleSubmit(e)}
                           />
                        </div>}
                     </Grid.Column>
                  </Grid>
               </Form>
            }}
         </ZenFormik>
         {formButton}
      </div>
   </>
}

const FormView = ({ id, items, readOnlyContent, permission, onEdit, participants }) => {
   const initViewFile = { fileInfo: "", openView: false }
   const statusName = optionsPostStatus.find(t => t.value == items.status)
   const [viewFile, setViewFile] = useState(initViewFile);
   const { openView, fileInfo } = viewFile
   const [vote, setVote] = useState()

   useEffect(() => {
      ApiPostMessage.seenPosts(id, res => {

      })
   }, [])

   const handleVoteClick = (value) => {
      ApiPostMessage.votePost(items.id, { id: value }, res => {
         if (res.status === 200) {
            setVote(value)
            ZenMessageToast.success()
         } else {
            ZenMessageAlert.warning(ZenHelper.getResponseError(res))
         }
      })
   }

   const handleViewFile = (e, fileAtt) => {
      e.preventDefault();
      setViewFile({ openView: true, fileInfo: fileAtt })
   }
   const handleDownloadFile = (e, fileAtt) => {
      e.preventDefault();
      ApiFileAttachment.download('post', fileInfo.gid,
         res => {
            if (res.status >= 200 && res.status <= 204) {
               const url = window.URL.createObjectURL(new Blob([res.data]));

               const link = document.createElement('a');
               link.href = url;
               link.setAttribute('download', fileInfo.file_name);
               document.body.appendChild(link);
               link.click();
               document.body.removeChild(link);
            } else {
               ZenMessageAlert.error(zzControlHelper.getResponseError(res))
            }
         })
   }
   return <>
      <Grid columns={2} >
         <Grid.Column width="4">
            <Segment style={{ width: "100%", }}>
               <div style={{ width: "100%" }} >
                  <Header as="h3" content="Thông báo tới :" />
                  <List style={{ paddingLeft: "20px" }}>
                     {
                        items.participants && items.participants.map(item => {
                           return <List.Item style={{ paddingBottom: "10px" }}>
                              {item.is_seen ? <strong>
                                 {`${item.full_name} ( đã xem ngày ${zzControlHelper.formatDateTime(item.seen_date, 'DD/MM/YYYY')} )`}
                              </strong> :
                                 item.full_name
                              }
                           </List.Item>
                        })
                     }
                  </List>
                  {
                     items.has_poll && <>
                        <Divider />
                        <Header as="h3" content={`Thăm dò/bình chọn, đến ngày : ${zzControlHelper.formatDateTime(items.poll_expired, 'DD/MM/YYYY')}`} />
                        <div style={{ paddingBottom: "10px" }}>{items?.cau_hoi} </div>
                        <Form>
                           {
                              items.polls && items.polls.map((item, index) => {
                                 return <>
                                    <Form.Field>
                                       <Radio
                                          label={item.noi_dung}
                                          name='radioGroup'
                                          value={item.id}
                                          checked={vote === item.id}
                                          onChange={(e, { value }) => handleVoteClick(value)}
                                       />
                                    </Form.Field>
                                 </>
                              })
                           }
                        </Form>
                     </>
                  }
                  <Divider />
                  <Header as="h3" content={`Thông tin khác :`} />
                  <p>Loại thông báo : <span style={{ fontWeight: "bold" }}>{items.ten_loai_post}</span></p>
                  <p>Người soạn : <span style={{ fontWeight: "bold" }}>{items.cuser_name}</span></p>
                  <p>Ngày soạn : <span style={{ fontWeight: "bold" }}>{zzControlHelper.formatDateTime(items.cdate, 'DD/MM/YYYY')}</span></p>
                  <p>Người duyệt : <span style={{ fontWeight: "bold" }}>{items.duyet_user_name}</span></p>
                  <p>Ngày duyệt : <span style={{ fontWeight: "bold" }}>{parseInt(items.duyet_date) > 1900 ? zzControlHelper.formatDateTime(items.duyet_date, 'DD/MM/YYYY') : ""}</span></p>
                  <p>Trạng thái : <span style={{ fontWeight: "bold" }}>
                     {items.status === 0 ? "Đang dự thảo" : ""}
                     {items.status === 1 ? "Chờ duyệt" : ""}
                     {items.status === 2 ? "Đã ban hành" : ""}
                  </span></p>
               </div>
            </Segment>
         </Grid.Column>

         <Grid.Column width="12">
            <Segment style={{ width: "100%" }}>
               <div style={{ width: "100%", margin: "0 0 14px 0" }}>
                  <label className="pm-title">
                     {items.title}
                  </label>
               </div>

               <div style={{ minHeight: "140px" }}
                  className="pm-row-comment"
                  dangerouslySetInnerHTML={{ __html: items.content }} />

               <Header as={"h3"} content="File kèm theo" />

               <div>
                  <List>
                     {
                        items.arrayFileAttaches?.map((fileAtt, idx) => {
                           //const isRemoveFile = (loading === enumLoading.REMOVEFILE && fileAtt.id === itemComment.id)
                           return <List.Item>
                              <span key={fileAtt.id || idx}
                                 style={{ marginLeft: "7px" }}
                              >
                                 <a
                                    href="#"
                                    onClick={(e) => handleViewFile(e, fileAtt)}
                                 >
                                    <Icon name="file alternate" />
                                    {fileAtt.file_name}
                                 </a>
                              </span>
                           </List.Item>
                        })
                     }
                  </List>
                  <br />
               </div>
               <ZenFileViewer
                  fileInfo={fileInfo}
                  open={openView}
                  onClose={() => setViewFile(initViewFile)}
                  onDownload={handleDownloadFile}
               />
               {items.trao_doi_sau_ph && <DiscussComment codeName="CRM" type={'posts'} keyValue={this.postId} />}
            </Segment>
         </Grid.Column>
      </Grid>

   </>
}

const FormListComment = ({ onChange, activities, value, readOnlyContent,
   userInfo, onComment, onRemoveComment }) => {

   const handleCollapse = () => {
      if (activities && activities.length > 0) {
         const eleListCm = document.getElementById("list-comment")
         const eleIconCollapse = document.getElementById("dropdown-comment")
         // caret up/down
         if (eleListCm.style.maxHeight != "0px") {
            eleListCm.style.maxHeight = 0;
            eleIconCollapse.className = eleIconCollapse.className.replace("caret up", "caret down")
         } else {
            eleListCm.style.maxHeight = eleListCm.scrollHeight + "px";
            eleIconCollapse.className = eleIconCollapse.className.replace("caret down", "caret up")
         }
      }
   }

   function listCommentUI() {
      return <div id={"list-comment"}
         style={{ overflow: "hidden", transition: "max-height 0.2s ease-out" }}
      >
         {
            activities && activities.map(com => {
               return <div key={com.id} className="pm-row-comment">
                  <div style={{ float: "left" }}>
                     <Icon name="user circle" size="big" style={{ margin: 0 }} />
                  </div>

                  {userInfo.userId == com.cuser &&
                     <Icon link name="close" size="small" color="grey"
                        style={{ position: "absolute", right: 7, top: 7, margin: 0 }}
                        onClick={() => onRemoveComment(com)}
                     />
                  }
                  <div className="pm-user-comment">
                     <label className="pm-user-comment-name">
                        {`${com.cuser || "NoName"}`}
                     </label>
                     <RelativeTime value={com.cdate} className="pm-time-comment" />
                  </div>

                  <div style={{ marginLeft: "48px" }}
                     dangerouslySetInnerHTML={{ __html: com.content }} />
               </div>
            })
         }
      </div>
   }

   return <>
      {activities && activities.length > 0 && <div style={{ display: "flex", cursor: "pointer" }}
         onClick={handleCollapse}
      >
         <div style={{ whiteSpace: "nowrap" }}>
            <a>Comment</a>
         </div>

         <Divider style={{ width: "100%" }} />

         <Icon id={"dropdown-comment"} name="caret up" size="large" />
      </div>}

      <div style={{ position: "relative" }}>
         {!readOnlyContent && <div className="z-dimmer" />}

         {listCommentUI()}

         {/* <ZenEditor onChange={onChange} value={value} /> */}
         <Form>
            <TextArea
               onChange={(e, i) => onChangeContent(i.value)}
               value={readOnlyContent ? items.content : content}
               readOnly={readOnlyContent}
            />
         </Form>

         <div style={{ textAlign: "right", marginTop: "14px" }}>
            <Button color="facebook" content="Comment"
               size="small" onClick={onComment} style={{ margin: 0 }}
            />
         </div>
      </div>
   </>
}

const FormButton = ({ loadingSave, onSave, onClose }) => {
   return <>
      <ZenButton btnType="cancel" size="small"
         onClick={onClose}
      />
      <ZenButton btnType="save" size="small" form={"form-postmess"}
         loading={loadingSave} style={{ marginRight: "0px" }}
         onClick={onSave}
      />
   </>
}

const formatValidation = [
   {
      id: "ma_loai_post",
      validationType: "string",
      validations: [
         {
            type: "required",
            params: ["Không được bỏ trống trường này"]
         },
      ]
   },
   {
      id: "title",
      validationType: "string",
      validations: [
         {
            type: "required",
            params: ["Không được bỏ trống trường này"]
         },
      ]
   },
   // {
   //    id: "content",
   //    validationType: "string",
   //    validations: [
   //       {
   //          type: "required",
   //          params: ["Không được bỏ trống trường này"]
   //       },
   //    ]
   // },
]

const IconName = {
   Edit: "edit",
   Cancel: "cancel"
}

const linkHeader = [
   {
      ...IntlFormat.default(Mess_Post.List),
      route: routes.PostMessage,
      active: false,
      isSetting: false,
   }
]

export const PostMessageDetail = {
   route: routes.PostMessageDetail(),
   Zenform: PostMessageDetailForm,
   action: {
      view: { visible: true, permission: permissions.PostMessageXem },
      edit: { visible: true, permission: permissions.PostMessageSua },
      delete: { visible: true, permission: permissions.PostMessageXoa },
   },
}
export const PostMessageNew = {
   route: routes.PostMessageNew,
   Zenform: PostMessageDetailForm,
   action: {
      view: { visible: true, permission: permissions.PostMessageXem },
      add: { visible: true, permission: permissions.PostMessageThem },
   },
}