import React, { useState } from 'react';
import { EditorState, ContentState, convertToRaw, convertFromRaw, convertFromHTML } from 'draft-js';
import { Editor, DraftWYSIWYGEmoji } from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import './TextDraftEditor.css';
import draftToHtml from 'draftjs-to-html';

import DOMPurify from 'dompurify';
import { ApiFileAttachment } from '../../../Api';

import PropTypes from 'prop-types';
import { propTypes } from 'react-json-pretty';

const TextDraftEditor = (props) => {
    const { placeholder, value, onChange, suggestions, ...rest } = props
    const [editorState, setEditorState] = useState(
        () => value && convertFromHTML(value).contentBlocks ? EditorState.createWithContent(ContentState.createFromBlockArray(convertFromHTML(value))) : 
        EditorState.createEmpty()
    );
    const [convertedContent, setConvertedContent] = useState()
    // Chuyển văn bản thành block editor -- ví dụ  editorState={raw}
    let _contentState = ContentState.createFromText(value);
    const raw = convertToRaw(_contentState)

    const handleEditorChange = (state) => {
        setEditorState(state);
        convertContentToHTML();
    }
    const convertContentToHTML = () => {
        let currentContentAsHTML = draftToHtml(convertToRaw(editorState.getCurrentContent()));
        onChange && onChange(currentContentAsHTML);
        setConvertedContent(currentContentAsHTML);
    }


    const createMarkup = (html) => {
        return {
            __html: DOMPurify.sanitize(html)
        }
    }

    function uploadCallback(file) {
        console.log(file)
        return new Promise(
            (resolve, reject) => {
                var reader = new FileReader();
                reader.onloadend = function () {
                    if (file.type.indexOf('image/') === 0) {
                        const formData = new FormData();
                        formData.append('file_upload', file);
                        ApiFileAttachment.uploadEditor(formData, res => {
                            if (res.status === 200) {
                                resolve({ data: { link: res.data.data.baseurl + '/' + res.data.data.files[0].fullPath } });
                            }
                        })
                    }
                }
                reader.readAsDataURL(file);
            }
        );
    }
    const embedCallBack = (embeddedLink, height, width) => {
        if (embeddedLink.indexOf("youtube") >= 0) {
            embeddedLink = embeddedLink.replace("watch?v=", "embed/");
            embeddedLink = embeddedLink.replace("/watch/", "/embed/");
            embeddedLink = embeddedLink.replace("youtu.be/", "youtube.com/embed/");
        } else if (embeddedLink.indexOf("maps") >= 0) {
            embeddedLink = embeddedLink.replace("goo.gl/maps/", "google.com/maps/embed?pb=")
        }
        return embeddedLink
    }

    const config = {
        image: { uploadCallback: uploadCallback },
        embedded: { embedCallback: embedCallBack },
    }

    return (<>
        <div className="DraftEdit">
            {placeholder && <header className="DraftEdit-header">
                {placeholder}
            </header>}
            <Editor
                // class css
                wrapperClassName="DraftEdit-wrapper-class"
                editorClassName="DraftEdit-editor-class"
                toolbarClassName="DraftEdit-toolbar-class"
                // Props Editor
                editorState={editorState}
                onEditorStateChange={handleEditorChange}

                toolbar={config}

                placeholder={placeholder}

                mention={{
                    separator: ' ',
                    trigger: '@',
                    suggestions: suggestions,
                }}

                hashtag={{
                    separator: ' ',
                    trigger: '#',
                    className: 'hashtag-className',
                }}
                {...rest}
            // orther props
            // wrapperStyle={{...wrapperStyle}}
            // editorStyle={{...editorStyle}}
            // toolbarStyle={{...toolbarStyle}}
            // readOnly
            // editor, toolbar, wrapperClassName
            />
        </div>
    </>)
}

TextDraftEditor.prototype = {
    placeholder: PropTypes.any,
    value: PropTypes.any,
    onChange: PropTypes.func,
    wrapperStyle: PropTypes.object,
    toolbarStyle: PropTypes.object,
    editorStyle: PropTypes.object,
    suggestions: PropTypes.array,
}
TextDraftEditor.defaultProps = {
    value: "",
    suggestions: [],
    placeholder: "",
}

export default TextDraftEditor

// Custorm tool bar

// {
//     inline: {
//         visible: true,
//             inDropdown: false,
//                 bold: { visible: true, icon: 'xxx.png', },
//         italic: { visible: true, icon: 'xxx.png', },
//         underline: { visible: true, icon: 'xxx.png', },
//         strikeThrough: { visible: true, icon: 'xxx.png', },
//         monospace: { visible: true, icon: 'xxx.png', },
//     },
//     blockType: { visible: true, },
//     fontSize: { visible: true, icon: 'xxx.png', },
//     fontFamily: { visible: true, },
//     list: {
//         visible: true,
//             inDropdown: true,
//                 unordered: { visible: true, icon: 'xxx.png', },
//         ordered: { visible: true, icon: 'xxx.png', },
//         indent: { visible: true, icon: 'xxx.png', },
//         outdent: { visible: true, icon: 'xxx.png', },
//     },
//     textAlign: {
//         visible: true,
//             inDropdown: true,
//                 left: { visible: true, icon: 'xxx.png', },
//         center: { visible: true, icon: 'xxx.png', },
//         right: { visible: true, icon: 'xxx.png', },
//         justify: { visible: true, icon: 'xxx.png', },
//     },
//     colorPicker: { visible: true, icon: 'xxx.png', },
//     link: {
//         visible: true,
//             inDropdown: true,
//                 addLink: { visible: true, icon: 'xxx.png', },
//         removeLink: { visible: true, icon: 'xxx.png', },
//     },
//     image: {
//         visible: true,
//             icon: 'xxx.png',
//                 fileUpload: true,
//                     url: true,
//           },
//     history: {
//         visible: true,
//             inDropdown: true,
//                 undo: { visible: true, icon: 'xxx.png', },
//         redo: { visible: true, icon: 'xxx.png', },
//     }
// }
