import { IntlFormat } from "../../../utils/intlFormat"
import { Mess_Post } from "./variable"

const local = "en"

const En_US = {
   ...IntlFormat.setMessageLanguage(Mess_Post, local)
}
export default En_US