import { IntlFormat } from "../../../utils/intlFormat";
import { Mess_Post } from "./variable";

const Vi_VN = {
   ...IntlFormat.setMessageLanguage(Mess_Post)
}

export default Vi_VN