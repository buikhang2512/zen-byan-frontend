const prefixPostMess = "PostMes."

export const Mess_Post = {
   List: {
      id: prefixPostMess,
      defaultMessage: "Danh sách thông báo",
      en: ""
   },
   Header: {
      id: prefixPostMess + "header",
      defaultMessage: "Thông báo",
      en: ""
   },
   Detail: {
      id: prefixPostMess + "detail",
      defaultMessage: "Chi tiết thông báo",
      en: ""
   },
   Title: {
      id: prefixPostMess + "title",
      defaultMessage: "Tiêu đề",
      en: ""
   },
   PublishedDate: {
      id: prefixPostMess + "published_date",
      defaultMessage: "Ngày đăng",
      en: ""
   },
   Content: {
      id: prefixPostMess + "content",
      defaultMessage: "Nội dung",
      en: ""
   },
   Participant: {
      id: prefixPostMess + "participant",
      defaultMessage: "Thông báo tới",
      en: ""
   },
   Status: {
      id: prefixPostMess + "status",
      defaultMessage: "Trạng thái",
      en: ""
   },
   CreateNew: {
      id: prefixPostMess + "create_new",
      defaultMessage: "Tạo thông báo mới",
      en: ""
   }
}

