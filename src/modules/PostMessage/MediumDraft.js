// import React, { useRef } from 'react';
// import ReactDOM from 'react-dom';
// import PropTypes from 'prop-types';

// import 'medium-draft/lib/index.css';
// import 'medium-draft/lib/basic.css'

// import {
//   EditorState,
//   convertToRaw,
//   convertFromRaw,
//   KeyBindingUtil,
//   Modifier,
//   AtomicBlockUtils,
//   Entity,
// } from 'draft-js'

// import {
//   Editor,
//   StringToTypeMap,
//   Block,
//   keyBindingFn,
//   createEditorState,
//   addNewBlockAt,
//   addNewBlock,
//   beforeInput,
//   getCurrentBlock,
//   ImageSideButton,
//   rendererFn,
//   HANDLED,
//   NOT_HANDLED
// } from 'medium-draft'

// import {
//   setRenderOptions,
//   blockToHTML,
//   entityToHTML,
//   styleToHTML
// } from 'medium-draft/lib/exporter'

// // import createEmojiPlugin from '@draft-js-plugins/emoji';
// // import '@draft-js-plugins/emoji/lib/plugin.css'
// // const emojiPlugin = createEmojiPlugin({useNativeArt: true,});
// // const { EmojiSuggestions, EmojiSelect } = emojiPlugin;
// // const plugins = [emojiPlugin]

// const newBlockToHTML = (block) => {
//   const blockType = block.type;
//   if (block.type === Block.ATOMIC) {
//     if (block.text === 'E') {
//       return {
//         start: '<figure class="md-block-atomic md-block-atomic-embed">',
//         end: '</figure>',
//       };
//     } else if (block.text === '-') {
//       return <div className="md-block-atomic md-block-atomic-break"><hr /></div>;
//     }
//   }
//   return blockToHTML(block);
// };

// const newEntityToHTML = (entity, originalText) => {
//   if (entity.type === 'embed') {
//     return (
//       <div>
//         <a
//           className="embedly-card"
//           href={entity.data.url}
//           data-card-controls="0"
//           data-card-theme="dark"
//         >Embedded ― {entity.data.url}
//         </a>
//       </div>
//     );
//   }
//   return entityToHTML(entity, originalText);
// };


// const newTypeMap = StringToTypeMap;
// newTypeMap['2.'] = Block.OL;

// const { hasCommandModifier } = KeyBindingUtil;

// /*
// A demo for example editor. (Feature not built into medium-draft as too specific.)
// Convert quotes to curly quotes.
// */
// const DQUOTE_START = '“';
// const DQUOTE_END = '”';
// const SQUOTE_START = '‘';
// const SQUOTE_END = '’';

// const handleBeforeInput = (editorState, str, onChange) => {
//   if (str === '"' || str === '\'') {
//     const currentBlock = getCurrentBlock(editorState);
//     const selectionState = editorState.getSelection();
//     const contentState = editorState.getCurrentContent();
//     const text = currentBlock.getText();
//     const len = text.length;
//     if (selectionState.getAnchorOffset() === 0) {
//       onChange(EditorState.push(editorState, Modifier.insertText(contentState, selectionState, (str === '"' ? DQUOTE_START : SQUOTE_START)), 'transpose-characters'));
//       return HANDLED;
//     } else if (len > 0) {
//       const lastChar = text[len - 1];
//       if (lastChar !== ' ') {
//         onChange(EditorState.push(editorState, Modifier.insertText(contentState, selectionState, (str === '"' ? DQUOTE_END : SQUOTE_END)), 'transpose-characters'));
//       } else {
//         onChange(EditorState.push(editorState, Modifier.insertText(contentState, selectionState, (str === '"' ? DQUOTE_START : SQUOTE_START)), 'transpose-characters'));
//       }
//       return HANDLED;
//     }
//   }
//   return beforeInput(editorState, str, onChange, newTypeMap);
// };


// class SeparatorSideButton extends React.Component {
//   constructor(props) {
//     super(props);
//     this.onClick = this.onClick.bind(this);
//   }

//   onClick() {
//     const entityKey = Entity.create('separator', 'IMMUTABLE', {});
//     this.props.setEditorState(
//       AtomicBlockUtils.insertAtomicBlock(
//         this.props.getEditorState(),
//         entityKey,
//         '-'
//       )
//     );
//     this.props.close();
//   }

//   render() {
//     return (
//       <button
//         className="md-sb-button md-sb-img-button"
//         type="button"
//         title="Add a separator"
//         onClick={this.onClick}
//       >
//         <i className="icon minus" style={{ width: "0.1em", marginLeft: "-11px" }} />
//       </button>
//     );
//   }
// }


// class EmbedSideButton extends React.Component {

//   static propTypes = {
//     setEditorState: PropTypes.func,
//     getEditorState: PropTypes.func,
//     close: PropTypes.func,
//   };

//   constructor(props) {
//     super(props);
//     this.onClick = this.onClick.bind(this);
//     this.addEmbedURL = this.addEmbedURL.bind(this);
//   }

//   onClick() {
//     const url = window.prompt('Enter a URL');
//     this.props.close();
//     if (!url) {
//       return;
//     }
//     this.addEmbedURL(url);
//   }

//   addEmbedURL(url) {
//     const entityKey = Entity.create('embed', 'IMMUTABLE', { url });
//     this.props.setEditorState(
//       AtomicBlockUtils.insertAtomicBlock(
//         this.props.getEditorState(),
//         entityKey,
//         'E'
//       )
//     );
//   }

//   render() {
//     return (
//       <button
//         className="md-sb-button md-sb-img-button"
//         type="button"
//         title="Add an Embed"
//         onClick={this.onClick}
//       >
//         <i className="icon code" style={{ width: "0.1em", marginLeft: "-14px" }} />
//       </button>
//     );
//   }

// }


// class AtomicEmbedComponent extends React.Component {

//   static propTypes = {
//     data: PropTypes.object.isRequired,
//   }

//   constructor(props) {
//     super(props);

//     this.state = {
//       showIframe: false,
//     };

//     this.enablePreview = this.enablePreview.bind(this);
//   }

//   componentDidMount() {
//     this.renderEmbedly();
//   }

//   componentDidUpdate(prevProps, prevState) {
//     if (prevState.showIframe !== this.state.showIframe && this.state.showIframe === true) {
//       this.renderEmbedly();
//     }
//   }

//   getScript() {
//     const script = document.createElement('script');
//     script.async = 1;
//     script.src = '//cdn.embedly.com/widgets/platform.js';
//     script.onload = () => {
//       window.embedly();
//     };
//     document.body.appendChild(script);
//   }

//   renderEmbedly() {
//     if (window.embedly) {
//       window.embedly();
//     } else {
//       this.getScript();
//     }
//   }

//   enablePreview() {
//     this.setState({
//       showIframe: true,
//     });
//   }

//   render() {
//     const { url } = this.props.data;
//     const innerHTML = `<div><a class="embedly-card" href="${url}" data-card-controls="0" data-card-theme="dark">Embedded ― ${url}</a></div>`;
//     return (
//       <div className="md-block-atomic-embed">
//         <div dangerouslySetInnerHTML={{ __html: innerHTML }} />
//       </div>
//     );
//   }

//   /*render() {
//     const { url } = this.props.data;
//     const innerHTML = `<div><a class="embedly-card" href="${url}" data-card-controls="0" data-card-theme="dark">Embedded ― ${url}</a></div>`;
//     return (
//       <div className="md-block-atomic-embed">
//         {this.state.showIframe ? <div dangerouslySetInnerHTML={{ __html: innerHTML }} /> : (
//           <div>
//             <p>Embedded URL - <a href={url} target="_blank">{url}</a></p>
//             <button type="button" onClick={this.enablePreview}>Show Preview</button>
//           </div>
//         )}
//       </div>
//     );
//   }*/
// }

// const AtomicSeparatorComponent = (props) => (
//   <hr />
// );

// const AtomicBlock = (props) => {
//   const { blockProps, block, contentState } = props;
//   const entity = contentState.getEntity(block.getEntityAt(0));
//   const data = entity.getData();
//   const type = entity.getType();
//   if (blockProps.components[type]) {
//     const AtComponent = blockProps.components[type];
//     return (
//       <div className={`md-block-atomic-wrapper md-block-atomic-wrapper-${type}`}>
//         <AtComponent data={data} />
//       </div>
//     );
//   }
//   return <p>Block of type <b>{type}</b> is not supported.</p>;
// };

// class CustomImageSideButton extends ImageSideButton {
//   /*
//   We will only check for first file and also whether
//   it is an image or not.
//   */
//   onChange(e) {
//     const file = e.target.files[0];

//     if (file.type.indexOf('image/') === 0) {
//       // This is a post request to server endpoint with image as `image`
//       const formData = new FormData();
//       formData.append('image', file);
//       fetch('/your-server-endpoint', {
//         method: 'POST',
//         body: formData,
//       }).then((response) => {
//         if (response.status === 200) {
//           // Assuming server responds with
//           // `{ "url": "http://example-cdn.com/image.jpg"}`
//           return response.json().then(data => {
//             if (data.url) {
//               this.props.setEditorState(addNewBlock(
//                 this.props.getEditorState(),
//                 Block.IMAGE, {
//                 src: data.url,
//               }
//               ));
//             }
//           });
//         }
//       });
//     }
//     this.props.close();
//   }

// }


// class MyEditor extends React.Component {
//   constructor(props) {
//     super(props);

//     this.state = {
//       editorState: createEditorState(),
//       editorEnabled: true,
//       placeholder: 'Write here...',
//     };

//     this.onChange = (editorState, callback = null) => {
//       const currentContent = this.state.editorState.getCurrentContent();
//       const eHTML = this.exporter(currentContent);
//       this.props.onChange(eHTML)
//       if (this.state.editorEnabled) {
//         this.setState({ editorState }, () => {
//           if (callback) {
//             callback();
//           }
//         });
//       }
//     };

//     this.sideButtons = [
//       {
//         title: 'Image',
//         component: ImageSideButton,//CustomImageSideButton,
//       },
//       {
//         title: 'Embed',
//         component: EmbedSideButton,
//       },
//       {
//         title: 'Separator',
//         component: SeparatorSideButton,
//       }
//     ];

//     this.exporter = setRenderOptions({
//       styleToHTML,
//       blockToHTML: newBlockToHTML,
//       entityToHTML: newEntityToHTML,
//     });

//     this.getEditorState = () => this.state.editorState;

//     this.logData = this.logData.bind(this);
//     this.renderHTML = this.renderHTML.bind(this);
//     this.toggleEdit = this.toggleEdit.bind(this);
//     this.fetchData = this.fetchData.bind(this);
//     this.loadSavedData = this.loadSavedData.bind(this);
//     this.keyBinding = this.keyBinding.bind(this);
//     this.handleKeyCommand = this.handleKeyCommand.bind(this);
//     this.handleDroppedFiles = this.handleDroppedFiles.bind(this);
//     this.handleReturn = this.handleReturn.bind(this);
//   }

//   componentDidMount() {
//     this.setState({
//       placeholder: 'Write your text here . . .',
//       editorState: createEditorState(this.props.value)
//     }, () => {
//       this._editor.focus();
//     });
//     //setTimeout(this.fetchData, 1000);
//     // this.refs.editor.focus();
//   }

//   rendererFn(setEditorState, getEditorState) {
//     const atomicRenderers = {
//       embed: AtomicEmbedComponent,
//       separator: AtomicSeparatorComponent,
//     };
//     const rFnOld = rendererFn(setEditorState, getEditorState);
//     const rFnNew = (contentBlock) => {
//       const type = contentBlock.getType();
//       switch (type) {
//         case Block.ATOMIC:
//           return {
//             component: AtomicBlock,
//             editable: false,
//             props: {
//               components: atomicRenderers,
//             },
//           };
//         default: return rFnOld(contentBlock);
//       }
//     };
//     return rFnNew;
//   }

//   keyBinding(e) {
//     if (hasCommandModifier(e)) {
//       if (e.which === 83) {  /* Key S */
//         return 'editor-save';
//       }
//       // else if (e.which === 74 /* Key J */) {
//       //  return 'do-nothing';
//       //}
//     }
//     if (e.altKey === true) {
//       if (e.shiftKey === true) {
//         switch (e.which) {
//           /* Alt + Shift + L */
//           case 76: return 'load-saved-data';
//           /* Key E */
//           // case 69: return 'toggle-edit-mode';
//         }
//       }
//       if (e.which === 72 /* Key H */) {
//         return 'toggleinline:HIGHLIGHT';
//       }
//     }
//     return keyBindingFn(e);
//   }

//   handleKeyCommand(command) {
//     if (command === 'editor-save') {
//       window.localStorage['editor'] = JSON.stringify(convertToRaw(this.state.editorState.getCurrentContent()));
//       //window.ga('send', 'event', 'draftjs', command);
//       return true;
//     } else if (command === 'load-saved-data') {
//       this.loadSavedData();
//       return true;
//     } else if (command === 'toggle-edit-mode') {
//       this.toggleEdit();
//     }
//     return false;
//   }

//   fetchData() {
//     const req = new XMLHttpRequest();
//     req.open('GET', 'data.json', true);
//     req.onreadystatechange = () => {
//       if (req.readyState === 4) {
//         const data = JSON.parse(req.responseText);
//         this.setState({
//           editorState: createEditorState(data),
//           placeholder: 'Write here...'
//         }, () => {
//           this._editor.focus();
//         });
//       }
//     };
//     req.send();
//   }

//   logData(e) {
//     const es = convertToRaw(this.state.editorState.getCurrentContent());
//     console.log(es);
//     console.log(this.state.editorState.getSelection().toJS());
//   }

//   renderHTML(e) {
//     const currentContent = this.state.editorState.getCurrentContent();
//     const eHTML = this.exporter(currentContent);
//     var newWin = window.open(
//       `${window.location.pathname}rendered.html`,
//       'windowName', `height=${window.screen.height},width=${window.screen.wdith}`);
//     newWin.onload = () => {
//       newWin.postMessage(eHTML, window.location.origin);
//     };
//   }

//   loadSavedData() {
//     const data = window.localStorage.getItem('editor');
//     if (data === null) {
//       return;
//     }
//     try {
//       const blockData = JSON.parse(data);
//       this.onChange(EditorState.push(this.state.editorState, convertFromRaw(blockData)), this._editor.focus);
//     } catch (e) {
//       console.log(e);
//     }
//   }

//   toggleEdit(e) {
//     this.setState({
//       editorEnabled: !this.state.editorEnabled
//     }, () => {
//     });
//   }

//   handleDroppedFiles(selection, files) {
//     const file = files[0];
//     if (file.type.indexOf('image/') === 0) {
//       // eslint-disable-next-line no-undef
//       const src = URL.createObjectURL(file);
//       this.onChange(addNewBlockAt(
//         this.state.editorState,
//         selection.getAnchorKey(),
//         Block.IMAGE, {
//         src,
//       }
//       ));
//       return HANDLED;
//     }
//     return NOT_HANDLED
//   }

//   handleReturn(e) {
//     // const currentBlock = getCurrentBlock(this.state.editorState);
//     // var text = currentBlock.getText();
//     return NOT_HANDLED;
//   }

//   render() {
//     const { editorState, editorEnabled } = this.state;
//     const { value } = this.props;
//     return (
//       <div style={{
//         border: "1px solid rgba(34, 36, 38, 0.15)",
//         padding: "20px 20px 20px 50px",
//       }} >
//         {/* <div className="editor-action">
//             <button onClick={this.logData}>Log State</button>
//             <button onClick={this.renderHTML}>Render HTML</button>
//             <button onClick={this.toggleEdit}>Toggle Edit</button>
//           </div> */}
//         <Editor
//           ref={(e) => { this._editor = e; }}
//           editorState={editorState}
//           onChange={this.onChange}
//           editorEnabled={editorEnabled}
//           handleDroppedFiles={this.handleDroppedFiles}
//           handleKeyCommand={this.handleKeyCommand}
//           placeholder={this.state.placeholder}
//           keyBindingFn={this.keyBinding}
//           beforeInput={handleBeforeInput}
//           handleReturn={this.handleReturn}
//           sideButtons={this.sideButtons}
//           rendererFn={this.rendererFn}
//         />
//       </div>
//     );
//   }
// };

// export default MyEditor