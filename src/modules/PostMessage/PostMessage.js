import React, { memo } from "react";
import { Icon, Label, Table } from "semantic-ui-react";
import { ZenLookup } from "../ComponentInfo/Dictionary/ZenLookup";
import { ApiPostMessage } from "../../Api";
import * as routes from "../../constants/routes";
import * as permissions from "../../constants/permissions";
import { IntlFormat } from "../../utils/intlFormat";
import { Mess_Post } from "./zLanguage/variable";
import "./PostMessage.css"
import { ZenLink } from "../../components/Control";
import { zzControlHelper } from "../../components/Control/zzControlHelper";
import { auth } from "../../utils";
import { isEmpty } from "lodash-es";

const PostInfo = {
   route: routes.PostMessage,

   action: {
      view: { visible: true, permission: permissions.PostMessageXem },
      add: { visible: true, permission: permissions.PostMessageThem, link: { route: routes.PostMessageNew } },
      edit: { visible: false, permission: permissions.PostMessageSua, link: { route: routes.PostMessageDetail(), params: "id" } },
      del: { visible: false, permission: permissions.PostMessageXoa }
   },

   linkHeader: {
      ...IntlFormat.default(Mess_Post.List),
      active: true,
      isSetting: false,
   },

   tableList: {
      keyForm: "postMessage",
      formModal: ZenLookup.PostMessage,
      fieldCode: "id",
      changeCode: false,
      api: {
         url: ApiPostMessage,
         type: "sql",
      },
      columns: [
         // { text: "ID", fieldName: "id", type: "string", filter: "", sorter: true, },
         { text: "", fieldName: "icon", type: "string", sorter: false, custom: true },
         {
            id: "Posts.NoiDung", defaultMessage: "Nội dung", collapsing: true,
            fieldName: "title", type: "string", filter: "string", sorter: true, custom: true
         },
         { id: "Posts.ten_loai_post", defaultMessage: "Loại", fieldName: "ten_loai_post", type: "string", filter: "string", sorter: true, },
         { id: "Posts.pubished_date", defaultMessage: "Ngày ban hành", fieldName: "published_date", type: "date", filter: "date", sorter: true, },
         { id: "Posts.trang_thai", defaultMessage: "Trạng thái", fieldName: "status", type: "string", filter: "string", sorter: true, custom: true },
         { id: "Posts.cuser", defaultMessage: "Người tạo", fieldName: "cuser_name", type: "string", filter: "string", sorter: true, },
         { id: "Posts.user_duyet", defaultMessage: "Người duyệt", fieldName: "duyet_user", type: "string", filter: "string", sorter: true, },
         { ...IntlFormat.default(Mess_Post.Participant), fieldName: "str_participants", type: "string", filter: "", sorter: false, custom: true },
      ],

      customCols: (data, item, fieldName, infoCol) => {
         const checkSeen = () => {
            let result = false
            if (!isEmpty(item['participants'])) {
               if (item['participants'].filter(x => x.full_name == auth.getUserInfo().fullName).length > 0) {
                  let participant = item['participants'].filter(x => x.full_name == auth.getUserInfo().fullName)[0]
                  result = participant.is_seen
               }
            }
            return result
         }
         let customCell = undefined
         if (fieldName === 'icon') {
            customCell = <Table.Cell key={fieldName}>
               {!checkSeen() && <Icon name="file alternate outline" />}
               {item['has_poll'] && <Icon name="list ol" />}
            </Table.Cell>
         }
         if (fieldName === 'title') {
            customCell = <Table.Cell key={fieldName}>
               <ZenLink style={{ fontWeight: 'bold' }} to={`${routes.PostMessageDetail(zzControlHelper.btoaUTF8(item['id']))}`}>{item[fieldName]}</ZenLink>
               {/* <div style={{
                  whiteSpace: "normal",
                  inlineSize: "300px",
                  overflowWrap: "break-word",
               }}
                  dangerouslySetInnerHTML={{ __html: item['content'].slice(0, 200) }} /> */}
            </Table.Cell>
         }
         if (fieldName === 'str_participants') {
            customCell = <Table.Cell key={fieldName}>
               <ListParticipant users={item['participants']} />
            </Table.Cell>
         }
         if (fieldName === 'status') {
            customCell = <Table.Cell key={fieldName}>
               {item.status === 0 ? <Label >Đang dự thảo</Label> : ""}
               {item.status === 1 ? <Label color="yellow">Chờ duyệt</Label> : ""}
               {item.status === 2 ? <Label color="green">Đã ban hành</Label> : ""}
            </Table.Cell>
         }
         if (fieldName === 'user_duyet') {
            customCell = <Table.Cell key={fieldName}>
               <Listuserduyet users={item[fieldName]} />
            </Table.Cell>
         }
         return customCell
      }
   },
}

const ListParticipant = memo(({ users }) => (
   users && users.map(fname => {
      return <Label key={fname.full_name} as="a" content={fname.full_name} />
   })
))

const Listuserduyet = memo(({ users }) => (
   users && users.split(',').map(fname => {
      return <Label key={fname} as="a" content={fname} />
   })
))

export { PostInfo }

