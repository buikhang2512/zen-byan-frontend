export const optionsPostStatus = [
   { key: 0, value: 0, text: "Unpublish", des: "Còn soạn thảo, chưa publish" },
   { key: 1, value: 1, text: "Published", des: "Đã publish thông báo" },
   { key: 2, value: 2, text: "Closed", des: "Đã đóng thông báo do hết thời gian hiệu lực" },
   { key: 9, value: 9, text: "Cancel", des: "Hủy thông báo" },
]