import { SOVchSO3List, SOVchSO3Edit } from './SOVchSO3/index'
import { SOVchSO4List, SOVchSO4Edit } from './SOVchSO4/index'
import { SOVchSO1List, SOVchSO1Edit } from './SOVchSO1/index'

export const SO_Voucher = {
    //SO1: SOVchSO1List,
    SO3: SOVchSO3List,
    //SO4: SOVchSO4List,
}

export const SO_VoucherEdit = {
    //SO1: SOVchSO1Edit,
    SO3: SOVchSO3Edit,
    //SO4: SOVchSO4Edit,
}