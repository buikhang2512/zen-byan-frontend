import React, {useState} from 'react';
import { Button } from 'semantic-ui-react';
import { ZenFieldSelectApi } from '../../../../components/Control';
import { ZenLookup } from '../../../ComponentInfo/Dictionary/ZenLookup';
import { ZenVoucherLookup } from "../../../../components/Control/zVoucher";

const spLookup = "zusSO3LookupSO1";

const SOVchSO1Lookup = ({reaOnly, onSelected}) => {
    const [open, setOpen] = useState(false)
    const handleOpen = () => setOpen(!open)

    const handleCondition = (item) => {
        let _sql = "";

        if(item.so_ct1) _sql += ` AND so_ct >= '${item.so_ct1}'`
        if(item.so_ct2) _sql += ` AND so_ct <= '${item.so_ct2}'`
        if(item.ngay_ct1) _sql += ` AND ngay_ct >= '${item.ngay_ct1}'`
        if(item.ngay_ct2) _sql += ` AND ngay_ct <= '${item.ngay_ct2}'`
        if(item.ma_kh) _sql += `AND ma_kh = '${item.ma_kh}'`

        return _sql
    }

    const handleSelected = (item) => {
        onSelected(item)
        setOpen(false)
    }

    return <>
        <Button content={"Chọn hóa đơn bán"}
            size="tiny" icon="plus"
            primary basic
            disabled={reaOnly}
            onClick={handleOpen}
        />

        <ZenVoucherLookup open={open} title={"Chọn đơn hàng bán"}
            stored={spLookup}
            onCondition={handleCondition}
            onClose={handleOpen}
            onSelected={handleSelected}
            FilterFormPH={FilterFormPH}

            initFilter={[{ name: "ma_kh", text: " Khách hàng"},]}
            // Khai báo các trường thông tin chung
            columnsPH={[
                { name: "ngay_ct", text: "Ngày CTừ", type: "date", },
                { name: "so_ct", text: "Số CTừ", type: "string", },
                { name: "ma_kh", text: "Mã KH", type: "string", },
                { name: "nguoi_gd", text: "Người giao dịch", type: "string", },
                { name: "ma_hd" , text: "Mã hợp đồng", type: "string", },
                { name: "ma_nt" , text: "Mã ngoại tệ", type: "string", },
                { name: "ty_gia", text: "Tỷ giá", type: "number", },
            ]}
            // Khai báo các trường Chi tiết
            columnsCT={[
                { name: "ma_vt", text: "Mã vt", type: "string", },
                { name: "ten_vt", text: "Tên vt", type: "string", },
                { name: "dvt", text: "DVT", type: "string", },
                { name: "ma_kho", text: "Mã kho", type:"string", },

                { name: "so_luong", text: "Số lượng", type: "string", },
                { name: "sl_xuat",text:"Đã nhập", type: "string", },
                { name: "sl_ton", text: "Còn tồn", type: "string", },

                { name: "ma_hd", text: "Hợp đồng", type: "string", },
                { name: "ma_spct", text: "SPCT", type: "string", },
                { name: "ma_phi", text: "Phí", type: "string"},
                { name: "ma_bp", text: "Bộ Phận", type: "string", },
            ]}
            />
    </>
}

const FilterFormPH = ({ formik}) => {
    return <>
        <ZenFieldSelectApi lookup={ZenLookup.Ma_kh} loadApi
            label={"po4.ma_kh"} defaultlabel="Khách hàng"
            name="ma_kh" formik={formik}
        />
    </>
}

export { SOVchSO1Lookup }