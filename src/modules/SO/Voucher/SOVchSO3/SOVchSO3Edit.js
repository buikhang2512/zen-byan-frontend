﻿import React, { useContext, useEffect, useState } from "react";
import { Grid, Table, Checkbox, Form } from "semantic-ui-react";
import { ZenField, ZenFieldDate, ZenFieldSelectApi } from "../../../../components/Control/index";
import { ZenHelper } from "../../../../utils";
import { ApiSoDmGiaBan, ApiSOVchSO3 } from "../../Api/index";
import {
    ContextVoucher, VoucherHelper,
    RowItemPH, NumberCell, TextCell, DeleteCell, RowHeaderCell,
    ActionType, SelectCell, TableScroll, TableTotalPH, RowTotalPH
} from "../../../../components/Control/zVoucher";
import { ZenLookup } from "../../../ComponentInfo/Dictionary/ZenLookup";
import * as routes from "../../../../constants/routes";
import { ConfigBySiDmCt, DropDownCell } from "../../../../components/Control/zVoucher/ZenVoucherHelper";
import { useIntl } from "react-intl";
import { GlobalStorage, KeyStorage } from "../../../../utils/storage";
import * as permissions from "../../../../constants/permissions";


const PHForm = ({ formik, permission, modeForm, FieldNT }) => {
    const { ct, onUpdatePHCT } = useContext(ContextVoucher)
    let isReadOnly = VoucherHelper.f_ReadOnly(modeForm, permission)

    function getTyGia(ma_nt, ngay_ct) {
        let date_ct = ngay_ct ? ngay_ct : null
        let newArr = [];
        const getLocalTyGia = GlobalStorage.getByField(KeyStorage.CacheData, 'sidmtgnt').data
        newArr = getLocalTyGia.filter(i => i.ma_nt === ma_nt)
        if (newArr.length) {
            if (date_ct) {
                var findDateClosest = newArr.filter(i => ZenHelper.formatDateTime(i.ngay_tg, 'YYYY-MM-DD') <= date_ct)
                if (!findDateClosest || findDateClosest.length === 0) return 1;
                if (findDateClosest.length < 2) return findDateClosest[0].ty_gia;
                if (findDateClosest.length > 1) return findDateClosest[findDateClosest.length - 1].ty_gia
            } else { return null }
        }
        return null
    }

    function PHCT(ma_nt, ngay_ct) {
        const tygia = ma_nt === "VND" ? 1 : getTyGia(ma_nt, ngay_ct);
        const totalPH = {
            ma_nt: ma_nt,
            ty_gia: tygia,
            ngay_ct: ngay_ct,
            t_tien2: 0,
            t_tien_nt2: 0,
            t_ck: 0,
            t_ck_nt: 0,
            t_thue: 0,
            t_thue_nt: 0,
            t_tt: 0,
            t_tt_nt: 0
        }
        const newCT = ct.map(item => {
            // tính CT
            if (ma_nt === 'VND') {
                item.tien_nt2 = item.tien2
                item.gia_nt2 = item.gia2
                item.tien_ck_nt = item.tien_ck
                item.thue_gtgt_nt = item.thue_gtgt
                item.tt_nt = item.tt
            } else {
                item.tien2 = Math.round(item.tien_nt2 * tygia)
                item.gia2 = Math.round(item.gia_nt2 * tygia)
                item.tien_ck = Math.round(item.tien2 * (item.tl_ck / 100))
                item.thue_gtgt = Math.round((item.tien2 - item.tien_ck) * (item.ts_gtgt / 100))
                item.tt = item.tien2 - item.tien_ck + item.thue_gtgt
            }
            // tính PH
            totalPH.t_tien2 += VoucherHelper.f_NullToZero(item.tien2)
            totalPH.t_tien_nt2 += VoucherHelper.f_NullToZero(item.tien_nt2)

            totalPH.t_ck += VoucherHelper.f_NullToZero(item.tien_ck)
            totalPH.t_ck_nt += VoucherHelper.f_NullToZero(item.tien_ck_nt)

            totalPH.t_thue += VoucherHelper.f_NullToZero(item.thue_gtgt)
            totalPH.t_thue_nt += VoucherHelper.f_NullToZero(item.thue_gtgt_nt)

            totalPH.t_tt += VoucherHelper.f_NullToZero(item.tt)
            totalPH.t_tt_nt += VoucherHelper.f_NullToZero(item.tt_nt)
            return item
        });
        onUpdatePHCT(totalPH, newCT)
    }
    const handleChangeDatect = (e) => {
        const { ma_nt } = formik.values
        PHCT(ma_nt, e.value)
    }

    const handleChangeNT = ({ ma_nt, ty_gia }) => {
        const totalPH = {
            ma_nt: ma_nt,
            ty_gia: ty_gia,
            t_tien2: 0,
            t_tien_nt2: 0,
            t_ck: 0,
            t_ck_nt: 0,
            t_thue: 0,
            t_thue_nt: 0,
            t_tt: 0,
            t_tt_nt: 0
        }
        const newCT = ct.map(item => {
            // tính CT
            if (ma_nt === 'VND') {
                item.tien_nt2 = item.tien2
                item.gia_nt2 = item.gia2
                item.tien_ck_nt = item.tien_ck
                item.thue_gtgt_nt = item.thue_gtgt
                item.tt_nt = item.tt
            } else {
                item.tien2 = Math.round(item.tien_nt2 * ty_gia)
                item.gia2 = Math.round(item.gia_nt2 * ty_gia)
                item.tien_ck = Math.round(item.tien2 * (item.tl_ck / 100))
                item.thue_gtgt = Math.round((item.tien2 - item.tien_ck) * (item.ts_gtgt / 100))
                item.tt = item.tien2 - item.tien_ck + item.thue_gtgt
            }
            // tính PH
            totalPH.t_tien2 += VoucherHelper.f_NullToZero(item.tien2)
            totalPH.t_tien_nt2 += VoucherHelper.f_NullToZero(item.tien_nt2)

            totalPH.t_ck += VoucherHelper.f_NullToZero(item.tien_ck)
            totalPH.t_ck_nt += VoucherHelper.f_NullToZero(item.tien_ck_nt)

            totalPH.t_thue += VoucherHelper.f_NullToZero(item.thue_gtgt)
            totalPH.t_thue_nt += VoucherHelper.f_NullToZero(item.thue_gtgt_nt)

            totalPH.t_tt += VoucherHelper.f_NullToZero(item.tt)
            totalPH.t_tt_nt += VoucherHelper.f_NullToZero(item.tt_nt)
            return item
        });
        onUpdatePHCT(totalPH, newCT)
    }

    const handleSelectedLookup = (itemSelected, { name }) => {
        if (name === 'ma_kh') {
            formik.setValues({
                ...formik.values,
                [name]: itemSelected.ma_kh,
                dia_chi_vat: itemSelected.dia_chi,
                ma_so_thue: itemSelected.ma_so_thue,
                nguoi_gd: itemSelected.nguoi_gd,
                ma_httt: itemSelected.ma_httt || ""
            })
        } else if (name === "ma_httt") {
            // formik.setValues({
            //     ...formik.values,
            //     [name]: itemSelected.ma_httt,
            //     tk_pt: itemSelected.tk,
            //     tk_thue: itemSelected.tk_thue_gtgt_ban,
            //     tk_ck_ds: itemSelected.tk_ck
            // })
            // Formik chỉ render lại children của formik,
            // sử dụng onUpdatePHCT để render lại khi formik.values thay đổi
            onUpdatePHCT({
                ...formik.values,
                [name]: itemSelected.ma_httt,
                tk_pt: itemSelected.tk || "",
                tk_thue: itemSelected.tk_thue_gtgt_ban || "",
                tk_ck_ds: itemSelected.tk_ck || ""
            })
        } else if (name === 'ma_gd') {
            onUpdatePHCT({
                ...formik.values,
                [name]: itemSelected.ma_gd,
            })
        }
    }

    return <React.Fragment>
        <Grid columns="3">
            <RowItemPH content={[
                {
                    width: 6,
                    input: <ZenFieldSelectApi required
                        tabIndex={1}
                        lookup={{
                            ...ZenLookup.Ma_gd,
                            where: `ma_ct = 'SO3'`,
                            onLocalWhere: (items) => {
                                return items?.filter(t => t.ma_ct == 'SO3')
                            },
                        }}
                        label={"so3.magd"} defaultlabel="Giao dịch"
                        name="ma_gd" formik={formik}
                        onItemSelected={handleSelectedLookup}
                        readOnly={isReadOnly}
                    />
                },
            ]} />
            <RowItemPH content={[
                {
                    width: 6,
                    input: <ZenFieldSelectApi required loadApi
                        tabIndex={1}
                        lookup={ZenLookup.Ma_kh}
                        label={"ardmkh.header"} defaultlabel="Khách hàng"
                        name="ma_kh" formik={formik}
                        onItemSelected={handleSelectedLookup}
                        readOnly={isReadOnly}
                    />
                },
                {
                    width: 7,
                    input: <ZenField readOnly={isReadOnly} tabIndex={1}
                        label={"so3.dia_chi"} defaultlabel="Địa chỉ"
                        name="dia_chi_vat" props={formik}
                    />
                },
                {
                    width: 3,
                    input: <ZenFieldDate required readOnly={isReadOnly} tabIndex={3}
                        label={"so3.ngay_ct"} defaultlabel="Ngày chứng từ"
                        name="ngay_ct" props={formik}
                        onChange={handleChangeDatect}
                    />
                },
            ]} />

            <RowItemPH content={[
                {
                    width: 6,
                    input: <ZenField readOnly={isReadOnly} tabIndex={1}
                        label={"so3.nguoi_gd"} defaultlabel="Người giao dịch"
                        name="nguoi_gd" props={formik}
                    />
                },
                {
                    width: 7,
                    input: <ZenField readOnly={isReadOnly} tabIndex={1}
                        label={"so3.ma_so_thue"} defaultlabel="Mã số thuế"
                        name="ma_so_thue" props={formik}
                    />
                },
                {
                    width: 3,
                    input: <ZenField required readOnly={isReadOnly} tabIndex={3}
                        label={"so3.so_hd"} defaultlabel="Số hóa đơn"
                        name="so_ct" props={formik}
                    />
                },
            ]} />

            <RowItemPH content={[
                {
                    width: 6,
                    input: <ZenField readOnly={isReadOnly} tabIndex={1}
                        label={"so3.dien_giai"} defaultlabel="Diễn giải"
                        name="dien_giai" props={formik}
                    />
                },
                { width: 7, input: undefined },
                {
                    width: 3,
                    input: <ZenFieldSelectApi readOnly={isReadOnly} tabIndex={3}
                        loadApi
                        lookup={ZenLookup.SoDmMHD}
                        label={"so3.so_seri"} defaultlabel="Số seri"
                        name="so_seri" formik={formik}
                    />
                },
            ]} />

            <RowItemPH content={[
                {
                    width: 6,
                    input: <ZenFieldSelectApi loadApi tabIndex={1}
                        lookup={{
                            ...ZenLookup.Ma_hd,
                            where: `loai = 1`
                        }}
                        label={"so3.ma_hd"} defaultlabel="Mã hợp đồng"
                        name="ma_hd" formik={formik}
                        readOnly={isReadOnly}
                        onItemSelected={handleSelectedLookup}
                    />
                },
                { width: 7, input: undefined },
                { width: 3, input: undefined }, // cột của vùng thông tin chứng từ
            ]} />

            <RowItemPH content={[
                {
                    width: 6,
                    input: <ZenFieldSelectApi required tabIndex={1}
                        lookup={{
                            ...ZenLookup.Ma_httt,
                            onLocalWhere: (items) => {
                                return items?.filter(t => t.moduleid == "SO")
                            }
                        }}
                        label={"so3.ma_httt"} defaultlabel="Phương thức thanh toán"
                        name="ma_httt" formik={formik}
                        readOnly={isReadOnly}
                        onItemSelected={handleSelectedLookup}
                    />
                },
                {
                    width: 7,
                    input: <ZenField readOnly={isReadOnly} tabIndex={1}
                        label={"so3.du_ht"} defaultlabel="Dư hiện thời"
                        name="du_ht" props={formik}
                    />
                },
                {
                    width: 3, input: <FieldNT formik={formik} onChangeNT={handleChangeNT} tabIndex={3} />
                },
            ]} />
        </Grid>
    </React.Fragment>
}

const CTForm = ({ ph, ct, permission, modeForm }) => {
    const [reserr, setReserr] = useState()
    const { onChangeCT, errorCT, itemSiDmCt, onUpdatePHCT } = useContext(ContextVoucher)
    let isReadOnly = VoucherHelper.f_ReadOnly(modeForm, permission)
    const intl = useIntl();

    function getgiaban(param) {
        var result = new Promise((resolve, reject) =>
            ApiSoDmGiaBan.getPrice(param, res => {
                if (res.status >= 200 && 204 >= res.status) {
                    resolve(res.data.data)
                } else {
                    reject(res)
                }
            })
        )
        return result
    }

    useEffect(() => {
        async function getPriceWhenChangeNT() {
            const ctlength = ct.length;
            const item = [];
            for (let i = 0; i < ctlength; i++) {
                await getgiaban({
                    ma_vt: ct[i].ma_vt,
                    ma_kh: ph.ma_kh,
                    ma_nt: ph.ma_nt,
                    ngay_ct: ph.ngay_ct
                }).then(newItem => {
                    const itemCurrent = {
                        ...ct[i],
                        gia2: ph.ma_nt == "VND" ? newItem.gia : Math.round(newItem.gia * ph.ty_gia),
                        gia_nt2: ph.ma_nt !== "VND" ? newItem.gia : VoucherHelper.f_RoundNumberByNT(newItem.gia / ph.ty_gia, ph.ma_nt),
                    }
                    changeSoLuong(itemCurrent)
                    const ttph = calcTotalPH(itemCurrent, i)
                    onChangeCT(itemCurrent, i, "text_change", ttph, itemCurrent)
                }).catch(err => {
                    setReserr(ZenHelper.getResponseError(err))
                })
            }
        }
        getPriceWhenChangeNT()
    }, [ph.ma_nt])

    const switchAction = async (propsElement, type, itemCurrent, index) => {
        const { name, value, itemSelected } = propsElement
        const prevItemCurrent = { ...itemCurrent }
        var _gia = 0;
        switch (type) {
            case ActionType.TEXT_CHANGE:
                const { lookup } = propsElement
                itemCurrent[name] = value

                // set tên nếu là lookup
                // => cần để khởi tạo item cho input lookup, nếu hiển thị nhìu hơn 1 trường mã
                if (lookup) {
                    if (name === 'ma_vt') {
                        await getgiaban({
                            ma_vt: propsElement.itemSelected.ma_vt,
                            ma_kh: ph.ma_kh,
                            ma_nt: ph.ma_nt,
                            ngay_ct: ph.ngay_ct
                        }).then(newItem => {
                            _gia = newItem.gia
                        }).catch(err => {
                            setReserr(ZenHelper.getResponseError(err))
                        })
                        itemCurrent = {
                            ...itemCurrent,
                            gia2: ph.ma_nt == "VND" ? _gia : Math.round(_gia * ph.ty_gia),
                            gia_nt2: ph.ma_nt !== "VND" ? _gia : VoucherHelper.f_RoundNumberByNT(_gia / ph.ty_gia, ph.ma_nt),
                            ...validMaVt(itemSelected)
                        }
                        if (!prevItemCurrent['ma_thue']) {
                            itemCurrent['ma_thue'] = itemSelected['ma_thue']
                            itemCurrent['ts_gtgt'] = itemSelected['ts_gtgt']
                            f_calcNumber(propsElement, type, itemCurrent, index)
                            return
                        }
                    }
                    else if (name === 'ma_thue') {
                        itemCurrent['ts_gtgt'] = itemSelected['ts_gtgt']
                        f_calcNumber(propsElement, type, itemCurrent, index)
                        return
                    }
                }
                changeSoLuong(itemCurrent)
                const _totalPH = calcTotalPH(itemCurrent, index)
                onChangeCT(itemCurrent, index, type, _totalPH, propsElement)
                break;

            case ActionType.NUMBER_CHANGE:
                itemCurrent[name] = value
                f_calcNumber(propsElement, type, itemCurrent, index)
                break;

            case ActionType.ADD_ROW:
                await getgiaban({
                    ma_vt: propsElement.itemSelected.ma_vt,
                    ma_kh: ph.ma_kh,
                    ma_nt: ph.ma_nt,
                    ngay_ct: ph.ngay_ct
                }).then(newItem => {
                    _gia = newItem.gia
                }).catch(err => {
                    setReserr(ZenHelper.getResponseError(err))
                })
                let newCT = ({
                    ...validMaVt(itemSelected),
                    ma_vt: value,
                    ma_thue: itemSelected.ma_thue,
                    ts_gtgt: itemSelected.ts_gtgt,

                    so_luong: 0,
                    gia2: ph.ma_nt == "VND" ? _gia : Math.round(_gia * ph.ty_gia),
                    gia_nt2: ph.ma_nt !== "VND" ? _gia : VoucherHelper.f_RoundNumberByNT(_gia / ph.ty_gia, ph.ma_nt),
                    tien2: 0, tien_nt2: 0,
                    tl_ck: 0,
                    tien_ck_nt: 0, tien_ck: 0,
                    thue_gtgt_nt: 0, thue_gtgt: 0,
                    tt_nt: 0, tt: 0,
                })
                if (itemSelected.ma_kho) {
                    newCT.ma_kho = itemSelected.ma_kho
                }
                onChangeCT(newCT, null, type)
                break;

            case ActionType.DELETE_ROW:
                // xóa dòng CT, tính lại PH
                const ctDel = ct.filter((item, idx) => idx !== index)
                const totalPH = {
                    t_so_luong: VoucherHelper.f_SumArray(ctDel, 'so_luong'),
                    t_tien2: VoucherHelper.f_SumArray(ctDel, 'tien2'),
                    t_tien_nt2: VoucherHelper.f_SumArray(ctDel, 'tien_nt2'),
                    t_thue: VoucherHelper.f_SumArray(ctDel, 'thue_gtgt'),
                    t_thue_nt: VoucherHelper.f_SumArray(ctDel, 'thue_gtgt_nt'),
                    t_ck: VoucherHelper.f_SumArray(ctDel, 'tien_ck'),
                    t_ck_nt: VoucherHelper.f_SumArray(ctDel, 'tien_ck_nt'),
                    t_tt: 0,
                    t_tt_nt: 0,
                }
                totalPH.t_tt = totalPH.t_tien2 + totalPH.t_thue - totalPH.t_ck
                totalPH.t_tt_nt = totalPH.t_tien_nt2 + totalPH.t_thue_nt - totalPH.t_ck_nt
                onChangeCT(itemCurrent, index, type, totalPH)
                break;
            default:
                break;
        }
    }

    function f_calcNumber(propsElement, type, itemCurrent, index) {
        const { name } = propsElement

        if (name === 'so_luong') {
            changeSoLuong(itemCurrent)
        } else if (name === 'gia2') {
            changeGia2(itemCurrent)
        } else if (name === 'gia_nt2') {
            changeGia_nt2(itemCurrent)
        } else if (name === 'tien2') {
            changeTien2(itemCurrent)
        } else if (name === 'tien_nt2') {
            changeTien_nt2(itemCurrent)
        } else if (name === 'ma_thue' || name === "ma_vt") {
            changeMa_thue(itemCurrent)
        } else if (name === 'thue_gtgt') {
            changeThue_gtgt(itemCurrent)
        } else if (name === 'thue_gtgt_nt') {
            changeThue_gtgt_nt(itemCurrent)
        } else if (name === 'tl_ck') {
            changeTl_ck(itemCurrent)
        } else if (name === 'tien_ck') {
            changeTien_ck(itemCurrent)
        } else if (name === 'tien_ck_nt') {
            changeTien_ck_nt(itemCurrent)
        } else if (name === 'gia') {
            changeGia(itemCurrent)
        } else if (name === 'gia_nt') {
            changeGia_nt(itemCurrent)
        }


        //cập nhật PH, CT
        VoucherHelper.f_Timeout(() => {
            const totalPH = calcTotalPH(itemCurrent, index)
            onChangeCT(itemCurrent, index, type, totalPH, propsElement)
        })
    }

    function validMaVt(itemSelected) {
        return {
            ten_vt: itemSelected['ten_vt'],
            dvt: itemSelected['dvt'],
            ma_kho: itemSelected['ma_kho'],
            ton_kho: itemSelected['ton_kho'],
            tk_dt: itemSelected['tk_dt'],
            tk_vt: itemSelected['tk_vt'],
            tk_tl: itemSelected['tk_tl'],
            tk_gv: itemSelected['tk_gv'],
            tk_ck: itemSelected['tk_ck'],
            gia_ton: itemSelected['gia_ton'],
        }
    }

    // =============================== VALUE CHANGE
    function changeSoLuong(item) {
        f_calcTien2(item)
        f_calcTien_Ck(item)
        f_calcThue(item)
        f_calcTT(item)
        f_calcTien(item) // tiền vốn
    }
    function changeGia2(item) {
        if (ph.ma_nt === 'VND') {
            item.gia_nt2 = item.gia2
        } else {
            item.gia_nt2 = VoucherHelper.f_RoundNumberByNT(item.gia2 / ph.ty_gia, ph.ma_nt)
        }
        changeSoLuong(item)
    }
    function changeGia_nt2(item) {
        item.gia2 = Math.round(item.gia_nt2 * ph.ty_gia)
        changeSoLuong(item)
    }
    function changeTien2(item) {
        if (ph.ma_nt === 'VND') {
            item.tien_nt2 = item.tien2
        } else {
            item.tien_nt2 = VoucherHelper.f_RoundNumberByNT(item.tien2 / ph.ty_gia, ph.ma_nt)
        }
        f_calcThue(item)
        f_calcTien_Ck(item)
        f_calcTT(item)
    }
    function changeTien_nt2(item) {
        item.tien2 = Math.round(item.tien_nt2 * ph.ty_gia)
        f_calcThue(item)
        f_calcTien_Ck(item)
        f_calcTT(item)
    }

    function changeTl_ck(item) {
        f_calcTien_Ck(item)
        f_calcThue(item)
        f_calcTT(item)
    }
    function changeTien_ck(item) {
        if (ph.ma_nt === 'VND') {
            item.tien_ck_nt = item.tien_ck
        } else {
            item.tien_ck_nt = VoucherHelper.f_RoundNumberByNT(item.tien_ck / ph.ty_gia, ph.ma_nt)
        }
        f_calcThue(item)
        f_calcTT(item)
    }
    function changeTien_ck_nt(item) {
        item.tien_ck = Math.round(item.tien_ck_nt * ph.ty_gia)
        f_calcThue(item)
        f_calcTT(item)
    }

    function changeMa_thue(item) {
        f_calcThue(item)
        f_calcTT(item)
    }
    function changeThue_gtgt(item) {
        if (ph.ma_nt === 'VND') {
            item.thue_gtgt_nt = item.thue_gtgt
        } else {
            item.thue_gtgt_nt = VoucherHelper.f_RoundNumberByNT(item.thue_gtgt / ph.ty_gia, ph.ma_nt)
        }
        f_calcTT(item)
    }
    function changeThue_gtgt_nt(item) {
        item.thue_gtgt = Math.round(item.thue_gtgt_nt * ph.ty_gia)
        f_calcTT(item)
    }

    function changeGia(item) {
        if (ph.ma_nt === 'VND') {
            item.gia_nt = item.gia
        } else {
            item.gia_nt = VoucherHelper.f_RoundNumberByNT(item.gia / ph.ty_gia, ph.ma_nt)
        }
        changeSoLuong(item)
    }
    function changeGia_nt(item) {
        item.gia = Math.round(item.gia_nt * ph.ty_gia)
        changeSoLuong(item)
    }

    // ==================================== tính toán
    function f_calcTien2(item) {
        item.tien_nt2 = VoucherHelper.f_RoundNumberByNT(item.so_luong * item.gia_nt2, ph.ma_nt)
        if (ph.ma_nt === 'VND') {
            item.tien2 = item.tien_nt2
        } else {
            item.tien2 = VoucherHelper.f_RoundNumberByNT(item.tien_nt2 * ph.ty_gia, ph.ma_nt)
        }
    }
    function f_calcTien_Ck(item) {
        item.tien_ck_nt = VoucherHelper.f_RoundNumberByNT(item.tien_nt2 * item.tl_ck / 100, ph.ma_nt)
        if (ph.ma_nt === 'VND') {
            item.tien_ck = item.tien_ck_nt
        } else {
            item.tien_ck = VoucherHelper.f_RoundNumberByNT(item.tien_ck_nt * ph.ty_gia, ph.ma_nt)
        }
    }
    function f_calcThue(item) {
        item.thue_gtgt_nt = VoucherHelper.f_RoundNumberByNT((item.tien_nt2 - item.tien_ck) * item.ts_gtgt / 100, ph.ma_nt)
        if (ph.ma_nt === 'VND') {
            item.thue_gtgt = item.thue_gtgt_nt
        } else {
            item.thue_gtgt = VoucherHelper.f_RoundNumberByNT(item.thue_gtgt_nt * ph.ty_gia, ph.ma_nt)
        }
    }
    function f_calcTT(item) {
        item.tt_nt = VoucherHelper.f_RoundNumberByNT(item.tien_nt2 + item.thue_gtgt_nt - item.tien_ck_nt, ph.ma_nt)
        if (ph.ma_nt === 'VND') {
            item.tt = item.tt_nt
        } else {
            item.tt = VoucherHelper.f_RoundNumberByNT(item.tt_nt * ph.ty_gia, ph.ma_nt)
        }
    }
    function f_calcTien(item) {
        item.tien_nt = VoucherHelper.f_RoundNumberByNT(item.so_luong * item.gia_nt, ph.ma_nt)
        if (ph.ma_nt === 'VND') {
            item.tien = item.tien_nt
        } else {
            item.tien = VoucherHelper.f_RoundNumberByNT(item.tien_nt * ph.ty_gia, ph.ma_nt)
        }
    }
    function calcTotalPH(itemCurrent, index) {
        // tạo CT mới
        const newCT = ct.map((item, idx) => idx === index ? itemCurrent : item)
        // tính tổng PH
        const totalPH = {
            t_so_luong: VoucherHelper.f_SumArray(newCT, 'so_luong'),
            t_tien2: VoucherHelper.f_SumArray(newCT, 'tien2'),
            t_tien_nt2: VoucherHelper.f_SumArray(newCT, 'tien_nt2'),
            t_thue: VoucherHelper.f_SumArray(newCT, 'thue_gtgt'),
            t_thue_nt: VoucherHelper.f_SumArray(newCT, 'thue_gtgt_nt'),
            t_ck: VoucherHelper.f_SumArray(newCT, 'tien_ck'),
            t_ck_nt: VoucherHelper.f_SumArray(newCT, 'tien_ck_nt'),
            t_tt: 0,
            t_tt_nt: 0
        }

        totalPH.t_tt = totalPH.t_tien2 + totalPH.t_thue - totalPH.t_ck
        totalPH.t_tt_nt = totalPH.t_tien_nt2 + totalPH.t_thue_nt - totalPH.t_ck_nt
        return totalPH
    }

    // ================= giá đích danh
    function isGiaDichDanh(rowCT) {
        if (isReadOnly) return true
        // PP tính giá tồn của VT là đích danh (4)
        if (rowCT.gia_ton == "4") return false
        // có check xuất theo giá dd
        if (ph.gia_dd) return false
        return true
    }
    function tableheadercell(varialbe, tablecell) {
        if (varialbe !== '3') {
            return tablecell
        }
        delete tablecell.maNT
        return { ...tablecell, isNT: true }
    }

    return <>
        <div style={{ textAlign: "right", paddingBottom: "14px" }}>
            {ph.ma_gd !== '3' && <Checkbox checked={ph.gia_dd ? true : false} tabIndex={-1}
                name="gia_dd"
                label={intl.formatMessage({ id: "so3.gia_dd", defaultMessage: "Xuất theo giá đích danh" })}
                onChange={(e, { checked }) => onUpdatePHCT({ gia_dd: checked })}
                style={{ fontWeight: "bold" }}
                readOnly={isReadOnly}
            />}
        </div>

        <TableScroll>
            <Table.Header>
                <RowHeaderCell colAction={isReadOnly}
                    itemSiDmCt={itemSiDmCt}
                    header={[
                        { text: ['so3.ma_vt', 'Mã hàng'] },
                        { text: ['so3.ten_vt', 'Tên hàng'] },

                        tableheadercell(ph.ma_gd, { text: ['so3.ma_kho', 'Mã Kho'] }),
                        { text: ['so3.dvt', 'Đơn vị tính'] },
                        { text: ['so3.so_luong', 'Số lượng'] },
                        tableheadercell(ph.ma_gd, { text: ['so3.ton', 'Tồn'] }),
                        { text: ['so3.gia_nt2', 'Giá'], maNT: ph.ma_nt, isNT: true },
                        { text: ['so3.tien_nt2', 'Thành tiền'], maNT: ph.ma_nt, isNT: true },
                        { text: ['so3.gia2', 'Giá'], maNT: 'VND' },
                        { text: ['so3.tien2', 'Thành tiền'], maNT: 'VND' },
                        { text: ['so3.tl_ck', '% GG'] },
                        { text: ['so3.tien_ck_nt', 'Giảm giá'], maNT: ph.ma_nt, isNT: true },
                        { text: ['so3.tien_ck', 'Giảm giá'], maNT: 'VND' },
                        { text: ['so3.ts_gtgt', '% VAT'] },
                        { text: ['so3.thue_gtgt_nt', 'VAT'], maNT: ph.ma_nt, isNT: true },
                        { text: ['so3.thue_gtgt', 'VAT'], maNT: 'VND' },
                        tableheadercell(ph.ma_gd, { text: ['so3.gia_nt', 'Giá vốn'], maNT: ph.ma_nt, isNT: true }),
                        tableheadercell(ph.ma_gd, { text: ['so3.tien_nt', 'Tiền vốn'], maNT: ph.ma_nt, isNT: true }),
                        tableheadercell(ph.ma_gd, { text: ['so3.gia', 'Giá vốn'], maNT: 'VND' }),
                        tableheadercell(ph.ma_gd, { text: ['so3.tien', 'Tiền vốn'], maNT: 'VND' }),
                        { text: ['so3.ma_nvkd', 'NVKD'] },
                    ]}
                />
            </Table.Header>
            <Table.Body>
                {
                    ct && ct.map((item, index) => {
                        const error = errorCT ? errorCT.find(x => x.index === index) : undefined

                        return <Table.Row key={item.stt_rec0 ? item.stt_rec0 : ('REC' + index)}>

                            {ph.ma_gd !== '3' && <SelectCell
                                lookup={ZenLookup.Ma_vt}
                                error={error}
                                name="ma_vt" rowItem={item} index={index}
                                placeholder={['so3.ma_vt', 'Mã hàng']}
                                readOnly={isReadOnly}
                                onChange={switchAction}
                            />}

                            {ph.ma_gd == '3' && <SelectCell
                                lookup={{
                                    ...ZenLookup.Ma_vt,
                                    where: "loai = '4' or loai = '9'",
                                    onLocalWhere: (items) => {
                                        return items.filter(t => t.loai == 4 || t.loai == 9)
                                    }
                                }}
                                error={error}
                                name="ma_vt" rowItem={item} index={index}
                                placeholder={['so3.ma_vt', 'Mã hàng']}
                                readOnly={isReadOnly}
                                onChange={switchAction}
                            />}

                            <TextCell name="ten_vt"
                                rowItem={item} index={index}
                                placeholder={['so3.ten_vt', 'Tên vật tư']}
                                onChange={switchAction}
                                readOnly={isReadOnly}
                            />
                            {ph.ma_gd !== '3' && <SelectCell lookup={ZenLookup.Ma_kho} error={error}
                                name="ma_kho" rowItem={item} index={index}
                                placeholder={['so3.ma_kho', 'mã kho']}
                                readOnly={isReadOnly}
                                onChange={switchAction}
                                // set focus khi thêm mới
                                autoFocus={(item.autoFocus && ct.length === index + 1) ? true : false}
                            />}

                            <DropDownCell lookup={{ ...ZenLookup.Ma_dvt }} error={error}
                                name="dvt" rowItem={item} index={index} width={"100px"}
                                placeholder={['so3.dvt', 'Đơn vị tính']}
                                readOnly={isReadOnly}
                                onChange={switchAction}
                            />

                            <NumberCell name="so_luong"
                                rowItem={item} index={index}
                                placeholder={["so3.so_luong", "Số lượng"]}
                                onChange={switchAction} error={error}
                                readOnly={isReadOnly}
                                decimalScale={VoucherHelper.decimal4Sl()}
                                // set focus khi thêm mới
                                autoFocus={(item.ma_kho && item.autoFocus && ct.length === index + 1) ? true : false}
                            />

                            {ph.ma_gd !== '3' && <NumberCell name="ton"
                                rowItem={item} index={index}
                                placeholder={["so3.ton", "Tồn"]}
                                onChange={switchAction} error={error}
                                readOnly={true}
                            />}

                            {ph.ma_nt !== "VND" && <NumberCell name="gia_nt2"
                                rowItem={item} index={index} decimalScale={VoucherHelper.decimal4Nt()}
                                placeholder={["so3.gia_nt", "Giá NT", { ma_nt: ph.ma_nt }]}
                                onChange={switchAction} error={error}
                                readOnly={isReadOnly}
                            />}

                            {ph.ma_nt !== "VND" && <NumberCell name="tien_nt2"
                                rowItem={item} index={index} decimalScale={VoucherHelper.decimal4Nt()}
                                placeholder={["so3.tien_nt", "Tiền NT", { ma_nt: ph.ma_nt }]}
                                onChange={switchAction} error={error}
                                readOnly={isReadOnly}
                            />}

                            <NumberCell name="gia2"
                                rowItem={item} index={index}
                                placeholder={["so3.gia", "Giá", { ma_nt: 'VND' }]}
                                onChange={switchAction} error={error}
                                readOnly={isReadOnly}
                            />

                            <NumberCell name="tien2"
                                rowItem={item} index={index}
                                placeholder={["so3.tien", "thành tiền", { ma_nt: 'VND' }]}
                                onChange={switchAction} error={error}
                                readOnly={isReadOnly}
                            />

                            <NumberCell name="tl_ck" decimalScale={VoucherHelper.decimal4Ts()}
                                rowItem={item} index={index}
                                placeholder={["so3.tl_ck", "% Tỉ lệ chiết khấu "]}
                                onChange={switchAction} error={error}
                                readOnly={isReadOnly}
                            />

                            {ph.ma_nt !== "VND" && <NumberCell name="tien_ck_nt"
                                rowItem={item} index={index} decimalScale={VoucherHelper.decimal4Nt()}
                                placeholder={["so3.tien_ck_nt", "VAT NT", { ma_nt: ph.ma_nt }]}
                                onChange={switchAction} error={error}
                                readOnly={isReadOnly}
                            />}

                            <NumberCell name="tien_ck"
                                rowItem={item} index={index}
                                placeholder={["so3.tien_ck", "VAT VND", { ma_nt: 'VND' }]}
                                onChange={switchAction} error={error}
                                readOnly={isReadOnly}
                            />

                            <DropDownCell lookup={{ ...ZenLookup.Ma_thue }} error={error}
                                name="ma_thue" rowItem={item} index={index}
                                placeholder={['so3.gtgt', '% VAT']}
                                readOnly={isReadOnly}
                                onChange={switchAction}
                            />

                            {ph.ma_nt !== "VND" && <NumberCell name="thue_gtgt_nt"
                                rowItem={item} index={index} decimalScale={VoucherHelper.decimal4Nt()}
                                placeholder={["so3.thue_gtgt_nt", "VAT NT", { ma_nt: ph.ma_nt }]}
                                onChange={switchAction} error={error}
                                readOnly={isReadOnly}
                            />}

                            <NumberCell name="thue_gtgt"
                                rowItem={item} index={index}
                                placeholder={["so3.thue_gtgt", "VAT VND", { ma_nt: 'VND' }]}
                                onChange={switchAction} error={error}
                                readOnly={isReadOnly}
                            />

                            {ph.ma_gd !== '3' && ph.ma_nt !== "VND" && <NumberCell name="gia_nt"
                                rowItem={item} index={index} decimalScale={VoucherHelper.decimal4Nt()}
                                placeholder={["so3.gia_nt", "Giá vốn NT", { ma_nt: ph.ma_nt }]}
                                onChange={switchAction} error={error}
                                readOnly={isGiaDichDanh(item)}
                            />}

                            {ph.ma_gd !== '3' && ph.ma_nt !== "VND" && <NumberCell name="tien_nt"
                                rowItem={item} index={index} decimalScale={VoucherHelper.decimal4Nt()}
                                placeholder={["so3.tien_nt", "Tiền vốn NT", { ma_nt: ph.ma_nt }]}
                                onChange={switchAction} error={error}
                                readOnly={isGiaDichDanh(item)}
                            />}

                            {ph.ma_gd !== '3' && <NumberCell name="gia"
                                rowItem={item} index={index}
                                placeholder={["so3.gia", "Giá vốn VND", { ma_nt: 'VND' }]}
                                onChange={switchAction} error={error}
                                readOnly={isGiaDichDanh(item)}
                            />}

                            {ph.ma_gd !== '3' && <NumberCell name="tien"
                                rowItem={item} index={index}
                                placeholder={["so3.tien", "tiền vốn VND", { ma_nt: 'VND' }]}
                                onChange={switchAction} error={error}
                                readOnly={isGiaDichDanh(item)}
                            />}

                            <SelectCell lookup={{ ...ZenLookup.Ma_nvkd, formatInput: "{ma_nvkd}", }}
                                error={error}
                                name="ma_nvkd" rowItem={item} index={index}
                                placeholder={['so3.ma_nvkd', 'Mã NVKD']}
                                readOnly={isReadOnly}
                                onChange={switchAction}
                            />

                            <ConfigBySiDmCt
                                rowItem={item} index={index}
                                itemSiDmCt={itemSiDmCt}
                                readOnly={isReadOnly}
                                switchAction={switchAction}
                            />

                            {isReadOnly === false && <DeleteCell collapsing
                                rowItem={item} index={index}
                                onClick={switchAction} />}
                        </Table.Row>
                    })
                }

                {isReadOnly === false && <Table.Row>
                    {ph.ma_gd !== '3' && <SelectCell isAdd clearable={false} name="addRow"
                        lookup={ZenLookup.Ma_vt}
                        placeholder={['so3.ma_vt', 'Mã VT']}
                        onChange={switchAction}
                    />}

                    {ph.ma_gd == '3' && <SelectCell isAdd clearable={false} name="addRow"
                        lookup={{
                            ...ZenLookup.Ma_vt,
                            where: "loai = '4' or loai = '9'",
                            onLocalWhere: (items) => {
                                return items.filter(t => t.loai == 4 || t.loai == 9)
                            }
                        }}
                        placeholder={['so3.ma_vt', 'Mã VT']}
                        onChange={switchAction}
                    />}
                    <Table.Cell colSpan={24} />
                </Table.Row>}
            </Table.Body>
        </TableScroll>
    </>
}

const CTFormTotal = ({ ph }) => {
    return <Grid columns="2">
        <Grid.Column width="8" />
        <Grid.Column width="8" textAlign="right">
            <TableTotalPH maNt={ph.ma_nt}>
                <RowTotalPH text="Tổng số lượng" value={ph.t_so_luong} maNt={ph.ma_nt} oneCol />
                <RowTotalPH text="Tổng tiền" value={ph.t_tien2} valueNT={ph.t_tien_nt2} maNt={ph.ma_nt} />
                <RowTotalPH text="Tổng chiết khấu" value={ph.t_ck} valueNT={ph.t_ck_nt} maNt={ph.ma_nt} />
                <RowTotalPH text="Thuế GTGT" value={ph.t_thue} valueNT={ph.t_thue_nt} maNt={ph.ma_nt} />
                <RowTotalPH text="Tổng thanh toán" value={ph.t_tt} valueNT={ph.t_tt_nt} maNt={ph.ma_nt} />
            </TableTotalPH>
        </Grid.Column>
    </Grid>
}

const onValidCT = (ph, ct, currentRowCT) => {
    const { item, index } = currentRowCT
    const errorField = {}

    if (!item.ma_vt) {
        errorField.ma_vt = ["Mã hàng không được để trống"]
    }
    if (!item.ma_kho && !item.ton_kho === false) {
        errorField.ma_kho = ["Mã kho không được để trống"]
    }
    if (!item.dvt) {
        errorField.dvt = ["đơn vị tính không được để trống"]
    }
    if (VoucherHelper.f_NullToZero(item.gia2) === 0) {
        errorField.gia2 = ["Giá trị phải lớn hơn 0"]
    }
    if (ph.ma_nt !== 'VND' && VoucherHelper.f_NullToZero(item.gia_nt2) === 0) {
        errorField.gia_nt2 = ["Giá trị phải lớn hơn 0"]
    }
    if (VoucherHelper.f_NullToZero(item.so_luong) === 0) {
        errorField.so_luong = ["Giá trị phải lớn hơn 0"]
    }

    // do trường này thuộc CT, nhưng nằm ở tab Hach Toan
    // nên cần set tabName để khi valid sẽ chuyển sang tab Hach Toan
    // tabName là tương ứng với name trong addTab đã khai báo bên dưới
    if (!item.tk_dt) {
        errorField.tk_dt = ["Tk doanh thu không được để trống"]
        errorField.tabName = 'HachToan'
    }
    if (!item.tk_gv) {
        errorField.tk_gv = ["Tk giá vốn không được để trống"]
        errorField.tabName = 'HachToan'
    }
    if (!item.tk_vt) {
        errorField.tk_vt = ["Tk kho không được để trống"]
        errorField.tabName = 'HachToan'
    }

    return Object.keys(errorField).length > 0 ? errorField : undefined
}

const TabHachToan = ({ ph, ct, permission, modeForm }) => {
    const { onChangeCT, errorCT, onUpdatePHCT } = useContext(ContextVoucher)
    let isReadOnly = VoucherHelper.f_ReadOnly(modeForm, permission)

    const switchAction = (propsElement, type, itemCurrent, index) => {
        const { name, value, itemSelected } = propsElement

        switch (type) {
            case ActionType.TEXT_CHANGE:
                // set value 
                itemCurrent[name] = value
                onChangeCT(itemCurrent, index, type, {}, propsElement)
                break;
            default:
                console.log(type)
                break;
        }
    }

    const changeTK = (e, { name, value }) => {
        onUpdatePHCT({ [name]: value })
    }

    return <>
        <TableScroll>
            <Table.Header>
                <RowHeaderCell colAction={true}
                    header={[
                        { text: ['so3ht.ten_vt', 'Tên vật tư'] },

                        { text: ['so3ht.tien2', 'Tiền hàng'], maNT: ph.ma_nt, isNT: true },
                        { text: ['so3ht.tien_ck', 'Giảm giá'], maNT: ph.ma_nt, isNT: true },
                        { text: ['so3ht.thue_gtgt', 'VAT'], maNT: ph.ma_nt, isNT: true },

                        { text: ['so3ht.tien2', 'Tiền hàng'], maNT: 'VND' },
                        { text: ['so3ht.tien_ck', 'Giảm giá'], maNT: 'VND' },
                        { text: ['so3ht.thue_gtgt', 'VAT'], maNT: 'VND' },

                        { text: ['so3ht.tk_dt', 'Tk doanh thu'] },
                        { text: ['so3ht.tk_gv', 'Tk giá vốn'] },
                        { text: ['so3ht.tk_vt', 'Tk kho'] },
                        { text: ['so3ht.tk_ck', 'Tk chiết khấu'] },
                    ]}
                />
            </Table.Header>
            <Table.Body>

                {
                    ct && ct.map((item, index) => {
                        const error = errorCT ? errorCT.find(x => x.index === index) : undefined
                        return <Table.Row key={item.stt_rec0 ? item.stt_rec0 : ('REC' + index)}>
                            <TextCell name="ten_vt"
                                rowItem={item} index={index}
                                placeholder={['so3.ten_vt', 'Tên vật tư']}
                                readOnly
                            />
                            {ph.ma_nt !== "VND" && <NumberCell name="tien_nt2"
                                rowItem={item} index={index} decimalScale={2}
                                placeholder={["so3ht.tien_nt2", "Tiền hàng", { ma_nt: ph.ma_nt }]}
                                readOnly
                            />}
                            {ph.ma_nt !== "VND" && <NumberCell name="tien_ck_nt"
                                rowItem={item} index={index} decimalScale={2}
                                placeholder={["so3ht.tien_ck", "Giảm giá", { ma_nt: ph.ma_nt }]}
                                readOnly
                            />}
                            {ph.ma_nt !== "VND" && <NumberCell name="thue_gtgt_nt"
                                rowItem={item} index={index} decimalScale={2}
                                placeholder={["so3ht.thue_gtgt", "VAT", { ma_nt: ph.ma_nt }]}
                                readOnly
                            />}

                            <NumberCell name="tien2"
                                rowItem={item} index={index}
                                placeholder={["so3ht.tien2", "Tiền hàng", { ma_nt: 'VND' }]}
                                readOnly
                            />
                            <NumberCell name="tien_ck"
                                rowItem={item} index={index}
                                placeholder={["so3ht.tien_ck", "Giảm giá", { ma_nt: 'VND' }]}
                                readOnly
                            />
                            <NumberCell name="thue_gtgt"
                                rowItem={item} index={index}
                                placeholder={["so3ht.thue_gtgt", "VAT", { ma_nt: 'VND' }]}
                                readOnly
                            />

                            <SelectCell lookup={{
                                ...ZenLookup.TK, where: 'chi_tiet = 1',
                                onLocalWhere: (items) => {
                                    return items.filter(t => t.chi_tiet == true)
                                },
                                formatInput: "{tk_dt}",
                            }}
                                error={error}
                                name="tk_dt" rowItem={item} index={index}
                                placeholder={['so3ht.tk_dt', 'Tk Dt']}
                                readOnly={isReadOnly}
                                onChange={switchAction}
                            />
                            <SelectCell lookup={{
                                ...ZenLookup.TK, where: 'chi_tiet = 1',
                                onLocalWhere: (items) => {
                                    return items.filter(t => t.chi_tiet == true)
                                },
                                formatInput: "{tk_gv}",
                            }}
                                error={error}
                                name="tk_gv" rowItem={item} index={index}
                                placeholder={['so3ht.tk_gv', 'Tk gv']}
                                readOnly={isReadOnly}
                                onChange={switchAction}
                            />
                            <SelectCell lookup={{
                                ...ZenLookup.TK, where: 'chi_tiet = 1',
                                onLocalWhere: (items) => {
                                    return items.filter(t => t.chi_tiet == true)
                                },
                                formatInput: "{tk_vt}",
                            }}
                                error={error}
                                name="tk_vt" rowItem={item} index={index}
                                placeholder={['so3ht.tk_vt', 'Tk kho']}
                                readOnly={isReadOnly}
                                onChange={switchAction}
                            />
                            <SelectCell lookup={{
                                ...ZenLookup.TK, where: 'chi_tiet = 1',
                                onLocalWhere: (items) => {
                                    return items.filter(t => t.chi_tiet == true)
                                },
                                formatInput: "{tk_ck}",
                            }}
                                error={error}
                                name="tk_ck" rowItem={item} index={index}
                                placeholder={['so3ht.tk_ck', 'Tk ck']}
                                readOnly={isReadOnly}
                                onChange={switchAction}
                            />
                        </Table.Row>
                    })
                }
            </Table.Body>
        </TableScroll>

        <Form style={{ paddingTop: "14px" }}>
            <Form.Group>
                <ZenFieldSelectApi loadApi width="3"
                    lookup={{
                        ...ZenLookup.TK,
                        format: "{tk}",
                        where: "chi_tiet = 1",
                        onLocalWhere: (items) => {
                            return items.filter(t => t.chi_tiet == true)
                        }
                    }}
                    readOnly={true}
                    label={"so3.tk_pt"} defaultlabel="Tài khoản phải thu"
                    name="tk_pt" value={ph.tk_pt}
                    onChange={changeTK}
                />
                <ZenFieldSelectApi loadApi width="3"
                    lookup={{
                        ...ZenLookup.TK,
                        format: "{tk}",
                        where: "chi_tiet = 1",
                        onLocalWhere: (items) => {
                            return items.filter(t => t.chi_tiet == true)
                        }
                    }}
                    readOnly={isReadOnly}
                    label={"so3.tk_thue"} defaultlabel="Tài khoản thuế"
                    name="tk_thue" value={ph.tk_thue}
                    onChange={changeTK}
                />
                <ZenFieldSelectApi loadApi width="3"
                    lookup={{
                        ...ZenLookup.TK,
                        format: "{tk}",
                        where: "chi_tiet = 1",
                        onLocalWhere: (items) => {
                            return items.filter(t => t.chi_tiet == true)
                        }
                    }}
                    readOnly={isReadOnly}
                    label={"so3.tk_ck_ds"} defaultlabel="TK chiết khấu doanh số"
                    name="tk_ck_ds" value={ph.tk_ck_ds}
                    onChange={changeTK}
                />
            </Form.Group>
        </Form>
    </>
}

export const SOVchSO3Edit = {
    PHForm: PHForm,
    CTForm: CTForm,
    CTFormTotal: CTFormTotal,
    onValidCT: onValidCT,
    // thêm tab mới
    addTab: [
        {
            visible: true,
            name: "HachToan",
            title: "Hạch toán",
            component: TabHachToan
        },
    ],
    invoiceInfo: {},
    ma_ct: 'SO3',
    formId: "SOVchSO3",
    route: {
        add: routes.SOVchSO3New,
        edit: routes.SOVchSO3Edit()
    },
    linkHeader: {
        id: "sovchso3",
        defaultMessage: "Hóa đơn bán hàng hóa dịch vụ",
        route: routes.SOVchSO3,
        active: false,
    },
    formId: "SOVchSO3",
    api: {
        url: ApiSOVchSO3,
    },
    action: {
        view: { visible: true, permission: permissions.SO3Xem },
        add: { visible: true, permission: permissions.SO3Them },
        edit: { visible: true, permission: permissions.SO3Sua },
        del: { visible: true, permission: permissions.SO3Xoa }
    },
    initItem: {
        stt_rec: "", so_ct: "", ma_ct: "SO3", ma_nt: "VND", ty_gia: 1, ma_gd: "",
        ngay_ct: ZenHelper.formatDateTime(new Date(), "YYYY-MM-DD"),
        ngay_lct: ZenHelper.formatDateTime(new Date(), "YYYY-MM-DD"),
        ma_kh: "", ma_httt: "", dia_chi_vat: "", dien_giai: "", nguoi_gd: "",
        trang_thai: "", t_tien: 0, t_tien_nt: 0, t_so_luong: 0,
        //Todo:để tạm (fix post)
        opt1: "1900-01-01T00:00:00",
        opt2: "1900-01-01T00:00:00",
    },
    formValidation: [
        {
            id: "ngay_ct",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ]
        },
        {
            id: "so_ct",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ],
        },
        {
            id: "ma_kh",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ]
        },
        {
            id: "ma_httt",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ]
        },
    ]
}
