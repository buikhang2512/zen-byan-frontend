﻿import React, { useEffect, useRef, useState } from "react";
import { Button, Form, Icon, Label, Modal } from "semantic-ui-react";
import { ZenButton, ZenDatePeriod, ZenField, ZenFieldSelectApi, ZenFormik } from "../../../../components/Control";
import { ZenHelper } from "../../../../utils/global";
import * as routes from "../../../../constants/routes";
import { ApiSOVchSO4 } from "../../Api/index";
import { ZenLookup } from "../../../ComponentInfo/Dictionary/ZenLookup";
import { zzControlHelper } from "../../../../components/Control/zzControlHelper";
import { GlobalStorage, KeyStorage } from "../../../../utils/storage";

const styles = {
    label: {
        marginTop: "0.14285714em",
        marginBottom: "0.14285714em"
    }
}

const ContainerFilter = (props) => {
    const [open, setOpen] = useState(false)
    const [data, setData] = useState()
    const [value, setValue] = useState()
    const [sql, setSql] = useState("1=1")
    const refFormik = useRef();
    const { onLoadData, fieldCode } = props;
    const globalStorage = GlobalStorage.get(KeyStorage.Global) || {};
    const initItem = {
        ngay1: globalStorage.from_date,
        ngay2: globalStorage.to_date,
    }
    // Set drag drop
    useEffect(() => {
        zzControlHelper.dragElement(document.getElementById('Filter-form'), 'header-Filter-form');
    })
    // Lắng nghe sự kiện khi có thay đổi query
    useEffect(() => {
        onLoadData(sql)
    }, [sql]);

    // Bắt sk mở lại modal setdata
    useEffect(() => {
        if (open) {
            refFormik.current.setValues({
                ...refFormik.current.values,
                ...data,
            })
        }
    }, [open])

    const handleSearch = (values) => {
        let _sql = "";
        if (values.ngay1) _sql += ` AND ngay_ct >= '${values.ngay1}'`;
        if (values.ngay2) _sql += ` AND ngay_ct <= '${values.ngay2}'`;
        if (values.so_ct1) _sql += ` AND so_ct >= '${values.so_ct1}'`;
        if (values.so_ct2) _sql += ` AND so_ct <= '${values.so_ct2}'`;
        if (values.ma_kh) _sql += ` AND ma_kh = '${values.ma_kh}'`;
        if (values.ma_so_thue) _sql += ` AND ma_so_thue = '${values.ma_so_thue}'`;
        if (values.ma_httt) _sql += ` AND ma_httt = '${values.ma_httt}'`;
        if (values.dien_giai) _sql += ` AND dien_giai LIKE '%${values.dien_giai}%'`;
        if (!_sql) {
            setData(null)
        } else {
            setData(values)
        }
        setSql(_sql.replace("AND", ""))
        setOpen(false)
    }

    const handleChangeDate = ({ startDate, endDate }) => {
        refFormik.current.setValues({
            ...refFormik.current.values,
            ngay1: startDate,
            ngay2: endDate
        })
    }

    const handleSelectKH = (itemSelected, { name }) => {
        refFormik.current.setValues({
            ...refFormik.current.values,
            ma_kh: itemSelected.ma_kh,
        })
    }

    const handleDelete = (e, i) => {
        if (i == 'all') {
            handleSearch("")
        } else {
            delete data[i]
            handleSearch(data)
        }
    }

    return <>
        <Modal id="Filter-form"
            closeOnEscape closeIcon closeOnDimmerClick={false}
            onClose={() => setOpen(false)}
            onOpen={() => setOpen(true)}
            open={open}
            size={"tiny"}
            trigger={<div style={{ display: "inline-block" }}><Button basic icon><Icon name="filter" link /></Button></div>}
        >
            <Modal.Header id='header-Filter-form' style={{ cursor: "grabbing" }}>
                Tìm kiếm
            </Modal.Header>
            <Modal.Content>
                <ZenFormik ref={refFormik}
                    initItem={initItem}
                >
                    {
                        formikProps => {
                            const { values } = formikProps
                            setValue(values)

                            return <Form>
                                <ZenDatePeriod
                                    onChange={handleChangeDate}
                                    value={[values.ngay1, values.ngay2]}
                                    textLabel="Từ ngày - đến ngày"
                                    defaultPopupYear={ZenHelper.getFiscalYear()}
                                />

                                <Form.Group widths="equal">
                                    <ZenField label={"so3.so_ct1"} defaultlabel="Số chứng từ"
                                        name="so_ct1" props={formikProps}
                                    />
                                    <ZenField label={"so3.so_ct2"} defaultlabel="Đến số"
                                        name="so_ct2" props={formikProps}
                                    />
                                </Form.Group>
                                <Form.Group>
                                    <ZenFieldSelectApi width={16} loadApi
                                        lookup={ZenLookup.Ma_kh}
                                        label={"so3.ma_kh"} defaultlabel="Khách hàng"
                                        name="ma_kh" formik={formikProps}
                                        onItemSelected={handleSelectKH}
                                    />
                                </Form.Group>
                                <Form.Group>
                                    <ZenField name="ma_so_thue" width={16}
                                        label={"so3.ma_so_thue"} defaultlabel="Mã số thuế"
                                        props={formikProps}
                                    />
                                </Form.Group>
                                <Form.Group>
                                    <ZenFieldSelectApi width={16} loadApi
                                        lookup={{
                                            ...ZenLookup.Ma_httt,
                                            onLocalWhere: (items) => {
                                                return items?.filter(t => t.moduleid == "SO")
                                            }
                                        }}
                                        name="ma_httt"
                                        label={"so3.ma_httt"} defaultlabel="Phương thức thanh toán"
                                        formik={formikProps}
                                    />
                                </Form.Group>
                                <ZenField
                                    label={"so3.dien_giai"} defaultlabel="Diễn giải"
                                    name="dien_giai" props={formikProps} />
                            </Form>
                        }
                    }
                </ZenFormik>
            </Modal.Content>

            <Modal.Actions>
                <ZenButton btnType={"cancel"} size="small" onClick={() => setOpen(false)} />
                <Button content="Tìm" icon="search" size="small"
                    primary
                    onClick={() => handleSearch(value)}
                />
            </Modal.Actions>
        </Modal>
        {data && data.ngay1 && <Label as='a' color={'grey'} style={styles.label}>Từ ngày : {ZenHelper.formatDateTime(data.ngay1, 'DD/MM/YYYY')}<Icon name='delete' onClick={(e) => handleDelete(e, 'ngay1')} /></Label>}
        {data && data.ngay2 && <Label as='a' color={'grey'} style={styles.label}>Đến ngày : {ZenHelper.formatDateTime(data.ngay2, 'DD/MM/YYYY')}<Icon name='delete' onClick={(e) => handleDelete(e, 'ngay2')} /></Label>}
        {data && data.so_ct1 && <Label as='a' color={'grey'} style={styles.label}>Từ số : {data.so_ct1}<Icon name='delete' onClick={(e) => handleDelete(e, 'so_ct1')} /></Label>}
        {data && data.so_ct2 && <Label as='a' color={'grey'} style={styles.label}>Đến số : {data.so_ct2}<Icon name='delete' onClick={(e) => handleDelete(e, 'so_ct2')} /></Label>}
        {data && data.ma_kh && <Label as='a' color={'grey'} style={styles.label}>Khách hàng : {data.ma_kh}<Icon name='delete' onClick={(e) => handleDelete(e, 'ma_kh')} /></Label>}
        {data && data.ma_so_thue && <Label as='a' color={'grey'} style={styles.label}>MST : {data.ma_so_thue}<Icon name='delete' onClick={(e) => handleDelete(e, 'ma_so_thue')} /></Label>}
        {data && data.ma_httt && <Label as='a' color={'grey'} style={styles.label}>Phương thức thanh toán : {data.ma_httt}<Icon name='delete' onClick={(e) => handleDelete(e, 'ma_httt')} /></Label>}
        {data && data.dien_giai && <Label as='a' color={'grey'} style={styles.label}>Diễn giải : {data.dien_giai}<Icon name='delete' onClick={(e) => handleDelete(e, 'dien_giai')} /></Label>}
        {data && JSON.stringify(data) !== '{}' && <a style={{ cursor: "pointer", fontWeight: "bold", margin: "5px" }} onClick={(e) => handleDelete(e, 'all')}>Xóa điều kiện lọc</a>}
    </>
}

export const SOVchSO4List = {
    route: routes.SOVchSO4,

    action: {
        view: { visible: true, permission: "06.02.1" },
        add: { visible: true, permission: "06.02.2", link: { route: routes.SOVchSO4New } },
        edit: { visible: true, permission: "06.02.3", link: { route: routes.SOVchSO4Edit(), params: "stt_rec" } },
        del: { visible: true, permission: "06.02.4" },
    },

    linkHeader: {
        id: "sovchso4",
        defaultMessage: "Nhập hàng bán trả lại",
        active: true
    },

    tableList: {
        unPagination: false,
        fieldCode: "stt_rec",
        ma_ct: 'SO4',
        ContainerTop: ContainerFilter,

        api: {
            url: ApiSOVchSO4,
            type: "sql",
        },
        columns: [
            { id: "so4.ngay_ct", defaultMessage: "Ngày CTừ", fieldName: "ngay_ct", filter: "date", sorter: true, },
            { id: "so4.so_ct", defaultMessage: "Số CTừ", fieldName: "so_ct", filter: "string", sorter: true, link: { route: routes.SOVchSO4Edit(), params: "stt_rec" } },
            { id: "so4.ma_kh", defaultMessage: "Mã khách hàng", fieldName: "ma_kh", filter: "string", sorter: true, },
            { id: "so4.Ten_kh", defaultMessage: "Tên KH", fieldName: "ten_kh", filter: "string", sorter: true, },
            { id: "so4.t_tien_nt2", defaultMessage: "Tiền hàng", fieldName: "t_tien_nt2", filter: "number", sorter: true, },
            { id: "so4.t_ck_nt", defaultMessage: "chiết khấu", fieldName: "t_ck_nt", filter: "number", sorter: true, },
            { id: "so4.t_thue_nt", defaultMessage: "VAT", fieldName: "t_thue_nt", filter: "number", sorter: true, },
            { id: "so4.t_tt_nt", defaultMessage: "Thanh toán", fieldName: "t_tt_nt", filter: "number", sorter: true, },
            { id: "so4.ma_nt", defaultMessage: "Mã NT", fieldName: "ma_nt", filter: "string", sorter: true, },
            { id: "so4.tk_pt", defaultMessage: "TK pt", fieldName: "tk_pt", filter: "string", sorter: true, },
            { id: "so4.dien_giai", defaultMessage: "Diễn giải", fieldName: "dien_giai", filter: "string", sorter: true, },
            { id: "so4.ma_hd", defaultMessage: "Ma HD", fieldName: "ma_hd", filter: "string", sorter: true, },
            { id: "so4.ma_bp", defaultMessage: "Ma BP", fieldName: "ma_bp", filter: "string", sorter: true, },
            { id: "so4.ma_nvkd", defaultMessage: "NVKD", fieldName: "ma_nvkd", filter: "string", sorter: true, },
        ],
    },
}
