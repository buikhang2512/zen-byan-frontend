﻿import React, { useContext, useState } from "react";
import { Button, Checkbox, Form, Grid, Icon, Label, Table } from "semantic-ui-react";
import { ZenField, ZenFieldDate, ZenFieldSelectApi } from "../../../../components/Control/index";
import { ZenHelper } from "../../../../utils";
import { ApiSOVchSO4 } from "../../Api/index";
import {
    ContextVoucher, VoucherHelper,
    RowItemPH, NumberCell, TextCell, DeleteCell, RowHeaderCell,
    ActionType, SelectCell, TableScroll, TableTotalPH, RowTotalPH
} from "../../../../components/Control/zVoucher";
import { ZenLookup } from "../../../ComponentInfo/Dictionary/ZenLookup";
import * as routes from "../../../../constants/routes";
import { ConfigBySiDmCt, DropDownCell } from "../../../../components/Control/zVoucher/ZenVoucherHelper";
import { useIntl } from "react-intl";
import { SOVchSO4Lookup } from "./SOVchSO4Lookup";
import { GlobalStorage, KeyStorage } from "../../../../utils/storage";

const PHForm = ({ formik, permission, modeForm, FieldNT }) => {
    const { ct, tain, onUpdatePHCT } = useContext(ContextVoucher)
    let isReadOnly = VoucherHelper.f_ReadOnly(modeForm, permission)

    function getTyGia(ma_nt, ngay_ct) {
        let date_ct = ngay_ct ? ngay_ct : null
        let newArr = [];
        const getLocalTyGia = GlobalStorage.getByField(KeyStorage.CacheData, 'sidmtgnt').data
        newArr = getLocalTyGia.filter(i => i.ma_nt === ma_nt)
        if (newArr.length) {
            if (date_ct) {
                var findDateClosest = newArr.filter(i => ZenHelper.formatDateTime(i.ngay_tg, 'YYYY-MM-DD') <= date_ct)
                if (!findDateClosest || findDateClosest.length === 0) return 1;
                if (findDateClosest.length < 2) return findDateClosest[0].ty_gia;
                if (findDateClosest.length > 1) return findDateClosest[findDateClosest.length - 1].ty_gia
            } else { return null }
        }
        return null
    }

    function PHCT(ma_nt, ngay_ct) {
        const tygia = ma_nt === "VND" ? 1 : getTyGia(ma_nt, ngay_ct);
        const totalPH = {
            ma_nt: ma_nt,
            ty_gia: tygia,
            ngay_ct: ngay_ct,
            t_tien2: 0,
            t_tien_nt2: 0,
            t_ck: 0,
            t_ck_nt: 0,
            t_thue: 0,
            t_thue_nt: 0,
            t_tt: 0,
            t_tt_nt: 0
        }
        const newCT = ct.map(item => {
            // tính CT
            if (ma_nt === 'VND') {
                item.tien_nt2 = item.tien2
                item.gia_nt2 = item.gia2
                item.tien_ck_nt = item.tien_ck
                item.thue_gtgt_nt = item.thue_gtgt
                item.tt_nt = item.tt
            } else {
                item.tien2 = Math.round(item.tien_nt2 * tygia)
                item.gia2 = Math.round(item.gia_nt2 * tygia)
                item.tien_ck = Math.round(item.tien2 * (item.tl_ck / 100))
                item.thue_gtgt = Math.round((item.tien2 - item.tien_ck) * (item.ts_gtgt / 100))
                item.tt = item.tien2 - item.tien_ck + item.thue_gtgt
            }
            // tính PH
            totalPH.t_tien2 += VoucherHelper.f_NullToZero(item.tien2)
            totalPH.t_tien_nt2 += VoucherHelper.f_NullToZero(item.tien_nt2)

            totalPH.t_ck += VoucherHelper.f_NullToZero(item.tien_ck)
            totalPH.t_ck_nt += VoucherHelper.f_NullToZero(item.tien_ck_nt)

            totalPH.t_thue += VoucherHelper.f_NullToZero(item.thue_gtgt)
            totalPH.t_thue_nt += VoucherHelper.f_NullToZero(item.thue_gtgt_nt)

            totalPH.t_tt += VoucherHelper.f_NullToZero(item.tt)
            totalPH.t_tt_nt += VoucherHelper.f_NullToZero(item.tt_nt)
            return item
        });
        onUpdatePHCT(totalPH, newCT)
    }
    const handleChangeDatect = (e) => {
        const { ma_nt } = formik.values
        PHCT(ma_nt, e.value)
    }

    const handleChangeNT = ({ ma_nt, ty_gia }) => {
        const totalPH = {
            ma_nt: ma_nt,
            ty_gia: ty_gia,
            t_tien2: 0,
            t_tien_nt2: 0,
            t_ck: 0,
            t_ck_nt: 0,
            t_thue: 0,
            t_thue_nt: 0,
            t_tt: 0,
            t_tt_nt: 0
        }
        const newCT = ct.map(item => {
            // tính CT
            if (ma_nt === 'VND') {
                item.tien_nt2 = item.tien2
                item.gia_nt2 = item.gia2
                item.tien_ck_nt = item.tien_ck
                item.thue_gtgt_nt = item.thue_gtgt
                item.tt_nt = item.tt
            } else {
                item.tien2 = Math.round(item.tien_nt2 * ty_gia)
                item.gia2 = Math.round(item.gia_nt2 * ty_gia)
                item.tien_ck = Math.round(item.tien2 * (item.tl_ck / 100))
                item.thue_gtgt = Math.round((item.tien2 - item.tien_ck) * (item.ts_gtgt / 100))
                item.tt = item.tien2 - item.tien_ck + item.thue_gtgt
            }
            // tính PH
            totalPH.t_tien2 += VoucherHelper.f_NullToZero(item.tien2)
            totalPH.t_tien_nt2 += VoucherHelper.f_NullToZero(item.tien_nt2)

            totalPH.t_ck += VoucherHelper.f_NullToZero(item.tien_ck)
            totalPH.t_ck_nt += VoucherHelper.f_NullToZero(item.tien_ck_nt)

            totalPH.t_thue += VoucherHelper.f_NullToZero(item.thue_gtgt)
            totalPH.t_thue_nt += VoucherHelper.f_NullToZero(item.thue_gtgt_nt)

            totalPH.t_tt += VoucherHelper.f_NullToZero(item.tt)
            totalPH.t_tt_nt += VoucherHelper.f_NullToZero(item.tt_nt)
            return item
        });
        onUpdatePHCT(totalPH, newCT)
    }

    const handleSelectedLookup = (itemSelected, { name }) => {
        if (name === 'ma_kh') {
            formik.setValues({
                ...formik.values,
                [name]: itemSelected.ma_kh,
                dia_chi_vat: itemSelected.dia_chi,
                ma_so_thue: itemSelected.ma_so_thue,
                nguoi_gd: itemSelected.nguoi_gd,
                ma_httt: itemSelected.ma_httt || ""
            })
        } else if (name === "ma_httt") {
            // Formik chỉ render lại children của formik,
            // sử dụng onUpdatePHCT để render lại state
            const newPh = {
                ...formik.values,
                [name]: itemSelected.ma_httt,
                tk_pt: itemSelected.tk || "",
                tk_thue: itemSelected.tk_thue_gtgt_ban || "",
            }

            if (tain?.length > 0) {
                const newTain = tain.map(t => {
                    t.tk_du = itemSelected.tk;
                    t.tk_thue_no = itemSelected.tk_thue_gtgt_ban;
                    return t
                })
                onUpdatePHCT(newPh, null, { tain: newTain })
            } else {
                onUpdatePHCT(newPh, null)
            }
        }
    }

    return <React.Fragment>
        <Grid columns="3">

            <RowItemPH content={[
                {
                    width: 6,
                    input: <ZenFieldSelectApi required loadApi
                        tabIndex={1}
                        lookup={ZenLookup.Ma_kh}
                        label={"so4.ma_kh"} defaultlabel="Khách hàng"
                        name="ma_kh" formik={formik}
                        onItemSelected={handleSelectedLookup}
                        readOnly={isReadOnly}
                    />
                },
                {
                    width: 7,
                    input: <ZenField readOnly={isReadOnly} tabIndex={1}
                        label={"so4.dia_chi"} defaultlabel="Địa chỉ"
                        name="dia_chi_vat" props={formik}
                    />
                },
                {
                    width: 3,
                    input: <ZenFieldDate required readOnly={isReadOnly}
                        label={"so4.ngay_ct"} defaultlabel="Ngày chứng từ"
                        name="ngay_ct" props={formik} tabIndex={3}
                        onChange={handleChangeDatect}
                    />
                },
            ]} />

            <RowItemPH content={[
                {
                    width: 6,
                    input: <ZenField readOnly={isReadOnly} tabIndex={1}
                        label={"so4.nguoi_gd"} defaultlabel="Người giao dịch"
                        name="nguoi_gd" props={formik}
                    />
                },
                {
                    width: 7,
                    input: <ZenField readOnly={isReadOnly} tabIndex={1}
                        label={"so4.ma_so_thue"} defaultlabel="Mã số thuế"
                        name="ma_so_thue" props={formik}
                    />
                },
                {
                    width: 3,
                    input: <ZenField required readOnly={isReadOnly}
                        label={"so4.so_ct"} defaultlabel="Số chứng từ"
                        name="so_ct" props={formik} tabIndex={3}
                        isCode
                    />
                },
            ]} />

            <RowItemPH content={[
                {
                    width: 6,
                    input: <ZenField readOnly={isReadOnly} tabIndex={1}
                        label={"so4.dien_giai"} defaultlabel="Diễn giải"
                        name="dien_giai" props={formik}
                    />
                },
                { width: 7, input: undefined },
                {
                    width: 3,
                    input: <ZenFieldSelectApi readOnly={isReadOnly} tabIndex={3}
                        loadApi
                        lookup={ZenLookup.SoDmMHD}
                        label={"so4.so_seri"} defaultlabel="Số seri"
                        name="so_seri" formik={formik}
                    />
                },

            ]} />

            <RowItemPH content={[
                {
                    width: 6,
                    input: <ZenFieldSelectApi loadApi
                        lookup={{
                            ...ZenLookup.Ma_hd,
                            where: `loai = 1`
                        }}
                        tabIndex={1}
                        label={"so4.ma_hd"} defaultlabel="Mã hợp đồng"
                        name="ma_hd" formik={formik}
                        readOnly={isReadOnly}
                        onItemSelected={handleSelectedLookup}
                    />
                },
                { width: 7, input: undefined },
                {
                    width: 3,
                    input: <ZenField required readOnly={isReadOnly}
                        label={"so4.so_hd"} defaultlabel="Số hóa đơn"
                        name="so_hd" props={formik} tabIndex={3}
                    />
                },

            ]} />

            <RowItemPH content={[
                {
                    width: 6,
                    input: <ZenFieldSelectApi required
                        lookup={{
                            // ...ZenLookup.Ma_httt_so,
                            ...ZenLookup.Ma_httt,
                            onLocalWhere: (items) => {
                                return items.filter(t => t.moduleid == "SO")
                            }
                        }}
                        tabIndex={1}
                        label={"so4.ma_httt"} defaultlabel="Phương thức thanh toán"
                        name="ma_httt" formik={formik}
                        readOnly={isReadOnly}
                        onItemSelected={handleSelectedLookup}
                    />
                },
                { width: 7, input: undefined },
                {
                    width: 3,
                    input: <ZenFieldDate required readOnly={isReadOnly}
                        label={"so4.ngay_hd"} defaultlabel="Ngày hóa đơn"
                        name="ngay_hd" props={formik} tabIndex={3}
                    />
                },
            ]} />

            <RowItemPH content={[
                { width: 6, input: undefined },
                { width: 7, input: undefined },
                {
                    width: 3,
                    input: <FieldNT formik={formik} onChangeNT={handleChangeNT} tabIndex={3} />
                },

            ]} />
        </Grid>
    </React.Fragment>
}

const CTForm = ({ ph, ct, permission, modeForm }) => {
    const { onChangeCT, errorCT, itemSiDmCt, onUpdatePHCT } = useContext(ContextVoucher)
    let isReadOnly = VoucherHelper.f_ReadOnly(modeForm, permission)
    const intl = useIntl();

    const switchAction = (propsElement, type, itemCurrent, index) => {
        const { name, value, itemSelected } = propsElement
        const prevItemCurrent = { ...itemCurrent }

        switch (type) {
            case ActionType.TEXT_CHANGE:
                const { lookup } = propsElement
                itemCurrent[name] = value

                // set tên nếu là lookup
                // => cần để khởi tạo item cho input lookup, nếu hiển thị nhìu hơn 1 trường mã
                if (lookup) {
                    if (name === 'ma_vt') {
                        itemCurrent = {
                            ...itemCurrent,
                            ...validMaVt(itemSelected)
                        }
                        if (!prevItemCurrent['ma_thue']) {
                            itemCurrent['ma_thue'] = itemSelected['ma_thue']
                            itemCurrent['ts_gtgt'] = itemSelected['ts_gtgt']
                            f_calcNumber(propsElement, type, itemCurrent, index)
                            return
                        }
                    }
                    else if (name === 'ma_thue') {
                        itemCurrent['ts_gtgt'] = itemSelected['ts_gtgt']
                        f_calcNumber(propsElement, type, itemCurrent, index)
                        return
                    }
                }

                onChangeCT(itemCurrent, index, type, {}, propsElement)
                break;

            case ActionType.NUMBER_CHANGE:
                itemCurrent[name] = value;
                f_calcNumber(propsElement, type, itemCurrent, index);
                break;

            case ActionType.ADD_ROW:
                let newCT = ({
                    ...validMaVt(itemSelected),
                    ma_vt: value,
                    ma_thue: itemSelected.ma_thue,
                    ts_gtgt: itemSelected.ts_gtgt,

                    so_luong: 0,
                    gia2: 0, gia_nt2: 0,
                    tien2: 0, tien_nt2: 0,
                    tl_ck: 0,
                    tien_ck_nt: 0, tien_ck: 0,
                    thue_gtgt_nt: 0, thue_gtgt: 0,
                })
                if (itemSelected.ma_kho) {
                    newCT.ma_kho = itemSelected.ma_kho
                }

                onChangeCT(newCT, null, type)
                break;

            case ActionType.DELETE_ROW:
                // xóa dòng CT, tính lại PH
                const ctDel = ct.filter((item, idx) => idx !== index)
                const totalPH = {
                    t_so_luong: VoucherHelper.f_SumArray(ctDel, 'so_luong'),
                    t_tien2: VoucherHelper.f_SumArray(ctDel, 'tien2'),
                    t_tien_nt2: VoucherHelper.f_SumArray(ctDel, 'tien_nt2'),
                    t_thue: VoucherHelper.f_SumArray(ctDel, 'thue_gtgt'),
                    t_thue_nt: VoucherHelper.f_SumArray(ctDel, 'thue_gtgt_nt'),
                    t_ck: VoucherHelper.f_SumArray(ctDel, 'tien_ck'),
                    t_ck_nt: VoucherHelper.f_SumArray(ctDel, 'tien_ck_nt'),
                    t_tt: 0,
                    t_tt_nt: 0,
                }
                totalPH.t_tt = totalPH.t_tien2 + totalPH.t_thue - totalPH.t_ck
                totalPH.t_tt_nt = totalPH.t_tien_nt2 + totalPH.t_thue_nt - totalPH.t_ck_nt
                onChangeCT(itemCurrent, index, type, totalPH)
                break;
            default:
                break;
        }
    }
    function f_calcNumber(propsElement, type, itemCurrent, index) {
        const { name, } = propsElement

        if (name === 'so_luong') {
            changeSoLuong(itemCurrent)
        } else if (name === 'gia2') {
            changeGia2(itemCurrent)
        } else if (name === 'gia_nt2') {
            changeGia_nt2(itemCurrent)
        } else if (name === 'tien2') {
            changeTien2(itemCurrent)
        } else if (name === 'tien_nt2') {
            changeTien_nt2(itemCurrent)
        } else if (name === 'ma_thue' || name === "ma_vt") {
            changeMa_thue(itemCurrent)
        } else if (name === 'thue_gtgt') {
            changeThue_gtgt(itemCurrent)
        } else if (name === 'thue_gtgt_nt') {
            changeThue_gtgt_nt(itemCurrent)
        } else if (name === 'tl_ck') {
            changeTl_ck(itemCurrent)
        } else if (name === 'tien_ck') {
            changeTien_ck(itemCurrent)
        } else if (name === 'tien_ck_nt') {
            changeTien_ck_nt(itemCurrent)
        } else if (name === 'gia') {
            changeGia(itemCurrent)
        } else if (name === 'gia_nt') {
            changeGia_nt(itemCurrent)
        }

        //cập nhật PH, CT
        VoucherHelper.f_Timeout(() => {
            const totalPH = calcTotalPH(itemCurrent, index)
            onChangeCT(itemCurrent, index, type, totalPH, propsElement)
        })
    }
    function validMaVt(itemSelected) {
        return {
            ten_vt: itemSelected['ten_vt'],
            dvt: itemSelected['dvt'],
            ma_kho: itemSelected['ma_kho'],
            ton_kho: itemSelected['ton_kho'],
            tk_dt: itemSelected['tk_dt'],
            tk_vt: itemSelected['tk_vt'],
            tk_tl: itemSelected['tk_tl'],
            tk_gv: itemSelected['tk_gv'],
            tk_ck: itemSelected['tk_ck'],
        }
    }

    // =============================== VALUE CHANGE
    function changeSoLuong(item) {
        f_calcTien2(item)
        f_calcTien_Ck(item)
        f_calcThue(item)
        f_calcTT(item)
        f_calcTien(item) // tiền vốn
    }
    function changeGia2(item) {
        if (ph.ma_nt === 'VND') {
            item.gia_nt2 = item.gia2
        } else {
            item.gia_nt2 = VoucherHelper.f_RoundNumberByNT(item.gia2 / ph.ty_gia, ph.ma_nt)
        }
        changeSoLuong(item)
    }
    function changeGia_nt2(item) {
        item.gia2 = Math.round(item.gia_nt2 * ph.ty_gia)
        changeSoLuong(item)
    }
    function changeTien2(item) {
        if (ph.ma_nt === 'VND') {
            item.tien_nt2 = item.tien2
        } else {
            item.tien_nt2 = VoucherHelper.f_RoundNumberByNT(item.tien2 / ph.ty_gia, ph.ma_nt)
        }
        f_calcThue(item)
        f_calcTien_Ck(item)
        f_calcTT(item)
    }
    function changeTien_nt2(item) {
        item.tien2 = Math.round(item.tien_nt2 * ph.ty_gia)
        f_calcThue(item)
        f_calcTien_Ck(item)
        f_calcTT(item)
    }

    function changeTl_ck(item) {
        f_calcTien_Ck(item)
        f_calcThue(item)
        f_calcTT(item)
    }
    function changeTien_ck(item) {
        if (ph.ma_nt === 'VND') {
            item.tien_ck_nt = item.tien_ck
        } else {
            item.tien_ck_nt = VoucherHelper.f_RoundNumberByNT(item.tien_ck / ph.ty_gia, ph.ma_nt)
        }
        f_calcThue(item)
        f_calcTT(item)
    }
    function changeTien_ck_nt(item) {
        item.tien_ck = Math.round(item.tien_ck_nt * ph.ty_gia)
        f_calcThue(item)
        f_calcTT(item)
    }

    function changeMa_thue(item) {
        f_calcThue(item)
        f_calcTT(item)
    }
    function changeThue_gtgt(item) {
        if (ph.ma_nt === 'VND') {
            item.thue_gtgt_nt = item.thue_gtgt
        } else {
            item.thue_gtgt_nt = VoucherHelper.f_RoundNumberByNT(item.thue_gtgt / ph.ty_gia, ph.ma_nt)
        }
        f_calcTT(item)
    }
    function changeThue_gtgt_nt(item) {
        item.thue_gtgt = Math.round(item.thue_gtgt_nt * ph.ty_gia)
        f_calcTT(item)
    }

    function changeGia(item) {
        if (ph.ma_nt === 'VND') {
            item.gia_nt = item.gia
        } else {
            item.gia_nt = VoucherHelper.f_RoundNumberByNT(item.gia / ph.ty_gia, ph.ma_nt)
        }
        changeSoLuong(item)
    }
    function changeGia_nt(item) {
        item.gia = Math.round(item.gia_nt * ph.ty_gia)
        changeSoLuong(item)
    }

    // ==================================== tính toán
    function f_calcTien2(item) {
        item.tien_nt2 = VoucherHelper.f_RoundNumberByNT(item.so_luong * item.gia_nt2, ph.ma_nt)
        if (ph.ma_nt === 'VND') {
            item.tien2 = item.tien_nt2
        } else {
            item.tien2 = VoucherHelper.f_RoundNumberByNT(item.tien_nt2 * ph.ty_gia, ph.ma_nt)
        }
    }
    function f_calcTien_Ck(item) {
        item.tien_ck_nt = VoucherHelper.f_RoundNumberByNT(item.tien_nt2 * item.tl_ck / 100, ph.ma_nt)
        if (ph.ma_nt === 'VND') {
            item.tien_ck = item.tien_ck_nt
        } else {
            item.tien_ck = VoucherHelper.f_RoundNumberByNT(item.tien_ck_nt * ph.ty_gia, ph.ma_nt)
        }
    }
    function f_calcThue(item) {
        item.thue_gtgt_nt = VoucherHelper.f_RoundNumberByNT((item.tien_nt2 - item.tien_ck) * item.ts_gtgt / 100, ph.ma_nt)
        if (ph.ma_nt === 'VND') {
            item.thue_gtgt = item.thue_gtgt_nt
        } else {
            item.thue_gtgt = VoucherHelper.f_RoundNumberByNT(item.thue_gtgt_nt * ph.ty_gia, ph.ma_nt)
        }
    }
    function f_calcTien(item) {
        item.tien_nt = VoucherHelper.f_RoundNumberByNT(item.so_luong * item.gia_nt, ph.ma_nt)
        if (ph.ma_nt === 'VND') {
            item.tien = item.tien_nt
        } else {
            item.tien = VoucherHelper.f_RoundNumberByNT(item.tien_nt * ph.ty_gia, ph.ma_nt)
        }
    }
    function f_calcTT(item) {
        item.tt_nt = VoucherHelper.f_RoundNumberByNT(item.tien_nt2 + item.thue_gtgt_nt - item.tien_ck_nt, ph.ma_nt)
        if (ph.ma_nt === 'VND') {
            item.tt = item.tt_nt
        } else {
            item.tt = VoucherHelper.f_RoundNumberByNT(item.tt_nt * ph.ty_gia, ph.ma_nt)
        }
    }

    function calcTotalPH(itemCurrent, index) {
        // tạo CT mới
        const newCT = ct.map((item, idx) => idx === index ? itemCurrent : item)
        // tính tổng PH
        const totalPH = {
            t_so_luong: VoucherHelper.f_SumArray(newCT, 'so_luong'),
            t_tien2: VoucherHelper.f_SumArray(newCT, 'tien2'),
            t_tien_nt2: VoucherHelper.f_SumArray(newCT, 'tien_nt2'),
            t_thue: VoucherHelper.f_SumArray(newCT, 'thue_gtgt'),
            t_thue_nt: VoucherHelper.f_SumArray(newCT, 'thue_gtgt_nt'),
            t_ck: VoucherHelper.f_SumArray(newCT, 'tien_ck'),
            t_ck_nt: VoucherHelper.f_SumArray(newCT, 'tien_ck_nt'),
            t_tt: 0,
            t_tt_nt: 0
        }

        totalPH.t_tt = totalPH.t_tien2 + totalPH.t_thue - totalPH.t_ck
        totalPH.t_tt_nt = totalPH.t_tien_nt2 + totalPH.t_thue_nt - totalPH.t_ck_nt
        return totalPH
    }

    return <>
        <div style={{ textAlign: "right", paddingBottom: "14px" }}>
            <Checkbox checked={ph.gia_tb ? true : false} tabIndex={-1}
                name="gia_tb"
                label={intl.formatMessage({ id: "in1.gia_tb", defaultMessage: "Nhập theo giá trung bình" })}
                onChange={(e, { checked }) => onUpdatePHCT({ gia_tb: checked })}
                style={{ fontWeight: "bold" }}
                readOnly={isReadOnly}
            />
        </div>
        <TableScroll>
            <Table.Header>
                <RowHeaderCell colAction={isReadOnly}
                    itemSiDmCt={itemSiDmCt}
                    header={[
                        { text: ['so4.ma_vt', 'Mã hàng'] },
                        { text: ['so4.ten_vt', 'Tên hàng'] },
                        { text: ['so4.ma_kho', 'Mã Kho'] },
                        { text: ['so4.dvt', 'Đơn vị tính'] },
                        { text: ['so4.so_luong', 'Số lượng'] },
                        { text: ['so4.ton', 'Tồn'] },
                        { text: ['so4.gia_nt2', 'Giá'], maNT: ph.ma_nt, isNT: true },
                        { text: ['so4.tien_nt2', 'Thành tiền'], maNT: ph.ma_nt, isNT: true },
                        { text: ['so4.gia2', 'Giá'], maNT: 'VND' },
                        { text: ['so4.tien2', 'Thành tiền'], maNT: 'VND' },
                        { text: ['so4.tl_ck', '% GG'] },
                        { text: ['so4.tien_ck_nt', 'Giảm giá'], maNT: ph.ma_nt, isNT: true },
                        { text: ['so4.tien_ck', 'Giảm giá'], maNT: 'VND' },
                        { text: ['so4.ts_gtgt', '% VAT'] },
                        { text: ['so4.thue_gtgt_nt', 'VAT'], maNT: ph.ma_nt, isNT: true },
                        { text: ['so4.thue_gtgt', 'VAT'], maNT: 'VND' },
                        { text: ['so4.gia_nt', 'Giá vốn'], maNT: ph.ma_nt, isNT: true },
                        { text: ['so4.tien_nt', 'Tiền vốn'], maNT: ph.ma_nt, isNT: true },
                        { text: ['so4.gia', 'Giá vốn'], maNT: 'VND' },
                        { text: ['so4.tien', 'Tiền vốn'], maNT: 'VND' },
                        { text: ['so4.ma_nvkd', 'NVKD'] },
                        { text: ['so4.so_hd', 'Số HĐ'] },
                        { text: ['so4.so_dh', 'Số ĐH'] },
                    ]}
                />
            </Table.Header>
            <Table.Body>
                {
                    ct && ct.map((item, index) => {
                        const error = errorCT ? errorCT.find(x => x.index === index) : undefined

                        return <Table.Row key={item.stt_rec0 ? item.stt_rec0 : ('REC' + index)}>

                            <SelectCell lookup={ZenLookup.Ma_vt} error={error}
                                name="ma_vt" rowItem={item} index={index}
                                placeholder={['so4.ma_vt', 'Mã hàng']}
                                readOnly={isReadOnly}
                                onChange={switchAction}
                            />

                            <TextCell name="ten_vt"
                                rowItem={item} index={index}
                                placeholder={['so4.ten_vt', 'Tên vật tư']}
                                onChange={switchAction}
                                readOnly={isReadOnly}
                            />

                            <SelectCell lookup={{ ...ZenLookup.Ma_kho }} error={error}
                                name="ma_kho" rowItem={item} index={index}
                                placeholder={['so4.ma_kho', 'mã kho']}
                                readOnly={isReadOnly}
                                onChange={switchAction}
                                // set focus khi thêm mới
                                autoFocus={(item.autoFocus && ct.length === index + 1) ? true : false}
                            />

                            <DropDownCell lookup={{ ...ZenLookup.Ma_dvt }} error={error}
                                name="dvt" rowItem={item} index={index} width={"100px"}
                                placeholder={['so4.dvt', 'Đơn vị tính']}
                                readOnly={isReadOnly}
                                onChange={switchAction}
                            />

                            <NumberCell name="so_luong"
                                rowItem={item} index={index}
                                placeholder={["so4.so_luong", "Số lượng"]}
                                onChange={switchAction} error={error}
                                readOnly={isReadOnly}
                                decimalScale={VoucherHelper.decimal4Sl()}
                                // set focus khi thêm mới
                                autoFocus={(item.ma_kho && item.autoFocus && ct.length === index + 1) ? true : false}
                            />

                            <NumberCell name="ton"
                                rowItem={item} index={index}
                                placeholder={["so4.ton", "Tồn"]}
                                onChange={switchAction} error={error}
                                readOnly={true}
                            />

                            {ph.ma_nt !== "VND" && <NumberCell name="gia_nt2"
                                rowItem={item} index={index} decimalScale={VoucherHelper.decimal4Nt()}
                                placeholder={["so4.gia_nt", "Giá NT", { ma_nt: ph.ma_nt }]}
                                onChange={switchAction} error={error}
                                readOnly={isReadOnly}
                            />}

                            {ph.ma_nt !== "VND" && <NumberCell name="tien_nt2"
                                rowItem={item} index={index} decimalScale={VoucherHelper.decimal4Nt()}
                                placeholder={["so4.tien_nt", "Tiền NT", { ma_nt: ph.ma_nt }]}
                                onChange={switchAction} error={error}
                                readOnly={isReadOnly}
                            />}

                            <NumberCell name="gia2"
                                rowItem={item} index={index}
                                placeholder={["so4.gia", "Giá", { ma_nt: 'VND' }]}
                                onChange={switchAction} error={error}
                                readOnly={isReadOnly}
                            />

                            <NumberCell name="tien2"
                                rowItem={item} index={index}
                                placeholder={["so4.tien", "thành tiền", { ma_nt: 'VND' }]}
                                onChange={switchAction} error={error}
                                readOnly={isReadOnly}
                            />

                            <NumberCell name="tl_ck"
                                rowItem={item} index={index} decimalScale={VoucherHelper.decimal4Ts()}
                                placeholder={["so4.tl_ck", "% Tỉ lệ chiết khấu "]}
                                onChange={switchAction} error={error}
                                readOnly={isReadOnly}
                            />

                            {ph.ma_nt !== "VND" && <NumberCell name="tien_ck_nt"
                                rowItem={item} index={index} decimalScale={VoucherHelper.decimal4Nt()}
                                placeholder={["so4.tien_ck_nt", "VAT NT", { ma_nt: ph.ma_nt }]}
                                onChange={switchAction} error={error}
                                readOnly={isReadOnly}
                            />}

                            <NumberCell name="tien_ck"
                                rowItem={item} index={index}
                                placeholder={["so4.tien_ck", "VAT VND", { ma_nt: 'VND' }]}
                                onChange={switchAction} error={error}
                                readOnly={isReadOnly}
                            />

                            <DropDownCell lookup={{ ...ZenLookup.Ma_thue }} error={error}
                                name="ma_thue" rowItem={item} index={index}
                                placeholder={['so4.gtgt', '% VAT']}
                                readOnly={isReadOnly}
                                onChange={switchAction}
                            />

                            {ph.ma_nt !== "VND" && <NumberCell name="thue_gtgt_nt"
                                rowItem={item} index={index} decimalScale={VoucherHelper.decimal4Nt()}
                                placeholder={["so4.thue_gtgt_nt", "VAT NT", { ma_nt: ph.ma_nt }]}
                                onChange={switchAction} error={error}
                                readOnly={isReadOnly}
                            />}

                            <NumberCell name="thue_gtgt"
                                rowItem={item} index={index}
                                placeholder={["so4.thue_gtgt", "VAT VND", { ma_nt: 'VND' }]}
                                onChange={switchAction} error={error}
                                readOnly={isReadOnly}
                            />

                            {ph.ma_nt !== "VND" && <NumberCell name="gia_nt"
                                rowItem={item} index={index} decimalScale={VoucherHelper.decimal4Nt()}
                                placeholder={["so4.gia_nt", "Giá vốn NT", { ma_nt: ph.ma_nt }]}
                                onChange={switchAction} error={error}
                                readOnly={isReadOnly}
                            />}

                            {ph.ma_nt !== "VND" && <NumberCell name="tien_nt"
                                rowItem={item} index={index} decimalScale={VoucherHelper.decimal4Nt()}
                                placeholder={["so4.tien_nt", "Tiền vốn NT", { ma_nt: ph.ma_nt }]}
                                onChange={switchAction} error={error}
                                readOnly={isReadOnly}
                            />}

                            <NumberCell name="gia"
                                rowItem={item} index={index}
                                placeholder={["so4.gia", "Giá vốn VND", { ma_nt: 'VND' }]}
                                onChange={switchAction} error={error}
                                readOnly={isReadOnly}
                            />

                            <NumberCell name="tien"
                                rowItem={item} index={index}
                                placeholder={["so4.tien", "tiền vốn VND", { ma_nt: 'VND' }]}
                                onChange={switchAction} error={error}
                                readOnly={isReadOnly}
                            />

                            <SelectCell lookup={{ ...ZenLookup.Ma_nvkd }} error={error}
                                name="ma_nvkd" rowItem={item} index={index}
                                placeholder={['so4.ma_nvkd', 'Mã NVKD']}
                                readOnly={isReadOnly}
                                onChange={switchAction}
                            />

                            <TextCell name="so_hd"
                                rowItem={item} index={index}
                                placeholder={['so4.so_hd', 'Số HĐ']}
                                readOnly={isReadOnly}
                                onChange={switchAction}
                            />

                            <TextCell name="so_dh"
                                rowItem={item} index={index}
                                placeholder={['so4.so_dh', 'Số ĐH']}
                                onChange={switchAction}
                                readOnly={isReadOnly}
                            />

                            <ConfigBySiDmCt
                                rowItem={item} index={index}
                                itemSiDmCt={itemSiDmCt}
                                readOnly={isReadOnly}
                                switchAction={switchAction}
                            />

                            {isReadOnly === false && <DeleteCell collapsing
                                rowItem={item} index={index}
                                onClick={switchAction} />}
                        </Table.Row>
                    })
                }

                {isReadOnly === false && <Table.Row>
                    <SelectCell isAdd clearable={false} name="addRow"
                        lookup={ZenLookup.Ma_vt}
                        placeholder={['so4.ma_vt', 'Mã VT']}
                        onChange={switchAction}
                    />
                    <Table.Cell colSpan={26} />
                </Table.Row>}
            </Table.Body>
        </TableScroll>
    </>
}

const CTFormTotal = ({ ph }) => {
    return <Grid columns="2">
        <Grid.Column width="8" />
        <Grid.Column width="8" textAlign="right">
            <TableTotalPH maNt={ph.ma_nt}>
                <RowTotalPH text="Tổng số lượng" value={ph.t_so_luong} maNt={ph.ma_nt} oneCol />
                <RowTotalPH text="Tổng tiền" value={ph.t_tien2} valueNT={ph.t_tien_nt2} maNt={ph.ma_nt} />
                <RowTotalPH text="Tổng chiết khấu" value={ph.t_ck} valueNT={ph.t_ck_nt} maNt={ph.ma_nt} />
                <RowTotalPH text="Thuế GTGT" value={ph.t_thue} valueNT={ph.t_thue_nt} maNt={ph.ma_nt} />
                <RowTotalPH text="Tổng thanh toán" value={ph.t_tt} valueNT={ph.t_tt_nt} maNt={ph.ma_nt} />
            </TableTotalPH>
        </Grid.Column>
    </Grid>
}

const onValidCT = (ph, ct, currentRowCT) => {
    const { item, index } = currentRowCT
    const errorField = {}

    if (!item.ma_vt) {
        errorField.ma_vt = ["Mã hàng không được để trống"]
    }
    if (!item.ma_kho && !item.ton_kho === false) {
        errorField.ma_kho = ["Mã kho không được để trống"]
    }
    if (!item.dvt) {
        errorField.dvt = ["đơn vị tính không được để trống"]
    }
    if (VoucherHelper.f_NullToZero(item.gia2) === 0) {
        errorField.gia2 = ["Giá trị phải lớn hơn 0"]
    }
    if (ph.ma_nt !== 'VND' && VoucherHelper.f_NullToZero(item.gia_nt2) === 0) {
        errorField.gia_nt2 = ["Giá trị phải lớn hơn 0"]
    }
    if (VoucherHelper.f_NullToZero(item.so_luong) === 0) {
        errorField.so_luong = ["Giá trị phải lớn hơn 0"]
    }

    // do trường này thuộc CT, nhưng nằm ở tab Hach Toan
    // nên cần set tabName để khi valid sẽ chuyển sang tab Hach Toan
    // tabName là tương ứng với name trong addTab đã khai báo bên dưới
    if (!item.tk_tl) {
        errorField.tk_tl = ["Tk tl không được để trống"]
        errorField.tabName = 'HachToan'
    }
    if (!item.tk_gv) {
        errorField.tk_gv = ["Tk giá vốn không được để trống"]
        errorField.tabName = 'HachToan'
    }
    if (!item.tk_vt) {
        errorField.tk_vt = ["Tk kho không được để trống"]
        errorField.tabName = 'HachToan'
    }

    return Object.keys(errorField).length > 0 ? errorField : undefined
}

const TabHachToan = ({ ph, ct, permission, modeForm }) => {
    const { onChangeCT, errorCT, onUpdatePHCT } = useContext(ContextVoucher)
    let isReadOnly = VoucherHelper.f_ReadOnly(modeForm, permission)

    const switchAction = (propsElement, type, itemCurrent, index) => {
        const { name, value, itemSelected } = propsElement

        switch (type) {
            case ActionType.TEXT_CHANGE:
                // set value 
                itemCurrent[name] = value
                onChangeCT(itemCurrent, index, type, {}, propsElement)
                break;
            default:
                console.log(type)
                break;
        }
    }

    const changeTK = (e, { name, value }) => {
        onUpdatePHCT({ [name]: value })
    }

    return <>
        <TableScroll>
            <Table.Header>
                <RowHeaderCell colAction={true}
                    header={[
                        { text: ['so4ht.ten_vt', 'Tên vật tư'] },

                        { text: ['so4ht.tien2', 'Tiền hàng'], maNT: ph.ma_nt, isNT: true },
                        { text: ['so4ht.tien_ck', 'Giảm giá'], maNT: ph.ma_nt, isNT: true },
                        { text: ['so4ht.thue_gtgt', 'VAT'], maNT: ph.ma_nt, isNT: true },
                        { text: ['so4ht.tien', 'Tiền vốn'], maNT: ph.ma_nt, isNT: true },

                        { text: ['so4ht.tien2', 'Tiền hàng'], maNT: 'VND' },
                        { text: ['so4ht.tien_ck', 'Giảm giá'], maNT: 'VND' },
                        { text: ['so4ht.thue_gtgt', 'VAT'], maNT: 'VND' },
                        { text: ['so4ht.tien', 'Tiền vốn'], maNT: 'VND' },

                        { text: ['so4ht.tk_tl', 'Tk hbtl'] },
                        { text: ['so4ht.tk_gv', 'Tk giá vốn'] },
                        { text: ['so4ht.tk_vt', 'Tk kho'] },
                        { text: ['so4ht.tk_ck', 'Tk chiết khấu'] },
                    ]}
                />
            </Table.Header>
            <Table.Body>

                {
                    ct && ct.map((item, index) => {
                        const error = errorCT ? errorCT.find(x => x.index === index) : undefined
                        return <Table.Row key={item.stt_rec0 ? item.stt_rec0 : ('REC' + index)}>
                            <TextCell name="ten_vt"
                                rowItem={item} index={index}
                                placeholder={['so3.ten_vt', 'Tên vật tư']}
                                readOnly
                            />
                            {ph.ma_nt !== "VND" && <NumberCell name="tien_nt2"
                                rowItem={item} index={index} decimalScale={2}
                                placeholder={["so4ht.tien_nt2", "Tiền hàng", { ma_nt: ph.ma_nt }]}
                                readOnly
                            />}
                            {ph.ma_nt !== "VND" && <NumberCell name="tien_ck_nt"
                                rowItem={item} index={index} decimalScale={2}
                                placeholder={["so4ht.tien_ck", "Giảm giá", { ma_nt: ph.ma_nt }]}
                                readOnly
                            />}
                            {ph.ma_nt !== "VND" && <NumberCell name="thue_gtgt_nt"
                                rowItem={item} index={index} decimalScale={2}
                                placeholder={["so4ht.thue_gtgt", "VAT", { ma_nt: ph.ma_nt }]}
                                readOnly
                            />}
                            {ph.ma_nt !== "VND" && <NumberCell name="tien_nt"
                                rowItem={item} index={index} decimalScale={2}
                                placeholder={["so4ht.tien_nt", "Tiền vốn", { ma_nt: ph.ma_nt }]}
                                readOnly
                            />}

                            <NumberCell name="tien2"
                                rowItem={item} index={index}
                                placeholder={["so4ht.tien2", "Tiền hàng", { ma_nt: 'VND' }]}
                                readOnly
                            />
                            <NumberCell name="tien_ck"
                                rowItem={item} index={index}
                                placeholder={["so4ht.tien_ck", "Giảm giá", { ma_nt: 'VND' }]}
                                readOnly
                            />
                            <NumberCell name="thue_gtgt"
                                rowItem={item} index={index}
                                placeholder={["so4ht.thue_gtgt", "VAT", { ma_nt: 'VND' }]}
                                readOnly
                            />
                            <NumberCell name="tien"
                                rowItem={item} index={index}
                                placeholder={["so4ht.tien", "VAT", { ma_nt: 'VND' }]}
                                readOnly
                            />

                            <SelectCell lookup={{
                                ...ZenLookup.TK, where: 'chi_tiet = 1',
                                onLocalWhere: (items) => {
                                    return items.filter(t => t.chi_tiet == true)
                                },
                                formatInput: "{tk_tl}",
                            }}
                                error={error}
                                name="tk_tl" rowItem={item} index={index}
                                placeholder={['so4ht.tk_tl', 'Tk hbtl']}
                                readOnly={isReadOnly}
                                onChange={switchAction}
                            />
                            <SelectCell lookup={{
                                ...ZenLookup.TK, where: 'chi_tiet = 1',
                                onLocalWhere: (items) => {
                                    return items.filter(t => t.chi_tiet == true)
                                },
                                formatInput: "{tk_gv}",
                            }}
                                error={error}
                                name="tk_gv" rowItem={item} index={index}
                                placeholder={['so4ht.tk_gv', 'Tk gv']}
                                readOnly={isReadOnly}
                                onChange={switchAction}
                            />
                            <SelectCell lookup={{
                                ...ZenLookup.TK, where: 'chi_tiet = 1',
                                onLocalWhere: (items) => {
                                    return items.filter(t => t.chi_tiet == true)
                                },
                                formatInput: "{tk_vt}",
                            }}
                                error={error}
                                name="tk_vt" rowItem={item} index={index}
                                placeholder={['so4ht.tk_vt', 'Tk kho']}
                                readOnly={isReadOnly}
                                onChange={switchAction}
                            />
                            <SelectCell lookup={{
                                ...ZenLookup.TK, where: 'chi_tiet = 1',
                                onLocalWhere: (items) => {
                                    return items.filter(t => t.chi_tiet == true)
                                },
                                formatInput: "{tk_ck}",
                            }}
                                error={error}
                                name="tk_ck" rowItem={item} index={index}
                                placeholder={['so4ht.tk_ck', 'Tk ck']}
                                readOnly={isReadOnly}
                                onChange={switchAction}
                            />
                        </Table.Row>
                    })
                }
            </Table.Body>
        </TableScroll>

        <Form style={{ paddingTop: "14px" }}>
            <Form.Group>
                <ZenFieldSelectApi loadApi width="3"
                    lookup={{
                        ...ZenLookup.TK,
                        format: "{tk}",
                        where: "chi_tiet = 1",
                        onLocalWhere: (items) => {
                            return items.filter(t => t.chi_tiet == true)
                        }
                    }}
                    readOnly={true}
                    label={"so3.tk_pt"} defaultlabel="Tài khoản phải thu"
                    name="tk_pt" value={ph.tk_pt}
                    onChange={changeTK}
                />
                <ZenFieldSelectApi loadApi width="3"
                    lookup={{
                        ...ZenLookup.TK,
                        format: "{tk}",
                        where: "chi_tiet = 1",
                        onLocalWhere: (items) => {
                            return items.filter(t => t.chi_tiet == true)
                        }
                    }}
                    readOnly={true}
                    label={"so3.tk_thue"} defaultlabel="Tài khoản thuế"
                    name="tk_thue" value={ph.tk_thue}
                    onChange={changeTK}
                />
            </Form.Group>
        </Form>
    </>
}

const onGetCT4Tain = (ph, ct, cp, onUpdatePHCT) => {
    const tain = []
    // ============= get tain từ ct
    let arrThue = [0]
    ct.forEach(item => {
        if (item.ts_gtgt > 0 && !arrThue.includes(item.ts_gtgt)) {
            arrThue.push(item.ts_gtgt)
        }
    })
    arrThue.forEach(ts => {
        let tempCT = ct.filter(item => VoucherHelper.f_NullToZero(item.ts_gtgt) == ts)
        if (tempCT.length > 0) {
            const newTain = {
                mau_bc: "1", ten_vt: ph.dien_giai,
                ma_kh: ph.ma_kh, ten_kh: ph.ten_kh,
                dia_chi: ph.dia_chi, ma_so_thue: ph.ma_so_thue,

                ngay_ct0: ph.ngay_hd, so_ct0: ph.so_hd,
                so_seri0: ph.so_seri,

                thue_suat: ts,
                tk_du: ph.tk_pt,
                tk_thue_no: ph.tk_thue,
                tien_hang: VoucherHelper.f_SumArray(tempCT, 'tien2'),
                tien_hang_nt: VoucherHelper.f_SumArray(tempCT, 'tien_nt2'),
                t_thue: VoucherHelper.f_SumArray(tempCT, 'thue_gtgt'),
                t_thue_nt: VoucherHelper.f_SumArray(tempCT, 'thue_gtgt_nt')
            }
            tain.push(newTain)
        }
    })

    // tính PH
    const tain_t_thue = VoucherHelper.f_SumArray(tain, 't_thue')
    const tain_t_thue_nt = VoucherHelper.f_SumArray(tain, 't_thue_nt')
    const totalPH = {
        t_thue: tain_t_thue,
        t_thue_nt: tain_t_thue_nt,
        t_tt: VoucherHelper.f_SumArray(ct, 'tien2') - VoucherHelper.f_SumArray(ct, 'tien_ck') + tain_t_thue,
        t_tt_nt: VoucherHelper.f_SumArray(ct, 'tien_nt2') - VoucherHelper.f_SumArray(ct, 'tien_ck_nt') + tain_t_thue_nt,
    }
    // ct.tien = ct.tien0 + ct.cp
    onUpdatePHCT(totalPH, null, { tain: tain })
}

const CustomHeader = () => {
    const { modeForm, permission, onUpdatePHCT } = useContext(ContextVoucher)
    const isReadOnly = VoucherHelper.f_ReadOnly(modeForm, permission)

    const handleSelect = (items) => {
        // items: [{...ph, cts:[]}, ...]
        let newCT = [];
        for (let index = 0; index < items.length; index++) {
            const rowPh = items[index];
            const tempCt = rowPh.cts.map(rowCt => {
                return {
                    stt_rec_hd: rowCt.stt_rec,
                    stt_rec0_hd: rowCt.stt_rec0,
                    stt_rec_px: rowCt.stt_rec_px,
                    stt_rec0_px: rowCt.stt_rec0_px,
                    so_hd: rowPh.so_ct,
                    so_px: rowPh.so_px,
                    stt_rec_dh: rowCt.stt_rec_dh,
                    stt_rec0_dh: rowCt.stt_rec0_dh,
                    so_dh: rowCt.so_dh,
                    ma_vt: rowCt.ma_vt,
                    ten_vt: rowCt.ten_vt,
                    dvt: rowCt.dvt,
                    ma_thue: rowCt.ma_thue,
                    ma_kho: rowCt.ma_kho,
                    ma_vitri: rowCt.ma_vitri,
                    ma_lo: rowCt.ma_lo,
                    ma_phi: rowCt.ma_phi,
                    ma_hd: rowCt.ma_hd,
                    ma_spct: rowCt.ma_spct,
                    ma_bp: rowCt.ma_bp,
                    ma_nvkd: rowCt.ma_nvkd,

                    tk_vt: rowCt.tk_vt,
                    tk_gv: rowCt.tk_gv,
                    tk_tl: rowCt.tk_tl,
                    tk_ck: rowCt.tk_ck,
                    tk_km: rowCt.tk_km,
                    tk_kho_theo: rowCt.tk_kho_theo,

                    so_luong: rowCt.ton,
                    so_luong_qd: rowCt.ton_qd,
                    gia_nt2: rowCt.gia_nt2,
                    gia2: rowCt.gia2,
                    tien2: rowCt.tien2,
                    tien_nt2: rowCt.tien_nt2,
                    tl_ck: rowCt.tl_ck,
                    tien_ck_nt: rowCt.tien_ck_nt,
                    tien_ck: rowCt.tien_ck,
                    ts_gtgt: rowCt.ts_gtgt,
                    thue_gtgt_nt: rowCt.thue_gtgt_nt,
                    thue_gtgt: rowCt.thue_gtgt,
                    gia_nt: rowCt.gia_Nt,
                    gia: rowCt.gia,
                    tien_nt: rowCt.tien_nt,
                    tien: rowCt.tien,
                }
            });
            newCT = newCT.concat([...tempCt])
        }

        // tính ph
        const totalPH = {
            t_so_luong: VoucherHelper.f_SumArray(newCT, "so_luong"),
            t_tien2: VoucherHelper.f_SumArray(newCT, "tien2"),
            t_tien_nt2: VoucherHelper.f_SumArray(newCT, "tien_nt2"),
            t_thue: VoucherHelper.f_SumArray(newCT, "thue_gtgt"),
            t_thue_nt: VoucherHelper.f_SumArray(newCT, "thue_gtgt_nt"),
            t_ck: VoucherHelper.f_SumArray(newCT, "tien_ck"),
            t_ck_nt: VoucherHelper.f_SumArray(newCT, "tien_ck_nt"),
        }
        totalPH.t_tt = totalPH.t_tien2 - totalPH.t_ck + totalPH.t_thue;
        totalPH.t_tt_nt = totalPH.t_tien_nt2 - totalPH.t_ck_nt + totalPH.t_thue_nt;
        onUpdatePHCT(totalPH, newCT)
    }

    return <div style={{ float: "right" }}>
        <SOVchSO4Lookup onSelected={handleSelect}
            readOnly={isReadOnly} />
    </div>
}

export const SOVchSO4Edit = {
    PHForm: PHForm,
    CTForm: CTForm,
    CTFormTotal: CTFormTotal,
    onValidCT: onValidCT,
    CustomHeader: CustomHeader,
    // Bảng: tain - Thuế
    tainInfo: {
        visible: true,      // ẩn/hiện tab thuế
        hasThueNK: false,   // thêm cột thuế nhập khẩu
        hiddenCols: [],     // ẩn cột trên table: ['col1','col2']
        nameThue: "t_thue", // trường tổng tiền trên PH
        nameTien: "t_tt",   // trường tổng thuế trên PH
        //nameThue_Ttdb: 'mau_hd', // trường thuế ttdb(nếu có): ẩn cột khi ko khai báo
        onGetCT: onGetCT4Tain,   // handle get tain từ ct
    },
    // thêm tab mới
    addTab: [
        {
            visible: true,
            name: "HachToan",
            title: "Hạch toán",
            component: TabHachToan
        },
    ],

    ma_ct: 'SO4',
    formId: "SOVchSO4",
    route: {
        add: routes.SOVchSO4New,
        edit: routes.SOVchSO4Edit()
    },
    linkHeader: {
        id: "sovchso4",
        defaultMessage: "Nhập hàng bán trả lại",
        route: routes.SOVchSO4,
        active: false,
    },
    formId: "SOVchSO4",
    api: {
        url: ApiSOVchSO4,
    },
    action: {
        view: { visible: true, permission: "06.02.1" },
        add: { visible: true, permission: "06.02.2" },
        edit: { visible: true, permission: "06.02.3" },
        del: { visible: true, permission: "06.02.4" }
    },
    initItem: {
        stt_rec: "", so_ct: "", ma_ct: "SO4", ma_nt: "VND", ty_gia: 1,
        ngay_ct: ZenHelper.formatDateTime(new Date(), "YYYY-MM-DD"),
        ngay_lct: ZenHelper.formatDateTime(new Date(), "YYYY-MM-DD"),
        so_hd: "", ngay_hd: "", ma_httt: "",
        ma_kh: "", dia_chi_vat: "", dien_giai: "", nguoi_gd: "",
        trang_thai: "", t_tien: 0, t_tien_nt: 0, t_so_luong: 0,
    },
    formValidation: [
        {
            id: "ngay_ct",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ]
        },
        {
            id: "so_ct",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ],
        },
        {
            id: "ma_kh",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ]
        },
        {
            id: "ma_httt",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ]
        },
        {
            id: "so_hd",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ]
        },
        {
            id: "ngay_hd",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ]
        },
    ]
}
