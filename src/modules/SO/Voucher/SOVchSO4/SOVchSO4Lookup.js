import React, { useState } from "react";
import { Button, Form, Icon, Label } from "semantic-ui-react";
import { ZenField, ZenFieldSelectApi } from "../../../../components/Control";
import { ZenLookup } from "../../../ComponentInfo/Dictionary/ZenLookup";
import { ZenVoucherLookup } from "../../../../components/Control/zVoucher";

const spLookup = "asSoLookupHdPn";

const SOVchSO4Lookup = ({ readOnly, onSelected }) => {
    const [open, setOpen] = useState(false)
    const handleOpen = () => setOpen(!open)

    const handleCondition = (item) => {
        let _sql = "";

        if (item.so_ct1) _sql += ` AND p.so_ct >= '${item.so_ct1}'`
        if (item.so_ct2) _sql += ` AND p.so_ct <= '${item.so_ct2}'`
        if (item.ngay_ct1) _sql += ` AND p.ngay_ct >= '${item.ngay_ct1}'`
        if (item.ngay_ct2) _sql += ` AND p.ngay_ct <= '${item.ngay_ct2}'`
        if (item.ma_kh) _sql += ` AND p.ma_kh = '${item.ma_kh}'`

        return _sql
    }

    const handleSelected = (item) => {
        onSelected(item)
        setOpen(false)
    }

    return <>
        <Button content={"Chọn hóa đơn trả lại"}
            size="tiny" icon="plus"
            primary basic
            disabled={readOnly}
            onClick={handleOpen}
        />

        <ZenVoucherLookup open={open} title={"Chọn hóa đơn trả lại"}
            stored={spLookup}
            onCondition={handleCondition}
            onClose={handleOpen}
            onSelected={handleSelected}
            FilterFormPH={FilterFormPH}
            // FilterFormCT={FilterFormCT}

            initFilter={[
                { name: "ma_kh", text: "Khách hàng" },
            ]}

            columnsPH={[
                { name: "ngay_ct", text: "Ngày ct", type: "date", },
                { name: "so_ct", text: "Số ct", type: "string" },
                { name: "ma_kh", text: "Mã NCC", type: "string", },
                { name: "ten_kh", text: "Tên NCC", type: "string" },
                { name: "nguoi_gd", text: "Người giao dịch", type: "string", },
                { name: "ma_hd", text: "Mã hợp đồng", type: "string", },
                { name: "ma_nt", text: "Mã ngoại tệ", type: "string" },
                { name: "ty_gia", text: "Tỷ giá", type: "number" },
            ]}
            columnsCT={[
                { name: "ma_vt", text: "Mã vt", type: "string", },
                { name: "ten_vt", text: "Tên vt", type: "string" },
                { name: "dvt", text: "Đvt", type: "string", },
                { name: "ma_kho", text: "Mã kho", type: "string" },

                { name: "so_luong", text: "Số lượng", type: "number" },
                { name: "sl_xuat", text: "Đã nhập", type: "number" },
                { name: "sl_ton", text: "Còn tồn", type: "number" },

                { name: "ma_hd", text: "Hợp đồng", type: "string" },
                { name: "ma_spct", text: "SPCT", type: "string", },
                { name: "ma_phi", text: "Phí", type: "string" },
                { name: "ma_bp", text: "Bộ phận", type: "string" },
            ]}
        />
    </>
}

const FilterFormPH = ({ formik }) => {
    return <>
        <ZenFieldSelectApi lookup={ZenLookup.ma_kh} loadApi
            label={"po4.ma_kh"} defaultlabel="Khách hàng"
            name="ma_kh" formik={formik}
        />
    </>
}

// const FilterFormCT = ({ formik }) => {
//     return <>

//     </>
// }

export { SOVchSO4Lookup }