const prefixSOVchSO1 = 'so1.'

export const Mess_SOVchSO1 = {
    Headers: {
        id: prefixSOVchSO1 + 'header',
        defaultMessage: 'Đơn đặt hàng bán',
        en: "",
    },
    Ngay_ct: {
        id: prefixSOVchSO1 + 'ngay_ct',
        defaultMessage: 'Ngày CTừ',
        en: "",
    },
    So_ct: {
        id: prefixSOVchSO1 + 'so_ct',
        defaultMessage: 'Số CTừ',
        en: "",
    },
    Ma: {
        id: prefixSOVchSO1 + 'ma_kh',
        defaultMessage: 'Mã KH',
        en: "",
    },
    Ten: {
        id: prefixSOVchSO1 + 'Ten_kh',
        defaultMessage: 'Tên KH',
        en: "",
    },
    Nguoi_mua: {
        id: prefixSOVchSO1 + 'nguoi_gd',
        defaultMessage: 'Người mua',
        en: "",
    },
    Dien_giai: {
        id: prefixSOVchSO1 + 'dien_giai',
        defaultMessage: 'Diễn giải',
        en: "",
    },
    Dia_chi: {
        id: prefixSOVchSO1 + 'dia_chi',
        defaultMessage: 'Địa chỉ',
        en: "",
    },
    Tien_hang: {
        id: prefixSOVchSO1 + 't_tien_nt2',
        defaultMessage: 'Tiền hàng',
        en: "",
    },
    Tien_thue: {
        id: prefixSOVchSO1 + 't_thue_nt',
        defaultMessage: 'Tiền thuế',
        en: "",
    },
    Tong_tt: {
        id: prefixSOVchSO1 + 't_tt_nt',
        defaultMessage: 'Tổng thanh toán',
        en: "",
    },
    Ma_vt: {
        id: prefixSOVchSO1 + 'ma_vt',
        defaultMessage: 'Mã Hàng',
        en: "",
    },
    Ten_vt: {
        id: prefixSOVchSO1 + 'ten_vt',
        defaultMessage: 'Tên hàng',
        en: "",
    },
    Ma_kho: {
        id: prefixSOVchSO1 + 'ma_kho',
        defaultMessage: 'Kho',
        en: "",
    },
    Soluong: {
        id: prefixSOVchSO1 + 'so_luong',
        defaultMessage: 'Số lượng',
        en: "",
    },
    Dvt: {
        id: prefixSOVchSO1 + 'dvt',
        defaultMessage: 'Đvt',
        en: "",
    },
    
}