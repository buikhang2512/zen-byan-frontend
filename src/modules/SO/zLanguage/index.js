import { IntlFormat,LanguageFormat } from '../../../utils/intlFormat';
import { Mess_SOVchSO1 } from './Lang_SOVchSO1';

const SO1_VI = {...IntlFormat.setMessageLanguage(Mess_SOVchSO1)}

const SO1_EN = {...IntlFormat.setMessageLanguage(Mess_SOVchSO1,LanguageFormat.en)}

export { default as SO_VI } from './vi-vn.json';
export { default as SO_EN } from './en-us.json';

export { SO1_VI }
export { SO1_EN }


