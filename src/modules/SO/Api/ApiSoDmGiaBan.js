import axios from '../../../Api/axios';

const ExtName = "SoDmGiaBan"

export const ApiSoDmGiaBan = {
   get(callback, filter = {}, pagination = {}) {
      axios.get(`${ExtName}`, {
         params: {
            qf: filter.qf,
            keyword: filter.keyword,
            sort: filter.sort,
            page: pagination.page,
            pageSize: pagination.pageSize
         },
      })
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   getByCode(code, callback, params) {
      axios.get(`${ExtName}/single`, {'params' : {...params}})
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   insert(data, callback) {
      axios.post(`${ExtName}`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   update(data, callback) {
      axios.put(`${ExtName}`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   updatePatch(params, data, callback) {
      axios.patch(`${ExtName}`, {
         ...params,
         "patchModel": data
      })
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   delete(code, callback, params) {
      axios.delete(`${ExtName}`, {data:{ ...params}})
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   getPrice(data,callback) {
      axios.post(`${ExtName}/getgiaban`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   }
}