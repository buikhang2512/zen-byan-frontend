import React from "react";
import { ZenField, ZenFieldNumber } from "../../../components/Control/index";
import { FormMode } from "../../../utils";
import { Form } from "semantic-ui-react";
import { ApiSODmVNKD } from "../Api";
const SODmNVKDModal = (props) => {
   const { formik, permission, mode } = props

   return <React.Fragment>
      <ZenField required autoFocus readOnly={!permission || (mode === FormMode.ADD ? false : true)}
         label={"SODmNVKD.ma_nvkd"} defaultlabel="Mã nhân viên kinh doanh"
         name="ma_nvkd" props={formik} textCase="uppercase"
         textCase="uppercase" maxLength={8}
         isCode
      />
      <ZenField required readOnly={!permission}
         label={"SODmNVKD.ten_nvkd"} defaultlabel="Tên nhân viên kinh doanh"
         name="ten_nvkd" props={formik}
      />
   </React.Fragment>
}

const SODmNVKD = {
   FormModal: SODmNVKDModal,
   api: {
      url: ApiSODmVNKD,
   },
   permission: {
      view: "06.26.1",
      add: "06.26.2",
      edit: "06.26.3"
   },
   formId: "SODmNVKD-form",
   size: "tiny",
   header: "Nhân viên kinh doanh",
   initItem: {
      ma_nvkd: "",
      ten_nvkd: "",
      ksd: false
   },
   formValidation: [
      {
         id: "ma_nvkd",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
            {
               type: "max",
               params: [8, "Không được vượt quá 8 kí tự"]
            },
         ]
      },
      {
         id: "ten_nvkd",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
         ]
      },
   ]
}

export { SODmNVKD };