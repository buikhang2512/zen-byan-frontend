import React from "react";
import {
  ZenField,
  ZenFieldNumber,
  ZenFieldDate,
  ZenFieldSelectApi,
} from "../../../components/Control/index";
import { FormMode } from "../../../utils";
import { Form } from "semantic-ui-react";
import { ApiSoDmGiaBan } from "../Api";
import { date } from "yup";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
const SODmGiaBanModal = (props) => {
  const { formik, permission, mode } = props;

  const handleChangeVT = (e,i) => {
    formik.setFieldValue('ma_vt',e.ma_vt)
    formik.setFieldValue('ten_vt',e.ten_vt)
    formik.setFieldValue('dvt',e.dvt)
  }

  return (
    <React.Fragment>
      <ZenFieldSelectApi
        required
        readOnly={!permission || (mode === FormMode.ADD ? false : true)}
        loadApi
        lookup={ZenLookup.Ma_vt}
        label={"SODmGiaBan.ma_vt"}
        defaultlabel="Mã vật tư"
        name="ma_vt"
        formik={formik}
        onItemSelected= {handleChangeVT}
      />
      <Form.Group>
        <ZenFieldDate
          width="6"
          readOnly={!permission || (mode === FormMode.ADD ? false : true)}
          label="SODmGiaBan.ngay_ad"
          defaultlabel="Ngày áp dụng"
          name="ngay_ad"
          props={formik}
        />
      </Form.Group>
      <Form.Group>
        <ZenFieldSelectApi
          width="6"
          readOnly={!permission}
          lookup={ZenLookup.Ma_nt}
          label={"SODmGiaBan.ma_nt"}
          defaultlabel="Mã NT mới"
          name="ma_nt"
          formik={formik}
        />
        <ZenFieldNumber
          readOnly={!permission}
          maxLength={19}
          label={"SODmGiaBan.gia"}
          defaultlabel="Giá"
          name="gia"
          props={formik}
          decimalScale={0}
          width="10"
        />
      </Form.Group>
    </React.Fragment>
  );
};

const SODmGiaBan = {
  FormModal: SODmGiaBanModal,
  api: {
    url: ApiSoDmGiaBan,
    //params: ["ma_vt", "ngay_ad", "ma_kh", "ma_nhkh", "ma_nhvt"],
  },
  permission: {
    view: "06.25.1",
    add: "06.25.2",
    edit: "06.25.3",
    //hiddenKsd: ["edit"],
  },
  formId: "SODmGiaBan-form",
  size: "tiny",
  isScrolling: false,
  header: "Danh mục giá bán",
  initItem: {
    ma_vt: "",
    ngay_ad: new Date(),
    ma_nt: "VND",
    gia: 0,
    ksd: false,
  },
  formValidation: [
    {
      id: "ma_vt",
      validationType: "string",
      validations: [
        {
          type: "required",
          params: ["Không được bỏ trống trường này"],
        },
      ],
    },
  ],
};

export { SODmGiaBan };
