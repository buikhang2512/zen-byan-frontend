import * as routes from "../../../constants/routes";
import { ApiSODmTs, ApiSODmVNKD, ApiSoDmGiaBan, ApiSODmMhd } from "../Api/index";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import * as permissions from "../../../constants/permissions"

const SO_Dictionary = {
  SODmTs: {
    route: routes.SODmTs,

    action: {
      view: { visible: true, permission: permissions.SODmTsXem },
      add: { visible: true, permission: permissions.SODmTsThem },
      edit: { visible: true, permission: permissions.SODmTsSua },
      del: { visible: true, permission: permissions.SODmTsXoa },
    },

    linkHeader: {
      id: "sodmts",
      defaultMessage: "Danh mục thuế suất",
      active: true,
      isSetting: true,
    },

    tableList: {
      keyForm: "SODmTs",
      formModal: ZenLookup.Ma_thue,
      fieldCode: "ma_thue",
      unPagination: false,
      showFilterLabel: true,

      api: {
        url: ApiSODmTs,
        type: "sql",
      },
      columns: [
        {
          id: "sodmts.ma_thue",
          defaultMessage: "Mã thuế suất",
          fieldName: "ma_thue",
          type: "string",
          filter: "string",
          sorter: true,
          editForm: true,
        },
        {
          id: "sodmts.ten_thue",
          defaultMessage: "Tên thuế suất",
          fieldName: "ten_thue",
          type: "string",
          filter: "string",
          sorter: true,
        },
        {
          id: "sodmts.ts_gtgt",
          defaultMessage: "Thuế suất GTGT",
          fieldName: "ts_gtgt",
          type: "number",
          filter: "number",
          sorter: true,
          suffix: "%",
          showZero: true,
        },
        {
          id: "sodmts.tk_thue_co",
          defaultMessage: "TK thuế đầu ra",
          fieldName: "tk_thue_co",
          type: "string",
          filter: "string",
          sorter: true,
        },
        {
          id: "ksd",
          defaultMessage: "Không sử dụng",
          fieldName: "ksd",
          type: "bool",
          filter: "",
          sorter: true,
          collapsing: true,
          propsCell: { textAlign: "center" },
        },
      ],
    },
  },
  // SoDmNVKD: {
  //   route: routes.SoDmNVKD,
  //   importExcel: true,
  //   action: {
  //     view: { visible: true, permission: "06.26.1" },
  //     add: { visible: true, permission: "06.26.2" },
  //     edit: { visible: true, permission: "06.26.3" },
  //     del: { visible: true, permission: "06.26.4" },
  //   },

  //   linkHeader: {
  //     id: "SoDmNVKD",
  //     defaultMessage: "Danh mục nhân viên kinh doanh",
  //     active: true,
  //     isSetting: false,
  //   },

  //   tableList: {
  //     keyForm: "SODmNvkd",
  //     formModal: ZenLookup.Ma_nvkd,
  //     fieldCode: "ma_nvkd",
  //     unPagination: false,
  //     duplicate: true,

  //     api: {
  //       url: ApiSODmVNKD,
  //       type: "sql",
  //     },
  //     columns: [
  //       {
  //         id: "SODmNVKD.ma_nvkd",
  //         defaultMessage: "Mã NVKD",
  //         fieldName: "ma_nvkd",
  //         type: "string",
  //         filter: "string",
  //         sorter: true,
  //         editForm: true,
  //       },
  //       {
  //         id: "SODmNVKD.ten_nvkd",
  //         defaultMessage: "Tên nhân viên kinh doanh",
  //         fieldName: "ten_nvkd",
  //         type: "string",
  //         filter: "string",
  //         sorter: true,
  //         editForm: true,
  //       },
  //       {
  //         id: "ksd",
  //         defaultMessage: "Không sử dụng",
  //         fieldName: "ksd",
  //         type: "bool",
  //         filter: "",
  //         sorter: true,
  //         collapsing: true,
  //         ropsCell: { textAlign: "center" },
  //       },
  //     ],
  //   },
  // },
  // SoDmGiaBan: {
  //   route: routes.SoDmGiaBan,
  //   importExcel: true,
  //   action: {
  //     view: { visible: true, permission: "06.25.1" },
  //     add: { visible: true, permission: "06.25.2" },
  //     edit: { visible: true, permission: "06.25.3" },
  //     del: { visible: true, permission: "06.25.4" },
  //   },

  //   linkHeader: {
  //     id: "SoDmGiaBan",
  //     defaultMessage: "Danh mục giá bán",
  //     active: true,
  //     isSetting: false,
  //   },

  //   tableList: {
  //     keyForm: "SODmGiaBan",
  //     formModal: ZenLookup.Ma_gia_ban,
  //     fieldCode: "ma_vt",
  //     unPagination: false,
  //     changeCode: false,
  //     api: {
  //       url: ApiSoDmGiaBan,
  //       type: "sql",
  //       params: ["ma_vt", "ngay_ad", "ma_kh", "ma_nhkh", "ma_nhvt"],
  //     },
  //     columns: [
  //       {
  //         id: "SODmGiaBan.ngay_ad",
  //         defaultMessage: "Ngày áp dụng",
  //         fieldName: "ngay_ad",
  //         type: "date",
  //         filter: "date",
  //         sorter: true,
  //         editForm: true,
  //       },
  //       {
  //         id: "SODmGiaBan.ma_vt",
  //         defaultMessage: "Mã hàng",
  //         fieldName: "ma_vt",
  //         type: "string",
  //         filter: "string",
  //         sorter: true,
  //         editForm: true,
  //       },
  //       {
  //         id: "SODmGiaBan.ten_vt",
  //         defaultMessage: "Tên vật tư",
  //         fieldName: "ten_vt",
  //         type: "string",
  //         filter: "string",
  //         sorter: true,
  //       },
  //       {
  //         id: "SODmGiaBan.dvt",
  //         defaultMessage: "Đơn vị tính",
  //         fieldName: "dvt",
  //         type: "string",
  //         filter: "string",
  //         sorter: true,
  //       },
  //       {
  //         id: "SODmGiaBan.gia",
  //         defaultMessage: "Giá bán",
  //         fieldName: "gia",
  //         type: "number",
  //         filter: "number",
  //         sorter: true,
  //       },
  //       {
  //         id: "SODmGiaBan.ma_nt",
  //         defaultMessage: "Mã ngoại tệ",
  //         fieldName: "ma_nt",
  //         type: "string",
  //         filter: "string",
  //         sorter: true,
  //       },
  //     ],
  //   },
  // },
  // SoDmMhd: {
  //   route: routes.SoDmMhd,

  //   action: {
  //     view: { visible: true, permission: "" },
  //     add: { visible: true, permission: "" },
  //     edit: { visible: true, permission: "" },
  //     del: { visible: true, permission: "", },
  //   },

  //   linkHeader: {
  //     id: "SODMMHD",
  //     defaultMessage: "Danh mục mẫu hóa đơn",
  //     active: true,
  //     isSetting: false,
  //   },

  //   tableList: {
  //     keyForm: "SODMMHD",
  //     formModal: ZenLookup.SoDmMHD,
  //     fieldCode: "so_seri_mhd",
  //     unPagination: false,
  //     multiKey: ["so_seri_mhd", "so_seri"],
  //     api: {
  //       url: ApiSODmMhd,
  //       type: "sql",
  //       params: ["so_seri_mhd", "so_seri"],
  //     },
  //     columns: [
  //       { id: "SoDmMhd.so_seri_mhd", defaultMessage: "Mẫu hóa đơn", fieldName: "so_seri_mhd", type: "string", filter: "string", sorter: true, editForm: true, },
  //       { id: "SoDmMhd.so_seri_mhd", defaultMessage: "Số seri", fieldName: "so_seri", type: "string", filter: "string", sorter: true, },
  //       { id: "SoDmMhd.so_seri_mhd", defaultMessage: "Số lượng đăng ký", fieldName: "so_luong", type: "string", filter: "string", sorter: true, },
  //       { id: "SoDmMhd.so_seri_mhd", defaultMessage: "Ngày đăng ký", fieldName: "ngay_dk", type: "date", filter: "date", sorter: true, },
  //     ]
  //   }
  // }
};

export default SO_Dictionary;
