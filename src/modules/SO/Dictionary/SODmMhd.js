import React from "react";
import { ZenField, ZenFieldDate, ZenFieldNumber } from "../../../components/Control";
import { ApiSODmMhd } from "../Api";

const SODmMhdModal = (props) => {
    const { formik, permission, mode } = props;
    return (
        <>
            <ZenField
                label="SODmMhd.so_seri_mhd"
                defaultlabel="Ký hiệu mẫu hóa đơn"
                name="so_seri_mhd"
                props={formik}
                isCode
            />
            <ZenField
                label="SODmMhd.so_seri"
                defaultlabel="Ký hiệu hóa đơn"
                name="so_seri"
                props={formik}
            />
            <ZenFieldNumber
                label="SODmMhd.so_luong"
                defaultlabel="Số lượng phát hành"
                name="so_luong"
                props={formik}
            />
            <ZenFieldDate
                label="SODmMhd.ngay_dk"
                defaultlabel="Ngày bắt đầu"
                name="ngay_dk"
                props={formik}
            />
            <ZenField
                label="SODmMhd.zis_id"
                defaultlabel="Mã mẫu HĐ điện tử"
                name="zis_id"
                props={formik}
            />
        </>
    )
}

const SODmMhd = {
    FormModal: SODmMhdModal,
    api: {
        url: ApiSODmMhd,
        param: ['so_seri_mhd', 'so_seri']
    },
    permission: {
        view: "",
        add: "",
        edit: "",
    },
    formId: "SODmMhd-form",
    size: "tiny",
    isScrolling: false,
    header: "Danh mục mẫu hóa đơn",
    initItem: {
        so_seri_mhd: "",
        so_seri: "",
        so_luong: 0,
        ngay_dk: new Date(),
        so_tkngh: "",
        ten_ngh: "",
        ksd: false,
        zis_id: ""
    },
    formValidation: [
        {
            id: "so_seri_mhd",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"],
                },
            ],
        },
        {
            id: "so_seri",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"],
                },
            ],
        },
        {
            id: "so_luong",
            validationType: "number",
            validations: [
                {
                    type: "min",
                    params: [1, "Không được nhỏ hơn 0"],
                },
            ],
        },
        {
            id: "ngay_dk",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"],
                },
            ],
        },
    ],
}
export { SODmMhd };