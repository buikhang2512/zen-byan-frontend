import React from "react";
import {
  ZenField,
  ZenFieldNumber,
  ZenFieldSelectApi,
} from "../../../components/Control/index";
import { FormMode } from "../../../utils";
import { Form } from "semantic-ui-react";
import { ApiSODmTs } from "../Api";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import * as permissions from "../../../constants/permissions"

const SODmTsModal = (props) => {
  const { formik, permission, mode } = props;

  return (
    <React.Fragment>
      <ZenField
        required
        autoFocus
        readOnly={!permission || (mode === FormMode.ADD ? false : true)}
        label={"sodmts.ma_thue"}
        defaultlabel="Mã thuế"
        name="ma_thue"
        props={formik}
        textCase="uppercase"
        isCode
      />
      <ZenField
        required
        readOnly={!permission}
        label={"sodmts.ten_thue"}
        defaultlabel="Tên thuế"
        name="ten_thue"
        props={formik}
      />

      <Form.Group>
        <ZenFieldNumber
          required
          readOnly={!permission}
          label={"sodmts.ts_gtgt"}
          defaultlabel="Thuế suất"
          name="ts_gtgt"
          props={formik}
          decimalScale={2}
          width="4"
          showZero
          suffix=" %"
        />

        <ZenFieldSelectApi
          width="12"
          loadApi
          readOnly={!permission}
          lookup={{ ...ZenLookup.TK, where: "chi_tiet= 1" }}
          label={"sodmts.tk_thue_co"}
          defaultlabel="TK thuế đầu ra"
          name="tk_thue_co"
          formik={formik}
        />
      </Form.Group>
    </React.Fragment>
  );
};

const SODmTs = {
  FormModal: SODmTsModal,
  api: {
    url: ApiSODmTs,
  },
  permission: {
    view: permissions.SODmTsXem,
    add: permissions.SODmTsThem,
    edit: permissions.SODmTsSua,
  },
  formId: "sodmts-form",
  size: "tiny",
  initItem: {
    ma_thue: "",
    ten_thue: "",
    ts_gtgt: 0,
    tk_thue_co: "",
    ksd: false,
  },
  isScrolling: false,
  formValidation: [
    {
      id: "ma_thue",
      validationType: "string",
      validations: [
        {
          type: "required",
          params: ["Không được bỏ trống trường này"],
        },
        {
          type: "max",
          params: [2, "Không được vượt quá 2 kí tự"],
        },
      ],
    },
    {
      id: "ten_thue",
      validationType: "string",
      validations: [
        {
          type: "required",
          params: ["Không được bỏ trống trường này"],
        },
      ],
    },
    {
      id: "ts_gtgt",
      validationType: "number",
      validations: [
        {
          type: "required",
          params: ["Không được bỏ trống trường này"],
        },
      ],
    },
  ],
};

export { SODmTs };
