import { SORptBK01 } from "./SORptBK01"
import { SORptBK02 } from "./SORptBK02"
import { SORptTH01 } from "./SORptTH01"
import { SORptBCPT0401 } from "./SORptBCPT0401"
import { SORptBCPT0402 } from "./SORptBCPT0402"
import { SORptBCPT06 } from "./SORptBCPT06"
import { SORptLaiLo } from "./SORptLaiLo"

export const SO_Report = {
    SORptBK01: SORptBK01,
    SORptBK02: SORptBK02,
    SORptTH01: SORptTH01,
    SORptBCPT0401: SORptBCPT0401,
    SORptBCPT0402: SORptBCPT0402,
    SORptBCPT06: SORptBCPT06,
    SORptLaiLo: SORptLaiLo,
}
