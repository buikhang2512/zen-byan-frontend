import React from "react";
import { Table, Form } from "semantic-ui-react";
import { ZenFieldSelectApi, ZenDatePeriod, ZenField } from "../../../components/Control";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { RptHeader, RptTable, RptTableCell, RptTableRow } from "../../../components/Control/zReport";
import * as routes from "../../../constants/routes";
import { IntlFormat } from "../../../utils/intlFormat";
import { Mess_Report } from "../../ComponentInfo/zLanguage/variable";

const FilterForm = ({ formik }) => {
    const handleItemSelected = (item, { name }) => {
        if (name === "tk") {
            formik.setFieldValue("ten_tk", item.ten_tk);
        } else if (name === "tk_du") {
            formik.setFieldValue("ten_tk_du", item.ten_tk);
        } else if (name === "ma_kh") {
            formik.setFieldValue("ten_kh", item.ten_kh);
        } else if (name === "ma_nhkh") {
            formik.setFieldValue("ten_nhkh", item.ten_nhkh);
        } else if (name === "ma_hd") {
            formik.setFieldValue("ten_hd", item.ten_hd);
        } else if (name === "ma_nhhd") {
            formik.setFieldValue("ten_nhhd", item.ten_nhhd);
        } else if (name === "ma_vt") {
            formik.setFieldValue("ten_vt", item.ten_vt);
        } else if (name === "ma_nhvt") {
            formik.setFieldValue("ten_nhvt", item.ten_nhvt);
        } else if (name === "ma_bp") {
            formik.setFieldValue("ten_bp", item.ten_bp);
        } else if (name === "ma_nvkd") {
            formik.setFieldValue("ten_nvkd", item.ten_nvkd);
        } else if (name === "ma_httt") {
            formik.setFieldValue("ten_httt", item.ten_httt);
        } else if (name === "ma_kho") {
            formik.setFieldValue("ten_kho", item.ten_kho);
        } else if (name === "ma_plkh1") {
            formik.setFieldValue("ten_plkh1", item.ten_plkh);
        } else if (name === "ma_plkh2") {
            formik.setFieldValue("ten_plkh2", item.ten_plkh);
        } else if (name === "ma_plkh3") {
            formik.setFieldValue("ten_plkh3", item.ten_plkh);
        } else if (name === "ma_plvt1") {
            formik.setFieldValue("ten_plvt1", item.ten_plvt);
        } else if (name === "ma_plvt2") {
            formik.setFieldValue("ten_plvt2", item.ten_plvt);
        } else if (name === "ma_plvt3") {
            formik.setFieldValue("ten_plvt3", item.ten_plvt);
        }
    };
    return <>
        <ZenFieldSelectApi
            loadApi
            lookup={ZenLookup.Ma_kh}
            label={"rpt.Ma_kh"} defaultlabel="Mã khách hàng"
            name="ma_kh" formik={formik}
            onItemSelected={handleItemSelected}
        />
        <ZenFieldSelectApi
            lookup={ZenLookup.Ma_nhkh}
            label={"rpt.Ma_nhkh"} defaultlabel="Mã nhóm khách hàng"
            name="ma_nhkh" formik={formik}
            onItemSelected={handleItemSelected}
        />
        <ZenFieldSelectApi
            loadApi
            lookup={ZenLookup.Ma_hd}
            label={"rpt.Ma_hd"} defaultlabel="Mã hợp đồng"
            name="ma_hd" formik={formik}
            onItemSelected={handleItemSelected}
        />
        <ZenFieldSelectApi
            loadApi
            lookup={ZenLookup.Ma_nhhd}
            label={"rpt.Ma_nhhd"} defaultlabel="Mã nhóm hợp đồng"
            name="ma_nhhd" formik={formik}
            onItemSelected={handleItemSelected}
        />
        <ZenFieldSelectApi
            loadApi
            lookup={ZenLookup.Ma_vt}
            label={"rpt.Ma_vt"} defaultlabel="Mã vật tư"
            name="ma_vt" formik={formik}
            onItemSelected={handleItemSelected}
        />
        <ZenFieldSelectApi
            lookup={ZenLookup.Ma_nhvt}
            label={"rpt.Ma_nhhd"} defaultlabel="Mã nhóm vật tư"
            name="ma_nhvt" formik={formik}
            onItemSelected={handleItemSelected}
        />
        <ZenFieldSelectApi
            lookup={ZenLookup.Ma_bp}
            label={"rpt.Ma_bp"} defaultlabel="Mã bộ phận"
            name="ma_bp" formik={formik}
            onItemSelected={handleItemSelected}
        />
        <ZenFieldSelectApi
            lookup={ZenLookup.Ma_nvkd}
            label={"rpt.Ma_nvkd"} defaultlabel="Mã NVKD"
            name="ma_nvkd" formik={formik}
            onItemSelected={handleItemSelected}
        />
        <ZenFieldSelectApi
            loadApi
            lookup={ZenLookup.Ma_httt_so}
            label={"rpt.Ma_httt"} defaultlabel="Mã hình thức thanh toán"
            name="ma_httt" formik={formik}
            onItemSelected={handleItemSelected}
        />
        <ZenFieldSelectApi
            lookup={ZenLookup.Ma_kho}
            label={"rpt.Ma_kho"} defaultlabel="Mã kho"
            name="ma_kho" formik={formik}
            onItemSelected={handleItemSelected}
        />
        <ZenFieldSelectApi
            lookup={{
                ...ZenLookup.Ma_plkh,
                onLocalWhere: (items) => {
                    return items?.filter(t => t.loai == "1") || []
                }
            }}
            label={"rpt.Ma_plkh1"} defaultlabel="Phân loại kh 1"
            name="ma_plkh1" formik={formik}
            onItemSelected={handleItemSelected}
        />
        <ZenFieldSelectApi
            lookup={{
                ...ZenLookup.Ma_plkh,
                onLocalWhere: (items) => {
                    return items?.filter(t => t.loai == "2") || []
                }
            }}
            label={"rpt.Ma_plkh2"} defaultlabel="Phân loại kh 2"
            name="ma_plkh2" formik={formik}
            onItemSelected={handleItemSelected}
        />
        <ZenFieldSelectApi
            lookup={{
                ...ZenLookup.Ma_plkh,
                onLocalWhere: (items) => {
                    return items?.filter(t => t.loai == "3") || []
                }
            }}
            label={"rpt.Ma_plkh3"} defaultlabel="Phân loại kh 3"
            name="ma_plkh3" formik={formik}
            onItemSelected={handleItemSelected}
        />
        <ZenFieldSelectApi
            lookup={{
                ...ZenLookup.Ma_plvt,
                onLocalWhere: (items) => {
                    return items?.filter(t => t.loai == "1")
                }
            }}
            label={"rpt.Ma_plvt1"} defaultlabel="Phân loại vt 1"
            name="ma_plvt1" formik={formik}
            onItemSelected={handleItemSelected}
        />
        <ZenFieldSelectApi
            lookup={{
                ...ZenLookup.Ma_plvt,
                onLocalWhere: (items) => {
                    return items?.filter(t => t.loai == "2")
                }
            }}
            label={"rpt.Ma_plvt2"} defaultlabel="Phân loại vt 2"
            name="ma_plvt2" formik={formik}
            onItemSelected={handleItemSelected}
        />
        <ZenFieldSelectApi
            lookup={{
                ...ZenLookup.Ma_plvt,
                onLocalWhere: (items) => {
                    return items?.filter(t => t.loai == "3")
                }
            }}
            label={"rpt.Ma_plvt3"} defaultlabel="Phân loại vt 3"
            name="ma_plvt3" formik={formik}
            onItemSelected={handleItemSelected}
        />
    </>
}

const TableForm = ({ data = [], filter = {} }) => {
    return <>
        <RptTable maNt={filter.Ma_nt}>
            <Table.Header fullWidth>
                <RptHeader maNt={filter.Ma_nt}
                    header={[
                        { text: ["rpt.ma_vt", "Mã hàng"] },
                        { text: ["rpt.ten_vt", "Tên mặt hàng"] },
                        { text: ["rpt.dvt", "ĐVT"] },
                        { text: ["rpt.So_luong", "Số lượng"] },
                        { text: ["rpt.gia", "Tiền vốn"] },
                        { text: ["rpt.tien", "Tiền bán"] },
                        { text: ["rpt.tien_ck", "CK"] },
                        { text: ["rpt.ck_ds", "CK doanh số"] },
                        { text: ["rpt.tien_sck", "Tiền bán sau CK"] },
                        { text: ["rpt.tien_lai", "Tiền lãi"] },
                        { text: ["rpt.ma_bp", "Mã BP"] },
                        { text: ["rpt.ma_nvkd", "Mã NVKD"] },
                        { text: ["rpt.ten_nvkd", "Tên NVKD"] },
                    ]}
                />

            </Table.Header>
            <Table.Body>
                {data.length > 0 ? data.map((item, index) => {
                    const isNt = filter.Ma_nt && filter.Ma_nt !== 'VND' ? true : false
                    return <RptTableRow key={index} isBold={item.bold} index={index} item={item}>
                        <RptTableCell value={item.ma_vt} />
                        <RptTableCell value={item.ten_vt} />
                        <RptTableCell value={item.dvt} />
                        <RptTableCell value={item.so_luong} type="number" />
                        <RptTableCell value={item.tien_von} type="number" />
                        <RptTableCell value={item.tien2} type="number" />
                        <RptTableCell value={item.tien_ck} type="number" />
                        <RptTableCell value={item.ck_ds} />
                        <RptTableCell value={item.tien_sck} type="number" />
                        <RptTableCell value={item.tien_lai} type="number" />
                        <RptTableCell value={item.ma_bp} />
                        <RptTableCell value={item.ma_nvkd} />
                        <RptTableCell value={item.ten_nvkd} />
                    </RptTableRow>
                }) : undefined}
            </Table.Body>
        </RptTable>
    </>
}

export const SORptBCPT0401 = {
    FilterForm: FilterForm,
    TableForm: TableForm,
    permission: "06.40.4",
    visible: true,    // hiện/ẩn báo cáo
    route: routes.SORptBCPT0401,

    period: {
        fromDate: "ngay1",
        toDate: "ngay2"
    },

    linkHeader: {
        id: "SORptBCPT0401",
        defaultMessage: "Báo cáo doanh số theo nhân viên bán hàng",
        active: true
    },

    info: {
        code: "06.20.34.1",
    },
    initFilter: {
        ngay1: "",
        ngay2: "",
        ma_kh: "",
        ma_nhkh: "",
        ma_hd: "",
        ma_nhhd: "",
        ma_vt: "",
        ma_nhvt: "",
        ma_kho: "",
        ma_httt: "",
        ma_nvkd: "",
        ma_bp: "",
        ma_plkh1: "",
        ma_plkh2: "",
        ma_plkh3: "",
        ma_plvt1: "",
        ma_plvt2: "",
        ma_plvt3: "",
    },
    columns: [
        {
            ...IntlFormat.default(Mess_Report.MaVt),
            fieldName: "ma_vt", type: "string", sorter: true
        },
        {
            ...IntlFormat.default(Mess_Report.TenVt),
            fieldName: "ten_vt", type: "string", sorter: true,
        },
        {
            ...IntlFormat.default(Mess_Report.Dvt),
            fieldName: "dvt", type: "string", sorter: true,
        },
        {
            ...IntlFormat.default(Mess_Report.SoLuong),
            fieldName: "so_luong", type: "number", sorter: true,
        },
        {
            ...IntlFormat.default(Mess_Report.TienVon),
            fieldName: "gia", type: "number", sorter: true,
        },
        {
            ...IntlFormat.default(Mess_Report.TienBan),
            fieldName: "tien", type: "number", sorter: true,
        },
        {
            ...IntlFormat.default(Mess_Report.ChietKhau),
            fieldName: "tien_ck", type: "number", sorter: true,
        },
        {
            ...IntlFormat.default(Mess_Report.ChietKhauDS),
            fieldName: "ck_ds", type: "number", sorter: true,
        },
        {
            ...IntlFormat.default(Mess_Report.TienSauCK),
            fieldName: "tien_sck", type: "number", sorter: true,
        },
        {
            ...IntlFormat.default(Mess_Report.TienLai),
            fieldName: "tien_lai", type: "number", sorter: true,
        },
        {
            ...IntlFormat.default(Mess_Report.MaBp),
            fieldName: "ma_bp", type: "string", sorter: true,
        },
        {
            ...IntlFormat.default(Mess_Report.MaNvkd),
            fieldName: "ma_nvkd", type: "string", sorter: true,
        },
    ],
    formValidation: []
}
