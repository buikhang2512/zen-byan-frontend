import React from "react";
import { Table, Form } from "semantic-ui-react";
import { ZenFieldSelectApi, ZenDatePeriod, ZenField, ZenFieldCheckbox } from "../../../components/Control";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { RptHeader, RptTable, RptTableCell, RptTableRow } from "../../../components/Control/zReport";
import * as routes from "../../../constants/routes";
import { IntlFormat } from "../../../utils/intlFormat";
import { Mess_Report } from "../../ComponentInfo/zLanguage/variable";

const FilterForm = ({ formik }) => {
    const handleItemSelected = (item, { name }) => {
        if (name === "tk_dt") {
            formik.setFieldValue("ten_tk_dt", item.ten_tk);
        } else if (name === "TkDu_DT") {
            formik.setFieldValue("ten_tkdu_dt", item.ten_tk);
        } else if (name === "tk_cp") {
            formik.setFieldValue("ten_tk_cp", item.ten_tk);
        } else if (name === "tkdu_cp") {
            formik.setFieldValue("ten_tkdu_cp", item.ten_tk);
        } else if (name === "ma_kh") {
            formik.setFieldValue("ten_kh", item.ten_kh);
        } else if (name === "ma_hd") {
            formik.setFieldValue("ten_hd", item.ten_hd);
        }
    };
    return <>
        <ZenFieldSelectApi
            loadApi
            lookup={ZenLookup.Ma_hd}
            label={"rpt.Ma_hd"} defaultlabel="Mã hợp đồng"
            name="ma_hd" formik={formik}
            onItemSelected={handleItemSelected}
        />
        <ZenFieldSelectApi
            loadApi
            lookup={ZenLookup.Ma_kh}
            label={"rpt.Ma_kh"} defaultlabel="Mã khách hàng"
            name="ma_kh" formik={formik}
            onItemSelected={handleItemSelected}
        />
        <ZenFieldSelectApi
            lookup={ZenLookup.TK}
            label={"rpt.Tk_DT"} defaultlabel="Tài khoản doanh thu"
            name="tk_dt" formik={formik}
            onItemSelected={handleItemSelected}
        />

        <ZenFieldCheckbox //tạm dùng checkbox
            label={"rpt.PsNc_DT"} defaultlabel="Phát sinh nợ"
            name="psnc_dt" props={formik}
        />
        <ZenFieldSelectApi
            lookup={ZenLookup.TK}
            label={"rpt.TkDu_DT"} defaultlabel="Tài khoản Đ/Ứ doanh thu"
            name="tkdu_dt" formik={formik}
            onItemSelected={handleItemSelected}
        />

        <ZenFieldSelectApi
            lookup={ZenLookup.TK}
            label={"rpt.Tk_CP"} defaultlabel="Tài khoản chi phí"
            name="tk_cp" formik={formik}
            onItemSelected={handleItemSelected}
        />
        <ZenFieldCheckbox //tạm dùng checkbox
            label={"rpt.PsNc_CP"} defaultlabel="Phát sinh nợ"
            name="psnc_cp" props={formik}
            onItemSelected={handleItemSelected}
        />
        <ZenFieldSelectApi
            lookup={ZenLookup.TK}
            label={"rpt.TkDu_CP"} defaultlabel="Tài khoản Đ/Ứ chi phí"
            name="tkdu_cp" formik={formik}
        />
    </>
}

const TableForm = ({ data = [], filter = {} }) => {
    return <>
        <RptTable maNt={filter.Ma_nt}>
            <Table.Header fullWidth>
                <RptHeader maNt={filter.Ma_nt}
                    header={[
                        { text: ["rpt.ma_hd", "Mã hợp đồng"] },
                        { text: ["rpt.ten_hd", "Tên hợp đồng"] },
                        { text: ["rpt.ngay_ct", "Ngày ct"] },
                        { text: ["rpt.so_ct", "Số ct"] },
                        { text: ["rpt.dien_giai", "Diễn giải"] },
                        { text: ["rpt.tien", "Tiền"] },
                        { text: ["rpt.ma_ct", "Mã CT"] },
                        { text: ["rpt.ma_kh_hd", "Ma_kh_hd"] },
                        { text: ["rpt.ma_kh", "Ma_kh"] },
                        { text: ["rpt.tien_nt", "Tien_nt"] },
                        { text: ["rpt.dt_cp", "dt_cp"] },
                    ]}
                />
            </Table.Header>
            <Table.Body>
                {data.length > 0 ? data.map((item, index) => {
                    const isNt = filter.Ma_nt && filter.Ma_nt !== 'VND' ? true : false
                    return <RptTableRow key={index} isBold={item.bold} index={index} item={item}>
                        <RptTableCell value={item.ma_hd} />
                        <RptTableCell value={item.ten_hd} />
                        <RptTableCell value={item.ngay_ct} type="date" />
                        <RptTableCell value={item.so_ct} />
                        <RptTableCell value={item.dien_giai} />
                        <RptTableCell value={item.tien} type="number" />
                        <RptTableCell value={item.ma_ct} />
                        <RptTableCell value={item.ma_kh_hd} />
                        <RptTableCell value={item.ma_kh} />
                        <RptTableCell value={item.tien_nt} />
                        <RptTableCell value={item.dt_cp} />
                    </RptTableRow>
                }) : undefined}
            </Table.Body>
        </RptTable>
    </>
}

export const SORptLaiLo = {
    FilterForm: FilterForm,
    TableForm: TableForm,
    permission: "06.40.7",
    visible: true,    // hiện/ẩn báo cáo
    route: routes.SORptLaiLo,

    period: {
        fromDate: "ngay1",
        toDate: "ngay2"
    },

    linkHeader: {
        id: "SORptLaiLo",
        defaultMessage: "Báo cáo lãi lỗ theo hợp đồng",
        active: true
    },

    info: {
        code: "06.20.66.1",
    },
    initFilter: {
        ngay1: "",
        ngay2: "",
        ma_kh: "",
        ma_hd: "",
        tk_dt: "",
        psnc_dt: "",
        tkdu_dt: "",
        tk_cp: "",
        psnc_cp: "",
        tkdu_cp: "",
    },
    columns: [
        {
            ...IntlFormat.default(Mess_Report.MaHd),
            fieldName: "ma_hd", type: "string", sorter: true
        },
        {
            ...IntlFormat.default(Mess_Report.TenHd),
            fieldName: "ten_hd", type: "string", sorter: true,
        },
        {
            ...IntlFormat.default(Mess_Report.NgayCt),
            fieldName: "ngay_ct", type: "date", sorter: true
        },
        {
            ...IntlFormat.default(Mess_Report.SoCt),
            fieldName: "so_ct", type: "string", sorter: true,
        },
        {
            ...IntlFormat.default(Mess_Report.MaKh),
            fieldName: "ma_kh", type: "string", sorter: true,
        },
        {
            ...IntlFormat.default(Mess_Report.DienGiai),
            fieldName: "dien_giai", type: "string", sorter: true,
        },

        {
            ...IntlFormat.default(Mess_Report.ThanhTien),
            fieldName: "tien", type: "number", sorter: true,
        },
    ],
    formValidation: []
}
