import React from "react";
import { Table, Form, Item } from "semantic-ui-react";
import { ZenFieldSelectApi, ZenDatePeriod, ZenField, ZenFieldSelect } from "../../../components/Control";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { RptHeader, RptTable, RptTableCell, RptTableRow } from "../../../components/Control/zReport";
import * as routes from "../../../constants/routes";
import { IntlFormat } from "../../../utils/intlFormat";
import { Mess_Report } from "../../ComponentInfo/zLanguage/variable";

const FilterForm = ({ formik }) => {
    const Options_GroupType = [
        { key: "1", value: "MA_KH", text: "Khách hàng" },
        { key: "2", value: "MA_BP", text: "Bộ phận" },
        { key: "3", value: "MA_HD", text: "Hợp đồng" },
        { key: "4", value: "MA_HTTT", text: "Hình thức thanh toán" },
        { key: "5", value: "MA_VT", text: "Vật tư" },
        { key: "6", value: "MA_KHO", text: "Kho" },
        { key: "7", value: "MA_PHI", text: "Phí" },
        { key: "8", value: "STT_REC", text: "Chứng từ" },
    ]
    const handleItemSelected = (item, { name }) => {
        if (name === "tk") {
            formik.setFieldValue("ten_tk", item.ten_tk);
        } else if (name === "tk_du") {
            formik.setFieldValue("ten_tk_du", item.ten_tk);
        } else if (name === "ma_kh") {
            formik.setFieldValue("ten_kh", item.ten_kh);
        } else if (name === "ma_nhkh") {
            formik.setFieldValue("ten_nhkh", item.ten_nhkh);
        } else if (name === "ma_hd") {
            formik.setFieldValue("ten_hd", item.ten_hd);
        } else if (name === "ma_nhhd") {
            formik.setFieldValue("ten_nhhd", item.ten_nhhd);
        } else if (name === "ma_vt") {
            formik.setFieldValue("ten_vt", item.ten_vt);
        } else if (name === "ma_nhvt") {
            formik.setFieldValue("ten_nhvt", item.ten_nhvt);
        } else if (name === "ma_bp") {
            formik.setFieldValue("ten_bp", item.ten_bp);
        } else if (name === "ma_nvkd") {
            formik.setFieldValue("ten_nvkd", item.ten_nvkd);
        } else if (name === "ma_httt") {
            formik.setFieldValue("ten_httt", item.ten_httt);
        } else if (name === "ma_kho") {
            formik.setFieldValue("ten_kho", item.ten_kho);
        } else if (name === "ma_plkh1") {
            formik.setFieldValue("ten_plkh1", item.ten_plkh);
        } else if (name === "ma_plkh2") {
            formik.setFieldValue("ten_plkh2", item.ten_plkh);
        } else if (name === "ma_plkh3") {
            formik.setFieldValue("ten_plkh3", item.ten_plkh);
        } else if (name === "ma_plvt1") {
            formik.setFieldValue("ten_plvt1", item.ten_plvt);
        } else if (name === "ma_plvt2") {
            formik.setFieldValue("ten_plvt2", item.ten_plvt);
        } else if (name === "ma_plvt3") {
            formik.setFieldValue("ten_plvt3", item.ten_plvt);
        }
    };

    return <>
        <ZenField
            label={"rpt.Soct1"} defaultlabel="Số chứng từ số"
            name="soct1" props={formik}
        />
        <ZenField
            label={"rpt.Soct2"} defaultlabel="Đến số"
            name="soct2" props={formik}
        />
        <ZenFieldSelect options={Options_GroupType}
            label={"rpt.loai"} defaultlabel="Nhóm theo"
            name="grouptype" props={formik}
        />
        <ZenFieldSelectApi
            loadApi
            lookup={ZenLookup.Ma_kh}
            label={"rpt.Ma_kh"} defaultlabel="Mã khách hàng"
            name="ma_kh" formik={formik}
            onItemSelected={handleItemSelected}
        />
        <ZenFieldSelectApi
            lookup={ZenLookup.Ma_nhkh}
            label={"rpt.Ma_nhkh"} defaultlabel="Mã nhóm khách hàng"
            name="ma_nhkh" formik={formik}
            onItemSelected={handleItemSelected}
        />
        <ZenFieldSelectApi
            loadApi
            lookup={ZenLookup.Ma_hd}
            label={"rpt.Ma_hd"} defaultlabel="Mã hợp đồng"
            name="ma_hd" formik={formik}
            onItemSelected={handleItemSelected}
        />
        <ZenFieldSelectApi
            loadApi
            lookup={ZenLookup.Ma_nhhd}
            label={"rpt.Ma_nhhd"} defaultlabel="Mã nhóm hợp đồng"
            name="ma_nhhd" formik={formik}
            onItemSelected={handleItemSelected}
        />
        <ZenFieldSelectApi
            loadApi
            lookup={ZenLookup.Ma_vt}
            label={"rpt.Ma_vt"} defaultlabel="Mã vật tư"
            name="ma_vt" formik={formik}
            onItemSelected={handleItemSelected}
        />
        <ZenFieldSelectApi
            lookup={ZenLookup.Ma_nhvt}
            label={"rpt.Ma_nhhd"} defaultlabel="Mã nhóm vật tư"
            name="ma_nhvt" formik={formik}
            onItemSelected={handleItemSelected}
        />
        <ZenFieldSelectApi
            lookup={ZenLookup.Ma_bp}
            label={"rpt.Ma_bp"} defaultlabel="Mã bộ phận"
            name="ma_bp" formik={formik}
            onItemSelected={handleItemSelected}
        />
        <ZenFieldSelectApi
            lookup={ZenLookup.Ma_nvkd}
            label={"rpt.Ma_nvkd"} defaultlabel="Mã NVKD"
            name="ma_nvkd" formik={formik}
            onItemSelected={handleItemSelected}
        />
        <ZenFieldSelectApi
            loadApi
            lookup={ZenLookup.Ma_httt_so}
            label={"rpt.Ma_httt"} defaultlabel="Mã hình thức thanh toán"
            name="ma_httt" formik={formik}
            onItemSelected={handleItemSelected}
        />
        <ZenFieldSelectApi
            lookup={ZenLookup.Ma_kho}
            label={"rpt.Ma_kho"} defaultlabel="Mã kho"
            name="ma_kho" formik={formik}
            onItemSelected={handleItemSelected}
        />
        <ZenFieldSelectApi
            lookup={{
                ...ZenLookup.Ma_plkh,
                onLocalWhere: (items) => {
                    return items?.filter(t => t.loai == "1") || []
                }
            }}
            label={"rpt.Ma_plkh1"} defaultlabel="Phân loại kh 1"
            name="ma_plkh1" formik={formik}
            onItemSelected={handleItemSelected}
        />
        <ZenFieldSelectApi
            lookup={{
                ...ZenLookup.Ma_plkh,
                onLocalWhere: (items) => {
                    return items?.filter(t => t.loai == "2") || []
                }
            }}
            label={"rpt.Ma_plkh2"} defaultlabel="Phân loại kh 2"
            name="ma_plkh2" formik={formik}
            onItemSelected={handleItemSelected}
        />
        <ZenFieldSelectApi
            lookup={{
                ...ZenLookup.Ma_plkh,
                onLocalWhere: (items) => {
                    return items?.filter(t => t.loai == "3") || []
                }
            }}
            label={"rpt.Ma_plkh3"} defaultlabel="Phân loại kh 3"
            name="ma_plkh3" formik={formik}
            onItemSelected={handleItemSelected}
        />
        <ZenFieldSelectApi
            lookup={{
                ...ZenLookup.Ma_plvt,
                onLocalWhere: (items) => {
                    return items?.filter(t => t.loai == "1")
                }
            }}
            label={"rpt.Ma_plvt1"} defaultlabel="Phân loại vt 1"
            name="ma_plvt1" formik={formik}
            onItemSelected={handleItemSelected}
        />
        <ZenFieldSelectApi
            lookup={{
                ...ZenLookup.Ma_plvt,
                onLocalWhere: (items) => {
                    return items?.filter(t => t.loai == "2")
                }
            }}
            label={"rpt.Ma_plvt2"} defaultlabel="Phân loại vt 2"
            name="ma_plvt2" formik={formik}
            onItemSelected={handleItemSelected}
        />
        <ZenFieldSelectApi
            lookup={{
                ...ZenLookup.Ma_plvt,
                onLocalWhere: (items) => {
                    return items?.filter(t => t.loai == "3")
                }
            }}
            label={"rpt.Ma_plvt3"} defaultlabel="Phân loại vt 3"
            name="ma_plvt3" formik={formik}
            onItemSelected={handleItemSelected}
        />
    </>
}

const TableForm = ({ data = [], filter = {} }) => {
    return <>
        <RptTable maNt={filter.Ma_nt}>
            <Table.Header fullWidth >
                <RptHeader maNt={filter.Ma_nt}
                    header={[
                        { text: ["rpt.ngay_ct", "Ngày CT"] },
                        { text: ["rpt.so_ct", "Số CT"] },
                        { text: ["rpt.dien_giai", "Diễn giải"] },
                        { text: ["rpt.ma_vt", "Mã VT"] },
                        { text: ["rpt.ten_vt", "Tên VT"] },
                        { text: ["rpt.so_luong", "Số lượng"] },
                        { text: ["rpt.gia", "Đơn giá"] },
                        { text: ["rpt.tien", "Tông tiền hàng"] },
                        { text: ["rpt.tien_ck", "Chiết khấu"] },
                        { text: ["rpt.ck_ds", "CK doanh số"] },
                        { text: ["rpt.thue_gtgt", "Tổng tiền thuế"] },
                        { text: ["rpt.t_t_tt", "Tổng thanh toán"] },
                        { text: ["rpt.ma_kh", "Mã KH"] },
                        { text: ["rpt.ma_hd", "Mã HĐ"] },
                        { text: ["rpt.ma_bp", "Mã BP"] },
                        { text: ["rpt.ma_kho", "Mã kho"] },
                        { text: ["rpt.ma_nvkd", "Mã NVKD"] },
                        { text: ["rpt.ma_ct", "Mã CT"] },
                    ]}
                />
            </Table.Header>
            <Table.Body>
                {data.length > 0 ? data.map((item, index) => {
                    const isNt = filter.Ma_nt && filter.Ma_nt !== 'VND' ? true : false
                    return <RptTableRow key={index} isBold={item.bold} index={index} item={item}>
                        <RptTableCell value={item.ngay_ct} type="date" />
                        <RptTableCell value={item.so_ct} />
                        <RptTableCell value={item.dien_giai} />
                        <RptTableCell value={item.ma_vt} />
                        <RptTableCell value={item.ten_vt} />
                        <RptTableCell value={item.so_luong} type="number" />
                        <RptTableCell value={item.gia2} type="number" />
                        <RptTableCell value={item.tien2} type="number" />
                        <RptTableCell value={item.tien_ck} type="number" />
                        <RptTableCell value={item.ck_ds} type="number" />
                        <RptTableCell value={item.thue_gtgt} type="number" />
                        <RptTableCell value={item.t_t_tt} type="number" />
                        <RptTableCell value={item.ma_kh} />
                        <RptTableCell value={item.ma_hd} />
                        <RptTableCell value={item.ma_bp} />
                        <RptTableCell value={item.ma_kho} />
                        <RptTableCell value={item.ma_nvkd} />
                        <RptTableCell value={item.ma_ct} />
                    </RptTableRow>
                }) : undefined}
            </Table.Body>
        </RptTable>
    </>
}

export const SORptBK02 = {
    FilterForm: FilterForm,
    TableForm: TableForm,
    permission: "06.40.2",
    visible: true,    // hiện/ẩn báo cáo
    route: routes.SORptBK02,

    period: {
        fromDate: "ngay1",
        toDate: "ngay2"
    },

    linkHeader: {
        id: "SORptBK02",
        defaultMessage: "Bảng kê bán hàng nhóm theo chỉ tiêu",
        active: true
    },

    info: {
        code: "06.20.04.3",
    },
    initFilter: {
        ngay1: "",
        ngay2: "",
        grouptype: "MA_KH",
        soct1: "",
        soct2: "",
        ma_kh: "",
        ma_nhkh: "",
        ma_hd: "",
        ma_nhhd: "",
        ma_vt: "",
        ma_nhvt: "",
        ma_kho: "",
        ma_httt: "",
        ma_nvkd: "",
        ma_bp: "",
        ma_plkh1: "",
        ma_plkh2: "",
        ma_plkh3: "",
        ma_plvt1: "",
        ma_plvt2: "",
        ma_plvt3: "",
    },
    columns: [
        {
            ...IntlFormat.default(Mess_Report.NgayCt),
            fieldName: "ngay_ct", type: "date", sorter: true
        },
        {
            ...IntlFormat.default(Mess_Report.SoCt),
            fieldName: "so_ct", type: "string", sorter: true,
        },
        {
            ...IntlFormat.default(Mess_Report.DienGiai),
            fieldName: "dien_giai", type: "string", sorter: true,
        },
        {
            ...IntlFormat.default(Mess_Report.MaKh),
            fieldName: "ma_kh", type: "string", sorter: true,
        },
        {
            ...IntlFormat.default(Mess_Report.MaVt),
            fieldName: "ma_vt", type: "string", sorter: true,
        },
        {
            ...IntlFormat.default(Mess_Report.TenVt),
            fieldName: "ten_vt", type: "string", sorter: true,
        },
        {
            ...IntlFormat.default(Mess_Report.Dvt),
            fieldName: "dvt", type: "string", sorter: true,
        },
        {
            ...IntlFormat.default(Mess_Report.SoLuong),
            fieldName: "so_luong", type: "number", sorter: true,
        },
        {
            ...IntlFormat.default(Mess_Report.DonGia),
            fieldName: "gia", type: "number", sorter: true,
        },
        {
            ...IntlFormat.default(Mess_Report.ThanhTien),
            fieldName: "tien", type: "number", sorter: true,
        },
        {
            ...IntlFormat.default(Mess_Report.ChietKhau),
            fieldName: "tien_ck", type: "number", sorter: true,
        },
        {
            ...IntlFormat.default(Mess_Report.ChietKhauDS),
            fieldName: "ck_ds", type: "number", sorter: true,
        },
        {
            ...IntlFormat.default(Mess_Report.ThueGtgt),
            fieldName: "thue_gtgt", type: "number", sorter: true,
        },
        {
            ...IntlFormat.default(Mess_Report.TongTien),
            fieldName: "t_t_tt", type: "number", sorter: true,
        },
        {
            ...IntlFormat.default(Mess_Report.MaHd),
            fieldName: "ma_hd", type: "string", sorter: true,
        },
        {
            ...IntlFormat.default(Mess_Report.MaBp),
            fieldName: "ma_bp", type: "string", sorter: true,
        },
        {
            ...IntlFormat.default(Mess_Report.MaKho),
            fieldName: "ma_kho", type: "string", sorter: true,
        },
        {
            ...IntlFormat.default(Mess_Report.MaNvkd),
            fieldName: "ma_nvkd", type: "string", sorter: true,
        },
        {
            ...IntlFormat.default(Mess_Report.MaCt),
            fieldName: "ma_ct", type: "string", sorter: true,
        },
    ],
    formValidation: []
}