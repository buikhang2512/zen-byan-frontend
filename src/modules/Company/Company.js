import React, { createRef } from "react";
import { Button, Form, Segment } from "semantic-ui-react";
import * as routes from "../../constants/routes";
import {
   ZenButton,
   ZenField,
   ZenFieldSelect,
   ZenFormik,
   ZenMessageToast
} from "../../components/Control";
import { FormMode } from "../../components/Control/zzControlHelper";
import { IntlFormat } from "../../utils/intlFormat";
import { Mess_Company } from "./zLanguage/variable";
import { ApiCompany } from "./Api";
import { ZenHelper } from "../../utils/global";
import { ValidError } from "../../utils/language/variable";
import { AppContext } from "../../AppContext";
import * as permissions from "../../constants/permissions"

class CompanyForm extends React.Component {
   constructor(props) {
      super(props);
      this.refForm = createRef();
      this.optionsKeToan = Object.keys(optionsKeToan).map(key => {
         return {
            value: key,
            text: key
         }
      })
      this.state = {
         loading: loadingType.Form,
         mode: FormMode.VIEW,
         items: {},
         error: null,
      }
   }

   componentDidMount() {
      ApiCompany.get(res => {
         if (res.status === 200) {
            this.setState({
               items: res.data.data || {},
               mode: FormMode.VIEW,
               loading: ""
            })
         } else {
            this.setState({
               error: ZenHelper.getResponseError(res),
               loading: ""
            })
         }
      })
   }

   handleSubmit = (values) => {
      this.setState({
         loading: loadingType.BtnSave
      })

      ApiCompany.update(values, res => {
         if (res.status === 204) {
            this.setState({
               mode: FormMode.VIEW,
               loading: ""
            })
            this.context.actions.setTenantName(values.ten_cty)
            ZenMessageToast.success();
         } else {
            this.setState({
               error: ZenHelper.getResponseError(res),
               loading: ""
            })
         }
      })
   }

   handleChangeMode = (mode) => {
      this.setState({ mode: mode })
      if (mode === FormMode.VIEW) {
         this.refForm.current.handleReset();
      }
   }

   render() {
      const { permission } = this.props
      const { mode, items, loading } = this.state

      return <>
         <h3>Thông tin doanh nghiệp</h3>
         <Segment loading={loading === loadingType.Form}>
            <div style={{ margin: "0 20%" }}>
               <ZenFormik
                  form={"company-form"} ref={this.refForm}
                  validation={formValidation}
                  initItem={items}
                  onSubmit={this.handleSubmit}
               >
                  {
                     formik => {
                        return <Form>
                           <ZenField autoFocus
                              readOnly={mode === FormMode.VIEW ? true : false}
                              name="ten_cty" props={formik}
                              {...IntlFormat.label(Mess_Company.TenCty)}
                           />
                           <ZenField
                              readOnly={mode === FormMode.VIEW ? true : false}
                              name="dia_chi" props={formik}
                              {...IntlFormat.label(Mess_Company.DiaChi)}
                           />
                           <ZenField
                              readOnly={mode === FormMode.VIEW ? true : false}
                              name="ma_thue" props={formik}
                              {...IntlFormat.label(Mess_Company.MaThue)}
                           />

                           <Form.Group widths="equal">
                              <ZenField
                                 readOnly={mode === FormMode.VIEW ? true : false}
                                 name="giam_doc" props={formik}
                                 {...IntlFormat.label(Mess_Company.GiamDoc)}
                              />
                              <ZenField
                                 readOnly={mode === FormMode.VIEW ? true : false}
                                 name="ktt" props={formik}
                                 {...IntlFormat.label(Mess_Company.KeToanTruong)}
                              />
                           </Form.Group>

                           <Form.Group widths="equal">
                              <ZenField
                                 readOnly={mode === FormMode.VIEW ? true : false}
                                 name="tel" props={formik}
                                 {...IntlFormat.label(Mess_Company.DienThoai)}
                              />
                              <ZenField
                                 readOnly={mode === FormMode.VIEW ? true : false}
                                 name="fax" props={formik}
                                 isIntl={false} label="Fax"
                              />
                           </Form.Group>

                           <ZenField
                              readOnly={mode === FormMode.VIEW ? true : false}
                              name="email" props={formik}
                              isIntl={false} label="Email"
                           />

                           <Form.Group widths="equal">
                              <ZenField
                                 readOnly={mode === FormMode.VIEW ? true : false}
                                 name="ten_nh" props={formik}
                                 {...IntlFormat.label(Mess_Company.NganHang)}
                              />
                              <ZenField
                                 readOnly={mode === FormMode.VIEW ? true : false}
                                 name="so_tk_nh" props={formik}
                                 {...IntlFormat.label(Mess_Company.SoTK_NgHang)}
                              />
                           </Form.Group>

                           <Form.Group>
                              <ZenFieldSelect width="4"
                                 readOnly={mode === FormMode.VIEW ? true : false}
                                 name="qd_cdkt" props={formik}
                                 options={this.optionsKeToan}
                                 {...IntlFormat.label(Mess_Company.CheDoKT)}
                                 onSelectedItem={(option) => {
                                    formik.setFieldValue("ten_qd_cdkt", option ? optionsKeToan[option.value] : "")
                                 }}
                              />

                              <ZenField width="12"
                                 readOnly placeholder={"Thông tư"}
                                 name="ten_qd_cdkt" props={formik}
                                 isIntl={false}
                                 style={{ paddingTop: "18px" }}
                              />
                           </Form.Group>
                        </Form>
                     }
                  }
               </ZenFormik>

               <ButtonForm mode={mode} loading={loading}
                  permission={permission}
                  onChangeMode={this.handleChangeMode}
                  refFormik={this.refForm.current}
               />
            </div>

         </Segment>
      </>
   }
}

const ButtonForm = ({ loading, permission, refFormik, mode, onChangeMode }) => {
   return permission?.edit ? <>
      {mode === FormMode.VIEW && <Button type="button" size="small"
         icon="edit" primary floated="right"
         onClick={() => onChangeMode(FormMode.EDIT)}
         content={IntlFormat.text(Mess_Company.BtnEdit)}
      />}

      {mode !== FormMode.VIEW && <>
         <Button size="small"
            loading={loading === loadingType.BtnSave}
            disabled={loading === loadingType.BtnSave}
            icon="save" primary floated="right"
            onClick={(e) => refFormik.handleSubmit(e)}
            content={IntlFormat.text(Mess_Company.BtnSave)}
         />

         <ZenButton btnType="cancel" size="small" type="button"
            floated="right"
            loading={loading === loadingType.BtnSave}
            onClick={() => onChangeMode(FormMode.VIEW)}
         />
      </>}
      <div style={{ clear: "both" }} />
   </> : <></>
}

const formValidation = [
   {
      ...ValidError.formik({ name: "ten_cty", type: "string" },
         { type: "required", intl: IntlFormat.default(ValidError.Required) },
      )
   },
]

const loadingType = {
   Form: "form",
   BtnSave: "btnsave"
}

const optionsKeToan = {
   "133": "(Ban hành kèm theo Thông tư 133/2016/TT-BTC ngày 26/8/2016 của Bộ Tài chính)",
   "200": "(Ban hành kèm theo Thông tư số 200/2014/TT-BTC ngày 22/12/2014 của Bộ Tài Chính)"
}
CompanyForm.contextType = AppContext;

export const Company = {
   route: routes.Company,
   Zenform: CompanyForm,
   action: {
      view: { visible: true, permission: permissions.Company },
      edit: { visible: true, permission: permissions.Company },
   },
}