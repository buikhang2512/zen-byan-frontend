
import { IntlFormat, LanguageFormat } from "../../../utils/intlFormat";
import { Mess_Company } from "./variable";

const Company_VI = {
   ...IntlFormat.setMessageLanguage(Mess_Company),
}

const Company_EN = {
   ...IntlFormat.setMessageLanguage(Mess_Company, LanguageFormat.en),
}

export { Company_VI, Company_EN }