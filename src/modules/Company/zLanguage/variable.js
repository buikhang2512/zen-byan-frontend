const prefixCompany = "company."

export const Mess_Company = {
   TenCty: {
      id: prefixCompany + "TenCty",
      defaultMessage: "Tên công ty",
      en: "Company name",
      cn: "",
      fr: "",
   },
   DiaChi: {
      id: prefixCompany + "DiaChi",
      defaultMessage: "Địa chỉ",
   },
   GiamDoc: {
      id: prefixCompany + "GiamDoc",
      defaultMessage: "Giám đốc",
   },
   MaThue: {
      id: prefixCompany + "MaThue",
      defaultMessage: "Mã số thuế",
   },
   KeToanTruong: {
      id: prefixCompany + "ktt",
      defaultMessage: "Kế toán trưởng",
   },
   DienThoai: {
      id: prefixCompany + "DienThoai",
      defaultMessage: "Điện thoại",
   },
   NganHang: {
      id: prefixCompany + "NganHang",
      defaultMessage: "Ngân hàng",
   },
   SoTK_NgHang: {
      id: prefixCompany + "SoTK_NgHang",
      defaultMessage: "Số tài khoản",
   },
   CheDoKT: {
      id: prefixCompany + "CheDoKT",
      defaultMessage: "Chế độ kế toán",
   },
   BtnEdit: {
      id: prefixCompany + "BtnEdit",
      defaultMessage: "Sửa thông tin",
      en: "",
   },
   BtnSave: {
      id: prefixCompany + "BtnSave",
      defaultMessage: "Lưu thông tin",
      en: "",
   },
}