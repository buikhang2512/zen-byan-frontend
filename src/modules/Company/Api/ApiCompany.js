import axios from '../../../Api/axios';

const ExtName = "company"

export const ApiCompany = {
   get(callback) {
      axios.get(`${ExtName}`)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   update(data, callback) {
      axios.put(`${ExtName}`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },
}