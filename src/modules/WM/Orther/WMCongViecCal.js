import React, { useEffect, useMemo, useRef, useState, useCallback } from "react";
import { Accordion, Breadcrumb, Button, Checkbox, Form, Grid, Header, Icon, Loader, Modal, Segment, Tab, Table } from "semantic-ui-react";
import { ZenFieldCheckbox, ZenLoading, Helmet, ZenButton, ContainerScroll, ZenFormik, ZenFieldDate, ZenFieldSelect, SegmentHeader, ZenFieldSelectApi, InputDatePeriod, ZenDatePeriod, HeaderLink, ZenMessageAlert } from "../../../components/Control/index";
import { FormMode, zzControlHelper } from "../../../components/Control/zzControlHelper";
import { ZenHelper, auth } from "../../../utils";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { Calendar, momentLocalizer, } from "react-big-calendar";
import moment from "moment";
import "react-big-calendar/lib/css/react-big-calendar.css";
import { ApiWMCongViec, ApiHrCctcTree } from '../Api/index'
import * as routes from '../../../constants/routes'
import { Link } from "react-router-dom";
import { debounce } from "lodash-es";
import { RowTotalPH, TableScroll, TableTotalPH } from "../../../components/Control/zVoucher";
import FormatNumber from "../../../components/Control/FormatNumber"
import { FormattedDate } from "react-intl";
import * as permissions from "../../../constants/permissions";
import { GlobalStorage, KeyStorage } from "../../../utils/storage"
import { getModal } from "../../ComponentInfo/Dictionary/ZenGetModal";
import { ModalCongViecDetail } from "../../AR/Dictionary/ARDmKhDetail.js/CongViecComponent";
import withDragAndDrop from 'react-big-calendar/lib/addons/dragAndDrop'

import 'react-big-calendar/lib/addons/dragAndDrop/styles.css'

// import CheckboxTree from 'react-checkbox-tree';

import CheckboxTree from 'checkbox-tree-master';
import 'checkbox-tree-master/lib/react-checkbox-tree.css';
import "./CheckBoxTree.css"

import _ from "lodash";

const DragAndDropCalendar = withDragAndDrop(Calendar)

const localizer = momentLocalizer(moment);

export const WMCongViecCalForm = (props) => {
    //const { onLoadData, fieldCode } = props;
    const refForm = useRef();
    const [expand, setExpand] = useState(0);
    const [events, setEvents] = useState([]);
    const [eventsOriginal, setEventsOriginal] = useState([]);
    const [day, setDay] = useState(new Date());
    const [open, setOpen] = useState(false);
    const [loadingForm, setLoadingForm] = useState(false)
    const [loadingCctc, setLoadingCctc] = useState(false)
    const [event, setEvent] = useState();
    const [loaicv, setLoaicv] = useState([]);
    const [checkallCVCT, setCheckallCVCT] = useState(true)
    const [congvieccuatoi, setCongVieccuatoi] = useState([
        { ten_loai_cv: "Công việc tôi tạo", ma: "CR", checked: true },
        { ten_loai_cv: "Công việc tôi phụ trách", ma: "PT", checked: true },
        { ten_loai_cv: "Công việc tôi tham gia/theo dõi", ma: "TG", checked: true },
    ]);

    const [modalInfoCV, setModalInfoCV] = useState(getModal(ZenLookup.WMCongViec));

    const [openModalDetailCongviec, setOpenModalDetailCongViec] = useState()
    const [itemcv, setItemcv] = useState()

    const [draggedEvent, setDraggedEvent] = useState()
    const [currentView, setCurrentView] = useState('month');
    const [checkall, setCheckall] = useState(true)

    const [nodes, setNodes] = useState([])
    const [checkedCctc, setChecedCctc] = useState([
    ])
    const [expandedCctc, setExpandedCCtc] = useState([
    ])

    useEffect(() => {
        let loaicv = GlobalStorage.getByField(KeyStorage.CacheData, 'ma_loai_cv')?.data
        let nextloaicv = []
        for (let item of loaicv) {
            nextloaicv.push({ ...item, checked: true })
        }
        setLoaicv(nextloaicv)
        setLoadingForm(true)
        setLoadingCctc(true)
        ApiWMCongViec.getCongViecCal({ day: new Date() }, res => {
            if (res.status === 200) {
                var newEvents = []
                if (res.data.data.length > 0) {
                    res.data.data.forEach(event => {
                        let newevent = { ...event, start: new Date(event.start), end: new Date(event.end) }
                        newEvents.push(newevent)
                    })
                }
                setEvents(newEvents)
                setEventsOriginal(newEvents)
            }
            setLoadingForm(false)
        })

        ApiHrCctcTree.get(res => {
            if (res.status === 200) {
                let newArr = convertNodes([res.data.data])
                setNodes(newArr)
            }
            setLoadingCctc(false)
        })
    }, [])

    function convertNodes(nodes) {
        nodes.forEach((node, index) => {
            if (node.children.length === 0) {
                delete node.children
                node['showCheckbox'] = false
                nodes[index] = node
            } else {
                nodes[index]['showCheckbox'] = false
                convertNodes(node.children)
            }
        })
        return nodes
    }

    const handleSubmit = (item) => {
        if (item) {
            setDay(item)
        }
        setLoadingForm(true)
        ApiWMCongViec.getCongViecCal({ day: ZenHelper.formatDateTime(item ? item : new Date(), 'YYYY/MM/DD') }, res => {
            if (res.status === 200) {
                var newEvents = []
                if (res.data.data.length > 0) {
                    res.data.data.forEach(event => {
                        let newevent = { ...event, start: new Date(event.start), end: new Date(event.end) }
                        newEvents.push(newevent)
                    })
                }
                setEvents(newEvents)
                setEventsOriginal(newEvents)
            }
            setLoadingForm(false)
        })
    }

    const debounceDateView = useCallback(debounce((item) => handleSubmit(item), 1000), [])

    const handleChange = (item) => {
        debounceDateView(item)
    }

    const handleChangeView = (e) => {
        let tp = 'W'
        var _switch = {
            'month': 'M',
            'week': 'W',
            'day': 'D',
        };
        tp = _switch[e]
        setType(tp)
    }

    const popoverButtonClickHandler = (e, event) => {
        //handle button click
        setEvent(event.event)
        setOpen(true)
    }
    const onChangeChk = (data, item, index) => {
        const { checked } = data
        if (!checked) {
            setCheckall(false)
            loaicv[index].checked = checked
            setLoaicv(loaicv)
            const newEvents = events.filter(t => t.ma_loai_cv !== item.ma_loai_cv)
            setEvents(newEvents)

        }
        if (checked) {
            loaicv[index].checked = checked
            setLoaicv(loaicv)
            const newItemEvent = eventsOriginal.filter(t => t.ma_loai_cv === item.ma_loai_cv)
            setEvents(events.concat(newItemEvent))
            let checkall = !loaicv.some(el => { return el.checked === false })
            setCheckall(checkall)

        }
    }

    const handleCheckAll = (data) => {
        const { checked } = data
        if (checked) {
            loaicv.forEach(x => x.checked = true)
            setEvents([].concat(eventsOriginal))
            setCheckall(true)
        }
        if (!checked) {
            loaicv.forEach(x => x.checked = false)
            setEvents([])
            setCheckall(false)
        }
    }

    const onChangeChkCVCT = async (data, item, index) => {
        const { checked } = data
        if (!checked) {
            setCheckallCVCT(false)
            congvieccuatoi[index].checked = checked
            setCongVieccuatoi([].concat(congvieccuatoi))
            let _congviecuatoi = congvieccuatoi.filter(x => x.checked === true)
            if (_congviecuatoi.length > 0) {
                let type = _.join(Object.keys(_congviecuatoi.reduce((r, { ma }) => (r[ma] = '', r), {})), ',')
                let _events = await getMyCongViec(type)
                setEvents([].concat(_events))
            } else {
                setEvents([])
            }
            //setEvents(newEvents)

        }
        if (checked) {
            congvieccuatoi[index].checked = checked
            setCongVieccuatoi(congvieccuatoi)
            let _congviecuatoi = congvieccuatoi.filter(x => x.checked === true)
            if (_congviecuatoi.length > 0) {
                let type = _.join(Object.keys(_congviecuatoi.reduce((r, { ma }) => (r[ma] = '', r), {})), ',')
                let _events = await getMyCongViec(type)
                setEvents([].concat(_events))
            } else {
                setEvents([])
            }
            let checkall = !congvieccuatoi.some(el => { return el.checked === false })
            setCheckallCVCT(checkall)
        }
    }

    function getMyCongViec(data) {
        let result = new Promise((resolve, reject) => {
            setLoadingForm(true)
            ApiWMCongViec.getMyCongViec({ type: data, day: day || new Date() }, res => {
                if (res.status === 200) {
                    resolve(res.data.data)
                } else {
                    reject(res)
                }
                setLoadingForm(false)
            })
        })
        return result
    }

    const handleCheckAllCVCT = (data) => {
        const { checked } = data
        if (checked) {
            congvieccuatoi.forEach(x => x.checked = true)
            setEvents([].concat(eventsOriginal))
            setCheckallCVCT(true)
        }
        if (!checked) {
            congvieccuatoi.forEach(x => x.checked = false)
            setEvents([])
            setCheckallCVCT(false)
        }
    }

    const handleOpenCloseModal = (formMode, item) => {
        if (!modalInfoCV) {
            ZenMessageAlert.error("Chưa khai báo thông tin modal")
            return
        }

        setModalInfoCV({
            ...modalInfoCV,
            info: {
                ...modalInfoCV.info,
                initItem: { ...modalInfoCV.info.initItem, id: item?.id },
                checkPermission: (permiss) => auth.checkPermission(permiss)
            },
            id: item ? item.id : "",
            formMode: formMode,
            open: true,
            other: {
                type: 'ma_kh'
            }
        })
    }

    const handleAfterSaveModal = (newItem, { mode }) => {
        // change
        ApiWMCongViec.getCongViecCal({ day: ZenHelper.formatDateTime(day ? day : new Date(), 'YYYY/MM/DD') }, res => {
            if (res.status === 200) {
                var newEvents = []
                if (res.data.data.length > 0) {
                    res.data.data.forEach(event => {
                        let newevent = { ...event, start: new Date(event.start), end: new Date(event.end) }
                        newEvents.push(newevent)
                    })
                }
                setEvents(newEvents)
                setEventsOriginal(newEvents)
            }
            setModalInfoCV({
                ...modalInfoCV,
                id: "",
                formMode: FormMode.NON,
                open: false,
            })
        })
    }

    function getUrlCongVieclist() {
        return {
            pathname: `${routes.WMCongViecList}`,
        }
    }
    function getUrlCongViecMonth() {
        return {
            pathname: `${routes.WMCongViec}`,
        }
    }

    const handleOpenCVDetail = (e, item) => {
        e.preventDefault();
        setOpenModalDetailCongViec(true)
        setItemcv(item)
    }

    const handleSelect = ({ start, end }) => {
        if (!modalInfoCV) {
            ZenMessageAlert.error("Chưa khai báo thông tin modal")
            return
        }
        setModalInfoCV({
            ...modalInfoCV,
            info: {
                ...modalInfoCV.info,
                initItem: { ...modalInfoCV.info.initItem, ngay_bd: start, ngay_kt: end },
                checkPermission: (permiss) => auth.checkPermission(permiss)
            },
            id: "",
            formMode: FormMode.ADD,
            open: true,
            other: {
                type: 'ma_kh'
            }
        })
    }

    // Drag Drop

    const dragFromOutsideItem = () => {
        return events
    }

    const moveEvent = ({ event, start, end, isAllDay: droppedOnAllDaySlot }) => {

        let allDay = event.allDay

        if (!event.allDay && droppedOnAllDaySlot) {
            allDay = true
        } else if (event.allDay && !droppedOnAllDaySlot) {
            allDay = false
        }

        const nextEvents = events.map(existingEvent => {
            return existingEvent.id == event.id
                ? { ...existingEvent, start, end, allDay }
                : existingEvent
        })

        const patchData = [
            {
                path: "/ngay_bd",
                op: "replace",
                operationType: 0,
                value: start
            },
            {
                path: "/ngay_kt",
                op: "replace",
                operationType: 0,
                value: end
            },
        ]
        setLoadingForm(true)
        ApiWMCongViec.updatePatch(event.id, patchData, res => {
            if (res.status === 200) {
                setEvents(nextEvents)
            } else {
                ZenMessageAlert.error(zzControlHelper.getResponseError(res))
            }
            setLoadingForm(false)
        })

        // alert(`${event.title} was dropped onto ${updatedEvent.start}`)
    }

    const onDropFromOutside = ({ start, end, allDay }) => {

        const event = {
            id: draggedEvent.id,
            title: draggedEvent.title,
            start,
            end,
            allDay: allDay,
        }
        setDraggedEvent(null)
        moveEvent({ event, start, end })
    }

    //Resize
    const resizeEvent = ({ event, start, end }) => {
        const nextEvents = events.map(existingEvent => {
            return existingEvent.id == event.id
                ? { ...existingEvent, start, end }
                : existingEvent
        })

        const patchData = [
            {
                path: "/ngay_bd",
                op: "replace",
                operationType: 0,
                value: start
            },
            {
                path: "/ngay_kt",
                op: "replace",
                operationType: 0,
                value: end
            },
        ]
        setLoadingForm(true)
        ApiWMCongViec.updatePatch(event.id, patchData, res => {
            if (res.status === 200) {
                setEvents(nextEvents)
            } else {
                ZenMessageAlert.error(zzControlHelper.getResponseError(res))
            }
            setLoadingForm(false)
        })
    }

    const handleCheckCctc = (checked, node) => {
        setChecedCctc(checked)
    }

    const handleExpandedCctc = (expanded) => {
        setExpandedCCtc(expanded)
    }

    const handleClickCctc = (e) => {
        let item = {}
        if (e.parent) {
            item = e.parent?.children?.filter(x => x.value === e.value)[0]
        }
        setLoadingForm(true)
        ApiWMCongViec.getMyCongViec({ type: item?.type, day: day || new Date(), id: e.value }, res => {
            if (res.status === 200) {
                var newEvents = []
                if (res.data.data.length > 0) {
                    res.data.data.forEach(event => {
                        let newevent = { ...event, start: new Date(event.start), end: new Date(event.end) }
                        newEvents.push(newevent)
                    })
                }
                setEvents(newEvents)
                setEventsOriginal(newEvents)
            } else {
                ZenMessageAlert.warning(zzControlHelper.getResponseError(res))
            }
            setLoadingForm(false)
        })
    }

    return <>
        <div style={{ display: "flex", justifyContent: "space-between", marginBottom: "10px" }}>
            <PageHeader />
            <div>
                <ZenButton
                    btnType="refresh"
                    onClick={(e) => handleSubmit()}
                    size="small"
                />
                <ZenButton btnType="add"
                    permission={permissions.WMCongViecThem}
                    onClick={(e) => handleOpenCloseModal(FormMode.ADD)}
                    size="small"
                />
                <Button.Group size="small">
                    <Link to={getUrlCongVieclist()} className=" ui button basic small primary icon">
                        <Icon name="list" />
                    </Link>
                    <Link to={getUrlCongViecMonth()} className=" ui button small primary icon">
                        <Icon name="calendar alternate outline" />
                    </Link>
                </Button.Group>
            </div>
        </div>
        <Segment>
            <Grid className="two column ">
                <Grid.Column style={{ flex: " 0 0 350px" }}>
                    <Segment style={{ height: "105vh" }}>
                        <div>
                            <div>
                                <Header content={`Công việc của tôi`} />
                                <Checkbox
                                    label={`Tất cả loại công việc`}
                                    name={`checboxall`}
                                    checked={checkallCVCT}
                                    onClick={(evt, data) => handleCheckAllCVCT(data)}
                                />
                                {congvieccuatoi.map((item, index) => {
                                    return <div key={`${index}${item.ma}`}>
                                        <Form.Group style={{ padding: "5px 0px" }}>
                                            <Checkbox
                                                label={`${item.ten_loai_cv}`}
                                                name={`checbox${index}${item.ma}`}
                                                checked={item.checked}
                                                onClick={(evt, data) => onChangeChkCVCT(data, item, index)}
                                            />
                                        </Form.Group>
                                    </div>
                                })}
                            </div>
                            {(auth.checkPermission("45.03.6") || auth.checkPermission("45.03.7")) && <div style={{ paddingTop: "20px" }}>
                                <Header content={`Công việc của nhân viên`} />
                                <div style={{
                                    height: "auto",
                                    maxHeight: "250px",
                                    overflowX: "auto",
                                    overflowY: "auto",
                                }}>
                                     <ZenLoading loading={loadingCctc} inline="centered" dimmerPage={false} />
                                    <CheckboxTree
                                        nodes={nodes}
                                        checked={checkedCctc}
                                        expanded={expandedCctc}
                                        //onCheck={(checked, node) => handleCheckCctc(checked, node)}
                                        onExpand={expanded => handleExpandedCctc(expanded)}
                                        onClick={(e) => handleClickCctc(e)}

                                        icons={{
                                            check: <Icon style={{ margin: 0 }} name="check square outline" />,
                                            uncheck: <Icon style={{ margin: 0 }} name="square outline" />,
                                            halfCheck: <Icon style={{ margin: 0 }} name="check square outline" />,
                                            expandClose: <Icon style={{ margin: 0 }} name="chevron right" />,
                                            expandOpen: <Icon style={{ margin: 0 }} name="chevron down" />,
                                            expandAll: <Icon style={{ margin: 0 }} name="plus square outline" />,
                                            collapseAll: <Icon style={{ margin: 0 }} name="minus square outline" />,
                                            parentClose: <Icon name="sitemap" />,
                                            parentOpen: <Icon name="sitemap" />,
                                            leaf: <Icon name="address card" />,
                                        }}
                                    />
                                </div>
                            </div>}
                            <div style={{ paddingTop: "20px" }}>
                                <Header content={`Loại công việc`} />
                                <Checkbox
                                    label={`Tất cả`}
                                    name={`checboxall`}
                                    checked={checkall}
                                    onClick={(evt, data) => handleCheckAll(data)}
                                />
                                {loaicv.map((item, index) => {
                                    return <div key={`${index}${item.ma}`}>
                                        <Form.Group style={{ padding: "5px 0px" }}>
                                            <Checkbox
                                                label={`${item.ten_loai_cv}`}
                                                name={`checbox${index}${item.ma}`}
                                                checked={item.checked}
                                                onClick={(evt, data) => onChangeChk(data, item, index)}
                                            />
                                        </Form.Group>
                                    </div>
                                })}
                            </div>
                        </div>
                    </Segment>
                </Grid.Column>
                <Grid.Column style={{ flex: "1" }}>
                    <Segment>
                        <ZenLoading loading={loadingForm} inline="centered" dimmerPage={false} />
                        <DragAndDropCalendar
                            localizer={localizer}
                            defaultDate={new Date}
                            default={() => new Date(day)}
                            defaultView="month"
                            events={events}
                            style={{ height: "100vh" }}
                            views={{ month: true, week: true, work_week: true, day: true, agenda: true }} // 'month','week', 'day', 'agenda'
                            popup
                            components={{
                                event: CustomEventContainer({
                                    onPopoverButtonClick: popoverButtonClickHandler,
                                    handleOpenCVDetail: handleOpenCVDetail
                                })
                            }}
                            date={day}
                            onNavigate={date => {
                                handleSubmit(date)
                            }}
                            selectable
                            onSelectSlot={(e) => handleSelect(e)}

                            resizable
                            onEventResize={resizeEvent}
                            onEventDrop={moveEvent}

                            dragFromOutsideItem={dragFromOutsideItem}
                            onDropFromOutside={onDropFromOutside}

                            onView={view => setCurrentView(view)}
                            resizableAccessor={() => currentView !== 'month'}
                        //draggableAccessor={() => currentView !== 'month'}
                        />
                    </Segment>
                </Grid.Column>
            </Grid>
        </Segment>
        {
            open && <ModalEvent open={open}
                event={event}
                onClose={() => setOpen(false)}
            />
        }
        {
            zzControlHelper.openModalComponent(modalInfoCV,
                {
                    onClose: () => handleOpenCloseModal(FormMode.NON),
                    onAfterSave: handleAfterSaveModal,
                }
            )
        }
        {
            openModalDetailCongviec && <ModalCongViecDetail
                open={openModalDetailCongviec}
                modalInfoCV={modalInfoCV}
                onClose={() => { setOpenModalDetailCongViec(false); setItemcv(); handleSubmit(day) }}
                cvItem={{ ...itemcv }}
                type={'MA_KH'}
                valueKey={itemcv.ma_kh}
                keyData={'ma_kh'}
                fileCodeName={"CRM"}
            />
        }
    </>
}

const CustomEventContainer = ({ onPopoverButtonClick, handleOpenCVDetail }) => props => {
    return <CustomEvent handleOpenCVDetail={handleOpenCVDetail} event={props} onPopoverButtonClick={onPopoverButtonClick} />;
}

const ModalEvent = (props) => {
    const { event, open, onClose, } = props
    const [loadingForm, setLoadingForm] = useState(false)
    const [data, setData] = useState()
    useEffect(() => {
        const { id } = event
        setLoadingForm(true)
        ApiWMCongViec.getCongViecCal({ day: new Date() }, res => {
            if (res.status === 200) {
                setData(res.data.data)
            }
            setLoadingForm(false)
        })
    }, [])
    return <>
        <Modal id="event-form"
            closeOnEscape closeIcon closeOnDimmerClick={false}
            onClose={() => onClose()}
            //onOpen={() => onClose()}
            open={open}
            size={"tiny"}
        >
            {/* <Modal.Header id='header-tailieu-form' style={{ cursor: "grabbing" }}>
            </Modal.Header> */}
            <Modal.Content>
                {/* <ZenLoading loading={true} inline="centered" content={"loading..."} /> */}
                <Table basic='very'>
                    <Table.Body>
                        <Table.Row >
                            <Table.Cell textAlign="right" collapsing>Tên trường :</Table.Cell>
                            <Table.Cell style={{ fontWeight: "bold", whiteSpace: "" }}>
                                {data?.ten_kh}
                            </Table.Cell>
                        </Table.Row>
                        <Table.Row >
                            <Table.Cell textAlign="right" collapsing>Giáo viên :</Table.Cell>
                            <Table.Cell style={{ fontWeight: "bold", whiteSpace: "" }}>
                                {data?.ten_nv}
                            </Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell textAlign="right" collapsing>Ngày :</Table.Cell>
                            <Table.Cell style={{ fontWeight: "bold", whiteSpace: "" }}>
                                {`${zzControlHelper.formatDateTime(data?.ngay, 'DD/MM/YYYY')} `}&nbsp;&nbsp;&nbsp;{`     
                                ${zzControlHelper.formatDateTime(data?.tu, 'HH:mm')} - ${zzControlHelper.formatDateTime(data?.den, 'HH:mm')}`}
                            </Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell textAlign="right" collapsing>Ghi chú :</Table.Cell>
                            <Table.Cell style={{ fontWeight: "bold", whiteSpace: "textarea" }}>
                                {data?.ghi_chu}
                            </Table.Cell>
                        </Table.Row>
                    </Table.Body>
                </Table>
            </Modal.Content>
            <Modal.Actions>
                <ZenButton btnType={"cancel"} size="small" onClick={() => onClose()} />
            </Modal.Actions>
        </Modal>
    </>
}

const CustomEvent = React.memo((props) => {
    const { event } = props
    //...
    return (
        <div onClick={(e) => props.handleOpenCVDetail(e, event.event)}
            style={{
                fontSize: "0.8em",
                fontWeight: "800",
                textDecorationLine: event.event.isclose ? 'line-through' : 'none',
            }}
        >
            {event.title}
        </div>
    );
})

const initItem = {
    ngay: new Date(),
    type: "nv",
    idnv: "",
    makh: "",
}

const PageHeader = ({ item }) => {
    return <div style={{}}>
        <Helmet idMessage={"PMDuAn.detail"}
            defaultMessage={"Lịch công việc"} />
        <SegmentHeader>
            <HeaderLink >
                <Breadcrumb.Divider icon='right chevron' />
                <Breadcrumb.Section active>
                    Lịch công việc
                </Breadcrumb.Section>
            </HeaderLink>
        </SegmentHeader>
        <br />
    </div>
}

export const WMCongViecCal = {
    WMCongViecCalform: {
        route: routes.WMCongViec,
        Zenform: WMCongViecCalForm,
        action: {
            view: { visible: true, permission: permissions.WMCongViecXem, },
        },
    },
}
