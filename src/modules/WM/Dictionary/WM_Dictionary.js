import React from 'react'
import { Button, Icon, Table } from 'semantic-ui-react';
import * as routes from '../../../constants/routes';
import { ZenLookup } from '../../ComponentInfo/Dictionary/ZenLookup';
import { ApiWMCongViec, ApiWMDmGiaiDoanCV, ApiWMDmLoaiCV, ApiWMDmUuTienCV } from '../Api';
import * as permissions from "../../../constants/permissions";
import { Link } from 'react-router-dom';
import { GlobalStorage, KeyStorage } from "../../../utils/storage";
import { ZenHelper } from '../../../utils';

const optionMaForm = ZenHelper.translateListToSelectOptions(GlobalStorage.getByField(KeyStorage.CacheData, 'ma_form')?.data, false, 'ma_form', 'ma_form', 'ma_form', [], 'ten_form')

const WM_Dictionary = {
    WMDmLoaiCV: {
        route: routes.WMDmLoaiCV,

        action: {
            view: { visible: true, permission: permissions.WMDmLoaiCvXem },
            add: { visible: true, permission: permissions.WMDmLoaiCvThem },
            edit: { visible: true, permission: permissions.WMDmLoaiCvSua },
            del: { visible: true, permission: permissions.WMDmLoaiCvXoa },
        },

        linkHeader: {
            id: 'wmdmloaicv',
            defaultMessage: "Danh mục loại công việc",
            active: true,
            isSetting: true,
        },

        tableList: {
            keyForm: "WMDmLoaiCV",
            formModal: ZenLookup.WMDmLoaiCV,
            fieldCode: "ma_loai_cv",
            unPagination: false,
            duplicate: true,
            showFilterLabel: true,

            api: {
                url: ApiWMDmLoaiCV,
                type: "sql",
            },
            columns: [
                { id: "wmdmloaicv.ma_loai_cv", defaultMessage: "Mã loại", fieldName: "ma_loai_cv", filter: "string", sorter: true, editForm: true },
                { id: "wmdmloaicv.ten_loai_cv", defaultMessage: "Tên loại", fieldName: "ten_loai_cv", filter: "string", sorter: true, },
                { id: "wmdmloaicv.phai_bc", defaultMessage: "Phải báo cáo", fieldName: "phai_bc", filter: "bool", sorter: true, },
                { id: "wmdmloaicv.ma_form_bc", defaultMessage: "Mẫu form báo cáo", fieldName: "ma_form_bc", filter: "list", sorter: true, custom: true, listFilter: optionMaForm },
                { id: "PMDmGiaiDoanDa.ordinal", defaultMessage: "Thứ tự hiển thị", fieldName: "ordinal", filter: "number", sorter: true, },
                { id: "ksd", defaultMessage: "Không sử dụng", fieldName: "ksd", type: "bool", filter: "", sorter: true, collapsing: true },
            ],
            customCols: (data, item, fieldName, { onEdit, ...restCol }) => {
                let customCell = undefined
                if (fieldName === 'ma_form_bc') {
                    const loai = optionMaForm.find(t => t.value == item["ma_form_bc"]) || {}
                    customCell = <Table.Cell key={fieldName}>
                        {loai.text || item["ten_form_bc"]}
                    </Table.Cell>
                }
                return customCell
            }
        }
    },

    WMDmGiaiDoanCV: {
        route: routes.WMDmGiaiDoanCV,

        action: {
            view: { visible: true, permission: permissions.WMDmGiaiDoanCvXem },
            add: { visible: true, permission: permissions.WMDmUuTienCvThem },
            edit: { visible: true, permission: permissions.WMDmGiaiDoanCvSua },
            del: { visible: true, permission: permissions.WMDmLoaiCvXoa },
        },

        linkHeader: {
            id: 'wmdmgiaidoancv',
            defaultMessage: "Danh mục giai đoạn công việc",
            active: true,
            isSetting: true,
        },

        tableList: {
            keyForm: "WMDmGiaiDoanCV",
            formModal: ZenLookup.WMDmGiaiDoanCV,
            fieldCode: "ma_gdoan_cv",
            unPagination: false,
            duplicate: true,
            showFilterLabel: true,

            api: {
                url: ApiWMDmGiaiDoanCV,
                type: "sql",
            },
            columns: [
                { id: "WMDmGiaiDoanCV.ma_gdoan_cv", defaultMessage: "Mã giai đoạn", fieldName: "ma_gdoan_cv", filter: "string", sorter: true, editForm: true },
                { id: "WMDmGiaiDoanCV.ten_gdoan_cv", defaultMessage: "Tên giai đoạn", fieldName: "ten_gdoan_cv", filter: "string", sorter: true, },
                { id: "WMDmGiaiDoanCV.ordinal", defaultMessage: "Thứ tự", fieldName: "ordinal", filter: "string", sorter: true, },
                { id: "WMDmGiaiDoanCV.color", defaultMessage: "Màu đại diện", fieldName: "color", filter: "string", sorter: true, custom: true },
                { id: "PMDmGiaiDoanDa.isclose", defaultMessage: "Đóng công việc", fieldName: "isclose", filter: "bool", sorter: true, },
                { id: "ksd", defaultMessage: "Không sử dụng", fieldName: "ksd", type: "bool", filter: "", sorter: true, collapsing: true },
            ],
            customCols: (data, item, fieldName, { onEdit, ...restCol }) => {
                let customCell = undefined
                if (fieldName === 'color') {
                    customCell = <Table.Cell key={fieldName} singleLine>
                        <div class="ui label" style={{ backgroundColor: item.color ? item.color : '#000000', color: item.color ? item.color : '#000000' }}>__________</div>
                    </Table.Cell>
                }
                return customCell
            }
        }
    },

    WMDmUuTienCV: {
        route: routes.WMDmUuTienCV,

        action: {
            view: { visible: true, permission: permissions.WMDmUuTienCvXem },
            add: { visible: true, permission: permissions.WMDmUuTienCvThem },
            edit: { visible: true, permission: permissions.WMDmUuTienCvSua },
            del: { visible: true, permission: permissions.WMDmGiaiDoanCvXoa },
        },

        linkHeader: {
            id: 'wmdmUuTiencv',
            defaultMessage: "Danh mục mức độ ưu tiên",
            active: true,
            isSetting: true,
        },

        tableList: {
            keyForm: "WMDmUuTienCV",
            formModal: ZenLookup.WMDmUuTienCV,
            fieldCode: "ma_uu_tien",
            unPagination: false,
            duplicate: true,
            showFilterLabel: true,

            api: {
                url: ApiWMDmUuTienCV,
                type: "sql",
            },
            columns: [
                { id: "wmdmloaicv.ma_uu_tien", defaultMessage: "Mã mức ưu tiên", fieldName: "ma_uu_tien", filter: "string", sorter: true, editForm: true },
                { id: "wmdmloaicv.ten_uu_tien", defaultMessage: "Tên mức ưu tiên", fieldName: "ten_uu_tien", filter: "string", sorter: true, },
                { id: "wmdmloaicv.diem", defaultMessage: "Điểm ưu tiên", fieldName: "diem", filter: "string", sorter: true, },
                { id: "ksd", defaultMessage: "Không sử dụng", fieldName: "ksd", type: "bool", filter: "", sorter: true, collapsing: true },
            ]
        }
    },

    WMCongViecList: {
        route: routes.WMCongViecList,

        action: {
            view: { visible: true, permission: permissions.WMCongViecXem },
            add: { visible: true, permission: permissions.WMCongViecThem },
            edit: { visible: true, permission: permissions.WMCongViecSua },
            del: { visible: true, permission: permissions.WMCongViecXoa },
        },

        linkHeader: {
            id: 'wmcongviec',
            defaultMessage: "Công việc",
            active: true,
            isSetting: false,
        },

        CustomBtnHeader: (e) => {
            const { btnAdd, btnRefresh } = e;

            function getUrlCongVieclist() {
                return {
                    pathname: `${routes.WMCongViecList}`,
                }
            }
            function getUrlCongViecMonth() {
                return {
                    pathname: `${routes.WMCongViec}`,
                }
            }

            return <>
                {btnRefresh}
                <div style={{ display: "inline-block", marginLeft: '3px', marginRight: '3px' }}>
                    {btnAdd}
                </div>
                <Button.Group size="small">
                    <Link to={getUrlCongVieclist()} className=" ui button small primary icon">
                        <Icon name="list" />
                    </Link>
                    <Link to={getUrlCongViecMonth()} className=" ui button basic small primary icon">
                        <Icon name="calendar alternate outline" />
                    </Link>
                </Button.Group>
            </>
        },

        tableList: {
            keyForm: "wmcongviec",
            formModal: ZenLookup.WMCongViec,
            fieldCode: "id",
            unPagination: false,
            duplicate: true,
            showFilterLabel: true,

            api: {
                url: ApiWMCongViec,
                type: "sql",
            },
            columns: [
                { id: "wmdmloaicv.ten_cong_viec", defaultMessage: "Tên công việc", fieldName: "ten_cong_viec", filter: "string", sorter: true, editForm: true },
                { id: "wmdmloaicv.ten_loai_cv", defaultMessage: "Loại công việc", fieldName: "ten_loai_cv", filter: "string", sorter: true, },
                { id: "wmdmloaicv.gdoan_cv", defaultMessage: "Giai đoạn", fieldName: "ten_gdoan_cv", filter: "string", sorter: true, },
                { id: "ksd", defaultMessage: "Không sử dụng", fieldName: "ksd", type: "bool", filter: "", sorter: true, collapsing: true },
            ]
        }

    }
}

export default WM_Dictionary