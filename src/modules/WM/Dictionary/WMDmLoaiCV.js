import React, { useEffect } from "react";
import { ZenField, ZenFieldSelectApi, ZenFieldTextArea, ZenFieldCheckbox,ZenFieldNumber } from "../../../components/Control/index";
import { FormMode } from "../../../utils";
import { Form } from "semantic-ui-react";
import { ApiWMDmLoaiCV } from "../Api";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import * as permissions from "../../../constants/permissions";

const WMDmLoaiCVModal = (props) => {
   const { formik, permission, mode } = props

   const handleItemSelected = (option, { name }) => {
      if (name === "ma_form_bc") {
         formik.setFieldValue("ten_form_bc", option.ten_form)
      }
   }

   return <React.Fragment>
      <Form.Group widths="equal">
        <ZenField  required autoFocus readOnly={!permission || (mode === FormMode.ADD ? false : true)}
            label={"WMDmLoaiCV.ma_loai_cv"} defaultlabel="Mã loại công việc"
            width={8}
            name="ma_loai_cv" props={formik}
            isCode
        />
        <ZenFieldNumber
            readOnly={!permission}
            label={"PMDmGiaiDoanDa.ordinal"} defaultlabel="Thứ tự hiển thị"
            name="ordinal" props={formik}
         />
        </Form.Group>
        <ZenField required readOnly={!permission}
            label={"WMDmLoaiCV.ten_loai_cv"} defaultlabel="Tên loại công việc"
            name="ten_loai_cv" props={formik}
        />
        <ZenFieldCheckbox 
            readOnly={!permission}
            label={"WMDmLoaiCV.phai_pc"} defaultlabel="Công việc yêu cầu phải báo cáo kết quả"
            name="phai_bc"
            props={formik}
        />
      <ZenFieldSelectApi 
         readOnly={!permission}
         lookup={{...ZenLookup.SIDmForm,format:`{ten_form}`}}
         label={"WMDmLoaiCV.ma_form_bc"} defaultlabel="Mẫu form báo cáo kết quả"
         name="ma_form_bc" formik={formik} 
         onItemSelected={handleItemSelected}
      />
   </React.Fragment>
}

const WMDmLoaiCV = {
   FormModal: WMDmLoaiCVModal,
   api: {
      url: ApiWMDmLoaiCV,
   },
   permission: {
      view: permissions.WMDmLoaiCvXem,
      add: permissions.WMDmLoaiCvThem,
      edit: permissions.WMDmLoaiCvSua
   },
   formId: "WMDmLoaiCV-form",
   size: "tiny",
   isScrolling: false,
   initItem: {
      ma_loai_cv: "",
      ten_loai_cv: "",
      phai_bc: false,
      ma_form_bc: "",
      ten_form_bc: "",
      ordinal:0,
      ksd: false
   },
   formValidation: [
      {
         id: "ma_loai_cv",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
            // {
            //    type: "max",
            //    params: [8, "Không được vượt quá 8 kí tự"]
            // },
         ]
      },
      {
         id: "ten_loai_cv",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
         ]
      },
   ]
}

export { WMDmLoaiCV };