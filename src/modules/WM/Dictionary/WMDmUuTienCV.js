import React, { useEffect } from "react";
import { ZenField, ZenFieldNumber, ZenFieldSelectApi, ZenFieldTextArea, ZenFieldCheckbox } from "../../../components/Control/index";
import { FormMode } from "../../../utils";
import { Form } from "semantic-ui-react";
import { ApiWMDmUuTienCV } from "../Api";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import * as permissions from "../../../constants/permissions";

const WMDmUuTienCVModal = (props) => {
   const { formik, permission, mode } = props

   return <React.Fragment>
      <ZenField width={8} required autoFocus readOnly={!permission || (mode === FormMode.ADD ? false : true)}
         label={"WMDmGiaiDoanCV.ma_uu_tien"} defaultlabel="Mã mức ưu tiên"
         name="ma_uu_tien" props={formik}
         isCode
      />
      <ZenField required readOnly={!permission}
         label={"WMDmGiaiDoanCV.ten_uu_tien"} defaultlabel="Tên mức ưu tiên"
         name="ten_uu_tien" props={formik}
      />
      <ZenFieldNumber
         readOnly={!permission} width={6}
         label={"WMDmGiaiDoanCV.diem"} defaultlabel="Điểm"
         name="diem" props={formik}
      />
   </React.Fragment>
}

const WMDmUuTienCV = {
   FormModal: WMDmUuTienCVModal,
   api: {
      url: ApiWMDmUuTienCV,
   },
   permission: {
      view: permissions.WMDmUuTienCvXem,
      add: permissions.WMDmUuTienCvThem,
      edit: permissions.WMDmGiaiDoanCvSua,
   },
   formId: "WMDmUuTienCV-form",
   size: "tiny",
   isScrolling: false,
   initItem: {
      ma_uu_tien: "",
      ten_uu_tien: "",
      diem: 0,
      ksd: false
   },
   formValidation: [
      {
         id: "ma_uu_tien",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
            // {
            //    type: "max",
            //    params: [8, "Không được vượt quá 8 kí tự"]
            // },
         ]
      },
      {
         id: "ten_uu_tien",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
         ]
      },
      {
         id: "diem",
         validationType: "number",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
            {
               type: "min",
               params: [1, "Không được nhỏ hơn 1"],
            },
         ],
      },
   ]
}

export { WMDmUuTienCV };