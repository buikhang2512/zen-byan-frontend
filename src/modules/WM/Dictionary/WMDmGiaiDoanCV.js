import React, { useEffect } from "react";
import { ZenField, ZenFieldSelectApi, ZenFieldTextArea, ZenFieldNumber, ZenFieldCheckbox } from "../../../components/Control/index";
import { FormMode } from "../../../utils";
import { Form } from "semantic-ui-react";
import { ApiWMDmGiaiDoanCV } from "../Api";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import * as permissions from "../../../constants/permissions";

const styles = {
   choosecolors: {
      width: '100%',
      margin: '10px',
   }
}

const WMDmGiaiDoanCVModal = (props) => {
   const { formik, permission, mode } = props

   const onChangecolor = (e) => {
      formik.setFieldValue('color', e.target.value)
   }

   return <React.Fragment>
      <Form.Group widths="equal">
         <ZenField width={8} required autoFocus readOnly={!permission || (mode === FormMode.ADD ? false : true)}
            label={"WMDmGiaiDoanCV.ma_gdoan_cv"} defaultlabel="Mã giai đoạn"
            name="ma_gdoan_cv" props={formik}
            isCode
         />
          <ZenFieldNumber
         readOnly={!permission}
         label={"WMDmGiaiDoanCV.ordinal"} defaultlabel="Thứ tự hiển thị"
         name="ordinal" props={formik}
      />
      </Form.Group>
      <ZenField required readOnly={!permission}
         label={"WMDmGiaiDoanCV.ten_gdoan_cv"} defaultlabel="Tên giai đoạn"
         name="ten_gdoan_cv" props={formik}
      />
      <ZenFieldCheckbox readOnly={!permission}
         name="isclose" props={formik}
         label={"WMDmGiaiDoanCV.isclosed"} defaultlabel="Đóng công việc"
      />
      <Form.Group>
         <Form.Field>
            <label>Màu đại diện</label>
            <div style={{...styles.choosecolors}}>
            <input value={formik.values.color} type="color" onChange={onChangecolor}/>
            </div>
      </Form.Field>
      </Form.Group>
   </React.Fragment>
}

const WMDmGiaiDoanCV = {
   FormModal: WMDmGiaiDoanCVModal,
   api: {
      url: ApiWMDmGiaiDoanCV,
   },
   permission: {
      view: permissions.WMDmGiaiDoanCvXem,
      add: permissions.WMDmLoaiCvThem,
      edit: permissions.WMDmGiaiDoanCvSua
   },
   formId: "WMDmGiaiDoanCV-form",
   size: "tiny",
   isScrolling: false,
   initItem: {
      ma_gdoan_cv: "",
      ten_gdoan_cv: "",
      ordinal: "",
      isclosed: false,
      ksd: false
   },
   formValidation: [
      {
         id: "ma_gdoan_cv",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
            // {
            //    type: "max",
            //    params: [8, "Không được vượt quá 8 kí tự"]
            // },
         ]
      },
      {
         id: "ten_gdoan_cv",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
         ]
      },
      {
         id: "ordinal",
         validationType: "number",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
         ]
      },
   ]
}

export { WMDmGiaiDoanCV };