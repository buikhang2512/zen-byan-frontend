import React, { useEffect } from "react";
import { ZenField, ZenFieldSelectApi, ZenFieldTextArea, ZenFieldDate, ZenFieldSelectApiMulti } from "../../../components/Control/index";
import { auth, ZenHelper } from "../../../utils";
import { Form } from "semantic-ui-react";
import { ApiWMCongViec } from "../Api";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { FormMode } from "../../../components/Control/zzControlHelper";
import * as permissions from "../../../constants/permissions";

const FormModal = (props) => {
    const { formik, permission, mode, infoModal } = props
    const { other } = infoModal

    useEffect(() => {
        if (mode === 1) {
            let user = auth.getUserInfo()
            formik.setFieldValue("assignees", user.hrid)
        }
    }, [])

    useEffect(() => {
        let assigneesArr = formik.values.assignees
        if (assigneesArr && Array.isArray(assigneesArr)) {
            const assignees = assigneesArr.map(t => t.id_nv).toString();
            formik.setFieldValue("assignees", assignees)
            formik.setFieldValue("assigneesArr", assigneesArr)
        }

        let followersArr = formik.values.followers
        if (followersArr && Array.isArray(followersArr)) {
            const followers = followersArr.map(t => t.id_nv).toString();
            formik.setFieldValue("followers", followers)
            formik.setFieldValue("followersArr", followersArr)
        }
    }, [formik.values.assignees, formik.values.followers])

    const handleSelectedItemMulti = (items, target) => {
        if (target.name === "assignees") {
            formik.setFieldValue("assigneesArr", items)
        } else if (target.name === "followers") {
            formik.setFieldValue("followersArr", items)
        }
    }

    return <React.Fragment>
        <Form.Group widths="equal">
            <ZenFieldSelectApi
                readOnly={!permission}
                lookup={ZenLookup.WMDmLoaiCV}
                label={"WMCongViec.ma_loai_cv"} defaultlabel="Loại công việc"
                name="ma_loai_cv" formik={formik}
                onItemSelected={(option) => {
                    formik.setFieldValue('ma_form_bc', option?.ma_form_bc)
                    formik.setFieldValue('phai_bc', option?.phai_bc)
                    formik.setFieldValue('ten_loai_cv', option.ten_loai_cv)
                }}
            />

            <ZenFieldSelectApi
                readOnly={!permission}
                lookup={ZenLookup.WMDmUuTienCV}
                label={"WMCongViec.ma_uu_tien"} defaultlabel="Mức ưu tiên"
                name="ma_uu_tien" formik={formik}
            />

            <ZenFieldSelectApi
                readOnly={!permission}
                lookup={ZenLookup.WMDmGiaiDoanCV}
                label={"WMCongViec.ma_gdoan_cv"} defaultlabel="Giai đoạn"
                name="ma_gdoan_cv" formik={formik}
            />
        </Form.Group>

        <ZenField readOnly={!permission}
            label={"WMCongViec.ten_cong_viec"} defaultlabel="Tên công việc"
            name="ten_cong_viec" props={formik}
        />

        <Form.Group widths="equal">
            <ZenFieldDate
                showTime readOnly={!permission}
                name="ngay_bd" props={formik}
                label={"WMCongViec.ngay_bd"} defaultlabel="Ngày thực hiện"
            />
            <ZenFieldDate
                showTime readOnly={!permission}
                name="ngay_kt" props={formik}
                label={"WMCongViec.ngay_kt"} defaultlabel="Ngày hoàn thành"
            />
        </Form.Group>

        <ZenFieldTextArea readOnly={!permission}
            label={"WMCongViec.noi_dung"} defaultlabel="Nội dung"
            name="noi_dung" props={formik}
        />

        <ZenFieldSelectApiMulti
            search={true} readOnly={!permission}
            lookup={{ ...ZenLookup.ID_NV, top: null }}
            name="assignees"
            label={"ardmkh.nv_follow"} defaultlabel="Người thực hiện"
            formik={formik}
            onSelectedItem={handleSelectedItemMulti}
        />

        <ZenFieldSelectApiMulti
            search={true} readOnly={!permission}
            lookup={{ ...ZenLookup.ID_NV, top: null }}
            name="followers"
            label={"ardmkh.nv_follow"} defaultlabel="Người theo dõi"
            formik={formik}
            onSelectedItem={handleSelectedItemMulti}
        />

        <ZenFieldSelectApi
            readOnly={!permission}
            // readOnly={other ? other.type.toUpperCase() === "MA_KH" || !permission : true}
            lookup={ZenLookup.Ma_kh}
            label={"WMCongViec.ma_kh"} defaultlabel="Khách hàng"
            name="ma_kh" formik={formik}
        />

        <ZenFieldSelectApi upward
            readOnly={other ? other.type.toUpperCase() === "MA_DEAL" || !permission : true}
            lookup={ZenLookup.CRMDeal}
            label={"WMCongViec.ma_deal"} defaultlabel="Deal/Cơ hội bán hàng"
            name="ma_deal" formik={formik}
        />

        <ZenFieldSelectApi upward
            readOnly={!permission}
            lookup={ZenLookup.PMDuAn}
            label={"WMCongViec.ma_du_an"} defaultlabel="Dự án"
            name="ma_du_an" formik={formik}
        />

        <ZenField readOnly
            label={"WMCongViec.ma_form_bc"} defaultlabel="Form báo cáo"
            name="ma_form_bc" props={formik}
        />

    </React.Fragment>
}

const WMCongViec = {
    FormModal: FormModal,
    customHeader: (props, state) => {
        const { items } = state
        const { formMode } = props

        if (formMode === FormMode.VIEW) {
            return items.ten_cong_viec
        } else if (formMode === FormMode.EDIT) {
            return "Sửa: " + items.ten_cong_viec
        } else if (formMode === FormMode.ADD) {
            return "Thêm mới công việc"
        }
    },
    api: {
        url: ApiWMCongViec,
    },
    permission: {
        view: permissions.WMCongViecXem,
        add: permissions.WMCongViecThem,
        edit: permissions.WMCongViecSua
    },
    formId: "WMCongViec-form",
    size: "small",
    fieldCode: "id",
    initItem: {
        ten_cong_viec: "",
        ma_loai_cv: "",
        ma_gdoan_cv: "",
        ma_uu_tien: "",
        ngay_bd: ZenHelper.formatDateTime(new Date(), "YYYY-MM-DD"),
        ngay_kt: ZenHelper.formatDateTime(new Date(), "YYYY-MM-DD"),
        ksd: false
    },
    formValidation: [
        {
            id: "ten_cong_viec",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ]
        },
    ]
}

export { WMCongViec };