import axios from '../../../Api/axios';

const ExtName = "WMCongViec"

export const ApiWMCongViec = {
   get(callback, filter = {}, pagination = {}) {
      axios.get(`${ExtName}`, {
         params: {
            qf: filter.qf,
            keyword: filter.keyword,
            sort: filter.sort,
            page: pagination.page,
            pageSize: pagination.pageSize
         },
      })
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   getByCode(code, callback) {
      axios.get(`${ExtName}/` + code)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   insert(data, callback) {
      axios.post(`${ExtName}`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   update(data, callback) {
      axios.put(`${ExtName}`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   updatePatch(code, data, callback) {
      axios.patch(`${ExtName}/${code}`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   delete(code, callback) {
      axios.delete(`${ExtName}/` + code)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   insertNS(data, callback) {
      const { id_cviec } = data
      //{id_cviec, loai, id_nv}
      axios.post(`${ExtName}/${id_cviec}/ns`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   deleteNS(data, callback) {
      const { id_cviec, id_nv, loai } = data
      axios.delete(`${ExtName}/${id_cviec}/ns/${id_nv}`, {
         params: {
            loai: loai
         }
      })
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   getCongViecCal(data, callback) {
      axios.get(`${ExtName}/cal`, {
         params: {
            day: data.day
         }
      })
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   getMyCongViec(params, callback) {
      axios.get(`${ExtName}/cal`, {
         params: {
            ...params,
         }
      })
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },
}