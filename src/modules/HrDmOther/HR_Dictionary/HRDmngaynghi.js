import React, { useEffect, useState } from "react";
import { ZenField, ZenFieldSelectApi, ZenFieldDate, ZenFieldSelect, ZenSelectOption, ZenMessageAlert } from "../../../components/Control/index";
import { FormMode, ZenHelper } from "../../../utils";
import { Apihrdmngaynghi } from "../Api";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { Segment, Header, Checkbox, Table, TableRow, TableCell, Icon, Button, Form, Dropdown } from 'semantic-ui-react'
import DatePicker from 'react-datepicker'
import { GlobalStorage, KeyStorage } from "../../../utils/storage";
import _ from "lodash";
import * as permissions from "../../../constants/permissions";

const HRDmNgayNghiModal = (props) => {
    const { formik, permission, mode } = props
    //const [cts, setCts] = useState([])
    const [optionbophan, setOptionbophan] = useState([])

    // const setData = (formik) => {
    //     if (formik.values.cts?.length > 0) {
    //         const b = formik.values.cts;
    //         if (b) { setCts(b) }
    //     } else { return [] }
    // }

    useEffect(() => {
        //setData(formik)
        const bophan = GlobalStorage.getByField(KeyStorage.CacheData, "id_cctc")?.data
        setOptionbophan(ZenHelper.translateListToSelectOptions(bophan.filter(x => x.chi_tiet == 1), false, 'id_cctc', 'id_cctc', 'id_cctc', [], 'ten_cctc'))
    }, [])

    function setForm(data) {
        const newValues = {
            ...formik.values,
            cts: data
        }
        formik.setValues(newValues);
    }

    const addRow = (e, item) => {
        const {cts} = formik.values
        const a = []
        cts.push({
            ksd: false,
            ngay: new Date,
            tinh_cong: false,
            lap_lai: 0,
            id_cctcs: ""
        })
        //setCts(a.concat(cts))
        setForm(cts)
    }

    const DelCell = (e, index) => {
        const newCT = cts.filter((t, idx) => idx !== index); //cts.splice(index, 1);
        setCts(newCT)
        setForm(newCT)
    }

    const handleChangeDate = (date, index) => {
        const {cts} = formik.values
        cts[index].ngay = date
        setForm(cts)
    }

    const handleChangeTinhCong = (e, { name, checked, index }) => {
        const {cts} = formik.values
        cts[index].tinh_cong = checked
        setForm(cts)
    }
    const handleChangeLaplai = (e, index) => {
        const {cts} = formik.values
        cts[index].lap_lai = e.value
        setForm(cts)
    }

    const handleSelectIDcctc = (item,index) => {
       const {cts} = formik.values
        if(_.join(item.value, ',') !== ', ' ) {
            let next_stt = _.join(item.value, ',')
            cts[index].id_cctcs = next_stt
            setForm(cts)
        }  
    }

    return <>
        <ZenField required readOnly={!permission}
            label="hrdmngaynghi.ly_do" defaultlabel="Lý do"
            name="ly_do"
            props={formik}
        />
        <ZenFieldSelectApi required readOnly={!permission}
            label="hrdmngaynghi.loai" defaultlabel="Loại"
            name="loai"
            lookup={{
                ...ZenLookup.SIDmloai,
                where: "ma_nhom = 'LOAI_NGAY_NGHI'",
                onLocalWhere: (items) => {
                    return items.filter(t => t.ma_nhom === 'LOAI_NGAY_NGHI')
                }
            }}
            formik={formik}
        />
        <Segment>
            <Header size='medium'>Danh sách ngày nghỉ</Header>
            <Table celled>
                <Table.Header>
                    <TableRow>
                        <Table.HeaderCell>Ngày</Table.HeaderCell>
                        <Table.HeaderCell width="1">Tính công</Table.HeaderCell>
                        <Table.HeaderCell >Lặp lại</Table.HeaderCell>
                        <Table.HeaderCell width="8">Bộ phận áp dụng</Table.HeaderCell>
                        {!permission === false && <Table.HeaderCell> </Table.HeaderCell>}
                    </TableRow>
                </Table.Header>
                <Table.Body>
                    {formik.values.cts && formik.values.cts.map((item, index) => {

                        return <TableRow key={index}>

                            <Table.Cell>
                                <DatePicker
                                    selected={new Date(item.ngay)}
                                    dateFormat="dd/MM/yyyy"
                                    onChange={(date) => handleChangeDate(date, index)}
                                />
                            </Table.Cell>
                            <Table.Cell textAlign="center">
                                <Checkbox name="tinh_cong"
                                    tabIndex={-1}
                                    checked={item.tinh_cong || false}
                                    index={index}
                                    onChange={(e, { name, checked, index }) => handleChangeTinhCong(e, { name, checked, index })}
                                />
                            </Table.Cell>
                            <Table.Cell>
                                <Form.Select
                                    fluid
                                    options={[
                                        { key: '0', text: 'Không lặp', value: '0' },
                                        { key: '1', text: 'Theo năm', value: 'Y' },
                                    ]}
                                    value={item.lap_lai}
                                    placeholder='Lặp lại'
                                    onChange={(e, i) => handleChangeLaplai(i, index)}
                                />
                            </Table.Cell>
                            <Table.Cell>
                                <Dropdown error={formik && (formik.touched['id_cctcs'] && formik.errors['id_cctcs'])} fluid multiple selection options={optionbophan} value={item.id_cctcs?.split(',')}
                                    readOnly={!permission}
                                    onChange={(e, item) => { handleSelectIDcctc(item,index) }}
                                />
                            </Table.Cell>
                            {!permission === false && <Table.Cell className="delete-col">
                                <Icon link name="trash" size="large"
                                    index={index}
                                    onClick={(e) => DelCell(e, index)}
                                // onClick={(e, propsElement) => onClick(propsElement, ActionType.DELETE_ROW, rowItem, index)}
                                // {...propsIcon}
                                />
                            </Table.Cell>
                            }
                        </TableRow>
                    })}
                    {!permission === false && <Table.Row>
                        <Table.Cell>
                            <Button
                                icon="plus"
                                size="small"
                                primary
                                content="Thêm mới"
                                onClick={addRow}
                            />
                        </Table.Cell>
                        <Table.Cell colSpan={18} />
                    </Table.Row>}
                </Table.Body>
            </Table>
        </Segment>
    </>
}

const onBeforeSave = (item) => {
    var result = true
    var chk_ngay = false
    var chk_bophan = false

    if (item.cts) {
        if (item.cts.length > 0) {
            const ngay = item.cts.filter(function (i) { return i.ngay == null || i.ngay == "" })
            const bophan = item.cts.filter(function (i) { return i.id_cctcs == null || i.id_cctcs == "" })
            if (ngay.length > 0) {
                chk_ngay = true
            } else { chk_ngay = false }
            if (bophan.length > 0) {
                chk_bophan = true
            } else { chk_bophan = false }
            if (chk_ngay == true || chk_bophan == true) {
                result = false
            } else {
                result = true
            }
        } else {
            ZenMessageAlert.warning('Danh sách ngày nghỉ không được để trống')
            return
        }
    } else {
        ZenMessageAlert.warning('Danh sách ngày nghỉ không được để trống')
        return
    }

    if (result) {
        return item
    } else {
        if (chk_ngay) {
            ZenMessageAlert.warning('Ngày không được để trống')
            return
        }
        if (chk_bophan) {
            ZenMessageAlert.warning('Bộ phân không được để trống')
            return
        }
    }
}

const HRDmNgayNghi = {
    FormModal: HRDmNgayNghiModal,
    onBeforeSave: onBeforeSave,
    api: {
        url: Apihrdmngaynghi,
    },
    permission: {
        view: permissions.HrDmNgayNghiXem,
        add: permissions.HrDmNgayNghiThem,
        edit: permissions.HrDmNgayNghiSua
    },
    formId: "HRDmngaynghi-form",
    size: "large",
    isScrolling: false,
    initItem: {
        id: "",
        ksd: false,
        ly_do: "",
        loai: "",
        cts: [
            // {
            //     ksd: false,
            //     ngay: "",
            //     tinh_cong: false,
            //     lap_lai: "",
            //     id_cctcs: ""
            // }
        ]
    },
    formValidation: [
        {
            id: "ly_do",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ]
        },
        {
            id: "loai",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ]
        },
    ]
}

export { HRDmNgayNghi }