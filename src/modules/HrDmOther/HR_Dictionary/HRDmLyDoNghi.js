import React, { useEffect, useState } from "react";
import { ZenField, ZenFieldSelectApi, ZenFieldDate, ZenFieldSelect, ZenSelectOption, ZenMessageAlert, ZenFieldNumber, ZenFieldTextArea } from "../../../components/Control/index";
import { FormMode, ZenHelper } from "../../../utils";
import { ApiHrDmLyDoNghi } from "../Api";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { Segment, Header, Checkbox, Table, TableRow, TableCell, Icon, Button, Form, Dropdown } from 'semantic-ui-react'
import DatePicker from 'react-datepicker'
import { GlobalStorage, KeyStorage } from "../../../utils/storage";
import _ from "lodash";
import * as permissions from "../../../constants/permissions";

const HrDmLyDoNghiModal = (props) => {
    const { formik, permission, mode } = props
    const optionKynghi = [
        { key: "0", value: 0, text: "Không giới hạn" },
        { key: "1", value: "W", text: "Tuần" },
        { key: "2", value: "M", text: "Tháng" },
        { key: "3", value: "Y", text: "Năm" },

    ]

    const handleSelectKynghi = (item) => {
        formik.setFieldValue('ky_tinh_toi_da',item.value) 
     }

    return <>
        <ZenField width={5} required readOnly={!permission}
            label="HrDmLyDoNghi.ma_ld_nghi" defaultlabel="Mã Lý do"
            name="ma_ld_nghi"
            props={formik}
        />
        <ZenField required readOnly={!permission}
            label="HrDmLyDoNghi.ten_ld_nghi" defaultlabel="Tên Lý do"
            name="ten_ld_nghi"
            props={formik}
        />
        <Form.Group width={12}>
            <Form.Field>
            <label>Số ngày được phép nghỉ tối đa </label>
            <Form.Group widths="equal">
                <ZenFieldNumber readOnly={!permission}
                    // label="HrDmLyDoNghi.ngay_nghi_doi_da" defaultlabel="Tên Lý do"
                    name="ngay_nghi_doi_da"
                    props={formik}
                />
                <span style={{whiteSpace:'nowrap', lineHeight:"38px", paddingRight:"5px"}}>Ngày / </span>
                <Dropdown error={formik && (formik.touched['ky_tinh_toi_da'] && formik.errors['ky_tinh_toi_da'])} fluid selection options={optionKynghi} value={formik.values.ky_tinh_toi_da}
                    readOnly={!permission}
                    onChange={(e, item) => { handleSelectKynghi(item) }}
                />
                </Form.Group>
            </Form.Field>
        </Form.Group>
        <ZenFieldTextArea
            label="HrDmLyDoNghi.ghi_chu" defaultlabel="Ghi chú"
            name="ghi_chu"
            props={formik}
        />
    </>
}

const HrDmLyDoNghi = {
    FormModal: HrDmLyDoNghiModal,
    api: {
        url: ApiHrDmLyDoNghi,
    },
    permission: {
        view: permissions.HrDmLyDoNghiXem,
        add: permissions.HrDmLyDoNghiThem,
        edit: permissions.HrDmLyDoNghiSua
    },
    formId: "HrDmLyDoNghi-form",
    size: "small",
    isScrolling: false,
    initItem: {
        ksd: false,
        ma_ld_nghi: "",
        ten_ld_nghi: "",
        ghi_chu: "",
        ngay_nghi_doi_da: 0,
        ky_tinh_toi_da: 0,
        tl_luong: 0
    },
    formValidation: [
        {
            id: "ma_ld_nghi",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ]
        },
        {
            id: "ten_ld_nghi",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ]
        },
    ]
}

export { HrDmLyDoNghi }