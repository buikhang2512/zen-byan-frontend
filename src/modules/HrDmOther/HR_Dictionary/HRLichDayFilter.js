import React, { useEffect, useMemo, useRef, useState } from "react";
import { Accordion, Button, Form, Icon } from "semantic-ui-react";
import { ref } from "yup";
import { ZenLoading, ZenFormik, ZenFieldSelect, ZenFieldSelectApi, InputDatePeriod, ZenDatePeriod } from "../../../components/Control/index";
import { zzControlHelper } from "../../../components/Control/zzControlHelper";
import { ZenHelper } from "../../../utils";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import * as permissions from "../../../constants/permissions";
import { auth } from "../../../utils";

export const HrLichDayFilter = (props) => {
    const { onLoadData, fieldCode, onAfterLoadData } = props;
    const refForm = useRef();
    const [expand, setExpand] = useState(0);

    const memoSIDmLoai = useMemo(() => {
        const data = zzControlHelper.getDataFromLocal(ZenLookup.SIDmloai)
        return {
            TenPlkh: data?.filter(t => t.ma_nhom === "PLKH")
                ?.reduce((obj, item) => Object.assign(obj, { [item.ma]: item.ten }), {}),
            optionLoaiKh: zzControlHelper.convertToOptionsSemantic(data.filter(t => t.ma_nhom === "LOAI_KH"), true, "ma", "ma", "ten")
        }
    }, [])

    useEffect(() => {
        onLoadData(``);
    }, []);


    const handleSubmit = async (item, { name }) => {
        const newItem = { ...refForm.current.values, [name]: item.value, }
        const strSql = createStrSql(newItem)
        onLoadData(strSql, newItem, true);
        setLoadingForm(false)
    }

    const handleChangeDate = ({ startDate, endDate }) => {
        if (startDate) {
            refForm.current.setFieldValue('ngay1', startDate)
        }
        if (endDate) {
            refForm.current.setFieldValue('ngay2', endDate)
        }

        const newItem = { ...refForm.current.values, ngay1: startDate, ngay2: endDate }
        const strSql = createStrSql(newItem)
        onLoadData(strSql, newItem, true)
    }

    function createStrSql(item = {}) {
        let _sql = "";
        if (item.ngay1) _sql += ` AND ngay >= '${item.ngay1}'`;
        if (item.ngay2) _sql += ` AND ngay <= '${item.ngay2}'`;
        if (item.id_nv) _sql += `AND id_nv = '${item.id_nv}'`;
        if (item.ma_kh) _sql += `AND ma_kh = '${item.ma_kh}'`;

        return _sql.replace("AND", "")
    }

    return <>
        {/* <Accordion fluid styled>
            <Accordion.Title
                active={expand === 0}
                index={0}
                onClick={(e, { index }) => setExpand(expand === index ? -1 : index)}
            >
                <Icon name="dropdown" />
                Điều kiện lọc
            </Accordion.Title>
            <Accordion.Content active={expand === 0}> */}
        {!auth.checkPermission(permissions.HrLichDayXemAll) && <>
            <ZenFormik form={"HrLichDay-filter"}
                ref={refForm}
                validation={[]}
                initItem={initItem}
                onSubmit={handleSubmit}
                onReset={(item) => onLoadData("")}
            >
                {
                    formik => {
                        const { values } = formik
                        return <Form size="small">
                            <Form.Group widths="4">
                                <ZenDatePeriod
                                    onChange={handleChangeDate}
                                    value={[values.ngay1, values.ngay2]}
                                    textLabel="Time"
                                    defaultPopupYear={ZenHelper.getFiscalYear()}
                                />
                                <ZenFieldSelectApi
                                    lookup={ZenLookup.ID_NV}
                                    label="hrlichday.id_nv" defaultlabel="Teacher’s Name"
                                    name="id_nv"
                                    formik={formik}
                                    onItemSelected={handleSubmit}
                                />
                                <ZenFieldSelectApi
                                    lookup={ZenLookup.Ma_kh}
                                    label="hrlichday.ma_kh" defaultlabel="School's Name"
                                    name="ma_kh"
                                    formik={formik}
                                    onItemSelected={handleSubmit}
                                />
                            </Form.Group>
                        </Form>
                    }
                }
            </ZenFormik>

            <Button primary
                content="Reset"
                size="small" icon="refresh"
                onClick={(e) => refForm.current.handleReset(e)}
            />
        </>}
            {/* </Accordion.Content>
        </Accordion> */}
    </>
}

const initItem = {
    ngay1: "",
    ngay2: "",
    id_nv: "",
    ma_kh: "",
}