import React, { useEffect } from "react";
import { ZenField, ZenFieldSelectApi } from "../../../components/Control/index";
import { FormMode } from "../../../utils";
import { Form } from "semantic-ui-react";
import { ApiHrDmCctc, ApiHRDmCctc } from "../Api";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import * as permissions from "../../../constants/permissions";

const HRDmCctcModal = (props) => {
   const { formik, permission, mode } = props
   useEffect(() => {
      if (mode === FormMode.ADD) {
         formik.setFieldValue("loai", "1")
      }
   }, [])

   const handleselectloai = (item,{name}) => {
      const bac = Number(item.bac) + 1
      formik.setFieldValue("bac", item.bac ? bac : 1)
      formik.setFieldValue("id_parent", item.id_cctc)
   }

   return <React.Fragment>
      <ZenField width={"8"} required autoFocus readOnly={!permission || (mode === FormMode.ADD ? false : true)}
         label={"HRDmCctc.ma_plkh"} defaultlabel="Mã "
         name="id_cctc" props={formik}
         isCode
      />
      <ZenField required readOnly={!permission}
         label={"HRDmCctc.ten_plkh"} defaultlabel="Tên "
         name="ten_cctc" props={formik}
      />
      <ZenFieldSelectApi 
         required readOnly={!permission}
         lookup={ZenLookup.HRDmcctc}
         label={"HRDmCctc.id_parent"} defaultlabel="Cấp cha"
         name="id_parent" formik={formik} 
         onItemSelected={(item, { name }) => handleselectloai(item,{name})}
      />
   </React.Fragment>
}

const HRDmCctc = {
   FormModal: HRDmCctcModal,
   api: {
      url: ApiHrDmCctc,
   },
   permission: {
      view: permissions.HRDmCctcXem,
      add: permissions.HRDmCctcThem,
      edit: permissions.HRDmCctcSua
   },
   formId: "HRDmCctc-form",
   size: "tiny",
   isScrolling: false,
   initItem: {
      id_cctc:"",
      ten_cctc: "",
      id_parent: "",
      bac:"",
      ksd: false,
   },
   formValidation: [
      {
         id: "id_cctc",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
         ]
      },
      {
         id: "ten_cctc",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
         ]
      },
      {
         id: "id_parent",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
         ]
      },
   ]
}

export { HRDmCctc };