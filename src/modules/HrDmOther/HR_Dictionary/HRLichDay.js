import React, { useEffect } from "react";
import { ZenField, ZenFieldSelectApi, ZenFieldDate, ZenFieldTextArea } from "../../../components/Control/index";
import { FormMode } from "../../../utils";
import { Form } from "semantic-ui-react";
import { ApiHrLichDay } from "../Api";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import DatePicker from "react-datepicker";
import { zzControlHelper } from "../../../components/Control/zzControlHelper";
import moment from "moment";
import * as permissions from "../../../constants/permissions";

const HrLichDayModal = (props) => {
    const { formik, permission, mode, infoModal } = props

    const { ngay, tu, den } = formik.values

    useEffect(() => {
        if (!ngay) {
            formik.setFieldValue('ngay', new Date())
            formik.setFieldValue('tu', new Date())
            formik.setFieldValue('den', new Date())
        }
    }, [ngay])

    const handleselect = (item, { name }) => {
        if (name == 'id_nv') {
            formik.setFieldValue('ten_nv', item.ho_ten)
        } else if (name == 'ma_kh') {
            formik.setFieldValue('ten_kh', item.ten_kh)
        }
    }

    const handleChangeDate = (item) => {
        formik.setFieldValue('tu', item.value)
        formik.setFieldValue('den', item.value)
    }

    function checkReadonly(noChange, value, permission) {
        if (noChange && value) {
            return noChange
        }
        return !permission
    }

    return <>
        <ZenFieldSelectApi
            readOnly={checkReadonly(infoModal?.noChange, formik.values.id_nv, permission) === 'nv' ? true : !permission}
            lookup={ZenLookup.ID_NV}
            label="hrlichday.id_nv" defaultlabel="Giáo viên"
            name="id_nv"
            formik={formik}
            onItemSelected={(item, { name }) => handleselect(item, { name })}
        />
        <ZenFieldSelectApi
            readOnly={checkReadonly(infoModal?.noChange, formik.values.ma_kh, permission) === 'kh' ? true : !permission}
            lookup={ZenLookup.Ma_kh}
            label="hrlichday.ma_kh" defaultlabel="Trường/khách hàng"
            name="ma_kh"
            formik={formik}
            onItemSelected={(item, { name }) => handleselect(item, { name })}
        />
        <ZenFieldSelectApi
            readOnly={!permission}
            lookup={{
                ...ZenLookup.SIDmloai, format: "{ten}",
                onLocalWhere: (items) => {
                    return items.filter(t => t.ma_nhom === 'LICH_DAY_CAP') || []
                },
                where: "MA_NHOM = 'LICH_DAY_CAP'"
            }}
            formik={formik}
            label="hrlichday.cap" defaultlabel="Cấp dạy"
            name="cap"
        />
        <Form.Group>
            <ZenFieldDate width={8} readOnly={checkReadonly(infoModal?.noChange, formik.values.ngay, permission) ? true : !permission}
                label="hrlichday.ngay" defaultlabel="Ngày"
                name="ngay"
                props={formik}
                onChange={(item) => handleChangeDate(item)}
            />
            <Form.Field
                error={formik && (formik.touched['tu'] && formik.errors['tu'])}
            >
                <label>Từ</label>
                <DatePicker
                    selected={formik.values.tu ? new Date(formik.values.tu) : undefined} //moment(new Date(formik.values.tu).toISOString()).toDate()
                    onChange={(date) => {
                        let tu = new Date(date)
                        let ngay = new Date(formik.values.ngay)
                        ngay.setHours(tu.getHours())
                        ngay.setMinutes(tu.getMinutes())
                        formik.setFieldValue('tu', zzControlHelper.formatDateTime(ngay, 'YYYY-MM-DD HH:mm'))
                    }}
                    showTimeSelect
                    showTimeSelectOnly
                    timeIntervals={30}
                    timeFormat="HH:mm"
                    timeCaption="Time"
                    dateFormat="HH:mm"
                />
                {formik && (formik.touched['tu'] && formik.errors['tu']) ? <div class="ui pointing above prompt label" role="alert" aria-atomic="true">Không được bỏ trống trường này</div> : undefined}
            </Form.Field>
            <Form.Field
                error={formik && (formik.touched['den'] && formik.errors['den'])}
            >
                <label>Đến</label>
                <DatePicker
                    selected={formik.values.den ? new Date(formik.values.den) : undefined}
                    onChange={(date) => {
                        let den = new Date(date)
                        let ngay = new Date(formik.values.ngay)
                        ngay.setHours(den.getHours())
                        ngay.setMinutes(den.getMinutes())
                        formik.setFieldValue('den', zzControlHelper.formatDateTime(ngay, 'YYYY-MM-DD HH:mm'))
                    }}
                    showTimeSelect
                    showTimeSelectOnly
                    timeIntervals={30}
                    timeFormat="HH:mm"
                    timeCaption="Time"
                    dateFormat="HH:mm"
                />
                {formik && (formik.touched['den'] && formik.errors['den']) ? <div class="ui pointing above prompt label" role="alert" aria-atomic="true">Không được bỏ trống trường này</div> : undefined}
            </Form.Field>
        </Form.Group>
        <ZenFieldTextArea
            label="hrlichday.ghi_chu" defaultlabel="Ghi chú"
            name="ghi_chu"
            props={formik}
        />
    </>
}

const customHeader = (props, state) => {
    const { id, header, fieldCode, formMode, currentItem, intl } = props
    const { valueFieldKey } = state
    const txtHeader = !id ? ('Thêm mới lịch dạy') : formMode == 1 ? ('Thêm mới lịch dạy') : "Sửa"
    // : ('Hạng mục' + ": " + id)
    return txtHeader
}
const HRLichDay = {
    FormModal: HrLichDayModal,
    customHeader,
    api: {
        url: ApiHrLichDay,
    },
    permission: {
        view: permissions.HrLichDayXem,
        add: permissions.HrLichDayThem,
        edit: permissions.HrLichDaySua
    },
    formId: "HrLichDay-form",
    size: "tiny",
    isScrolling: false,
    initItem: {
        id_nv: "",
        ma_kh: "",
        ma_hd: "",
        ngay: "",
        tu: "",
        den: "",
        ghi_chu: "",
        cap: "",
    },
    formValidation: [
        {
            id: "id_nv",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ]
        },
        {
            id: "ma_kh",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ]
        },
        {
            id: "ngay",
            validationType: "date",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ]
        },
        {
            id: "den",
            validationType: "date",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ]
        },
        {
            id: "tu",
            validationType: "date",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ]
        }
    ]
}

export { HRLichDay };