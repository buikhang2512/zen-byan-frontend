import React, { useEffect } from "react";
import { ZenField, ZenFieldSelectApi, ZenFieldDate } from "../../../components/Control/index";
import { FormMode } from "../../../utils";
import { Form } from "semantic-ui-react";
import { ApiHrHsNs } from "../Api";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import * as routes from "../../../constants/routes"
import { zzControlHelper } from "../../../components/Control/zzControlHelper";
import * as permissions from "../../../constants/permissions";

const HRHsNsModal = (props) => {
    const { formik, permission, mode } = props

    useEffect(() => {
        if (mode === FormMode.ADD) {
            formik.setFieldValue("loai", "1")
        }
    }, [])

    // const handleselectloai = (item, { name }) => {
    //     const bac = Number(item.bac) + 1
    //     formik.setFieldValue("bac", item.bac ? bac : 1)
    //     formik.setFieldValue("id_parent", item.)
    // }

    return <React.Fragment>
        <Form.Group>
            <ZenField width={"8"} required autoFocus readOnly={!permission || (mode === FormMode.ADD ? false : true)}
                label={"HRHsNh.id_nv"} defaultlabel="ID nhân sự"
                name="id_nv" props={formik}
                isCode
            />
            <ZenField width={"8"}  readOnly={!permission} readOnly={!permission || (mode === FormMode.ADD ? false : true)}
                label={"HRHsNh.ma_nvns"} defaultlabel="Mã nhân viên"
                name="ma_nvns" props={formik}
                isCode
            />
        </Form.Group>
        <Form.Group>
            <ZenField width={"16"} required  readOnly={!permission}
                label={"HRHsNh.ho_ten"} defaultlabel="Họ và tên"
                name="ho_ten" props={formik}
            />
        </Form.Group>
        <Form.Group>
            <ZenFieldDate width={"8"} readOnly={!permission}
                label={"HRHsNh.ngay_sinh"} defaultlabel="Ngày sinh"
                props={formik}
                name="ngay_sinh"
            />
            <ZenFieldSelectApi width={"8"}   readOnly={!permission}
                lookup={{
                    ...ZenLookup.SIDmloai, format: "{ten}",
                    onLocalWhere: (items) => {
                        return items.filter(t => t.ma_nhom === 'GIOI_TINH') || []
                    },
                    where: "MA_NHOM = 'GIOI_TINH'"
                }}
                label={"HRHsNs.gioi_tinh"} defaultlabel="Giới tính"
                name="gioi_tinh" formik={formik}
            />
        </Form.Group>
        <Form.Group>
            <ZenField width={"16"}   readOnly={!permission}
                label={"HRHsNh"} defaultlabel="Nơi sinh"
                name="noi_sinh" props={formik}
            />
        </Form.Group>
        <Form.Group>
            <ZenField width={"8"}   readOnly={!permission}
                label={"HRHsNh.email"} defaultlabel="Email"
                name="email" props={formik}
            />
            <ZenField width={"8"}  readOnly={!permission}
                label={"HRHsNh.so_dien_thoai"} defaultlabel="Điện thoại"
                name="dien_thoai" props={formik}
            />
        </Form.Group>
        <Form.Group>
            <ZenField width={"16"}   readOnly={!permission}
                label={"HRHsNh"} defaultlabel="Địa chỉ thường trú"
                name="ho_khau" props={formik}
            />
        </Form.Group>
        <Form.Group>
            <ZenField width={"16"}   readOnly={!permission}
                label={"HRHsNh"} defaultlabel="Địa chỉ cư trú"
                name="noi_o" props={formik}
            />
        </Form.Group>
    </React.Fragment>
}

const HRHsNs = {
    FormModal: HRHsNsModal,
    api: {
        url: ApiHrHsNs,
    },
    saveAndRedirect: {
        linkto : (item) => {
           const id = zzControlHelper.btoaUTF8(item['id_nv'])
           window.location.assign(`#${routes.HrHsNs_detail(id)}`)
        }
     },
    permission: {
        view: permissions.HrHsNsXem,
        add: permissions.HrHsNsThem,
        edit: permissions.HrHsNsSua
    },
    formId: "HRHsNs-form",
    size: "tiny",
    isScrolling: false,
    initItem: {
    },
    formValidation: [
        {
            id: "id_nv",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ]
        },
        {
            id: "ho_ten",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ]
        }
    ]
}

export { HRHsNs };