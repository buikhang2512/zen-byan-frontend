import React, { useEffect, useState } from 'react';
import * as routes from "../../../constants/routes";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { FormatDate, FormatNumber, ZenLink, ZenMessageAlert } from '../../../components/Control';
import { useRouteMatch, useHistory, Link } from 'react-router-dom';
import { ApiHrDmCctc, Apihrdmngaynghi, ApiHrHsNs, ApiHrLichDay, ApiHrDmLyDoNghi } from '../Api';
import { Button, Dropdown, Icon, Popup, Table } from 'semantic-ui-react';
import { ZenDictionaryModal } from '../../../components/Control/zDictionary';
import { HRDmCctc } from './HRDmCctc';
import { ZenHelper } from '../../../utils';
import { FormMode } from '../../../components/Control/zzControlHelper';
import { zzControlHelper } from '../../../components/Control/zzControlHelper';
import _ from 'lodash';
import { HrLichDayFilter } from './HRLichDayFilter';
import * as permissions from "../../../constants/permissions";
import { GlobalStorage, KeyStorage } from '../../../utils/storage';
import { ApiLookup } from '../../../Api';

function setParamsLookup(lookup) {
   return {
      code_name: lookup.code,
      field_list: lookup.cols,
      where: lookup.where,
      top: lookup.top === null ? 0 : (lookup.top || 20)
   }
}


const sidmloai = GlobalStorage.getByField(KeyStorage.CacheData, 'ma_loai')?.data
let optionGioiTinh = []
let optionLOAIHSNS = []
let optionTTHSNS = []
let optionBoPhan = []
if (zzControlHelper.checkLocalData(ZenLookup.HRDmcctc)) {
   optionBoPhan = ZenHelper.translateListToSelectOptions(GlobalStorage.getByField(KeyStorage.CacheData, 'id_cctc').data, false, 'id_cctc', 'id_cctc', 'id_cctc', [], 'ten_cctc')
} else {
   ApiLookup.get(res => {
      if (res.status === 200) {
         optionBoPhan = ZenHelper.translateListToSelectOptions(res.data.data, false, 'id_cctc', 'id_cctc', 'id_cctc', [], 'ten_cctc')
      }
   }, {
      ...setParamsLookup(ZenLookup.HRDmcctc)
   })
}
if (sidmloai) {
   optionGioiTinh = ZenHelper.translateListToSelectOptions(sidmloai.filter(x => x.ma_nhom === 'GIOI_TINH'), false, 'ma_loai', 'ma', 'ma_loai', [], 'ten')
   optionLOAIHSNS = ZenHelper.translateListToSelectOptions(sidmloai.filter(x => x.ma_nhom === 'LOAI_HSNS'), false, 'ma_loai', 'ma', 'ma_loai', [], 'ten')
   optionTTHSNS = ZenHelper.translateListToSelectOptions(sidmloai.filter(x => x.ma_nhom === 'TT_HSNS'), false, 'ma_loai', 'ma', 'ma_loai', [], 'ten')

} else {
   ApiLookup.get(res => {
      if (res.status === 200) {
         optionGioiTinh = ZenHelper.translateListToSelectOptions(res.data.data.filter(x => x.ma_nhom === 'GIOI_TINH'), false, 'ma_loai', 'ma', 'ma_loai', [], 'ten')
         optionLOAIHSNS = ZenHelper.translateListToSelectOptions(res.data.data.filter(x => x.ma_nhom === 'LOAI_HSNS'), false, 'ma_loai', 'ma', 'ma_loai', [], 'ten')
         optionTTHSNS = ZenHelper.translateListToSelectOptions(res.data.data.filter(x => x.ma_nhom === 'TT_HSNS'), false, 'ma_loai', 'ma', 'ma_loai', [], 'ten')
      }
   }, {
      ...setParamsLookup(ZenLookup.SIDmloai)
   })
}
const HR_Dictionary = {
   HrDmCctc: {
      route: routes.HrDmCctc,
      action: {
         view: { visible: true, permission: permissions.HRDmCctcXem },
         add: { visible: true, permission: permissions.HRDmCctcThem },
         edit: { visible: true, permission: permissions.HRDmCctcSua },
         del: { visible: true, permission: permissions.HRDmCctcXoa },
      },

      linkHeader: {
         id: "hrdmcctc",
         defaultMessage: "Danh mục cơ cấu tổ chức",
         active: true,
         isSetting: true,
      },

      tableList: {
         keyForm: "HRDmCctc",
         formModal: ZenLookup.HRDmcctc,
         fieldCode: "id_cctc",
         unPagination: false,
         addchild: true,
         onBeforeAction: (item, mode) => {
            const itm = _.cloneDeep(item)
            if (mode === FormMode.ADD) {
               if (itm.id_cctc) {
                  itm.id_parent = itm.id_cctc
                  delete itm.id_cctc
               }
               if (itm.ten_cctc) { delete itm.ten_cctc }
               if (itm.bac) { delete itm.bac }
               if (itm.stt) { delete itm.stt }
               if (itm.stt_cay) { delete itm.stt_cay }
               if (itm.ten_parent) { delete itm.ten_parent }
            }
            return { item: itm, open: true }
         },
         showFilterLabel: true,

         api: {
            url: ApiHrDmCctc,
            type: "sql"
         },
         columns: [
            { id: "hrdmcctc.ten_cctc", defaultMessage: "Tên phòng ban/bộ phận", fieldName: "ten_cctc", filter: "string", sorter: true, editForm: true, custom: true },
            { id: "hrdmcctc.id_cctc", defaultMessage: "Mã phòng ban/bộ phận", fieldName: "id_cctc", filter: "string", sorter: true, },
            { id: "hrdmcctc.id_parent", defaultMessage: "Cấp trên", fieldName: "id_parent", filter: "list", sorter: true, custom: true, listFilter: optionBoPhan },
            { id: "ksd", defaultMessage: "Không sử dụng", fieldName: "ksd", type: "bool", filter: "", sorter: true },
         ],
         //  rowStyle: (item) => {
         //    // style chung cho dòng
         //    const rowStyle = {
         //       fontWeight: !item.ten_cctc ? "bold" : undefined,
         //       color: item.ten_cctc ? "black" : "#2557b8",
         //    }
         //    return {
         //       style: rowStyle,
         //       className: ""
         //    }
         // },

         customCols: (data, item, fieldName, { onEdit, ...restCol }) => {
            let customCell = undefined
            if (fieldName === 'ten_cctc') {
               customCell = <Table.Cell key={fieldName} singleLine>
                  <ZenLink style={{
                     paddingLeft: `${Number(item.bac - 1) * 14}px`,
                     ...restCol.style,
                  }}
                     onClick={() => onEdit(item, { linkRow: restCol.link, editForm: restCol.editForm })}
                     permission=""
                  >
                     {item.chi_tiet ? " " + item.ten_cctc : (<span><Icon name={item.icon_name} /> {item.ten_cctc}</span>)}
                  </ZenLink>
               </Table.Cell>
            }
            if (fieldName === 'id_parent') {
               const loai = optionBoPhan.find(t => t.value == item["id_parent"]) || {}
               customCell = <Table.Cell key={fieldName}>
                  {loai.text || item["ten_parent"]}
               </Table.Cell>
            }
            return customCell
         }
      }
   },

   HrHsNs: {
      route: routes.HrHsNs,
      action: {
         view: { visible: true, permission: permissions.HrHsNsXem },
         add: { visible: true, permission: permissions.HrHsNsThem },
         edit: { visible: true, permission: permissions.HrHsNsSua },
         del: { visible: true, permission: permissions.HrHsNsXoa },
      },

      linkHeader: {
         id: "hrhsns",
         defaultMessage: "Hồ sơ nhân sự",
         active: true,
         isSetting: false,
      },

      tableList: {
         keyForm: "HRHsNs",
         formModal: ZenLookup.HRHsNs,
         fieldCode: "id_nv",
         unPagination: false,
         showFilterLabel: true,

         api: {
            url: ApiHrHsNs,
            type: "sql"
         },

         columns: [
            { id: "Account", defaultMessage: "Account", fieldName: "user_account", filter: "", visibleColMove: false, sorter: false, custom: true },
            { id: "hrhsns.id_nv", defaultMessage: "ID Nhân sự", fieldName: "id_nv", filter: "string", sorter: true, editForm: true, custom: true },
            { id: "hrdmcctc.ho_ten", defaultMessage: "Họ và tên", fieldName: "ho_ten", filter: "string", sorter: true, },
            { id: "hrdmcctc.gioi_tinh", defaultMessage: "GT", fieldName: "gioi_tinh", filter: "list", sorter: true, listFilter: optionGioiTinh },
            { id: "hrdmcctc.ngay_sinh", defaultMessage: "Ngày sinh", fieldName: "ngay_sinh", filter: "date", sorter: true, },
            { id: "hrdmcctc.dien_thoai", defaultMessage: "Điện thoại", fieldName: "dien_thoai", filter: "string", sorter: true, },
            { id: "hrdmcctc.email", defaultMessage: "Email", fieldName: "email", filter: "string", sorter: true, },
            { id: "hrdmcctc.ten_bo_phan", defaultMessage: "Bộ phận", fieldName: "ma_bo_phan", filter: "list", sorter: true, custom: true, listFilter: optionBoPhan },
            { id: "hrdmcctc.ten_loai_hsns", defaultMessage: "Loại HsNs", fieldName: "loai_hsns", filter: "list", sorter: true, custom: true, listFilter: optionLOAIHSNS },
            { id: "hrdmcctc.ten_trang_thai", defaultMessage: "Trạng thái", fieldName: "trang_thai", filter: "list", sorter: true, custom: true, listFilter: optionTTHSNS },
         ],
         customCols: (data, item, fieldName, { onEdit, ...restCol }) => {
            let customCell = undefined
            const linkto = (a) => {
               let paramsUrl = zzControlHelper.btoaUTF8(a.params)
               const route = a.route.replace(":id", paramsUrl)
               return route
            }

            if (fieldName === 'user_account' && item.user_account) {
               customCell = <Table.Cell key={fieldName} collapsing>
                  <span style={{
                     ...restCol.style,
                  }}
                  >
                     <Popup
                        content={item.user_account}
                        key={item.user_account}
                        trigger={<Icon name="user circle" />}
                     />
                  </span>
               </Table.Cell>
            }
            if (fieldName === 'id_nv') {
               customCell = <Table.Cell key={fieldName} singleLine>
                  <ZenLink style={{ ...restCol.style }}
                     to={linkto({ route: routes.HrHsNs_detail(), params: item.id_nv })}
                     permission=""
                  >
                     {item.id_nv}
                  </ZenLink>
               </Table.Cell>;
            }
            if (fieldName === 'ma_bo_phan') {
               const loai = optionBoPhan.find(t => t.value == item["ma_bo_phan"]) || {}
               customCell = <Table.Cell key={fieldName}>
                  {loai.text || item["ten_bo_phan"]}
               </Table.Cell>
            }
            if (fieldName === 'loai_hsns') {
               const loai = optionLOAIHSNS.find(t => t.value == item["ma_loai_hsns"]) || {}
               customCell = <Table.Cell key={fieldName}>
                  {loai.text || item["ten_loai_hsns"]}
               </Table.Cell>
            }
            if (fieldName === 'trang_thai') {
               const loai = optionTTHSNS.find(t => t.value == item["ma_trang_thai"]) || {}
               customCell = <Table.Cell key={fieldName}>
                  {loai.text || item["ten_trang_thai"]}
               </Table.Cell>
            }
            return customCell
         }
      },
   },

   HrHsNsGV: {
      route: routes.HrHsNsGV,
      action: {
         view: { visible: true, permission: permissions.HRHsNsGV },
         add: { visible: true, permission: permissions.HrHsNsThem },
         edit: { visible: true, permission: permissions.HrHsNsSua },
         del: { visible: true, permission: permissions.HrHsNsXoa },
      },

      linkHeader: {
         id: "hrhsns-gv",
         defaultMessage: "Danh sách giáo viên nước ngoài",
         active: true,
         isSetting: false,
      },

      tableList: {
         ContainerTop: (props) => {
            const { onLoadData, fieldCode } = props;
            useEffect(() => {
               onLoadData(`ma_bo_phan = 'GVNN'`)
            }, [])
            return <></>
         },
         keyForm: "HRHsNs",
         formModal: ZenLookup.HRHsNs,
         fieldCode: "id_nv",
         unPagination: false,
         showFilterLabel: true,

         api: {
            url: ApiHrHsNs,
            type: "sql"
         },

         columns: [
            { id: "Account", defaultMessage: "Account", fieldName: "user_account", filter: "", visibleColMove: false, sorter: false, custom: true },
            { id: "hrhsns.id_nv", defaultMessage: "ID Nhân sự", fieldName: "id_nv", filter: "string", sorter: true, editForm: true, custom: true },
            { id: "hrdmcctc.ho_ten", defaultMessage: "Họ và tên", fieldName: "ho_ten", filter: "string", sorter: true, },
            { id: "hrdmcctc.gioi_tinh", defaultMessage: "GT", fieldName: "gioi_tinh", filter: "list", sorter: true, listFilter: optionGioiTinh },
            { id: "hrdmcctc.ngay_sinh", defaultMessage: "Ngày sinh", fieldName: "ngay_sinh", filter: "date", sorter: true, },
            { id: "hrdmcctc.dien_thoai", defaultMessage: "Điện thoại", fieldName: "dien_thoai", filter: "string", sorter: true, },
            { id: "hrdmcctc.email", defaultMessage: "Email", fieldName: "email", filter: "string", sorter: true, },
            { id: "hrdmcctc.ten_bo_phan", defaultMessage: "Bộ phận", fieldName: "ma_bo_phan", filter: "list", sorter: true, custom: true, listFilter: optionBoPhan },
            { id: "hrdmcctc.ten_loai_hsns", defaultMessage: "Loại HsNs", fieldName: "loai_hsns", filter: "list", sorter: true, custom: true, listFilter: optionLOAIHSNS },
            { id: "hrdmcctc.ten_trang_thai", defaultMessage: "Trạng thái", fieldName: "trang_thai", filter: "list", sorter: true, custom: true, listFilter: optionTTHSNS },
         ],
         customCols: (data, item, fieldName, { onEdit, ...restCol }) => {
            let customCell = undefined

            const linkto = (a) => {
               let paramsUrl = zzControlHelper.btoaUTF8(a.params.id)
               const route = a.route.replace(":id", paramsUrl)
               return {
                  pathname: route,
                  //search: `?`+ new URLSearchParams({tk: filter.tk,}).toString(),
                  state: { route: a.params.route },
               }
            }

            if (fieldName === 'user_account' && item.user_account) {
               customCell = <Table.Cell key={fieldName} collapsing>
                  <span style={{
                     ...restCol.style,
                  }}
                  >
                     <Popup
                        content={item.user_account}
                        key={item.user_account}
                        trigger={<Icon name="user circle" />}
                     />
                  </span>
               </Table.Cell>
            }
            if (fieldName === 'id_nv') {
               customCell = <Table.Cell key={fieldName} singleLine>
                  <ZenLink style={{ ...restCol.style }}
                     to={linkto({ route: routes.HrHsNs_detail(), params: { id: item.id_nv, route: routes.HrHsNsGV } })}
                     permission=""
                  >
                     {item.id_nv}
                  </ZenLink>
               </Table.Cell>;
            }
            if (fieldName === 'ma_bo_phan') {
               const loai = optionBoPhan.find(t => t.value == item["ma_bo_phan"]) || {}
               customCell = <Table.Cell key={fieldName}>
                  {loai.text || item["ten_bo_phan"]}
               </Table.Cell>
            }
            if (fieldName === 'loai_hsns') {
               const loai = optionLOAIHSNS.find(t => t.value == item["ma_loai_hsns"]) || {}
               customCell = <Table.Cell key={fieldName}>
                  {loai.text || item["ten_loai_hsns"]}
               </Table.Cell>
            }
            if (fieldName === 'trang_thai') {
               const loai = optionTTHSNS.find(t => t.value == item["ma_trang_thai"]) || {}
               customCell = <Table.Cell key={fieldName}>
                  {loai.text || item["ten_trang_thai"]}
               </Table.Cell>
            }
            return customCell
         }
      },
   },

   HrHsNsNV: {
      route: routes.HrHsNsNV,
      action: {
         view: { visible: true, permission: permissions.HRHsNsNV },
         add: { visible: true, permission: permissions.HrHsNsThem },
         edit: { visible: true, permission: permissions.HrHsNsSua },
         del: { visible: true, permission: permissions.HrHsNsXoa },
      },

      linkHeader: {
         id: "hrhsns-gv",
         defaultMessage: "Danh sách nhân sự quản lý",
         active: true,
         isSetting: false,
      },

      tableList: {
         ContainerTop: (props) => {
            const { onLoadData, fieldCode } = props;
            useEffect(() => {
               onLoadData(`ma_bo_phan <> 'GVNN' and ma_bo_phan <> 'GVCV'`)
            }, [])
            return <></>
         },
         keyForm: "HRHsNs",
         formModal: ZenLookup.HRHsNs,
         fieldCode: "id_nv",
         unPagination: false,
         showFilterLabel: true,

         api: {
            url: ApiHrHsNs,
            type: "sql"
         },

         columns: [
            { id: "Account", defaultMessage: "Account", fieldName: "user_account", filter: "", visibleColMove: false, sorter: false, custom: true },
            { id: "hrhsns.id_nv", defaultMessage: "ID Nhân sự", fieldName: "id_nv", filter: "string", sorter: true, editForm: true, custom: true },
            { id: "hrdmcctc.ho_ten", defaultMessage: "Họ và tên", fieldName: "ho_ten", filter: "string", sorter: true, },
            { id: "hrdmcctc.gioi_tinh", defaultMessage: "GT", fieldName: "gioi_tinh", filter: "list", sorter: true, listFilter: optionGioiTinh },
            { id: "hrdmcctc.ngay_sinh", defaultMessage: "Ngày sinh", fieldName: "ngay_sinh", filter: "date", sorter: true, },
            { id: "hrdmcctc.dien_thoai", defaultMessage: "Điện thoại", fieldName: "dien_thoai", filter: "string", sorter: true, },
            { id: "hrdmcctc.email", defaultMessage: "Email", fieldName: "email", filter: "string", sorter: true, },
            { id: "hrdmcctc.ten_bo_phan", defaultMessage: "Bộ phận", fieldName: "ma_bo_phan", filter: "list", sorter: true, custom: true, listFilter: optionBoPhan },
            { id: "hrdmcctc.ten_loai_hsns", defaultMessage: "Loại HsNs", fieldName: "loai_hsns", filter: "list", sorter: true, custom: true, listFilter: optionLOAIHSNS },
            { id: "hrdmcctc.ten_trang_thai", defaultMessage: "Trạng thái", fieldName: "trang_thai", filter: "list", sorter: true, custom: true, listFilter: optionTTHSNS },
         ],
         customCols: (data, item, fieldName, { onEdit, ...restCol }) => {
            let customCell = undefined

            const linkto = (a) => {
               let paramsUrl = zzControlHelper.btoaUTF8(a.params.id)
               const route = a.route.replace(":id", paramsUrl)
               return {
                  pathname: route,
                  //search: `?`+ new URLSearchParams({tk: filter.tk,}).toString(),
                  state: { route: a.params.route },
               }
            }

            if (fieldName === 'user_account' && item.user_account) {
               customCell = <Table.Cell key={fieldName} collapsing>
                  <span style={{
                     ...restCol.style,
                  }}
                  >
                     <Popup
                        content={item.user_account}
                        key={item.user_account}
                        trigger={<Icon name="user circle" />}
                     />
                  </span>
               </Table.Cell>
            }
            if (fieldName === 'id_nv') {
               customCell = <Table.Cell key={fieldName} singleLine>
                  <ZenLink style={{ ...restCol.style }}
                     to={linkto({ route: routes.HrHsNs_detail(), params: { id: item.id_nv, route: routes.HrHsNsNV } })}
                     permission=""
                  >
                     {item.id_nv}
                  </ZenLink>
               </Table.Cell>;
            }
            if (fieldName === 'ma_bo_phan') {
               const loai = optionBoPhan.find(t => t.value == item["ma_bo_phan"]) || {}
               customCell = <Table.Cell key={fieldName}>
                  {loai.text || item["ten_bo_phan"]}
               </Table.Cell>
            }
            if (fieldName === 'loai_hsns') {
               const loai = optionLOAIHSNS.find(t => t.value == item["ma_loai_hsns"]) || {}
               customCell = <Table.Cell key={fieldName}>
                  {loai.text || item["ten_loai_hsns"]}
               </Table.Cell>
            }
            if (fieldName === 'trang_thai') {
               const loai = optionTTHSNS.find(t => t.value == item["ma_trang_thai"]) || {}
               customCell = <Table.Cell key={fieldName}>
                  {loai.text || item["ten_trang_thai"]}
               </Table.Cell>
            }
            return customCell
         }
      },
   },

   HrHsNsGVCV: {
      route: routes.HrHsNsGVCV,
      action: {
         view: { visible: true, permission: permissions.HRHsNsGVCV },
         add: { visible: true, permission: permissions.HrHsNsThem },
         edit: { visible: true, permission: permissions.HrHsNsSua },
         del: { visible: true, permission: permissions.HrHsNsXoa },
      },

      linkHeader: {
         id: "hrhsns-gvcv",
         defaultMessage: "Danh sách giáo viên cover",
         active: true,
         isSetting: false,
      },

      tableList: {
         ContainerTop: (props) => {
            const { onLoadData, fieldCode } = props;
            useEffect(() => {
               onLoadData(`ma_bo_phan = 'GVCV'`)
            }, [])
            return <></>
         },
         keyForm: "HRHsNs",
         formModal: ZenLookup.HRHsNs,
         fieldCode: "id_nv",
         unPagination: false,
         showFilterLabel: true,

         api: {
            url: ApiHrHsNs,
            type: "sql"
         },

         columns: [
            { id: "Account", defaultMessage: "Account", fieldName: "user_account", filter: "", visibleColMove: false, sorter: false, custom: true },
            { id: "hrhsns.id_nv", defaultMessage: "ID Nhân sự", fieldName: "id_nv", filter: "string", sorter: true, editForm: true, custom: true },
            { id: "hrdmcctc.ho_ten", defaultMessage: "Họ và tên", fieldName: "ho_ten", filter: "string", sorter: true, },
            { id: "hrdmcctc.gioi_tinh", defaultMessage: "GT", fieldName: "gioi_tinh", filter: "list", sorter: true, listFilter: optionGioiTinh },
            { id: "hrdmcctc.ngay_sinh", defaultMessage: "Ngày sinh", fieldName: "ngay_sinh", filter: "date", sorter: true, },
            { id: "hrdmcctc.dien_thoai", defaultMessage: "Điện thoại", fieldName: "dien_thoai", filter: "string", sorter: true, },
            { id: "hrdmcctc.email", defaultMessage: "Email", fieldName: "email", filter: "string", sorter: true, },
            { id: "hrdmcctc.ten_bo_phan", defaultMessage: "Bộ phận", fieldName: "ma_bo_phan", filter: "list", sorter: true, custom: true, listFilter: optionBoPhan },
            { id: "hrdmcctc.ten_loai_hsns", defaultMessage: "Loại HsNs", fieldName: "loai_hsns", filter: "list", sorter: true, custom: true, listFilter: optionLOAIHSNS },
            { id: "hrdmcctc.ten_trang_thai", defaultMessage: "Trạng thái", fieldName: "trang_thai", filter: "list", sorter: true, custom: true, listFilter: optionTTHSNS },
         ],
         customCols: (data, item, fieldName, { onEdit, ...restCol }) => {
            let customCell = undefined

            const linkto = (a) => {
               let paramsUrl = zzControlHelper.btoaUTF8(a.params.id)
               const route = a.route.replace(":id", paramsUrl)
               return {
                  pathname: route,
                  //search: `?`+ new URLSearchParams({tk: filter.tk,}).toString(),
                  state: { route: a.params.route },
               }
            }

            if (fieldName === 'user_account' && item.user_account) {
               customCell = <Table.Cell key={fieldName} collapsing>
                  <span style={{
                     ...restCol.style,
                  }}
                  >
                     <Popup
                        content={item.user_account}
                        key={item.user_account}
                        trigger={<Icon name="user circle" />}
                     />
                  </span>
               </Table.Cell>
            }
            if (fieldName === 'id_nv') {
               customCell = <Table.Cell key={fieldName} singleLine>
                  <ZenLink style={{ ...restCol.style }}
                     to={linkto({ route: routes.HrHsNs_detail(), params: { id: item.id_nv, route: routes.HrHsNsGVCV } })}
                     permission=""
                  >
                     {item.id_nv}
                  </ZenLink>
               </Table.Cell>;
            }
            if (fieldName === 'ma_bo_phan') {
               const loai = optionBoPhan.find(t => t.value == item["ma_bo_phan"]) || {}
               customCell = <Table.Cell key={fieldName}>
                  {loai.text || item["ten_bo_phan"]}
               </Table.Cell>
            }
            if (fieldName === 'loai_hsns') {
               const loai = optionLOAIHSNS.find(t => t.value == item["ma_loai_hsns"]) || {}
               customCell = <Table.Cell key={fieldName}>
                  {loai.text || item["ten_loai_hsns"]}
               </Table.Cell>
            }
            if (fieldName === 'trang_thai') {
               const loai = optionTTHSNS.find(t => t.value == item["ma_trang_thai"]) || {}
               customCell = <Table.Cell key={fieldName}>
                  {loai.text || item["ten_trang_thai"]}
               </Table.Cell>
            }
            return customCell
         }
      },
   },

   HrLichDay: {
      route: routes.HrLichDay,
      action: {
         view: { visible: true, permission: permissions.HrLichDayXem },
         add: { visible: true, permission: permissions.HrLichDayThem },
         edit: { visible: true, permission: permissions.HrLichDaySua },
         del: { visible: true, permission: permissions.HrLichDayXoa },
      },

      linkHeader: {
         id: "hrlichday",
         defaultMessage: "Lich dạy",
         active: true,
         isSetting: false,
      },

      CustomBtnHeader: (e) => {
         const { btnAdd, btnRefresh } = e;

         function getUrlLichdaylist() {
            return {
               pathname: `${routes.HrLichDay}`,
               //   search: `?`+ new URLSearchParams({tk: filter.tk,}).toString(),
               //   state: { tk: filter.tk },
            }
         }
         function getUrlLichdayWeek() {
            return {
               pathname: `${routes.HrLichDayWeek}`,
               //   search: `?`+ new URLSearchParams({tk: filter.tk,}).toString(),
               //   state: { tk: filter.tk },
            }
         }
         function getUrlLichdayMonth() {
            return {
               pathname: `${routes.HrLichDayMonth}`,
               //   search: `?`+ new URLSearchParams({tk: filter.tk,}).toString(),
               //   state: { tk: filter.tk },
            }
         }

         return <>
            {btnRefresh}
            <div style={{ display: "inline-block", marginLeft: '3px', marginRight: '3px' }}>
               {btnAdd}
            </div>
            <Button.Group>
               <Link to={getUrlLichdaylist()} className=" ui button small primary icon">
                  <Icon name="list" />
               </Link>
               <Link to={getUrlLichdayWeek()} className=" ui button basic small primary icon">
                  <Icon name="calendar outline" />
               </Link>
               <Link to={getUrlLichdayMonth()} className=" ui button basic small primary icon">
                  <Icon name="calendar alternate outline" />
               </Link>

            </Button.Group>
         </>
      },

      tableList: {
         ContainerTop: HrLichDayFilter,
         keyForm: "HrLichDay",
         formModal: ZenLookup.HrLichDay,
         fieldCode: "id",
         unPagination: false,
         changeCode: false,
         showFilterLabel: true,

         api: {
            url: ApiHrLichDay,
            type: "sql"
         },

         columns: [
            { id: "hrhsns.id_nv", defaultMessage: "ID Nhân sự", fieldName: "id_nv", filter: "string", sorter: true, editForm: true, },
            { id: "hrdmcctc.ten_nv", defaultMessage: "Họ và tên", fieldName: "ten_nv", filter: "string", sorter: true, },
            { id: "hrdmcctc.ngay", defaultMessage: "Ngày", fieldName: "ngay", filter: "date", sorter: true, },
            //{ id: "hrdmcctc.ngay_sinh", defaultMessage: "Ngày sinh", fieldName: "ngay_sinh", filter: "date", sorter: true, },
            { id: "hrdmcctc.tu_den", defaultMessage: "Từ - Đến", fieldName: "tu_den", sorter: false, custom: true },
            { id: "hrdmcctc.cham_cong", defaultMessage: "Chấm công", fieldName: "cham_cong", sorter: false, custom: true },
            { id: "hrdmcctc.ma_kh", defaultMessage: "Mã trường", fieldName: "ma_kh", filter: "string", sorter: true, },
            { id: "hrdmcctc.ten_kh", defaultMessage: "Tên trường", fieldName: "ten_kh", filter: "string", sorter: true, },
            { id: "hrdmcctc.ghi_chu", defaultMessage: "Ghi chú", fieldName: "ghi_chu", filter: "string", sorter: true, },
         ],
         customCols: (data, item, fieldName, { onEdit, ...restCol }) => {
            function isValidDate(d) {
               return new Date(d).getFullYear()
            }

            let customCell = undefined

            if (fieldName === 'tu_den') {
               customCell = <Table.Cell key={fieldName} collapsing>
                  {isValidDate(item.ngay) > 1900 &&
                     `${zzControlHelper.formatDateTime(item.tu, 'HH:mm')}
                       -  
                     ${zzControlHelper.formatDateTime(item.den, 'HH:mm')}
                  `}
               </Table.Cell>
            }
            if (fieldName === 'cham_cong') {
               customCell = <Table.Cell key={fieldName} collapsing>
                  {item.cham_cong !== null && item.cham_cong !== 0 && item.cham_cong + ' ' + item.loai}
               </Table.Cell>
            }
            return customCell
         }
      },
   },
   HrDmNgayNghi: {
      route: routes.HrDmNgayNghi,
      action: {
         view: { visible: true, permission: permissions.HrDmNgayNghiXem },
         add: { visible: true, permission: permissions.HrDmNgayNghiThem },
         edit: { visible: true, permission: permissions.HrDmNgayNghiSua },
         del: { visible: true, permission: permissions.HrDmNgayNghiXoa },
      },

      linkHeader: {
         id: "hrdmngaynghi",
         defaultMessage: "Danh mục ngày nghỉ",
         active: true,
         isSetting: true,
      },

      tableList: {
         keyForm: "hrdmngaynghi",
         formModal: ZenLookup.HrDmNgayNghi,
         fieldCode: "id",
         unPagination: false,
         showFilterLabel: true,

         api: {
            url: Apihrdmngaynghi,
            type: "sql"
         },

         columns: [
            { id: "hrdmngaynghi.ly_do", defaultMessage: "Lý do nghỉ", fieldName: "ly_do", filter: "string", sorter: true, editForm: true, },
            { id: "hrdmngaynghi.ten_loai", defaultMessage: "Loại ngày nghỉ", fieldName: "ten_loai", filter: "string", sorter: true, },
            { id: "ksd", defaultMessage: "Không sử dụng", fieldName: "ksd", type: "bool", filter: "", sorter: true },
         ],
      },
   },
   HrDmLyDoNghi: {
      route: routes.HrDmLyDoNghi,
      action: {
         view: { visible: true, permission: permissions.HrDmLyDoNghiXem },
         add: { visible: true, permission: permissions.HrDmLyDoNghiThem },
         edit: { visible: true, permission: permissions.HrDmLyDoNghiSua },
         del: { visible: true, permission: permissions.HrDmLyDoNghiXoa },
      },

      linkHeader: {
         id: "HrDmLyDoNghi",
         defaultMessage: "Danh mục lý do nghỉ",
         active: true,
         isSetting: true,
      },

      tableList: {
         keyForm: "HrDmLyDoNghi",
         formModal: ZenLookup.HrDmLyDoNghi,
         fieldCode: "ma_ld_nghi",
         unPagination: false,
         showFilterLabel: true,

         api: {
            url: ApiHrDmLyDoNghi,
            type: "sql"
         },

         columns: [
            { id: "HrDmLyDoNghi.ma_ld_nghi", defaultMessage: "Mã lý do ", fieldName: "ma_ld_nghi", filter: "string", sorter: true, editForm: true, },
            { id: "HrDmLyDoNghi.ten_ld_nghi", defaultMessage: "Tên lý do ", fieldName: "ten_ld_nghi", filter: "string", sorter: true, },
            { id: "HrDmLyDoNghi.ngay_nghi_doi_da", defaultMessage: "Số ngày được phép nghỉ tối đa", fieldName: "ngay_nghi_doi_da", filter: "string", sorter: true, custom: true },
            { id: "HrDmLyDoNghi.tl_luong", defaultMessage: "Tỷ lệ hưởng lương", fieldName: "tl_luong", filter: "string", sorter: true, },
            { id: "ksd", defaultMessage: "Không sử dụng", fieldName: "ksd", type: "bool", filter: "", sorter: true },
         ],
         customCols: (data, item, fieldName, { onEdit, ...restCol }) => {
            function isValidDate(d) {
               return new Date(d).getFullYear()
            }

            let customCell = undefined

            if (fieldName === 'ngay_nghi_doi_da') {
               customCell = <Table.Cell key={fieldName} collapsing>
                  {item.ngay_nghi_doi_da !== null && item.ngay_nghi_doi_da !== 0 && item.ngay_nghi_doi_da + ' / ' + item.ky_tinh_toi_da}
               </Table.Cell>
            }
            return customCell
         }
      },
   },
}

export { HR_Dictionary };