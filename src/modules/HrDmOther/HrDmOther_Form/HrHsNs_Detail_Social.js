import React, { useEffect, useRef, useState } from "react";
import { Table, Button, Icon, Label, Modal, Form, Header } from "semantic-ui-react";
import { FormattedMessage } from "react-intl";
import { ButtonDelete, ButtonEdit, ZenButton, ZenField, ZenFieldDate, ZenFieldSelect, ZenFieldSelectApi, ZenFieldTextArea, ZenFormik, ZenLink, ZenMessageAlert, ZenMessageToast, ZenModal } from "../../../components/Control";
import { MESSAGES, FormMode } from "utils";
import * as permissions from "../../../constants/permissions";
import { ApiHrQtHopDongLD } from "../Api/ApiHrQtHopDongLD";
import { ZenHelper } from "../../../utils";
import { zzControlHelper } from "../../../components/Control/zzControlHelper";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { ApiFileAttachment } from "../../../Api";
import { ApiHrHsNs } from "../Api";
import _ from "lodash";

const DetailSocialTabList = ({ data, parentCallback }) => {
    const [id, setId] = useState();
    const [index, setIndex] = useState();
    const _isMounted = useRef(true);
    const refFormik = useRef();
    const refUpload = useRef();
    const [realdonly, setReadonly] = useState(true);
    const [error, setError] = useState();
    const [btnLoading, setBtnLoading] = useState();
    const [dataOriginal, setDataOriginal] = useState();
    const [cgMXH, setCgMXH] = useState(false);
    const newItem = { key: '', value: '' };
    useEffect(() => {
        if (data?.anh_dai_dien) { delete data.anh_dai_dien }
        const keys = data ? Object.keys(data) : []
        const keynames = keys.filter((key) => { return key.toLowerCase().indexOf("ten_") !== -1; })
        if (keynames.length > 0) {
            keynames.forEach(keyname => {
                if (data.hasOwnProperty(keyname)) {
                    if (data[keyname] === null || data[keyname] === "") {
                        delete data[keyname]
                    }
                }
            })
        }
        refFormik.current.setValues({
            ...refFormik.current.values,
            ...data,
        })
        setDataOriginal(data)
    }, [data])

    const handleAfterSave = (newItem) => {
        data = newItem
        sendData(newItem)
    }

    const sendData = (value) => {
        parentCallback(value);
    }

    const handleSubmit = (params, formId) => {
        //setBtnLoading(true)
        ApiHrHsNs.updateSocial(params.id_nv, params, res => {
            if (res.status >= 200 && 204 >= res.status) {
                handleAfterSave({ ...params })
                ZenMessageToast.success();
                setReadonly(true)
            } else {
                setError(zzControlHelper.getResponseError(res))
                ZenMessageAlert.error(zzControlHelper.getResponseError(res))
            }
            if (btnLoading) { setBtnLoading() }
        })

        setCgMXH(false)
    }

    const handleChangeSelecthdld = (e, i) => {
        if (i && i.value) {
            refFormik.current.setValues({
                ...refFormik.current.values,
                ma_loai_hdld: i.value,
                ten_loai_hdld: i.options.filter(t => t.value === i.value)[0].ten
            })
        }
    }
    const onRefreshForm = () => {
        refFormik.current.setValues({
            ...refFormik.current.values,
            ...dataOriginal,
        })
        setReadonly(true)
        setCgMXH(false)
    }

    const handleAddContractName = (item) => {
        const itemarr = item ? eval(item) : []
        itemarr.push(Object.assign({}, newItem));
        refFormik.current.setValues({
            ...refFormik.current.values,
            social_ext: JSON.stringify(itemarr)
        })
    }

    const handleRemoveContactName = (item, idx) => {
        const itemarr = eval(item)
        _.remove(itemarr, (i, x) => { return x == idx });
        refFormik.current.setValues({
            ...refFormik.current.values,
            social_ext: JSON.stringify(itemarr)
        })
    }

    const handleContactNameChange = (item, idx, e, dt) => {
        const itemarr = eval(item)
        itemarr[idx][dt.name] = dt.value;
        refFormik.current.setValues({
            ...refFormik.current.values,
            social_ext: JSON.stringify(itemarr)
        })
        setCgMXH(true)
    }


    return <>
        <ZenFormik form={"hrdmttc"} ref={refFormik}
            validation={formValidation}
            initItem={initItem}
            onSubmit={handleSubmit}
        >
            {
                formik => {
                    return <Form>
                        <Form.Group>
                            <ZenField width={8} name="home_page" readOnly={realdonly}
                                props={formik}
                                label="hdld.home_page" defaultlabel="Home Page/Blog"
                            />
                        </Form.Group>
                        <Form.Group>
                            <ZenField width={4} name="facebook" readOnly={realdonly}
                                props={formik}
                                label="hdld.facebook" defaultlabel="Facebook"
                            />
                            <ZenField width={4} name="twitter" props={formik} readOnly={realdonly}
                                label="hdld.twitter" defaultlabel="Twitter"
                            />
                        </Form.Group>
                        <Form.Group>
                            <ZenField width={4} name="linkedin" readOnly={realdonly}
                                props={formik}
                                label="hdld.linkedin" defaultlabel="Linkedin"
                            />
                            <ZenField width={4} name="skype" props={formik} readOnly={realdonly}
                                label="hdld.skype" defaultlabel="Skype"
                            />
                        </Form.Group>
                        <Header style={{ marginBottom: "20px" }} content="Mạng xã hội/liên hệ khác" as='h4' />

                        {formik.values.social_ext && eval(formik.values.social_ext).map((item, idx) => {
                            return <Form.Group>
                                <Form.Field width={3}>
                                    <label>Tên MXH/Liên hệ </label>
                                    <Form.Input
                                        placeholder='Mạng xã hội'
                                        name='key'
                                        value={item['key']}
                                        onChange={(e, data) => handleContactNameChange(formik.values.social_ext, idx, e, data)}
                                    />
                                </Form.Field>
                                <Form.Field width={5}>
                                    <label>&nbsp;</label>
                                    <Form.Input
                                        placeholder='Liên kết mạng xã hội'
                                        name='value'
                                        value={item['value']}
                                        onChange={(e, data) => handleContactNameChange(formik.values.social_ext, idx, e, data)}
                                    />
                                </Form.Field>


                                <Form.Field>
                                    <label>&nbsp;</label>
                                    <ZenButton icon='remove' color='red' basic
                                        permission={permissions.HrHsNsSua}
                                        onClick={() => {handleRemoveContactName(formik.values.social_ext, idx);
                                            refFormik.current.handleSubmit()}}
                                    />
                                </Form.Field>

                            </Form.Group>
                        })}
                        {<a href="#" onClick={(e) => {
                            e.preventDefault();
                            handleAddContractName(formik.values.social_ext);
                        }}>
                            <Icon style={{ marginBottom: "20px" }} name="add" />
                            Thêm MXH/liên hệ khác
                        </a>}
                        { cgMXH &&
                            <div style={{position:"relative",left:"7%",display:"inline-block"}}>
                                <ZenButton btnType="cancel"
                                    size="small"
                                    content="Hủy"
                                    color="red"
                                    onClick={() => onRefreshForm()}
                                />
                                <ZenButton btnType="save"
                                    permission={permissions.HrHsNsSua}
                                    size="small"
                                    type="submit"
                                    onClick={(e) => refFormik.current.handleSubmit(e)}
                                    style={{ margin: "0px" }}
                                    loading={btnLoading}
                                    disabled={btnLoading}
                                />
                            </div>
                        }
                    </Form>

                }
            }
        </ZenFormik>

        <div className="btn-form-right">
            {
                !realdonly ? <>
                    <ZenButton btnType="cancel"
                        size="small"
                        content="Hủy"
                        color="red"
                        onClick={() => onRefreshForm()}
                    />
                    <ZenButton btnType="save"
                        size="small"
                        type="submit"
                        onClick={(e) => refFormik.current.handleSubmit(e)}
                        style={{ margin: "0px" }}
                        loading={btnLoading}
                        disabled={btnLoading}
                    />
                </>
                    : <ZenButton style={{ margin: 0 }}
                        permission={permissions.HrHsNsSua}
                        type={"button"}
                        size="small"
                        primary
                        icon="edit"
                        content={"Sửa thông tin"}
                        onClick={() => setReadonly(false)}
                    />
            }
        </div>
    </>
};

const initItem = {
    id_nv: "",
    home_page: "",
    facebook: "",
    twitter: "",
    linkedin: "",
    skype: "",
    social_ext: []
}

const formValidation = [
]

export default DetailSocialTabList