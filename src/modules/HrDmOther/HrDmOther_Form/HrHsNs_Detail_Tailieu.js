import React, { useEffect, useRef, useState } from "react";
import { Table, Button, Icon, Label, Modal, Form } from "semantic-ui-react";
import { FormattedMessage } from "react-intl";
import {
    ButtonDelete, ButtonEdit, FileAttachment, ZenButton, ZenField, ZenFieldDate,
    ZenFieldSelect, ZenFieldSelectApi, ZenFieldTextArea, ZenFormik, ZenMessageAlert,
    ZenMessageToast, ZenModal
} from "../../../components/Control";
import { MESSAGES, FormMode } from "utils";
import * as permissions from "../../../constants/permissions";
import { ZenHelper } from "../../../utils";
import { zzControlHelper } from "../../../components/Control/zzControlHelper";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { ApiFileAttachment } from "../../../Api";
import { ApiHrQtTaiLieu } from "../Api/ApiHrQtTailieu";
import { ApiHrHsNs } from "../Api";
import { ListFileUpload } from "./listFileUpload";

const DetailTailieuTabList = (props) => {
    const { idnv } = props
    const [ctx, setCtx] = useState([])
    const [open, setOpen] = useState(false)
    const [id, setId] = useState();
    const [index, setIndex] = useState();
    const [error, setError] = useState();
    function fetchRequestTailieu() {
        var result = new Promise((resolve, reject) =>
            ApiHrHsNs.getTailieu(idnv, res => {
                if (res.status >= 200 && 204 >= res.status) {
                    resolve(res.data.data)
                } else {
                    reject(res)
                }
            })
        )
        return result
    }

    let questBefore = (mess, callBack) => {
        let result = ZenMessageAlert.question(mess);
        Promise.all([result])
           .then(values => {
              if (values[0]) {
                 callBack()
              }
           }).catch(err => {
  
           });
     }

    useEffect(() => {
        fetchRequestTailieu().then(data => {
            setCtx(data.filter(dt => dt.id_nv === idnv))
        }).catch(err => {
            setError(ZenHelper.getResponseError(err))
        })
    }, [])

    const openModal = (item, index) => {
        setOpen(true)
        if (item) {
            setId(item.id)
        } else {
            setId()
        }
        if (index) setIndex(index)
    }

    const handleDeleteItem = (e, index) => {
        questBefore(`Bạn muốn xóa Giấy tờ, Tài liệu của ${e.ten_nv} ?`, () => {
            ApiHrQtTaiLieu.delete(e.id, res => {
                if (res.status >= 200 && 204 >= res.status) {
                    const newItems = ctx.filter((t, idx) => idx !== index);
                    setCtx(newItems)
                    ZenMessageToast.success();
                } else {
                    setError(zzControlHelper.getResponseError(res))
                    ZenMessageAlert.error(zzControlHelper.getResponseError(res))
                }
            })
        })
    }

    var findIndex = (id,state) => {
        var result = -1;
        state.forEach((task, index) => {
            if(task.id === id){
                result = index;
            }
        });
        return result;
    }

    const handleAfterUpload = (newItem, id = null) => {
        if (id) {
            if (newItem.file_upload) {
                fetchRequestTailieu().then(data => {
                    setCtx(data.filter(dt => dt.id_nv === idnv))
                }).catch(err => {
                    setError(ZenHelper.getResponseError(err))
                })
                setOpen(false)
            } else {
                var indx = findIndex(newItem.id,ctx)
                ctx[indx] = newItem
                setCtx(ctx)
                setOpen(false)
            }
        } else {
            setCtx(ctx.concat(newItem))
            setOpen(false)
        }
    }

    const handleAfterUploadFiles = (newItem,id,indx,) => {
        const item = ctx[indx]
        if(newItem) {
            if(item.files) {
                item.files = item.files.concat([newItem])
                ctx[indx] = item
                setCtx([].concat(ctx))
            } else {
                item.files = [].concat(newItem)
                ctx[indx] = item
                setCtx([].concat(ctx))
            }
        } else {
            if(!newItem && id) {
                item.files = item.files.filter(t => t.id !== id)
                ctx[indx] = item
                setCtx([].concat(ctx))
            }
        }
    }

    return <>
        <div style={{ float: "right", padding: '0px 0px 7px 0px' }}>
            <ZenButton
                icon="add"
                primary
                content="Thêm mới giấy tờ/tài liệu"
                onClick={() => { openModal() }}
            />
        </div>
        <Table celled selectable striped>
            <Table.Header>
                <Table.Row>
                    <Table.HeaderCell textAlign="center">
                        Loại giấy tờ
                    </Table.HeaderCell>
                    {/* <Table.HeaderCell textAlign="center">
                        Số
                    </Table.HeaderCell> */}
                    <Table.HeaderCell textAlign="center">
                        Ngày cấp
                    </Table.HeaderCell>
                    <Table.HeaderCell textAlign="center">
                        Nơi cấp
                    </Table.HeaderCell>
                    <Table.HeaderCell textAlign="center">
                        Ngày hết hạn
                    </Table.HeaderCell>
                    <Table.HeaderCell textAlign="center">
                        Ghi chú
                    </Table.HeaderCell>
                    <Table.HeaderCell textAlign="center">
                        File kèm theo
                    </Table.HeaderCell>
                    <Table.HeaderCell collapsing></Table.HeaderCell>
                </Table.Row>
            </Table.Header>
            {ctx && (
                <Table.Body>
                    {ctx.map((item, index) => {
                        return (
                            <Table.Row key={item.id}>
                                <Table.Cell>{item.ten_loai_tai_lieu}</Table.Cell>
                                {/* <Table.Cell>{item.so_tai_lieu}</Table.Cell> */}
                                <Table.Cell>
                                    {new Date(item.ngay_cap).getFullYear() < 1901 ? "" : ZenHelper.formatDateTime(item.ngay_cap, 'DD-MM-YYYY')}
                                </Table.Cell>
                                <Table.Cell>{item.noi_cap}</Table.Cell>
                                <Table.Cell>
                                    {new Date(item.ngay_het_han).getFullYear() < 1901 ? "" : ZenHelper.formatDateTime(item.ngay_het_han, 'DD-MM-YYYY')}
                                </Table.Cell>
                                <Table.Cell>{item.ghi_chu}</Table.Cell>
                                <Table.Cell style={{ maxWidth: "150px" }} width="3">
                                    <ListFileUpload 
                                        codeName={'HR'} 
                                        docKey={item.id} 
                                        files={item.files}
                                        indx={index} 
                                        defaultTypefile={'TL'}
                                        handleAfterUpload={handleAfterUploadFiles}
                                    />
                                </Table.Cell>
                                <Table.Cell collapsing>
                                    <Button.Group size="small" style={{ float: "right" }}>
                                        <ButtonEdit
                                            onClick={() => openModal(item, index)}
                                        />
                                        <ButtonDelete
                                            onClick={() => handleDeleteItem(item, index)}
                                        />
                                    </Button.Group>
                                </Table.Cell>
                            </Table.Row>
                        );
                    })}
                </Table.Body>
            )}
        </Table>

        {open && <Modaltailieu open={open}
            id={id}
            idnv={idnv}
            onClose={() => setOpen(false)}
            onAfterSave={handleAfterUpload}
        />}
    </>
};

const Modaltailieu = ({ open, id, idnv, onClose, onAfterSave, title }) => {
    const _isMounted = useRef(true);
    const refFormik = useRef();
    const refUpload = useRef();
    const [attachmentInfo, setAttachmentInfo] = useState();
    const [fileInfo, setFileInfo] = useState();
    const [itemInfo, setItemInfo] = useState();
    const [error, setError] = useState();
    const [roles, setRoles] = useState([]);
    const [btnLoading, setBtnLoading] = useState();

    useEffect(() => {
        zzControlHelper.dragElement(document.getElementById('tailieu-form'), 'header-tailieu-form')
    })

    useEffect(() => {
        if (id) {
            ApiHrQtTaiLieu.getByCode(id, res => {
                if (res.status >= 200 && 204 >= res.status) {
                    refFormik.current.setValues({
                        ...refFormik.current.values,
                        ...res.data.data,
                    })
                } else {
                    setError(zzControlHelper.getResponseError(res))
                }
            })
        } else {
            refFormik.current.setValues({
                ...refFormik.current.values,
                id_nv: idnv,
            })
        }
    }, [])

    const handleSelectedFile = async (e) => {
        if (e.target.value) {
            const fileInfo = await selectedFile(e);
            if (fileInfo) {
                setFileInfo(fileInfo)
            }
        } else {
            setFileInfo()
        }
    }

    // const handleSubmit = (params, formId) => {
    //     setBtnLoading(true)
    //     if (id) {
    //         if (refUpload.current.files[0]) {
    //             const formData = new FormData();
    //             formData.append("id", id)
    //             formData.append("id_nv", params.id_nv)
    //             formData.append("ma_loai_tai_lieu", params.ma_loai_tai_lieu)
    //             formData.append("so_tai_lieu", params.so_tai_lieu)
    //             formData.append("ngay_cap", params.ngay_cap)
    //             formData.append("ngay_het_han", params.ngay_het_han)
    //             formData.append("noi_cap", params.noi_cap)
    //             formData.append("ghi_chu", params.ghi_chu)

    //             formData.append('file_upload', refUpload.current.files[0]);

    //             ApiHrQtTaiLieu.update(formData, res => {
    //                 if (res.status >= 200 && 204 >= res.status) {
    //                     onAfterSave && onAfterSave({ ...params, file_upload: fileInfo }, id)
    //                     ZenMessageToast.success();
    //                 } else {
    //                     setError(zzControlHelper.getResponseError(res))
    //                     ZenMessageAlert.error(zzControlHelper.getResponseError(res))
    //                 }
    //             })
    //         } else {
    //             ZenMessageAlert.warning("Bạn vẫn chưa chọn file")
    //         }
    //     } else {
    //         if (refUpload.current.files[0]) {
    //             const formData = new FormData();
    //             formData.append("id_nv", idnv)
    //             formData.append("ma_loai_tai_lieu", params.ma_loai_tai_lieu)
    //             formData.append("so_tai_lieu", params.so_tai_lieu)
    //             formData.append("ngay_cap", params.ngay_cap)
    //             formData.append("ngay_het_han", params.ngay_het_han)
    //             formData.append("noi_cap", params.noi_cap)
    //             formData.append("ghi_chu", params.ghi_chu)

    //             formData.append('file_upload', refUpload.current.files[0]);

    //             ApiHrQtTaiLieu.insert(formData, res => {
    //                 if (res.status >= 200 && 204 >= res.status) {
    //                     onAfterSave && onAfterSave({ ...res.data.data })
    //                     ZenMessageToast.success();
    //                 } else {
    //                     setError(zzControlHelper.getResponseError(res))
    //                     ZenMessageAlert.error(zzControlHelper.getResponseError(res))
    //                 }
    //             })
    //         } else {
    //             ZenMessageAlert.warning("Bạn vẫn chưa chọn file")
    //         }
    //     }
    //     setBtnLoading(false)
    // }

    const handleSubmit = (params, formId) => {
        setBtnLoading(true)
        if (id) {
            ApiHrQtTaiLieu.update(params, res => {
                if (res.status >= 200 && 204 >= res.status) {
                    onAfterSave && onAfterSave({ ...params, file_upload: fileInfo }, id)
                    ZenMessageToast.success();
                } else {
                    setError(zzControlHelper.getResponseError(res))
                    ZenMessageAlert.error(zzControlHelper.getResponseError(res))
                }
            })
        } else {
            ApiHrQtTaiLieu.insert(params, res => {
                if (res.status >= 200 && 204 >= res.status) {
                    onAfterSave && onAfterSave({ ...res.data.data })
                    ZenMessageToast.success();
                } else {
                    setError(zzControlHelper.getResponseError(res))
                    ZenMessageAlert.error(zzControlHelper.getResponseError(res))
                }
            })
        }
        setBtnLoading(false)
    }

    function selectedFile(e) {
        return new Promise(resolve => {
            if (e.target.files.length > 0) {
                const reader = new FileReader(),
                    file = e.target.files[0];
                reader.onloadend = () => {
                    resolve(file);
                };
                reader.readAsDataURL(file);
            }
        })
    }
    const handleChangeSelecttailieu = (e, i) => {
        if (i && i.value) {
            refFormik.current.setValues({
                ...refFormik.current.values,
                ma_loai_tai_lieu: i.value,
                ten_loai_tai_lieu: i.options.filter(t => t.value === i.value)[0].ten
            })
        }
    }

    const onValidate = (item) => {
        const errors = {};
        const { ngay_cap, ngay_het_han } = item
        const ngay1 = parseInt(ngay_cap) > 1900 ? ngay_cap : null
        const ngay2 = parseInt(ngay_het_han) > 1900 ? ngay_het_han : null
        if (!ngay2 || !ngay1) {
            return
        }
        if (ngay2 <= ngay1) {
            errors.ngay_cap = 'Ngày cấp phải nhỏ hơn Ngày hết hạn';
        }
        return errors
    }

    const customHeader = (id) => {
        const header = 'giấy tờ/tài liệu'
        const txtHeader = !id ? `Thêm mới ${header}` : capitalize(header) + ` : ${id}`
        return txtHeader
    }
    const capitalize = (s) => {
        if (typeof s !== 'string') return ''
        return s.charAt(0).toUpperCase() + s.slice(1)
    }

    return <>
        <Modal id="tailieu-form"
            closeOnEscape closeIcon closeOnDimmerClick={false}
            onClose={() => onClose()}
            //onOpen={() => onClose()}
            open={open}
            size={"tiny"}
        >
            <Modal.Header id='header-tailieu-form' style={{ cursor: "grabbing" }}>
                {customHeader(id)}
            </Modal.Header>

            <Modal.Content>
                <ZenFormik form={"hrdmother"} ref={refFormik}
                    validation={formValidation}
                    initItem={initItem}
                    onSubmit={handleSubmit}
                    onValidate={onValidate}
                >
                    {
                        formik => {
                            return <Form>
                                <Form.Group>
                                    <ZenFieldSelectApi width={8} name="ma_loai_tai_lieu"
                                        required
                                        lookup={{ ...ZenLookup.MA_LOAI_TAI_LIEU, format: `{ten}` }}
                                        formik={formik}
                                        onChange={handleChangeSelecttailieu}
                                        label="hdld.ma_loai_tai_lieu" defaultlabel="Loại giấy tờ"
                                    />
                                    <ZenField width={8} name="so_tai_lieu" props={formik}
                                        label="hdld.so_tai_lieu" defaultlabel="Số"
                                        isCode
                                    />
                                </Form.Group>
                                <Form.Group>
                                    <ZenFieldDate width={8} name="ngay_cap" props={formik}
                                        label="hdld.ngay_cap" defaultlabel="Ngày cấp"
                                    />
                                    <ZenFieldDate width={8} name="ngay_het_han" props={formik}
                                        label="hdld.ngay_het_han" defaultlabel="Ngày hết hạn"
                                    />
                                </Form.Group>
                                <Form.Group>
                                    <ZenField width={16} name="noi_cap" props={formik}
                                        label="hdld.noi_cap" defaultlabel="Nơi cấp"
                                    />
                                </Form.Group>
                                <Form.Group>
                                    <ZenFieldTextArea width={16} name="ghi_chu" props={formik}
                                        label="hdld.ghi_chu" defaultlabel="Ghi chú"
                                    />
                                </Form.Group>
                            </Form>
                        }
                    }
                </ZenFormik>
                {/* <div style={{ paddingTop: "7px" }}>
                    <Form>
                        <Form.Field>
                            <label>Tập tin kèm theo</label>
                            <div className="ui input">
                                {fileInfo && <input
                                    // <Icon name="file alternate outline" />
                                    value={fileInfo.name}
                                />}
                                <a style={{ cursor: "pointer", paddingTop: "10px" }}
                                    onClick={() => refUpload.current.click()}>
                                    <Icon name="attach" />
                                    <FormattedMessage id="fileAttach" defaultMessage="File kèm theo" />
                                </a>
                            </div>
                        </Form.Field>
                    </Form>
                </div> */}

                {/* <input hidden type="file"
                    ref={refUpload}
                    onChange={handleSelectedFile} /> */}
            </Modal.Content>
            <Modal.Actions>
                <ZenButton btnType={"cancel"} size="small" onClick={() => onClose()} />
                <ZenButton btnType="save" size="small"
                    loading={btnLoading} type="submit"
                    onClick={(e) => refFormik.current.handleSubmit(e)}
                />
            </Modal.Actions>
        </Modal>
    </>
}

const initItem = {
    id_nv: "",
    ma_loai_tai_lieu: "",
    so_tai_lieu: "",
    ngay_cap: "",
    noi_cap: "",
    ngay_het_han: "",
    ghi_chu: "",
}

const formValidation = [
    {
        id: "ma_loai_tai_lieu",
        validationType: "string",
        validations: [
            {
                type: "required",
                params: ["Không được bỏ trống trường này"]
            },
        ]
    },
    // {
    //     id: "so_tai_lieu",
    //     validationType: "string",
    //     validations: [
    //         {
    //             type: "required",
    //             params: ["Không được bỏ trống trường này"]
    //         },
    //     ]
    // },
    // {
    //     id: "ngay_cap",
    //     validationType: "date",
    //     validations: [
    //         {
    //             type: "required",
    //             params: ["Không được bỏ trống trường này"]
    //         },
    //     ]
    // },
    // {
    //     id: "ngay_het_han",
    //     validationType: "date",
    //     validations: [
    //         {
    //             type: "required",
    //             params: ["Không được bỏ trống trường này"]
    //         },
    //     ]
    // },
]

export default DetailTailieuTabList