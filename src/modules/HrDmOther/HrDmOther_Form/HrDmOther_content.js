import React, { forwardRef, memo, useEffect, useImperativeHandle, useMemo, useRef, useState } from "react";
import { FormattedMessage } from "react-intl";
import { Checkbox, Header, Form, Icon, Message, Table, Button } from "semantic-ui-react";
import { ScrollManager, ZenFieldCheckbox, ZenLink, ZenModal, ZenFormik, ZenField, ZenButton, ZenMessageToast, ZenMessageAlert, ButtonEdit, ButtonDelete, ZenFieldNumber } from "../../../components/Control";
import { Apihrdmkhac } from "../Api/Apihrdmkhac";
import { ZenHelper } from '../../../utils';
import { ApiConfig } from "../../../Api";
import { zzControlHelper } from "../../../components/Control/zzControlHelper";
import './HRDmOther.css'
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import * as permissions from "../../../constants/permissions";

const HrDmOtherContent = ({ modules, activeModule, keyword }) => {
    //const { modules, activeModule, keyword } = props;
    const [itemmenu, setItemmenu] = useState([])
    const [docKey, setDockey] = useState();
    const [index, setIndex] = useState();
    const [error, setError] = useState()
    const [open, setOpen] = useState(false)
    const [openChangeCode, setOpenChangecode] = useState(false)

    function fetchRequestList(code) {
        var result = new Promise((resolve, reject) =>
            Apihrdmkhac.getList(code, res => {
                if (res.status >= 200 && 204 >= res.status) {
                    resolve(res.data.data)
                } else {
                    reject(res)
                }
            })
        )
        return result
    }

    const handleOpenModal = (e, index) => {
        setDockey(e.ma)
        setIndex(index)
        setOpen(true)
    }

    const openModal = (item, index) => {
        if (item) {
            setDockey(item.ma)
        } else {
            setDockey()
        }
        setIndex(index)
        setOpen(true)
    }

    const openModalChangecode = (item, index) => {
        if (item) {
            setDockey(item.ma)
        } else {
            setDockey()
        }
        if (index) setIndex(index)
        setOpenChangecode(true)
    }

    const handleDeleteItem = (e, index) => {
        Apihrdmkhac.delete({ code_name: activeModule, ma: e.ma }, res => {
            if (res.status >= 200 && 204 >= res.status) {
                const newItems = itemmenu.filter((t, idx) => idx !== index);
                setItemmenu(newItems)
                ZenMessageToast.success();
            } else {
                setError(zzControlHelper.getResponseError(res))
                ZenMessageAlert.error(zzControlHelper.getResponseError(res))
            }
        })
    }

    const showItemsMenu = (itemmenu) => {
        return itemmenu.map((item, index) => {
            return (
                <Table.Row key={item.code_name + index}>
                    <Table.Cell content={index + 1} textAlign="center" />
                    <Table.Cell>
                        <a style={{ cursor: "pointer", fontWeight: "bold" }} index={index} ma={item.ma} onClick={() => handleOpenModal(item, index)}>{item.ma}</a>
                    </Table.Cell>
                    <Table.Cell>{item.ten}</Table.Cell>
                    <Table.Cell>{item.ordinal}</Table.Cell>
                    <Table.Cell>
                        <Checkbox readOnly={true}
                            name="ksd"
                            checked={item.ksd}
                        />
                    </Table.Cell>
                    <Table.Cell collapsing>
                        <Button.Group size="small" style={{ float: "right" }}>
                            <ButtonEdit permission={permissions.HRDmKhacSua}
                                onClick={() => openModal(item, index)}
                            />
                            <ButtonEdit permission={permissions.HRDmKhacSua}
                                icon={"exchange"}
                                basic
                                color={"black"}
                                onClick={() => openModalChangecode(item, index)}
                            />
                            <ButtonDelete permission={permissions.HRDmKhacXoa}
                                onClick={() => handleDeleteItem(item, index)}
                            />
                        </Button.Group>
                    </Table.Cell>
                    {/* <Icon style={{ cursor: "pointer" }} name="trash" ma={item.ma} index={index} onClick={(e) => handleDeleteItem(e, index)} /> */}
                </Table.Row>
            );
        })
    }

    useEffect(() => {
        if (activeModule) {
            fetchRequestList(activeModule).then(data => {
                setItemmenu(data)
            }).catch(err => {
                setError(ZenHelper.getResponseError(err))
            })
        }
    }, [activeModule])

    // function goi gu ben ngoai
    // useImperativeHandle(ref, () => ({
    //     /** Open modal */
    //     openModal() {
    //         setOpen(true)
    //         setDockey()
    //     },
    //     refreshApi() {
    //         if (activeModule) {
    //             fetchRequestList(activeModule).then(data => {
    //                 setItemmenu(data)
    //             }).catch(err => {
    //                 setError(ZenHelper.getResponseError(err))
    //             })
    //         }
    //     }
    // }));
    const refreshApi = () => {
        if (activeModule) {
            fetchRequestList(activeModule).then(data => {
                setItemmenu(data)
            }).catch(err => {
                setError(ZenHelper.getResponseError(err))
            })
        }
    }
    const handleAfterUpload = (newItem, id = null) => {
        if (id) {
            itemmenu[index].ten = newItem.ten
            itemmenu[index].ksd = newItem.ksd
            itemmenu[index].ordinal = newItem.ordinal
            setItemmenu(itemmenu)
            setOpen(false)
        } else {
            setItemmenu(itemmenu.concat(newItem))
            setOpen(false)
        }
    }

    const handleChangeCodeAfter = (currentItem, code) => {
        const idx = itemmenu.findIndex(
            (t) => t['ma'] == currentItem['ma']
        );
        if (idx > -1) {
            itemmenu[idx]['ma'] = code;
            setItemmenu(itemmenu)
        }
        setOpenChangecode(false)
    }
    return (
        <>
            <div style={{ padding: "0em 1.2em" }}>
                <div style={{ backgroundColor: "#FFFFFF" }} className="HR-header">
                    <div className="HR-header-content">
                        <Header size='medium'>
                            {activeModule ?
                                modules && modules.filter(module => module.code_name == activeModule)[0].name : ""
                            }
                        </Header>
                        <div>
                            <ZenButton
                                btnType="refresh"
                                size="small"
                                onClick={() => refreshApi()}
                            />
                            <ZenButton permission={permissions.HRDmKhacThem}
                                btnType="add"
                                size="small"
                                onClick={() => openModal()}
                            />
                        </div>
                    </div>
                </div>
                <div>
                    <Table celled striped definition selectable textAlign="left">
                        <Table.Header fullWidth >
                            <Table.Row>
                                <Table.HeaderCell width="1" className="HR-sticky-top" />
                                <Table.HeaderCell className="HR-sticky-top" >
                                    <FormattedMessage id="hrdmkhac.code" defaultMessage="Mã" />
                                </Table.HeaderCell>
                                <Table.HeaderCell className="HR-sticky-top">
                                    <FormattedMessage
                                        id="hrdmkhac.name"
                                        defaultMessage="Tên"
                                    />
                                </Table.HeaderCell>
                                <Table.HeaderCell className="HR-sticky-top">
                                    <FormattedMessage
                                        id="hrdmkhac.ordinal"
                                        defaultMessage="Thứ tự hiển thị"
                                    />
                                </Table.HeaderCell>
                                <Table.HeaderCell className="HR-sticky-top">
                                    <FormattedMessage
                                        id="hrdmkhac.ksd"
                                        defaultMessage="Không SD"
                                    />
                                </Table.HeaderCell>
                                <Table.HeaderCell className="HR-sticky-top" width="1">
                                </Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>

                        <Table.Body>
                            {showItemsMenu(itemmenu)}
                        </Table.Body>
                    </Table>

                    {open && <ModalHRDmOther open={open}
                        codeName={activeModule} docKey={docKey} index={index}
                        onClose={() => setOpen(false)}
                        onAfterSave={handleAfterUpload}
                        title={activeModule ?
                            modules && modules.filter(module => module.code_name == activeModule)[0].name : ""
                        }
                    />}
                    {openChangeCode && (
                        <ModalChangeCode
                            apiChangeCode={ApiConfig.changeCode}
                            codeName={activeModule}
                            docKey={docKey}
                            lookup={ZenLookup.HrDmKhac}
                            open={openChangeCode}
                            onClose={() =>
                                setOpenChangecode(false)
                                //    : false, currentItem: {}
                            }
                            onAfterSave={handleChangeCodeAfter}
                        />
                    )}
                </div>
            </div>
        </>
    );
}

const ModalHRDmOther = ({ open, codeName, docKey, onClose, onAfterSave, title }) => {
    const _isMounted = useRef(true);
    const refFormik = useRef();
    const [itemInfo, setItemInfo] = useState();
    const [error, setError] = useState();
    const [btnLoading, setBtnLoading] = useState();

    useEffect(() => {
        if (codeName && docKey) {
            Apihrdmkhac.getByCode(codeName, docKey, res => {
                if (_isMounted.current) {
                    if (res.status === 200) {
                        setItemInfo(res.data.data)
                        refFormik.current.setValues({
                            ...refFormik.current.values,
                            ...res.data.data,
                        })
                        if (error) setError()
                    } else {
                        setError(zzControlHelper.getResponseError(res))
                    }
                }
            })
        } else {
            refFormik.current.setValues({})
        }
        return (() => {
            _isMounted.current = false
        })
    }, [codeName])

    const handleSubmit = (params, formId) => {
        if (docKey) {
            setBtnLoading(true)
            Apihrdmkhac.update({ code_name: codeName, ...params }, res => {
                if (res.status >= 200 && 204 >= res.status) {
                    onAfterSave && onAfterSave({ code_name: codeName, ...params }, docKey)
                    ZenMessageToast.success();
                } else {
                    setError(zzControlHelper.getResponseError(res))
                    ZenMessageAlert.error(zzControlHelper.getResponseError(res))
                }
                if (btnLoading) setBtnLoading()
            })
        } else {
            setBtnLoading(true)
            Apihrdmkhac.insert({ code_name: codeName, ...params }, res => {
                if (res.status >= 200 && 204 >= res.status) {
                    onAfterSave && onAfterSave({ ...res.data.data })
                    ZenMessageToast.success();
                } else {
                    setError(zzControlHelper.getResponseError(res))
                    ZenMessageAlert.error(zzControlHelper.getResponseError(res))
                }
                if (btnLoading) setBtnLoading()
            })
        }
    }

    const customHeader = (title, id) => {
        const header = title.replace('Danh mục ', '')
        const txtHeader = !id ? `Thêm ${header}` : capitalize(header) + ` : ${id}`
        return txtHeader
    }

    const capitalize = (s) => {
        if (typeof s !== 'string') return ''
        return s.charAt(0).toUpperCase() + s.slice(1)
    }

    return <ZenModal
        open={open} onClose={() => onClose()} size="tiny"
        error={error ?
            <Message negative list={error} onDismiss={() => setError()} />
            : undefined}

        header={<FormattedMessage id="hrdmother.add" defaultMessage={customHeader(title, docKey)} />}

        content={<>
            <ZenFormik form={"hrdmother"} ref={refFormik}
                validation={formValidation}
                initItem={initItem}
                onSubmit={handleSubmit}
            >
                {
                    formik => {
                        return <Form>
                            <Form.Group>
                                <ZenField width={8} name="ma" props={formik} readOnly={!docKey ? false : true}
                                    label="hrdmother.ma" defaultlabel="Mã"
                                />
                                <ZenFieldNumber width={8}
                                    name="ordinal"
                                    props={formik}
                                    label="hrdmother.ordinal" defaultlabel="Thứ tự hiển thị"
                                />
                            </Form.Group>
                            <Form.Group>
                                <ZenField width={16} name="ten" props={formik}
                                    label="hrdmother.ten" defaultlabel="Tên"
                                />
                            </Form.Group>
                            {/* <div className='HR-field-checkbox'> */}
                            <ZenFieldCheckbox
                                tabIndex={-1}
                                label="hrdmother.ksd"
                                defaultlabel="Không sử dụng"
                                name='ksd'
                                props={formik}
                            />
                            {/* </div> */}
                        </Form>
                    }
                }
            </ZenFormik>
        </>}
        actions={<>
            <ZenButton btnType="save" size="small"
                loading={btnLoading} type="submit"
                onClick={(e) => refFormik.current.handleSubmit(e)}
            />
        </>}
    />
}

const ModalChangeCode = ({
    open,
    docKey,
    codeName,
    apiChangeCode,
    lookup,
    onClose,
    onAfterSave,
}) => {
    const _isMounted = useRef(true);
    const [value, setValue] = useState();
    const [error, setError] = useState();
    const [btnSave, setBtnSave] = useState(false);
    const [currentItem, setCurrentItem] = useState({});

    useEffect(() => {
        if (codeName && docKey) {
            Apihrdmkhac.getByCode(codeName, docKey, res => {
                if (_isMounted.current) {
                    if (res.status === 200) {
                        setCurrentItem(res.data.data)
                        if (error) setError()
                    } else {
                        setError(zzControlHelper.getResponseError(res))
                    }
                }
            })
        }
        return (() => {
            _isMounted.current = false
        })
    }, [codeName])

    const handleChangeCode = () => {
        if (!value) {
            ZenMessageAlert.error("Vui lòng nhập mã mới");
            return;
        }

        setBtnSave(true);
        const data = {
            code_name: codeName || lookup.code,
            new_value: value,
            old_value: currentItem[lookup.value],
        };
        apiChangeCode(data, (res) => {
            if (res.status >= 200 && res.status <= 204) {
                onAfterSave(currentItem, value);
                ZenMessageToast.success();
            } else {
                setBtnSave(false);
                setError(zzControlHelper.getResponseError(res));
            }
        });
    };

    return (
        <ZenModal
            open={open}
            size="tiny"
            onClose={onClose}
            header="Đổi mã"
            content={
                <>
                    {error && <Message negative list={error} />}

                    <Form>
                        <Form.Input
                            readOnly
                            value={currentItem[lookup.value]}
                            label="Mã cũ"
                        />
                        <Form.Input
                            onChange={(e, { value }) => {
                                setValue(value?.toUpperCase());
                            }}
                            input={{
                                style: { textTransform: "uppercase" },
                            }}
                            label="Mã mới"
                            placeholder="Nhập mã mới"
                            autoFocus
                        />
                    </Form>
                </>
            }
            actions={
                <Button
                    type="button"
                    primary
                    content="Xác nhận"
                    size="small"
                    loading={btnSave}
                    disabled={btnSave}
                    onClick={handleChangeCode}
                />
            }
        />
    );
};

const initItem = {
    ma: "",
    ten: "",
    ksd: false,
    ordinal: "",
}

const formValidation = [
    {
        id: "ma",
        validationType: "string",
        validations: [
            {
                type: "required",
                params: ["Không được bỏ trống trường này"]
            },
        ]
    },
    {
        id: "ten",
        validationType: "string",
        validations: [
            {
                type: "required",
                params: ["Không được bỏ trống trường này"]
            },
        ]
    },
]

export default HrDmOtherContent