import React, { useEffect, useRef, useState } from "react";
import { Table, Button, Icon, Label, Modal, Form } from "semantic-ui-react";
import { FormattedMessage } from "react-intl";
import { ButtonDelete, ButtonEdit, ZenButton, ZenField, ZenFieldDate, ZenFieldSelect, ZenFieldSelectApi, ZenFieldTextArea, ZenFormik, ZenMessageAlert, ZenMessageToast, ZenModal } from "../../../components/Control";
import { MESSAGES, FormMode } from "utils";
import * as permissions from "../../../constants/permissions";
import { ApiHrQtHopDongLD } from "../Api/ApiHrQtHopDongLD";
import { ZenHelper } from "../../../utils";
import { zzControlHelper } from "../../../components/Control/zzControlHelper";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { ApiFileAttachment } from "../../../Api";
import { ApiHrHsNs } from "../Api";

const DetailttcTabList = ({ data, parentCallback }) => {
    const [id, setId] = useState();
    const [index, setIndex] = useState();
    const _isMounted = useRef(true);
    const refFormik = useRef();
    const refUpload = useRef();
    const [readonly, setReadonly] = useState(true);
    const [error, setError] = useState();
    const [btnLoading, setBtnLoading] = useState();
    const [dataOriginal, setDataOriginal] = useState();
    useEffect(() => {
        if (data?.anh_dai_dien) { delete data.anh_dai_dien }
        const keys = data ? Object.keys(data) : []
        const keynames = keys.filter((key) => { return key.toLowerCase().indexOf("ten_") !== -1; })
        if (keynames.length > 0) {
            keynames.forEach(keyname => {
                if (data.hasOwnProperty(keyname)) {
                    if (data[keyname] === null || data[keyname] === "") {
                        delete data[keyname]
                    }
                }
            })
        }
        refFormik.current.setValues({
            ...refFormik.current.values,
            ...data,
        })
        setDataOriginal(data)
    }, [data])

    const handleAfterSave = (newItem) => {
        data = newItem
        sendData(newItem)
    }

    const sendData = (value) => {
        parentCallback(value);
    }

    const handleSubmit = (params, formId) => {
        //setBtnLoading(true)
        ApiHrHsNs.update(params, res => {
            if (res.status >= 200 && 204 >= res.status) {
                handleAfterSave({ ...params })
                ZenMessageToast.success();
                setReadonly(true)
            } else {
                setError(zzControlHelper.getResponseError(res))
                ZenMessageAlert.error(zzControlHelper.getResponseError(res))
            }
            if (btnLoading) { setBtnLoading() }
        })

    }

    const handleChangeSelecthdld = (e, i) => {
        if (i && i.value) {
            refFormik.current.setValues({
                ...refFormik.current.values,
                ma_loai_hdld: i.value,
                ten_loai_hdld: i.options.filter(t => t.value === i.value)[0].ten
            })
        }
    }
    const onRefreshForm = () => {
        refFormik.current.setValues({
            ...refFormik.current.values,
            ...dataOriginal,
        })
        setReadonly(true)
    }

    return <>
        <ZenFormik form={"hrdmttc"} ref={refFormik}
            validation={formValidation}
            initItem={initItem}
            onSubmit={handleSubmit}
        >
            {
                formik => {
                    return <Form>
                        <Form.Group>
                            <ZenField width={5} name="id_nv" readOnly={true}
                                props={formik}
                                label="hdld.nguoi_ky" defaultlabel="ID nhân sư"
                            />
                            <ZenField width={5} name="ma_nvns" readOnly={readonly}
                                props={formik}
                                label="hdld.ma_nvns" defaultlabel="Mã nhân viên"
                                isCode
                            />
                            <ZenFieldSelectApi width={3} name="loai_hsns" readOnly={readonly}
                                lookup={{
                                    ...ZenLookup.SIDmloai, format: "{ten}",
                                    onLocalWhere: (items) => {
                                        return items.filter(t => t.ma_nhom === 'LOAI_HSNS') || []
                                    },
                                    where: "MA_NHOM = 'LOAI_HSNS'"
                                }}
                                formik={formik}
                                //onChange={handleChangeSelecthdld}
                                label="hdld.ma_loaihdld" defaultlabel="Loại hồ sơ"
                            />
                            <ZenFieldSelectApi width={3} name="trang_thai" readOnly={readonly}
                                lookup={{
                                    ...ZenLookup.SIDmloai, format: "{ten}",
                                    onLocalWhere: (items) => {
                                        return items.filter(t => t.ma_nhom === 'TT_HSNS') || []
                                    },
                                    where: "MA_NHOM = 'TT_HSNS'"
                                }}
                                formik={formik}
                                //onChange={handleChangeSelecthdld}
                                label="hdld.trang_thai" defaultlabel="Trạng thái"
                            />
                        </Form.Group>
                        <Form.Group>
                            <ZenField width={5} name="ho_ten" props={formik} readOnly={readonly}
                                label="hdld.ho_ten" defaultlabel="Họ và tên"
                            />
                            <ZenFieldSelectApi width={5} name="gioi_tinh" readOnly={readonly}
                                lookup={{
                                    ...ZenLookup.SIDmloai, format: "{ten}",
                                    onLocalWhere: (items) => {
                                        return items.filter(t => t.ma_nhom === 'GIOI_TINH') || []
                                    },
                                    where: "MA_NHOM = 'GIOI_TINH'"
                                }}
                                label={"HRHsNs.gioi_tinh"} defaultlabel="Giới tính"
                                formik={formik}
                            />
                            <ZenFieldSelectApi width={6} name="ma_tt_hon_nhan" readOnly={readonly}

                                // lookup={{
                                //     ...ZenLookup.HrDmKhac, format: "{ten}",
                                //     where: "code_name = 'MA_TTHN'"
                                // }}
                                lookup={{ ...ZenLookup.MA_TTHN, format: `{ten}` }}
                                formik={formik}
                                label="hdld.ma_tt_hon_nhan" defaultlabel="Tình trạng hôn nhân"
                            />
                        </Form.Group>
                        <Form.Group>
                            <ZenFieldDate width={5} readOnly={readonly}
                                label={"HRHsNh.ngay_sinh"} defaultlabel="Ngày sinh"
                                props={formik}
                                name="ngay_sinh"
                            />
                            <ZenField width={5} readOnly={readonly}
                                label={"HRHsNh"} defaultlabel="Nơi sinh"
                                name="noi_sinh" props={formik}
                            />
                            <ZenField width={6} name="que_quan" props={formik}
                                label="hdld.que_quan" defaultlabel="Quê quán"
                            />
                        </Form.Group>
                        <Form.Group>
                            <ZenField width={5} readOnly={readonly}
                                label={"HRHsNh"} defaultlabel="Số CMND/CCCD"
                                name="so_cmnd" props={formik}
                            />
                            <ZenFieldDate width={5} name="ngay_cap_cmnd" props={formik}

                                label="hdld.ngay_cap_cmnd" defaultlabel="Ngày cấp CMND"
                            />
                            <ZenField width={6} name="noi_cap_cmnd" props={formik}
                                label="hdld.noi_cap_cmnd" defaultlabel="Nơi cấp"
                            />
                        </Form.Group>
                        <Form.Group>
                            <ZenField width={5} readOnly={readonly}
                                label={"HRHsNh.email"} defaultlabel="Email"
                                name="email" props={formik}
                            />
                            <ZenField width={5} readOnly={readonly}
                                label={"HRHsNh.so_dien_thoai"} defaultlabel="Điện thoại"
                                name="dien_thoai" props={formik}
                            />
                        </Form.Group>
                        <Form.Group>
                            <ZenField width={10} readOnly={readonly}
                                label={"HRHsNh"} defaultlabel="Địa chỉ thường trú"
                                name="ho_khau" props={formik}
                            />
                        </Form.Group>
                        <Form.Group>
                            <ZenField width={10} readOnly={readonly}
                                label={"HRHsNh"} defaultlabel="Địa chỉ cư trú"
                                name="noi_o" props={formik}
                            />
                        </Form.Group>
                        <Form.Group>
                            <ZenFieldSelectApi width={5} name="ma_quoc_tich" readOnly={readonly}

                                lookup={{ ...ZenLookup.SIDmQuocGia, format: `{ten_quoc_gia}` }}
                                formik={formik}
                                label="hdld.ma_quoc_tich" defaultlabel="Quốc tịch"
                            />
                            <ZenFieldSelectApi width={5} name="ma_dan_toc" readOnly={readonly}

                                lookup={{ ...ZenLookup.Ma_Dan_Toc, format: `{ten}` }}
                                formik={formik}
                                label="hdld.ma_dan_toc" defaultlabel="Dân tộc"
                            />
                            <ZenFieldSelectApi width={6} name="ma_ton_giao" readOnly={readonly}

                                lookup={{ ...ZenLookup.MA_TON_GIAO, format: `{ten}` }}
                                formik={formik}
                                label="hdld.ma_ton_giao" defaultlabel="Tôn giáo"
                            />
                        </Form.Group>
                        <Form.Group>
                            <ZenFieldDate width={5} name="ngay_lam_ct" readOnly={readonly}
                                props={formik}
                                label="hdld.ngay_lam_ct" defaultlabel="Ngày vào làm"
                            />
                            <ZenFieldSelectApi width={5} readOnly={readonly}
                                lookup={{ ...ZenLookup.HRDmcctc, format: "{ten_cctc}",
                                    where: 'chi_tiet = 1',
                                    onLocalWhere: (items) => {
                                        return items.filter(t => t.chi_tiet == 1) || []
                                    },
                                }}
                                name={'ma_bo_phan'}
                                formik={formik}
                                label="hdld.ma_bo_phan" defaultlabel="Phòng ban/bộ phận"
                            />
                            <ZenFieldSelectApi width={6} name="ma_cvcm" readOnly={readonly}

                                lookup={{ ...ZenLookup.MA_CVCM, format: `{ten}` }}
                                formik={formik}
                                label="hdld.ma_cvcm" defaultlabel="Chức vụ"
                            />
                        </Form.Group>
                        <Form.Group>
                            <ZenField width={5} readOnly={readonly}
                                label={"HRHsNh.email"} defaultlabel="Tài khoản ngân hàng"
                                name="so_tk_ngh" props={formik}
                            />
                            <ZenFieldSelectApi width={5} name="ma_ngh" readOnly={readonly}

                                lookup={{ ...ZenLookup.Ma_ngh, format: "{ten_ngh}", }}
                                formik={formik}
                                label="hdld.ma_ngh" defaultlabel="Ngân hàng"
                            />
                            <ZenField width={6} readOnly={readonly}
                                label={"HRHsNh.so_dien_thoai"} defaultlabel="Chủ tài khoản"
                                name="chu_tk_ngh" props={formik}
                            />
                        </Form.Group>
                    </Form>
                }
            }
        </ZenFormik>

        <div className="btn-form-right">
            {
                !readonly ? <>
                    <ZenButton btnType="cancel"
                        size="small"
                        content="Hủy"
                        color="red"
                        onClick={() => onRefreshForm()}
                    />
                    <ZenButton btnType="save"
                        size="small"
                        type="submit"
                        onClick={(e) => refFormik.current.handleSubmit(e)}
                        style={{ margin: "0px" }}
                        loading={btnLoading}
                        disabled={btnLoading}
                    />
                </>
                    : <ZenButton style={{ margin: 0 }}
                        permission={permissions.HrHsNsSua}
                        type={"button"}
                        size="small"
                        primary
                        icon="edit"
                        content={"Sửa thông tin chung"}
                        onClick={() => setReadonly(false)}
                    />
            }
        </div>
    </>
};

const initItem = {
    id_nv: "",
    loai_hsns: "",
    ma_nvns: "",
    ma_cham_cong: "",
    ma_so_thue: "",
    bi_danh: "",
    ho_ten: "",
    ho_dem: "",
    ngay_sinh: "",
    gioi_tinh: "",
    ma_tt_hon_nhan: "",
    so_cmnd: "",
    ngay_cap_cmnd: "",
    noi_cap_cmnd: "",
    noi_sinh: "",
    email: "",
    dien_thoai: "",
    ho_khau: "",
    noi_o: "",
    ma_quoc_tich: "",
    ma_dan_toc: "",
    ma_ton_giao: "",
    ma_bo_phan: "",
    ma_cvcm: "",
    so_tk_ngh: "",
    ma_ngh: "",
    chu_tk_ngh: "",
    trang_thai: "",
}

const formValidation = [
    {
        id: "id_nv",
        validationType: "string",
        validations: [
            {
                type: "required",
                params: ["Không được bỏ trống trường này"]
            },
        ]
    },
    {
        id: "ho_ten",
        validationType: "string",
        validations: [
            {
                type: "required",
                params: ["Không được bỏ trống trường này"]
            },
        ]
    },
    {
        id: "loai_hsns",
        validationType: "string",
        validations: [
            {
                type: "required",
                params: ["Không được bỏ trống trường này"]
            },
        ]
    },
    {
        id: "trang_thai",
        validationType: "string",
        validations: [
            {
                type: "required",
                params: ["Không được bỏ trống trường này"]
            },
        ]
    },
]

export default DetailttcTabList