import React, { useEffect, useRef, useState } from "react";
import { Table, Button, Icon, Label, Modal, Form } from "semantic-ui-react";
import { FormattedMessage } from "react-intl";
import { ZenFieldNumber, ButtonDelete, ButtonEdit, ZenButton, ZenField, ZenFieldDate, ZenFieldSelect, ZenFieldSelectApi, ZenFieldTextArea, ZenFormik, ZenMessageAlert, ZenMessageToast, ZenModal } from "../../../components/Control";
import { MESSAGES, FormMode } from "utils";
import * as permissions from "../../../constants/permissions";
import { ApiHrQtHopDongLD } from "../Api/ApiHrQtHopDongLD";
import { ZenHelper } from "../../../utils";
import { zzControlHelper } from "../../../components/Control/zzControlHelper";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { ApiFileAttachment } from "../../../Api";
import { ApiHrHsNs } from "../Api";
import { ListFileUpload } from "./listFileUpload";

const DetailhdldTabList = (props) => {
    const { idnv } = props
    const [ctx, setCtx] = useState([])
    const [open, setOpen] = useState(false)
    const [id, setId] = useState();
    const [index, setIndex] = useState();
    const [error, setError] = useState();
    function fetchRequestHdld() {
        var result = new Promise((resolve, reject) =>
            ApiHrHsNs.getHdld(idnv, res => {
                if (res.status >= 200 && 204 >= res.status) {
                    resolve(res.data.data)
                } else {
                    reject(res)
                }
            })
        )
        return result
    }

    let questBefore = (mess, callBack) => {
        let result = ZenMessageAlert.question(mess);
        Promise.all([result])
            .then(values => {
                if (values[0]) {
                    callBack()
                }
            }).catch(err => {

            });
    }

    useEffect(() => {
        fetchRequestHdld().then(data => {
            setCtx(data.filter(dt => dt.id_nv === idnv))
        }).catch(err => {
            setError(ZenHelper.getResponseError(err))
        })
    }, [])

    const openModal = (item, index) => {
        if (item) {
            setId(item.id)
        } else {
            setId()
        }
        setIndex(index)
        setOpen(true)
    }
    const handleDeleteItem = (e, index) => {
        questBefore(`Bạn muốn xóa Hợp đồng lao động của ${e.ten_nv} ?`, () => {
            ApiHrQtHopDongLD.delete(e.id, res => {
                if (res.status >= 200 && 204 >= res.status) {
                    const newItems = ctx.filter((t, idx) => idx !== index);
                    setCtx(newItems)
                    ZenMessageToast.success();
                } else {
                    setError(zzControlHelper.getResponseError(res))
                    ZenMessageAlert.error(zzControlHelper.getResponseError(res))
                }
            })
        })
    }

    var findIndex = (id,state) => {
        var result = -1;
        state.forEach((task, index) => {
            if(task.id === id){
                result = index;
            }
        });
        return result;
    }

    const handleAfterUpload = (newItem, id = null) => {
        if (id) {
            if (newItem.file_upload) {
                fetchRequestHdld().then(data => {
                    setCtx(data.filter(dt => dt.id_nv === idnv))
                }).catch(err => {
                    setError(ZenHelper.getResponseError(err))
                })
                setOpen(false)
            } else {
                var indx = findIndex(newItem.id,ctx)
                ctx[indx] = newItem
                setCtx(ctx)
                setOpen(false)
            }
        } else {
            setCtx(ctx.concat(newItem))
            setOpen(false)
        }
    }

    const handleAfterUploadFiles = (newItem, id, indx,) => {
        const item = ctx[indx]
        if (newItem) {
            if (item.files) {
                item.files = item.files.concat([newItem])
                ctx[indx] = item
                setCtx([].concat(ctx))
            } else {
                item.files = [].concat(newItem)
                ctx[indx] = item
                setCtx([].concat(ctx))
            }
        } else {
            if (!newItem && id) {
                item.files = item.files.filter(t => t.id !== id)
                ctx[indx] = item
                setCtx([].concat(ctx))
            }
        }
    }

    return <>
        <div style={{ float: "right", padding: '0px 0px 7px 0px' }}>
            <ZenButton
                icon="add"
                primary
                content="Thêm mới hợp đồng"
                onClick={() => { openModal() }}
            />
        </div>
        <Table celled selectable striped>
            <Table.Header>
                <Table.Row>
                    <Table.HeaderCell width="1" textAlign="center">
                        Số HĐ
                    </Table.HeaderCell>
                    <Table.HeaderCell textAlign="center">
                        Loại hợp đồng
                    </Table.HeaderCell>
                    <Table.HeaderCell textAlign="center">
                        Ngày ký
                    </Table.HeaderCell>
                    <Table.HeaderCell textAlign="center">
                        Thời hạn
                    </Table.HeaderCell>
                    <Table.HeaderCell textAlign="center">
                        Ngày hết hạn
                    </Table.HeaderCell>
                    <Table.HeaderCell textAlign="center">
                        File kèm theo
                    </Table.HeaderCell>
                    <Table.HeaderCell collapsing></Table.HeaderCell>
                </Table.Row>
            </Table.Header>
            {ctx && (
                <Table.Body>
                    {ctx.map((item, index) => {
                        return (
                            <Table.Row key={item.id}>
                                <Table.Cell textAlign="center">{item.so_hd}</Table.Cell>
                                <Table.Cell>{item.ten_loai_hdld}</Table.Cell>
                                <Table.Cell>
                                    {ZenHelper.formatDateTime(item.ngay_ky, 'DD-MM-YYYY')}
                                </Table.Cell>
                                <Table.Cell textAlign="center">
                                    {item.thoi_han}
                                </Table.Cell>
                                <Table.Cell textAlign="center">
                                    {ZenHelper.formatDateTime(item.ngay_kt, 'DD-MM-YYYY')}
                                </Table.Cell>
                                <Table.Cell style={{ maxWidth: "150px" }} width="3">
                                    <ListFileUpload
                                        codeName={'HR'}
                                        docKey={item.id}
                                        defaultTypefile={'HDLD'}
                                        files={item.files}
                                        indx={index}
                                        handleAfterUpload={handleAfterUploadFiles}
                                    />
                                </Table.Cell>
                                <Table.Cell collapsing>

                                    <Button.Group size="small" style={{ float: "right" }}>
                                        <ButtonEdit
                                            onClick={() => openModal(item, index)}
                                        />
                                        <ButtonDelete
                                            onClick={() => handleDeleteItem(item, index)}
                                        />
                                    </Button.Group>
                                </Table.Cell>
                            </Table.Row>
                        );
                    })}
                </Table.Body>
            )}
        </Table>

        {open && <ModalHdld open={open}
            id={id}
            idnv={idnv}
            onClose={() => setOpen(false)}
            onAfterSave={handleAfterUpload}
        />}
    </>
};

const ModalHdld = ({ open, id, idnv, onClose, onAfterSave, title }) => {
    const _isMounted = useRef(true);
    const refFormik = useRef();
    const refUpload = useRef();
    const [attachmentInfo, setAttachmentInfo] = useState();
    const [fileInfo, setFileInfo] = useState();
    const [itemInfo, setItemInfo] = useState();
    const [error, setError] = useState();
    const [roles, setRoles] = useState([]);
    const [btnLoading, setBtnLoading] = useState();

    useEffect(() => {
        zzControlHelper.dragElement(document.getElementById('Hdld-form'), 'header-Hdld-form')
    })

    useEffect(() => {
        if (id) {
            ApiHrQtHopDongLD.getByCode(id, res => {
                if (res.status >= 200 && 204 >= res.status) {
                    refFormik.current.setValues({
                        ...refFormik.current.values,
                        ...res.data.data,
                    })
                } else {
                    setError(zzControlHelper.getResponseError(res))
                }
            })
        } else {
            refFormik.current.setValues({
                ...refFormik.current.values,
                id_nv: idnv,
            })
        }
    }, [])

    const handleSelectedFile = async (e) => {
        if (e.target.value) {
            const fileInfo = await selectedFile(e);
            if (fileInfo) {
                setFileInfo(fileInfo)
            }
        } else {
            setFileInfo()
        }
    }

    const handleSubmit = (params, formId) => {
        //setBtnLoading(true)
        if (id) {
            ApiHrQtHopDongLD.update(params, res => {
                if (res.status >= 200 && 204 >= res.status) {
                    onAfterSave && onAfterSave({ ...params, file_upload: fileInfo }, id)
                    ZenMessageToast.success();
                } else {
                    setError(zzControlHelper.getResponseError(res))
                    ZenMessageAlert.error(zzControlHelper.getResponseError(res))
                }
                if (btnLoading) setBtnLoading()
            })
        } else {
            ApiHrQtHopDongLD.insert(params, res => {
                if (res.status >= 200 && 204 >= res.status) {
                    onAfterSave && onAfterSave({ ...res.data.data })
                    ZenMessageToast.success();
                } else {
                    setError(zzControlHelper.getResponseError(res))
                    ZenMessageAlert.error(zzControlHelper.getResponseError(res))
                }
                if (btnLoading) setBtnLoading()
            })
        }
        if (btnLoading) setBtnLoading()
    }


    // const handleSubmit = (params, formId) => {
    //     //setBtnLoading(true)
    //     if (id) {
    //         const formData = new FormData();
    //         formData.append("id", id)
    //         formData.append("id_nv", params.id_nv)
    //         formData.append("ma_loai_hdld", params.ma_loai_hdld)
    //         formData.append("so_hd", params.so_hd)
    //         formData.append("thoi_han", params.thoi_han)
    //         formData.append("ngay_bd", params.ngay_bd)
    //         formData.append("ngay_kt", params.ngay_kt)
    //         formData.append("ngay_ky", params.ngay_ky)
    //         formData.append("nguoi_ky", params.nguoi_ky)
    //         formData.append("nguoi_ky_cvu", params.nguoi_ky_cvu)
    //         formData.append("ghi_chu", params.ghi_chu)
    //         if (refUpload.current.files[0]) {
    //             formData.append('file_upload', refUpload.current.files[0]);
    //         }
    //         ApiHrQtHopDongLD.update(formData, res => {
    //             if (res.status >= 200 && 204 >= res.status) {
    //                 onAfterSave && onAfterSave({ ...params, file_upload: fileInfo }, id)
    //                 ZenMessageToast.success();
    //             } else {
    //                 setError(zzControlHelper.getResponseError(res))
    //                 ZenMessageAlert.error(zzControlHelper.getResponseError(res))
    //             }
    //             if (btnLoading) setBtnLoading()
    //         })
    //     } else {
    //         const formData = new FormData();
    //         formData.append("id_nv", idnv)
    //         formData.append("ma_loai_hdld", params.ma_loai_hdld)
    //         formData.append("so_hd", params.so_hd)
    //         formData.append("thoi_han", params.thoi_han)
    //         formData.append("ngay_bd", params.ngay_bd)
    //         formData.append("ngay_kt", params.ngay_kt)
    //         formData.append("ngay_ky", params.ngay_ky)
    //         formData.append("nguoi_ky", params.nguoi_ky)
    //         formData.append("nguoi_ky_cvu", params.nguoi_ky_cvu)
    //         formData.append("ghi_chu", params.ghi_chu)
    //         if (refUpload.current.files[0]) {
    //             formData.append('file_upload', refUpload.current.files[0]);
    //         }
    //         ApiHrQtHopDongLD.insert(formData, res => {
    //             if (res.status >= 200 && 204 >= res.status) {
    //                 onAfterSave && onAfterSave({ ...res.data.data })
    //                 ZenMessageToast.success();
    //             } else {
    //                 setError(zzControlHelper.getResponseError(res))
    //                 ZenMessageAlert.error(zzControlHelper.getResponseError(res))
    //             }
    //             if (btnLoading) setBtnLoading()
    //         })
    //     }
    //     if (btnLoading) setBtnLoading()
    // }

    function selectedFile(e) {
        return new Promise(resolve => {
            if (e.target.files.length > 0) {
                const reader = new FileReader(),
                    file = e.target.files[0];
                reader.onloadend = () => {
                    resolve(file);
                };
                reader.readAsDataURL(file);
            }
        })
    }
    const handleChangeSelecthdld = (e, i) => {
        if (i && i.value) {
            refFormik.current.setValues({
                ...refFormik.current.values,
                ma_loai_hdld: i.value,
                ten_loai_hdld: i.options.filter(t => t.value === i.value)[0].ten
            })
        }
    }

    const handleChangeTH = (e, i) => {
        const date = new Date(refFormik.current.values.ngay_bd);
        date.setMonth(date.getMonth() + +e.target.value);
        refFormik.current.setValues({
            ...refFormik.current.values,
            thoi_han: e.target.value,
            ngay_kt: zzControlHelper.formatDateTime(date, 'YYYY-MM-DD'),
        })
    }

    const handleChangeDateBD = (e, i) => {
        const date = new Date(e.value);
        date.setMonth(date.getMonth() + +refFormik.current.values.thoi_han);
        if (e.value) {
            refFormik.current.setValues({
                ...refFormik.current.values,
                ngay_bd: e.value,
                ngay_kt: zzControlHelper.formatDateTime(date, 'YYYY-MM-DD'),
            })
        }
    }

    const onValidate = (item) => {
        const errors = {};
        const { ngay_bd, ngay_ky, ngay_kt } = item
        const ngay1 = parseInt(ngay_bd) > 1900 ? ngay_bd : null
        const ngay2 = parseInt(ngay_ky) > 1900 ? ngay_ky : null
        const ngay3 = parseInt(ngay_kt) > 1900 ? ngay_kt : null
        // if (!ngay3 || !ngay2 || !ngay1) {
        //     return
        // }
        if (ngay1 < ngay2) {
            errors.ngay_bd = 'Ngày hiệu lực phải lớn hơn hoặc bằng ngày ký';
        }
        if (ngay3 <= ngay2) {
            errors.ngay_kt = 'Ngày kết thúc phải lớn hơn hiệu lực';
        }
        return errors
    }

    const customHeader = (id) => {
        const header = 'hợp đồng lao động'
        const txtHeader = !id ? `Thêm mới ${header}` : capitalize(header) + ` : ${id}`
        return txtHeader
    }
    const capitalize = (s) => {
        if (typeof s !== 'string') return ''
        return s.charAt(0).toUpperCase() + s.slice(1)
    }

    return <>
        <Modal id="Hdld-form"
            closeOnEscape closeIcon closeOnDimmerClick={false}
            onClose={() => onClose()}
            //onOpen={() => onClose()}
            open={open}
            size={"tiny"}
        >
            <Modal.Header id='header-Hdld-form' style={{ cursor: "grabbing" }}>
                {customHeader(id)}
            </Modal.Header>

            <Modal.Content>
                <ZenFormik form={"hrdmother"} ref={refFormik}
                    validation={formValidation}
                    initItem={initItem}
                    onSubmit={handleSubmit}
                    onValidate={onValidate}
                >
                    {
                        formik => {
                            return <Form>
                                <Form.Group>
                                    <ZenFieldSelectApi width={8} name="ma_loai_hdld"
                                        required
                                        lookup={{ ...ZenLookup.MA_LOAI_HDLD, format: `{ten}` }}
                                        formik={formik}
                                        onChange={handleChangeSelecthdld}
                                        label="hdld.ma_loaihdld" defaultlabel="Loại hợp đồng"
                                    />
                                    <ZenField width={8} name="so_hd" props={formik}
                                        required
                                        label="hdld.so_hd" defaultlabel="Số hợp đồng"
                                        isCode
                                    />
                                </Form.Group>
                                <Form.Group>
                                    <ZenFieldDate width={8} name="ngay_ky" props={formik}
                                        required
                                        label="hdld.ngay_ky" defaultlabel="Ngày ký hợp đồng"
                                    />
                                    <ZenFieldDate width={8} name="ngay_bd" props={formik}
                                        required
                                        label="hdld.ngay_bd" defaultlabel="Ngày hiệu lực"
                                        onChange={handleChangeDateBD}
                                    />
                                </Form.Group>
                                <Form.Group>
                                    <ZenFieldNumber width={8} name="thoi_han"
                                        label={"role.thoi_han"} defaultlabel="Thời hạn (tháng)"
                                        onChange={handleChangeTH}
                                        props={formik}
                                    />
                                    <ZenFieldDate width={8} name="ngay_kt" props={formik}
                                        required
                                        label="hdld.ngay_kt" defaultlabel="Ngày kết thúc/hết hiệu lực"
                                    />
                                </Form.Group>
                                <Form.Group>
                                    <ZenField width={8} name="nguoi_ky" props={formik}
                                        label="hdld.nguoi_ky" defaultlabel="người ký"
                                    />
                                    <ZenField width={8} name="nguoi_ky_cvu" props={formik}
                                        label="hdld.nguoi_ky_cvu" defaultlabel="Chức vụ của người ký"
                                    />
                                </Form.Group>
                                <Form.Group>
                                    <ZenFieldTextArea width={16} name="ghi_chu" props={formik}
                                        label="hdld.ghi_chu" defaultlabel="Ghi chú"
                                    />
                                </Form.Group>
                            </Form>
                        }
                    }
                </ZenFormik>
                {/* <div style={{ paddingTop: "7px" }}>
                    <Form>
                        <Form.Field>
                            <label>Tập tin kèm theo</label>
                            <div className="ui input">
                                {fileInfo && <input
                                    // <Icon name="file alternate outline" />
                                    value={fileInfo.name}
                                />}
                                <a style={{ cursor: "pointer", paddingTop: "10px" }}
                                    onClick={() => refUpload.current.click()}>
                                    <Icon name="attach" />
                                    <FormattedMessage id="fileAttach" defaultMessage="File kèm theo" />
                                </a>
                            </div>
                        </Form.Field>
                    </Form>
                </div>

                <input hidden type="file"
                    ref={refUpload}
                    onChange={handleSelectedFile} /> */}
            </Modal.Content>
            <Modal.Actions>
                <ZenButton btnType={"cancel"} size="small" onClick={() => onClose()} />
                <ZenButton btnType="save" size="small"
                    loading={btnLoading} type="submit"
                    onClick={(e) => refFormik.current.handleSubmit(e)}
                />
            </Modal.Actions>
        </Modal>
    </>
}

const initItem = {
    id_nv: "",
    ma_loai_hdld: "",
    so_hd: "",
    thoi_han: "",
    ngay_bd: "",
    ngay_kt: "",
    ngay_ky: "",
    nguoi_ky: "",
    nguoi_ky_cvu: "",
    file_upload: "",
    ghi_chu: "",
}

const formValidation = [
    {
        id: "ma_loai_hdld",
        validationType: "string",
        validations: [
            {
                type: "required",
                params: ["Không được bỏ trống trường này"]
            },
        ]
    },
    {
        id: "so_hd",
        validationType: "string",
        validations: [
            {
                type: "required",
                params: ["Không được bỏ trống trường này"]
            },
        ]
    },
    {
        id: "ngay_ky",
        validationType: "date",
        validations: [
            {
                type: "required",
                params: ["Không được bỏ trống trường này"]
            },
        ]
    },
    {
        id: "ngay_bd",
        validationType: "date",
        validations: [
            {
                type: "required",
                params: ["Không được bỏ trống trường này"]
            },
        ]
    },
    {
        id: "ngay_kt",
        validationType: "date",
        validations: [
            {
                type: "required",
                params: ["Không được bỏ trống trường này"]
            },
        ]
    },
]

export default DetailhdldTabList