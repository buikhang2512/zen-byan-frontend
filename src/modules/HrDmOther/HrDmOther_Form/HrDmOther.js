import React, { createRef, memo, useEffect, useMemo, useState } from "react";
import {
  Dimmer,
  Grid,
  Icon,
  Loader,
  Menu,
  Message,
  Input,
} from "semantic-ui-react";
import * as routes from "../../../constants/routes";
import { ContainerScroll, ScrollManager, ZenButton } from "../../../components/Control";
import HrDmOtherContent from "./HrDmOther_content";
import { Apihrdmkhac } from "../Api/Apihrdmkhac";
import { ZenHelper } from '../../../utils';
import { Apihrdmkhacds } from "../Api";
import * as permissions from "../../../constants/permissions";

const HrOtherMenu = () => {
  const [menu, setMenu] = useState([]);
  const [activeModule, setactiveModule] = useState();
  const [isLoading, setIsloading] = useState(false);
  const [error, setError] = useState();
  const refHrDmOtherContent = createRef();
  function fetchRequestMenu() {
    var result = new Promise((resolve, reject) =>
      Apihrdmkhacds.get(res => {
        if (res.status >= 200 && 204 >= res.status) {
          resolve(res.data.data)
        } else {
          reject(res)
        }
      })
    )
    return result
  }

  useEffect(() => {
    fetchRequestMenu().then(data => {
      setMenu(data)
      setactiveModule(data[0].code_name)
    }).catch(err => {
      setError(ZenHelper.getResponseError(err))
    })
  }, [])

  const handleChangeTab = (e,{name}) => {
    setactiveModule(name);
  };

  const showMenu = (menu) => {
    return (
      <Menu fluid pointing vertical>
        {/* <Menu.Item
          key="ALL"
          content={
            <>
              {" "}
              {(activeModule === "ALL" ? true : false) ? (
                <span>
                  <Icon name={"caret right"} />
                </span>
              ) : undefined}
              Tất cả báo cáo
            </>
          }
          name="ALL"
          onClick={handleChangeTab}
          active={activeModule === "ALL" ? true : false}
          header={activeModule === "ALL" ? true : false}
        /> */}
        {menu.map((module) => {
          const isActive = activeModule === module.code_name;
          return (
            <Menu.Item
              key={module.stt}
              content={
                <>
                  {isActive ? (
                    <span>
                      <Icon name={"caret right"} />
                    </span>
                  ) : undefined}{" "}
                  {module.name}{" "}
                </>
              }
              name={module.code_name}
              onClick={handleChangeTab}
              active={isActive}
              header={isActive}
            />
          );
        })}
      </Menu>
    );
  }
  return (
    <>
      {/* <div className="zrc-container-flex">
          <ZenButton
            btnType="refresh"
            size="small"
            onClick = {() =>  refHrDmOtherContent.current.refreshApi()}
          />
          <ZenButton
            btnType="add"
            size="small"
            onClick = {() => refHrDmOtherContent.current.openModal()}
          />
      </div> */}
      <Grid id='hrdmorther'>
        <Grid.Column width={4}>
          {isLoading && (
            <Dimmer inverted active={isLoading}>
              <Loader inverted content="Loading" />
            </Dimmer>
          )}
          {error && <Message list={error} negative />}
          {showMenu(menu)}
        </Grid.Column>

        <Grid.Column width={12}>
          <ContainerScroll isSegment={false}
            idElementContainer='hrdmorther'
            id={"right-content"}
            border
          >
            <HrDmOtherContent
              activeModule={activeModule}
              modules={menu}
              keyword={""}
              ref={refHrDmOtherContent}
            />
          </ContainerScroll>
        </Grid.Column>
      </Grid>
    </>
  )
}

export const HrDmOther = {
  HrModule: {
    route: routes.HrDmOther,
    Zenform: HrOtherMenu,
    linkHeader: {
      id: "hrdmorther",
      defaultMessage: "Danh mục tham số nhân sự",
      active: true,
      isSetting: true,
    },
    action: {
      view: { visible: true, permission: permissions.HRDmKhacXem },
    },
  },
}