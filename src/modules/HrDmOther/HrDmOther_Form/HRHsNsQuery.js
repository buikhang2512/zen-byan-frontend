import * as routes from '../../../constants/routes'

import React, { useState, useEffect, useRef } from "react";
import { ContainerScroll, HeaderLink, SegmentHeader,Helmet, ButtonDelete, FormatDate, ButtonEdit, ZenButton, ZenField, ZenFieldDate, ZenFieldSelect, ZenFieldSelectApi, ZenFieldTextArea, ZenFormik, ZenMessageAlert, ZenMessageToast, ZenModal, ZenLoading, ZenFieldSelectApiMulti, ZenLink } from "../../../components/Control";
import { FormatNumber, ZenDatePeriod } from "../../../components/Control/index";
import { FormMode, ZenHelper } from "../../../utils";
import { Form, Segment, Checkbox, Button, Dimmer, Message, Loader, Container, Table, Divider, Input, Icon, Breadcrumb, Grid, Accordion } from "semantic-ui-react";
import _ from 'lodash';
import { ApiGLCalc as api, ApiHrHsNs } from '../Api';
import { useIntl } from 'react-intl';
import { GlobalStorage, KeyStorage } from "../../../utils/storage";
import { ZenLookup } from '../../ComponentInfo/Dictionary/ZenLookup';
import { zzControlHelper } from '../../../components/Control/zzControlHelper';
import * as permissions from "../../../constants/permissions";

const initItem = {
    ma_quoc_tich: "",
    loai_hsns: "",
    gioi_tinh: "",
    ma_tdcm: "",
    ma_cap: "",
    ma_tinh_kv: "",
    ma_huyen_kv: "",
    ma_dac_diem_gv: "",
    week_day: "",
    thoi_gian: "",
}

const formValidation = []

const optionweek = [
    { key: "1", value: "2", text: "Thứ 2" },
    { key: "2", value: "3", text: "Thứ 3" },
    { key: "3", value: "4", text: "Thứ 4" },
    { key: "4", value: "5", text: "Thứ 5" },
    { key: "5", value: "6", text: "Thứ 6" },
    { key: "6", value: "7", text: "Thứ 7" },
    { key: "7", value: "8", text: "Chủ nhật" },
]

const optionthoigian = [
    { key: "s1", value: "S", text: "Sáng" },
    { key: "c2", value: "C", text: "Chiều" },
    { key: "t3", value: "T", text: "Tối" },
]

const HrQueryHsNs_form = (props) => {
    const refFormik = useRef()
    const [errorInfo, setErrorInfo] = useState({ messageList: [], isError: false });
    const [loadingForm, setLoadingForm] = useState(false);
    const [dataInfo, setDataInfo] = useState([]);
    const [roles, setRoles] = useState([]);
    const [rolesCap, setRolesCap] = useState([]);
    const [TDCM, setTDCM] = useState([]);
    const [tinh, setTinh] = useState([]);
    const [huyen, setHuyen] = useState([]);
    const [maopt1, setMaopt1] = useState([]);
    const [btnload, setBtnLoad] = useState(false);
    const [query, setQuery] = useState(false);
    const [expand, setExpand] = useState(0);

    useEffect(() => {
        const daciem = GlobalStorage.getByField(KeyStorage.CacheData, "ma_dac_diem_gv")?.data
        const cap = GlobalStorage.getByField(KeyStorage.CacheData, "ma_tddt")?.data
        const tdcm = GlobalStorage.getByField(KeyStorage.CacheData, "ma_tdcm")?.data
        setRoles(ZenHelper.translateListToSelectOptions(daciem, false, 'ma', 'ma', 'ma', [], 'ten'))
        setRolesCap(ZenHelper.translateListToSelectOptions(cap, false, 'ma', 'ma', 'ma', [], 'ten'))
        setTDCM(ZenHelper.translateListToSelectOptions(tdcm, false, 'ma', 'ma', 'ma', [], 'ten'))

        const tinh = GlobalStorage.getByField(KeyStorage.CacheData, "ma_tinh")?.data
        const huyen = GlobalStorage.getByField(KeyStorage.CacheData, "ma_huyen")?.data
        setTinh(ZenHelper.translateListToSelectOptions(tinh, false, 'ma_tinh', 'ma_tinh', 'ma_tinh', [], 'ten_tinh'))
        setHuyen(ZenHelper.translateListToSelectOptions(huyen, false, 'ma_huyen', 'ma_huyen', 'ma_huyen', [], 'ten_huyen'))

        const hrdmkhac = GlobalStorage.getByField(KeyStorage.CacheData, "hrdmkhac")?.data
        const maopt1 = hrdmkhac?.filter(x => x.code_name === 'MA_OPT1') || []
        setMaopt1(ZenHelper.translateListToSelectOptions(maopt1, false, 'ma', 'ma', 'ma', [], 'ten'))
        getData(initItem)
    }, [])
    useEffect(() => {
        let param = _.clone(refFormik.current.values)
        if (_.join(param.ma_dac_diem_gv, ',') !== ', ') {
            let next_stt = _.join(param.ma_dac_diem_gv, ',')
            param.ma_dac_diem_gv = next_stt
        }
        if (_.join(param.ma_cap, ',') !== ', ') {
            let next_stt = _.join(param.ma_cap, ',')
            param.ma_cap = next_stt
        }
        if (_.join(param.ma_tdcm, ',') !== ', ') {
            let next_stt = _.join(param.ma_tdcm, ',')
            param.ma_tdcm = next_stt
        }
        if (_.join(param.ma_tinh_kv, ',') !== ', ') {
            let next_stt = _.join(param.ma_tinh_kv, ',')
            param.ma_tinh_kv = next_stt
        }
        if (_.join(param.ma_huyen_kv, ',') !== ', ') {
            let next_stt = _.join(param.ma_huyen_kv, ',')
            param.ma_huyen_kv = next_stt
        }

        if (_.join(param.ma_opt1, ',') !== ', ') {
            let next_stt = _.join(param.ma_opt1, ',')
            param.ma_opt1 = next_stt
        }

        if (_.join(param.week_day, ',') !== ', ') {
            let next_stt = _.join(param.week_day, ',')
            param.week_day = next_stt
        }
        if (query) {
            getData(param)
        }
        setQuery(false)

    }, [query])

    const onRefreshForm = () => {
        refFormik.current.setValues({
            ...refFormik.current.values,
            ...initItem,
        })
        getData(initItem)
    }

    function getData(params) {
        setBtnLoad(true)
        ApiHrHsNs.Query(params, res => {
            if (res.status >= 200 && 204 >= res.status) {
                //handleAfterSave({ ...params })
                setDataInfo(res.data.data)
            } else {
                setErrorInfo(zzControlHelper.getResponseError(res))
            }
            setBtnLoad(false)
        })

    }
    const onChange = () => {
        setQuery(true)
        return {}
    }

    const handleExpandFilter = (e, { index }) => setExpand(expand === index ? -1 : index);

    const PageHeader = () => {
        return <>
            <Helmet idMessage={"hrhsnsquery.detail"}
                defaultMessage={"Lọc hồ sơ giáo viên"} />

            <SegmentHeader>
                <HeaderLink >
                    <Breadcrumb.Divider icon='right chevron' />
                    <Breadcrumb.Section active>
                        {'Lọc hồ sơ giáo viên'}
                    </Breadcrumb.Section>
                </HeaderLink>
            </SegmentHeader>
            <br />
        </>
    }

    const linkto = (a) => {
        let paramsUrl = zzControlHelper.btoaUTF8(a.params.id)
        const route = a.route.replace(":id", paramsUrl)
        return {
            pathname: route,
            //search: `?`+ new URLSearchParams({tk: filter.tk,}).toString(),
            state: { route: a.params.route },
        }
    }

    return <React.Fragment>
        <PageHeader />
        <Accordion fluid styled>
            <Accordion.Title
                active={expand === 0}
                index={0}
                onClick={handleExpandFilter}
            >
                <Icon name="dropdown" />
                Điều kiện lọc
            </Accordion.Title>
            <Accordion.Content active={expand === 0}>
                <ZenFormik form={"hrdmttc"} ref={refFormik}
                    validation={formValidation}
                    initItem={initItem}
                    onValidate={onChange}
                >
                    {
                        formik => {
                            return <Form>
                                <Form.Group>
                                    <ZenFieldSelectApi width={3} name="ma_quoc_tich"

                                        lookup={{ ...ZenLookup.SIDmQuocGia, format: `{ten_quoc_gia}` }}
                                        formik={formik}
                                        label="hdld.ma_quoc_tich" defaultlabel="Quốc tịch"
                                    />
                                    <ZenFieldSelectApi width={3} name="gioi_tinh"
                                        lookup={{
                                            ...ZenLookup.SIDmloai, format: "{ten}",
                                            onLocalWhere: (items) => {
                                                return items.filter(t => t.ma_nhom === 'GIOI_TINH') || []
                                            },
                                            where: "MA_NHOM = 'GIOI_TINH'"
                                        }}
                                        label={"hrhsnsquery.gioi_tinh"} defaultlabel="Giới tính"
                                        formik={formik}
                                    />
                                    <ZenFieldSelectApi width={3} name="loai_hsns"
                                        lookup={{
                                            ...ZenLookup.SIDmloai, format: "{ten}",
                                            onLocalWhere: (items) => {
                                                return items.filter(t => t.ma_nhom === 'LOAI_HSNS') || []
                                            },
                                            where: "MA_NHOM = 'LOAI_HSNS'"
                                        }}
                                        formik={formik}
                                        //onChange={handleChangeSelecthdld}
                                        label="hdld.ma_loaihdld" defaultlabel="Loại hồ sơ"
                                    />
                                    <ZenFieldSelect width={3} multiple options={TDCM} name={'ma_tdcm'}
                                        props={formik}
                                        label="hrhsnsquery.ma_tdcm" defaultlabel="Trình độ chuyên môn"

                                    />
                                    {/* <ZenFieldSelectApi width={3} name={'ma_cap'}
                                    lookup={ZenLookup.MA_TDDT}
                                    formik={formik}
                                    label="hrhsnsquery.ma_cap" defaultlabel="Cấp dạy"
                                />
                                <ZenFieldSelectApi width={3} name="ma_huyen_kv"
                                    lookup={ZenLookup.SIDmTinh}
                                    formik={formik}
                                    label="hrhsnsquerygv.ma_huyen_kv" defaultlabel="Khu vực (Tỉnh/tp)"
                                />
                                <ZenFieldSelectApi width={3} name="ma_huyen_kv"
                                    lookup={ZenLookup.SIDmHuyen}
                                    formik={formik}
                                    label="hrhsnsquerygv.ma_huyen_kv" defaultlabel="Khu vực (Quận/huyện)"
                                /> */}
                                    <ZenFieldSelect width={3} multiple options={rolesCap} name={'ma_cap'}
                                        props={formik}
                                        label="hdld.ma_cap" defaultlabel="Cấp dạy"
                                    />
                                    <ZenFieldSelect width={3} multiple options={tinh} name="ma_tinh_kv"
                                        props={formik}
                                        label="hrhsnsgv.ma_tinh_kv" defaultlabel="Tỉnh/thành phố"
                                    />
                                    <ZenFieldSelect width={3} multiple options={huyen} name="ma_huyen_kv"
                                        props={formik}
                                        label="hrhsnsgv.ma_huyen_kv" defaultlabel="Quận/huyện"
                                    />
                                </Form.Group>
                                <Form.Group>
                                    <ZenFieldSelect width={3} multiple options={optionweek} name="week_day"
                                        label={"hrhsnsquery.week_day"} defaultlabel="Ngày trống trong tuần"
                                        props={formik}
                                    />
                                    {/* <ZenFieldSelect width={3} options={optionthoigian} name="thoi_gian"
                                        label={"hrhsnsquery.thoi_gian"} defaultlabel="Khung thời gian"
                                        props={formik}
                                    /> */}
                                    <ZenFieldSelect width={3} multiple options={roles} name="ma_dac_diem_gv"
                                        label={"hrhsnsgv.dac_diem"} defaultlabel="Đặc điểm giáo viên"
                                        props={formik}
                                    />
                                    <ZenFieldSelectApiMulti width={3} name="ma_loai_tai_lieu"
                                        required
                                        lookup={{ ...ZenLookup.MA_LOAI_TAI_LIEU, format: `{ten}` }}
                                        formik={formik}
                                        name="loai_giay"
                                        label="hdld.ma_loai_tai_lieu" defaultlabel="Loại giấy tờ"
                                    />
                                    {/* <ZenFieldSelectApi width={3} name="ma_dac_diem_gv"
                                    lookup={ZenLookup.MA_DAC_DIEM_GV}
                                    label={"hrhsnsquerygv.dac_diem"} defaultlabel="Đặc điểm giáo viên"
                                    props={formik}
                                /> */}
                                    <ZenFieldSelect width={3} multiple options={maopt1} name="ma_opt1"
                                        label={"hrhsnsgv.Available"} defaultlabel="Available"
                                        props={formik}
                                    />
                                </Form.Group>
                            </Form>
                        }
                    }
                </ZenFormik>
                <Button primary
                    content="Reset"
                    size="small" icon="refresh"
                    onClick={() => onRefreshForm()}
                />
            </Accordion.Content>
        </Accordion>
        <Segment>
            <Container id="hrhsnsquery-container" fluid>
                <ContainerScroll isSegment={false}
                    idElementContainer={'hrhsnsquery-container'}
                    id={"table-content"}
                >
                    <Container>
                        {btnload && <ZenLoading loading={btnload} inline="centered" />}
                        {errorInfo.isError && <Message negative list={errorInfo.messageList}
                            onDismiss={() => setErrorInfo({ isError: false, messageList: [] })} />}
                    </Container>
                    <Table>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell content={`ID giáo viên`} />
                                <Table.HeaderCell content={`Họ và tên`} />
                                <Table.HeaderCell content={`Giới tính `} />
                                <Table.HeaderCell content={`Ngày sinh `} />
                                <Table.HeaderCell content={`Điện thoại `} />
                                <Table.HeaderCell content={`Email`} />
                            </Table.Row>
                        </Table.Header>
                        <Table.Body>
                            {dataInfo && dataInfo.length > 0 && dataInfo.map((x, idx) => {

                                return <Table.Row>
                                    <Table.Cell>
                                        <ZenLink style={{ fontWeight: "bold" }}
                                            to={linkto({ route: routes.HrHsNs_detail(), params: { id: x.id_nv, route: routes.HrQueryHsNs } })}
                                        >
                                            {x.id_nv}
                                        </ZenLink>
                                    </Table.Cell>
                                    <Table.Cell>{x.ho_ten}</Table.Cell>
                                    <Table.Cell>{x.gioi_tinh}</Table.Cell>
                                    <Table.Cell content={<FormatDate value={x.ngay_sinh} />} />
                                    <Table.Cell>{x.dien_thoai}</Table.Cell>
                                    <Table.Cell>{x.email}</Table.Cell>
                                </Table.Row>
                            })}
                        </Table.Body>
                    </Table>
                </ContainerScroll>
            </Container>
        </Segment>
    </React.Fragment>
}

export const HrHsNsQuery = {
    HrHsNsQuery: {
        route: routes.HrQueryHsNs,
        Zenform: HrQueryHsNs_form,
        action: {
            view: { visible: true, permission: permissions.HRQueryHsNs, },
        },
    },
}