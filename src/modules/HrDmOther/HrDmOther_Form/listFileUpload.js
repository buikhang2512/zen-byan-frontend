import React, { useState, useEffect, useRef } from "react"
import { FormattedMessage, useIntl } from "react-intl";
import { Icon, Message, Form } from "semantic-ui-react";
import { ApiFileAttachment } from "../../../Api";
import { ZenMessageAlert, ZenMessageToast, ZenModal, ZenFormik, ZenButton, ZenFieldSelectApi, ZenFieldTextArea, ZenFileViewer } from "../../../components/Control";
import { zzControlHelper } from "../../../components/Control/zzControlHelper";

export const ListFileUpload = (props) => {
    const { codeName, docKey,defaultTypefile, files ,indx, handleAfterUpload } = props
    //const [listFile, setListFile] = useState([]);
    const [error, setError] = useState()
    const [open, setOpen] = useState(false)
    const [loading, setLoading] = useState(true)
    const intl = useIntl();

    const [viewFile, setViewFile] = useState(initViewFile);
    const { openView, fileInfo } = viewFile

    function viewFileName(item) {
        const temp = item.file_name ? item.file_name : ""

        return temp ? <>
            <div style={{ display: "flex" ,paddingBottom:"10px"}}>
                <div style={{ display: "inline-block", width: '88%' }}>
                    <a href="#"
                        onClick={(e) => handleViewFile(e, item)}>
                        <div style={{ display: "flex" }}>
                            <div style={{ whiteSpace: "nowrap", textOverflow: "ellipsis", overflow: "hidden", }}>
                                {temp}
                            </div>
                        </div>
                    </a>
                </div>
                <Icon style={{ cursor: "pointer", margin:"0px auto", fontSize:"1.2em" }} onClick={() => handleDeleteFile(item)} name="x" color="red" />
            </div>
        </> : undefined
    }

    const handleDeleteFile = (item) => {
        ZenMessageAlert.question(intl.formatMessage({ id: "fileAttach.q_delete", defaultMessage: "Bạn có chắc muốn xóa không?" }))
            .then(success => {
                if (success == 1) {
                    ApiFileAttachment.delete(codeName, item.id, item.gid,
                        res => {
                            if (res.status >= 200 && res.status <= 204) {
                                handleAfterUploadfile(null,item.id,indx)
                                ZenMessageToast.success();
                            } else {
                                zzControlHelper.getResponseError(res);
                            }
                        })
                }
            })
    }

    const handleDownloadFile = (e) => {
        e.preventDefault();
        ApiFileAttachment.download(codeName, fileInfo.gid, res => {
            if (res.status >= 200 && res.status <= 204) {
                const url = window.URL.createObjectURL(new Blob([res.data]));

                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download', fileInfo.file_name);
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            } else {
                ZenMessageAlert.error(zzControlHelper.getResponseError(res))
            }
        })
    }

    const handleAfterUploadfile = (newItem,id) => {
        handleAfterUpload(newItem,id,indx)
        setOpen(false)
    }

    const handleViewFile = (e, fileAtt) => {
        e.preventDefault();
        // setViewFile({ openView: true, fileInfo: {...fileAtt,file_name_sys:fileAtt.gid + "_" + fileAtt.file_name} })
        setViewFile({ openView: true, fileInfo: fileAtt })
     }
  
    const handleCloseFile = () => setViewFile(initViewFile)

    return <>
        {error && <Message negative list={error} onDismiss={() => setError()} />}
        {(files && files.length > 0 )? files.map((item, idx) => {
            return viewFileName(item)
        }) : <div style={{ display: "flex" ,paddingBottom:"10px"}}>Chưa có file kèm theo</div>}

        <a style={{ cursor: "pointer"}}
            onClick={() => setOpen(true)}>
            <Icon name="attach" />
            Thêm file
        </a>

        {open && <ModalFileAttachment open={open}
            codeName={codeName} docKey={docKey}
            onClose={() => setOpen(false)}
            onAfterSave={handleAfterUploadfile}
            defaultTypefile={defaultTypefile}
        />}

        <ZenFileViewer
            fileInfo={fileInfo}
            open={openView}
            onClose={handleCloseFile}
            onDownload={handleDownloadFile}
        />
    </>
}

const ModalFileAttachment = ({ open, codeName, docKey, onClose, onAfterSave,defaultTypefile}) => {
    const _isMounted = useRef(true);
    const refFormik = useRef();
    const refUpload = useRef();
    const [attachmentInfo, setAttachmentInfo] = useState();
    const [fileInfo, setFileInfo] = useState();
    const [error, setError] = useState();
    const [btnLoading, setBtnLoading] = useState();

    useEffect(() => {
        if (codeName) {
            ApiFileAttachment.getAttachmentInfo(codeName, res => {
                if (_isMounted.current) {
                    if (res.status === 200) {
                        setAttachmentInfo(res.data.data)
                        if (error) setError()
                    } else {
                        setError(zzControlHelper.getResponseError(res))
                    }
                }
            })
        }
        if(defaultTypefile) {
            refFormik.current.setValues({
                ...refFormik.current.value,
                ma_loai: defaultTypefile,
            })
        }
        return (() => {
            _isMounted.current = false
            setFileInfo()
        })
    }, [codeName])

    const handleSubmit = (params, formId) => {
        if (refUpload.current.files[0]) {
            setBtnLoading(true)
            const formData = new FormData();
            formData.append("doc_key", docKey)
            formData.append("ma_loai", params.ma_loai)
            formData.append("ghi_chu", params.ghi_chu)
            formData.append('file_upload', refUpload.current.files[0]);

            ApiFileAttachment.upload(codeName, formData, res => {
                if (res.status >= 200 && res.status <= 204) {
                    onAfterSave && onAfterSave({ ...res.data.data, ten_loai: params.ten_loai },docKey)
                    ZenMessageToast.success();
                } else {
                    setError(zzControlHelper.getResponseError(res))
                    ZenMessageAlert.error(zzControlHelper.getResponseError(res))
                }
                if (btnLoading) setBtnLoading()
            })
        } else {
            ZenMessageAlert.warning("Bạn vẫn chưa chọn File")
        }
    }

    const handleSelectedFile = async (e) => {
        if (e.target.value) {
            const fileInfo = await selectedFile(e);
            if (fileInfo) {
                setFileInfo(fileInfo)
            }
        } else {
            setFileInfo()
        }
    }

    function selectedFile(e) {
        return new Promise(resolve => {
            if (e.target.files.length > 0) {
                const reader = new FileReader(),
                    file = e.target.files[0];
                reader.onloadend = () => {
                    resolve(file);
                };
                reader.readAsDataURL(file);
            }
        })
    }

    return <ZenModal
        open={open} onClose={() => onClose()} size="tiny"
        error={error ?
            <Message negative list={error} onDismiss={() => setError()} />
            : undefined}

        header={<FormattedMessage id="fileAttach.add" defaultMessage="Thêm File kèm theo" />}

        content={<>
            <ZenFormik form={"file-attachment"} ref={refFormik}
                validation={formValidation}
                initItem={initItem}
                onSubmit={handleSubmit}
            >
                {
                    formik => {
                        return <Form>
                            <ZenFieldSelectApi lookup={{
                                code: "LOAI_FILE_ATTACH",
                                cols: 'ma_loai,ten_loai,ordinal',
                                value: 'ma_loai',
                                text: "ten_loai",
                                tableName: "SIDmLoaiTaiLieu",
                                where: `nhom = '${attachmentInfo?.nhom}'`
                            }}
                                name="ma_loai" required
                                formik={formik}
                                onItemSelected={(item) => { formik.setFieldValue("ten_loai", item.ten_loai) }}
                                label="fileAttach.loai" defaultlabel="Loại tài liệu"
                            />

                            <ZenFieldTextArea name="ghi_chu" props={formik}
                                label="fileAttach.ghi_chu" defaultlabel="Ghi chú"
                            />
                        </Form>
                    }
                }
            </ZenFormik>
            <div style={{ paddingTop: "7px" }}>
                <a style={{ cursor: "pointer" }}
                    onClick={() => refUpload.current.click()}>
                    <Icon name="attach" />
                    <FormattedMessage id="fileAttach" defaultMessage="File kèm theo" />
                </a>

                {fileInfo && <p>
                    <Icon name="file alternate outline" />
                    {fileInfo.name}
                </p>}
            </div>

            <input hidden type="file"
                ref={refUpload}
                onChange={handleSelectedFile} />
        </>}

        actions={<>
            <ZenButton btnType="save" size="small"
                loading={btnLoading} type="submit"
                onClick={(e) => refFormik.current.handleSubmit(e)} />
        </>}
    />
}

const styles = {
    fileName: {
        whiteSpace: "nowrap",
        textOverflow: "ellipsis",
        overflow: "hidden",
    }
}

const initItem = {
    ma_loai: "",
    ghi_chu: "",
    file_upload: "",
}

const initViewFile = { fileInfo: "", openView: false }

const formValidation = [
    {
        id: "ma_loai",
        validationType: "string",
        validations: [
            {
                type: "required",
                params: ["Không được bỏ trống trường này"]
            },
        ]
    },
]