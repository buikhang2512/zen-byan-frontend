import React, { createRef, memo, useEffect, useMemo, useRef, useState } from "react";
import {
    Dimmer,
    Grid,
    Icon,
    Loader,
    Menu,
    Message,
    Input,
    GridColumn,
    Segment,
    Image,
    Header,
    Divider,
    Form,
    Button,
    Popup,
    Table,
    Breadcrumb,
    Modal,
} from "semantic-ui-react";
import * as routes from "../../../constants/routes";
import { ContainerScroll, ScrollManager, ZenButton, ZenField, ZenFieldCheckbox, ZenFieldSelectApi, ZenFormik, ZenMessageAlert, ZenMessageToast, ZenModal, ZenFieldSelect, Helmet, SegmentHeader, HeaderLink, FormatDate, ZenLoading } from "../../../components/Control";
import HrDmOtherContent from "./HrDmOther_content";
import { ZenHelper } from '../../../utils';
import './HRHsNs_Detail.css'
import no_img from './image.png'
import { useLocation } from "react-router";
import { zzControlHelper } from "../../../components/Control/zzControlHelper";
import { ApiHrHsNs } from "../Api";
import { FormattedMessage } from "react-intl";
import { ApiRole } from "../../../Api/index";
import DetailhdldTabList from "./HrHsNs_Detail_HDLD";
import DetailTailieuTabList from "./HrHsNs_Detail_Tailieu";
import DetailttcTabList from "./HrHsNs_Detail_TTC";
import DetailGVTabList from "./HrHsNs_Detail_GV";
import UserFormAdd from "../../User/UserFormAdd";
import DetailSocialTabList from "./HrHsNs_Detail_Social";
import DetailLuongTabList from "./HrHsNs_Detail_Luong";
import * as permissions from "../../../constants/permissions";
import { auth } from "../../../utils";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";

const HrHsNs_form = (props) => {
    const [isLoading, setIsloading] = useState(false);
    const [data, setData] = useState();
    const [error, setError] = useState();
    const [imgInfo, setImgInfo] = useState();
    const [activeItem, setActiveitem] = useState('ttc');
    const [open, setOpen] = useState(false)
    const [accout, setAccout] = useState();
    const [realdonly, setReadonly] = useState(false);
    const refFormik = useRef();
    const refUploadImg = useRef();
    const [btnLoading, setBtnLoading] = useState();
    const [openModalAccount, setOpenModalAccount] = useState(false);
    function fetchRequestByCode() {
        const id = zzControlHelper.atobUTF8(props.match.params.id)
        var result = new Promise((resolve, reject) =>
            ApiHrHsNs.getByCode(id, res => {
                if (res.status >= 200 && 204 >= res.status) {
                    resolve(res.data.data)
                } else {
                    reject(res)
                }
            })
        )
        return result
    }

    function fetchRequestAnh() {
        const id = zzControlHelper.atobUTF8(props.match.params.id)
        var result = new Promise((resolve, reject) =>
            ApiHrHsNs.getAvatar(id, res => {
                if (res.status >= 200 && 204 >= res.status) {
                    resolve(res.data.data)
                } else {
                    reject(res)
                }
            })
        )
        return result
    }

    useEffect(() => {
        fetchRequestByCode().then(data => {
            setData(data)
        }).catch(err => {
            setError(ZenHelper.getResponseError(err))
        })
        fetchRequestAnh().then(data => {
            if (data.anh_dai_dien) setImgInfo({ file_base64: `data:image/jpeg;base64,${data.anh_dai_dien}` })
        }).catch(err => {
            setError(ZenHelper.getResponseError(err))
        })
    }, [])

    const handleChangeTab = (e, { name }) => {
        setActiveitem(name)
    }

    const openModal = () => {
        setOpen(true)
        setAccout({ email: data.email, full_name: data.ho_ten, id_nv: data.id_nv, issendmail: true })
    }

    function uploadAnh(data) {
        var result = new Promise((resolve, reject) =>
            ApiHrHsNs.uploadAvatar(data, res => {
                if (res.status >= 200 && 204 >= res.status) {
                    resolve(res)
                } else {
                    reject(res)
                }
            })
        )
        return result
    }

    const handleSelectedImg = async (e) => {
        if (e.target.value) {
            var fileInfo = await ZenHelper.selectedImage(e);
        }
        setBtnLoading(true)
        const formData = new FormData();
        formData.append("id_nv", data.id_nv)
        if (refUploadImg.current.files[0]) {
            formData.append("anh_dai_dien", refUploadImg.current.files[0])
            formData.append("xoa_anh", false)
        }
        uploadAnh(formData).then(res => {
            setImgInfo(fileInfo)
            ZenMessageToast.success()
        }).catch(err => {
            setImgInfo()
            setError(zzControlHelper.getResponseError(err))
            ZenMessageAlert.error(zzControlHelper.getResponseError(err))
        })
        setBtnLoading(false)
    }
    const handleDelImg = () => {
        refUploadImg.current.value = ""
        setImgInfo()
    }

    const handleAfterSaveModal = (newUser) => {
        const patchData = [
            {
                value: newUser.email,
                path: `/user_account`,
                op: "replace",
                operationType: 0,
            },
        ]

        ApiHrHsNs.updatePatch(data.id_nv, patchData, res => {
            if (res.status >= 200 && res.status <= 204) {
                setData({ user_account: newUser.email })
                setOpen(false)
            } else {
                ZenMessageAlert.error(zzControlHelper.getResponseError(res))
            }
        })
    }

    const callbackFunction = (childData) => {
        setData(childData)
    }

    const PageHeader = ({ item }) => {
        const location = useLocation()
        return <>
            <Helmet idMessage={"hrhsns.detail"}
                defaultMessage={"Chi tiết hồ sơ nhân sự"} />

            <SegmentHeader>
                <HeaderLink listHeader={[{
                    id: "hrhsns",
                    defaultMessage: "Hồ sơ nhân sự",
                    route: location?.state?.route ? location?.state?.route : routes.HrHsNs,
                    active: false,
                    isSetting: false,
                }]}>
                    <Breadcrumb.Divider icon='right chevron' />
                    <Breadcrumb.Section active>
                        {item?.ho_ten || "Chi tiết"}
                    </Breadcrumb.Section>
                </HeaderLink>
            </SegmentHeader>
            <br />
        </>
    }

    const handleSetAccount = (dt) => {
        ApiHrHsNs.setAccount(dt, res => {
            if (res.status >= 200 && res.status <= 204) {
                setData({ ...data, user_account: dt.email })
                setOpenModalAccount(false)
            } else {
                ZenMessageAlert.error(zzControlHelper.getResponseError(res))
            }
        })
    }

    return <>
        <PageHeader item={data} />
        <Grid>
            <Grid.Column width={4}>
                {isLoading && (
                    <Dimmer inverted active={isLoading}>
                        <Loader inverted content="Loading" />
                    </Dimmer>
                )}
                {error && <Message list={error} negative />}
                <Segment>
                    <div className="HR-Ctn-content">
                        <div className="HR-box-img">
                            <Image className="HR-image"
                                src={imgInfo ? imgInfo.file_base64 : no_img}
                                target='_blank'
                            />
                            <div className="HR-box-btn">
                                <Button size="small" icon primary onClick={() => refUploadImg.current.click()} >
                                    {/* <Icon name="edit" /> */}
                                    <Popup
                                        content={'Chọn ảnh đại diện'}
                                        key={'a'}
                                        trigger={<Icon name="edit" />}
                                    />
                                </Button>
                                {/* <Button size="small" color='red' icon="trash" onClick={handleDelImg} /> */}
                                <input hidden type="file" onChange={handleSelectedImg} ref={refUploadImg} />
                            </div>
                        </div>

                        {data && <>
                            <Table basic='very'>
                                <Table.Body>
                                    <Table.Row key={data.id_nv}>
                                        <Table.Cell textAlign="right" collapsing>ID :</Table.Cell>
                                        <Table.Cell style={{ fontWeight: "bold", whiteSpace: "" }}>
                                            {data.id_nv}
                                        </Table.Cell>
                                    </Table.Row>
                                    <Table.Row>
                                        <Table.Cell textAlign="right" collapsing>Họ Tên :</Table.Cell>
                                        <Table.Cell style={{ fontWeight: "bold", whiteSpace: "" }}>
                                            {data.ho_ten}
                                        </Table.Cell>
                                    </Table.Row>
                                    <Table.Row>
                                        <Table.Cell textAlign="right" collapsing>Giới tính :</Table.Cell>
                                        <Table.Cell style={{ fontWeight: "bold", whiteSpace: "textarea" }}>
                                            {data.ten_gioi_tinh}
                                        </Table.Cell>
                                    </Table.Row>
                                    <Table.Row>
                                        <Table.Cell textAlign="right" collapsing>Ngày sinh :</Table.Cell>
                                        <Table.Cell style={{ fontWeight: "bold", whiteSpace: "textarea" }}>
                                            <FormatDate value={data.ngay_sinh} />
                                        </Table.Cell>
                                    </Table.Row>
                                    <Table.Row>
                                        <Table.Cell textAlign="right" collapsing>Điện thoại : </Table.Cell>
                                        <Table.Cell style={{ fontWeight: "bold", whiteSpace: "textarea" }}>
                                            {data.dien_thoai}
                                        </Table.Cell>
                                    </Table.Row>
                                    <Table.Row>
                                        <Table.Cell textAlign="right" collapsing>Email :</Table.Cell>
                                        <Table.Cell style={{ fontWeight: "bold", whiteSpace: "textarea" }}>
                                            {data.email}
                                        </Table.Cell>
                                    </Table.Row>
                                </Table.Body>
                            </Table>
                        </>}

                    </div>
                </Segment>
                <Segment>
                    <div className="HR-Ctn-content">
                        <Header size='medium'>Tài khoản đăng nhập</Header>
                        <Divider />
                        {(data && data.user_account) ?
                            (!data.user_isinactive) ?
                                <>
                                    <div className="HR-field-info">
                                        <label>Tài khoản đăng nhập của nhân viên là : </label>
                                    </div>
                                    <div className="HR-field-info">
                                        <label className="HR-label">{data.user_account}</label>
                                    </div>
                                </>
                                :
                                <>
                                    <div className="HR-field-info">
                                        <label>Tài khoản : </label>
                                        <label className="HR-label">{data.user_account}</label>
                                    </div>
                                    <div className="HR-field-info" style={{ paddingTop: "5px" }}>
                                        <label style={{ color: "red", lineHeight: "1.2" }}>
                                            Tài khoản này đã bị cấm đưang nhập, vui lòng truy cập chức năng quản lý người sử dụng để cấp lại quyền
                                        </label>
                                    </div>
                                </>
                            :
                            <>
                                <div className="HR-field-info" style={{ textAlign: "center" }}>
                                    <label>Nhân viên này chưa được cấp tài khoản đăng nhập. </label>
                                </div>
                                <ZenButton
                                    permission={permissions.HrHsNsSua}
                                    icon="add user"
                                    primary fluid
                                    content="Tạo tài khoản cho nhân viên"
                                    onClick={() => openModal()}
                                />
                                <ZenButton style={{ marginTop: "5px" }}
                                    permission={permissions.HrHsNsSua}
                                    primary fluid
                                    content="Gán tài khoản đã tồn tại"
                                    icon="user plus"
                                    onClick={(e) => setOpenModalAccount(true)}
                                />
                            </>}
                        {open && <UserFormAdd
                            initItem={{
                                email: data?.email,
                                full_name: data?.ho_ten,
                                id_nv: data?.id_nv,
                                issendmail: true,
                            }}
                            header={"Tạo tài khoản truy cập"}
                            open={open}
                            onAfterSave={handleAfterSaveModal}
                            onClose={() => setOpen(false)}
                            createFromDetail={true}
                        />}
                        {
                            openModalAccount && <ModalSetAccount
                                data={data}
                                open={openModalAccount}
                                onClose={() => setOpenModalAccount(false)}
                                onSaveModal={(data) => handleSetAccount(data)}
                            />
                        }
                        {/* {open && <ModalHRHsNs open={open}
                            //codeName={activeModule} 
                            accout={accout}
                            onClose={() => setOpen(false)}
                        // onAfterSave={handleAfterUpload}
                        // title={activeModule ?
                        //     modules && modules.filter(module => module.code_name == activeModule)[0].name : ""
                        // }
                        />} */}
                    </div>
                </Segment>
            </Grid.Column>
            <Grid.Column width={12}>
                <Segment>
                    <div>
                        <Menu pointing secondary>
                            <Menu.Item
                                name='ttc'
                                active={activeItem === 'ttc'}
                                onClick={handleChangeTab}
                            > Thông tin Chung</Menu.Item>
                            <Menu.Item
                                name='mxh'
                                active={activeItem === 'mxh'}
                                onClick={handleChangeTab}
                            > Mạng xã hội</Menu.Item>
                            <Menu.Item
                                name='ttgv'
                                active={activeItem === 'ttgv'}
                                onClick={handleChangeTab}
                            > Thông tin giáo viên</Menu.Item>
                            {auth.checkPermission(permissions.HrHsNsGiayTo) && <Menu.Item
                                name='gttl'
                                active={activeItem === 'gttl'}
                                onClick={handleChangeTab}
                            >Giấy tờ/tài liệu</Menu.Item>}
                            {auth.checkPermission(permissions.HrHsNsHdld) && <Menu.Item
                                name='hdld'
                                active={activeItem === 'hdld'}
                                onClick={handleChangeTab}
                            >Hợp đồng lao động </Menu.Item>}
                            {auth.checkPermission(permissions.HrHsNsPhuCap) && <Menu.Item
                                name='luong'
                                active={activeItem === 'luong'}
                                onClick={handleChangeTab}
                            >Quá trình lương </Menu.Item>}
                            {/* <Menu.Menu position='right'>
                                <Menu.Item
                                    name='logout'
                                    active={activeItem === 'logout'}
                                    onClick={handleChangeTab}
                                />
                            </Menu.Menu> */}
                        </Menu>
                        {activeItem === 'ttc' ? <DetailttcTabList
                            data={data}
                            parentCallback={callbackFunction}
                        /> : undefined}
                        {activeItem === 'mxh' ? <DetailSocialTabList
                            data={data}
                            parentCallback={callbackFunction}
                        /> : undefined}
                        {activeItem === 'ttgv' ? <DetailGVTabList
                            idnv={data.id_nv}
                        /> : undefined}
                        {auth.checkPermission(permissions.HrHsNsGiayTo) && activeItem === 'gttl' ? <DetailTailieuTabList
                            idnv={data.id_nv}
                        /> : undefined}
                        {auth.checkPermission(permissions.HrHsNsHdld) && activeItem === 'hdld' ? <DetailhdldTabList
                            idnv={data.id_nv}
                        /> : undefined}
                        {auth.checkPermission(permissions.HrHsNsPhuCap) && activeItem === 'luong' ? <DetailLuongTabList
                            idnv={data.id_nv}
                        /> : undefined}
                    </div>
                </Segment>
            </Grid.Column>
        </Grid>
    </>
}

const ModalSetAccount = (props) => {
    const { open, onClose, data, onSaveModal } = props
    const [account, setAccount] = useState({ id: '', email: '' })
    const [loading, setLoading] = useState(false)


    return <>

        <ZenModal
            open={open}
            onClose={onClose}
            size="tiny"
            header="Gán tài khoản đã tồn tại"

            actions={
                <ZenButton btnType="save"
                    onClick={() => onSaveModal({ id_nv: data.id_nv, user: account.id, email: account.email })}
                    size="small"
                />
            }
            scrolling={false}
        >
            <Modal.Content>
                <Form>
                    <ZenFieldSelectApi
                        //loadApi
                        lookup={ZenLookup.User}
                        name="user" value={account.id}
                        label={"ardmkh.user"} defaultlabel="User"
                        onChange={(e, i) => {
                            setAccount({ id: i.value, email: i.options.filter(t => t.value === i.value)[0].email })
                        }}
                    />
                </Form>

                <ZenLoading loading={loading} />

            </Modal.Content>
        </ZenModal>
    </>
}

export const HrHsNsDetail = {
    HrHsNs: {
        route: routes.HrHsNs_detail(),
        Zenform: HrHsNs_form,
        action: {
            view: { visible: true, permission: "", notPermission: true },
        },
    },
}