import React, { useEffect, useRef, useState } from "react";
import { Table, Button, Icon, Label, Modal, Form, Grid } from "semantic-ui-react";
import { FormattedMessage } from "react-intl";
import { ZenFieldNumber, ButtonDelete, ButtonEdit, ZenButton, ZenField, ZenFieldDate, ZenFieldSelect, ZenFieldSelectApi, ZenFieldTextArea, ZenFormik, ZenMessageAlert, ZenMessageToast, ZenModal, FormatNumber, FormatDate } from "../../../components/Control";
import { MESSAGES, FormMode } from "utils";
import * as permissions from "../../../constants/permissions";
import { ApiHrQtLuong, ApiHrHsNs, ApiHrQtPhucap } from "../Api/index";
import { ZenHelper } from "../../../utils";
import { zzControlHelper } from "../../../components/Control/zzControlHelper";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { ApiFileAttachment } from "../../../Api";
import { ListFileUpload } from "./listFileUpload";
import { isEmpty } from "lodash";

const DetailLuongTabList = (props) => {
    const { idnv } = props
    const [ctx, setCtx] = useState([])
    const [ctxphucap, setCtxphucap] = useState([])
    const [open, setOpen] = useState(false)
    const [id, setId] = useState();
    const [index, setIndex] = useState();
    const [openphucap, setOpenphucap] = useState(false)
    const [idphucap, setIdphucap] = useState();
    const [indexphucap, setIndexphucap] = useState();
    const [error, setError] = useState();
    function fetchRequestLuong() {
        var result = new Promise((resolve, reject) =>
            ApiHrHsNs.getLuong(idnv, res => {
                if (res.status >= 200 && 204 >= res.status) {
                    resolve(res.data.data)
                } else {
                    reject(res)
                }
            })
        )
        return result
    }

    function fetchRequestPhucap() {
        var result = new Promise((resolve, reject) =>
            ApiHrHsNs.getPhucap(idnv, res => {
                if (res.status >= 200 && 204 >= res.status) {
                    resolve(res.data.data)
                } else {
                    reject(res)
                }
            })
        )
        return result
    }

    let questBefore = (mess, callBack) => {
        let result = ZenMessageAlert.question(mess);
        Promise.all([result])
            .then(values => {
                if (values[0]) {
                    callBack()
                }
            }).catch(err => {

            });
    }

    useEffect(() => {
        fetchRequestLuong().then(data => {
            setCtx(data)
        }).catch(err => {
            setError(ZenHelper.getResponseError(err))
        })
        fetchRequestPhucap().then(data => {
            setCtxphucap(data)
        }).catch(err => {
            setError(ZenHelper.getResponseError(err))
        })
    }, [])

    const openModal = (item, index) => {
        if (item) {
            setId(item.id)
        } else {
            setId()
        }
        setIndex(index)
        setOpen(true)
    }
    const handleDeleteItem = (e, index) => {
        questBefore(`Bạn muốn xóa Quá trình lương của ${e.ten_nv} ?`, () => {
            ApiHrQtLuong.delete(e.id, res => {
                if (res.status >= 200 && 204 >= res.status) {
                    const newItems = ctx.filter((t, idx) => idx !== index);
                    setCtx(newItems)
                    ZenMessageToast.success();
                } else {
                    setError(zzControlHelper.getResponseError(res))
                    ZenMessageAlert.error(zzControlHelper.getResponseError(res))
                }
            })
        })
    }

    const handleDeleteItemPhucap = (e, index) => {
        questBefore(`Bạn muốn xóa Quá trình phụ cấp của ${e.ten_nv} ?`, () => {
            ApiHrQtPhucap.delete(e.id, res => {
                if (res.status >= 200 && 204 >= res.status) {
                    const newItems = ctxphucap.filter((t, idx) => idx !== index);
                    setCtxphucap(newItems)
                    ZenMessageToast.success();
                } else {
                    setError(zzControlHelper.getResponseError(res))
                    ZenMessageAlert.error(zzControlHelper.getResponseError(res))
                }
            })
        })
    }

    var findIndex = (id, state) => {
        var result = -1;
        state.forEach((task, index) => {
            if (task.id === id) {
                result = index;
            }
        });
        return result;
    }

    const handleAfterUpload = (newItem, id = null) => {
        if (id) {
            if (newItem.file_upload) {
                fetchRequestLuong().then(data => {
                    setCtx(data.filter(dt => dt.id_nv === idnv))
                }).catch(err => {
                    setError(ZenHelper.getResponseError(err))
                })
                setOpen(false)
            } else {
                var indx = findIndex(newItem.id, ctx)
                ctx[indx] = newItem
                setCtx(ctx)
                setOpen(false)
            }
        } else {
            setCtx(ctx.concat(newItem))
            setOpen(false)
        }
    }

    const handleAfterSavephucap = (newItem, id = null) => {
        if (id) {
            let indx = findIndex(newItem.id, ctxphucap)
            ctxphucap[indx] = newItem
            setCtxphucap(ctxphucap)
            setOpenphucap(false)
        } else {
            setCtxphucap(ctxphucap.concat(newItem))
            setOpenphucap(false)
        }
    }

    const handleAfterUploadFiles = (newItem, id, indx,) => {
        const item = ctx[indx]
        if (newItem) {
            if (item.files) {
                item.files = item.files.concat([newItem])
                ctx[indx] = item
                setCtx([].concat(ctx))
            } else {
                item.files = [].concat(newItem)
                ctx[indx] = item
                setCtx([].concat(ctx))
            }
        } else {
            if (!newItem && id) {
                item.files = item.files.filter(t => t.id !== id)
                ctx[indx] = item
                setCtx([].concat(ctx))
            }
        }
    }

    const openModalphucap = (item, index) => {
        if (item) {
            setIdphucap(item.id)
        } else {
            setIdphucap()
        }
        setIndexphucap(index)
        setOpenphucap(true)
    }

    return <>
        <Grid style={{ padding: "1em 1em" }}>
            <Grid.Row>
                <div style={{ width: "100%", padding: '0px 0px 7px 0px' }}>
                    <ZenButton
                        icon="add"
                        primary
                        floated="right"
                        content="Thêm mới quá trình lương"
                        onClick={() => { openModal() }}
                    />
                </div>
                <div style={{ overflowX: 'auto', overFlowY: 'auto', height: 'auto', maxHeight: '300px', width: "100%" }}>
                    <Table celled selectable striped>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell width="2" textAlign="center" className="sticky-top">
                                    Ngày áp dụng
                                </Table.HeaderCell>
                                <Table.HeaderCell width="2" textAlign="center" className="sticky-top">
                                    Mức lương
                                </Table.HeaderCell>
                                <Table.HeaderCell width="2" textAlign="center" className="sticky-top">
                                    Tỉ lệ doanh thu
                                </Table.HeaderCell>
                                <Table.HeaderCell textAlign="center" className="sticky-top">
                                    Ghi chú
                                </Table.HeaderCell>
                                <Table.HeaderCell textAlign="center" className="sticky-top">
                                    File kèm theo
                                </Table.HeaderCell>
                                <Table.HeaderCell collapsing className="sticky-top"></Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>
                        {ctx && !isEmpty(ctxphucap) && (
                            <Table.Body>
                                {ctx.map((item, index) => {
                                    return (
                                        <Table.Row key={item.id}>
                                            <Table.Cell textAlign="center"> <FormatDate value={item.ngay_ad} /></Table.Cell>
                                            <Table.Cell textAlign="right">
                                                <FormatNumber value={item.muc_luong} />
                                            </Table.Cell>
                                            <Table.Cell textAlign="right">
                                                {item.tl_doanh_thu ? (<><FormatNumber value={item.tl_doanh_thu} /> %</>) : null}
                                            </Table.Cell>
                                            <Table.Cell textAlign="center">
                                                {item.ghi_chu}
                                            </Table.Cell>
                                            <Table.Cell style={{ maxWidth: "150px" }} width="3">
                                                <ListFileUpload
                                                    codeName={'HR'}
                                                    docKey={item.id}
                                                    defaultTypefile={'LUONG'}
                                                    files={item.files}
                                                    indx={index}
                                                    handleAfterUpload={handleAfterUploadFiles}
                                                />
                                            </Table.Cell>
                                            <Table.Cell collapsing>

                                                <Button.Group size="small" style={{ float: "right" }}>
                                                    <ButtonEdit
                                                        onClick={() => openModal(item, index)}
                                                    />
                                                    <ButtonDelete
                                                        onClick={() => handleDeleteItem(item, index)}
                                                    />
                                                </Button.Group>
                                            </Table.Cell>
                                        </Table.Row>
                                    );
                                })}
                            </Table.Body>
                        )}
                    </Table>
                </div>
                {open && <ModalQtluong open={open}
                    id={id}
                    idnv={idnv}
                    onClose={() => setOpen(false)}
                    onAfterSave={handleAfterUpload}
                />}
            </Grid.Row>
            <Grid.Row>
                <div style={{ width: "100%", padding: '0px 0px 7px 0px' }}>
                    <ZenButton
                        icon="add"
                        primary
                        floated="right"
                        content="Thêm mới quá trình phụ cấp"
                        onClick={() => { openModalphucap() }}
                    />
                </div>
                <div style={{ overflowX: 'auto', overFlowY: 'auto', height: 'auto', maxHeight: '250px', width: "100%" }}>
                    <Table celled selectable striped>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell width="2" textAlign="center" className="sticky-top">
                                    Ngày áp dụng
                                </Table.HeaderCell>
                                <Table.HeaderCell textAlign="center" className="sticky-top">
                                    Phụ cấp trách nhiệm
                                </Table.HeaderCell>
                                <Table.HeaderCell textAlign="center" className="sticky-top">
                                    Phụ cấp khác
                                </Table.HeaderCell>
                                <Table.HeaderCell collapsing className="sticky-top"></Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>
                        {ctxphucap && !isEmpty(ctxphucap) && (
                            <Table.Body>
                                {ctxphucap.map((item, index) => {
                                    return (
                                        <Table.Row key={item.id}>
                                            <Table.Cell textAlign="center"> <FormatDate value={item.ngay_ad} /></Table.Cell>
                                            <Table.Cell textAlign="right">
                                                <FormatNumber value={item.pc_trach_nhiem} />
                                            </Table.Cell>
                                            <Table.Cell textAlign="right">
                                                <FormatNumber value={item.pc_khac} />
                                            </Table.Cell>
                                            <Table.Cell collapsing>

                                                <Button.Group size="small" style={{ float: "right" }}>
                                                    <ButtonEdit
                                                        onClick={() => openModalphucap(item, index)}
                                                    />
                                                    <ButtonDelete
                                                        onClick={() => handleDeleteItemPhucap(item, index)}
                                                    />
                                                </Button.Group>
                                            </Table.Cell>
                                        </Table.Row>
                                    );
                                })}
                            </Table.Body>
                        )}
                    </Table>
                </div>
                {openphucap && <ModalQtphucap open={openphucap}
                    id={idphucap}
                    idnv={idnv}
                    onClose={() => setOpenphucap(false)}
                    onAfterSave={handleAfterSavephucap}
                />}
            </Grid.Row>
        </Grid>
    </>
};

const ModalQtluong = ({ open, id, idnv, onClose, onAfterSave, title }) => {
    const _isMounted = useRef(true);
    const refFormik = useRef();
    const [fileInfo, setFileInfo] = useState();
    const [itemInfo, setItemInfo] = useState();
    const [error, setError] = useState();
    const [roles, setRoles] = useState([]);
    const [btnLoading, setBtnLoading] = useState();

    useEffect(() => {
        zzControlHelper.dragElement(document.getElementById('qtluong-form'), 'header-qtluong-form')
    })

    useEffect(() => {
        if (id) {
            ApiHrQtLuong.getByCode(id, res => {
                if (res.status >= 200 && 204 >= res.status) {
                    refFormik.current.setValues({
                        ...refFormik.current.values,
                        ...res.data.data,
                    })
                } else {
                    setError(zzControlHelper.getResponseError(res))
                }
            })
        } else {
            refFormik.current.setValues({
                ...refFormik.current.values,
                id_nv: idnv,
            })
        }
    }, [])

    const handleSubmit = (params, formId) => {
        //setBtnLoading(true)
        if (id) {
            ApiHrQtLuong.update(params, res => {
                if (res.status >= 200 && 204 >= res.status) {
                    onAfterSave && onAfterSave({ ...params, file_upload: fileInfo }, id)
                    ZenMessageToast.success();
                } else {
                    setError(zzControlHelper.getResponseError(res))
                    ZenMessageAlert.error(zzControlHelper.getResponseError(res))
                }
                if (btnLoading) setBtnLoading()
            })
        } else {
            ApiHrQtLuong.insert(params, res => {
                if (res.status >= 200 && 204 >= res.status) {
                    onAfterSave && onAfterSave({ ...res.data.data })
                    ZenMessageToast.success();
                } else {
                    setError(zzControlHelper.getResponseError(res))
                    ZenMessageAlert.error(zzControlHelper.getResponseError(res))
                }
                if (btnLoading) setBtnLoading()
            })
        }
        if (btnLoading) setBtnLoading()
    }
    function selectedFile(e) {
        return new Promise(resolve => {
            if (e.target.files.length > 0) {
                const reader = new FileReader(),
                    file = e.target.files[0];
                reader.onloadend = () => {
                    resolve(file);
                };
                reader.readAsDataURL(file);
            }
        })
    }
    const handleChangeSelecthdld = (e, i) => {
        if (i && i.value) {
            refFormik.current.setValues({
                ...refFormik.current.values,
                ma_loai_hdld: i.value,
                ten_loai_hdld: i.options.filter(t => t.value === i.value)[0].ten
            })
        }
    }

    const handleChangeTH = (e, i) => {
        const date = new Date(refFormik.current.values.ngay_bd);
        date.setMonth(date.getMonth() + +e.target.value);
        refFormik.current.setValues({
            ...refFormik.current.values,
            thoi_han: e.target.value,
            ngay_kt: zzControlHelper.formatDateTime(date, 'YYYY-MM-DD'),
        })
    }

    const handleChangeDateBD = (e, i) => {
        const date = new Date(e.value);
        date.setMonth(date.getMonth() + +refFormik.current.values.thoi_han);
        if (e.value) {
            refFormik.current.setValues({
                ...refFormik.current.values,
                ngay_bd: e.value,
                ngay_kt: zzControlHelper.formatDateTime(date, 'YYYY-MM-DD'),
            })
        }
    }

    // const onValidate = (item) => {
    //     const errors = {};
    //     const { ngay_bd, ngay_ky, ngay_kt } = item
    //     const ngay1 = parseInt(ngay_bd) > 1900 ? ngay_bd : null
    //     const ngay2 = parseInt(ngay_ky) > 1900 ? ngay_ky : null
    //     const ngay3 = parseInt(ngay_kt) > 1900 ? ngay_kt : null
    //     // if (!ngay3 || !ngay2 || !ngay1) {
    //     //     return
    //     // }
    //     if (ngay1 < ngay2) {
    //         errors.ngay_bd = 'Ngày hiệu lực phải lớn hơn hoặc bằng ngày ký';
    //     }
    //     if (ngay3 <= ngay2) {
    //         errors.ngay_kt = 'Ngày kết thúc phải lớn hơn hiệu lực';
    //     }
    //     return errors
    // }

    const customHeader = (id) => {
        const header = 'Quá trình lương'
        const txtHeader = !id ? `Thêm mới ${header}` : capitalize(header) + ` : ${id}`
        return txtHeader
    }
    const capitalize = (s) => {
        if (typeof s !== 'string') return ''
        return s.charAt(0).toUpperCase() + s.slice(1)
    }

    return <>
        <Modal id="qtluong-form"
            closeOnEscape closeIcon closeOnDimmerClick={false}
            onClose={() => onClose()}
            //onOpen={() => onClose()}
            open={open}
            size={"tiny"}
        >
            <Modal.Header id='header-qtluong-form' style={{ cursor: "grabbing" }}>
                {customHeader(id)}
            </Modal.Header>

            <Modal.Content>
                <ZenFormik form={"hrdmother"} ref={refFormik}
                    validation={formValidation}
                    initItem={initItem}
                    onSubmit={handleSubmit}
                //onValidate={onValidate}
                >
                    {
                        formik => {
                            return <Form>
                                <Form.Group>
                                    <ZenFieldDate width={8} name="ngay_ad"
                                        required
                                        props={formik}
                                        label="hdld.ngay_ad" defaultlabel="Ngày áp dụng"
                                    />
                                </Form.Group>
                                <Form.Group>
                                    <ZenFieldNumber width={8} name="muc_luong" props={formik}
                                        required
                                        label="hdld.muc_luong" defaultlabel="Mức lương"
                                    />
                                    <ZenFieldNumber width={8} name="tl_doanh_thu" props={formik}
                                        decimalScale={2}
                                        label="hdld.tl_doanh_thu" defaultlabel="Tỷ lệ hưởng trên doanh thu (%)"
                                    />
                                </Form.Group>
                                <Form.Group>
                                    <ZenFieldNumber width={16} name="muc_luong_gio" props={formik}
                                        label="hdld.muc_luong_gio" defaultlabel="Mức lương giờ"
                                    />
                                </Form.Group>
                                <Form.Group>
                                    <ZenFieldNumber width={8} name="bh_luong" props={formik}
                                        required
                                        label="hdld.bh_luong" defaultlabel="Mức lương đóng bảo hiểm"
                                    />
                                    <ZenFieldNumber width={8} name="bh_tl" props={formik}
                                        decimalScale={2}
                                        label="hdld.bh_tl" defaultlabel="Tỷ lệ đóng bảo hiểm (%)"
                                    />
                                </Form.Group>
                                <Form.Group>
                                    <ZenFieldNumber width={8} name="tncn_gtgc" props={formik}
                                        required
                                        label="hdld.tncn_gtgc" defaultlabel="Mức giảm trừ thuế TNCN"
                                    />
                                    <ZenFieldNumber width={8} name="tncn_tl" props={formik}
                                        decimalScale={2}
                                        label="hdld.tncn_tl" defaultlabel="Tỷ lệ tính thuế TNCN (%)"
                                    />
                                </Form.Group>
                                <Form.Group>
                                    <ZenField width={8} name="so_qd" props={formik}
                                        label="hdld.so_qd" defaultlabel="Số quyết định"
                                    />
                                    <ZenFieldDate width={8} name="ngay_qd" props={formik}
                                        label="hdld.ngay_qd" defaultlabel="Ngày ký quyết định"
                                    />
                                </Form.Group>
                                <Form.Group>
                                    <ZenFieldSelectApi width="16"
                                        lookup={{
                                            ...ZenLookup.ID_NV,
                                            // onLocalWhere: (items) => {
                                            //     return items.filter(t => t.loai_hsns == '5')
                                            // },
                                            where: "loai_hsns = 5"
                                        }}
                                        label="hrlichday.id_nv_ky" defaultlabel="Người ký"
                                        name="id_nv_ky"
                                        formik={formik}
                                    />
                                </Form.Group>
                                <Form.Group>
                                    <ZenFieldTextArea width={16} name="ghi_chu" props={formik}
                                        label="hdld.ghi_chu" defaultlabel="Ghi chú"
                                    />
                                </Form.Group>
                            </Form>
                        }
                    }
                </ZenFormik>
            </Modal.Content>
            <Modal.Actions>
                <ZenButton btnType={"cancel"} size="small" onClick={() => onClose()} />
                <ZenButton btnType="save" size="small"
                    loading={btnLoading} type="submit"
                    onClick={(e) => refFormik.current.handleSubmit(e)}
                />
            </Modal.Actions>
        </Modal>
    </>
}

const initItem = {
    id_nv: "",
    ngay_ad: "",
    muc_luong: 0,
    muc_luong_gio: 0,
    tl_doanh_thu: 0,
    so_qd: "",
    ngay_qd: "",
    id_nv_ky: "",
    ghi_chu: "",
    bh_luong: "",
    bh_tl: "",
    tncn_gtgc: "",
    tncn_tl: "",
}

const formValidation = [
    {
        id: "ngay_ad",
        validationType: "string",
        validations: [
            {
                type: "required",
                params: ["Không được bỏ trống trường này"]
            },
        ]
    },
    {
        id: "muc_luong",
        validationType: "number",
        validations: [
            {
                type: "required",
                params: ["Không được bỏ trống trường này"]
            },
            {
                type: "min",
                params: [1, "Không được nhỏ hơn 0"]
            },
        ]
    },
]

const ModalQtphucap = ({ open, id, idnv, onClose, onAfterSave, title }) => {
    const _isMounted = useRef(true);
    const refFormik = useRef();
    const refUpload = useRef();
    const [attachmentInfo, setAttachmentInfo] = useState();
    const [fileInfo, setFileInfo] = useState();
    const [itemInfo, setItemInfo] = useState();
    const [error, setError] = useState();
    const [roles, setRoles] = useState([]);
    const [btnLoading, setBtnLoading] = useState();

    useEffect(() => {
        zzControlHelper.dragElement(document.getElementById('qtphucap-form'), 'header-qtphucap-form')
    })

    useEffect(() => {
        if (id) {
            ApiHrQtPhucap.getByCode(id, res => {
                if (res.status >= 200 && 204 >= res.status) {
                    refFormik.current.setValues({
                        ...refFormik.current.values,
                        ...res.data.data,
                    })
                } else {
                    setError(zzControlHelper.getResponseError(res))
                }
            })
        } else {
            refFormik.current.setValues({
                ...refFormik.current.values,
                id_nv: idnv,
            })
        }
    }, [])


    const handleSubmit = (params, formId) => {
        if (id) {
            ApiHrQtPhucap.update(params, res => {
                if (res.status >= 200 && 204 >= res.status) {
                    onAfterSave && onAfterSave({ ...params, }, id)
                    ZenMessageToast.success();
                } else {
                    setError(zzControlHelper.getResponseError(res))
                    ZenMessageAlert.error(zzControlHelper.getResponseError(res))
                }
                if (btnLoading) setBtnLoading()
            })
        } else {
            ApiHrQtPhucap.insert(params, res => {
                if (res.status >= 200 && 204 >= res.status) {
                    onAfterSave && onAfterSave({ ...res.data.data })
                    ZenMessageToast.success();
                } else {
                    setError(zzControlHelper.getResponseError(res))
                    ZenMessageAlert.error(zzControlHelper.getResponseError(res))
                }
                if (btnLoading) setBtnLoading()
            })
        }
        if (btnLoading) setBtnLoading()
    }

    const customHeader = (id) => {
        const header = 'Quá trình phụ cấp'
        const txtHeader = !id ? `Thêm mới ${header}` : capitalize(header) + ` : ${id}`
        return txtHeader
    }
    const capitalize = (s) => {
        if (typeof s !== 'string') return ''
        return s.charAt(0).toUpperCase() + s.slice(1)
    }

    return <>
        <Modal id="qtphucap-form"
            closeOnEscape closeIcon closeOnDimmerClick={false}
            onClose={() => onClose()}
            //onOpen={() => onClose()}
            open={open}
            size={"tiny"}
        >
            <Modal.Header id='header-qtphucap-form' style={{ cursor: "grabbing" }}>
                {customHeader(id)}
            </Modal.Header>

            <Modal.Content>
                <ZenFormik form={"hrqtphucap"} ref={refFormik}
                    validation={formValidationphucap}
                    initItem={initItemphucap}
                    onSubmit={handleSubmit}
                //onValidate={onValidate}
                >
                    {
                        formik => {
                            return <Form>
                                <Form.Group>
                                    <ZenFieldDate width={8} name="ngay_ad" width="16"
                                        required
                                        props={formik}
                                        label="hdld.ngay_ad" defaultlabel="Ngày áp dụng"
                                    />
                                </Form.Group>
                                <Form.Group>
                                    <ZenFieldNumber width={16} name="pc_trach_nhiem" props={formik}
                                        label="hdld.pc_trach_nhiem" defaultlabel="Phụ cấp trách nhiệm"
                                    />
                                </Form.Group>
                                <Form.Group>
                                    <ZenFieldNumber width={16} name="pc_khac" props={formik}
                                        label="hdld.pc_khac" defaultlabel="Phụ cấp khác"
                                    />
                                </Form.Group>
                            </Form>
                        }
                    }
                </ZenFormik>
            </Modal.Content>
            <Modal.Actions>
                <ZenButton btnType={"cancel"} size="small" onClick={() => onClose()} />
                <ZenButton btnType="save" size="small"
                    loading={btnLoading} type="submit"
                    onClick={(e) => refFormik.current.handleSubmit(e)}
                />
            </Modal.Actions>
        </Modal>
    </>
}

const initItemphucap = {
    id_nv: "",
    ngay_ad: "",
    pc_trach_nhiem: 0,
}

const formValidationphucap = [
    {
        id: "ngay_ad",
        validationType: "string",
        validations: [
            {
                type: "required",
                params: ["Không được bỏ trống trường này"]
            },
        ]
    },
]

export default DetailLuongTabList