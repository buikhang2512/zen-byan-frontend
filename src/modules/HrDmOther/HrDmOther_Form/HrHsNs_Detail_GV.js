import React, { useEffect, useRef, useState } from "react";
import { Table, Button, Icon, Label, Modal, Form } from "semantic-ui-react";
import { FormattedMessage } from "react-intl";
import { ButtonDelete, ButtonEdit, ZenButton, ZenField, ZenFieldDate, ZenFieldSelect, ZenFieldSelectApi, ZenFieldSelectApiMulti, ZenFieldTextArea, ZenFormik, ZenMessageAlert, ZenMessageToast, ZenModal } from "../../../components/Control";
import { MESSAGES, FormMode } from "utils";
import * as permissions from "../../../constants/permissions";
import { ApiHrQtHopDongLD } from "../Api/ApiHrQtHopDongLD";
import { ZenHelper } from "../../../utils";
import { zzControlHelper } from "../../../components/Control/zzControlHelper";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { ApiFileAttachment } from "../../../Api";
import { ApiHrHsNs, ApiHrHsNsGV } from "../Api";
import { GlobalStorage, KeyStorage } from "../../../utils/storage";
import _ from "lodash";

const DetailGVTabList = (props) => {
    const { idnv } = props
    const [data, setData] = useState();
    const [index, setIndex] = useState();
    const _isMounted = useRef(true);
    const refFormik = useRef();
    const refUpload = useRef();
    const [realdonly, setReadonly] = useState(true);
    const [error, setError] = useState();
    const [btnLoading, setBtnLoading] = useState();
    const [dataOriginal, setDataOriginal] = useState();
    const [tdcm, setTdcm] = useState([]);
    const [roles, setRoles] = useState([]);
    const [rolesCap, setRolesCap] = useState([]);
    const [tinh, setTinh] = useState([]);
    const [huyen, setHuyen] = useState([]);
    useEffect(() => {
        refFormik.current.setValues({
            ...refFormik.current.values,
            ...data,
        })
        setDataOriginal(data)
    }, [data])

    useEffect(() => {
        const daciem = GlobalStorage.getByField(KeyStorage.CacheData, "ma_dac_diem_gv")?.data
        const cap = GlobalStorage.getByField(KeyStorage.CacheData, "ma_tddt")?.data
        const tdcm = GlobalStorage.getByField(KeyStorage.CacheData, "ma_tdcm")?.data
        setTdcm(ZenHelper.translateListToSelectOptions(tdcm, false, 'ma', 'ma', 'ma', [], 'ten'))
        setRoles(ZenHelper.translateListToSelectOptions(daciem, false, 'ma', 'ma', 'ma', [], 'ten'))
        setRolesCap(ZenHelper.translateListToSelectOptions(cap, false, 'ma', 'ma', 'ma', [], 'ten'))

        const tinh = GlobalStorage.getByField(KeyStorage.CacheData, "ma_tinh")?.data
        const huyen = GlobalStorage.getByField(KeyStorage.CacheData, "ma_huyen")?.data
        setTinh(ZenHelper.translateListToSelectOptions(tinh, false, 'ma_tinh', 'ma_tinh', 'ma_tinh', [], 'ten_tinh'))
        setHuyen(ZenHelper.translateListToSelectOptions(huyen, false, 'ma_huyen', 'ma_huyen', 'ma_huyen', [], 'ten_huyen'))
    }, [])

    useEffect(() => {
        ApiHrHsNsGV.get(idnv, res => {
            if (res.status >= 200 && 204 >= res.status) {
                refFormik.current.setValues({
                    ...refFormik.current.values,
                    ...res.data.data,
                    ma_tdcm: res.data.data.ma_tdcm?.split(','),
                    ma_dac_diem_gv: res.data.data.ma_dac_diem_gv?.split(','),
                    ma_cap: res.data.data.ma_cap?.split(','),
                    ma_tinh_kv: res.data.data.ma_tinh_kv?.split(','),
                    ma_huyen_kv: res.data.data.ma_huyen_kv?.split(','),
                })
                setDataOriginal({
                    ...res.data.data,
                    ma_tdcm: res.data.data.ma_tdcm?.split(','),
                    ma_dac_diem_gv: res.data.data.ma_dac_diem_gv?.split(','),
                    ma_cap: res.data.data.ma_cap?.split(','),
                    ma_tinh_kv: res.data.data.ma_tinh_kv?.split(','),
                    ma_huyen_kv: res.data.data.ma_huyen_kv?.split(','),
                })
            }
        })
    }, [])

    const handleAfterSave = (newItem) => {
        setData({
            ...newItem,
            ma_tdcm: newItem.ma_tdcm?.split(','),
            ma_dac_diem_gv: newItem.ma_dac_diem_gv?.split(','),
            ma_cap: newItem.ma_cap?.split(','),
            ma_tinh_kv: newItem.ma_tinh_kv?.split(','),
            ma_huyen_kv: newItem.ma_huyen_kv?.split(','),
        })
    }

    const handleSubmit = (params, formId) => {
        if (_.join(params.ma_tdcm, ',') !== ', ') {
            let next_stt = _.join(params.ma_tdcm, ',')
            params.ma_tdcm = next_stt
        }
        if (_.join(params.ma_dac_diem_gv, ',') !== ', ') {
            let next_stt = _.join(params.ma_dac_diem_gv, ',')
            params.ma_dac_diem_gv = next_stt
        }
        if (_.join(params.ma_cap, ',') !== ', ') {
            let next_stt = _.join(params.ma_cap, ',')
            params.ma_cap = next_stt
        }
        if (_.join(params.ma_tinh_kv, ',') !== ', ') {
            let next_stt = _.join(params.ma_tinh_kv, ',')
            params.ma_tinh_kv = next_stt
        }
        if (_.join(params.ma_huyen_kv, ',') !== ', ') {
            let next_stt = _.join(params.ma_huyen_kv, ',')
            params.ma_huyen_kv = next_stt
        }
        ApiHrHsNsGV.insert({ ...params, id_nv: idnv }, res => {
            if (res.status >= 200 && 204 >= res.status) {
                handleAfterSave({ ...params, id_nv: idnv })
                ZenMessageToast.success();
                setReadonly(true)
            } else {
                setError(zzControlHelper.getResponseError(res))
                ZenMessageAlert.error(zzControlHelper.getResponseError(res))
            }
            if (btnLoading) { setBtnLoading() }
        })

    }

    const onRefreshForm = () => {
        refFormik.current.setValues({
            ...refFormik.current.values,
            ...dataOriginal,
        })
        setReadonly(true)
    }

    return <>
        <ZenFormik form={"hrdmttc"} ref={refFormik}
            validation={formValidation}
            initItem={initItem}
            onSubmit={handleSubmit}
        >
            {
                formik => {
                    return <Form>
                        <Form.Group>
                            {/* <ZenFieldSelectApiMulti width={8} name="ma_tdcm" readOnly={realdonly}
                                // lookup={{
                                //     ...ZenLookup.HrDmKhac, format:"{ten}",
                                //     where: "code_name = 'MA_TDCM'"
                                // }}
                                lookup={{ ...ZenLookup.MA_TDCM, format: `{ten}` }}
                                formik={formik}
                                label="hdld.ma_tdcm" defaultlabel="Trình độ chuyên môn"
                            /> */}
                            <ZenFieldSelect width={8} multiple options={tdcm} readOnly={realdonly}
                                name="ma_tdcm"
                                props={formik}
                                label="hdld.ma_tdcm" defaultlabel="Trình độ chuyên môn"
                            />
                        </Form.Group>
                        <Form.Group>
                            <ZenFieldSelect width={8} multiple options={rolesCap} name={'ma_cap'} readOnly={realdonly}
                                props={formik}
                                label="hdld.ma_cap" defaultlabel="Cấp dạy"
                            />
                        </Form.Group>
                        <Form.Group>
                            <ZenFieldSelect width={8} multiple options={tinh} name="ma_tinh_kv" readOnly={realdonly}
                                props={formik}
                                label="hrhsnsgv.ma_tinh_kv" defaultlabel="Khu vực dạy (Tỉnh/thành phố)"
                            />
                        </Form.Group>
                        <Form.Group>
                            <ZenFieldSelect width={8} multiple options={huyen} name="ma_huyen_kv" readOnly={realdonly}
                                props={formik}
                                label="hrhsnsgv.ma_huyen_kv" defaultlabel="Khu vực dạy (Quận/huyện)"
                            />
                        </Form.Group>
                        <Form.Group>
                            <ZenFieldSelectApiMulti width={8} name="ma_opt1" readOnly={realdonly}
                                lookup={{
                                    ...ZenLookup.HrDmKhac, format: "{ten}",
                                    where: "code_name = 'MA_OPT1'",
                                    onLocalWhere: (items) => {
                                        return items.filter(x => x.code_name === 'MA_OPT1')
                                    }
                                }}
                                formik={formik}
                                label="hrhsnsgv.Available" defaultlabel="Available"
                            />
                        </Form.Group>
                        <Form.Group>
                            <ZenFieldSelect width={8} multiple options={roles} name="ma_dac_diem_gv" readOnly={realdonly}
                                label={"hrhsnsgv.dac_diem"} defaultlabel="Đặc điểm giáo viên"
                                props={formik}
                            />
                        </Form.Group>
                    </Form>
                }
            }
        </ZenFormik>

        <div className="btn-form-right">
            {
                !realdonly ? <>
                    <ZenButton btnType="cancel"
                        size="small"
                        content="Hủy"
                        color="red"
                        onClick={() => onRefreshForm()}
                    />
                    <ZenButton btnType="save"
                        size="small"
                        type="submit"
                        onClick={(e) => refFormik.current.handleSubmit(e)}
                        style={{ margin: "0px" }}
                        loading={btnLoading}
                        disabled={btnLoading}
                    />
                </>
                    : <ZenButton style={{ margin: 0 }}
                        permission={permissions.HrHsNsSua}
                        type={"button"}
                        size="small"
                        primary
                        icon="edit"
                        content={"Sửa thông tin chung"}
                        onClick={() => setReadonly(false)}
                    />
            }
        </div>
    </>
};

const initItem = {
    id_nv: "",
    ma_tdcm: "",
    ma_cap: "",
    ma_tinh_kv: "",
    ma_huyen_kv: "",
    ma_dac_diem_gv: "",
    ma_opt1: "",
}

const formValidation = [
    // {
    //     id: "ma_loai_hdld",
    //     validationType: "string",
    //     validations: [
    //         {
    //             type: "required",
    //             params: ["Không được bỏ trống trường này"]
    //         },
    //     ]
    // },
    // {
    //     id: "so_hd",
    //     validationType: "string",
    //     validations: [
    //         {
    //             type: "required",
    //             params: ["Không được bỏ trống trường này"]
    //         },
    //     ]
    // },
    // {
    //     id: "ngay_ky",
    //     validationType: "string",
    //     validations: [
    //         {
    //             type: "required",
    //             params: ["Không được bỏ trống trường này"]
    //         },
    //     ]
    // },
    // {
    //     id: "ngay_bd",
    //     validationType: "string",
    //     validations: [
    //         {
    //             type: "required",
    //             params: ["Không được bỏ trống trường này"]
    //         },
    //     ]
    // },
    // {
    //     id: "ngay_kt",
    //     validationType: "string",
    //     validations: [
    //         {
    //             type: "required",
    //             params: ["Không được bỏ trống trường này"]
    //         },
    //     ]
    // },
]

export default DetailGVTabList