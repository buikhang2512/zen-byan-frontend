import React, { useEffect, useRef, useState } from "react";
import { Breadcrumb, Button, Form, Segment, Table, Popup } from "semantic-ui-react";
import {
    ContainerScroll, ZenFormik, FormatNumber,
    SegmentHeader, ZenFieldSelectApi, HeaderLink, ZenLoading, ZenFieldNumber, ZenMessageAlert, ZenMessageToast, Helmet,
} from "../../../../components/Control/index";
import { zzControlHelper } from "../../../../components/Control/zzControlHelper";
import { ZenHelper } from "../../../../utils";
import { ZenLookup } from "../../../ComponentInfo/Dictionary/ZenLookup";
import * as routes from './../../../../constants/routes'
import _, { isEmpty } from "lodash";
import './HRLICHDAY.css'
import { ApiPAChamCongKH } from "../../Api/ApiPAChamCongKh";
import './HRChamCongGv.css'
import { GlobalStorage, KeyStorage } from "../../../../utils/storage";
import { ApiPADmKyLuong } from "../../../PA/Api";
import * as permissions from "../../../../constants/permissions";

export const HrChamCongGvForm = (props) => {
    const { onLoadData, fieldCode } = props;
    const refForm = useRef();
    const [loadingForm, setLoadingForm] = useState(false)
    const [_kyluong, setKyluong] = useState(ZenHelper.formatDateTime(new Date(), 'YYYY/MM/DD'))
    const [firstdayofm, setfirstdayofm] = useState(getFirstDayMonth())
    const [lastdayofm, setlastdayofm] = useState(getLastDayMonth())
    const [data, setData] = useState([])
    const [tennv, setTennv] = useState([])
    const [maKyLuong, setMaKyLuong] = useState()
    const [idNv, setIdNv] = useState()

    const [widthtennv, setWidthtennv] = useState()
    const [widthkh, setWidthkh] = useState()
    const [widthtotalhour, setWidthtotalhour] = useState()
    const [heighttennv, setHeighttennv] = useState()
    const [tongcong, setTongcong] = useState([])
    // useEffect(() => {

    // }, [])
    useEffect(() => {
        const elementtennv = document.getElementById('getwidthtennv-id')
        const elementkh = document.getElementById('getwidthkh-id')
        const elementtotal = document.getElementById('getwidthtotalhour-id')
        const widthtennv = elementtennv.getBoundingClientRect().width
        const widthkh = elementkh.getBoundingClientRect().width
        const widthtotalhour = elementtotal.getBoundingClientRect().width
        const heighttennv = elementtennv.getBoundingClientRect().height
        setWidthtennv(widthtennv)
        setWidthkh(widthkh)
        setWidthtotalhour(widthtotalhour)
        setHeighttennv(heighttennv)
    })

    function getfianceyear() {
        return GlobalStorage.getByField(KeyStorage.Global, 'financial_year')
    }

    function getChamconggv() {
        setLoadingForm(true)
        ApiPAChamCongKH.get({ maKyLuong: maKyLuong, idNv: idNv }, res => {
            if (res.status === 200) {
                if (res.data.data && res.data.data.length > 0) {
                    setData(res.data.data)
                    setTennv(Object.keys(res.data.data.reduce((r, { ten_nv }) => (r[ten_nv] = '', r), {})))
                }
            }
            setLoadingForm(false)
        })
    }

    function getFirstDayMonth(day) {
        let date = day ? new Date(day) : new Date();
        let month = date.getMonth(),
            year = date.getFullYear();
        return ZenHelper.getfirstDayOfMonth(month, year)
    }

    function getLastDayMonth(day) {
        let date = day ? new Date(day) : new Date();
        let month = date.getMonth(),
            year = date.getFullYear();
        return ZenHelper.getLastDayOfMonth(month, year)
    }

    function renderHeaderTable() {
        let result = [];
        let firstday = _.clone(firstdayofm)
        let dateoffirstday = firstday.getDate()
        const lastDay = lastdayofm.getDate()
        for (let i = dateoffirstday; i <= lastDay; i++) {
            result.push(<Table.HeaderCell textAlign="center" className="HRCCGV-sticky-top">{
                `${getDayofWeek(new Date(firstday.setDate(i)).getDay())}`}
                <br />
                {`${zzControlHelper.formatDateTime(firstday.setDate(i), 'DD/MM')}`}
            </Table.HeaderCell>)
        }
        return result
    }

    function getDayofWeek(day) {
        var _switch = {
            0: 'CN',
            1: 'T2',
            2: 'T3',
            3: 'T4',
            4: 'T5',
            5: 'T6',
            6: 'T7',
        };
        return _switch[day]
    }

    function rendercalendarfirst(data) {
        return data.map(item => {
            return <>
                <Table.Cell style={{ left: `${widthtennv}px` }} className="HRCCGV-sticky-left" content={item.ma_kh} />
                <Table.Cell style={{ left: `${widthtennv + widthkh}px` }} className="HRCCGV-sticky-left" content={item.tong_gio} />
                <Table.Cell style={{ left: `${widthtennv + widthkh + widthtotalhour}px` }} className="HRCCGV-sticky-left" content={item.tong_ngay} />
                {
                    renderTableCellgiocong(item)
                }
            </>

        })
    }

    function rendercalendar(data) {
        if (data && data.length > 0) {
            return data.map(item => {
                return <Table.Row>
                    <Table.Cell style={{ left: `${widthtennv}px` }} className="HRCCGV-sticky-left" content={item.ma_kh} />
                    <Table.Cell style={{ left: `${widthtennv + widthkh}px` }} className="HRCCGV-sticky-left" content={item.tong_gio} />
                    <Table.Cell style={{ left: `${widthtennv + widthkh + widthtotalhour}px` }} className="HRCCGV-sticky-left" content={item.tong_ngay} />
                    {
                        renderTableCellgiocong(item)
                    }
                </Table.Row>

            })
        }
    }

    function renderTableCellgiocong(item) {
        let result = [];
        let firstday = _.clone(firstdayofm)
        let dateoffirstday = firstday.getDate()
        const lastDay = lastdayofm.getDate()
        for (let i = dateoffirstday; i <= lastDay; i++) {
            let dayofweek = getDayofWeek(new Date(firstday.setDate(i)).getDay())
            let active = dayofweek === 'T7' || dayofweek === 'CN'
            result.push(
                <PopupUpdateTimeKeeping
                    workhour={item[`ngay${i}_gio`]}
                    id={item.id}
                    fname={`ngay${i}_gio`}
                    active={active}
                    onAfterSave={handleAfterSave}
                />)
        }
        return result
    }

    const handleAfterSave = ({ sogio, id, fname }) => {
        const item = data.filter(t => t.id === id)[0]
        item[fname] = sogio
        const countDay = lastdayofm.getDate()
        item[`tong_gio`] = 0
        for (let i = 1; i <= countDay; i++) {
            item[`tong_gio`] += item[`ngay${i}_gio`]
        }
        tongCongchamconggv(data)
        setData([].concat(data))
    }

    let questBefore = (mess, callBack) => {
        let result = ZenMessageAlert.question(mess);
        Promise.all([result])
            .then(values => {
                if (values[0]) {
                    callBack()
                }
            }).catch(err => {

            });
    }

    const handleSelect = (item, { name }) => {
        if (name === 'ma_ky_luong') {
            setMaKyLuong(item.ma_ky_luong)
            setfirstdayofm(new Date(item.ngay_bd))
            setlastdayofm(new Date(item.ngay_kt))
        }

        if (name === 'id_nv') {
            setIdNv(item.id_nv)
        }
        setLoadingForm(true)
        ApiPAChamCongKH.get({ maKyLuong: item.ma_ky_luong || maKyLuong, idNv: name === 'id_nv' ? item.id_nv : idNv }, res => {
            if (res.status === 200) {
                setData(res.data.data)
                tongCongchamconggv(res.data.data)
                setTennv(Object.keys(res.data.data.reduce((r, { ten_nv }) => (r[ten_nv] = '', r), {})))
            }
            setLoadingForm(false)
        })
    }

    const handlSyntheticTimekeeping = (params) => {
        if (data && data.length > 0) {
            questBefore("Đã có dữ liệu tổng hợp chấm công giáo viên, khi tổng hợp từ chi tiết theo trường, các thay đổi sẽ bị thay thế. Bạn chắc chắn muốn thực hiện?", res => {
                tonghopchamcong(params)
            })
        } else {
            tonghopchamcong(params)
        }
    }
    function tonghopchamcong(params) {
        ApiPAChamCongKH.tonghop(params, res => {
            if (res.status >= 200 && res.status <= 204) {
                ZenMessageToast.success()
                getChamconggv()
            } else {
                ZenMessageAlert.error(zzControlHelper.getResponseError(res))
            }
        })
    }

    function tongCongchamconggv(data) {
        let tongcong = [];
        data.map(item => {
            if (!tongcong[`tong_gio`]) {
                tongcong[`tong_gio`] = 0
            }
            if (!tongcong[`tong_ngay`]) {
                tongcong[`tong_ngay`] = 0
            }
            tongcong[`tong_gio`] += item[`tong_gio`]
            tongcong[`tong_ngay`] += item[`tong_ngay`]
            for (let i = 1; i <= 31; i++) {
                if (!tongcong[`ngay${i}_gio`]) {
                    tongcong[`ngay${i}_gio`] = 0
                }
                if (i == 31) {
                    if (tongcong[`ngay${i}_gio`] >= 0) {
                        tongcong[`ngay${i}_gio`] += item[`ngay${i}_gio`]
                    }
                    return
                } else {
                    tongcong[`ngay${i}_gio`] += item[`ngay${i}_gio`]
                }
            }
        })
        setTongcong(tongcong)
    }

    useEffect(() => {
        ApiPADmKyLuong.get((res) => {
            if (res.status == 200) {
                let opt = res.data.data.filter(t => t.nam == getfianceyear())
                var closest = opt.reduce(function (prev, curr) {
                    return (Math.abs(new Date(curr.ngay_bd) - new Date()) < Math.abs(new Date(prev.ngay_kt) - new Date()) ? curr : prev);
                });
                refForm.current.setFieldValue('ma_ky_luong', closest.ma_ky_luong)
                handleSelect(closest, { name: 'ma_ky_luong' })
            }
        });
    }, []);

    return <>
        <div className='HRCCGV-header'>
            <PageHeader />
            <div className='HRCCGV-header-form'>
                <ZenFormik form={"HrChamCongGV-filter"}
                    ref={refForm}
                    initItem={initItem}
                    onSubmit={handlSyntheticTimekeeping}
                    validation={formValidation}
                    onReset={(item) => {
                        //setLoadingForm(true)
                        // setKyluong(ZenHelper.formatDateTime(item.ngay, 'YYYY/MM/DD'))
                        // setIdnv(item.idnv)
                    }}
                >
                    {
                        formik => {
                            return <Form style={{ maxWidth: 'inherit', paddingRight: "15px" }}>
                                <Form.Group widths='equal'>
                                    <ZenFieldSelectApi style={{ whiteSpace: 'nowrap', minWidth: '160px' }}
                                        name='ma_ky_luong'
                                        lookup={{
                                            ...ZenLookup.Ma_ky_luong, format: `{ten_ky_luong}`,
                                            where: `nam = '${getfianceyear()}'`,
                                            onLocalWhere: (items) => {
                                                return items.filter(t => t.nam == getfianceyear())
                                            }
                                        }}
                                        placeholder="Kỳ lương"
                                        formik={formik}
                                        onItemSelected={(item, name) => handleSelect(item, name)}
                                        isIntl={false}
                                        unClearable
                                    />
                                    {/* <Form.Field width="3">
                                    <label>Kỳ lương</label>
                                    <DatePicker
                                        selected={new Date(formik.values.ma_ky_luong)}
                                        dateFormat="dd/MM/yyyy"
                                        onChange={(date) => { formik.setFieldValue('ma_ky_luong', date) }} //handleSubmit(date, {name : 'ngay'})
                                    />
                                </Form.Field> */}
                                    {/* <ZenFieldSelect width={3}
                                    options={[
                                        { key: 0, value: "nv", text: "Giáo viên" },
                                        { key: 1, value: "kh", text: "Khách hàng" },
                                    ]}
                                    props={formik}
                                    name="type"
                                    label="hrlichday.type" defaultlabel="Xem theo"
                                    onSelectedItem={handleSubmit}
                                    clearable={false}
                                /> */}
                                    <ZenFieldSelectApi readOnly={false} style={{ whiteSpace: 'nowrap', minWidth: '160px' }}
                                        lookup={{ ...ZenLookup.ID_NV, format: `{ho_ten}` }}
                                        placeholder="Giáo viên"
                                        name="id_nv"
                                        formik={formik}
                                        onItemSelected={(item, name) => handleSelect(item, name)}
                                        isIntl={false}
                                    //onItemSelected={handleSubmit}
                                    />
                                    {/* <ZenFieldSelectApi width={3} readOnly={_type === 'kh' ? false : true}
                                    lookup={ZenLookup.Ma_kh}
                                    label="hrlichday.ma_kh" defaultlabel="Trường/khách hàng"
                                    name="makh"
                                    formik={formik}
                                    onItemSelected={handleSubmit}
                                /> */}
                                </Form.Group>
                            </Form>
                        }
                    }
                </ZenFormik>
                <div className='HRCCGV-header-btngrp'>
                    <Button
                        icon="calculator"
                        content="Tổng hợp từ chấm công "
                        primary type="submit"
                        onClick={(e) => refForm.current.handleSubmit(e)}
                    />
                </div>
            </div>
        </div>
        <Segment>
            <ZenLoading loading={loadingForm} inline="centered" dimmerPage={false} />
            <div style={{ overflowX: 'auto', overFlowY: 'auto', height: 'auto', maxHeight: '542px' }}>
                <Table celled compact selectable size="small" unstackable>
                    <Table.Header>
                        <Table.HeaderCell content="Giáo viên" textAlign="center" id="getwidthtennv-id" className="sticky-left HRCCGV-sticky-top" style={{ zIndex: '2' }} />
                        <Table.HeaderCell content="Trường" textAlign="center" id="getwidthkh-id" style={{ left: `${widthtennv}px`, zIndex: '2' }} className="HRCCGV-sticky-left HRCCGV-sticky-top" />
                        <Table.HeaderCell textAlign="center" id="getwidthtotalhour-id" style={{ left: `${widthtennv + widthkh}px`, zIndex: '2' }} className="HRCCGV-sticky-left HRCCGV-sticky-top">Tổng <br /> giờ</Table.HeaderCell>
                        <Table.HeaderCell textAlign="center" id="getwidthtotalday-id" style={{ left: `${widthtennv + widthkh + widthtotalhour}px`, zIndex: '2' }} className="HRCCGV-sticky-left HRCCGV-sticky-top">Tổng <br /> ngày</Table.HeaderCell>
                        {renderHeaderTable()}
                    </Table.Header>
                    <Table.Body>
                        {tongcong && <Table.Row>
                            <Table.Cell colSpan={2} style={{ whiteSpace: 'nowrap' }} className="sticky-left PADMLBL-sticky-top" style={{ top: `${heighttennv}px`, zIndex: "2" }} ><strong>Tổng cộng</strong></Table.Cell>
                            <Table.Cell textAlign="center" className="HRCCGV-sticky-left PADMLBL-sticky-top" style={{ top: `${heighttennv}px`, left: `${widthtennv + widthkh}px`, zIndex: '2' }} > <strong><FormatNumber value={tongcong[`tong_gio`]} /></strong> </Table.Cell>
                            <Table.Cell textAlign="center" className="HRCCGV-sticky-left PADMLBL-sticky-top" style={{ top: `${heighttennv}px`, left: `${widthtennv + widthkh + widthtotalhour}px`, zIndex: '2' }} > <strong><FormatNumber value={tongcong[`tong_ngay`]} /></strong> </Table.Cell>
                            {
                                (() => {
                                    let tablecell = [];
                                    for (let i = 1; i <= 31; i++) {
                                        if (i == 31) {
                                            if (tongcong[`ngay${i}_gio`]) {
                                                tablecell.push(
                                                    <Table.Cell textAlign="center" className="sticky-left PADMLBL-sticky-top" style={{ top: `${heighttennv}px` }} > <strong><FormatNumber value={tongcong[`ngay${i}_gio`]} /></strong> </Table.Cell>
                                                )
                                            }
                                        } else {
                                            tablecell.push(
                                                <Table.Cell textAlign="center" className="sticky-left PADMLBL-sticky-top" style={{ top: `${heighttennv}px` }} > <strong><FormatNumber value={tongcong[`ngay${i}_gio`]} /></strong> </Table.Cell>
                                            )
                                        }
                                    }
                                    return tablecell
                                })()
                            }
                        </Table.Row>}
                        {tennv && tennv?.map((ten, index) => {
                            const databynv = data?.filter(t => {
                                return t.ten_nv === ten
                            })
                            return <>
                                <Table.Row >
                                    {ten !== 'null' && ten !== '' && <Table.Cell rowSpan={databynv.length} style={{ whiteSpace: 'nowrap' }} className="sticky-left">{ten}</Table.Cell>}
                                    {databynv && databynv.length > 0 && rendercalendarfirst(databynv.length < 1 ? databynv : [databynv[0]])}
                                </Table.Row >

                                {databynv && databynv.length > 0 && rendercalendar(databynv.length > 1 ? databynv.splice(1, databynv.length - 1) : [])}
                            </>
                        })
                        }
                    </Table.Body>
                </Table>
            </div>

        </Segment>
    </>
}

const PageHeader = ({ item }) => {
    return <div style={{}}>
        <Helmet idMessage={"hrchamconggv.detail"}
            defaultMessage={"Tổng hợp chấm công giáo viên"} />
        <SegmentHeader>
            <HeaderLink >
                <Breadcrumb.Divider icon='right chevron' />
                <Breadcrumb.Section active>
                    Tổng hợp chấm công giáo viên
                </Breadcrumb.Section>
            </HeaderLink>
        </SegmentHeader>
        <br />
    </div>
}

const PopupUpdateTimeKeeping = ({ workhour, id, fname, onAfterSave, active }) => {
    const [open, setOpen] = useState(false)
    const [btnloading, setBtnloading] = useState(false)
    const refFormworkhour = useRef()

    const handleClose = () => {
        setOpen(false)
    }

    const handleSubmit = async (params) => {
        setBtnloading(true)
        ApiPAChamCongKH.update({ so_gio: params.so_gio, id: id, fname: fname }, res => {
            if (res.status >= 200 && 204 >= res.status) {
                onAfterSave && onAfterSave({ sogio: params.so_gio, id: id, fname: fname })
                ZenMessageToast.success();
                setOpen(false)
            } else {
                ZenMessageAlert.error(zzControlHelper.getResponseError(res))
            }
            setBtnloading(false)
        })
    }

    return <Popup trigger={
        <Table.Cell textAlign="center" className={`${active ? `HRCCGV-active-table-cell` : ''}`}>
            <FormatNumber value={workhour} />
        </Table.Cell>}
        style={{ padding: "0px" }} position="left center"
        on={"click"} open={open}
        onOpen={() => setOpen(true)}
        onClose={handleClose}
        closeOnEscape closeOnTriggerBlur closeOnTriggerClick hideOnScroll
        closeOnDocumentClick
    >
        <ZenFormik form={"workhour"} ref={refFormworkhour}
            validation={formValidationworkhour}
            initItem={{ ...initItemworkhour, so_gio: workhour }}
            onSubmit={handleSubmit}
        >
            {
                formik => {
                    return <Form style={{ padding: "10px 20px 10px 15px" }} widths={16}>
                        <Form.Group>
                            <ZenFieldNumber name="so_gio" width="10"
                                label="workhour.so_gio" defaultlabel="Điều chỉnh giờ công"
                                decimalScale={2}
                                props={formik}
                            />
                            <Form.Field width="3">
                                <label>&nbsp;</label>
                                <Button icon="ban"
                                    //size='small'
                                    basic
                                    onClick={handleClose}
                                />
                            </Form.Field>
                            <Form.Field width="3">
                                <label>&nbsp;</label>
                                <Button icon="save"
                                    //size='small'
                                    primary
                                    type="submit"
                                    loading={btnloading}
                                    disabled={btnloading}
                                    onClick={(e) => refFormworkhour.current.handleSubmit(e)}
                                />
                            </Form.Field>
                        </Form.Group>
                    </Form>
                }
            }
        </ZenFormik>
    </Popup>
}

const initItemworkhour = {
    so_gio: 0,
}

const formValidationworkhour = [
    {
        id: 'so_gio',
        validationType: 'number',
        validations: [
            {
                type: 'required',
                params: ["Không được bỏ trống trường này"]
            },
        ]
    },
]

const initItem = {
    ma_ky_luong: "",
    id_nv: ""
}

const formValidation = [
    {
        id: 'ma_ky_luong',
        validationType: 'string',
        validations: [
            {
                type: 'required',
                params: ["Không được bỏ trống trường này"]
            }
        ]
    },
]

export const HrChamCongGv = {
    HrChamCongGvform: {
        route: routes.HrChamCongGv,
        Zenform: HrChamCongGvForm,
        action: {
            view: { visible: true, permission: permissions.HrChamCongGVXem },
        },
    },
}