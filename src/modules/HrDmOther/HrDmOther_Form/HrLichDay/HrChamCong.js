import React, { useEffect, useRef, useState } from "react";
import { Breadcrumb, Button, Form, Segment, Table, Popup } from "semantic-ui-react";
import {
    ContainerScroll, ZenFormik, FormatNumber,
    SegmentHeader, ZenFieldSelectApi, HeaderLink, ZenLoading,
    ZenField, ZenMessageAlert, ZenMessageToast, ZenFieldCheckbox,Helmet
} from "../../../../components/Control/index";
import { zzControlHelper } from "../../../../components/Control/zzControlHelper";
import { ZenHelper } from "../../../../utils";
import { ZenLookup } from "../../../ComponentInfo/Dictionary/ZenLookup";
import * as routes from './../../../../constants/routes'
import _ from "lodash";
import './HRLICHDAY.css'
import { ApiPAChamCong } from "../../Api/ApiPAChamCong";
import './HRChamCongGv.css'
import { GlobalStorage, KeyStorage } from "../../../../utils/storage";
import { ApiPADmKyLuong } from "../../../PA/Api";
import { closest } from "dom-helpers";
import * as permissions from "../../../../constants/permissions";
import { useLocation } from "react-router-dom";

export const HrChamCongForm = (props) => {
    const { onLoadData, fieldCode } = props;
    const refForm = useRef();
    const [loadingForm, setLoadingForm] = useState(false)
    const [_kyluong, setKyluong] = useState(ZenHelper.formatDateTime(new Date(), 'YYYY/MM/DD'))
    const [firstdayofm, setfirstdayofm] = useState(getFirstDayMonth())
    const [lastdayofm, setlastdayofm] = useState(getLastDayMonth())
    const [data, setData] = useState([])
    const [tennv, setTennv] = useState([])
    const [maKyLuong, setMaKyLuong] = useState()
    const [maBoPhan, setMaBoPhan] = useState()

    const [widthtennv, setWidthtennv] = useState()
    const [widthkh, setWidthkh] = useState()
    const [widthbp, setWidthbp] = useState()

    const [kyluongOpt, setKyluongOpt] = useState([])

    // useEffect(() => {

    // }, [])
    useEffect(() => {
        const elementtennv = document.getElementById('getwidthtennv-id')
        const elementkh = document.getElementById('getwidthkh-id')
        const elementbp = document.getElementById('getwidthbp-id')
        const widthtennv = elementtennv.getBoundingClientRect().width
        const widthkh = elementkh.getBoundingClientRect().width
        const widthbp = elementbp.getBoundingClientRect().width
        setWidthtennv(widthtennv)
        setWidthkh(widthkh)
        setWidthbp(widthbp)
    })

    const location = useLocation()

    // useEffect(() => {
    //     let bp = new URLSearchParams(location.search).get("bp")
    //     if (bp) {
    //         refForm.current.setFieldValue("ma_bo_phan", bp)
    //     }
    // }, [location]);

    function getfianceyear() {
        return GlobalStorage.getByField(KeyStorage.Global, 'financial_year')
    }

    function getChamcong() {
        setLoadingForm(true)
        let bp = new URLSearchParams(location.search).get("bp")
        ApiPAChamCong.get({ maKyLuong: maKyLuong, maBoPhan: maBoPhan || bp }, res => {
            if (res.status === 200) {
                if (res.data.data && res.data.data.length > 0) {
                    setData(res.data.data)
                    //setTennv(Object.keys(res.data.data.reduce((r, { ten_nv }) => (r[ten_nv] = '', r), {})))
                }
            }
            setLoadingForm(false)
        })
    }

    function getFirstDayMonth(day) {
        let date = day ? new Date(day) : new Date();
        let month = date.getMonth(),
            year = date.getFullYear();
        return ZenHelper.getfirstDayOfMonth(month, year)
    }

    function getLastDayMonth(day) {
        let date = day ? new Date(day) : new Date();
        let month = date.getMonth(),
            year = date.getFullYear();
        return ZenHelper.getLastDayOfMonth(month, year)
    }

    function renderHeaderTable() {
        let result = [];
        let firstday = _.clone(firstdayofm)
        let dateoffirstday = firstday.getDate()
        const lastDay = lastdayofm.getDate()
        for (let i = dateoffirstday; i <= lastDay; i++) {
            result.push(<Table.HeaderCell textAlign="center" className="HRCCGV-sticky-top">{
                `${getDayofWeek(new Date(firstday.setDate(i)).getDay())}`}
                <br />
                {`${zzControlHelper.formatDateTime(firstday.setDate(i), 'DD/MM')}`}
            </Table.HeaderCell>)
        }
        return result
    }

    function getDayofWeek(day) {
        var _switch = {
            0: 'CN',
            1: 'T2',
            2: 'T3',
            3: 'T4',
            4: 'T5',
            5: 'T6',
            6: 'T7',
        };
        return _switch[day]
    }

    function rendercalendar(item) {
        return <>
            <Table.Cell style={{ whiteSpace: 'nowrap' }} className="sticky-left">{item.id_nv}</Table.Cell>
            <Table.Cell style={{ whiteSpace: 'nowrap', left: `${widthtennv}px` }} className="HRCCGV-sticky-left" content={item.ten_nv} />
            <Table.Cell style={{ whiteSpace: 'nowrap', left: `${widthtennv + widthkh}px` }} className="HRCCGV-sticky-left" content={item.ten_bo_phan} />
            <Table.Cell style={{ whiteSpace: 'nowrap', left: `${widthtennv + widthkh + widthbp}px` }} className="HRCCGV-sticky-left" textAlign="right"><FormatNumber value={item.tong_cong} /></Table.Cell>
            {
                renderTableCellgiocong(item)
            }
        </>
    }

    function renderTableCellgiocong(item) {
        let result = [];
        let firstday = _.clone(firstdayofm)
        let dateoffirstday = firstday.getDate()
        const lastDay = lastdayofm.getDate()
        for (let i = dateoffirstday; i <= lastDay; i++) {
            let dayofweek = getDayofWeek(new Date(firstday.setDate(i)).getDay())
            let active = dayofweek === 'T7' || dayofweek === 'CN'
            if (i < 10) i = "0" + i
            result.push(
                <PopupUpdateTimeKeeping
                    kyhieu={item[`ngay${i}_kh`]}
                    id={item.id}
                    fname={`ngay${i}_kh`}
                    active={active}
                    onAfterSave={handleAfterSave}
                    maKyLuong={maKyLuong}
                />)
        }
        return result
    }

    const handleAfterSave = ({ ky_hieu, id, fname }) => {
        getChamcong()
        // if (id !== 0) {
        //     const item = data.filter(t => t.id === id)[0]
        //     item[fname] = ky_hieu
        //     setData([].concat(data))
        // }
        // if (id === 0) {
        //     data.forEach((item, index) => {
        //         data[index][fname] = ky_hieu
        //     })
        //     setData([].concat(data))
        // }
    }

    let questBefore = (mess, callBack) => {
        let result = ZenMessageAlert.question(mess);
        Promise.all([result])
            .then(values => {
                if (values[0]) {
                    callBack()
                }
            }).catch(err => {

            });
    }

    const handleSelect = (item, { name }, params) => {
        if (name === 'ma_ky_luong') {
            setMaKyLuong(item.ma_ky_luong)
            setfirstdayofm(new Date(item.ngay_bd))
            setlastdayofm(new Date(item.ngay_kt))
        }
        if (name === 'ma_bo_phan') {
            setMaBoPhan(item.id_cctc)
        }
        setLoadingForm(true)
        ApiPAChamCong.get({ maKyLuong: item.ma_ky_luong || maKyLuong, maBoPhan: name === 'ma_bo_phan' ? item.id_cctc : params ? params?.bp : maBoPhan }, res => {
            if (res.status === 200) {
                setData(res.data.data)
            }
            setLoadingForm(false)
        })
    }

    const handlSyntheticTimekeeping = (params) => {
        if (params.ma_bo_phan) {
            taobangchamcong(params)
        } else {
            questBefore("Bạn đang tạo bảng chấm công cho toàn công ty, bạn chắc chắn muốn thực hiện không?", res => {
                let result = false
                data.map(item => {
                    let firstday = _.clone(firstdayofm)
                    let dateoffirstday = firstday.getDate()
                    const lastDay = lastdayofm.getDate()
                    for (let i = dateoffirstday; i <= lastDay; i++) {
                        if (i < 10) i = "0" + i
                        if (item[`ngay${i}_kh`]) {
                            result = true
                        }
                    }
                })
                if (result) {
                    questBefore("Đã có dữ chấm công, khi tổng hợp lại dữ liệu chấm công, dữ liệu hiện tại sẽ bị thay thế. Bạn chắc chắn muốn thực hiện?", res => {
                        taobangchamcong(params)
                    })
                } else {
                    taobangchamcong(params)
                }
            })
        }
    }
    function taobangchamcong(params) {
        if (!params.ma_ky_luong) {
            ZenMessageAlert.warning('Phải có mã kỳ lương để thực hiện')
            return
        }
        ApiPAChamCong.insert(params, res => {
            if (res.status >= 200 && res.status <= 204) {
                ZenMessageToast.success()
                getChamcong()
            } else {
                ZenMessageAlert.error(zzControlHelper.getResponseError(res))
            }
        })
    }

    useEffect(() => {
        ApiPADmKyLuong.get((res) => {
            if (res.status == 200) {
                let opt = res.data.data.filter(t => t.nam == getfianceyear())
                var closest = opt.reduce(function (prev, curr) {
                    return (Math.abs(new Date(curr.ngay_bd) - new Date()) < Math.abs(new Date(prev.ngay_kt) - new Date()) ? curr : prev);
                });
                refForm.current.setFieldValue('ma_ky_luong', closest.ma_ky_luong)
                let bp = new URLSearchParams(location.search).get("bp")
                setMaBoPhan(bp)
                if (bp) {
                    refForm.current.setFieldValue("ma_bo_phan", bp)
                    handleSelect(closest, { name: 'ma_ky_luong' }, { bp: bp })
                } else {
                    handleSelect(closest, { name: 'ma_ky_luong' })
                }
            }
        });
    }, []);

    return <>
        <div className='HRCCGV-header'>
            <PageHeader />
            <div className='HRCCGV-header-form'>
                <ZenFormik form={"HrChamCong-filter"}
                    ref={refForm}
                    initItem={initItem}
                    onSubmit={handlSyntheticTimekeeping}
                    validation={formValidation}
                    //onValidate={onValidate}
                    onReset={(item) => {
                        //setLoadingForm(true)
                        // setKyluong(ZenHelper.formatDateTime(item.ngay, 'YYYY/MM/DD'))
                        // setIdnv(item.idnv)
                    }}
                >
                    {
                        formik => {
                            return <Form style={{ maxWidth: 'inherit', paddingRight: "15px" }}>
                                <Form.Group widths='equal'>
                                    <ZenFieldSelectApi style={{ whiteSpace: 'nowrap', minWidth: '160px' }}
                                        name='ma_ky_luong'
                                        lookup={{
                                            ...ZenLookup.Ma_ky_luong, format: `{ten_ky_luong}`,
                                            where: `nam = '${getfianceyear()}'`,
                                            onLocalWhere: (items) => {
                                                return items.filter(t => t.nam == getfianceyear())
                                            }
                                        }}
                                        placeholder="Kỳ lương"
                                        formik={formik}
                                        onItemSelected={(item, name) => handleSelect(item, name)}
                                        isIntl={false}
                                        unClearable
                                    />
                                    <ZenFieldSelectApi readOnly={false} style={{ whiteSpace: 'nowrap', minWidth: '160px' }}
                                        lookup={{
                                            ...ZenLookup.HRDmcctc, format: `{ten_cctc}`,
                                            where: 'bac <> 1',
                                            onLocalWhere: (items) => {
                                                return items.filter(t => t.bac !== 1) || []
                                            },
                                        }}
                                        placeholder="Bộ phận"
                                        name="ma_bo_phan"
                                        formik={formik}
                                        onItemSelected={(item, name) => handleSelect(item, name)}
                                        isIntl={false}
                                    />
                                </Form.Group>
                            </Form>
                        }
                    }
                </ZenFormik>
                <div className='HRCCGV-header-btngrp'>
                    <Button
                        icon="calculator"
                        content="Tạo bảng chấm công"
                        primary type="submit"
                        onClick={(e) => refForm.current.handleSubmit(e)}
                    />
                </div>
            </div>
        </div>
        <Segment>
            <ZenLoading loading={loadingForm} inline="centered" dimmerPage={false} />
            <div style={{ overflowX: 'auto', overFlowY: 'auto', height: 'auto', maxHeight: '542px' }}>
                <Table celled compact selectable size="small" unstackable>
                    <Table.Header>
                        <Table.HeaderCell content="Mã NV" textAlign="center" id="getwidthtennv-id" className="sticky-left HRCCGV-sticky-top" style={{ zIndex: '2' }} />
                        <Table.HeaderCell content="Nhân viên" textAlign="center" id="getwidthkh-id" style={{ left: `${widthtennv}px`, zIndex: '2' }} className="HRCCGV-sticky-left HRCCGV-sticky-top" />
                        <Table.HeaderCell content="Bộ phận" textAlign="center" id="getwidthbp-id" style={{ left: `${widthtennv + widthkh}px`, zIndex: '2' }} className="HRCCGV-sticky-left HRCCGV-sticky-top" />
                        <Table.HeaderCell textAlign="center" id="getwidthtotal-id" style={{ left: `${widthtennv + widthkh + widthbp}px`, zIndex: '2' }} className="HRCCGV-sticky-left HRCCGV-sticky-top">Tổng</Table.HeaderCell>
                        {renderHeaderTable()}
                    </Table.Header>
                    <Table.Body>
                        {data && data?.map((item, index) => {
                            return <>
                                <Table.Row >
                                    {rendercalendar(item)}
                                </Table.Row >
                            </>
                        })
                        }
                    </Table.Body>
                </Table>
            </div>
        </Segment>
    </>
}

const PageHeader = ({ item }) => {
    return <div style={{}}>
        <Helmet idMessage={"HrChamCong.detail"}
            defaultMessage={"Bảng chấm công nhân viên"} />
        <SegmentHeader>
            <HeaderLink >
                <Breadcrumb.Divider icon='right chevron' />
                <Breadcrumb.Section active>
                    Bảng chấm công nhân viên
                </Breadcrumb.Section>
            </HeaderLink>
        </SegmentHeader>
        <br />
    </div>
}

const PopupUpdateTimeKeeping = ({ kyhieu, id, fname, onAfterSave, active, maKyLuong }) => {
    const [open, setOpen] = useState(false)
    const [btnloading, setBtnloading] = useState(false)
    const refFormworkhour = useRef()

    const handleClose = () => {
        setOpen(false)
    }

    const handleSubmit = async (params) => {
        setBtnloading(true)
        ApiPAChamCong.update({ ma_ky_luong: maKyLuong, ky_hieu: params.ky_hieu, id: params.allnv ? 0 : id, fname: fname, type: params.type ? "toend" : "" }, res => {
            if (res.status >= 200 && 204 >= res.status) {
                onAfterSave && onAfterSave({ ky_hieu: params.ky_hieu, id: params.allnv ? 0 : id, fname: fname })
                ZenMessageToast.success();
                setOpen(false)
            } else {
                ZenMessageAlert.error(zzControlHelper.getResponseError(res))
            }
            setBtnloading(false)
        })
    }

    return <Popup trigger={
        <Table.Cell textAlign="center" className={`${active ? `HRCCGV-active-table-cell` : ''}`}>
            {kyhieu}
        </Table.Cell>}
        style={{ padding: "0px" }} position="left center"
        on={"click"} open={open}
        onOpen={() => setOpen(true)}
        onClose={handleClose}
        closeOnEscape closeOnTriggerBlur closeOnTriggerClick hideOnScroll
        closeOnDocumentClick
    >
        <ZenFormik form={"workhour"} ref={refFormworkhour}
            validation={formValidationworkhour}
            initItem={{ ...initItemworkhour, ky_hieu: kyhieu }}
            onSubmit={handleSubmit}
        >
            {
                formik => {
                    return <Form style={{ padding: "10px 20px 10px 15px" }} widths={16}>
                        <Form.Group>
                            <div style={{ minWidth: '200px', paddingLeft: '0.5em' }}>
                                <ZenFieldSelectApi name="ky_hieu"
                                    label="workhour.ky_hieu" defaultlabel="Sửa ký hiệu chấm công"
                                    lookup={{ ...ZenLookup.PAKyHieuChamCong, format: "{ky_hieu} - {mo_ta}", }}
                                    formik={formik}
                                />
                            </div>
                            <Form.Field width="3">
                                <label>&nbsp;</label>
                                <Button icon="ban"
                                    //size='small'
                                    basic
                                    onClick={handleClose}
                                />
                            </Form.Field>
                            <Form.Field width="3">
                                <label>&nbsp;</label>
                                <Button icon="save"
                                    //size='small'
                                    primary
                                    type="submit"
                                    loading={btnloading}
                                    disabled={btnloading}
                                    onClick={(e) => refFormworkhour.current.handleSubmit(e)}
                                />
                            </Form.Field>
                        </Form.Group>
                        <Form.Group>
                            <ZenFieldCheckbox
                                name="allnv"
                                label="HRCC.allnv" defaultlabel="Sửa ký hiệu chấm công cho tất cả nhân viên"
                                props={formik}
                            />
                        </Form.Group>
                        <Form.Group>
                            <ZenFieldCheckbox
                                name="type"
                                label="HRCC.toend" defaultlabel="Sửa ký hiệu từ ngày hiện tại đến hết tháng tháng (trừ thứ 7, CN)"
                                props={formik}
                            />
                        </Form.Group>
                    </Form>
                }
            }
        </ZenFormik>
    </Popup>
}

const initItemworkhour = {
    ky_hieu: "",
    allnv: false,
}

const formValidationworkhour = [
    // {
    //     id: 'ky_hieu',
    //     validationType: 'string',
    //     validations: [
    //         {
    //             type: 'required',
    //             params: ["Không được bỏ trống trường này"]
    //         },
    //     ]
    // },
]

const initItem = {
    ma_ky_luong: "",
    ma_bo_phan: "",
}

const formValidation = [
    // {
    //     id: 'ma_ky_luong',
    //     validationType: 'string',
    //     validations: [
    //         {
    //             type: 'required',
    //             params: ["Không được bỏ trống trường này"]
    //         }
    //     ]
    // },
]

export const HrChamCong = {
    HrChamCongform: {
        route: routes.HrChamCong,
        Zenform: HrChamCongForm,
        action: {
            view: { visible: true, permission: permissions.HrChamCongXem },
        },
    },
}