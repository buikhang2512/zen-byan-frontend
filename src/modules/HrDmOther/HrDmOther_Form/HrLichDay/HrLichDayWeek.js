import React, { useCallback, useEffect, useMemo, useRef, useState } from "react";
import { Accordion, Breadcrumb, Button, Form, Grid, Icon, Segment, Tab, Table, Dropdown, Dimmer, Modal, Checkbox, Menu, Label, FormField, Divider, Message, TableCell, List, ButtonGroup } from "semantic-ui-react";
import {
    FormatDate, ContainerScroll, ZenFormik, ZenFieldDate, ZenFieldSelect, ZenField, ZenFieldCheckbox,
    SegmentHeader, ZenFieldSelectApi, InputDatePeriod, ZenDatePeriod, HeaderLink, ZenButton, ZenLoading, ZenFieldNumber, ZenMessageAlert, ZenMessageToast, Helmet
} from "../../../../components/Control/index";
import { FormMode, zzControlHelper } from "../../../../components/Control/zzControlHelper";
import { auth, ZenHelper } from "../../../../utils";
import { ZenLookup } from "../../../ComponentInfo/Dictionary/ZenLookup";
//import { Calendar, momentLocalizer, } from "react-big-calendar";
//import moment from "moment";
//import "react-big-calendar/lib/css/react-big-calendar.css";
import { ApiHrLichDay } from './../../Api/index'
import * as routes from './../../../../constants/routes'
import { Link } from "react-router-dom";
import { TableScroll } from "../../../../components/Control/zVoucher";
import _ from "lodash";
import { debounce, findIndex, values } from "lodash-es";
import { getModal } from "../../../ComponentInfo/Dictionary/ZenGetModal";
import DatePicker from "react-datepicker";
import './HRLICHDAY.css'
import * as permissions from "../../../../constants/permissions";
import { isEmpty } from "lodash-es";

export const HrLichDayWeekForm = (props) => {
    const { onLoadData, fieldCode } = props;
    const refForm = useRef();
    const [expand, setExpand] = useState(0);
    const [dataCal, setDataCal] = useState([]);
    const [ten1, setTen1] = useState([]);
    const [cap, setCap] = useState([]);
    const [day, setDay] = useState(ZenHelper.formatDateTime(new Date(), 'YYYY/MM/DD'));
    const [monday, setMonday] = useState();
    const [_type, setType] = useState('kh');
    const [infoModal, setInfoModal] = useState({})
    const [loadingForm, setLoadingForm] = useState(false)
    const [open, setOpen] = useState(false)
    const [openEdit, setOpenEdit] = useState(false)
    const [openDelete, setOpenDelete] = useState(false)
    const [openChamcong, setOpenChamCong] = useState(false)
    const [openGhichu, setOpenGhichu] = useState(false)
    const [openInfo, setOpenInfo] = useState(false)
    const [error, setError] = useState()
    const [id, setId] = useState()
    const [timeout, settimeout] = useState(0)
    const [dataTypeAction, setDataTypeAction] = useState([
        { key: 0, value: "edit", text: "Sửa lịch" },
        { key: 1, value: "copy", text: "Nhân bản" },
    ])
    const [_ngay, setNgay] = useState(ZenHelper.formatDateTime(new Date(), 'YYYY/MM/DD'))
    const [_idnv, setIdnv] = useState()
    const [_makh, setMakh] = useState()
    const [_isChangeBlur, setIsChangeBlur] = useState(true)

    function getlichtuan(_day, idnv) {
        const { values } = refForm.current
        ApiHrLichDay.getW(values.type || _type, _day || ZenHelper.formatDateTime(values.ngay, 'YYYY/MM/DD'), idnv || values.idnv, values.makh, res => {
            if (res.status === 200) {
                setDataCal(res.data.data)
                setTen1(Object.keys(res.data.data.reduce((r, { ten1 }) => (r[ten1] = '', r), {})))
            } else {
                setError(ZenHelper.getResponseError(res))
            }
            setLoadingForm(false)
        })
    }

    useEffect(() => {
        setLoadingForm(true)
        getlichtuan()
        const MD = getMonday(day)
        setMonday(MD)
        const info = getModal(ZenLookup.HrLichDay)
        setInfoModal(info)
    }, [])

    const uniqueArrayByProperty = (array, callback) => {
        return array.reduce((prev, item) => {
            const v = callback(item);
            if (!prev.includes(v)) prev.push(v)
            return prev
        }, [])
    }

    const handleOpenCloseModal = (formMode, item) => {
        setInfoModal({
            ...infoModal,
            info: {
                ...infoModal.info,
                checkPermission: (permis) => auth.checkPermission(permis),
                initItem: {
                    ...infoModal.info.initItem,
                    id: item ? item.id : "",
                    id_nv: item ? item.id_nv : "",
                    ma_kh: item ? item.ma_kh : "",
                    ngay: item ? item.ngay : "",
                },
                noChange: item ? item?.id_nv ? 'nv' : 'kh' : false,
            },
            id: item ? item.id : "",
            formMode: formMode,
            open: true,
            other: {
                id: item ? item.id : "",
                //id: 'id', // ChungZEN # 10/11/2021 => Chỗ này ko hiểu tại sao lại có 2 id?? báo lỗi nên đóng lại
                type: 'id',
            }
        })
    }

    const handleAfterSaveModal = (newItem, { mode }) => {
        const { ngay, id_nv, ma_kh, tu, den, id, ten_kh, ten_nv } = newItem
        const md = _.clone(monday)
        const _ngay = _.clone(ngay)
        const _ngay_ = zzControlHelper.formatDateTime(_ngay, 'DD/MM/YYYY')
        const { values } = refForm.current
        ApiHrLichDay.getW(values.type, zzControlHelper.formatDateTime(values.ngay, 'YYYY/MM/DD'), values.idnv, values.makh, res => {
            if (res.status === 200) {
                setDataCal(res.data.data)
                setTen1(Object.keys(res.data.data.reduce((r, { ten1 }) => (r[ten1] = '', r), {})))
            }
            setLoadingForm(false)
        })
        // if (mode === FormMode.ADD) {
        //     if (isValidTime(newItem.tu, 5, 0)) {
        //         newItem['ca'] = "1"
        //     }
        //     if (isValidTime(newItem.tu, 13, 0)) {
        //         newItem['ca'] = "2"
        //     }
        //     if (isValidTime(newItem.tu, 18, 0)) {
        //         newItem['ca'] = "3"
        //     }
        //     ApiHrLichDay.getW(_type, day, null, null, res => {
        //         if (res.status === 200) {
        //             setDataCal(res.data.data)
        //             setTen1(Object.keys(res.data.data.reduce((r, { ten1 }) => (r[ten1] = '', r), {})))
        //         }
        //         setLoadingForm(false)
        //     })
        // } else if (mode === FormMode.EDIT) {
        //     //let index = findIndex(dataCal,function(o) { return o.id == id; })
        //     const item = dataCal.filter(t => t.id == id)
        //     if (isValidTime(newItem.tu, 5, 0)) {
        //         newItem['ca'] = "1"
        //     }
        //     if (isValidTime(newItem.tu, 13, 0)) {
        //         newItem['ca'] = "2"
        //     }
        //     if (isValidTime(newItem.tu, 18, 0)) {
        //         newItem['ca'] = "3"
        //     }
        //     item.ca = newItem.ca
        //     item.ma1 = _type === "nv" ? newItem.id_nv : newItem.ma_kh,
        //     item.ten1 = _type === "nv" ? newItem.ten_nv : newItem.ten_kh,
        //     item.ma2 = _type === "nv" ? newItem.ma_kh : newItem.id_nv,
        //     item.ten2 = _type === "nv" ? newItem.ten_kh : newItem.ten_nv,
        //     item.den = newItem.den,
        //     item.tu = newItem.tu,
        //     item.time = `(${zzControlHelper.formatDateTime(newItem.tu, 'HH:mm')} - ${zzControlHelper.formatDateTime(newItem.den, 'HH:mm')})`
        // }
        // close modal
        setInfoModal({
            ...infoModal,
            id: "",
            formMode: FormMode.NON,
            open: false,
        })
    }

    function checkDate(strDate) {
        var comp = strDate.split('/')
        var d = parseInt(comp[0], 10)
        var m = parseInt(comp[1], 10)
        var y = parseInt(comp[2], 10)
        var date = new Date(y, m - 1, d);
        if (date.getFullYear() == y && date.getMonth() + 1 == m && date.getDate() == d) {
            return true
        }
        return false
    }

    const handleSubmit = (item, { name }, e) => {
        const { values } = refForm.current
        let ngay, idnv, makh;
        if (name === 'ngay') {
            setNgay(item)
            ngay = item
            const MD = getMonday(item)
            setMonday(MD)
        }
        if (name === 'type') {
            if (item.value === 'nv') {
                refForm.current.setValues({
                    ...refForm.current.values,
                    type: item.value,
                    makh: null,
                })
            }
            if (item.value === 'kh') {
                refForm.current.setValues({
                    ...refForm.current.values,
                    type: item.value,
                    idnv: null,
                })
            }
            setType(item.value)

        }
        if (name === 'idnv') {
            idnv = item?.value ? item.value : item.id_nv
            setIdnv(item?.value ? item.value : item.id_nv)
            makh = null
            setMakh()
        }
        if (name === 'makh') {
            makh = item?.value ? item.value : item.ma_kh
            setMakh(item?.value ? item.value : item.ma_kh)
            idnv = null
            setIdnv()
        }
        if (ngay !== _ngay || name === 'type' || name === 'idnv' || name === 'makh') {
            setLoadingForm(true)
            ApiHrLichDay.getW(
                (name === 'type') ? item.value : values.type,
                ZenHelper.formatDateTime(ngay, 'YYYY/MM/DD') || ZenHelper.formatDateTime(values.ngay, 'YYYY/MM/DD'),
                idnv || values.id_nv,
                makh || values.ma_kh, res => {
                    if (res.status === 200) {
                        setDataCal(res.data.data)
                        setTen1(Object.keys(res.data.data.reduce((r, { ten1 }) => (r[ten1] = '', r), {})))
                    }
                    setLoadingForm(false)
                })
        }
    }

    const debounceDateView = useCallback(debounce((item, name) => handleSubmit(item, { name: name }), 1000), [])

    const handleChange = (item, { name }) => {
        debounceDateView(item, name)
    }

    function getMonday(d) {
        d = new Date(d);
        var day = d.getDay(),
            diff = d.getDate() - day + (day == 0 ? -6 : 1);
        return new Date(d.setDate(diff));
    }

    function getUrlLichdaylist() {
        return {
            pathname: `${routes.HrLichDay}`,
        }
    }
    function getUrlLichdayWeek() {
        return {
            pathname: `${routes.HrLichDayWeek}`,
        }
    }
    function getUrlLichdayMonth() {
        return {
            pathname: `${routes.HrLichDayMonth}`,
        }
    }

    function getdayweek(monday, day) {
        const md = _.clone(monday)
        return ZenHelper.formatDateTime(md.setDate(new Date(md).getDate() + day), 'DD/MM/YYYY')
    }

    function isValidDate(d) {
        return new Date(d).getFullYear()
    }
    function isValidTime(d, hh1, mm1) {
        const hh = new Date(d).getHours()
        const mm = new Date(d).getMinutes()
        if (hh >= hh1) {
            if (mm >= mm1) {
                return d
            }
        }
    }


    function getTimestart(md, hh, mm, i) {
        const tu = new Date(md.setDate(new Date(md).getDate() + i - 1))
        tu.setHours(hh)
        tu.setMinutes(mm)
        return zzControlHelper.formatDateTime(tu, 'YYYY/MM/DD h:mm a')
    }

    function getTimeend(md, hh, mm, i) {
        const den = new Date(md.setDate(new Date(md).getDate() + i - 1))
        den.setHours(hh)
        den.setMinutes(mm)
        return den
    }

    const openModalDup = (item) => {
        if (item) {
            setId(item.id)
        }
        setOpen(true)
    }

    const openModalEdit = (item) => {
        if (item) {
            setId(item.id)
        }
        setOpenEdit(true)
    }

    const openModalGhichu = (item) => {
        if (item) {
            setId(item.id)
        }
        setOpenGhichu(true)
    }

    const openModalDelete = (item) => {
        if (item) {
            setId(item.id)
        }
        setOpenDelete(true)
    }

    const openModalChamcong = (item) => {
        if (item) {
            setId(item.id)
        }
        setOpenChamCong(true)
    }

    const openModalInfo = (item) => {
        if (item) {
            setId(item.id)
        }
        setOpenInfo(true)
    }

    let questBefore = (mess, callBack) => {
        let result = ZenMessageAlert.question(mess);
        Promise.all([result])
            .then(values => {
                if (values[0]) {
                    callBack()
                }
            }).catch(err => {

            });
    }

    const handleDeleteItem = (id) => {
        //_.remove(dataCal, (x,i) => {return x.id == id})
        questBefore(`Bạn có chắc muốn xóa lịch dạy này ?`, () => {
            ApiHrLichDay.delete(id, res => {
                if (res.status >= 200 && res.status <= 204) {
                    ZenMessageToast.success()
                    setDataCal(dataCal.filter(t => t.id !== id))
                } else {
                    ZenMessageAlert.error(ZenHelper.getResponseError(res))
                }
            })
        })
    }

    function rendercanledar(item, ten, ca, lengthcal, unicap, capidx) {
        let a = [];
        let ma;
        if (Array.isArray(dataCal) && dataCal.length > 0) {
            ma = dataCal?.filter(t => t.ten1 == ten)[0]
        }

        function renderLine(i) {
            if (unicap && unicap.length == 1) {
                if (ca == 3 && i == 7) {
                    return `HRLD-tablecellcal`
                }
            }
            if (unicap && unicap.length > 1) {
                if ((capidx == (unicap.length - 1)) && (ca == 3 && i == 7)) {
                    return `HRLD-tablecellcal`
                }
            }
        }
        const md = _.clone(monday)
        for (let i = 1; i <= 7; i++) {
            let day = getdayweek(md, i - 1)
            a.push(<>
                <Table.Cell textAlign="center" className={`HRLD_tbcell ${renderLine(i)}`}>
                    {
                        item.map((it, idx) => {
                            return isValidDate(it[`ngay`]) > 1970 && ZenHelper.formatDateTime(it[`ngay`], 'DD/MM/YYYY') === day && <>
                                <div className="HRLD_itemtbcell">
                                    <div>
                                        {idx == (item.length - 1) && ca == 3 && i == 7 && 'aaaab'}
                                        <a href="#" onClick={(e) => { e.preventDefault(); openModalInfo({ id: it[`id`], }) }}><strong>{it[`ten2`]} </strong></a><br />
                                        {`(${zzControlHelper.formatDateTime(it[`tu`], 'HH:mm')} - ${zzControlHelper.formatDateTime(it[`den`], 'HH:mm')})`}
                                        {it[`cham_cong`] !== null && it[`cham_cong`] !== 0 && <>
                                            / <strong style={{ color: "#db2828" }}>CC</strong>
                                            <strong>{` : ${it[`cham_cong`]} ${it[`loai`]}`}</strong>
                                        </>}
                                        <br />
                                        <div style={{ maxWidth: "250px" }}><em>{it['ghi_chu']}</em></div>
                                    </div>
                                    {it[`id`] && <div className="HRLD_btngroup_action">
                                        {/* <Button.Group size="small"> */}
                                        <Dropdown
                                            item
                                            icon='ellipsis horizontal'
                                            text=' '
                                            button
                                            className='icon HRLD_btn_action small'

                                        //trigger={<><Icon /></>} //<Icon name="ellipsis horizontal"/>
                                        >
                                            <Dropdown.Menu className="left">
                                                {auth.checkPermission(permissions.HrLichDaySua) && <Dropdown.Item
                                                    onClick={() => {
                                                        handleOpenCloseModal(FormMode.EDIT, {
                                                            id: it[`id`],
                                                            id_nv: _type == 'nv' ? it.ma1 : "",
                                                            ma_kh: _type == 'kh' ? it.ma1 : "",
                                                            ngay: ZenHelper.formatDateTime(md.setDate(new Date(md).getDate() + i - 1), 'YYYY/MM/DD'),
                                                        })
                                                    }}
                                                >
                                                    Sửa
                                                </Dropdown.Item>}
                                                {auth.checkPermission(permissions.HrLichDayXoa) && <Dropdown.Item
                                                    onClick={() => {
                                                        handleDeleteItem(it[`id`])
                                                    }}
                                                >
                                                    Xóa
                                                </Dropdown.Item>}
                                                {auth.checkPermission(permissions.HrLichDayThem) && <Dropdown.Item
                                                    onClick={() => { openModalDup({ id: it[`id`], }) }}
                                                >
                                                    Nhân bản
                                                </Dropdown.Item>}
                                                {auth.checkPermission(permissions.HrLichDaySua) && <Dropdown.Item
                                                    onClick={() => { openModalEdit({ id: it[`id`], }) }}
                                                >
                                                    Sửa hàng loạt
                                                </Dropdown.Item>}
                                                {auth.checkPermission(permissions.HrLichDayXoa) && <Dropdown.Item
                                                    onClick={() => { openModalDelete({ id: it[`id`], }) }}
                                                >
                                                    Xóa hàng loạt
                                                </Dropdown.Item>}
                                                {auth.checkPermission(permissions.HrLichDayChamCong) && <Dropdown.Item
                                                    onClick={() => { openModalChamcong({ id: it[`id`], }) }}
                                                >
                                                    Chấm công
                                                </Dropdown.Item>}
                                                {auth.checkPermission(permissions.HrLichDaySua) && <Dropdown.Item
                                                    onClick={() => { openModalGhichu({ id: it[`id`], }) }}
                                                >
                                                    Ghi chú
                                                </Dropdown.Item>}
                                            </Dropdown.Menu>
                                        </Dropdown>

                                        {/* </Button.Group> */}
                                    </div>
                                    }
                                </div>
                            </>
                        })
                    }

                    <span className="HRLD_btngroup">
                        {auth.checkPermission(permissions.HrLichDayThem) && <ZenButton icon={"plus"}
                            permission={permissions.HrLichDayThem}
                            size="small"
                            onClick={() => handleOpenCloseModal(FormMode.ADD, {
                                id_nv: _type == 'nv' ? ma.ma1 : "",
                                ma_kh: _type == 'kh' ? ma.ma1 : "",
                                ngay: ZenHelper.formatDateTime(md.setDate(new Date(md).getDate() + i - 1), 'YYYY/MM/DD'),
                            }
                            )}
                        />}
                    </span>
                </Table.Cell>
            </>)
        }
        return <>
            <Table.Cell>{ca == "1" ? 'Sáng' : ca == "2" ? 'Chiều' : 'Tối'}</Table.Cell>
            {a}
        </>
    }

    function checkmaten1(data, ca, ten) {
        var result = []
        result = data.filter(t => { if (t.ca == ca && t.ten1 == ten) return t })
        return result
    }

    const handleAfterSave = (a) => {
        const { values } = refForm.current
        ApiHrLichDay.getW(values.type, ZenHelper.formatDateTime(values.ngay, 'YYYY/MM/DD'), values.idnv, values.makh, res => {
            if (res.status === 200) {
                setDataCal(res.data.data)
                setTen1(Object.keys(res.data.data.reduce((r, { ten1 }) => (r[ten1] = '', r), {})))
            }
            setLoadingForm(false)
        })
    }

    const handleprevweek = (e) => {
        setIsChangeBlur(false)
        let prevday = e.setDate(new Date(e).getDate() - 1)
        let prevdayformat = zzControlHelper.formatDateTime(prevday, 'YYYY/MM/DD')
        setDay(prevdayformat)
        setNgay(prevdayformat)
        setLoadingForm(true)
        getlichtuan(prevdayformat)
        let md = getMonday(prevday)
        setMonday(md)
        refForm.current.values['ngay'] = zzControlHelper.formatDateTime(md, 'YYYY/MM/DD')
        setIsChangeBlur(true)
    }

    const handlenextweek = (e) => {
        setIsChangeBlur(false)
        let weekenday = new Date(e.setDate(new Date(e).getDate() + 6))
        let nextday = weekenday.setDate(new Date(weekenday).getDate() + 1)
        let nextdayformat = zzControlHelper.formatDateTime(nextday, 'YYYY/MM/DD')
        setDay(nextdayformat)
        setNgay(nextdayformat)
        setLoadingForm(true)
        getlichtuan(nextdayformat)
        let md = getMonday(nextday)
        setMonday(md)
        refForm.current.values['ngay'] = zzControlHelper.formatDateTime(md, 'YYYY/MM/DD')
        setIsChangeBlur(true)
    }

    function checkPermissionreverse(permisson) {
        var user = auth.getUserInfo()
        if (user.admin === "True") {
            return false;
        } else {
            return !permisson ? false : user.permisson.includes(permisson);
        }
    }

    const [btnLoading, setBtnLoading] = useState();

    const btnType = [
        //{ key: 'excel', icon: 'file excel outline', text: 'Tải bảng lương (excel)', value: 'xls' },
        { key: 'pdf', icon: 'file pdf outline', text: 'Xuất PDF', value: 'pdf' },
        //{ key: 'zip', icon: 'download', text: 'Tải phiếu lương (zip)', value: 'zip' },
        //{ key: 'fastprint', icon: 'print', text: 'In nhanh bảng lương', value: 'fastprint' },
    ]
    const handleDowload = (i) => {
        const { value } = i
        setBtnLoading(true)
        if (value === 'xls') {
        } else if (value === 'pdf') {
            ApiHrLichDay.dowloadpdf({ day: zzControlHelper.formatDateTime(day || new Date(), 'YYYY/MM/DD'), idnv: _idnv, makh: _makh }, res => {
                if (res.status === 200) {
                    zzControlHelper.openPdfPrintFile(res.data)
                } else {
                    ZenMessageAlert.error(zzControlHelper.getResponseError(res))
                }
                setBtnLoading(false)
            })
        } else if (value === 'zip') {
        } else if (value === 'fastprint') {
            // handlePrint()
            // setBtnLoading(false)
        }
    }

    return <>
        <Helmet idMessage={'HRLichDAYWeek'}
            defaultMessage={'Lịch dạy'} />
        <div style={{ display: "flex", justifyContent: "space-between", marginBottom: "10px" }}>
            <PageHeader />
            <div style={{ alignSelf: "center" }}>
                <ZenButton
                    btnType="refresh"
                    size="small"
                    onClick={() => {
                        setLoadingForm(true)
                        getlichtuan()
                    }}
                />
                <ButtonGroup size="small" style={{ paddingRight: '3px' }}>
                    <ZenButton
                        permission={permissions.HrLichDayThem}
                        btnType="add"
                        size="small"
                        onClick={() => handleOpenCloseModal(FormMode.ADD)}
                    />
                    <Dropdown
                        loading={btnLoading}
                        style={{ borderLeft: "1px solid rgb(65, 108, 218)" }}
                        className='button icon primary'
                        trigger={<></>}
                    >
                        <Dropdown.Menu className="left">

                            {btnType && btnType.map((item, index) => {
                                return <>
                                    <Dropdown.Item value={item.value} onClick={(e, i) => handleDowload(i)}>
                                        <Icon name={item.icon} />
                                        {item.text}
                                    </Dropdown.Item>
                                </>
                            })}
                        </Dropdown.Menu>
                    </Dropdown>
                </ButtonGroup>
                <Button.Group>
                    <Link to={getUrlLichdaylist()} className=" ui button basic small primary icon">
                        <Icon name="list" />
                    </Link>
                    <Link to={getUrlLichdayWeek()} className=" ui button small primary icon">
                        <Icon name="calendar outline" />
                    </Link>
                    <Link to={getUrlLichdayMonth()} className=" ui button basic small primary icon">
                        <Icon name="calendar alternate outline" />
                    </Link>
                </Button.Group>
            </div>
        </div>
        <Segment id="hrlichdayweek">
            <ZenFormik form={"HrLichDay-filter"}
                ref={refForm}
                initItem={initItem}
                onSubmit={handleSubmit}
                validation={[]}
            >
                {
                    formik => {
                        const { values } = formik
                        return <Form size="small">
                            <Form.Group widths={16}>
                                <ZenFieldDate width={3}
                                    label="hrlichday.ngay" defaultlabel="Xem ngày"
                                    name="ngay"
                                    props={formik}
                                    onChange={(e) => handleChange(e.value, { name: e.name }, e)}
                                    isChangeBlur={_isChangeBlur}
                                />
                                {checkPermissionreverse(permissions.HrLichDayXemAll) ? undefined : <>
                                    <ZenFieldSelect width={3}
                                        options={[
                                            { key: 0, value: "nv", text: "Giáo viên" },
                                            { key: 1, value: "kh", text: "Khách hàng" },
                                        ]}
                                        props={formik}
                                        name="type"
                                        label="hrlichday.type" defaultlabel="Xem theo"
                                        onSelectedItem={handleSubmit}
                                        clearable={false}
                                    />
                                    <ZenFieldSelectApi width={3} readOnly={_type === 'nv' ? false : true}
                                        lookup={ZenLookup.ID_NV}
                                        label="hrlichday.idnv" defaultlabel="Giáo viên"
                                        name="idnv"
                                        formik={formik}
                                        onItemSelected={handleSubmit}
                                    />
                                    <ZenFieldSelectApi width={3} readOnly={_type === 'kh' ? false : true}
                                        lookup={ZenLookup.Ma_kh}
                                        label="hrlichday.ma_kh" defaultlabel="Trường/khách hàng"
                                        name="makh"
                                        formik={formik}
                                        onItemSelected={handleSubmit}
                                    />
                                </>}
                                <Form.Field>
                                    <label>&nbsp;</label>
                                    <Button primary
                                        content="Reset"
                                        size="small" icon="refresh"
                                        onClick={(e) => { setLoadingForm(true); getlichtuan() }}

                                    />
                                </Form.Field>
                            </Form.Group>
                        </Form>
                    }
                }
            </ZenFormik>
            {error && <Message negative list={error} />}
            <ZenLoading loading={loadingForm} inline="centered" dimmerPage={false} />
            <div style={{ position: "relative" }}>
                <Table celled compact selectable size="small" unstackable>
                    <Table.Header className="HRCCGV-tableheader-active">
                        <Table.HeaderCell className="HRCCGV-sticky-top" content={_type === 'nv' ? `Giáo viên` : 'Trường/khách hàng'} />
                        <Table.HeaderCell className="HRCCGV-sticky-top" content="Cấp" />
                        <Table.HeaderCell className="HRCCGV-sticky-top" content="Ca" />
                        <Table.HeaderCell textAlign="center" className="HRCCGV-sticky-top" style={{ position: "relative" }}>
                            <div className="HRLD-prev-week">
                                <Icon name="caret left" size="big" onClick={e => handleprevweek(monday)} />
                            </div>
                            Thứ 2 <br /> {monday && ZenHelper.formatDateTime(monday, 'DD/MM/YYYY')}
                        </Table.HeaderCell>
                        <Table.HeaderCell textAlign="center" className="HRCCGV-sticky-top"> Thứ 3 <br /> {monday && getdayweek(monday, 1)} </Table.HeaderCell>
                        <Table.HeaderCell textAlign="center" className="HRCCGV-sticky-top"> Thứ 4 <br /> {monday && getdayweek(monday, 2)} </Table.HeaderCell>
                        <Table.HeaderCell textAlign="center" className="HRCCGV-sticky-top"> Thứ 5 <br /> {monday && getdayweek(monday, 3)} </Table.HeaderCell>
                        <Table.HeaderCell textAlign="center" className="HRCCGV-sticky-top"> Thứ 6 <br /> {monday && getdayweek(monday, 4)} </Table.HeaderCell>
                        <Table.HeaderCell textAlign="center" className="HRCCGV-sticky-top"> Thứ 7 <br /> {monday && getdayweek(monday, 5)} </Table.HeaderCell>
                        <Table.HeaderCell textAlign="center" className="HRCCGV-sticky-top" style={{ position: "relative" }}>
                            Chủ nhật <br /> {monday && getdayweek(monday, 6)}
                            <div className="HRLD-next-week">
                                <Icon name="caret right" size="big" onClick={e => handlenextweek(monday)} />
                            </div>
                        </Table.HeaderCell>
                    </Table.Header>
                    <Table.Body>
                        {ten1 && ten1.map((t, index) => {
                            const dataoften1 = dataCal.filter(x => x.ten1 == t)
                            const uniquecap = dataoften1.map(v => v['cap'])
                                .map((v, i, array) => array.indexOf(v) === i && i)
                                .filter(v => dataCal.filter(x => x.ten1 == t)[v])
                                .map(v => dataCal.filter(x => x.ten1 == t)[v])

                            return <>
                                <Table.Row>
                                    <Table.Cell style={{ borderRight: "1px solid rgba(34, 36, 38, 0.1)" }} className="HRLD-tablecellten" rowSpan={uniquecap.length * 4 + 1}><strong>{t}</strong></Table.Cell>
                                </Table.Row>

                                {
                                    uniquecap && uniquecap.map((cap, idx) => {
                                        return <>
                                            <Table.Row><Table.Cell style={{ borderRight: "1px solid rgba(34, 36, 38, 0.1)" }} rowSpan="4">{cap.cap}</Table.Cell></Table.Row>
                                            <Table.Row>
                                                {
                                                    rendercanledar(checkmaten1(dataoften1.filter(x => x.cap === cap.cap), 1, t), t, 1, null, null, null)
                                                }
                                            </Table.Row>
                                            <Table.Row>
                                                {
                                                    rendercanledar(checkmaten1(dataoften1.filter(x => x.cap === cap.cap), 2, t), t, 2, null, null, null)
                                                }
                                            </Table.Row>
                                            <Table.Row>
                                                {
                                                    rendercanledar(checkmaten1(dataoften1.filter(x => x.cap === cap.cap), 3, t), t, 3, checkmaten1(dataoften1.filter(x => x.cap === cap.cap), 3, t).length, uniquecap, idx)
                                                }
                                            </Table.Row>
                                        </>
                                    })
                                }
                            </>
                        })
                        }
                    </Table.Body>
                </Table>
            </div>
        </Segment>

        {zzControlHelper.openModalComponent(infoModal,
            {
                onClose: () => handleOpenCloseModal(FormMode.NON),
                onAfterSave: handleAfterSaveModal,
            }
        )}

        {open && <ModalDuplicateCanledar open={open}
            id={id}
            onClose={() => setOpen(false)}
        />}
        {openEdit && <ModalEditCanledar open={openEdit}
            id={id}
            onClose={() => setOpenEdit(false)}
            onAfterSave={handleAfterSave}
        />}
        {openDelete && <ModalDeleteCanledar open={openDelete}
            id={id}
            onClose={() => setOpenDelete(false)}
            onAfterSave={handleAfterSave}
        />}
        {openChamcong && <ModalChamcongCanledar open={openChamcong}
            id={id}
            onClose={() => setOpenChamCong(false)}
            onAfterSave={handleAfterSave}
        />}
        {openInfo && <ModalInfo open={openInfo}
            id={id}
            onClose={() => setOpenInfo(false)}
        />}
        {openGhichu && <ModalGhiChuCanledar open={openGhichu}
            id={id}
            onClose={() => setOpenGhichu(false)}
            onAfterSave={handleAfterSave}
        />}
    </>
}

const initItem = {
    ngay: new Date(),
    type: "kh",
    idnv: "",
    makh: "",
}

const PageHeader = ({ item }) => {
    return <div style={{}}>
        <SegmentHeader>
            <Helmet idMessage={"HRLichDayWeek"}
                defaultMessage={"Lịch dạy"} />
            <HeaderLink >
                <Breadcrumb.Divider icon='right chevron' />
                <Breadcrumb.Section active>
                    Lịch dạy
                </Breadcrumb.Section>
            </HeaderLink>
        </SegmentHeader>
        <br />
    </div>
}

const ModalDuplicateCanledar = ({ open, id, onClose, onAfterSave, }) => {
    const _isMounted = useRef(true);
    const refFormik = useRef();
    const [data, setData] = useState()
    const [fileInfo, setFileInfo] = useState();
    const [itemInfo, setItemInfo] = useState();
    const [error, setError] = useState();
    const [roles, setRoles] = useState([]);
    const [btnLoading, setBtnLoading] = useState();

    useEffect(() => {
        zzControlHelper.dragElement(document.getElementById('DupCanledar-form'), 'header-DupCanledar-form')
    })

    useEffect(() => {
        ApiHrLichDay.getByCode(id, res => {
            if (res.status === 200) {
                setData(res.data.data)
            }
        })
    }, [])

    const handleSubmit = (params, formId) => {
        const { ngay1, ngay2, ngay3, ngay4, ngay5, ngay6 } = params

        if (!params.so_tuan) {
            if (!ngay1 && !ngay2 && !ngay3 && !ngay4 && !ngay5 & !ngay6) {
                ZenMessageAlert.warning("Vui lòng chọn ngày cấn copy tới")
                return
            } else {
                params.days = []
                for (let i = 1; i <= 6; i++) {
                    if (params[`ngay${i}`]) {
                        params.days.push(params[`ngay${i}`])
                        delete params[`ngay${i}`]
                    }
                }
            }
        } else {
            for (let i = 1; i <= 6; i++) {
                if (params[`ngay${i}`]) {
                    delete params[`ngay${i}`]
                }
            }
            params.days = ""
        }
        for (let i = 1; i <= 6; i++) {
            if (params[`ngay${i}`]) {
                delete params[`ngay${i}`]
            }
        }

        ApiHrLichDay.duplication({ ...params, id: id, days: !isEmpty(params.days) ? _.join(params.days, ',') : "" }, res => {
            if (res.status >= 200 && 204 >= res.status) {
                onAfterSave && onAfterSave()
                ZenMessageToast.success();
                onClose()
            } else {
                setError(zzControlHelper.getResponseError(res))
                ZenMessageAlert.error(zzControlHelper.getResponseError(res))
            }
        })
    }

    const customHeader = (id) => {
        const header = 'hợp đồng lao động'
        const txtHeader = !id ? `Thêm mới ${header}` : capitalize(header) + ` : ${id}`
        return txtHeader
    }
    const capitalize = (s) => {
        if (typeof s !== 'string') return ''
        return s.charAt(0).toUpperCase() + s.slice(1)
    }

    const onValidate = (item) => {
        const errors = {};
        const { so_tuan, nv, type, ngay1, ngay2, ngay3, ngay4, ngay5, ngay6 } = item

        if (type == 'w' || type == 'd') {
            if (!so_tuan) {
                errors.so_tuan = 'Không được bỏ trống trường này !'
            }
            if (so_tuan < 1) {
                errors.so_tuan = 'Không được nhỏ hơn 0 !'
            }
        }

        return errors
    }

    return <>
        <Modal id="DupCanledar-form"
            closeOnEscape closeIcon closeOnDimmerClick={false}
            onClose={() => onClose()}
            //onOpen={() => onClose()}
            open={open}
            size={"tiny"}
        >
            <Modal.Header id='header-DupCanledar-form' style={{ cursor: "grabbing" }}>
                Nhân bản lịch
            </Modal.Header>

            <Modal.Content>
                <ZenFormik form={"DupCanledar"} ref={refFormik}
                    validation={formValidationDupCanlendar}
                    initItem={initItemDupCanledar}
                    onSubmit={handleSubmit}
                    onValidate={onValidate}
                >
                    {
                        formik => {
                            return <Form>
                                <div>
                                    <div>
                                        <label>Giáo viên : </label>
                                        <strong>{data?.ten_nv}</strong>
                                    </div>
                                    <div>
                                        <label>Khách hàng : </label>
                                        <strong>{data?.ten_kh}</strong>
                                    </div>
                                    <div>
                                        <label>Ngày : </label>
                                        <strong> {zzControlHelper.formatDateTime(data?.ngay, 'DD/MM/YYYY')} </strong>
                                        Từ <strong> {zzControlHelper.formatDateTime(data?.tu, 'HH:mm')} </strong>
                                        đến <strong> {zzControlHelper.formatDateTime(data?.den, 'HH:mm')} </strong>
                                    </div>
                                </div>
                                <br />
                                <Divider />
                                <Form.Group>
                                    <ZenFieldSelect width="16" name="type"
                                        options={[
                                            { key: "1d", value: "d", text: "Nhân bản ngày hiện tại" },
                                            { key: "2w", value: "w", text: "Nhân bản tất cả các ngày trong tuần" },
                                            { key: "3s", value: "s", text: "Copy ngày hiện tại cho 1 số ngày cố định" }
                                        ]}
                                        clearable={false}
                                        props={formik}
                                        label="DupCanledar.type" defaultlabel="Phương thức thực hiện"
                                    />
                                </Form.Group>
                                {
                                    (formik.values.type == 'd' || formik.values.type == 'w') && <>
                                        <Checkbox
                                            label="Chỉ nhân bản giáo viên hiện tại"
                                            onClick={(e, i) => { formik.setFieldValue('nv', i.checked ? 1 : 0) }}
                                        /> <br /> <br />
                                        <Form.Group>
                                            <ZenFieldNumber width={8} name="so_tuan" props={formik}
                                                required
                                                label="DupCanledar.so_tuan" defaultlabel="Số tuần nhân bản"
                                            />
                                        </Form.Group>
                                    </>
                                }

                                {
                                    formik.values.type == 's' && <>
                                        <label>Các ngày sẽ copy tới</label>
                                        <Form.Group widths="equal">
                                            <ZenFieldDate
                                                name="ngay1" props={formik}
                                            />
                                            <ZenFieldDate
                                                name="ngay2" props={formik}
                                            />
                                            <ZenFieldDate
                                                name="ngay3" props={formik}
                                            />
                                        </Form.Group>
                                        <Form.Group widths="equal">
                                            <ZenFieldDate
                                                name="ngay4" props={formik}
                                            />
                                            <ZenFieldDate
                                                name="ngay5" props={formik}
                                            />
                                            <ZenFieldDate
                                                name="ngay6" props={formik}
                                            />
                                        </Form.Group>
                                    </>
                                }
                                <ZenFieldCheckbox
                                    props={formik}
                                    name="incl_ghi_chu"
                                    onChange={(e, i) => formik.setFieldValue("incl_ghi_chu", i.checked ? 1 : 0)}
                                    label="incl_ghi_chu"
                                    defaultlabel="Nhân bản kèm theo ghi chú"
                                />
                                {data?.max_date && <label style={{ paddingTop: "10px" }}><i>Đã xếp lịch dạy đến ngày <span style={{ color: "red" }}>{data?.max_date}</span></i></label>}
                            </Form>
                        }
                    }
                </ZenFormik>
            </Modal.Content>
            <Modal.Actions>
                <ZenButton btnType={"cancel"} size="small" onClick={() => onClose()} />
                <ZenButton btnType="save" size="small"
                    loading={btnLoading} type="submit"
                    onClick={(e) => refFormik.current.handleSubmit(e)}
                />
            </Modal.Actions>
        </Modal>
    </>
}

const initItemDupCanledar = {
    type: "d",
    so_tuan: 0,
    id: "",
    nv: 0,
    days: [],
    incl_ghi_chu: 0,
    ngay1: "",
    ngay2: "",
    ngay3: "",
    ngay4: "",
    ngay5: "",
    ngay6: "",
}

const formValidationDupCanlendar = [
    {
        // id: "so_tuan",
        // validationType: "number",
        // validations: [
        //     {
        //         type: "required",
        //         params: ["Không được bỏ trống trường này"]
        //     },
        //     {
        //         type: "min",
        //         params: [1, "Không được nhỏ hơn 0"]
        //     },
        // ]
    },
]

const ModalGhiChuCanledar = ({ open, id, onClose, onAfterSave, }) => {
    const _isMounted = useRef(true);
    const refFormik = useRef()
    const [btnLoading, setBtnLoading] = useState();

    useEffect(() => {
        zzControlHelper.dragElement(document.getElementById('GhiChu-form'), 'header-GhiChu-form')
    })

    useEffect(() => {
        ApiHrLichDay.getByCode(id, res => {
            if (res.status >= 200 && res.status <= 204) {
                refFormik.current.setValues({ ...res.data.data })
            } else {
                ZenMessageAlert.error(zzControlHelper.getResponseError(res))
            }
        })
    }, [])

    const handleSubmit = (params, formId) => {
        const patchData = [
            {
                value: params.ghi_chu,
                path: `/ghi_chu`,
                op: "replace",
                operationType: 0,
            }
        ]

        ApiHrLichDay.updatePatch(id, patchData, res => {
            if (res.status >= 200 && 204 >= res.status) {
                onAfterSave && onAfterSave({ ...refFormik.current.values })
                ZenMessageToast.success();
                onClose()
            } else {
                ZenMessageAlert.error(zzControlHelper.getResponseError(res))
            }
        })
    }

    return <>
        <Modal id="Ghichu-form"
            closeOnEscape closeIcon closeOnDimmerClick={false}
            onClose={() => onClose()}
            open={open}
            size={"tiny"}
        >
            <Modal.Header id='header-Ghichu-form' style={{ cursor: "grabbing" }}>
                Ghi chú
            </Modal.Header>

            <Modal.Content>
                <ZenFormik form={"DupCanledar"} ref={refFormik}
                    validation={[]}
                    initItem={{ ghi_chu: "" }}
                    onSubmit={handleSubmit}
                >
                    {
                        formik => {
                            return <Form>
                                <Form.Group>
                                    <ZenField width="16" name="ghi_chu"
                                        props={formik}
                                        label="Edit.ghi_chu" defaultlabel="Ghi chú"
                                    />
                                </Form.Group>
                            </Form>
                        }
                    }
                </ZenFormik>
            </Modal.Content>
            <Modal.Actions>
                <ZenButton btnType={"cancel"} size="small" onClick={() => onClose()} />
                <Button size="small"
                    icon="checkmark"
                    content="Lưu"
                    size="small"
                    primary
                    loading={btnLoading} type="submit"
                    onClick={(e) => refFormik.current.handleSubmit(e)}
                />
            </Modal.Actions>
        </Modal>
    </>
}

const ModalEditCanledar = ({ open, id, onClose, onAfterSave, }) => {
    const _isMounted = useRef(true);
    const refFormik = useRef()
    const [fileInfo, setFileInfo] = useState();
    const [itemInfo, setItemInfo] = useState();
    const [error, setError] = useState();
    const [roles, setRoles] = useState([]);
    const [info, setInfo] = useState("id_nv");
    const [btnLoading, setBtnLoading] = useState();

    const optionweek = [
        { key: "1", value: "2", text: "Thứ 2" },
        { key: "2", value: "3", text: "Thứ 3" },
        { key: "3", value: "4", text: "Thứ 4" },
        { key: "4", value: "5", text: "Thứ 5" },
        { key: "5", value: "6", text: "Thứ 6" },
        { key: "6", value: "7", text: "Thứ 7" },
        { key: "7", value: "8", text: "Chủ nhật" },
    ]

    useEffect(() => {
        zzControlHelper.dragElement(document.getElementById('Edit-form'), 'header-Edit-form')
    })

    const handleSubmit = (params, formId) => {
        // if (_.join(params.wd_new, ',') !== ', ') {
        //     let next_stt = _.join(params.wd_new, ',')
        //     params.wd_new = next_stt
        // }
        ApiHrLichDay.updateAll({ ...params, id: id }, res => {
            if (res.status >= 200 && 204 >= res.status) {
                onAfterSave && onAfterSave({ ...res.data.data })
                ZenMessageToast.success();
                onClose()
            } else {
                setError(zzControlHelper.getResponseError(res))
                ZenMessageAlert.error(zzControlHelper.getResponseError(res))
            }
        })
    }

    const customHeader = (id) => {
        const header = 'hợp đồng lao động'
        const txtHeader = !id ? `Thêm mới ${header}` : capitalize(header) + ` : ${id}`
        return txtHeader
    }
    const capitalize = (s) => {
        if (typeof s !== 'string') return ''
        return s.charAt(0).toUpperCase() + s.slice(1)
    }

    const handleItemSelected = (option, { name }) => {
        setInfo(option.value)
        refFormik.current.setValues({
            ...refFormik.current.values,
            field: option.value,
        })
    }

    const onChangeChk = (data, evt) => {
        const { checked } = data
        refFormik.current.setValues({
            ...refFormik.current.values,
            type: evt,
        })
    }

    const onValidate = (item) => {
        const errors = {};
        const { type, field, tu_new, den_new, id_nv_new, wd_new } = item
        if (field == 'id_nv') {
            if (!id_nv_new) {
                errors.id_nv_new = 'Không được bỏ trống trường này !'
            }
        }
        if (field == 'thoi_gian') {
            if (!tu_new) {
                errors.tu_new = 'Không được bỏ trống trường này !'
            }
            if (!den_new) {
                errors.den_new = 'Không được bỏ trống trường này !'
            }
        }
        if (field == 'wd') {
            if (!wd_new) {
                errors.wd_new = 'Không được bỏ trống trường này !'
            }
        }
        return errors
    }

    return <>
        <Modal id="Edit-form"
            closeOnEscape closeIcon closeOnDimmerClick={false}
            onClose={() => onClose()}
            //onOpen={() => onClose()}
            open={open}
            size={"tiny"}
        >
            <Modal.Header id='header-Edit-form' style={{ cursor: "grabbing" }}>
                Sửa hàng loạt
            </Modal.Header>

            <Modal.Content>
                <ZenFormik form={"DupCanledar"} ref={refFormik}
                    validation={formValidationEditCanlendar}
                    initItem={initItemEditCanledar}
                    onSubmit={handleSubmit}
                    onValidate={onValidate}
                >
                    {
                        formik => {
                            return <Form>
                                <div style={{ fontWeight: "bold" }}>
                                    <Checkbox
                                        label="Sửa hàng loạt theo tuần"
                                        name="editweek"
                                        checked={formik.values.type === 'w'}
                                        onClick={(evt, data) => onChangeChk(data, 'w')}
                                    />
                                </div>
                                <div style={{ paddingLeft: "25px" }}>
                                    <p>Hệ thống sẽ cập nhật tất cả lịch trong cùng khung giờ của tất cả các ngày trong tuần tiếp theo cho đến hết lịch của trường</p>
                                </div>
                                <br />
                                <div style={{ fontWeight: "bold" }}>
                                    <Checkbox
                                        label="Sửa hàng loạt theo ngày"
                                        name="editday"
                                        checked={formik.values.type === 'd'}
                                        onClick={(evt, data) => onChangeChk(data, 'd')}
                                    />
                                </div>
                                <div style={{ paddingLeft: "25px" }}>
                                    <p>Hệ thống sẽ cập nhật tất cả lịch trong cùng khung giờ của các ngày trong tuần tiếp theo cho đến hết lịch của trường</p>
                                </div>
                                <br />
                                <Form.Group>
                                    <ZenFieldSelect width="16" name="field"
                                        options={[
                                            { key: "1", value: "id_nv", text: "Giáo viên" },
                                            { key: "2", value: "thoi_gian", text: "Khung thời gian" },
                                            { key: "3", value: "wd", text: "Ngày trong tuần" },
                                        ]}
                                        onSelectedItem={handleItemSelected}
                                        clearable={false}
                                        props={formik}
                                        label="Edit.field" defaultlabel="Thông tin sửa"
                                    />
                                </Form.Group>
                                <Form.Group>
                                    {
                                        info == "id_nv" &&
                                        <ZenFieldSelectApi width="16"
                                            lookup={ZenLookup.ID_NV}
                                            label="hrlichday.id_nv" defaultlabel="Giáo viên"
                                            name="id_nv_new"
                                            formik={formik}
                                        //onItemSelected={(item, { name }) => handleselect(item, { name })}
                                        />
                                    }
                                    {
                                        info == "thoi_gian" &&
                                        <>
                                            <Form.Field width={8}
                                                error={formik && (formik.touched['tu'] && formik.errors['tu'])}
                                            >
                                                <label>Từ</label>
                                                <DatePicker
                                                    selected={formik.values.tu_new ? new Date(formik.values.tu_new) : undefined} //moment(new Date(formik.values.tu).toISOString()).toDate()
                                                    onChange={(date) => {
                                                        let tu = new Date(date)
                                                        let ngay = new Date()
                                                        ngay.setHours(tu.getHours())
                                                        ngay.setMinutes(tu.getMinutes())
                                                        formik.setFieldValue('tu_new', zzControlHelper.formatDateTime(ngay, 'YYYY-MM-DD HH:mm'))
                                                    }}
                                                    showTimeSelect
                                                    showTimeSelectOnly
                                                    timeIntervals={30}
                                                    timeFormat="HH:mm"
                                                    timeCaption="Time"
                                                    dateFormat="HH:mm"
                                                />
                                                {formik && (formik.touched['tu'] && formik.errors['tu']) ? <div class="ui pointing above prompt label" role="alert" aria-atomic="true">Không được bỏ trống trường này</div> : undefined}
                                            </Form.Field>
                                            <Form.Field width={8}
                                                error={formik && (formik.touched['den'] && formik.errors['den'])}
                                            >
                                                <label>Đến</label>
                                                <DatePicker
                                                    selected={formik.values.den_new ? new Date(formik.values.den_new) : undefined}
                                                    onChange={(date) => {
                                                        let den = new Date(date)
                                                        let ngay = new Date()
                                                        ngay.setHours(den.getHours())
                                                        ngay.setMinutes(den.getMinutes())
                                                        formik.setFieldValue('den_new', zzControlHelper.formatDateTime(ngay, 'YYYY-MM-DD HH:mm'))
                                                    }}
                                                    showTimeSelect
                                                    showTimeSelectOnly
                                                    timeIntervals={30}
                                                    timeFormat="HH:mm"
                                                    timeCaption="Time"
                                                    dateFormat="HH:mm"
                                                />
                                                {formik && (formik.touched['den'] && formik.errors['den']) ? <div class="ui pointing above prompt label" role="alert" aria-atomic="true">Không được bỏ trống trường này</div> : undefined}
                                            </Form.Field>
                                        </>
                                    }
                                    {
                                        info == "wd" &&
                                        <ZenFieldSelect width={16} options={optionweek} name="wd_new"
                                            label={"hrhsnsquery.week_day"} defaultlabel="Ngày trong tuần"
                                            props={formik}
                                        />
                                    }
                                </Form.Group>
                            </Form>
                        }
                    }
                </ZenFormik>
            </Modal.Content>
            <Modal.Actions>
                <ZenButton btnType={"cancel"} size="small" onClick={() => onClose()} />
                <Button size="small"
                    icon="checkmark"
                    content="Thực hiện"
                    size="small"
                    primary
                    loading={btnLoading} type="submit"
                    onClick={(e) => refFormik.current.handleSubmit(e)}
                />
            </Modal.Actions>
        </Modal>
    </>
}

const initItemEditCanledar = {
    type: "w",
    field: "id_nv",
    id_nv_new: "",
    wd_new: "",
    tu_new: new Date(),
    den_new: new Date(),
}

const formValidationEditCanlendar = [
    // {
    //     id: "so_tuan",
    //     validationType: "number",
    //     validations: [
    //         {
    //             type: "required",
    //             params: ["Không được bỏ trống trường này"]
    //         },
    //         {
    //             type: "min",
    //             params: [1,"Không được nhỏ hơn 0"]
    //         },
    //     ]
    // },
]

const ModalDeleteCanledar = ({ open, id, onClose, onAfterSave, }) => {
    const _isMounted = useRef(true);
    const refFormik = useRef()
    const [fileInfo, setFileInfo] = useState();
    const [itemInfo, setItemInfo] = useState();
    const [error, setError] = useState();
    const [roles, setRoles] = useState([]);
    const [info, setInfo] = useState("id_nv");
    const [btnLoading, setBtnLoading] = useState();

    useEffect(() => {
        zzControlHelper.dragElement(document.getElementById('Delete-form'), 'header-Delete-form')
    })

    const handleSubmit = (params, formId) => {
        // if (_.join(params.wd_new, ',') !== ', ') {
        //     let next_stt = _.join(params.wd_new, ',')
        //     params.wd_new = next_stt
        // }
        // params.id = id
        ApiHrLichDay.deleteall({ ...params, id: id }, res => {
            if (res.status >= 200 && 204 >= res.status) {
                onAfterSave && onAfterSave({ ...res.data.data })
                ZenMessageToast.success();
                onClose()
            } else {
                setError(zzControlHelper.getResponseError(res))
                ZenMessageAlert.error(zzControlHelper.getResponseError(res))
            }
        })
    }

    const customHeader = (id) => {
        const header = 'hợp đồng lao động'
        const txtHeader = !id ? `Thêm mới ${header}` : capitalize(header) + ` : ${id}`
        return txtHeader
    }
    const capitalize = (s) => {
        if (typeof s !== 'string') return ''
        return s.charAt(0).toUpperCase() + s.slice(1)
    }

    const handleItemSelected = (option, { name }) => {
        setInfo(option.value)
        refFormik.current.setValues({
            ...refFormik.current.values,
            field: option.value,
        })
    }

    const onChangeChk = (data, evt) => {
        const { checked } = data
        refFormik.current.setValues({
            ...refFormik.current.values,
            type: evt,
        })
    }

    return <>
        <Modal id="Delete-form"
            closeOnEscape closeIcon closeOnDimmerClick={false}
            onClose={() => onClose()}
            //onOpen={() => onClose()}
            open={open}
            size={"tiny"}
        >
            <Modal.Header id='header-Delete-form' style={{ cursor: "grabbing" }}>
                Xóa hàng loạt
            </Modal.Header>

            <Modal.Content>
                <ZenFormik form={"DupCanledar"} ref={refFormik}
                    validation={formValidationDeleteCanlendar}
                    initItem={initItemDeleteCanledar}
                    onSubmit={handleSubmit}
                >
                    {
                        formik => {
                            return <Form>
                                <div style={{ fontWeight: "bold" }}>
                                    <Checkbox
                                        label="Xóa hàng loạt theo tuần"
                                        name="Deleteweek"
                                        checked={formik.values.type === 'w'}
                                        onClick={(evt, data) => onChangeChk(data, 'w')}
                                    />
                                </div>
                                <div style={{ paddingLeft: "25px" }}>
                                    <p>Hệ thống sẽ cập nhật tất cả lịch trong cùng khung giờ của tất cả các ngày trong tuần tiếp theo cho đến hết lịch của trường</p>
                                </div>
                                <br />
                                <div style={{ fontWeight: "bold" }}>
                                    <Checkbox
                                        label="Xóa hàng loạt theo ngày"
                                        name="Deleteday"
                                        checked={formik.values.type === 'd'}
                                        onClick={(evt, data) => onChangeChk(data, 'd')}
                                    />
                                </div>
                                <div style={{ paddingLeft: "25px" }}>
                                    <p>Hệ thống sẽ cập nhật tất cả lịch trong cùng khung giờ của các ngày trong tuần tiếp theo cho đến hết lịch của trường</p>
                                </div>
                                <br />
                            </Form>
                        }
                    }
                </ZenFormik>
            </Modal.Content>
            <Modal.Actions>
                <ZenButton btnType={"cancel"} size="small" onClick={() => onClose()} />
                <Button size="small"
                    icon="checkmark"
                    content="Thực hiện"
                    primary
                    loading={btnLoading} type="submit"
                    onClick={(e) => refFormik.current.handleSubmit(e)}
                />
            </Modal.Actions>
        </Modal>
    </>
}

const initItemDeleteCanledar = {
    type: "w",
}

const formValidationDeleteCanlendar = [
]

const ModalChamcongCanledar = ({ open, id, onClose, onAfterSave, }) => {
    const _isMounted = useRef(true);
    const refFormik = useRef()
    const [data, setData] = useState()
    const [error, setError] = useState();
    const [roles, setRoles] = useState([]);
    const [info, setInfo] = useState("id_nv");
    const [btnLoading, setBtnLoading] = useState();

    useEffect(() => {
        zzControlHelper.dragElement(document.getElementById('Chamcong-form'), 'header-Chamcong-form')
    })

    useEffect(() => {
        ApiHrLichDay.getByCode(id, res => {
            if (res.status === 200) {
                setData(res.data.data)
                let htu = new Date(res.data.data.tu).getHours(),
                    mtu = new Date(res.data.data.tu).getMinutes(),
                    hden = new Date(res.data.data.den).getHours(),
                    mden = new Date(res.data.data.den).getMinutes()
                refFormik.current.setValues({
                    ...refFormik.current.values,
                    gio: res.data.data?.loai == 'h' ? res.data.data?.cham_cong : ((hden + (mden / 60)) - (htu + (mtu / 60))),
                    ngay: res.data.data?.loai == 'd' ? res.data.data?.cham_cong : res.data.data?.cham_cong ? res.data.data?.cham_cong >= 8 ? 1 : 0.5 : ((hden + (mden / 60)) - (htu + (mtu / 60))) >= 8 ? 1 : 0.5
                })
            }
        })
    }, [])

    const handleSubmit = (params, formId) => {
        if (params.loai == 'd') {
            params.gio = params.ngay
            delete params.ngay
        } else {
            delete params.ngay
        }
        ApiHrLichDay.chamcong({ ...params, id: id }, res => {
            if (res.status >= 200 && 204 >= res.status) {
                onAfterSave && onAfterSave({ ...res.data.data })
                ZenMessageToast.success();
                onClose()
            } else {
                setError(zzControlHelper.getResponseError(res))
                ZenMessageAlert.error(zzControlHelper.getResponseError(res))
            }
        })
    }

    const customHeader = (id) => {
        const header = 'hợp đồng lao động'
        const txtHeader = !id ? `Thêm mới ${header}` : capitalize(header) + ` : ${id}`
        return txtHeader
    }
    const capitalize = (s) => {
        if (typeof s !== 'string') return ''
        return s.charAt(0).toUpperCase() + s.slice(1)
    }

    return <>
        <Modal id="Chamcong-form"
            closeOnEscape closeIcon closeOnDimmerClick={false}
            onClose={() => onClose()}
            //onOpen={() => onClose()}
            open={open}
            size={"tiny"}
        >
            <Modal.Header id='header-Chamcong-form' style={{ cursor: "grabbing" }}>
                Chấm công
            </Modal.Header>

            <Modal.Content>
                <ZenFormik form={"DupCanledar"} ref={refFormik}
                    validation={formValidationChamcongCanlendar}
                    initItem={initItemChamcongCanledar}
                    onSubmit={handleSubmit}
                >
                    {
                        formik => {
                            return <>
                                <Form>
                                    <div>
                                        <div>
                                            <label>Giáo viên : </label>
                                            <strong>{data?.ten_nv}</strong>
                                        </div>
                                        <div>
                                            <label>Khách hàng : </label>
                                            <strong>{data?.ten_kh}</strong>
                                        </div>
                                        <div>
                                            <label>Ngày : </label>
                                            <strong> {zzControlHelper.formatDateTime(data?.ngay, 'DD/MM/YYYY')} </strong>
                                            Từ <strong> {zzControlHelper.formatDateTime(data?.tu, 'HH:mm')} </strong>
                                            đến <strong> {zzControlHelper.formatDateTime(data?.den, 'HH:mm')} </strong>
                                        </div>
                                    </div>
                                    <br />
                                    <Divider />
                                    <Form.Group widths={"equal"}>
                                        <ZenFieldNumber
                                            name="ngay"
                                            decimalScale={"1"}
                                            props={formik}
                                            label="hrlichday.ngaycong" defaultlabel="Chấm công theo ngày công (Fulltime)"
                                        />
                                        <ZenFieldNumber
                                            name="gio"
                                            decimalScale={"2"}
                                            props={formik}
                                            label="hrlichday.sogiocong" defaultlabel="Chấm công theo giờ (Partime)"
                                        />
                                    </Form.Group>
                                    <Form.Group widths="equal">
                                        <Form.Field>
                                            <Button size="small" style={{ width: "100%" }}
                                                icon="calendar check outline"
                                                content="Chấm công ngày"
                                                primary
                                                loading={btnLoading} type="submit"
                                                onClick={(e) => {
                                                    refFormik.current.setFieldValue('loai', 'd')
                                                    refFormik.current.handleSubmit(e)
                                                }}
                                            />
                                            <label style={{ paddingTop: "10px" }}><i>Nếu giáo viên dạy <span style={{ color: "red" }}>Fulltime</span> thì sử dụng nút "Chấm công theo ngày"</i></label>
                                        </Form.Field>
                                        <Form.Field>
                                            <Button size="small" style={{ width: "100%" }}
                                                icon="clock outline"
                                                content="Chấm công giờ"
                                                primary
                                                loading={btnLoading} type="submit"
                                                onClick={(e) => {
                                                    refFormik.current.setFieldValue('loai', 'h')
                                                    refFormik.current.handleSubmit(e)
                                                }}
                                            />
                                            <label style={{ paddingTop: "10px" }}><i>Nếu giáo viên dạy <span style={{ color: "red" }}>Partime</span> thì sử dụng nút "Chấm công theo giờ"</i></label>
                                        </Form.Field>
                                    </Form.Group>
                                </Form>
                            </>
                        }
                    }
                </ZenFormik>
            </Modal.Content>
            <Modal.Actions>
                <div style={{ display: "flex", justifyContent: "space-between" }}>
                    <ZenButton btnType={"delete"} content="Xóa chấm công" size="small"
                        onClick={(e) => {
                            refFormik.current.setValues({ gio: 0, ngay: 0, loai: "" })
                            refFormik.current.handleSubmit(e)
                        }} />
                    <ZenButton btnType={"cancel"} size="small" onClick={() => onClose()} />
                </div>
            </Modal.Actions>
        </Modal>
    </>
}

const initItemChamcongCanledar = {
    gio: 0,
    ngay: 0,
}

const formValidationChamcongCanlendar = [
    {
        id: "gio",
        validationType: "number",
        validations: [
            {
                type: 'required',
                params: ["Không được bỏ trống trường này"]
            },
        ]
    }
]

const ModalInfo = ({ open, id, onClose }) => {
    const [data, setData] = useState()
    // const [error, setError] = useState();
    // const [btnLoading, setBtnLoading] = useState();

    // useEffect(() => {
    //     zzControlHelper.dragElement(document.getElementById('info-form'), 'header-info-form')
    // })

    useEffect(() => {
        ApiHrLichDay.getByCode(id, res => {
            if (res.status === 200) {
                setData(res.data.data)
            }
        })
    }, [])
    return <>
        <Modal id="info-form"
            closeOnEscape closeIcon closeOnDimmerClick={false}
            onClose={() => onClose()}
            open={open}
            size={"tiny"}
        >
            {/* <Modal.Header id='header-tailieu-form' style={{ cursor: "grabbing" }}>
            </Modal.Header> */}
            <Modal.Content>
                <Table basic='very'>
                    <Table.Body>
                        <Table.Row >
                            <Table.Cell textAlign="right" collapsing>Tên trường :</Table.Cell>
                            <Table.Cell style={{ fontWeight: "bold", whiteSpace: "" }}>
                                {data?.ten_kh}
                            </Table.Cell>
                        </Table.Row>
                        <Table.Row >
                            <Table.Cell textAlign="right" collapsing>Giáo viên :</Table.Cell>
                            <Table.Cell style={{ fontWeight: "bold", whiteSpace: "" }}>
                                {data?.ten_nv}
                            </Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell textAlign="right" collapsing>Ngày :</Table.Cell>
                            <Table.Cell style={{ fontWeight: "bold", whiteSpace: "" }}>
                                {`${zzControlHelper.formatDateTime(data?.ngay, 'DD/MM/YYYY')} `}&nbsp;&nbsp;&nbsp;{`     
                                ${zzControlHelper.formatDateTime(data?.tu, 'HH:mm')} - ${zzControlHelper.formatDateTime(data?.den, 'HH:mm')}`}
                            </Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell textAlign="right" collapsing>Ghi chú :</Table.Cell>
                            <Table.Cell style={{ fontWeight: "bold", whiteSpace: "textarea" }}>
                                {data?.ghi_chu}
                            </Table.Cell>
                        </Table.Row>
                    </Table.Body>
                </Table>
            </Modal.Content>
            <Modal.Actions>
                <ZenButton btnType={"cancel"} size="small" onClick={() => onClose()} />
            </Modal.Actions>
        </Modal>
    </>
}

export const HrLichDayWeek = {
    HrlichdayWeekform: {
        route: routes.HrLichDayWeek,
        Zenform: HrLichDayWeekForm,
        action: {
            view: { visible: true, permission: permissions.HrLichDayXem },
            add: { visible: true, permission: permissions.HrLichDayThem },
            edit: { visible: true, permission: permissions.HrLichDaySua },
            del: { visible: true, permission: permissions.HrLichDayXoa },
        },
    },
}