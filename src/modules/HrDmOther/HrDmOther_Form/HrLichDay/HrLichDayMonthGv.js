import React, { useEffect, useMemo, useRef, useState, useCallback } from "react";
import { Accordion, Breadcrumb, Button, Checkbox, Form, Grid, Header, Icon, Loader, Modal, Segment, Tab, Table } from "semantic-ui-react";
import { ZenFieldCheckbox, ZenLoading, ZenButton, ContainerScroll, ZenFormik, ZenFieldDate, ZenFieldSelect, SegmentHeader, ZenFieldSelectApi, InputDatePeriod, ZenDatePeriod, HeaderLink, Helmet } from "../../../../components/Control/index";
import { zzControlHelper } from "../../../../components/Control/zzControlHelper";
import { ZenHelper, auth } from "../../../../utils";
import { ZenLookup } from "../../../ComponentInfo/Dictionary/ZenLookup";
import { Calendar, momentLocalizer, Views, Navigate } from "react-big-calendar";
import moment from "moment";
import "react-big-calendar/lib/css/react-big-calendar.css";
import { ApiHrLichDay } from './../../Api/index'
import * as routes from './../../../../constants/routes'
import { debounce } from "lodash-es";
import { RowTotalPH, TableScroll, TableTotalPH } from "../../../../components/Control/zVoucher";
import FormatNumber from "../../../../components/Control/FormatNumber"
import { FormattedDate } from "react-intl";
import * as permissions from "../../../../constants/permissions";

const localizer = momentLocalizer(moment);

export const HrLichDayMonthForm = (props) => {
    //const { onLoadData, fieldCode } = props;
    const refForm = useRef();
    const [expand, setExpand] = useState(0);
    const [events, setEvents] = useState([]);
    const [eventsOriginal, setEventsOriginal] = useState([]);
    const [totals, setTotals] = useState([]);
    const [day, setDay] = useState(new Date());
    const [open, setOpen] = useState(false);
    const [loadingForm, setLoadingForm] = useState(false)
    const [event, setEvent] = useState();

    const [_type, setType] = useState('nv');
    const [_ngay, setNgay] = useState(ZenHelper.formatDateTime(new Date(), 'YYYY/MM/DD'))
    const [_idnv, setIdnv] = useState()
    const [_makh, setMakh] = useState()

    useEffect(() => {
        let user = auth.getUserInfo()
        setLoadingForm(true)
        ApiHrLichDay.getM('nv', _ngay, user.hrid, null, res => {
            if (res.status === 200) {
                var newEvents = []
                if (res.data.data.events.length > 0) {
                    res.data.data.events.forEach(event => {
                        let newevent = { ...event, start: new Date(event.start), end: new Date(event.end) }
                        newEvents.push(newevent)
                    })
                }
                setEvents(newEvents)
                setEventsOriginal(res.data.data.events)
                setTotals(res.data.data.totals)
            }
            setLoadingForm(false)
        })
    }, [])

    const handleSubmit = (item, { name }) => {
        let ngay, idnv, makh;
        if (name === 'ngay') {
            setNgay(item)
            ngay = item
            setDay(new Date(item))
        }
        if (name === 'type') {
            if (item.value === 'nv') {
                refForm.current.setValues({
                    ...refForm.current.values,
                    type: item.value,
                    makh: null,
                })
                setMakh()
            }
            if (item.value === 'kh') {
                refForm.current.setValues({
                    ...refForm.current.values,
                    type: item.value,
                    idnv: null,
                })
                setIdnv()
            }
            setType(item.value)

        }

        if (name === 'idnv') {
            idnv = item?.value ? item.value : item.id_nv
            setIdnv(item?.value ? item.value : item.id_nv)
            makh = null
            setMakh()
        }
        if (name === 'makh') {
            makh = item?.value ? item.value : item.ma_kh
            setMakh(item?.value ? item.value : item.ma_kh)
            idnv = null
            setIdnv()
        }
        if (ngay !== _ngay || name === 'type' || name === 'idnv' || name === 'makh') {
            setLoadingForm(true)
            ApiHrLichDay.getM((name === 'type') ? item.value : _type, ZenHelper.formatDateTime(ngay ? ngay : day, 'YYYY/MM/DD') || _ngay, name === 'idnv' ? idnv : _idnv, name === 'makh' ? makh : _makh, res => {
                if (res.status === 200) {
                    var newEvents = []
                    if (res.data.data.events.length > 0) {
                        res.data.data.events.forEach(event => {
                            let newevent = { ...event, start: new Date(event.start), end: new Date(event.end) }
                            newEvents.push(newevent)
                        })
                    }
                    setEvents(newEvents)
                    setTotals(res.data.data.totals)
                    setEventsOriginal(res.data.data.events)
                }
                setLoadingForm(false)
            })
        }
    }

    const debounceDateView = useCallback(debounce((item, name) => handleSubmit(item, { name: name }), 1000), [])

    const handleChange = (item, { name }) => {
        debounceDateView(item, name)
    }

    function getUrlLichdaylist() {
        return {
            pathname: `${routes.HrLichDay}`,
            //   search: `?`+ new URLSearchParams({tk: filter.tk,}).toString(),
            //   state: { tk: filter.tk },
        }
    }
    function getUrlLichdayWeek() {
        return {
            pathname: `${routes.HrLichDayWeek}`,
            //   search: `?`+ new URLSearchParams({tk: filter.tk,}).toString(),
            //   state: { tk: filter.tk },
        }
    }
    function getUrlLichdayMonth() {
        return {
            pathname: `${routes.HrLichDayMonth}`,
            //   search: `?`+ new URLSearchParams({tk: filter.tk,}).toString(),
            //   state: { tk: filter.tk },
        }
    }

    const handleChangeView = (e) => {
        let tp = 'W'
        var _switch = {
            'month': 'M',
            'week': 'W',
            'day': 'D',
        };
        tp = _switch[e]
        setType(tp)
    }

    const popoverButtonClickHandler = (e, event) => {
        //handle button click
        setEvent(event.event)
        setOpen(true)
    }
    const onChangeChk = (data, item, index) => {
        const { checked } = data
        if (!checked) {
            const newEvents = events.filter(t => t.ma2 !== item.ma)
            setEvents(newEvents)
        }
        if (checked) {
            const newItemEvent = eventsOriginal.filter(t => t.ma2 === item.ma)
            setEvents(events.concat(newItemEvent))
        }
    }

    function checkPermissionreverse(permisson) {
        var user = auth.getUserInfo()
        if (user.admin === "True") {
            return false;
        } else {
            return !permisson ? false : user.permisson.includes(permisson);
        }
    }

    return <>
        <div style={{ display: "flex", justifyContent: "space-between", marginBottom: "10px" }}>
            <PageHeader />
        </div>
        <Segment>

            <ZenLoading loading={loadingForm} inline="centered" dimmerPage={false} />
            <Calendar
                localizer={localizer}
                defaultDate={new Date}
                default={() => new Date(day)}
                defaultView="month"
                events={events}
                style={{ height: "100vh" }}
                views={{ month: true, week: true, work_week: true, day: true, agenda: true }} // 'month','week', 'day', 'agenda'
                showMultiDayTimes
                popup
                components={{
                    event: CustomEventContainer({
                        onPopoverButtonClick: popoverButtonClickHandler
                    })
                }}
                date={day}
                onNavigate={date => {
                    handleSubmit(date, { name: 'ngay' })
                }}
            //onShowMore={(events, date) => console.log(events,date)}
            //onView={(e) => {handleChangeView(e)}}
            />
        </Segment>
        {open && <ModalEvent open={open}
            event={event}
            onClose={() => setOpen(false)}
        />}
    </>
}

const ColoredDateCellWrapper = ({ children }) =>
    React.cloneElement(React.Children.only(children), {
        style: {
            backgroundColor: 'lightblue',
        },
    })

const CustomEventContainer = ({ onPopoverButtonClick }) => props => {
    return <CustomEvent event={props} onPopoverButtonClick={onPopoverButtonClick} />;
}

const ModalEvent = (props) => {
    const { event, open, onClose, } = props
    const [loadingForm, setLoadingForm] = useState(false)
    const [data, setData] = useState()
    useEffect(() => {
        const { id } = event
        ApiHrLichDay.getByCode(id, res => {
            if (res.status === 200) {
                setData(res.data.data)
            }
        })
    }, [])
    return <>
        <Modal id="event-form"
            closeOnEscape closeIcon closeOnDimmerClick={false}
            onClose={() => onClose()}
            //onOpen={() => onClose()}
            open={open}
            size={"tiny"}
        >
            {/* <Modal.Header id='header-tailieu-form' style={{ cursor: "grabbing" }}>
            </Modal.Header> */}
            <Modal.Content>
                {/* <ZenLoading loading={true} inline="centered" content={"loading..."} /> */}
                <Table basic='very'>
                    <Table.Body>
                        <Table.Row >
                            <Table.Cell textAlign="right" collapsing>School's name :</Table.Cell>
                            <Table.Cell style={{ fontWeight: "bold", whiteSpace: "" }}>
                                {data?.ten_kh}
                            </Table.Cell>
                        </Table.Row>
                        <Table.Row >
                            <Table.Cell textAlign="right" collapsing>Teacher's name :</Table.Cell>
                            <Table.Cell style={{ fontWeight: "bold", whiteSpace: "" }}>
                                {data?.ten_nv}
                            </Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell textAlign="right" collapsing>Date :</Table.Cell>
                            <Table.Cell style={{ fontWeight: "bold", whiteSpace: "" }}>
                                {`${zzControlHelper.formatDateTime(data?.ngay, 'DD/MM/YYYY')} `}&nbsp;&nbsp;&nbsp;{`     
                                ${zzControlHelper.formatDateTime(data?.tu, 'HH:mm')} - ${zzControlHelper.formatDateTime(data?.den, 'HH:mm')}`}
                            </Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell textAlign="right" collapsing>Note :</Table.Cell>
                            <Table.Cell style={{ fontWeight: "bold", whiteSpace: "textarea" }}>
                                {data?.ghi_chu}
                            </Table.Cell>
                        </Table.Row>
                    </Table.Body>
                </Table>
            </Modal.Content>
            <Modal.Actions>
                <ZenButton btnType={"cancel"} size="small" onClick={() => onClose()} />
            </Modal.Actions>
        </Modal>
    </>
}

const CustomEvent = React.memo((props) => {
    const { event } = props
    //...
    return (
        <div onClick={(e) => props.onPopoverButtonClick(e, event)} style={{ fontSize: "0.8em", fontWeight: "800" }}>
            {event.title}
        </div>
    );
})

const initItem = {
    ngay: new Date(),
    type: "nv",
    idnv: "",
    makh: "",
}

const PageHeader = ({ item }) => {
    return <div style={{}}>
        <Helmet idMessage={"PMDuAn.detail"}
            defaultMessage={"Schedule"} />
        <SegmentHeader>
            <HeaderLink >
                <Breadcrumb.Divider icon='right chevron' />
                <Breadcrumb.Section active>
                    Schedule
                </Breadcrumb.Section>
            </HeaderLink>
        </SegmentHeader>
        <br />
    </div>
}

export const HrLichDayMonthGv = {
    HrlichdayMonthGvform: {
        route: routes.HrLichDayGv,
        Zenform: HrLichDayMonthForm,
        action: {
            view: { visible: true, permission: permissions.HrLichDayXemAll },
        },
    },
}
