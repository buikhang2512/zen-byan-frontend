import axios, { axiosMultiFormData } from '../../../Api/axios';

const ExtName = "hrlichday"

export const ApiHrLichDay = {
   get(callback, filter = {}, pagination = {}) {
      axios.get(`${ExtName}`, {
         params: {
            qf: filter.qf,
            keyword: filter.keyword,
            sort: filter.sort,
            page: pagination.page,
            pageSize: pagination.pageSize
         },
      })
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   getByCode(code, callback) {
      axios.get(`${ExtName}/` + code)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   insert(data, callback) {
      axios.post(`${ExtName}`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   update(data, callback) {
      axios.put(`${ExtName}`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   updatePatch(code, data, callback) {
      axios.patch(`${ExtName}/${code}`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   delete(code, callback) {
      axios.delete(`${ExtName}/` + code)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   getW(type, day, idnv, makh, callback) {
      let query = ""
      if (type) query += `type=${type}`;
      if (day) query += `&day=${day}`;
      if (idnv) query += `&idnv=${idnv}`
      if (makh) query += `&makh=${makh}`
      axios.get(`${ExtName}/calw?${query}`)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   getM(type, day, idnv, makh, callback) {
      let query = ""
      if (type) query += `type=${type}`;
      if (day) query += `&day=${day}`;
      if (idnv) query += `&idnv=${idnv}`
      if (makh) query += `&makh=${makh}`
      axios.get(`${ExtName}/calm?${query}`)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },
   duplication(data, callback) {
      axios.post(`${ExtName}/duplication`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },
   updateAll(data, callback) {
      axios.put(`${ExtName}/All`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },
   deleteall(data, callback) {
      axios.delete(`${ExtName}/All`, { data: data })
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },
   chamcong(data, callback) {
      axios.post(`${ExtName}/Chamcong`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   dowloadpdf(data, callback) {
      const { day,idnv,makh } = data
      let query = ""
      if (day) query += `day=${day}`;
      if (idnv) query += `&idnv=${idnv}`
      if (makh) query += `&makh=${makh}`
      axios.post(`${ExtName}/pdf?${query}`,null, {
         responseType: 'blob'
      })
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   }
}