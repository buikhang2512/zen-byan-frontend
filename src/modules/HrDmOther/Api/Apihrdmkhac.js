import axios from '../../../Api/axios';

const ExtName = "HrDmKhac"

export const Apihrdmkhac = {
   getList(code,callback, filter = {}, pagination = {}) {
    axios.get(`${ExtName}/${code}`, {
       params: {
          qf: filter.qf,
          keyword: filter.keyword,
          sort: filter.sort,
          page: pagination.page,
          pageSize: pagination.pageSize
       },
    })
       .then(res => {
          callback(res)
       })
       .catch(err => {
          callback(err)
       });
 },

   getByCode(code,ma, callback) {
      axios.get(`${ExtName}/${code}/${ma}`)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   insert(data, callback) {
      axios.post(`${ExtName}`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   update(data, callback) {
      axios.put(`${ExtName}`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   updatePatch(code, data, callback) {
      axios.patch(`${ExtName}/${code}`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   delete(data, callback) {
      axios.delete(`${ExtName}`, {data : data})
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   }
}