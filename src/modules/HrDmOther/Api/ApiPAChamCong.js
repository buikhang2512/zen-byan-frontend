import axios, { axiosMultiFormData } from '../../../Api/axios';
const ExtName = 'PAChamCong'
export const ApiPAChamCong = {
    get(params, callback) {
        axios.get(`${ExtName}`, {
            params: {
                maKyLuong: params.maKyLuong,
                maBoPhan: params.maBoPhan,
                idNv: params.idNv,
            },
        })
            .then(res =>
                callback(res)
            )
            .catch(err =>
                callback(err)
            )
    },
    insert(params,callback) {
        axios.post(`${ExtName}`, params)
        .then(res =>
            callback(res)
        )
        .catch(err =>
            callback(err)
        )
    },
    update(params, callback) {
        axios.put(`${ExtName}`, params)
            .then(res =>
                callback(res)
            )
            .catch(err =>
                callback(err)
            )
    },
}