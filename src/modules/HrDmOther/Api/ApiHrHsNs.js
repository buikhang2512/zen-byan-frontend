import axios, { axiosMultiFormData } from '../../../Api/axios';

const ExtName = "HrHsNs"

export const ApiHrHsNs = {
   get(callback, filter = {}, pagination = {}) {
      axios.get(`${ExtName}`, {
         params: {
            qf: filter.qf,
            keyword: filter.keyword,
            sort: filter.sort,
            page: pagination.page,
            pageSize: pagination.pageSize
         },
      })
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   getByCode(code, callback) {
      axios.get(`${ExtName}/` + code)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   insert(data, callback) {
      axios.post(`${ExtName}`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   update(data, callback) {
      axios.put(`${ExtName}`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   updatePatch(code, data, callback) {
      axios.patch(`${ExtName}/${code}`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   delete(code, callback) {
      axios.delete(`${ExtName}/` + code)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },
   getAvatar(code, callback) {
      axios.get(`${ExtName}/${code}/anh`)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },
   uploadAvatar(data, callback) {
      axiosMultiFormData.put(`${ExtName}/anh`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   getHdld(code, callback) {
      axios.get(`${ExtName}/${code}/qthopdongld`)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },
   getTailieu(code, callback) {
      axios.get(`${ExtName}/${code}/qttailieu`)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },
   getLuong(code, callback) {
      axios.get(`${ExtName}/${code}/qtluong`)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },
   Query(data, callback) {
      axios.post(`${ExtName}/query`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },
   updateSocial(code, data, callback) {
      axios.put(`${ExtName}/${code}/social`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },
   getPhucap(code, callback) {
      axios.get(`${ExtName}/${code}/qtphucap`)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   setAccount(data, callback) {
      axios.post(`${ExtName}/${data.id_nv}/account?user=${data.user}`)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   }
}