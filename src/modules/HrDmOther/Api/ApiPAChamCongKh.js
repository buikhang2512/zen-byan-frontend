import axios, { axiosMultiFormData } from '../../../Api/axios';
const ExtName = 'PAChamCongKh'
export const ApiPAChamCongKH = {
    get(params, callback) {
        axios.get(`${ExtName}`, {
            params: {
                maKyLuong: params.maKyLuong,
                idNv: params.idNv,
            },
        })
            .then(res =>
                callback(res)
            )
            .catch(err =>
                callback(err)
            )
    },
    update(params, callback) {
        axios.put(`${ExtName}`, params)
            .then(res =>
                callback(res)
            )
            .catch(err =>
                callback(err)
            )
    },
    tonghop(params, callback) {
        axios.post(`${ExtName}/tonghoppa1`,params)
            .then(res =>
                callback(res)
            )
            .catch(err =>
                callback(err)
            )
    }
}