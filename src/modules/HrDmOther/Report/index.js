import { HrRptGiayToHetHan } from "./HrRptGiayToHetHan";
import { HrRptSinhNhat } from "./HrRptSinhNhat";

export const HR_Report = {
    HrRptGiayToHetHan: HrRptGiayToHetHan,
    HrRptSinhNhat: HrRptSinhNhat,
}