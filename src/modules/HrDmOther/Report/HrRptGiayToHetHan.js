import React from "react";
import { Table, Form } from "semantic-ui-react";
import { ZenFieldSelectApi, ZenDatePeriod, ZenFieldNumber, ZenFieldCheckbox, ZenFieldDate } from "../../../components/Control";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { RptHeader, RptTable, RptTableCell, RptTableRow } from "../../../components/Control/zReport";
import * as routes from "../../../constants/routes";

const FilterForm = ({ formik }) => {
   const handleItemSelected = (item, { name }) => {
      if (name === "tk") {
         formik.setFieldValue("ten_tk", item.ten_tk);
      }
   };
   return <>
      <ZenFieldSelectApi
         lookup={{ ...ZenLookup.MA_LOAI_TAI_LIEU, format: `{ten}` }}
         label={"rpt.loai_tai_lieu"} defaultlabel="Loại tài liệu"
         name="ma_loai_tai_lieu" formik={formik}
         onItemSelected={handleItemSelected}
      />
      <ZenFieldDate
         label={"rpt.ngay"} defaultlabel="Ngày"
         name="ngay" props={formik}
         decimalScale={0}
      />
      <ZenFieldNumber
         label={"rpt.so_ngay"} defaultlabel="Số ngày"
         name="so_ngay" props={formik}
         decimalScale={0}
      />
   </>
}

const TableForm = ({ data = [], filter = {} }) => {
   console.log(data)
   return <>
      <RptTable maNt={filter.Ma_Nt}>
         <Table.Header fullWidth>
            <RptHeader maNt={filter.Ma_Nt}
               header={[
                  { text: ["rpt.Id", "Id"] },
                  { text: ["rpt.Ten_nv", "Tên nhân sự"] },
                  { text: ["rpt.loai_tai_lieu", "Loại giấy tờ"] },
                  { text: ["rpt.ngay_cap", "Ngày cấp"] },
                  { text: ["rpt.noi_cap", "Nơi cấp"] },
                  { text: ["rpt.ngay_het_han", "Ngày hết hạn"] },
               ]}
            />
         </Table.Header>
         <Table.Body>
            {data.length > 0 ? data.map((item, index) => {
               console.log(item)
               const isNt = filter.Ma_Nt && filter.Ma_Nt !== 'VND' ? true : false
               return <RptTableRow key={index} isBold={item.Bold} index={index} item={item}>
                  <RptTableCell value={item.id} />
                  <RptTableCell value={item.ten_nv} />
                  <RptTableCell value={item.ngay_cap} type="date" />
                  <RptTableCell value={item.noi_cap} type="date" />
                  <RptTableCell value={item.ngay_het_han} type="date" />
               </RptTableRow>
            }) : undefined}
         </Table.Body>
      </RptTable>
   </>
}

export const HrRptGiayToHetHan = {
   FilterForm: FilterForm,
   TableForm: TableForm,
   permission: "",
   visible: true,    // hiện/ẩn báo cáo
   route: routes.HrRptGiayToHetHan,

   // period: {
   //    fromDate: "ngay_ct1",
   //    toDate: "ngay_ct2"
   // },

   linkHeader: {
      id: "HrRptGiayToHetHan",
      defaultMessage: "Kiểm tra giấy từ hết hạn",
      active: true
   },

   info: {
      code: "TLHetHan",
   },
   initFilter: {
      ma_loai_tai_lieu : "",
      ngay : new Date(),
      so_ngay: 1,
   },
   columns: [
      { id: "HrRptGiayToHetHan.id", defaultMessage: "Id", fieldName: "id_nv", type: "string", sorter: true, },
      { id: "HrRptGiayToHetHan.ho_ten", defaultMessage: "Tên nhân sự", fieldName: "ho_ten", type: "string", sorter: true },

      { id: "HrRptGiayToHetHan.loai", defaultMessage: "Loại giấy tờ", fieldName: "ten_loai_tai_lieu", type: "string", sorter: true, },
      { id: "HrRptGiayToHetHan.ngay_cap", defaultMessage: "Ngày cấp", fieldName: "ngay_cap", type: "date", sorter: true, },
      { id: "HrRptGiayToHetHan.noi_cap", defaultMessage: "Nơi Cấp", fieldName: "noi_cap", type: "date", sorter: true, },
      { id: "HrRptGiayToHetHan.ngay_het_han", defaultMessage: "Ngày hết hạn", fieldName: "ngay_het_han", type: "date", sorter: true, },
   ],
   formValidation: []
}
