import React from "react";
import { Table, Form } from "semantic-ui-react";
import { ZenFieldSelectApi, ZenDatePeriod, ZenFieldNumber, ZenFieldCheckbox, ZenFieldDate } from "../../../components/Control";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { RptHeader, RptTable, RptTableCell, RptTableRow } from "../../../components/Control/zReport";
import * as routes from "../../../constants/routes";
import { ZenHelper } from "../../../utils";

const FilterForm = ({ formik }) => {
   const handleItemSelected = (item, { name }) => {
      if (name === "tk") {
         formik.setFieldValue("ten_tk", item.ten_tk);
      }
   };
   const handleChangeDate = ({ startDate, endDate }) => {
      formik.setValues({
          ...formik.values,
          ngay1: startDate,
          ngay2: endDate,
      });
  };
   return <>
      <ZenDatePeriod
         onChange={handleChangeDate}
         value={[formik.values.ngay1, formik.values.ngay2]}
         textLabel="Từ ngày - đến ngày"
         defaultPopupYear={ZenHelper.getFiscalYear()}
      />
   </>
}

const TableForm = ({ data = [], filter = {} }) => {

   return <>
      <RptTable maNt={filter.Ma_Nt}>
         <Table.Header fullWidth>
            <RptHeader maNt={filter.Ma_Nt}
               header={[
                  { text: ["rpt.Id", "Id"] },
                  { text: ["rpt.Ten_nv", "Tên nhân sự"] },
                  { text: ["rpt.loai_tai_lieu", "Loại giấy tờ"] },
                  { text: ["rpt.ngay_cap", "Ngày cấp"] },
                  { text: ["rpt.noi_cap", "Nơi cấp"] },
                  { text: ["rpt.ngay_het_han", "Ngày hết hạn"] },
               ]}
            />
         </Table.Header>
         <Table.Body>
            {data.length > 0 ? data.map((item, index) => {
               console.log(item)
               const isNt = filter.Ma_Nt && filter.Ma_Nt !== 'VND' ? true : false
               return <RptTableRow key={index} isBold={item.Bold} index={index} item={item}>
                  <RptTableCell value={item.id} />
                  <RptTableCell value={item.ten_nv} />
                  <RptTableCell value={item.ngay_cap} type="date" />
                  <RptTableCell value={item.noi_cap} type="date" />
                  <RptTableCell value={item.ngay_het_han} type="date" />
               </RptTableRow>
            }) : undefined}
         </Table.Body>
      </RptTable>
   </>
}

export const HrRptSinhNhat = {
   FilterForm: FilterForm,
   TableForm: TableForm,
   permission: "",
   visible: true,    // hiện/ẩn báo cáo
   route: routes.HrRptSinhNhat,

   // period: {
   //    fromDate: "ngay1",
   //    toDate: "ngay2"
   // },

   linkHeader: {
      id: "HrRptSinhNhat",
      defaultMessage: "Nhân viên sắp tới sinh nhật",
      active: true
   },

   info: {
      code: "SNNV",
   },
   initFilter: {
      ngay1: new Date(),
      ngay2: new Date(),
   },
   columns: [
      { id: "HrRptSinhNhat.id_nv", defaultMessage: "Id", fieldName: "id_nv", type: "string", sorter: true },
      { id: "HrRptSinhNhat.ten_nv", defaultMessage: "Tên nhân sự", fieldName: "ho_ten", type: "string", sorter: true },

      { id: "HrRptSinhNhat.gioi_tinh", defaultMessage: "Giới tính", fieldName: "gioi_tinh", type: "string", sorter: true, },
      { id: "HrRptSinhNhat.ngay_cap", defaultMessage: "Ngày sinh", fieldName: "ngay_sinh", type: "date", sorter: true, },
      { id: "HrRptSinhNhat.ten_bo_phan", defaultMessage: "Bộ phận", fieldName: "ten_bo_phan", type: "string", sorter: true, },
   ],
   formValidation: []
}
