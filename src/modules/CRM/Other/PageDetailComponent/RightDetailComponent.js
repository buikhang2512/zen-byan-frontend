import React, { useContext, useEffect, useMemo, useRef, useState } from "react";
import {
   Button,
   Card, Comment, Form, Icon,
   Menu, Message, Segment, Table, TextArea, TableCell, Grid, Modal
} from "semantic-ui-react";

import {
   ZenMessageAlert, ZenButton,
   FormatDate, ConfirmDelete,
   ZenMessageToast, FormatNumber, ZenLink, ZenLoading, ZenFormik, ZenFieldSelectApi, ZenField, ZenFieldDate, ZenFieldNumber, ZenFieldTextArea, DiscussComment, ButtonEdit, ButtonDelete,
} from "../../../../components/Control/index";
import { ApiCRMDeal, ApiCRMHoatDong, ApiCRMNote } from "../../Api/index";
import { NoImage } from "../../../../resources/index";
import { FormMode, ZenHelper } from "../../../../utils/global";
import { IntlFormat } from "../../../../utils/intlFormat";
import { zzControlHelper } from "../../../../components/Control/zzControlHelper";
import { getModal } from "../../../ComponentInfo/Dictionary/ZenGetModal";
import { ZenLookup } from "../../../ComponentInfo/Dictionary/ZenLookup";
import { Mess_CRMDeal, Mess_CRMHoatDong } from "../../zLanguage/variable";
import { ContainerScroll } from "../../Component/Index";
import { ApiArDmKh } from "../../../AR/Api";
import { ApiSIDmHd } from "../../../SI/Api";
import { Mess_SIDmHd } from "../../../SI/zLanguage/variable";
import { ApiStored } from "../../../../Api";
import { FileAttachment } from "../../../../components/Control";
import { typeDetailPage } from "../../Component/zConstVariable";
import * as routes from "../../../../constants/routes";
import { DeleteCell, NumberCell, RowHeaderCell, RowTotalPH, TableScroll, TableTotalPH, TextCell, ActionType, VoucherHelper } from "../../../../components/Control/zVoucher";
import { auth } from "../../../../utils";
import { Mess_ARDmKh } from "../../../AR/zLanguage/variable"
import _ from "lodash";
import * as permissions from '../../../../constants/permissions'

export const RightDetailComponent = ({ data, keyData, type, menuBar, idElementContainer, ...rest }) => {
   return <MenuBar idElementContainer={idElementContainer}
      menuBar={menuBar} typeForm={type}
   >
      {
         ({ activeMenu }) => {
            return <>
               {menuBar.TTC?.visible && menuBar.TTC?.name === activeMenu && <TabTTC data={data} type={type} keyData={keyData} {...rest} />}
               {menuBar.TTCHD?.visible && menuBar.TTCHD?.name === activeMenu && <TabTTCHD data={data} type={type} keyData={keyData} {...rest} />}
               {menuBar.GhiChu?.visible && menuBar.GhiChu?.name === activeMenu && <TabNote data={data} type={type} keyData={keyData} {...rest} />}
               {menuBar.HHDV?.visible && menuBar.HHDV?.name == activeMenu && <TabHHDV data={data} type={type} keyData={keyData} {...rest} />}
               {menuBar.ThuTien?.visible && menuBar.ThuTien?.name == activeMenu && <TabThuTien data={data} type={type} keyData={keyData} {...rest} />}
               {menuBar.HoatDong?.visible && menuBar.HoatDong?.name === activeMenu && <TabHoatDong data={data} type={type} keyData={keyData} {...rest} />}
               {menuBar.HDNhanSu?.visible && menuBar.HDNhanSu?.name === activeMenu && <TabHDNhanSu data={data} type={type} keyData={keyData} {...rest} />}
               {menuBar.Deal?.visible && menuBar.Deal?.name === activeMenu && <TabDeal data={data} type={type} keyData={keyData} {...rest} />}
               {menuBar.HopDong?.visible && menuBar.HopDong?.name === activeMenu && <TabHopDong data={data} type={type} keyData={keyData} {...rest} />}
               {menuBar.FileAttach?.visible && menuBar.FileAttach?.name === activeMenu && <TabFileAttachment data={data} keyData={keyData} {...rest} fileCodeName={"CRM"} />}
               {menuBar.TraoDoi?.visible && menuBar.TraoDoi?.name === activeMenu && <TabTraoDoi data={data} type={type} keyData={keyData} {...rest} />}
            </>
         }
      }
   </MenuBar>
}

const MenuBar = ({ children, typeForm, idElementScroll = "content-right", idElementContainer, menuBar }) => {
   // ************ MEMO
   const memoizeMenu = useMemo(() => getMenu(), [""])

   // ************ STATE
   const [loadingForm, setLoadingForm] = useState(true)
   const [activeMenu, setActiveMenu] = useState(memoizeMenu.defaultMenu)

   // ************ HANDLE
   const handleChangeTab = (e, { name }) => {
      if (name !== activeMenu) {
         setActiveMenu(name)
         setLoadingForm(true)
         memoryStoreCRM.set(typeForm, { activeMenu: name })
      }
   }

   // ************ FUNCTION
   function getMenu() {
      let defaultMenu;
      const menus = Object.keys(menuBar).map(menu => {
         const itemMenu = menuBar[menu]
         if (itemMenu.default) {
            // default tab theo khai báo
            defaultMenu = itemMenu.name
         }
         return itemMenu
      })
      // get activeMenu in memory
      const tempActiveMenu = memoryStoreCRM.get(typeForm)
      defaultMenu = tempActiveMenu && tempActiveMenu.activeMenu ? tempActiveMenu.activeMenu : defaultMenu
      return {
         menus: menus,
         defaultMenu: defaultMenu
      }
   }

   return <ContextMenu.Provider
      value={{
         setLoadingForm: (isLoad) => setLoadingForm(isLoad),
         loadingForm: loadingForm,
      }}
   >
      <Segment>
         <Menu pointing secondary>
            {memoizeMenu.menus.map(menu => {
               const { visible, ...restMenu } = menu
               return visible ? <Menu.Item key={menu.name}
                  {...restMenu}
                  active={activeMenu === menu.name}
                  onClick={handleChangeTab}
               /> : undefined
            })}
         </Menu>

         <ContainerScroll
            id={idElementScroll} isSegment={false}
            idElementContainer={idElementContainer}
            heightSub={28}
         >
            {children({ activeMenu: activeMenu })}
            <ZenLoading loading={loadingForm} inline="centered" />
         </ContainerScroll>
      </Segment>
   </ContextMenu.Provider>
}

const TabTTC = ({ data, keyData = "id", type }) => {
   const { setLoadingForm, loadingForm } = useContext(ContextMenu)
   const _isMounted = useRef(true);
   const [err, setErr] = useState()
   const [items, setItems] = useState([])
   const [itemData, setItemData] = useState()
   const [infoModal, setInfoModal] = useState({})
   const [readonly, setReadonly] = useState(true);
   const refFormik = useRef()
   const [dataOriginal, setDataOriginal] = useState();
   const [error, setError] = useState();
   const [btnLoading, setBtnLoading] = useState();

   useEffect(() => {
      //  if(data?.anh_dai_dien) {delete data.anh_dai_dien}
      //  const keys = data ? Object.keys(data) : []
      //  const keynames = keys.filter((key) => { return key.toLowerCase().indexOf("ten_") !== -1; })
      //  if (keynames.length > 0) {
      //    keynames.forEach(keyname => {
      //      if (data.hasOwnProperty(keyname)) {
      //          if(data[keyname] === null || data[keyname] === "") {
      //          delete data[keyname]
      //          }
      //      }
      //    })
      //  }
      refFormik.current.setValues({
         ...refFormik.current.values,
         ...data,
      })
      setDataOriginal(data)
      loadingForm && setLoadingForm(false)
   }, [data])

   const handleAfterSave = (newItem) => {
      data = newItem
   }

   const handleSubmit = (params, formId) => {
      ApiCRMDeal.update(params, res => {
         if (res.status >= 200 && 204 >= res.status) {
            handleAfterSave({ ...params })
            ZenMessageToast.success();
            setReadonly(true)
         } else {
            setError(zzControlHelper.getResponseError(res))
            ZenMessageAlert.error(zzControlHelper.getResponseError(res))
         }
      })

   }

   const handleItemSelected = (option, { name }) => {
      if (name === "ma_uu_tien") {
         refFormik.current.setValues({
            ...refFormik.current.values,
            ten_uu_tien: option.ten_uu_tien,
         })
      } else if (name === "ma_giai_doan") {
         refFormik.current.setValues({
            ...refFormik.current.values,
            ten_giai_doan: option.ten_giai_doan,
         })
      }
   }

   const onRefreshForm = () => {
      refFormik.current.setValues({
         ...refFormik.current.values,
         ...dataOriginal,
      })
      setReadonly(true)
   }

   return <>
      <ZenFormik form={"hrdmttc"} ref={refFormik}
         validation={formValidationTTC}
         initItem={initItemTTC}
         onSubmit={handleSubmit}
      >
         {
            formik => {
               return <Form>
                  <ZenField required width={8} autoFocus readOnly={readonly}
                     name="ma_deal" props={formik}
                     label={'crmdeal.madeal'} defaultlabel="Mã cơ hội"
                     isCode
                  />
                  <ZenField readOnly={readonly}
                     name="ten_deal" props={formik}
                     label={'crmdeal.madeal'} defaultlabel="Tên cơ hội"
                  />
                  <ZenField readOnly={readonly}
                     name="nhu_cau" props={formik}
                     label={'crmdeal.nhucau'} defaultlabel="Sản phẩm dịch vụ có nhu cầu"
                  />
                  <ZenFieldSelectApi readOnly={readonly}
                     lookup={ZenLookup.Ma_kh} loadApi
                     name="ma_kh" formik={formik}
                     label={'crmdeal.ma_kh'} defaultlabel="Khách hàng"
                  />
                  <Form.Group widths="equal">
                     <ZenFieldNumber readOnly={readonly}
                        name="gia_tri_dk" props={formik}
                        {...IntlFormat.label(Mess_CRMDeal.GiaTriDk)}
                     />

                     <ZenFieldDate readOnly={readonly}
                        name="ngay_dong_dk" props={formik}
                        label={'crmdeal.ngay_dong_dk'} defaultlabel="Ngày đóng dự kiến"
                     />
                  </Form.Group>
                  <Form.Group widths="equal">
                     <ZenFieldSelectApi readOnly={readonly}
                        lookup={ZenLookup.Ma_uu_tien}
                        name="ma_uu_tien" formik={formik}
                        onItemSelected={handleItemSelected}
                        {...IntlFormat.label(Mess_CRMDeal.MaUuTien)}
                     />
                     <ZenFieldSelectApi readOnly={readonly}
                        lookup={ZenLookup.Ma_giai_doan}
                        name="ma_giai_doan" formik={formik}
                        onItemSelected={handleItemSelected}
                        {...IntlFormat.label(Mess_CRMDeal.MaGiaiDoan)}
                     />
                  </Form.Group>
                  <Form.Group widths="equal">
                     <ZenFieldSelectApi width={8} readOnly={readonly}
                        lookup={ZenLookup.ID_NV}
                        name="ma_nvns_owner" formik={formik}
                        label={'crmdeal.ma_nvns_owner'} defaultlabel="Nhân viên phụ trách"
                     />
                  </Form.Group>
                  <Form.Group>
                     <ZenFieldTextArea readOnly={readonly} width={16}
                        name="ghi_chu" props={formik}
                        {...IntlFormat.label(Mess_CRMDeal.GhiChu)}
                     />
                  </Form.Group>
               </Form>
            }
         }
      </ZenFormik>

      <div className="btn-form-right">
         {
            !readonly ? <>
               <ZenButton btnType="cancel"
                  size="small"
                  content="Hủy"
                  color="red"
                  onClick={() => onRefreshForm()}
               />
               <ZenButton btnType="save"
                  size="small"
                  type="submit"
                  onClick={(e) => refFormik.current.handleSubmit(e)}
                  style={{ margin: "0px" }}
                  loading={btnLoading}
                  disabled={btnLoading}
               />
            </>
               : <ZenButton style={{ margin: 0 }}
                  permission={permissions.DEALSua}
                  type={"button"}
                  size="small"
                  primary
                  icon="edit"
                  content={"Sửa thông tin chung"}
                  onClick={() => setReadonly(false)}
               />
         }
      </div>
   </>
};

const initItemTTC = {
   id: 0,
   ksd: false,
   ma_deal: "",
   ten_deal: "",
   nhu_cau: "",
   gia_tri_dk: 0,
   ma_uu_tien: "",
   ma_giai_doan: "",
   ngay_dong_dk: ZenHelper.formatDateTime(new Date(), "YYYY-MM-DD"),
   ngay_dong_tt: "",
   ma_nvns_owner: "",
   ma_ly_do_thua: "",
   ghi_chu: "",
   ma_kh: ""
}

const formValidationTTC = [
   {
      id: "ma_deal",
      validationType: "string",
      validations: [
         {
            type: "required",
            params: ["Không được bỏ trống trường này"]
         },
      ]
   },
]

const TabTTCHD = ({ data, keyData = "id", type }) => {
   const { setLoadingForm, loadingForm } = useContext(ContextMenu)
   const _isMounted = useRef(true);
   const [err, setErr] = useState()
   const [items, setItems] = useState([])
   const [itemData, setItemData] = useState()
   const [infoModal, setInfoModal] = useState({})
   const [readonly, setReadonly] = useState(true);
   const refFormik = useRef()
   const [dataOriginal, setDataOriginal] = useState();
   const [error, setError] = useState();
   const [btnLoading, setBtnLoading] = useState();

   useEffect(() => {
      //  if(data?.anh_dai_dien) {delete data.anh_dai_dien}
      //  const keys = data ? Object.keys(data) : []
      //  const keynames = keys.filter((key) => { return key.toLowerCase().indexOf("ten_") !== -1; })
      //  if (keynames.length > 0) {
      //    keynames.forEach(keyname => {
      //      if (data.hasOwnProperty(keyname)) {
      //          if(data[keyname] === null || data[keyname] === "") {
      //          delete data[keyname]
      //          }
      //      }
      //    })
      //  }
      refFormik.current.setValues({
         ...refFormik.current.values,
         ...data,
      })
      setDataOriginal(data)
      loadingForm && setLoadingForm(false)
   }, [data])

   const handleAfterSave = (newItem) => {
      data = newItem
   }

   const handleSubmit = (params, formId) => {
      ApiSIDmHd.update(params, res => {
         if (res.status >= 200 && 204 >= res.status) {
            handleAfterSave({ ...params })
            ZenMessageToast.success();
            setReadonly(true)
         } else {
            setError(zzControlHelper.getResponseError(res))
            ZenMessageAlert.error(zzControlHelper.getResponseError(res))
         }
      })

   }

   const handleItemSelected = (option, { name }) => {
      if (name === "id_nvkd") {
         refFormik.current.setFieldValue("ten_nvkd", option.ho_ten)
      } else if (name === "ma_kh") {
         refFormik.current.setFieldValue("ten_kh", option.ten_kh)
      } else if (name === "ma_nhhd") {
         refFormik.current.setFieldValue("ten_nhhd", option.ten_nhhd)
      }
   }

   const onRefreshForm = () => {
      refFormik.current.setValues({
         ...refFormik.current.values,
         ...dataOriginal,
      })
      setReadonly(true)
   }

   return <>
      <ZenFormik form={"hrdmttc"} ref={refFormik}
         validation={formValidationTTCHD}
         initItem={initItemTTCHD}
         onSubmit={handleSubmit}
      >
         {
            formik => {
               return <Form>
                  <Form.Group>
                     <ZenField required autoFocus readOnly
                        name="ma_hd" props={formik} textCase="uppercase"
                        width="7" //restrictChar={["/", "\\"]}
                        isCode
                        {...IntlFormat.label(Mess_SIDmHd.Ma)}
                     />
                     <ZenField required readOnly={readonly}
                        name="so_hd" props={formik}
                        width="6"
                        {...IntlFormat.label(Mess_SIDmHd.SoHd)}
                     />
                     <ZenFieldSelectApi readOnly={readonly}
                        lookup={{
                           ...ZenLookup.SIDmloai, format: `{ten}`,
                           onLocalWhere: (items) => {
                              return items.filter(t => t.ma_nhom === 'HOP_DONG') || []
                           },
                           where: "MA_NHOM: 'HOP_DONG'"
                        }}
                        name="loai" formik={formik}
                        width="3"
                        {...IntlFormat.label(Mess_SIDmHd.LoaiHD)}
                     />
                  </Form.Group>
                  <Form.Group>
                     <ZenField required readOnly={readonly}
                        name="ten_hd" props={formik}
                        width="16"
                        {...IntlFormat.label(Mess_SIDmHd.Ten)}
                     />

                  </Form.Group>
                  <Form.Group widths="equal">
                     <ZenFieldSelectApi loadApi width={16}
                        lookup={ZenLookup.Ma_kh}
                        name="ma_kh" formik={formik}
                        readOnly={readonly}
                        onItemSelected={handleItemSelected}
                        {...IntlFormat.label(Mess_ARDmKh.Header)}
                     />
                  </Form.Group>
                  <Form.Group widths="equal">
                     <ZenFieldSelectApi loadApi
                        lookup={ZenLookup.Ma_nhhd}
                        name="ma_nhhd" formik={formik}
                        readOnly={readonly}
                        {...IntlFormat.label(Mess_SIDmHd.NhomHD)}
                        onItemSelected={handleItemSelected}
                     />
                     <ZenFieldSelectApi loadApi
                        lookup={ZenLookup.ID_NV}
                        name="id_nvkd" formik={formik}
                        readOnly={readonly}
                        label={"SiDmHd.nvkh"} defaultlabel="Nhân viên kinh doanh"
                        onItemSelected={handleItemSelected}
                     />
                  </Form.Group>
                  <Form.Group >
                     <ZenFieldDate width="8" readOnly={readonly}
                        name="ngay_hd" props={formik}
                        {...IntlFormat.label(Mess_SIDmHd.NgayHd)}
                     />
                     <ZenFieldDate width="8" readOnly={readonly}
                        name="ngay_hl" props={formik}
                        label={"SiDmHd.ngay_hl"} defaultlabel="Ngày hiệu lực"
                     />
                     <ZenFieldDate width="8" readOnly={readonly}
                        name="ngay_hh" props={formik}
                        {...IntlFormat.label(Mess_SIDmHd.NgayHh)}
                     />
                  </Form.Group>

                  <Form.Group widths="equal">
                     <ZenFieldSelectApi
                        lookup={ZenLookup.Ma_nt}
                        name="ma_nt" formik={formik}
                        readOnly={readonly}
                        {...IntlFormat.label(Mess_SIDmHd.NgoaiTe)}
                     />
                     <ZenFieldNumber name="tien" readOnly={readonly}
                        decimalScale={0} props={formik}
                        label={"SiDmHd.tien"} defaultlabel="Giá trị"
                     />
                     <ZenFieldNumber name="tien_nt" readOnly={readonly}
                        decimalScale={2} props={formik}
                        label={"SiDmHd.tien_nt"} defaultlabel="Giá trị NT"
                     />
                  </Form.Group>

                  <ZenFieldTextArea readOnly={readonly}
                     name="noi_dung" props={formik}
                     {...IntlFormat.label(Mess_SIDmHd.NoiDung)}
                  />
               </Form>
            }
         }
      </ZenFormik>

      <div className="btn-form-right" style={{ paddingTop: "1em" }}>
         {
            !readonly ? <>
               <ZenButton btnType="cancel"
                  size="small"
                  content="Hủy"
                  color="red"
                  onClick={() => onRefreshForm()}
               />
               <ZenButton btnType="save"
                  size="small"
                  type="submit"
                  onClick={(e) => refFormik.current.handleSubmit(e)}
                  style={{ margin: "0px" }}
                  loading={btnLoading}
                  disabled={btnLoading}
               />
            </>
               : <ZenButton style={{ margin: 0 }}
                  permission={permissions.SIDmHdSua}
                  type={"button"}
                  size="small"
                  primary
                  icon="edit"
                  content={"Sửa thông tin chung"}
                  onClick={() => setReadonly(false)}
               />
         }
      </div>
   </>
};

const initItemTTCHD = {
   ma_hd: "", ten_hd: "", so_hd: "", noi_dung: "",
   ngay_hd: "", ngay_hh: "", ngay_hl: "",
   tien: "", tien_nt: "",
   loai: "", ma_nhhd: "", ma_kh: "", ma_nt: "VND",
   ordinal: 0,
   cts: [],
   ksd: false
}

const formValidationTTCHD = [
   {
      id: "ma_hd",
      validationType: "string",
      validations: [
         {
            type: "required",
            params: ["Không được bỏ trống trường này"]
         },
         {
            type: "max",
            params: [50, "Không được vượt quá 50 kí tự"]
         },
      ]
   },
   {
      id: "ten_hd",
      validationType: "string",
      validations: [
         {
            type: "required",
            params: ["Không được bỏ trống trường này"]
         },
      ]
   },
   {
      id: "so_hd",
      validationType: "string",
      validations: [
         {
            type: "required",
            params: ["Không được bỏ trống trường này"]
         },
         {
            type: "max",
            params: [50, "Không được vượt quá 12 kí tự"]
         },
      ]
   },
]

const TabNote = ({ data, keyData = "id", type, valueKey }) => {
   const { setLoadingForm, loadingForm } = useContext(ContextMenu)
   const _isMounted = useRef(true);
   const [err, setErr] = useState()
   const [items, setItems] = useState([])
   const [comment, setComment] = useState("")
   const [editItem, setEditItem] = useState()

   const memoUserInfo = useMemo(() => {
      return auth.getUserInfo();
   }, [])

   useEffect(() => {
      getItems();
      return () => {
         _isMounted.current = false
      }
   }, [])

   const handleSaveGhiChu = () => {
      if (editItem) {
         updateItem()
      } else {
         insItem()
      }
   }

   const handleAction = (e, item, mode) => {
      e.preventDefault();

      if (mode === FormMode.EDIT) {
         setEditItem({ ...item })
      } else if (mode === FormMode.DEL) {
         delItem(item)
      }
   }

   function insItem() {
      const dataIns = {
         content: comment,
         associate: {
            object_type: type,
            object_id: keyData === "id" ? data[keyData] : "",
            object_code: data[keyData],
         }
      }
      ApiCRMNote.insert(dataIns, res => {
         if (res.status === 201) {
            setItems([{...res.data.data, cuser_name: memoUserInfo.fullName}, ...items])
            setComment("")
         } else {
            ZenMessageAlert.error(ZenHelper.getResponseError(res))
         }
      })
   }

   function updateItem() {
      const patchData = [
         {
            value: editItem.content,
            path: `/content`,
            op: "replace",
            operationType: 0,
         }
      ]

      ApiCRMNote.updatePatch(editItem.id, patchData,
         res => {
            if (res.status === 200) {
               setItems(items.map(item => item.id == editItem.id ? editItem : item))
               setEditItem()
            } else {
               ZenMessageAlert.error(zzControlHelper.getResponseError(res))
            }
         })
   }

   function delItem(item) {
      ZenMessageAlert.question("Bạn có chắc muốn xóa không?")
         .then(success => {
            if (success == "1") {
               ApiCRMNote.delete(item.id, res => {
                  if (res.status === 204) {
                     setItems(items.filter(t => t.id != item.id))
                     ZenMessageToast.success();
                  } else {
                     ZenMessageAlert.error(zzControlHelper.getResponseError(res))
                  }
               })
            }
         })
   }

   function getItems() {
      const params = {
         type: type,
         oid: "",
         ocode: data[keyData]
      }
      ApiCRMNote.get(params, res => {
         if (_isMounted.current) {
            if (res.status === 200) {
               setItems(res.data.data)
            } else {
               setErr(ZenHelper.getResponseError(res))
            }
            loadingForm && setLoadingForm(false)
         }
      })
   }

   function inputComment() {
      return <Form className="ar-r-note">
         <TextArea name="content" value={comment}
            onChange={(e, { value }) => setComment(value)}
            placeholder="Nhập ghi chú..."
         />
         {
            comment.trim() && <div>
               <ZenButton btnType="save" size="mini" onClick={handleSaveGhiChu} />
               <ZenButton btnType="cancel" size="mini" content="Hủy" onClick={() => setComment("")} />
            </div>
         }
      </Form>
   }

   function inputCommentEdit(item) {
      return <Form>
         <TextArea name="content" value={editItem?.content}
            onChange={(e, { value }) => setEditItem({ ...editItem, content: value })}
            placeholder="Nhập ghi chú..."
         />
         {
            editItem && <div>
               <ZenButton btnType="save" size="mini" onClick={handleSaveGhiChu} />
               <ZenButton btnType="cancel" size="mini" content="Hủy" onClick={() => setEditItem()} />
            </div>
         }
      </Form>
   }

   return <>
      {err && <Message negative list={err} />}
      {inputComment()}

      {items.map((item, idx) => {
         return <Card key={item.id || idx} link fluid
            style={styles.hd_card}
         >
            <Card.Content>
               <p>
                  <span style={{ fontWeight: "bold" }}>Ghi chú bởi:</span>
                  {` ${item.cuser_name || "[Unknown]"} - Lúc: `} <FormatDate value={item.cdate} showTime />
               </p>

               {editItem?.id === item.id ? inputCommentEdit(item)
                  : <p style={{ whiteSpace: "pre-wrap", paddingRight: "100px" }}>
                     <span style={{ fontWeight: "bold" }}>Nội dung: </span>
                     <span>{item.content}</span>
                  </p>}

               {(editItem && editItem.id == item.id) || memoUserInfo?.userId == item.cuser && <p>
                  <a href="3" onClick={(e) => handleAction(e, item, FormMode.EDIT)}>Sửa</a>
                  <a href="3" style={{ paddingLeft: "14px" }}
                     onClick={(e) => handleAction(e, item, FormMode.DEL)}
                  >
                     Xóa
                  </a>
               </p>}
            </Card.Content>
         </Card>
      })}
   </>
}

const TabHoatDong = ({ data, keyData = "id", type }) => {
   const { setLoadingForm, loadingForm } = useContext(ContextMenu)
   const _isMounted = useRef(true);
   const [err, setErr] = useState()
   const [items, setItems] = useState([])
   const [itemData, setItemData] = useState()
   const [infoModal, setInfoModal] = useState({})

   useEffect(() => {
      const info = getModal(ZenLookup.CRMHoatDong)
      setInfoModal(info)
      getItems()
      return () => {
         _isMounted.current = false
      }
   }, [])

   // ********************** HANDLE
   const handleOpenCloseModal = (formMode, item) => {
      setInfoModal({
         ...infoModal,
         id: item ? item.id : "",
         formMode: formMode,
         open: true,
         other: {
            id: data[keyData],
            keyData: keyData,
            type: type,
         }
      })
   }

   const handleAfterSaveModal = (newItem, { mode }) => {
      if (mode === FormMode.ADD) {
         setItems(items.concat(newItem))
      } else if (mode === FormMode.EDIT) {
         setItems(items.map(t => t.id == newItem.id ? newItem : t))
      }
      // close modal
      setInfoModal({
         ...infoModal,
         id: "",
         formMode: FormMode.NON,
         open: false,
      })
   }

   const handleRemoveItem = () => {
      setItemData({ ...itemData, loadingRemove: true })

      ApiCRMHoatDong.delete(itemData.id, res => {
         if (res.status === 204) {
            setItems(items.filter(t => t.id != itemData.id))
            setItemData()
            ZenMessageToast.success()
         } else {
            setItemData()
            ZenMessageAlert.error(ZenHelper.getResponseError(res))
         }
      })
   }

   // ********************** FUNCTION
   function getItems() {
      // const spGet = {
      //    sqlCommand: "zusCRMHoatDongGetByObject",
      //    parameters: {
      //       pObject_id: data[keyData],
      //       pObject_type: type
      //    },
      // }
      // ApiStored.get(spGet, res => {
      //    if (_isMounted.current) {
      //       if (res.status === 200) {
      //          setItems(res.data.data[0])

      //       } else {
      //          setErr(ZenHelper.getResponseError(res))
      //       }
      //       loadingForm && setLoadingForm(false)
      //    }
      // })
      ApiCRMHoatDong.get({ oid: data[keyData], type: type }, res => {
         if (_isMounted.current) {
            if (res.status === 200) {
               setItems(res.data.data)

            } else {
               setErr(ZenHelper.getResponseError(res))
            }
            loadingForm && setLoadingForm(false)
         }
      })
   }

   return <>
      <ZenButton btnType="add" size="tiny" floated="right"
         onClick={() => handleOpenCloseModal(FormMode.ADD)}
      />
      <div style={{ clear: "both" }} />
      {err && <Message list={err} negative header="Error" />}

      {
         items.map((item, idx) => {
            return <Card key={item.id || idx} link fluid
               style={styles.hd_card}
            >
               <Card.Content>
                  <div style={{ float: "right" }}>
                     <ZenButton btnType="delete" visibleContent={false} size="mini"
                        onClick={() => setItemData({ id: item.id })} />
                     <ZenButton btnType="edit" visibleContent={false} size="mini" primary
                        onClick={() => handleOpenCloseModal(FormMode.EDIT, item)}
                     />
                  </div>

                  <Card.Header>
                     {item.ten_hoat_dong}
                  </Card.Header>
                  <Card.Description style={{ paddingBottom: "7px" }}>
                     {IntlFormat.text({
                        ...Mess_CRMHoatDong.UserThucHien, values: {
                           user: item.cuser || "[Unknown]",
                           time: <FormatDate value={item.thoi_gian} showTime />
                        }
                     })}
                  </Card.Description>

                  <Card.Description style={{ color: "black" }}>
                     <p>
                        <strong>
                           {IntlFormat.text(Mess_CRMHoatDong.KetQua)}
                        </strong>
                        {`: ${item.ket_qua}`}
                     </p>

                     <div className="ztb-textarea">
                        <p><strong>{IntlFormat.text(Mess_CRMHoatDong.MoTa)}:</strong></p>
                        {item.noi_dung}
                     </div>
                  </Card.Description>
               </Card.Content>
            </Card>
         })
      }

      {itemData &&
         <ConfirmDelete open loading={itemData.loadingRemove}
            onCancel={() => setItemData()} onConfirm={handleRemoveItem} />}

      {zzControlHelper.openModalComponent(infoModal,
         {
            onClose: () => handleOpenCloseModal(FormMode.NON),
            onAfterSave: handleAfterSaveModal,
         }
      )}
   </>
}

const TabDeal = ({ data, keyData = "id", type }) => {
   const { setLoadingForm, loadingForm } = useContext(ContextMenu)
   const _isMounted = useRef(true);
   const [err, setErr] = useState()
   const [items, setItems] = useState([])
   const [infoModal, setInfoModal] = useState({})

   useEffect(() => {
      const info = getModal(ZenLookup.CRMDeal)
      setInfoModal(info)
      getItems()
      return () => {
         _isMounted.current = false
      }
   }, [])

   // ********************** HANDLE
   const handleOpenCloseModal = (formMode, item) => {
      setInfoModal({
         ...infoModal,
         id: item ? item.id : "",
         formMode: formMode,
         open: true,
         other: {
            id: data[keyData],
            keyData: keyData,
            type: type,
         }
      })
   }

   const handleAfterSaveModal = (newItem, { mode }) => {
      if (mode === FormMode.ADD) {
         setItems(items.concat(newItem))
      } else if (mode === FormMode.EDIT) {
         setItems(items.map(t => t.id == newItem.id ? newItem : t))
      }
      // close modal
      setInfoModal({
         ...infoModal,
         id: "",
         formMode: FormMode.NON,
         open: false,
      })
   }

   const handleDeleteItem = (item) => {
      ZenMessageAlert.question("Bạn có chắc muốn xóa không?")
         .then(success => {
            if (success == 1) {
               if (type === typeDetailPage.MA_KH) {
                  ApiCRMDeal.deleteArDmKhOfDeal(item.id, data[keyData], res => {
                     if (res.status >= 200 && res.status <= 204) {
                        setItems(items.filter(t => t.id != item.id))
                        ZenMessageToast.success()
                     } else {
                        ZenMessageAlert.error(res)
                     }
                  })
               }
            }
         })
   }

   // ********************** FUNCTION
   function getItems() {
      ApiArDmKh.getDealsByKH(data[keyData], res => {
         if (_isMounted.current) {
            if (res.status === 200) {
               setItems(res.data.data)
            } else {
               setErr(ZenHelper.getResponseError(res))
            }
            loadingForm && setLoadingForm(false)
         }
      })
   }

   function renderList() {
      return <Table compact selectable striped
         className="crm-sticky-table"
      >
         <Table.Header>
            <Table.Row>
               <Table.HeaderCell content={IntlFormat.text(Mess_CRMDeal.TenDeal)} />
               <Table.HeaderCell content={IntlFormat.text(Mess_CRMDeal.MaGiaiDoan)} />
               <Table.HeaderCell content={IntlFormat.text(Mess_CRMDeal.MaUuTien)} />
               <Table.HeaderCell content={IntlFormat.text(Mess_CRMDeal.GiaTriDk)} />
               <Table.HeaderCell content={IntlFormat.text(Mess_CRMDeal.NgayDongDk)} />
               <Table.HeaderCell content="" />
            </Table.Row>
         </Table.Header>
         <Table.Body>
            {
               items.map(item => {
                  return <Table.Row key={item.id}
                     onDoubleClick={() => handleOpenCloseModal(FormMode.VIEW, item)}
                  >
                     <Table.Cell>
                        <ZenLink style={{ fontWeight: "bold" }}
                           to={routes.CRMDealDetail(zzControlHelper.btoaUTF8(item.id))}
                        >
                           {item.ten_deal}
                        </ZenLink>
                     </Table.Cell>
                     <Table.Cell content={item.ten_giai_doan} />
                     <Table.Cell content={item.ten_uu_tien} />
                     <Table.Cell content={<FormatNumber value={item.gia_tri_dk} />} textAlign="right" />
                     <Table.Cell content={<FormatDate value={item.ngay_dong_dk} />} />
                     <Table.Cell collapsing>
                        <Icon name="pencil" link
                           onClick={() => handleOpenCloseModal(FormMode.EDIT, item)}
                        />|
                        <Icon name="trash" link
                           onClick={() => handleDeleteItem(item)} />
                     </Table.Cell>
                  </Table.Row>
               })
            }
         </Table.Body>
      </Table>
   }

   return <>
      <ZenButton btnType="add" size="tiny" floated="right"
         onClick={() => handleOpenCloseModal(FormMode.ADD)}
      />
      <div style={{ clear: "both" }} />

      {err && <Message list={err} negative header="Error" />}

      {renderList()}

      {zzControlHelper.openModalComponent(infoModal,
         {
            onClose: () => handleOpenCloseModal(FormMode.NON),
            onAfterSave: handleAfterSaveModal,
         }
      )}
   </>
}

const TabHopDong = ({ data, keyData = "id", type }) => {
   const { setLoadingForm, loadingForm } = useContext(ContextMenu);
   const _isMounted = useRef(true);
   const [err, setErr] = useState();
   const [items, setItems] = useState([]);
   const [infoModal, setInfoModal] = useState({});

   useEffect(() => {
      const info = getModal(ZenLookup.Ma_hd)
      setInfoModal(info)
      getItems()

      return () => {
         _isMounted.current = false
      }
   }, [])

   // ********************** HANDLE
   const handleOpenCloseModal = (formMode, item) => {
      setInfoModal({
         ...infoModal,
         info: {
            ...infoModal.info,
            initItem: { ...infoModal.info.initItem, [keyData]: data[keyData] },
         },
         id: item ? item["ma_hd"] : "",
         formMode: formMode,
         open: true,
         other: {
            id: data[keyData],
            keyData: keyData,
            type: type,
         }
      })
   }

   const handleAfterSaveModal = (newItem, { mode }) => {
      // change
      if (mode === FormMode.ADD) {
         setItems(items.concat(newItem))
      } else if (mode === FormMode.EDIT) {
         setItems(items.map(t => t["ma_hd"] === newItem["ma_hd"] ? newItem : t))
      }
      // close modal
      setInfoModal({
         ...infoModal,
         id: "",
         formMode: FormMode.NON,
         open: false,
      })
   }

   const handleDeleteItem = (item) => {
      ZenMessageAlert.question("Bạn có chắc muốn xóa không?")
         .then(success => {
            if (success == 1) {
               if (type === typeDetailPage.MA_KH) {
                  const patchData = [
                     {
                        value: "",
                        path: `/ma_kh`,
                        op: "replace",
                        operationType: 0,
                     }
                  ]
                  ApiSIDmHd.updatePatch(item.ma_hd, patchData, res => {
                     if (res.status >= 200 && res.status <= 204) {
                        setItems(items.filter(t => t.ma_hd != item.ma_hd))
                        ZenMessageToast.success()
                     } else {
                        ZenMessageAlert.error(res)
                     }
                  })
               }
            }
         })
   }

   // ********************** FUNCTION
   function getItems() {
      ApiSIDmHd.get(res => {
         if (_isMounted.current) {
            if (res.status === 200) {
               setItems(res.data.data)
            } else {
               setErr(ZenHelper.getResponseError(res))
            }
            loadingForm && setLoadingForm(false)
         }
      }, { qf: `a.${[keyData]} = '${data[keyData]}'` })
   }

   function renderList() {
      return <Table compact selectable striped
         className="crm-sticky-table"
      >
         <Table.Header>
            <Table.Row>
               <Table.HeaderCell content={IntlFormat.text(Mess_SIDmHd.Ma)} />
               <Table.HeaderCell content={IntlFormat.text(Mess_SIDmHd.Ten)} />
               <Table.HeaderCell content={IntlFormat.text(Mess_SIDmHd.SoHd)} />
               <Table.HeaderCell content={IntlFormat.text(Mess_SIDmHd.GiaTriHD)} />
               <Table.HeaderCell content={IntlFormat.text(Mess_SIDmHd.NgayHd)} />
               <Table.HeaderCell content="" />
            </Table.Row>
         </Table.Header>
         <Table.Body>
            {
               items.map(item => {
                  return <Table.Row key={item["ma_hd"]}
                     onDoubleClick={() => handleOpenCloseModal(FormMode.VIEW, item)}
                  >
                     <Table.Cell>
                        <ZenLink style={{ fontWeight: "bold" }}
                           to={routes.SIDmHdDetail(zzControlHelper.btoaUTF8(item.ma_hd))}
                        >
                           {item.ma_hd}
                        </ZenLink>
                     </Table.Cell>
                     <Table.Cell content={item.ten_hd} />
                     <Table.Cell content={item.so_hd} />
                     <Table.Cell content={<FormatNumber value={item.tien} />} textAlign="right" />
                     <Table.Cell content={<FormatDate value={item.ngay_hd} />} />
                     <Table.Cell collapsing>
                        <Icon name="pencil" link
                           onClick={() => handleOpenCloseModal(FormMode.EDIT, item)}
                        />|
                        <Icon name="trash" link
                           onClick={() => handleDeleteItem(item)} />
                     </Table.Cell>
                  </Table.Row>
               })
            }
         </Table.Body>
      </Table>
   }

   return <>
      <ZenButton btnType="add" size="tiny" floated="right"
         onClick={() => handleOpenCloseModal(FormMode.ADD)}
      />
      <div style={{ clear: "both" }} />

      {err && <Message list={err} negative header="Error" />}

      {renderList()}

      {zzControlHelper.openModalComponent(infoModal,
         {
            onClose: () => handleOpenCloseModal(FormMode.NON),
            onAfterSave: handleAfterSaveModal,
         }
      )}
   </>
}

const TabHHDV = ({ data, keyData = "id", type }) => {
   const [permission, setPermission] = useState(false)
   const [btnLoading, setBtnLoading] = useState(false)
   const [dataOriginal, setDataOriginal] = useState();
   const refFormik = useRef()
   const [dataShow, setDataShow] = useState({ ...data })
   const { setLoadingForm } = useContext(ContextMenu)
   useEffect(() => {
      setLoadingForm(false)
      setDataOriginal(data)
   }, [])

   const switchAction = (propsElement, type, itemCurrent, index) => {
      const { name, value, itemSelected } = propsElement;
      const { cts } = refFormik.current.values;

      switch (type) {
         case ActionType.TEXT_CHANGE:
            const { lookup } = propsElement;
            // set value
            itemCurrent[name] = value;
            let calcPH = {};

            if (lookup) {
               if (name === "ma_vt") {
                  itemCurrent["ten_vt"] = itemSelected["ten_vt"];
                  itemCurrent["dvt"] = itemSelected["dvt"];
               }
            }
            cts[index] = { ...itemCurrent };
            refFormik.current.setValues({
               ...refFormik.current.values,
               cts: cts,
            })
            setDataShow({ ...refFormik.current.values })
            break;

         case ActionType.NUMBER_CHANGE:
            f_calcNumber(propsElement, type, itemCurrent, index);
            break;

         case ActionType.ADD_ROW:
            let newCT = {
               ma_vt: value,
               ten_vt: itemSelected.ten_vt,
               dvt: itemSelected.dvt,
               tk_vt: itemSelected.tk_vt,
               ts_gtgt: itemSelected.ts_gtgt,

               so_luong: 0,
               gia: 0,
               gia2: 0,
               gia_nt: 0,
               tien: 0,
               tien2: 0,
               tien_nt: 0,
               thue_gtgt_nt: 0,
               thue_gtgt: 0,
            };
            if (itemSelected.ma_kho) {
               newCT.ma_kho = itemSelected.ma_kho;
            }
            if (!cts) {
               cts = [].push(newCT)
            }
            cts.push(newCT)
            refFormik.current.setValues({
               ...refFormik.current.values,
               cts: cts,
            })
            setDataShow({ ...refFormik.current.values })
            break;

         case ActionType.DELETE_ROW:
            // xóa dòng CT, tính lại PH
            let ctDel = cts.filter((item, idx) => idx !== index);
            const totalPH = {
               t_so_luong: VoucherHelper.f_SumArray(ctDel, "so_luong"),
               t_tien0: VoucherHelper.f_SumArray(ctDel, "tien0"),
               t_tien_nt0: VoucherHelper.f_SumArray(ctDel, "tien_nt0"),
               t_tien: VoucherHelper.f_SumArray(ctDel, "tien"),
               t_tien_nt: VoucherHelper.f_SumArray(ctDel, "tien_nt"),
               t_cp: VoucherHelper.f_SumArray(ctDel, "cp"),
               t_cp_nt: VoucherHelper.f_SumArray(ctDel, "cp_nt"),
               t_tt: VoucherHelper.f_SumArray(ctDel, "tien"),
               t_tt_nt: VoucherHelper.f_SumArray(ctDel, "tien_nt"),
            };
            const newcts = cts.filter((t, indx) => indx !== index)
            //formik.setFieldValue('cts', newcts)
            refFormik.current.setValues({
               ...refFormik.current.values,
               ...totalPH,
               cts: newcts,
            })
            setDataShow({ ...refFormik.current.values, cts: newcts })
            break;
         default:
            console.log(type);
            break;
      }
   };

   function f_calcNumber(propsElement, type, itemCurrent, index) {
      const { cts } = refFormik.current.values
      const { name, value } = propsElement
      if (name === 'so_luong') {
         itemCurrent.so_luong = value ? value : 0
         changeSoLuong(itemCurrent)
      } else if (name === 'gia2') {
         itemCurrent.gia2 = value ? value : 0
         changeGia2(itemCurrent)
      } else if (name === 'gia_nt2') {
         changeGia_nt2(itemCurrent)
      } else if (name === 'tien2') {
         changeTien2(itemCurrent)
      } else if (name === 'tien_nt2') {
         changeTien_nt2(itemCurrent)
      } else if (name === 'ts_gtgt' || name === "ma_vt") {
         itemCurrent.ts_gtgt = value ? value : 0
         changeMa_thue(itemCurrent)
      } else if (name === 'thue_gtgt') {
         changeThue_gtgt(itemCurrent)
      } else if (name === 'thue_gtgt_nt') {
         changeThue_gtgt_nt(itemCurrent)
      } else if (name === 'tl_ck') {
         changeTl_ck(itemCurrent)
      } else if (name === 'tien_ck') {
         changeTien_ck(itemCurrent)
      } else if (name === 'tien_ck_nt') {
         changeTien_ck_nt(itemCurrent)
      } else if (name === 'gia') {
         changeGia(itemCurrent, value)
      } else if (name === 'gia_nt') {
         changeGia_nt(itemCurrent)
      }
      //cập nhật PH, CT
      VoucherHelper.f_Timeout(() => {
         const totalPH = calcTotalPH(itemCurrent, index)
         cts[index] = { ...itemCurrent };
         //formik.setFieldValue('cts',cts);
         refFormik.current.setValues({
            ...refFormik.current.values,
            ...totalPH,
            cts: cts,
         })
         refFormik.current.setValues({
            ...refFormik.current.values,
            ...totalPH,
            cts: cts,
         })
         setDataShow({ ...refFormik.current.values })
      })
   }

   // =============================== VALUE CHANGE
   function changeSoLuong(item) {
      f_calcTien2(item)
      f_calcTien_Ck(item)
      f_calcThue(item)
      f_calcTT(item)
      f_calcTien(item) // tiền vốn
   }
   function changeGia2(item) {
      changeSoLuong(item)
   }
   // function changeGia_nt2(item) {
   //    item.gia2 = Math.round(item.gia_nt2 * ph.ty_gia)
   //    changeSoLuong(item)
   // }
   function changeTien2(item) {
      item.tien_nt2 = item.tien2
      f_calcThue(item)
      f_calcTien_Ck(item)
      f_calcTT(item)
   }

   function changeTl_ck(item) {
      f_calcTien_Ck(item)
      f_calcThue(item)
      f_calcTT(item)
   }
   function changeTien_ck(item) {
      item.tien_ck_nt = item.tien_ck

      f_calcThue(item)
      f_calcTT(item)
   }

   function changeMa_thue(item) {
      f_calcThue(item)
      f_calcTT(item)
   }
   function changeThue_gtgt(item) {
      //item.thue_gtgt = Math.round(item.thue_gtgt / ph.ty_gia, ph.ma_nt)
      f_calcTT(item)
   }

   function changeGia(item) {
      item.gia_nt = item.gia

      changeSoLuong(item)
   }

   // ==================================== tính toán
   function f_calcTien2(item) {
      item.tien2 = Math.round(item.so_luong * item.gia2)

   }
   function f_calcTien_Ck(item) {
      item.tien_ck = item.tien_ck_nt

   }
   function f_calcThue(item) {
      item.thue_gtgt = item.tien2 * item.ts_gtgt / 100
   }
   function f_calcTT(item) {
      item.tt = item.tien_nt2 + item.thue_gtgt_nt - item.tien_ck_nt
   }
   function f_calcTien(item) {
      item.tien = item.tien_nt

   }
   function calcTotalPH(itemCurrent, index) {
      const { cts } = refFormik.current.values
      // tạo CT mới
      const newCT = cts.map((item, idx) => idx === index ? itemCurrent : item)
      // tính tổng PH
      const totalPH = {
         t_so_luong: VoucherHelper.f_SumArray(newCT, 'so_luong'),
         t_tien2: VoucherHelper.f_SumArray(newCT, 'tien2'),
         t_tien_nt2: VoucherHelper.f_SumArray(newCT, 'tien_nt2'),
         t_thue: VoucherHelper.f_SumArray(newCT, 'thue_gtgt'),
         t_thue_nt: VoucherHelper.f_SumArray(newCT, 'thue_gtgt_nt'),
         t_ck: VoucherHelper.f_SumArray(newCT, 'tien_ck'),
         t_ck_nt: VoucherHelper.f_SumArray(newCT, 'tien_ck_nt'),
         t_tt: 0,
         //t_tt_nt: 0
      }

      totalPH.t_tt = totalPH.t_tien2 + totalPH.t_thue - totalPH.t_ck
      //totalPH.t_tt_nt = totalPH.t_tien_nt2 + totalPH.t_thue_nt - totalPH.t_ck_nt
      return totalPH
   }

   const onChangeVT = (propsElement, item, index) => {
      const { cts } = refFormik.current.values
      const { options, name, value } = propsElement
      let option;
      if (propsElement.option) {
         option = propsElement.option
      } else {
         option = options.filter(t => t.ma_vt == value)[0]
      }
      item[name] = value;
      item["ten_vt"] = option["ten_vt"];
      item["dvt"] = option["dvt"];
      item["ts_gtgt"] = option["ts_gtgt"]
      cts[index] = { ...item };
      refFormik.current.setValues({
         ...refFormik.current.values,
         cts: cts,
      })
      setDataShow({ ...refFormik.current.values })
   }

   const onChangetext = (propsElement, item, index) => {
      const { cts } = refFormik.current.values
      const { name, value } = propsElement
      item[name] = value;
      cts[index] = { ...item };
      refFormik.current.setValues({
         ...refFormik.current.values,
         cts: cts,
      })
      setDataShow({ ...refFormik.current.values })
   }

   const addRow = (propsElement) => {
      const { cts } = refFormik.current.values
      const { options, name, value } = propsElement
      let option;
      if (propsElement.option) {
         option = propsElement.option
      } else {
         option = options.filter(t => t.ma_vt == value)[0]
      }
      let newCT = ({
         ma_hd: data.ma_hd,
         ma_vt: value,
         ten_vt: option.ten_vt,
         dvt: option.dvt,
         ten_dvt: "",
         so_luong: 0,
         gia2: 0,
         tien2: 0,
         ts_gtgt: option.ts_gtgt,
         thue: 0,
         tt: 0,
         ghi_chu: ""
      })
      if (cts) {
         cts.push(newCT)
         refFormik.current.setValues({
            ...refFormik.current.values,
            cts: cts,
         })
         setDataShow({ ...refFormik.current.values })
      } else {
         refFormik.current.setValues({
            ...refFormik.current.values,
            cts: [newCT],
         })
         setDataShow({ ...dataShow, cts: [newCT] })
      }
   }

   const onRefreshForm = () => {
      data = {
         ...dataOriginal,
      }
      refFormik.current.setValues({
         ...refFormik.current.values,
         ...dataOriginal,
      })
      setPermission(false)
   }

   const handleSubmit = (param) => {
      ApiSIDmHd.update(dataShow, res => {
         if (res.status >= 200 && res.status <= 204) {
            ZenMessageToast.success()
         } else {
            ZenMessageAlert.warning(zzControlHelper.getResponseError(res))
         }
      })
      setPermission(false)
   }

   return <>
      <div style={{ minHeight: "490px" }}>
         <TableScroll>
            <Table.Header>
               <RowHeaderCell colAction={!permission}
                  header={[
                     { text: ['so3.ma_vt', 'Mã HH/DV'], },
                     { text: ['so3.ten_vt', 'Tên HH/DV'] },
                     { text: ['so3.dvt', 'ĐVT'] },
                     { text: ['so3.so_luong', 'Số lượng'] },
                     { text: ['so3.gia_nt2', 'Đơn giá'] },
                     { text: ['so3.tien_nt2', 'Thành tiền'] },
                     { text: ['so3.ts_gtgt', '% VAT'] },
                     { text: ['so3.ghi_chu', 'Ghi chú'] },
                  ]}
               />
            </Table.Header>
            <Table.Body>
               <ZenFormik form={"hrdmttc"} ref={refFormik}
                  validation={formValidationTTC}
                  initItem={data}
                  onSubmit={handleSubmit}

               >
                  {
                     formik => { }
                  }
               </ZenFormik>
               {dataShow?.cts && dataShow?.cts.map((item, index) => {

                  return <Table.Row key={index}>
                     <Table.Cell>
                        <ZenFieldSelectApi
                           rowItem={item} index={index}
                           style={{ margin: '0', width: "200px" }}
                           readOnly={!permission}
                           lookup={{ ...ZenLookup.Ma_vt }}
                           name='ma_vt'
                           clearable
                           value={item.ma_vt} index={index}
                           onChange={(e, propsElement) => onChangeVT(propsElement, item, index)}
                        />
                     </Table.Cell>

                     <TextCell widthCell={1}
                        name="ten_vt" rowItem={item} index={index}
                        placeholder={['so0.ma_vt', 'Tên hàng hóa dịch vụ']}
                        readOnly={!permission}
                        onChange={switchAction}
                     //autoFocus={(item.autoFocus && .length === index + 1) ? true : false}
                     />
                     <TableCell>
                        <ZenFieldSelectApi
                           rowItem={item} index={index}
                           style={{ margin: '0', width: "200px" }}
                           readOnly={!permission}
                           lookup={{ ...ZenLookup.Ma_dvt }}
                           name='dvt'
                           clearable
                           value={item.dvt} index={index}
                           onChange={(e, propsElement) => onChangetext(propsElement, item, index)}
                        />
                     </TableCell>

                     <NumberCell name="so_luong"
                        rowItem={item} index={index}
                        placeholder={["so3.so_luong", "Số lượng"]}
                        onChange={switchAction}
                        readOnly={!permission}
                     />

                     <NumberCell name="gia2"
                        rowItem={item} index={index}
                        placeholder={["so3.gia", "Giá", { ma_nt: 'VND' }]}
                        onChange={switchAction}
                        readOnly={!permission}
                     />

                     <NumberCell name="tien2"
                        rowItem={item} index={index}
                        placeholder={["so3.tien", "thành tiền", { ma_nt: 'VND' }]}
                        onChange={switchAction}
                        readOnly={!permission}
                     />
                     <TableCell>
                        <ZenFieldSelectApi
                           rowItem={item} index={index}
                           readOnly={!permission}
                           lookup={{ ...ZenLookup.Ma_thue }}
                           style={{ margin: '0', width: "100px" }}
                           name='ts_gtgt'
                           clearable
                           value={item.ts_gtgt} index={index}
                           onChange={(e, propsElement) => switchAction(propsElement, 'number_change', item, index)}
                        />
                     </TableCell>
                     <TextCell name="ghi_chu"
                        rowItem={item} index={index}
                        placeholder={["so3.ghi_chu", "Ghi chú", { ma_nt: 'VND' }]}
                        onChange={switchAction}
                        readOnly={!permission}
                     />
                     <NumberCell name="thue_gtgt"
                        rowItem={item} index={index}
                        placeholder={["so3.thue_gtgt", "VAT VND", { ma_nt: 'VND' }]}
                        onChange={switchAction}
                        readOnly={!permission}
                     />

                     {!permission === false && <DeleteCell collapsing
                        rowItem={item} index={index}
                        onClick={switchAction}
                     />}
                  </Table.Row>
               })
               }
               {!permission === false && <Table.Row>
                  <Table.Cell>
                     <ZenFieldSelectApi name="addRow"
                        lookup={ZenLookup.Ma_vt}
                        placeholder={'Mã VT'}
                        onChange={(e, propsElement) => addRow(propsElement)}
                        value=""
                     />
                  </Table.Cell>
                  <Table.Cell colSpan={12} />
                  <Table.Cell colSpan={24} />
               </Table.Row>}
            </Table.Body>
         </TableScroll>
         <div style={{
            paddingTop: "30px"
         }}>
            <Grid columns="2">
               <Grid.Column width="8" >
                  <div className="btn-form-right" style={{ position: "absolute", bottom: "1em" }}>
                     {
                        permission ? <>
                           <ZenButton btnType="cancel"
                              size="small"
                              content="Hủy"
                              color="red"
                              onClick={() => onRefreshForm()}
                           />
                           <ZenButton btnType="save"
                              size="small"
                              type="submit"
                              onClick={(e) => { handleSubmit() }}
                              style={{ margin: "0px" }}
                              loading={btnLoading}
                              disabled={btnLoading}
                           />
                        </>
                           : <Button style={{ margin: 0 }}
                              type={"button"}
                              size="small"
                              primary
                              icon="edit"
                              content={"Sửa thông tin chung"}
                              onClick={() => setPermission(true)}
                           />
                     }
                  </div>
               </Grid.Column>
               <Grid.Column width="8" textAlign="right">
                  <TableTotalPH maNt={dataShow.ma_nt}>
                     <RowTotalPH text="Tổng số lượng" value={dataShow.t_so_luong} maNt={dataShow.ma_nt} oneCol />
                     <RowTotalPH text="Tổng tiền" value={dataShow.t_tien2} valueNT={dataShow.t_tien_nt2} maNt={dataShow.ma_nt} />
                     <RowTotalPH text="Thuế GTGT" value={dataShow.t_thue} valueNT={dataShow.t_thue_nt} maNt={dataShow.ma_nt} />
                     <RowTotalPH text="Tổng thanh toán" value={dataShow.t_tt} valueNT={dataShow.t_tt_nt} maNt={dataShow.ma_nt} />
                  </TableTotalPH>
               </Grid.Column>
            </Grid>
         </div>
      </div>
   </>
}

const TabTraoDoi = (props) => {
   const { type, data, keyData } = props
   const { setLoadingForm, loadingForm } = useContext(ContextMenu);

   useEffect(() => {
      setLoadingForm(false)
   }, [])
   return <DiscussComment codeName="CRM" type={type} keyValue={data['id']} />
}

const TabFileAttachment = ({ data, keyData = "id", fileCodeName }) => {
   const { setLoadingForm } = useContext(ContextMenu)
   const refFileAttachment = useRef();

   useEffect(() => {
      setLoadingForm(false)
   }, [])

   return <>
      <ZenButton btnType="add" size="tiny" floated="right"
         onClick={() => refFileAttachment.current.openModal()}
      />
      <div style={{ clear: "both" }} />
      <FileAttachment codeName={fileCodeName}
         docKey={data[keyData]}
         ref={refFileAttachment}
         tableProps={{ className: "crm-sticky-table" }}
      />
   </>
}

const TabThuTien = ({ data, keyData = "id", type, valueKey }) => {
   const { setLoadingForm, loadingForm } = useContext(ContextMenu)
   const _isMounted = useRef(true);
   const [err, setErr] = useState()
   const [items, setItems] = useState([])
   const [itemData, setItemData] = useState()
   const [openThutien, setOpenThutien] = useState(false)
   const [openDoanhthu, setOpenDoanhthu] = useState(false)
   const [index, setIndex] = useState()
   const [item, setItem] = useState([])

   useEffect(() => {
      getItems()
      return () => {
         _isMounted.current = false
      }
   }, [])

   const openModal = (item, index) => {
      if (item.ma_ct == "CA1") {
         setOpenThutien(true)
         if (item) {
            setItem(item)
         } else {
            setItem()
         }
         if (index) setIndex(index)
      }

      if (item.ma_ct == "SO3") {
         setOpenDoanhthu(true)
         if (item) {
            setItem(item)
         } else {
            setItem()
         }
         if (index) setIndex(index)
      }
   }

   const handleAfterSave = (newItem, id) => {
      if (id) {
         items[index] = newItem
         setItems([].concat(items))
      } else {
         items.push(newItem)
         setItems([].concat(items))
      }
      if (newItem.ma_ct == "CA1") setOpenThutien(false)
      if (newItem.ma_ct == "SO3") setOpenDoanhthu(false)
   }

   const handleDeleteItem = (item, index) => {
      ApiSIDmHd.deletethutien({stt_rec: item.stt_rec, ma_ct: item.ma_ct}, res => {
         if (res.status >= 200 && res.status <= 204) {
            setItems(items.filter(t => t.stt_rec !== item.stt_rec))
            ZenMessageToast.success()
         } else {

            ZenMessageAlert.error(ZenHelper.getResponseError(res))
         }
      })
   }

   // ********************** FUNCTION
   function getItems() {
      ApiSIDmHd.getthutien(data[keyData], res => {
         if (_isMounted.current) {
            if (res.status === 200) {
               setItems(res.data.data)
            } else {
               setErr(ZenHelper.getResponseError(res))
            }
            loadingForm && setLoadingForm(false)
         }
      })
   }

   function sumArray(items, name) {
      let sum = 0;
      items.forEach(item => {
         sum += item[name]
      })
      return sum
   }

   return <>
      <Button icon="add" size="tiny" floated="right"
         primary
         content="Thêm phiếu thu tiền"
         onClick={() => openModal(initItemThutien)}
      />
      <Button icon="add" size="tiny" floated="right"
         primary
         content="Ghi nhận doanh thu"
         onClick={() => openModal(initItemDoanhthu)}
      />
      <div style={{ clear: "both" }} />
      {err && <Message list={err} negative header="Error" />}

      <Table celled compact selectable striped
         className="crm-sticky-table"
      >
         <Table.Header>
            <Table.Row>
               <Table.HeaderCell content={"Ngày"} />
               <Table.HeaderCell content={"Số hóa đơn"} />
               <Table.HeaderCell content={"Tài khoản"} />
               <Table.HeaderCell content={"Diễn giải"} />
               <Table.HeaderCell content={"Doanh thu"} textAlign={'right'}/>
               <Table.HeaderCell content={"Số tiền đã thu"} textAlign={'right'}/>
               <Table.HeaderCell content="" />
            </Table.Row>
         </Table.Header>
         <Table.Body>
            {

               items.map((item, idx) => {
                  return <Table.Row key={item.stt_rec || `stt_rec${idx}`}
                  //onDoubleClick={() => handleOpenCloseModal(FormMode.VIEW, item)}
                  >
                     <Table.Cell content={<FormatDate value={item.ngay_ct} />} />
                     <Table.Cell content={item.so_ct} />
                     <Table.Cell content={item.tk} />
                     <Table.Cell content={item.dien_giai} />
                     <Table.Cell textAlign={'right'}><FormatNumber isZero={false} value={item.t_tien2} /></Table.Cell>
                     <Table.Cell textAlign={'right'}><FormatNumber isZero={false} value={item.ps_no} /></Table.Cell>
                     <Table.Cell collapsing>
                        <Button.Group size="tiny" style={{ float: "right" }}>
                           <ButtonEdit
                              onClick={() => openModal(item, idx)}
                           />
                           <ButtonDelete
                              onClick={() => handleDeleteItem(item, idx)}
                           />
                        </Button.Group>
                     </Table.Cell>
                  </Table.Row>
               })
            }
         </Table.Body>
      </Table>
      <Grid>
         <Grid.Column width="8" > </Grid.Column>
         <Grid.Column width="8" textAlign="right">
            <TableTotalPH >
               <RowTotalPH text="Tổng doanh thu" value={sumArray(items, 't_tien2')} oneCol />
               <RowTotalPH text="Tổng đã thu" value={sumArray(items, 'ps_no')} />
               <RowTotalPH text="Còn lại" value={sumArray(items, 't_tien2') - sumArray(items, 'ps_no')} />
            </TableTotalPH>
         </Grid.Column>
      </Grid>
      {
         openThutien && <ModalThuTien open={openThutien}
            sttrec={item ? item?.stt_rec : null}
            item={item}
            mahd={data[keyData]}
            onClose={() => setOpenThutien(false)}
            onAfterSave={handleAfterSave}
         />
      }

      {
         openDoanhthu && <ModalDoanhthu open={openDoanhthu}
            sttrec={item ? item?.stt_rec : null}
            item={item}
            mahd={data[keyData]}
            onClose={() => setOpenDoanhthu(false)}
            onAfterSave={handleAfterSave}
         />
      }
   </>
}

const TabHDNhanSu = ({ data, keyData = "id", type }) => {
   const { setLoadingForm, loadingForm } = useContext(ContextMenu)
   const _isMounted = useRef(true);
   const [err, setErr] = useState()
   const [items, setItems] = useState([])
   const [itemData, setItemData] = useState()
   const [open, setOpen] = useState(false)
   const [openDoanhthu, setOpenDoanhthu] = useState(false)
   const [index, setIndex] = useState()
   const [item, setItem] = useState([])

   useEffect(() => {
      getItems()
      return () => {
         _isMounted.current = false
      }
   }, [])

   const openModal = (item, index) => {
      setOpen(true)
      if (item) {
          setItem(item)
      } else {
          setItem()
      }
      if (index) setIndex(index)
   }
   
   const handleAfterSave = (newItem, id) => {
      setOpen(false)
      if (id) {
         items[index] = newItem
         setItems([].concat(items))
      } else {
         items.push(newItem)
         setItems([].concat(items))
      }
   }

   const handleDeleteItem = (item, index) => {
      ApiSIDmHd.deleteHrHsNsByMaHd(data.ma_hd, item.ma_nvns, res => {
         if (res.status >= 200 && res.status <= 204) {
            setItems(items.filter(t => t.id !== item.id))
            ZenMessageToast.success()
         } else {

            ZenMessageAlert.error(ZenHelper.getResponseError(res))
         }
      })
   }

   // ********************** FUNCTION
    function getItems() {
      ApiSIDmHd.getHrHsNsByMaHd(data.ma_hd, res => {
         if (_isMounted.current) {
            if (res.status === 200) {
               setItems(res.data.data)
            } else {
               setErr(ZenHelper.getResponseError(res))
            }
            loadingForm && setLoadingForm(false)
         }
      }, { params: {vai_tro: ""} })
   }

   return <>
      <Button icon="add" size="tiny" floated="right"
         primary
         content="Thêm mới nhân sự"
         onClick={() => openModal(initItemSIDmHdNv)}
       />

      <div style={{ clear: "both" }} />
      {err && <Message list={err} negative header="Error" />}

      <Table celled compact selectable striped className="crm-sticky-table" >
         <Table.Header>
            <Table.Row>
               <Table.HeaderCell content={"Mã nhân sự"} />
               <Table.HeaderCell content={"Tên nhân sự"} />
               <Table.HeaderCell content={"Đơn giá lương"} />
               <Table.HeaderCell content={"Ghi chú"} />
               <Table.HeaderCell content="" />
            </Table.Row>
         </Table.Header>
         <Table.Body>
            {
               items.map((item, idx) => {
                  return <Table.Row key={item.id}
                  //onDoubleClick={() => handleOpenCloseModal(FormMode.VIEW, item)}
                  >
                     <Table.Cell content={item.ma_nvns} />
                     <Table.Cell content={item.ten_nvns} />
                     <Table.Cell content={item.muc_luong} />
                     <Table.Cell content={item.ghi_chu} />
                     <Table.Cell collapsing>
                        <Button.Group size="tiny" style={{ float: "right" }}>
                           <ButtonEdit
                              onClick={() => openModal(item, idx)}
                           />
                           <ButtonDelete
                              onClick={() => handleDeleteItem(item, idx)}
                           />
                        </Button.Group>
                     </Table.Cell>
                  </Table.Row>
               })
            }
         </Table.Body>
      </Table>
      {
         open && <ModalHDNhanSu open={open}
            id={item ? item?.id : null}
            item={item}
            mahd={data[keyData]}
            onClose={() => setOpen(false)}
            onAfterSave={handleAfterSave}
         />
      }
   </>
}

const ModalThuTien = ({ open, sttrec, item, mahd, onClose, onAfterSave, title }) => {
   const _isMounted = useRef(true);
   const refFormik = useRef();
   const [error, setError] = useState();
   const [roles, setRoles] = useState([]);
   const [btnLoading, setBtnLoading] = useState();

   useEffect(() => {
      zzControlHelper.dragElement(document.getElementById('thutien-form'), 'header-thutien-form')
   })


   const handleSubmit = (params, formId) => {
      setBtnLoading(true)
      if (sttrec) {
         ApiSIDmHd.updatethutien(params, res => {
            if (res.status >= 200 && 204 >= res.status) {
               onAfterSave && onAfterSave({ ...params }, sttrec)
               ZenMessageToast.success();
            } else {
               setError(zzControlHelper.getResponseError(res))
               ZenMessageAlert.error(zzControlHelper.getResponseError(res))
            }
            setBtnLoading(false)
         })
      } else {
         ApiSIDmHd.insertthutien({ ...params, ma_hd: mahd }, res => {
            if (res.status >= 200 && 204 >= res.status) {
               onAfterSave && onAfterSave({ ...res.data.data[0], ma_hd: mahd })
               ZenMessageToast.success();
            } else {
               setError(zzControlHelper.getResponseError(res))
               ZenMessageAlert.error(zzControlHelper.getResponseError(res))
            }
            setBtnLoading(false)
         })
      }
      setBtnLoading(false)
   }

   const customHeader = (id) => {
      const header = 'Thu tiền theo hợp đồng'
      const txtHeader = !id ? `Thêm mới ${header}` : `Sửa ` + capitalize(header)
      return txtHeader
   }
   const capitalize = (s) => {
      if (typeof s !== 'string') return ''
      return s.charAt(0).toUpperCase() + s.slice(1)
   }

   return <>
      <Modal id="thutien-form"
         closeOnEscape closeIcon closeOnDimmerClick={false}
         onClose={() => onClose()}
         open={open}
         size={"tiny"}
      >
         <Modal.Header id='header-thutien-form' style={{ cursor: "grabbing" }}>
            {customHeader(sttrec)}
         </Modal.Header>

         <Modal.Content>
            <ZenFormik form={"sidmhdthutien"} ref={refFormik}
               validation={formValidationThutien}
               initItem={item || initItemThutien}
               onSubmit={handleSubmit}
            >
               {
                  formik => {
                     return <Form>
                        <Form.Group>
                           <ZenFieldDate width={8} name="ngay_ct" props={formik}
                              required
                              label="sidmhdthutien.ngay_ct" defaultlabel="Ngày thu"
                           />
                           <ZenFieldNumber width={16} name="ps_no" props={formik}
                              required
                              label="sidmhdthutien.ps_no" defaultlabel="Giá trị thu"
                           />
                        </Form.Group>
                        <Form.Group>
                           <ZenFieldSelectApi width={16} name="tk"
                              required
                              lookup={{ 
                                 ...ZenLookup.TK, 
                                 format: `{ten_tk}`,
                                 where: ` tk like '11%' và chi_tiet = 1`,
                                 onLocalWhere: ((items) => {
                                    return items.filter(t => { 
                                       return t.chi_tiet == '1' && t.tk.indexOf('11',0) == 0
                                    })
                                 }),
                              }}
                              formik={formik}
                              label="sidmthutien.tk" defaultlabel="Tài khoản"
                           />
                        </Form.Group>
                        <Form.Group>
                           <ZenField width={16} name="dien_giai" props={formik}
                              label="sidmhdthutien.dien_giai" defaultlabel="Diễn giải"
                           />
                        </Form.Group>
                     </Form>
                  }
               }
            </ZenFormik>
         </Modal.Content>
         <Modal.Actions>
            <ZenButton btnType={"cancel"} size="small" onClick={() => onClose()} />
            <ZenButton btnType="save" size="small"
               loading={btnLoading} type="submit"
               onClick={(e) => refFormik.current.handleSubmit(e)}
            />
         </Modal.Actions>
      </Modal>
   </>
}

const initItemThutien = {
   stt_rec: "",
   ngay_ct: new Date(),
   ma_hd: "",
   ma_ct: "CA1",
   dien_giai: "",
   tk: "",
   ps_no: 0,
   t_tien2: 0,
}

const formValidationThutien = [
   {
      id: "ngay_ct",
      validationType: "date",
      validations: [
         {
            type: "required",
            params: ["Không được bỏ trống trường này"]
         },
      ]
   },
   {
      id: "ps_no",
      validationType: "number",
      validations: [
         {
            type: "required",
            params: ["Không được bỏ trống trường này"]
         },
         {
            type: "min",
            params: [1, "Không được nhỏ hơn 0"]
         },
      ]
   },
   {
      id: "tk",
      validationType: "string",
      validations: [
         {
            type: "required",
            params: ["Không được bỏ trống trường này"]
         },
      ]
   },
]

const ModalDoanhthu = ({ open, sttrec, item, mahd, onClose, onAfterSave, title }) => {
   const _isMounted = useRef(true);
   const refFormik = useRef();
   const [error, setError] = useState();
   const [roles, setRoles] = useState([]);
   const [btnLoading, setBtnLoading] = useState();

   useEffect(() => {
      zzControlHelper.dragElement(document.getElementById('doanhthu-form'), 'header-doanhthu-form')
   })

   // useEffect(() => {
   // }, [])

   const handleSubmit = (params, formId) => {
      setBtnLoading(true)
      if (sttrec) {
         ApiSIDmHd.updatethutien(params, res => {
            if (res.status >= 200 && 204 >= res.status) {
               onAfterSave && onAfterSave({ ...params }, sttrec)
               ZenMessageToast.success();
            } else {
               setError(zzControlHelper.getResponseError(res))
               ZenMessageAlert.error(zzControlHelper.getResponseError(res))
            }
            setBtnLoading(false)
         })
      } else {
         ApiSIDmHd.insertthutien({ ...params, ma_hd: mahd }, res => {
            if (res.status >= 200 && 204 >= res.status) {
               onAfterSave && onAfterSave({ ...res.data.data[0], ma_hd: mahd })
               ZenMessageToast.success();
            } else {
               setError(zzControlHelper.getResponseError(res))
               ZenMessageAlert.error(zzControlHelper.getResponseError(res))
            }
            setBtnLoading(false)
         })
      }
   }

   const customHeader = (id) => {
      const header = 'Ghi nhận doanh thu'
      const txtHeader = !id ? `Thêm mới ${header}` : `Sửa ` + capitalize(header)
      return txtHeader
   }
   const capitalize = (s) => {
      if (typeof s !== 'string') return ''
      return s.charAt(0).toUpperCase() + s.slice(1)
   }

   return <>
      <Modal id="doanhthu-form"
         closeOnEscape closeIcon closeOnDimmerClick={false}
         onClose={() => onClose()}
         open={open}
         size={"tiny"}
      >
         <Modal.Header id='header-doanhthu-form' style={{ cursor: "grabbing" }}>
            {customHeader(sttrec)}
         </Modal.Header>

         <Modal.Content>
            <ZenFormik form={"sidmhddoanhthu"} ref={refFormik}
               validation={formValidationDoanhthu}
               initItem={item || initItemDoanhthu}
               onSubmit={handleSubmit}
            >
               {
                  formik => {
                     return <Form>
                        <Form.Group>
                           <ZenFieldDate width={8} name="ngay_ct" props={formik}
                              required
                              label="sidmhddoanhthu.ngay_ct" defaultlabel="Ngày"
                           />
                           <ZenFieldNumber width={16} name="t_tien2" props={formik}
                              required
                              label="sidmhddoanhthu.t_tien2" defaultlabel="Giá trị"
                           />
                        </Form.Group>
                        <Form.Group>
                           <ZenField width={16} name="so_ct" props={formik}
                              label="sidmhddoanhthu.so_ct" defaultlabel="Số hóa đơn"
                           />
                        </Form.Group>
                        <Form.Group>
                           <ZenField width={16} name="dien_giai" props={formik}
                              label="sidmhddoanhthu.dien_giai" defaultlabel="Diễn giải"
                           />
                        </Form.Group>
                     </Form>
                  }
               }
            </ZenFormik>
         </Modal.Content>
         <Modal.Actions>
            <ZenButton btnType={"cancel"} size="small" onClick={() => onClose()} />
            <ZenButton btnType="save" size="small"
               loading={btnLoading} type="submit"
               onClick={(e) => refFormik.current.handleSubmit(e)}
            />
         </Modal.Actions>
      </Modal>
   </>
}

const initItemDoanhthu = {
   stt_rec: "",
   ngay_ct: new Date(),
   ma_hd: "",
   ma_ct: "SO3",
   dien_giai: "",
   so_ct: "",
   ps_no:0,
   t_tien2: 0, 
}

const formValidationDoanhthu = [
   {
      id: "ngay_ct",
      validationType: "date",
      validations: [
         {
            type: "required",
            params: ["Không được bỏ trống trường này"]
         },
      ]
   },
   {
      id: "t_tien2",
      validationType: "number",
      validations: [
         {
            type: "required",
            params: ["Không được bỏ trống trường này"]
         },
         {
            type: "min",
            params: [1, "Không được nhỏ hơn 0"]
         },
      ]
   },
   // {
   //    id: "so_ct",
   //    validationType: "string",
   //    validations: [
   //       {
   //          type: "required",
   //          params: ["Không được bỏ trống trường này"]
   //       },
   //    ]
   // },
]

const initItemSIDmHdNv = {
   ma_nvns: "",
   ma_hd: "",
   muc_luong: 0,
   ghi_chu: "",
}

const ModalHDNhanSu = ({ open, id, item, mahd, onClose, onAfterSave, title }) => {
   const _isMounted = useRef(true);
   const refFormik = useRef();
   const [error, setError] = useState();
   const [roles, setRoles] = useState([]);
   const [btnLoading, setBtnLoading] = useState();

   useEffect(() => {
      zzControlHelper.dragElement(document.getElementById('nhansu-form'), 'header-nhansu-form')
   })
   const handleSubmit = (params, formId) => {
      setBtnLoading(true)
      if (id) {
         ApiSIDmHd.editHrHsNsByMaHd({id: id, ma_hd: mahd, ma_nvns: params.ma_nvns, muc_luong: params.muc_luong, ghi_chu: params.ghi_chu }, res => {
            if (res.status >= 200 && 204 >= res.status) {
               onAfterSave && onAfterSave({ ...params }, id)
               ZenMessageToast.success();
            } else {
               setError(zzControlHelper.getResponseError(res))
               ZenMessageAlert.error(zzControlHelper.getResponseError(res))
            }
            setBtnLoading(false)
         })
      } else {
         ApiSIDmHd.insertHrHsNsByMaHd({ ma_hd: mahd, ma_nvns: params.ma_nvns, muc_luong: params.muc_luong, ghi_chu: params.ghi_chu }, res => {
            if (res.status >= 200 && 204 >= res.status) {
               onAfterSave && onAfterSave(res.data.data, id)
               ZenMessageToast.success();
            } else {
               setError(zzControlHelper.getResponseError(res))
               ZenMessageAlert.error(zzControlHelper.getResponseError(res))
            }
            setBtnLoading(false)
         })
      }
   }

   const customHeader = (id) => {
      const header = 'Nhân sự'
      const txtHeader = !id ? `Thêm mới ${header}` : `Sửa ` + capitalize(header)
      return txtHeader
   }
   const capitalize = (s) => {
      if (typeof s !== 'string') return ''
      return s.charAt(0).toUpperCase() + s.slice(1)
   }

   return <>
      <Modal id="nhansu-form"
         closeOnEscape closeIcon closeOnDimmerClick={false}
         onClose={() => onClose()}
         open={open}
         size={"tiny"}
      >
         <Modal.Header id='header-nhansu-form' style={{ cursor: "grabbing" }}>
            {customHeader(id)}
         </Modal.Header>

         <Modal.Content>
            <ZenFormik form={"sidmhdnhansu"} ref={refFormik}
               validation={formValidationNhanSu}
               initItem={item || initItemSIDmHdNv}
               onSubmit={handleSubmit} >
               {
                  formik => {
                     return <Form>
                        <Form.Group>
                           <ZenFieldSelectApi width={16} name="ma_nvns"
                              required
                              lookup={ZenLookup.ID_NV}
                              formik={formik}
                              label="sidmhdnhansu.tk" defaultlabel="Nhân sự"
                           />
                        </Form.Group>
                        <Form.Group>
                           <ZenFieldNumber width={16} name="muc_luong" props={formik}
                              required
                              label="sidmhdnhansu.muc_luong" defaultlabel="Mức lương"
                           />
                        </Form.Group>
                        <Form.Group>
                           <ZenField width={16} name="ghi_chu" props={formik}
                              label="sidmhdnhansu.ghi_chu" defaultlabel="Ghi chú"
                           />
                        </Form.Group>
                     </Form>
                  }
               }
            </ZenFormik>
         </Modal.Content>
         <Modal.Actions>
            <ZenButton btnType={"cancel"} size="small" onClick={() => onClose()} />
            <ZenButton btnType="save" size="small"
               loading={btnLoading} type="submit"
               onClick={(e) => refFormik.current.handleSubmit(e)}
            />
         </Modal.Actions>
      </Modal>
   </>
}

const formValidationNhanSu = [
   {
      id: "ma_nvns",
      validationType: "string",
      validations: [
         {
            type: "required",
            params: ["Không được bỏ trống trường này"]
         },
      ]
   },
   {
      id: "muc_luong",
      validationType: "number",
      validations: [
         {
            type: "required",
            params: ["Không được bỏ trống trường này"]
         },
         {
            type: "min",
            params: [1, "Không được nhỏ hơn 0"]
         },
      ]
   },
]

const ContextMenu = React.createContext({})

const styles = {
   hd_card: {
      borderLeft: "solid 1px rgba(34, 36, 38, 0.1)",
      borderRight: "solid 1px rgba(34, 36, 38, 0.1)",
      borderBottom: "solid 1px rgba(34, 36, 38, 0.1)",
   }
}

/** Ghi nhớ vị trí của tab */
const memoryStoreCRM = {
   _data: new Map(),
   get(key) {
      if (!key) {
         return null
      }

      return this._data.get(key) || null
   },
   set(key, data) {
      if (!key) {
         return
      }
      return this._data.set(key, data)
   }
}