import React, { useEffect, useMemo, useRef, useState } from "react";
import { useIntl } from "react-intl";
import { Icon, Item, Loader, Message, Segment, Table } from "semantic-ui-react";
import { ZenLink, ZenLoading, ZenMessageAlert, ZenMessageToast, ZenModalLookup } from "../../../../components/Control";
import { FormMode, ZenHelper } from "../../../../utils";
import { getModal } from "../../../ComponentInfo/Dictionary/ZenGetModal";
import { ZenLookup } from "../../../ComponentInfo/Dictionary/ZenLookup";
import { IntlFormat } from "../../../../utils/intlFormat";
import { typeDetailPage } from "../../Component/zConstVariable";
import { ApiCRMDeal, ApiCRMLead } from "../../Api";
import { ApiArDmKh } from "../../../AR/Api";
import { NoImage } from "../../../../resources/index";
import { zzControlHelper } from "../../../../components/Control/zzControlHelper";
import { Mess_ARDmKh } from "../../../AR/zLanguage/variable";
import "./general.css";
import { ApiSIDmHd } from "../../../SI/Api";
import * as routes from "../../../../constants/routes";
import { auth } from "../../../../utils"
import * as permissions from '../../../../constants/permissions'

const extractString = ZenHelper.extractString(['{', '}']);

export const InfoComponent = ({ title, loading, item, viewObject, onSetItem }) => {
   // ************ STATE
   const intl = useIntl()
   const [collapse, setCollapse] = useState(false)
   const [loadingSave, setLoadingSave] = useState("")

   // ************ MEMO
   const memoizedView = useMemo(() => getBasicInfo(), [item]);

   // ************ HANDLE
   const onItemLookup = (option, { api, fieldUpdate, lookup, renderView, arrName }) => {
      if (option[lookup.value] == item[fieldUpdate]) {
         return
      }

      setLoadingSave(fieldUpdate)
      // PATCH
      const patchData = [
         {
            value: option[lookup.value],
            path: `/${fieldUpdate}`,
            op: "replace",
            operationType: 0,
         }
      ]

      api["updatePatch"](item.id, patchData, res => {
         if (res.status >= 200 && res.status <= 204) {
            const newItem = {}, valueRender = renderView.split(",")

            arrName.forEach((itemField, index) => {
               newItem[itemField] = option[valueRender[index]]
            });
            onSetItem(newItem)
            setLoadingSave()
            ZenMessageToast.success();
         } else {
            setLoadingSave()
            ZenMessageAlert.error(ZenHelper.getResponseError(res))
         }
      })
   };

   // ************ FUNCTION
   function getBasicInfo() {
      return viewObject.map(({ name, text, ...rest }) => {
         const arrName = extractString(name);
         let value = name;

         for (let index = 0; index < arrName.length; index++) {
            const field = arrName[index];
            let valueFormat = item[field]

            if (rest.type === "date") {
               valueFormat = item[field] ? intl.formatDate(item[field], {
                  year: 'numeric',
                  month: 'numeric',
                  day: 'numeric',
               }) : ""
            } else if (rest.type === "number") {
               valueFormat = item[field] ? intl.formatNumber(item[field], {}) : ""
            }

            value = value.replace(`{${field}}`, valueFormat || "")
         }

         return {
            text: text,
            value: value,
            arrName: arrName,
            ...rest
         }
      })
   }

   function tableUI() {
      return <Table basic='very'>
         <Table.Body>
            {
               memoizedView.map((t, index) => {
                  const isLoadingBtn = t.edit ? loadingSave === t.edit.fieldUpdate : false
                  let viewValue = t.value
                  // route
                  if (t.route) {
                     viewValue = <ZenLink to={t.route(zzControlHelper.btoaUTF8(item.ma_kh))}>
                        {t.value}
                     </ZenLink>
                  }
                  return <Table.Row key={index}>
                     <Table.Cell textAlign="right" collapsing>{t.text}</Table.Cell>
                     <Table.Cell style={{ fontWeight: "bold", whiteSpace: t.type == "textarea" ? "pre-wrap" : "" }}>
                        {viewValue}
                     </Table.Cell>
                     <Table.Cell textAlign="right" collapsing>
                        {t.edit && t.edit.lookup && <>
                           <ZenModalLookup trigger={<Icon link
                              name={isLoadingBtn ? "spinner" : "pencil"}
                              loading={isLoadingBtn ? true : false} />}

                              lookup={t.edit.lookup}
                              value={item[t.edit.fieldUpdate]}
                              onItemLookup={(option) => onItemLookup(option, { ...t.edit, arrName: t.arrName })}
                           />
                        </>}
                     </Table.Cell>
                  </Table.Row>
               })
            }
         </Table.Body>
      </Table>
   }

   return <Segment loading={loading}>
      {title && <div className={"crm-lf-header"}>
         {title}
         <Icon name={collapse ? "plus" : "minus"}
            style={{ float: "right" }} link
            onClick={() => setCollapse(!collapse)}
         />
      </div>}
      {!collapse && tableUI()}
   </Segment>
}

export const NVPhuTrachComponent = ({ title, item, keyItem = "id",
   isNvPhuTrach = true, typePage, textButtonAdd,
}) => {
   // *************** STATE
   const intl = useIntl();
   const _isMounted = useRef(true)
   const [data, setData] = useState([]);
   const [error, setError] = useState();
   const [loading, setLoading] = useState(true);
   const [collapse, setCollapse] = useState(false);
   // *************** MEMO
   const api = useMemo(() => getApi(), [typePage]);

   // *************** MAIN
   useEffect(() => {
      if (api) {
         api.url[api.get](item[keyItem], res => {
            if (_isMounted.current) {
               if (res.status >= 200 && res.status <= 204) {
                  setData(res.data.data)
               } else {
                  setError(ZenHelper.getResponseError(res))
               }
               loading && setLoading(false)
            }
         }, { vai_tro: isNvPhuTrach ? "1" : "2" }) // 1: phụ trách, 2 theo dõi
      }
      return () => {
         _isMounted.current = false
      }
   }, [])

   // *************** HANDLE
   const handleAddNvns = (option) => {
      const keyPri = typePage == typeDetailPage.Lead ? "lead_id"
         : typePage == typeDetailPage.Deal ? "deal_id"
            : keyItem

      const objIns = {
         [keyPri]: item[keyItem],
         ma_nvns: option.ma_nvns,
         vai_tro: isNvPhuTrach ? "1" : "2" // 1: phụ trách, 2 theo dõi
      }

      api.url[api.insert](objIns, res => {
         if (res.status >= 200 && res.status <= 204) {
            setData(data.concat({
               ...res.data.data,
               ten_nvns: option.ho_ten
            }))
            ZenMessageToast.success()
         } else {
            ZenMessageAlert.error(ZenHelper.getResponseError(res))
         }
      })
   }

   const handleRemoveData = (itemData) => {
      ZenMessageAlert.question(
         intl.formatMessage({
            id: "fileAttach.q_delete",
            defaultMessage: "Bạn có chắc muốn xóa không?"
         }))
         .then(success => {
            if (success == 1) {
               api.url[api.delete](item[keyItem], itemData.ma_nvns, res => {
                  if (res.status >= 200 && res.status <= 204) {
                     setData(data.filter(t => t.ma_nvns != itemData.ma_nvns))
                     ZenMessageToast.success()
                  } else {
                     ZenMessageAlert.error(ZenHelper.getResponseError(res))
                  }
               })
            }
         })
   }

   // *************** FUNCTION
   function getApi() {
      if (typePage === typeDetailPage.Lead) {
         return {
            url: ApiCRMLead,
            get: "getHrHsNsByLead",
            insert: "insertHrHsNsForLead",
            delete: "deleteHrHsNsOfLead",
         }
      } else if (typePage === typeDetailPage.MA_KH) {
         return {
            url: ApiArDmKh,
            get: "getHrHsNsByMaKh",
            insert: "insertHrHsNsByMaKh",
            delete: "deleteHrHsNsByMaKh",
         }
      } else if (typePage === typeDetailPage.MA_HD) {
         return {
            url: ApiSIDmHd,
            get: "getHrHsNsByMaHd",
            insert: "insertHrHsNsByMaHd",
            delete: "deleteHrHsNsByMaHd",
         }
      } else if (typePage === typeDetailPage.Deal) {
         return {
            url: ApiCRMDeal,
            get: "getHrHsNsByDeal",
            insert: "insertHrHsNsForDeal",
            delete: "deleteHrHsNsOfDeal",
         }
      }
   }

   function listDataUI() {
      return data.map((obj, index) => {
         return <React.Fragment key={obj.id || index}>
            <div className="crm-lf-row crm-select-hover">
               <div className="crm-col1">
                  <Icon name="user circle" size="big" />
               </div>
               <div className="crm-col2">
                  <div><strong>{obj.ma_nvns} - {obj.ten_nvns}</strong></div>
                  <div><Icon name="mail" />{obj.email || "..."}</div>
               </div>
               <div className="sub-hover">
                  <Icon name="trash" link onClick={() => handleRemoveData(obj)} />
               </div>
            </div>
            <div className="crm-hr" />
         </React.Fragment>
      })
   }

   function bottomUI() {
      return <div className="crm-lf-bottom">
         <ZenModalLookup
            trigger={<label className="crm-cursor">
               <Icon name='linkify' />
               {textButtonAdd}
            </label>}
            lookup={ZenLookup.ID_NV}
            onItemLookup={handleAddNvns}
         />
      </div>
   }

   return <Segment>
      <TitleHeader title={title}
         totalRows={data.length}
         collapse={collapse}
         onCollapse={() => setCollapse(!collapse)}
      />
      <ZenLoading loading={loading} inline="centered" />
      {error && <Message negative list={error} />}

      {!collapse && <>
         {listDataUI()}
         {bottomUI()}
      </>}
   </Segment>
}

export const KhachHangComponent = ({ title, item, keyItem = "id",
   typePage, createNewKH = false
}) => {
   // *************** STATE
   const _isMounted = useRef(true)
   const [data, setData] = useState([])
   const [error, setError] = useState()
   const [loading, setLoading] = useState(true);
   const [collapse, setCollapse] = useState(false);
   const [infoModal, setInfoModal] = useState();

   // *************** MAIN
   useEffect(() => {
      // get thông tin modal KH
      const temp = getModal(ZenLookup.Ma_kh)
      setInfoModal(temp)
      // get list KH
      getItems()

      return () => {
         _isMounted.current = false
      }
   }, [])

   // *************** HANDLE
   const handleAddItem = (option) => {
      const insItem = {
         deal_id: item[keyItem],
         object_code: option.ma_kh,
      }
      ApiCRMDeal.insertArDmKhForDeal(insItem, res => {
         if (res.status >= 200 && res.status <= 204) {
            const newItem = {
               ...res.data.data,
               ten_kh: option.ten_kh,
               email: option.email,
               tel: option.tel
            }
            setData(data.concat(newItem))
            ZenMessageToast.success();
         } else {
            ZenMessageAlert.error(ZenHelper.getResponseError(res))
         }
      })
   }

   const handleRemoveItem = (itemRow) => {
      ZenMessageAlert.question("Bạn có chắc muốn xóa không?")
         .then(success => {
            if (success == 1) {
               ApiCRMDeal.deleteArDmKhOfDeal(item[keyItem], itemRow.ma_kh,
                  res => {
                     if (res.status === 204) {
                        setData(data.filter(t => t.ma_kh != itemRow.ma_kh))
                        ZenMessageToast.success()
                     } else {
                        ZenMessageAlert.error(ZenHelper.getResponseError(res))
                     }
                  })
            }
         })
   }

   // ----- CREATE KHACH HANG
   const handleOpenCloseModal = (mode, itemData = {}) => {
      setInfoModal({
         ...infoModal,
         // id: itemData.ma_kh,
         formMode: mode,
         open: true,
         other: {
            id: item[keyItem],
            fieldCode: keyItem
         }
      })
   }

   const handleAfterSaveModal = (newItem, { mode }) => {
      if (mode === FormMode.ADD) {
         setData(data.concat(newItem))
      } else if (mode === FormMode.EDIT) {
         setData(data.map(t => t.ma_kh == newItem.ma_kh ? newItem : t))
      }

      // close modal
      setInfoModal({
         ...infoModal,
         formMode: FormMode.NON,
         open: false,
         other: ""
      })
   }

   // *************** FUNCTION
   function getItems() {
      ApiCRMDeal.getArDmKhByDeal(item[keyItem], res => {
         if (_isMounted.current) {
            if (res.status === 200) {
               setData(res.data.data)
            } else {
               setError(ZenHelper.getResponseError(res))
            }
            loading && setLoading(false)
         }
      })
   }

   function listDataUI() {
      return data.length > 0 && <>
         <Item.Group divided style={{ margin: "14px 0px" }}>
            {
               data.map(item => {
                  return <Item key={item.ma_kh}
                     className="crm-select-hover"
                  >
                     <Item.Image size='tiny' src={NoImage} />

                     <Item.Content>
                        <p style={{ fontWeight: "bold" }}>
                           <ZenLink to={routes.ARDmKhDetail(zzControlHelper.btoaUTF8(item.ma_kh))}>
                              {item.ma_kh} - {item.ten_kh}
                           </ZenLink>
                        </p>
                        <p><Icon name="mail" /> {item.email}</p>
                        <p><Icon name="phone" rotated='clockwise' /> {item.tel}</p>
                     </Item.Content>

                     <Item.Content className="sub-hover" style={{ textAlign: "right" }}>
                        <Icon name='trash' link onClick={() => handleRemoveItem(item)} />
                     </Item.Content>
                  </Item>
               })
            }
         </Item.Group>
         <div className="crm-hr" />
      </>
   }

   function bottomUI() {
      return <div className="crm-lf-bottom">
         <ZenModalLookup
            trigger={<label className="crm-cursor">
               <Icon name='linkify' />
               {IntlFormat.text(Mess_ARDmKh.Add)}&nbsp;
               {IntlFormat.text(Mess_ARDmKh.Header)}
            </label>}
            lookup={{ ...ZenLookup.Ma_kh, cols: ZenLookup.Ma_kh.cols + ",tel,email" }}
            onItemLookup={handleAddItem}
         />

         {createNewKH && <>
            <span style={{ margin: "0px 7px" }} />

            <label className="crm-cursor"
               onClick={() => handleOpenCloseModal(FormMode.ADD)}
            >
               <Icon rotated='clockwise' name='plus' />
               {IntlFormat.text(Mess_ARDmKh.CreateNew)}&nbsp;
               {IntlFormat.text(Mess_ARDmKh.Header)}
            </label>
         </>}
      </div>
   }

   return <Segment>
      <TitleHeader title={title}
         totalRows={data.length}
         collapse={collapse}
         onCollapse={() => setCollapse(!collapse)}
      />

      <ZenLoading loading={loading} inline={"centered"} />
      {error && <Message negative list={error} />}

      {
         !collapse && <>
            {listDataUI()}
            {bottomUI()}
         </>
      }

      {zzControlHelper.openModalComponent(infoModal,
         {
            onClose: () => handleOpenCloseModal(FormMode.NON),
            onAfterSave: handleAfterSaveModal,
         }
      )}
   </Segment>
}

const TitleHeader = ({ title, totalRows, collapse, onCollapse }) => {
   return <div className={"crm-lf-header"}>
      {title + ` (${totalRows})`}
      <Icon name={collapse ? "plus" : "minus"}
         style={{ float: "right" }} link
         onClick={onCollapse}
      />
   </div>
}

