import {
   ARDmKhDetail, SIDmHdDetail,
   CRMLeadDetail, CRMDealDetail
} from "./PageDetail/zindex"

export const CRM_Other = {
   ARDmKhDetail: ARDmKhDetail,
   CRMDealDetail: CRMDealDetail,
   CRMLeadDetail: CRMLeadDetail,
   SIDmHdDetail: SIDmHdDetail
};

