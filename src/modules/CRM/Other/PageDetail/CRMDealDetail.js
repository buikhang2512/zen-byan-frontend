import React, { memo } from "react";
import { Breadcrumb, Grid, Message, Step } from "semantic-ui-react";
import { HeaderLink, Helmet, SegmentHeader } from "../../../../components/Control/index";
import { ZenHelper } from "../../../../utils/global";
import { IntlFormat } from "../../../../utils/intlFormat";
import * as routes from "../../../../constants/routes";
import { ContainerScroll } from "../../../../components/Control";
import { menuBar, typeDetailPage } from "../../Component/zConstVariable";

import { InfoComponent, KhachHangComponent, NVPhuTrachComponent } from "../PageDetailComponent/LeftDetailComponent";
import { RightDetailComponent } from "../PageDetailComponent/RightDetailComponent";
import { ApiCRMDeal } from "../../Api/index";
import { Mess_ARDmKh } from "../../../AR/zLanguage/variable";
import { Mess_CRMDeal, Mess_CRMLead } from "../../zLanguage/variable";
import { zzControlHelper } from "../../../../components/Control/zzControlHelper";
import {useHeaderList} from "../../../../components/Control/zzCustomHook"
import * as permissions from "../../../../constants/permissions";
import { auth } from "../../../../utils";

class CRMDealDetailComponent extends React.Component {
   constructor(props) {
      super(props);
      this._isMounted = true
      this.idDeal = zzControlHelper.atobUTF8(props.match.params.id)
      this.idElementContainer = "deal-scroll";

      this.state = {
         error: [],
         loading: true,
         deal: {
            id: this.idDeal
         },
      }
   }

   componentDidMount() {
      ApiCRMDeal.getByCode(this.idDeal, res => {
         if (this._isMounted) {
            if (res.status === 200) {
               this.setState({
                  deal: res.data.data,
                  loading: false,
               })
            } else {
               this.setState({
                  error: ZenHelper.getResponseError(res),
                  loading: false,
               })
            }
         }
      })
   }

   componentWillUnmount() {
      this._isMounted = false
   }

   render() {
      const { error, loading, deal } = this.state
      return <>
         <DealHeaderLink deal={deal} />
         {error && error.length > 0 && <Message negative header="Error" list={error} />}
         
         {/*<Step.Group widths={5} size="small">*/}
         {/*   <Step link>*/}
         {/*      Tư vấn giải pháp*/}
         {/*   </Step>*/}
         {/*   <Step active link>*/}
         {/*      Đã gửi báo giá*/}
         {/*   </Step>*/}
         {/*   <Step disabled>*/}
         {/*      Đã gửi hợp đồng*/}
         {/*   </Step>*/}
         {/*   <Step disabled>*/}
         {/*      Đã ký hợp đồng*/}
         {/*   </Step>*/}
         {/*   <Step disabled>*/}
         {/*      Thất bại*/}
         {/*   </Step>*/}
         {/*</Step.Group>*/}

         <Grid columns="2" id={this.idElementContainer}>
            <Grid.Column width="6">
               <ContainerScroll
                  id={"deal-left"} isSegment={false}
                  idElementContainer={this.idElementContainer}
               >
                  <InfoComponent item={deal} loading={loading}
                     title={IntlFormat.text(Mess_CRMDeal.BasicInfo)}
                     viewObject={[
                        { name: "{ma_deal}", text: "Mã deal" },
                        { name: "{ten_deal}", text: "Tên deal" },
                        { name: "{nhu_cau}", text: IntlFormat.text(Mess_CRMDeal.NhuCau) },
                        { name: "{ngay_dong_dk}", text: IntlFormat.text(Mess_CRMDeal.NgayDongDk), type: "date" },
                        { name: "{gia_tri_dk}", text: IntlFormat.text(Mess_CRMDeal.GiaTriDk), type: "number" },
                        { name: "{ma_nvns_owner}", text: "Nhân viên phụ trách" }
                     ]}
                  />

                  <KhachHangComponent item={deal} keyItem={"id"}
                     typePage={typeDetailPage.Deal}
                     title={IntlFormat.text(Mess_ARDmKh.Header)}
                  />

                  <NVPhuTrachComponent item={deal} keyItem={"id"}
                     typePage={typeDetailPage.Deal} isNvPhuTrach={true}
                     title={IntlFormat.text(Mess_CRMLead.NguoiPhuTrach)}
                     textButtonAdd={IntlFormat.text(Mess_CRMLead.ThemNguoiPhuTrach)}
                  />

                  <NVPhuTrachComponent item={deal} keyItem={"id"}
                     typePage={typeDetailPage.Deal} isNvPhuTrach={false}
                     title={IntlFormat.text(Mess_CRMLead.NguoiTheoDoi)}
                     textButtonAdd={IntlFormat.text(Mess_CRMLead.ThemNguoiTheoDoi)}
                  />
               </ContainerScroll>
            </Grid.Column>

            <Grid.Column width="10">
               <RightDetailComponent idElementContainer={this.idElementContainer}
                  type={typeDetailPage.Deal}
                  data={deal} menuBar={{
                     ...menuBar,
                     TTC: { name: "ttc", content: "Thông tin chung", visible: true, default: true},
                     TTCHD: {visible: false},
                     GhiChu: auth.checkPermission(permissions.CRMDealTabGhiChuXem) ? { name: "ghichu", content: "Ghi chú", visible: true, } : { visible: false},
                     HoatDong: auth.checkPermission(permissions.CRMDealTabHoatDongXem) ? { name: "hoatdong", content: "Hoạt động", visible: true, } : { visible: false},
                     TraoDoi: auth.checkPermission(permissions.CRMDealTabTraoDoiXem) ? { name: "traodoi", content: "Trao Đổi", visible: true, } : { visible: false},
                     FileAttach: auth.checkPermission(permissions.CRMDealTabFileAttachXem) ? { name: "file", content: "File kèm theo", visible: true, } : { visible: false},
                     Deal: { visible: false },
                     HopDong: { visible: false },
                     //FileAttach: { visible: false },
                     HHDV: {visible: false},
                     ThuTien: {visible: false},
                  }}
               />
            </Grid.Column>
         </Grid>
      </>
   }
}

const DealHeaderLink = memo(({ deal }) => {
   const linkHeader = [{
      id: Mess_CRMDeal.List,
      defaultMessage: "Cơ hội bán hàng",
      route: routes.CRMDeal,
      active: false,
      isSetting: false,
   }]
   const [headers] = useHeaderList();
   return <>
      {!headers && <><Helmet idMessage={Mess_CRMDeal.DealDetail.id}
         defaultMessage={Mess_CRMDeal.DealDetail.defaultMessage} />

      <SegmentHeader>
         <HeaderLink listHeader={linkHeader}>
            <Breadcrumb.Divider icon='right chevron' />
            <Breadcrumb.Section active>
               {deal.ten_deal || IntlFormat.text({ id: "detail", defaultMessage: "Chi tiết" })}
            </Breadcrumb.Section>
         </HeaderLink>
      </SegmentHeader></>}
      <br />
   </>
})

export const CRMDealDetail = {
   route: routes.CRMDealDetail(),
   Zenform: CRMDealDetailComponent,
   action: {
      view: { visible: true, permission: permissions.DEALXem },
   },
}