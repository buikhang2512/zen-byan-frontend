import React from "react";
import { Breadcrumb, Grid, Message } from "semantic-ui-react";
import { HeaderLink, Helmet, SegmentHeader } from "../../../../components/Control/index";
import { ZenHelper } from "../../../../utils/global";
import { IntlFormat } from "../../../../utils/intlFormat";
import * as routes from "../../../../constants/routes";
import { ContainerScroll } from "../../Component/Index";
import { menuBar, typeDetailPage } from "../../Component/zConstVariable";

import { InfoComponent, NVPhuTrachComponent } from "../PageDetailComponent/LeftDetailComponent";
import { RightDetailComponent } from "../PageDetailComponent/RightDetailComponent";
import { ApiArDmKh } from "../../../AR/Api";
import { Mess_ARDmKh } from "../../../AR/zLanguage/variable";
import { Mess_CRMDeal, Mess_CRMLead } from "../../zLanguage/variable";
import { zzControlHelper } from "../../../../components/Control/zzControlHelper";

class CRMCompanyDetailComponent extends React.Component {
   linkHeader = [{
      ...IntlFormat.default(Mess_ARDmKh.List),
      route: routes.ARDmkh,
      active: false,
      isSetting: false,
   }]
   constructor(props) {
      super();
      this._isMounted = true
      this.maKH = zzControlHelper.atobUTF8(props.match.params.id)
      this.idElementContainer = "grid-container";

      this.state = {
         error: "",
         loading: true,
         itemKH: { ma_kh: this.maKH },
      }
   }

   componentDidMount() {
      ApiArDmKh.getByCode(this.maKH, res => {
         if (this._isMounted) {
            if (res.status === 200) {
               this.setState({
                  itemKH: res.data.data,
                  loading: false,
               })
            } else {
               this.setState({
                  error: ZenHelper.getResponseError(res),
                  loading: false,
               })
            }
         }
      })
   }

   componentWillUnmount() {
      this._isMounted = false
   }

   render() {
      const { itemKH, loading, error } = this.state
      return <>
         <Helmet idMessage={Mess_ARDmKh.Detail.id}
            defaultMessage={Mess_ARDmKh.Detail.defaultMessage} />

         <SegmentHeader>
            <HeaderLink listHeader={this.linkHeader}>
               <Breadcrumb.Divider icon='right chevron' />
               <Breadcrumb.Section active>
                  {itemKH.ten_kh || "Chi tiết"}
               </Breadcrumb.Section>
            </HeaderLink>
         </SegmentHeader>
         <br />

         <Grid columns={2} id={this.idElementContainer}>
            <Grid.Column width="6">
               {error && <Message negative header="Error" list={error} />}

               <ContainerScroll isSegment={false}
                  idElementContainer={this.idElementContainer}
                  id={"itemKH-left"}
               >
                  <InfoComponent item={itemKH} loading={loading}
                     title={IntlFormat.text(Mess_CRMDeal.BasicInfo)}
                     viewObject={[
                        { name: "{ma_kh} - {ten_kh}", text: IntlFormat.text(Mess_ARDmKh.Header) },
                        { name: "{email}", text: "Email" },
                        { name: "{tel}", text: IntlFormat.text(Mess_CRMLead.DienThoai) },
                        { name: "{dia_chi}", text: IntlFormat.text(Mess_ARDmKh.DiaChi) },
                        { name: "{ghi_chu}", text: IntlFormat.text(Mess_CRMDeal.GhiChu), type: "textarea" },
                     ]}
                  />
                  <NVPhuTrachComponent item={itemKH} keyItem={"ma_kh"}
                     typePage={typeDetailPage.MA_KH} isNvPhuTrach={true}
                     title={IntlFormat.text(Mess_CRMLead.NguoiPhuTrach)}
                     textButtonAdd={IntlFormat.text(Mess_CRMLead.ThemNguoiPhuTrach)}
                  />
                  <NVPhuTrachComponent item={itemKH} keyItem={"ma_kh"}
                     typePage={typeDetailPage.MA_KH} isNvPhuTrach={false}
                     title={IntlFormat.text(Mess_CRMLead.NguoiTheoDoi)}
                     textButtonAdd={IntlFormat.text(Mess_CRMLead.ThemNguoiTheoDoi)}
                  />
               </ContainerScroll>
            </Grid.Column>

            <Grid.Column width="10">
               <RightDetailComponent idElementContainer={this.idElementContainer}
                  type={typeDetailPage.MA_KH}
                  data={itemKH} menuBar={menuBar}
                  keyData="ma_kh" fileCodeName="MA_KH"
               />
            </Grid.Column>
         </Grid>
      </>
   }
}

export const ARDmKhDetail = {
   route: routes.ARDmKhDetail(),
   Zenform: CRMCompanyDetailComponent,
   action: {
      view: { visible: true, permission: "" },
   },
}

                        // {
                        //    name: "{ma_kh_kt} - {ten_kh_kt}", text: Mess_ARDmKh.MaKh_Kt,
                        //    edit: {
                        //       api: ApiCRMCompany,
                        //       fieldUpdate: "ma_kh_kt",
                        //       lookup: ZenLookup.Ma_kh,
                        //       renderView: "ma_kh,ten_kh"
                        //       // renderView: các trường trong bảng lookup, thứ tự tương ứng với các trường trong name,
                        //       // dùng để render lại view do sử dụng Api Patch ko có result
                        //    }
                        // },