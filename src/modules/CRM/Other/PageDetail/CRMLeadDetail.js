import React from "react";
import { Breadcrumb, Grid, Message } from "semantic-ui-react";
import { HeaderLink, Helmet, SegmentHeader } from "../../../../components/Control/index";
import { ZenHelper } from "../../../../utils/global";
import { IntlFormat } from "../../../../utils/intlFormat";
import * as routes from "../../../../constants/routes";
import { ContainerScroll } from "../../Component/Index";
import { menuBar, typeDetailPage } from "../../Component/zConstVariable";

import { InfoComponent, NVPhuTrachComponent } from "../PageDetailComponent/LeftDetailComponent";
import { RightDetailComponent } from "../PageDetailComponent/RightDetailComponent";
import { ApiCRMLead } from "../../Api/index";
import { Mess_CRMDeal, Mess_CRMLead } from "../../zLanguage/variable";
import { zzControlHelper } from "../../../../components/Control/zzControlHelper";

class CRMLeadDetailComponent extends React.Component {
   linkHeader = [{
      ...IntlFormat.default(Mess_CRMLead.List),
      route: routes.CRMLead,
      active: false,
      isSetting: false,
   }]
   constructor(props) {
      super();
      this._isMounted = true
      this.idLead = zzControlHelper.atobUTF8(props.match.params.id)
      this.idElementContainer = "grid-container"

      this.state = {
         error: "",
         loading: true,
         lead: {
            id: this.idLead
         },
      }
   }

   componentDidMount() {
      ApiCRMLead.getByCode(this.idLead, res => {
         if (this._isMounted) {
            if (res.status === 200) {
               this.setState({
                  lead: res.data.data,
                  loading: false,
               })
            } else {
               this.setState({
                  error: ZenHelper.getResponseError(res),
                  loading: false,
               })
            }
         }
      })
   }

   componentWillUnmount() {
      this._isMounted = false
   }

   render() {
      const { lead, loading, error } = this.state
      return <>
         <Helmet idMessage={Mess_CRMLead.Detail.id}
            defaultMessage={Mess_CRMLead.Detail.defaultMessage} />

         <SegmentHeader>
            <HeaderLink listHeader={this.linkHeader}>
               <Breadcrumb.Divider icon='right chevron' />
               <Breadcrumb.Section active>
                  {lead.lien_he || "Chi tiết"}
               </Breadcrumb.Section>
            </HeaderLink>
         </SegmentHeader>
         <br />

         <Grid columns={2} id={this.idElementContainer}>
            <Grid.Column width="6">
               {error && <Message negative header="Error" list={error} />}
               <ContainerScroll
                  id={"lead-left"} isSegment={false}
                  idElementContainer={this.idElementContainer}
               >
                  <InfoComponent item={lead} loading={loading}
                     title={IntlFormat.text(Mess_CRMDeal.BasicInfo)}
                     viewObject={[
                        { name: "{lien_he}", text: IntlFormat.text(Mess_CRMLead.LienHe) },
                        { name: "{email}", text: "Email" },
                        { name: "{dien_thoai}", text: IntlFormat.text(Mess_CRMLead.DienThoai) },
                        { name: "{chuc_danh}", text: IntlFormat.text(Mess_CRMLead.ChucDanh) },
                        { name: "{bo_phan}", text: IntlFormat.text(Mess_CRMLead.BoPhan) },
                        { name: "{cty_ten}", text: IntlFormat.text(Mess_CRMLead.CtyTen) },
                        { name: "{deal_ten}", text: IntlFormat.text(Mess_CRMLead.DealTen) },
                        { name: "{deal_gia_tri}", text: IntlFormat.text(Mess_CRMDeal.GiaTriDk), type: "number" },
                        { name: "{deal_ngay_dong_kh}", text: IntlFormat.text(Mess_CRMDeal.NgayDongDk), type: "date" },
                     ]}
                  />
                  <NVPhuTrachComponent item={lead} keyItem={"id"}
                     typePage={typeDetailPage.Lead} isNvPhuTrach={true}
                     title={IntlFormat.text(Mess_CRMLead.NguoiPhuTrach)}
                     textButtonAdd={IntlFormat.text(Mess_CRMLead.ThemNguoiPhuTrach)}
                  />

                  <NVPhuTrachComponent item={lead} keyItem={"id"}
                     typePage={typeDetailPage.Lead} isNvPhuTrach={false}
                     title={IntlFormat.text(Mess_CRMLead.NguoiTheoDoi)}
                     textButtonAdd={IntlFormat.text(Mess_CRMLead.ThemNguoiTheoDoi)}
                  />
               </ContainerScroll>
            </Grid.Column>

            <Grid.Column width="10">
               <RightDetailComponent idElementContainer={this.idElementContainer}
                  type={typeDetailPage.Lead}
                  data={lead} menuBar={{
                     ...menuBar,
                     HHDV: {visible: false},
                     TTC: {visible: false},
                     TTCHD: {visible: false},
                     Deal: { visible: false },
                     HopDong: { visible: false },
                     TraoDoi:{ visible: false},
                     FileAttach: { visible: false },
                     ThuTien: {visible: false},
                  }}
               />
            </Grid.Column>
         </Grid>
      </>
   }
}

export const CRMLeadDetail = {
   route: routes.CRMLeadDetail(),
   Zenform: CRMLeadDetailComponent,
   action: {
      view: { visible: true, permission: "" },
   },
}