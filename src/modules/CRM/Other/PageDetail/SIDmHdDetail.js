import React, {memo} from "react";
import { Breadcrumb, Grid, Message } from "semantic-ui-react";
import { HeaderLink, Helmet, SegmentHeader } from "../../../../components/Control/index";
import { ZenHelper } from "../../../../utils/global";
import { IntlFormat } from "../../../../utils/intlFormat";
import * as routes from "../../../../constants/routes";
import { ContainerScroll } from "../../Component/Index";
import { menuBar, typeDetailPage } from "../../Component/zConstVariable";

import { InfoComponent, NVPhuTrachComponent } from "../PageDetailComponent/LeftDetailComponent";
import { RightDetailComponent } from "../PageDetailComponent/RightDetailComponent";
import { ApiSIDmHd } from "../../../SI/Api";
import { Mess_SIDmHd } from "../../../SI/zLanguage/variable";
import { Mess_ARDmKh } from "../../../AR/zLanguage/variable";
import { Mess_CRMDeal, Mess_CRMLead } from "../../zLanguage/variable";
import { zzControlHelper } from "../../../../components/Control/zzControlHelper";
import {useIntl} from 'react-intl'
import {useHeaderList} from "../../../../components/Control/zzCustomHook"
import * as permissions from "../../../../constants/permissions";
import { auth } from "../../../../utils";

class SIDmHdDetailComponent extends React.Component {
   linkHeader = [{
      ...IntlFormat.default(Mess_SIDmHd.List),
      route: routes.SIDmHd,
      active: false,
      isSetting: false,
   }]
   constructor(props) {
      super();
      this._isMounted = true
      this.ma_hd = zzControlHelper.atobUTF8(props.match.params.id)
      this.idElementContainer = "grid-container"

      this.state = {
         error: "",
         loading: true,
         itemHD: {
            ma_hd: this.ma_hd
         },
      }
   }

   componentDidMount() {
      ApiSIDmHd.getByCode(this.ma_hd, res => {
         if (this._isMounted) {
            if (res.status === 200) {
               this.setState({
                  itemHD: res.data.data,
                  loading: false,
               })
            } else {
               this.setState({
                  error: ZenHelper.getResponseError(res),
                  loading: false,
               })
            }
         }
      })
   }

   componentWillUnmount() {
      this._isMounted = false
   }

   render() {
      const { itemHD, loading, error } = this.state
      
      return <>
         <Helmet idMessage={Mess_SIDmHd.Detail.id}
            defaultMessage={Mess_SIDmHd.Detail.defaultMessage} />
         
         <PHeader itemHD={itemHD} linkHeader={this.linkHeader}/>
         <br />

         <Grid columns={2} id={this.idElementContainer}>
            <Grid.Column width="6">
               {error && <Message negative header="Error" list={error} />}
               <ContainerScroll
                  id={"itemHD-left"} isSegment={false}
                  idElementContainer={this.idElementContainer}
               >
                  <InfoComponent item={itemHD} loading={loading}
                     title={IntlFormat.text(Mess_CRMDeal.BasicInfo)}
                     viewObject={[
                        { name: "{ten_hd}", text: IntlFormat.text(Mess_SIDmHd.Header) },
                        { name: "{so_hd}", text: IntlFormat.text(Mess_SIDmHd.SoHd) },
                        {
                           name: "{ten_kh}", text: IntlFormat.text(Mess_ARDmKh.Header),
                           route: (id) => routes.ARDmKhDetail(id)
                        },
                        { name: "{ngay_hd}", text: IntlFormat.text(Mess_SIDmHd.NgayHd), type: "date" },
                        { name: "{ngay_hh}", text: IntlFormat.text(Mess_SIDmHd.NgayHh), type: "date" },
                        { name: "{tien}", text: IntlFormat.text(Mess_SIDmHd.GiaTriHD), type: "number" },
                        { name: "{noi_dung}", text: IntlFormat.text(Mess_SIDmHd.NoiDung) },
                     ]}
                  />

                  <NVPhuTrachComponent item={itemHD} keyItem={"ma_hd"}
                     typePage={typeDetailPage.MA_HD} isNvPhuTrach={true}
                     title={IntlFormat.text(Mess_CRMLead.NguoiPhuTrach)}
                     textButtonAdd={IntlFormat.text(Mess_CRMLead.ThemNguoiPhuTrach)}
                  />
                  <NVPhuTrachComponent item={itemHD} keyItem={"ma_hd"}
                     typePage={typeDetailPage.MA_HD} isNvPhuTrach={false}
                     title={IntlFormat.text(Mess_CRMLead.NguoiTheoDoi)}
                     textButtonAdd={IntlFormat.text(Mess_CRMLead.ThemNguoiTheoDoi)}
                  />
               </ContainerScroll>
            </Grid.Column>

            <Grid.Column width="10">
               <RightDetailComponent idElementContainer={this.idElementContainer}
                  type={typeDetailPage.MA_HD}
                  data={itemHD} keyData={"ma_hd"}
                  fileCodeName="MA_HD"
                  menuBar={{
                     TTCHD: { name: "ttchd", content: "Thông tin chung", visible: true, default: true},
                     HHDV: { name: "HHDV", content: "Hàng hóa dịch vụ", visible: true, },
                     ThuTien: auth.checkPermission(permissions.SIDmHdDTTT) ? { name: "ThuTien", content: "Doanh thu/thu tiền", visible: true, } : { visible: false},
                     HoatDong: auth.checkPermission(permissions.SICRMTabHoatDongXem) ? { name: "hoatdong", content: "Hoạt động", visible: true, } : { visible: false},
                     ...menuBar,
                     HDNhanSu: auth.checkPermission(permissions.SIDMHDTabNhanSu) ? { name: "HDNhanSu", content: "Nhân sự", visible: true, } : { visible: false},
                     TTC:{ visible:false},
                     GhiChu: auth.checkPermission(permissions.SICRMTabGhiChuXem) ? { name: "ghichu", content: "Ghi chú", visible: true, } : { visible: false},
                     FileAttach: auth.checkPermission(permissions.SICRMTabFileAttachXem) ? { name: "file", content: "File kèm theo", visible: true, } : { visible: false},
                     Deal: { visible: false },
                     HopDong: { visible: false },
                     TraoDoi:{ visible: false},
                  }}
               />
            </Grid.Column>
         </Grid>
      </>
   }
}

const PHeader = ({linkHeader,itemHD}) => {
   const intl = useIntl();
   const [headers] = useHeaderList();
   return (<>
      {!headers && <SegmentHeader>
         <HeaderLink listHeader={linkHeader}>
            <Breadcrumb.Divider icon='right chevron' />
            <Breadcrumb.Section active>
               {itemHD.ma_hd || "Chi tiết"}
            </Breadcrumb.Section>
         </HeaderLink>
      </SegmentHeader>}</>
   );
 };

export const SIDmHdDetail = {
   route: routes.SIDmHdDetail(),
   Zenform: SIDmHdDetailComponent,
   action: {
      view: { visible: true, permission: permissions.SIDmHdXem },
   },
}