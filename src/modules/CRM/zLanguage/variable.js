const prefixCrmNguonKh = "CRMNguonKh."
const prefixCrmQuyMo = "CRMQuyMo."
const prefixCrmLinhVuc = "CRMLinhVuc."
const prefixCrmKenhBH = "CRMKenhBH."
const prefixCrmCompanyStatus = "CRMCompanyStatus."
const prefixCrmDealStatus = "CRMDealStatus."
const prefixCrmDealUuTien = "CRMDealUuTien."
const prefixCrmDealGiaiDoan = "CRMDealGiaiDoan."
const prefixCrmDealLyDoThua = "CRMDealLyDoThua."
const prefixCrmDeal = "CRMDeal."
const prefixCrmHoatDong = "CRMHoatDong."
const prefixCrmLead = "CRMLead."

export const Mess_CRMNguonKh = {
   List: {
      id: prefixCrmNguonKh,
      defaultMessage: "Danh sách nguồn khách hàng",
      en: "Customer resource list"
   },
   Header: {
      id: prefixCrmNguonKh + "header",
      defaultMessage: "Nguồn khách hàng",
      en: "Customer resource"
   },
   Ma: {
      id: prefixCrmNguonKh + "ma_nguon_kh",
      defaultMessage: "Mã nguồn KH",
      en: "Resource code"
   },
   Ten: {
      id: prefixCrmNguonKh + "ten_nguon_kh",
      defaultMessage: "Tên nguồn KH",
      en: "Resource name"
   },
}

export const Mess_CRMQuyMo = {
   List: {
      id: prefixCrmQuyMo,
      defaultMessage: "Danh sách quy mô"
   },
   Header: {
      id: prefixCrmQuyMo + "header",
      defaultMessage: "Quy mô"
   },
   Ma: {
      id: prefixCrmQuyMo + "ma_quy_mo",
      defaultMessage: "Mã quy mô"
   },
   Ten: {
      id: prefixCrmQuyMo + "ten_quy_mo",
      defaultMessage: "Tên quy mô"
   },
   GhiChu: {
      id: prefixCrmQuyMo + "ghi_chu",
      defaultMessage: "Ghi chú"
   },
}

export const Mess_CRMLinhVuc = {
   List: {
      id: prefixCrmLinhVuc,
      defaultMessage: "Danh sách lĩnh vực"
   },
   Header: {
      id: prefixCrmLinhVuc + "header",
      defaultMessage: "Lĩnh vực"
   },
   Ma: {
      id: prefixCrmLinhVuc + "ma_quy_mo",
      defaultMessage: "Mã lĩnh vực"
   },
   Ten: {
      id: prefixCrmLinhVuc + "ten_quy_mo",
      defaultMessage: "Tên lĩnh vực"
   },
   GhiChu: {
      id: prefixCrmLinhVuc + "ghi_chu",
      defaultMessage: "Ghi chú"
   },
}

export const Mess_CRMKenhBH = {
   List: {
      id: prefixCrmKenhBH,
      defaultMessage: "Danh sách kênh bán hàng"
   },
   Header: {
      id: prefixCrmKenhBH + "header",
      defaultMessage: "Kênh bán hàng"
   },
   Ma: {
      id: prefixCrmKenhBH + "ma_kenh_bh",
      defaultMessage: "Mã kênh BH"
   },
   Ten: {
      id: prefixCrmKenhBH + "ten_kenh_bh",
      defaultMessage: "Tên kênh BH"
   },
   GhiChu: {
      id: prefixCrmKenhBH + "ghi_chu",
      defaultMessage: "Ghi chú"
   },
}

export const Mess_CRMDealUuTien = {
   List: {
      id: prefixCrmDealUuTien,
      defaultMessage: "Danh sách độ ưu tiên"
   },
   Header: {
      id: prefixCrmDealUuTien + "header",
      defaultMessage: "Ưu tiên"
   },
   Ma: {
      id: prefixCrmDealUuTien + "ma_kenh_bh",
      defaultMessage: "Mã ưu tiên"
   },
   Ten: {
      id: prefixCrmDealUuTien + "ten_kenh_bh",
      defaultMessage: "Tên ưu tiên"
   },
   GhiChu: {
      id: prefixCrmDealUuTien + "ghi_chu",
      defaultMessage: "Ghi chú"
   },
   Ordinal: {
      id: prefixCrmDealUuTien + "ordinal",
      defaultMessage: "Thứ tự"
   },
}

export const Mess_CRMGiaiDoan = {
   List: {
      id: prefixCrmDealGiaiDoan,
      defaultMessage: "Danh sách giai đoạn của cơ hội bán hàng"
   },
   Header: {
      id: prefixCrmDealGiaiDoan + "header",
      defaultMessage: "Giai đoạn của cơ hội bán hàng"
   },
   Ma: {
      id: prefixCrmDealGiaiDoan + "ma_giai_doan",
      defaultMessage: "Mã giai đoạn"
   },
   Ten: {
      id: prefixCrmDealGiaiDoan + "ten_giai_doan",
      defaultMessage: "Tên giai đoạn"
   },
   GhiChu: {
      id: prefixCrmDealGiaiDoan + "ghi_chu",
      defaultMessage: "Ghi chú"
   },
   Loai: {
      id: prefixCrmDealGiaiDoan + "loai",
      defaultMessage: "Loại"
   },
   Ordinal: {
      id: prefixCrmDealGiaiDoan + "ordinal",
      defaultMessage: "Thứ tự"
   },
}

export const Mess_CRMDealLyDoThua = {
   List: {
      id: prefixCrmDealLyDoThua,
      defaultMessage: "Danh sách lý do thua"
   },
   Header: {
      id: prefixCrmDealLyDoThua + "header",
      defaultMessage: "Lý do thua"
   },
   Ma: {
      id: prefixCrmDealLyDoThua + "ma_ly_do",
      defaultMessage: "Mã lý do"
   },
   Ten: {
      id: prefixCrmDealLyDoThua + "ten_ly_do",
      defaultMessage: "Tên lý do"
   },
   GhiChu: {
      id: prefixCrmDealLyDoThua + "ghi_chu",
      defaultMessage: "Ghi chú"
   },
   Ordinal: {
      id: prefixCrmDealLyDoThua + "ordinal",
      defaultMessage: "Thứ tự"
   },
}

export const Mess_CRMCompanyStatus = {
   List: {
      id: prefixCrmCompanyStatus,
      defaultMessage: "Danh sách trạng thái khách hàng"
   },
   Header: {
      id: prefixCrmCompanyStatus + "header",
      defaultMessage: "Trạng thái KH"
   },
   Ma: {
      id: prefixCrmCompanyStatus + "ma_trang_thai",
      defaultMessage: "Mã trạng thái"
   },
   Ten: {
      id: prefixCrmCompanyStatus + "ten_trang_thai",
      defaultMessage: "Tên trạng thái"
   },
   GhiChu: {
      id: prefixCrmCompanyStatus + "ghi_chu",
      defaultMessage: "Ghi chú"
   },
}

export const Mess_CRMDealStatus = {
   List: {
      id: prefixCrmDealStatus,
      defaultMessage: "Danh sách trạng thái cơ hội bán hàng"
   },
   Header: {
      id: prefixCrmDealStatus + "header",
      defaultMessage: "Trạng thái cơ hội bán hàng"
   },
   Ma: {
      id: prefixCrmDealStatus + "ma_trang_thai",
      defaultMessage: "Mã trạng thái"
   },
   Ten: {
      id: prefixCrmDealStatus + "ten_trang_thai",
      defaultMessage: "Tên trạng thái"
   },
   GhiChu: {
      id: prefixCrmDealStatus + "ghi_chu",
      defaultMessage: "Ghi chú"
   },

}

export const Mess_CRMDeal = {
   List: {
      id: prefixCrmDeal,
      defaultMessage: "Danh sách cơ hội bán hàng"
   },
   Header: {
      id: prefixCrmDeal + "header",
      defaultMessage: "Cơ hội bán hàng"
   },
   BasicInfo: {
      id: prefixCrmDeal + "basic_info",
      defaultMessage: "Thông tin cơ bản"
   },
   DealDetail: {
      id: prefixCrmDeal + "deal_detail",
      defaultMessage: "Chi tiết cơ hội bán hàng"
   },
   TenDeal: {
      id: prefixCrmDeal + "ten_deal",
      defaultMessage: "Tên deal"
   },
   NhuCau: {
      id: prefixCrmDeal + "nhu_cau",
      defaultMessage: "Nhu cầu"
   },
   GiaTriDk: {
      id: prefixCrmDeal + "gia_tri_dk",
      defaultMessage: "Giá trị dự kiến"
   },
   MaUuTien: {
      id: prefixCrmDeal + "ma_uu_tien",
      defaultMessage: "Độ ưu tiên"
   },
   MaGiaiDoan: {
      id: prefixCrmDeal + "ma_giai_doan",
      defaultMessage: "Giai đoạn"
   },
   MaLyDoThua: {
      id: prefixCrmDeal + "ma_ly_do_thua",
      defaultMessage: "Lý do thất bại"
   },
   NgayDongDk: {
      id: prefixCrmDeal + "ngay_dong_dk",
      defaultMessage: "Ngày đóng deal dự kiến"
   },
   NgayDongTt: {
      id: prefixCrmDeal + "ngay_dong_tt",
      defaultMessage: "Ngày đóng deal thực tế"
   },
   MaNvnsOwner: {
      id: prefixCrmDeal + "ma_nvns_owner",
      defaultMessage: "Nhân viên phụ trách"
   },
   GhiChu: {
      id: prefixCrmDeal + "ghi_chu",
      defaultMessage: "Ghi chú"
   },
   Noti_Info: {
      id: prefixCrmDeal + "noti_info",
      defaultMessage: "Chưa có thông tin"
   }
}

export const Mess_CRMHoatDong = {
   List: {
      id: prefixCrmHoatDong,
      defaultMessage: "Danh mục loại hoạt động"
   },
   Header: {
      id: prefixCrmHoatDong + "header",
      defaultMessage: "Loại hoạt động"
   },
   Ma: {
      id: prefixCrmHoatDong + "ma_hoat_dong",
      defaultMessage: "Mã loại hoạt động"
   },
   Ten: {
      id: prefixCrmHoatDong + "ten_hoat_dong",
      defaultMessage: "Tên loại hoạt động"
   },
   NoiDung: {
      id: prefixCrmHoatDong + "noi_dung",
      defaultMessage: "Mô tả"
   },
   KetQua: {
      id: prefixCrmHoatDong + "ket_qua",
      defaultMessage: "Kết quả"
   },
   ThoiGian: {
      id: prefixCrmHoatDong + "thoi_gian",
      defaultMessage: "Thời gian"
   },
   UserThucHien: {
      id: prefixCrmHoatDong + "UserThucHien",
      defaultMessage: "Người thực hiện"
   },
   KetQua: {
      id: prefixCrmHoatDong + "KetQua",
      defaultMessage: "Kết quả"
   },
   MoTa: {
      id: prefixCrmHoatDong + "MoTa",
      defaultMessage: "Mô tả"
   }
}

export const Mess_CRMLead = {
   List: {
      id: prefixCrmLead,
      defaultMessage: "Danh sách đầu mối"
   },
   Header: {
      id: prefixCrmLead + "header",
      defaultMessage: "Đầu mối"
   },
   Detail: {
      id: prefixCrmLead + "detail",
      defaultMessage: "Chi tiết đầu mối"
   },
   LienHe: {
      id: prefixCrmLead + "lien_he",
      defaultMessage: "Liên hệ"
   },
   DanhXung: {
      id: prefixCrmLead + "xung_ho",
      defaultMessage: "Danh xưng"
   },
   MaNvnsOwner: {
      id: prefixCrmLead + "ma_nvns_owner",
      defaultMessage: "Nhân viên phụ trách"
   },
   DealTen: {
      id: prefixCrmLead + "deal_ten",
      defaultMessage: "Nhu cầu"
   },
   CtyTen: {
      id: prefixCrmLead + "cty_ten",
      defaultMessage: "Công ty"
   },
   DienThoai: {
      id: prefixCrmLead + "dien_thoai",
      defaultMessage: "Điện thoại"
   },
   ChucDanh: {
      id: prefixCrmLead + "chuc_danh",
      defaultMessage: "Chức danh"
   },
   BoPhan: {
      id: prefixCrmLead + "bo_phan",
      defaultMessage: "Bộ phận"
   },
   NguoiPhuTrach: {
      id: prefixCrmLead + "ng_phu_trach",
      defaultMessage: "Người phụ trách"
   },
   NguoiTheoDoi: {
      id: prefixCrmLead + "ng_theo_doi",
      defaultMessage: "Người theo dõi"
   },
   ThemNguoiPhuTrach: {
      id: prefixCrmLead + "add_phu_trach",
      defaultMessage: "Thêm người phụ trách"
   },
   ThemNguoiTheoDoi: {
      id: prefixCrmLead + "add_theo_doi",
      defaultMessage: "Thêm người theo dõi"
   },
}