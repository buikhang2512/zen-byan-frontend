import { IntlFormat } from "../../../utils/intlFormat";
import {
   Mess_CRMNguonKh, Mess_CRMLinhVuc, Mess_CRMQuyMo,
   Mess_CRMCompanyStatus, Mess_CRMDealStatus, Mess_CRMKenhBH,
   Mess_CRMDealLyDoThua, Mess_CRMDealUuTien, Mess_CRMGiaiDoan, Mess_CRMDeal, Mess_CRMHoatDong, Mess_CRMLead
} from "./variable";

const local = "en"

const En_US = {
   ...IntlFormat.setMessageLanguage(Mess_CRMNguonKh, local),
   ...IntlFormat.setMessageLanguage(Mess_CRMQuyMo, local),
   ...IntlFormat.setMessageLanguage(Mess_CRMLinhVuc, local),
   ...IntlFormat.setMessageLanguage(Mess_CRMKenhBH, local),
   ...IntlFormat.setMessageLanguage(Mess_CRMCompanyStatus, local),
   ...IntlFormat.setMessageLanguage(Mess_CRMDealStatus, local),
   ...IntlFormat.setMessageLanguage(Mess_CRMGiaiDoan, local),
   ...IntlFormat.setMessageLanguage(Mess_CRMDealUuTien, local),
   ...IntlFormat.setMessageLanguage(Mess_CRMDealLyDoThua, local),
   ...IntlFormat.setMessageLanguage(Mess_CRMDeal, local),
   ...IntlFormat.setMessageLanguage(Mess_CRMHoatDong, local),
   ...IntlFormat.setMessageLanguage(Mess_CRMLead, local),
}
export default En_US