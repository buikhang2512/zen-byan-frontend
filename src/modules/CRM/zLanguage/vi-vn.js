import { IntlFormat } from "../../../utils/intlFormat";
import {
   Mess_CRMNguonKh, Mess_CRMLinhVuc, Mess_CRMQuyMo,
   Mess_CRMCompanyStatus, Mess_CRMDealStatus, Mess_CRMKenhBH,
   Mess_CRMDealLyDoThua, Mess_CRMDealUuTien, Mess_CRMGiaiDoan, Mess_CRMDeal, Mess_CRMHoatDong, Mess_CRMLead
} from "./variable";

const Vi_VN = {
   // CRMNguonKH
   ...IntlFormat.setMessageLanguage(Mess_CRMNguonKh),

   // CRMQuyMo
   ...IntlFormat.setMessageLanguage(Mess_CRMQuyMo),

   // CRMLinHvuc
   ...IntlFormat.setMessageLanguage(Mess_CRMLinhVuc),

   // CRMKenhBH
   ...IntlFormat.setMessageLanguage(Mess_CRMKenhBH),

   // CRMCompanyStatus: Trang Thai KH
   ...IntlFormat.setMessageLanguage(Mess_CRMCompanyStatus),

   // CRMDealStatus: Trang Thai Ban Hang
   ...IntlFormat.setMessageLanguage(Mess_CRMDealStatus),

   // CRMDealGiaiDoan: Giai doan
   ...IntlFormat.setMessageLanguage(Mess_CRMGiaiDoan),

   // UU tien
   ...IntlFormat.setMessageLanguage(Mess_CRMDealUuTien),

   // Ly do thua
   ...IntlFormat.setMessageLanguage(Mess_CRMDealLyDoThua),

   // CRMDeal
   ...IntlFormat.setMessageLanguage(Mess_CRMDeal),
   // CRMHoatDong
   ...IntlFormat.setMessageLanguage(Mess_CRMHoatDong),
   // CRMLead
   ...IntlFormat.setMessageLanguage(Mess_CRMLead),
}

export default Vi_VN