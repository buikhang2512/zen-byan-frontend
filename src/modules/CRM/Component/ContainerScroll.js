import React, { useEffect } from "react";
import { Segment } from "semantic-ui-react";
import { ZenHelper } from "../../../utils";

export const ContainerScroll = ({ children, id = "scroll-container", idElementContainer,
   loadingForm, heightSub = 0, isSegment = true }) => {
   useEffect(() => {
      handleResizeBrowser()
      window.addEventListener("resize", handleResizeBrowser)
      return () => {
         window.removeEventListener("resize", handleResizeBrowser)
      }
   }, [])

   const handleResizeBrowser = () => {
      setTimeout(() => {
         const elementContainer = document.getElementById(idElementContainer)
         const elementScroll = document.getElementById(id)

         let heightScroll = window.innerHeight - elementContainer.offsetTop
            + ZenHelper.getElmHeight(elementContainer, ["margin-bottom", "margin-top"]);

         const prevElement = elementScroll.previousElementSibling
         if (prevElement) {
            const styleHeight = ['height', "margin-bottom",
               "margin-top", "border-top", "border-bottom"]
            heightScroll = heightScroll - ZenHelper.getElmHeight(prevElement, styleHeight)
         }

         if (elementContainer && elementScroll) {
            elementScroll.style.height = (heightScroll - heightSub) + "px"
         }
      }, 100);
   }
   return isSegment ? <Segment id={id} loading={loadingForm} style={styles.scroll}>
      {children}
   </Segment>
      : <div id={id} style={styles.scroll}>
         {children}
      </div>
}

const styles = {
   scroll: {
      overflowY: "auto",
      overflowX: "hidden"
   }
}