export const menuBar = {
   TTC: { name: "ttc", content: "Thông tin chung", visible: true, },
   GhiChu: { name: "ghichu", content: "Ghi chú", visible: true,  default: true},
   HHDV: { name: "HHDV", content: "Hàng hóa dịch vụ", visible: true, },
   HoatDong: { name: "hoatdong", content: "Hoạt động", visible: true, },
   Deal: { name: "deal", content: "Cơ hội bán hàng", visible: true, },
   HopDong: { name: "sidmhd", content: "Hợp đồng", visible: true, },
   TraoDoi: { name: "traodoi", content: "Trao Đổi", visible: true, },
   FileAttach: { name: "file", content: "File kèm theo", visible: true, },
}

export const optionsLoaiGiaiDoan = [
   { key: "1", value: "0", text: "Normal" },
   { key: "2", value: "1", text: "Thắng" },
   { key: "3", value: "2", text: "Thua" },
]

export const typeDetailPage = {
   MA_KH: "MA_KH",
   MA_HD: "MA_HD",
   Lead: "Lead",
   Deal: "Deal"
}