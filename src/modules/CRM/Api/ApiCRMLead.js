import axios from '../../../Api/axios';

const ExtName = "crmlead"

export const ApiCRMLead = {
   get(callback, filter = {}, pagination = {}) {
      axios.get(`${ExtName}`, {
         params: {
            qf: filter.qf,
            keyword: filter.keyword,
            sort: filter.sort,
            page: pagination.page,
            pageSize: pagination.pageSize
         },
      })
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   getByCode(id, callback) {
      axios.get(`${ExtName}/` + id)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   insert(data, callback) {
      axios.post(`${ExtName}`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   update(data, callback) {
      axios.put(`${ExtName}`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   updatePatch(id, data, callback) {
      axios.patch(`${ExtName}/${id}`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   delete(id, callback) {
      axios.delete(`${ExtName}/` + id)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   getHrHsNsByLead(lead_id, callback, params) {
      axios.get(`${ExtName}/${lead_id}/hrhsns`, {
         params: {
            vai_tro: params.vai_tro
         }
      })
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   insertHrHsNsForLead(data, callback) {
      axios.post(`${ExtName}/${data.id}/hrhsns`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   deleteHrHsNsOfLead(lead_id, ma_nvns, callback) {
      axios.delete(`${ExtName}/${lead_id}/hrhsns/${ma_nvns}`)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

}