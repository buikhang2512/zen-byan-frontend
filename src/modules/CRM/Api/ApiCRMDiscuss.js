import axios, { axiosMultiFormData } from '../../../Api/axios';

const ExtName = "discuss"

export const ApiCRMDiscuss = {
    get(objType, callback) {
        // {type,oid,ocode,sort} =objType
        axios.get(`${ExtName}`, {
            params: objType
        })
            .then(res => {
                callback(res)
            })
            .catch(err => {
                callback(err)
            });
    },

    insert(data, callback) {
        axiosMultiFormData.post(`${ExtName}`, data)
            .then(res => {
                callback(res)
            })
            .catch(err => {
                callback(err)
            });
    },

    update(data, callback) {
        axiosMultiFormData.put(`${ExtName}`, data)
            .then(res => {
                callback(res)
            })
            .catch(err => {
                callback(err)
            });
    },

    updatePatch(code, data, callback) {
        axios.patch(`${ExtName}/${code}`, data)
            .then(res => {
                callback(res)
            })
            .catch(err => {
                callback(err)
            });
    },

    delete(code, callback) {
        axios.delete(`${ExtName}/` + code)
            .then(res => {
                callback(res)
            })
            .catch(err => {
                callback(err)
            });
    }
}