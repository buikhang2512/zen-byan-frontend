import axios from '../../../Api/axios';

const ExtName = "crmdeal"

export const ApiCRMDeal = {
   get(callback, filter = {}, pagination = {}) {
      axios.get(`${ExtName}`, {
         params: {
            qf: filter.qf,
            keyword: filter.keyword,
            sort: filter.sort,
            page: pagination.page,
            pageSize: pagination.pageSize
         },
      })
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   getByCode(code, callback) {
      axios.get(`${ExtName}/` + code)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   insert(data, callback) {
      axios.post(`${ExtName}`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   update(data, callback) {
      axios.put(`${ExtName}`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   updatePatch(code, data, callback) {
      axios.patch(`${ExtName}/${code}`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   delete(code, callback) {
      axios.delete(`${ExtName}/` + code)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   getArDmKhByDeal(deal_id, callback) {
      axios.get(`${ExtName}/${deal_id}/ardmkh`)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   insertArDmKhForDeal(data, callback) {
      axios.post(`${ExtName}/${data.deal_id}/ardmkh`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   deleteArDmKhOfDeal(deal_id, ma_kh, callback) {
      axios.delete(`${ExtName}/${deal_id}/ardmkh/${ma_kh}`)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   getHrHsNsByDeal(deal_id, callback, params) {
      axios.get(`${ExtName}/${deal_id}/hrhsns`, {
         params: {
            vai_tro: params.vai_tro
         }
      })
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   insertHrHsNsForDeal(data, callback) {
      axios.post(`${ExtName}/${data.id}/hrhsns`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   deleteHrHsNsOfDeal(deal_id, ma_nvns, callback) {
      axios.delete(`${ExtName}/${deal_id}/hrhsns/${ma_nvns}`)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   getCongViecByDeal(ma_deal, callback) {
      axios.get(`${ExtName}/${ma_deal}/congviec`)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },
}