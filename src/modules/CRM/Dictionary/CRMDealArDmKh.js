import React, { useEffect } from "react";
import { ZenField, ZenFieldSelectApi } from "../../../components/Control";
import { FormMode } from "../../../utils";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { ApiCRMDeal } from "../Api/index";
import { IntlFormat } from "../../../utils/intlFormat";
import { Mess_ARDmKh } from "../../AR/zLanguage/variable";

const FormModal = ({ formik, permission, mode, infoModal, onAddParamsApi }) => {
   const { other } = infoModal
   useEffect(() => {
      onAddParamsApi({ deal_id: other.id })
   }, [other])

   const handleItemSelected = (item, { name }) => {
      // set các filed để view
      formik.setFieldValue("tel", item.tel)
      formik.setFieldValue("email", item.email)
      formik.setFieldValue("ten_kh", item.ten_kh)
      formik.setFieldValue("ma_kh", item.ma_kh)
   }

   return <React.Fragment>
      <ZenFieldSelectApi loadApi required autoFocus formik={formik}
         readOnly={!permission || (mode === FormMode.ADD ? false : true)}
         lookup={{ ...ZenLookup.Ma_kh, cols: ZenLookup.Ma_kh.cols + ",tel,email" }}
         name="object_code"
         onItemSelected={handleItemSelected}
         {...IntlFormat.label(Mess_ARDmKh.Ma)}
      />
      <ZenField readOnly
         label={"Email"} defaultlabel="Email"
         name="email" props={formik}
      />
      <ZenField readOnly
         name="tel" props={formik}
         {...IntlFormat.label(Mess_ARDmKh.Tel)}
      />
   </React.Fragment>
}

export const CRMDealArDmKh = {
   FormModal: FormModal,

   fieldCode: "id",
   api: {
      url: ApiCRMDeal,
      insert: "insertArDmKhForDeal",
   },
   permission: {
      view: "",
      add: "",
      edit: ""
   },
   formId: "crmcompanycontactdeal-form",
   size: "tiny",
   initItem: {
      object_code: "",
      ksd: false
   },
   formValidation: [
      {
         id: "object_code",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
         ]
      },
   ]
}