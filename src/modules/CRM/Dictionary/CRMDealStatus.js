import React from "react";
import { ZenField, ZenFieldTextArea } from "../../../components/Control/index";
import { FormMode } from "../../../utils";
import { IntlFormat } from "../../../utils/intlFormat";
import { ApiCRMDealStatus } from "../Api";
import { Mess_CRMDealStatus } from "../zLanguage/variable";

const FormModal = (props) => {
   const { formik, permission, mode } = props

   return <React.Fragment>
      <ZenField required autoFocus readOnly={!permission || (mode === FormMode.ADD ? false : true)}
         name="ma_trang_thai" props={formik}
         textCase="uppercase" //restrictChar={["/","\\"]}
         {...IntlFormat.label(Mess_CRMDealStatus.Ma)}
         isCode
      />
      <ZenField required readOnly={!permission}
         name="ten_trang_thai" props={formik}
         {...IntlFormat.label(Mess_CRMDealStatus.Ten)}
      />
      <ZenFieldTextArea readOnly={!permission}
         name="ghi_chu" props={formik}
         {...IntlFormat.label(Mess_CRMDealStatus.GhiChu)}
      />
   </React.Fragment>
}

const CRMDealStatus = {
   FormModal: FormModal,
   api: {
      url: ApiCRMDealStatus,
   },
   permission: {
      view: "",
      add: "",
      edit: ""
   },
   formId: "crmdealstatus-form",
   size: "tiny",
   initItem: {
      ma_trang_thai: "",
      ten_trang_thai: "",
      ghi_chu: "",
      ksd: false
   },
   formValidation: [
      {
         id: "ma_trang_thai",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
            {
               type: "max",
               params: [8, "Không được vượt quá 8 kí tự"]
            },
         ]
      },
      {
         id: "ten_trang_thai",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
         ]
      },
   ]
}

export { CRMDealStatus };