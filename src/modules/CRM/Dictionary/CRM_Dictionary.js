import React from "react";
import { Table } from "semantic-ui-react";
import * as routes from "../../../constants/routes";
import { auth } from "../../../utils";
import { IntlFormat } from "../../../utils/intlFormat";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import {
    ApiCRMCompanyStatus, ApiCRMLead,
    ApiCRMDealGiaiDoan, ApiCRMDealLyDoThua,
    ApiCRMDealStatus, ApiCRMDealUuTien, ApiCRMKenhBH,
    ApiCRMLinhVuc, ApiCRMNguonKh, ApiCRMQuyMo, ApiCRMDeal, ApiCRMSetupHoatDong
} from "../Api/index";
//import { optionsLoaiGiaiDoan } from "../Component/zConstVariable";
import {
    Mess_CRMDeal,
    Mess_CRMCompanyStatus, Mess_CRMDealStatus,
    Mess_CRMDealUuTien, Mess_CRMDealLyDoThua, Mess_CRMGiaiDoan,
    Mess_CRMKenhBH, Mess_CRMLinhVuc, Mess_CRMNguonKh, Mess_CRMQuyMo, Mess_CRMLead, Mess_CRMHoatDong
} from "../zLanguage/variable";
import * as permissions from "../../../constants/permissions";

import { optionsLoaiGiaiDoan } from "../Component/zConstVariable";

export const CRM_Dictionary = {
    CRMNguonKh: {
        route: routes.CRMNguonKh,

        action: {
            view: { visible: true, permission: permissions.CRMNguonKhXem },
            add: { visible: true, permission: permissions.CRMNguonKhThem },
            edit: { visible: true, permission: permissions.CRMNguonKhSua },
            del: { visible: true, permission: permissions.CRMNguonKhXoa }
        },

        linkHeader: {
            ...IntlFormat.default(Mess_CRMNguonKh.List),
            active: true,
            isSetting: true,
        },

        tableList: {
            keyForm: "CRMNguonKh",
            formModal: ZenLookup.Ma_nguon_kh,
            fieldCode: "ma_nguon_kh",
            unPagination: false,

            api: {
                url: ApiCRMNguonKh,
                type: "sql",
            },
            columns: [
                {
                    ...IntlFormat.default(Mess_CRMNguonKh.Ma),
                    fieldName: "ma_nguon_kh", type: "string", filter: "string", sorter: true, editForm: true
                },
                {
                    ...IntlFormat.default(Mess_CRMNguonKh.Ten),
                    fieldName: "ten_nguon_kh", type: "string", filter: "string", sorter: true,
                },
                {id: "PMDmGiaiDoanDa.ordinal", defaultMessage: "Thứ tự hiển thị", fieldName: "ordinal", filter: "number", sorter: true, },
                { id: "ksd", defaultMessage: "Không sử dụng", fieldName: "ksd", type: "bool", filter: "", sorter: true },
            ],
        },
    },
    CRMLinhVuc: {
        route: routes.CRMLinhVuc,

        action: {
            view: { visible: true, permission: permissions.CRMLinhVucXem },
            add: { visible: true, permission: permissions.CRMLinhVucThem },
            edit: { visible: true, permission: permissions.CRMLinhVucSua },
            del: { visible: true, permission: permissions.CRMLinhVucXoa }
        },

        linkHeader: {
            ...IntlFormat.default(Mess_CRMLinhVuc.List),
            active: true,
            isSetting: true,
        },

        tableList: {
            keyForm: "CRMLinhVuc",
            formModal: ZenLookup.Ma_linh_vuc,
            fieldCode: "ma_linh_vuc",
            unPagination: false,

            api: {
                url: ApiCRMLinhVuc,
                type: "sql",
            },
            columns: [
                {
                    ...IntlFormat.default(Mess_CRMLinhVuc.Ma), fieldName: "ma_linh_vuc",
                    type: "string", filter: "string", sorter: true, editForm: true,
                },
                {
                    ...IntlFormat.default(Mess_CRMLinhVuc.Ten), fieldName: "ten_linh_vuc",
                    type: "string", filter: "string", sorter: true, editForm: true,
                },
                {id: "PMDmGiaiDoanDa.ordinal", defaultMessage: "Thứ tự hiển thị", fieldName: "ordinal", filter: "number", sorter: true, },
                {
                    ...IntlFormat.default(Mess_CRMLinhVuc.GhiChu), fieldName: "ghi_chu",
                    type: "string", filter: "string", sorter: true, textarea: true
                },
                {
                    id: "ksd", defaultMessage: "Không sử dụng", fieldName: "ksd",
                    type: "bool", filter: "", sorter: true
                },
            ],
        },
    },

    CRMLoaiHoatDong: {
        route: routes.CRMLoaiHoatDong,

        action: {
            view: { visible: true, permission: permissions.CRMLoaiHoatDongXem },
            add: { visible: true, permission: permissions.CRMLoaiHoatDongThem },
            edit: { visible: true, permission: permissions.CRMLoaiHoatDongSua },
            del: { visible: true, permission: permissions.CRMLoaiHoatDongXoa }
        },

        linkHeader: {
            ...IntlFormat.default(Mess_CRMHoatDong.List),
            active: true,
            isSetting: true,
        },

        tableList: {
            keyForm: "CRMLoaiHoatDong",
            formModal: ZenLookup.Ma_hoat_dong,
            fieldCode: "ma_hoat_dong",
            unPagination: false,

            api: {
                url: ApiCRMSetupHoatDong,
                type: "sql",
            },
            columns: [
                {
                    ...IntlFormat.default(Mess_CRMHoatDong.Ma),
                    fieldName: "ma_hoat_dong", type: "string", filter: "string", sorter: true, editForm: true
                },
                {
                    ...IntlFormat.default(Mess_CRMHoatDong.Ten),
                    fieldName: "ten_hoat_dong", type: "string", filter: "string", sorter: true,
                },
                {id: "PMDmGiaiDoanDa.ordinal", defaultMessage: "Thứ tự hiển thị", fieldName: "ordinal", filter: "number", sorter: true, },
                {
                    ...IntlFormat.default(Mess_CRMHoatDong.KetQua),
                    fieldName: "ket_qua", type: "string", filter: "string", sorter: true,
                },
                { id: "ksd", defaultMessage: "Không sử dụng", fieldName: "ksd", type: "bool", filter: "", sorter: true },
            ],
        },
    },
    CRMDeal: {
        route: routes.CRMDeal,

        action: {
            view: { visible: true, permission: permissions.DEALXem },
            add: { visible: true, permission: permissions.DEALThem },
            edit: { visible: true, permission: permissions.DEALSua },
            del: { visible: true, permission: permissions.DEALXoa }
        },

        linkHeader: {
            id:"crmdeal.list",
            defaultMessage:"Cơ hội bán hàng",
            active: true,
            isSetting: false,
        },

        tableList: {
            keyForm: "crmdeal",
            formModal: ZenLookup.CRMDeal,
            fieldCode: "id",
            unPagination: false,

            api: {
                url: ApiCRMDeal,
                type: "sql",
            },
            onBeforeCallApiGet: (where) => {
                const { qfFilter, paramsApi } = where
                const userInfo = auth.getUserInfo()
                return new Promise(resolve => {
                    // là admin get hết
                    if (userInfo.admin.toLowerCase() == "true") {
                        resolve(where)
                        return
                    }

                    // get theo ma_nvns
                    const qf = `a.id IN (SELECT deal_id from crmdealnv where ma_nvns = '${userInfo.hrid}')`
                    if (qfFilter.qf) {
                        qfFilter.qf += ` and (${qf})`
                    } else {
                        qfFilter.qf = qf
                    }
                    resolve({ ...where, qfFilter: qfFilter })
                })
            },
            columns: [
                {
                    id: "crmdeal.ma", defaultMessage: "Mã deal",
                    fieldName: "ma_deal", type: "string", filter: "string", sorter: true,
                    link: { route: routes.CRMDealDetail(), params: "id" }
                },
                {
                    ...IntlFormat.default(Mess_CRMDeal.TenDeal),
                    fieldName: "ten_deal", type: "string", filter: "string", sorter: true,
                    link: { route: routes.CRMDealDetail(), params: "id" }
                },
                {
                    id: "crmdeal.nhu_cau", defaultMessage: "Sản phẩm/dịch vụ",
                    fieldName: "nhu_Cau", type: "string", filter: "string", sorter: true,
                },
                {
                    id: "crmdeal.giai_doan", defaultMessage: "Giai đoạn",
                    fieldName: "ma_giai_doan", type: "string", filter: "string", sorter: true,
                },
                {
                    ...IntlFormat.default(Mess_CRMDeal.GiaTriDk),
                    fieldName: "gia_tri_dk", type: "number", filter: "number", sorter: true,
                },
                {
                    id: "crmdeal.ngay_dong_dk", defaultMessage: "Ngày chốt dự kiến",
                    fieldName: "ngay_dong_dk", type: "date", filter: "date", sorter: true,
                },
                 {
                    id: "crmdeal.ma_kh", defaultMessage: "Khách hàng",
                    fieldName: "ma_kh", type: "string", filter: "string", sorter: true,
                },
                {
                    id: "crmdeal.ma_nvns_owner", defaultMessage: "Nhân viên phụ trách",
                    fieldName: "ma_nvns_owner", type: "string", filter: "string", sorter: true,
                },
                {
                    id: "ksd", defaultMessage: "Không sử dụng", fieldName: "ksd",
                    type: "bool", filter: "", sorter: true
                },
            ],
        },
    },

    CRMDealUuTien: {
        route: routes.CRMDealUuTien,

        action: {
            view: { visible: true, permission: permissions.CRMDealUuTienXem },
            add: { visible: true, permission: permissions.CRMDealUuTienThem },
            edit: { visible: true, permission: permissions.CRMDealUuTienSua },
            del: { visible: true, permission: permissions.CRMDealUuTienXoa }
        },

        linkHeader: {
            ...IntlFormat.default(Mess_CRMDealUuTien.List),
            active: true,
            isSetting: true,
        },

        tableList: {
            keyForm: "CRMDealUuTien",
            formModal: ZenLookup.Ma_uu_tien,
            fieldCode: "ma_uu_tien",
            unPagination: false,

            api: {
                url: ApiCRMDealUuTien,
                type: "sql",
            },
            columns: [
                {
                    ...IntlFormat.default(Mess_CRMDealUuTien.Ma), fieldName: "ma_uu_tien",
                    type: "string", filter: "string", sorter: true, editForm: true,
                },
                {
                    ...IntlFormat.default(Mess_CRMDealUuTien.Ten), fieldName: "ten_uu_tien",
                    type: "string", filter: "string", sorter: true, editForm: true,
                },
                {
                    ...IntlFormat.default(Mess_CRMDealUuTien.Ordinal), fieldName: "ordinal",
                    type: "number", filter: "number", sorter: true, propsCell: { textAlign: "left" }
                },
                {
                    ...IntlFormat.default(Mess_CRMDealUuTien.GhiChu), fieldName: "ghi_chu",
                    type: "string", filter: "string", sorter: true, textarea: true
                },
                {
                    id: "ksd", defaultMessage: "Không sử dụng", fieldName: "ksd",
                    type: "bool", filter: "", sorter: true
                },
            ],
        },
    },
    CRMDealLyDoThua: {
        route: routes.CRMDealLyDoThua,

        action: {
            view: { visible: true, permission: "" },
            add: { visible: true, permission: "" },
            edit: { visible: true, permission: "" },
            del: { visible: true, permission: "" }
        },

        linkHeader: {
            ...IntlFormat.default(Mess_CRMDealLyDoThua.List),
            active: true,
            isSetting: false,
        },

        tableList: {
            keyForm: "CRMDealLyDoThua",
            formModal: ZenLookup.Ma_ly_do,
            fieldCode: "ma_ly_do",
            unPagination: false,

            api: {
                url: ApiCRMDealLyDoThua,
                type: "sql",
            },
            columns: [
                {
                    ...IntlFormat.default(Mess_CRMDealLyDoThua.Ma), fieldName: "ma_ly_do",
                    type: "string", filter: "string", sorter: true, editForm: true,
                },
                {
                    ...IntlFormat.default(Mess_CRMDealLyDoThua.Ten), fieldName: "ten_ly_do",
                    type: "string", filter: "string", sorter: true, editForm: true,
                },
                {
                    ...IntlFormat.default(Mess_CRMDealLyDoThua.Ordinal), fieldName: "ordinal",
                    type: "number", filter: "number", sorter: true, propsCell: { textAlign: "left" }
                },
                {
                    ...IntlFormat.default(Mess_CRMDealLyDoThua.GhiChu), fieldName: "ghi_chu",
                    type: "string", filter: "string", sorter: true, textarea: true
                },
                {
                    id: "ksd", defaultMessage: "Không sử dụng", fieldName: "ksd",
                    type: "bool", filter: "", sorter: true
                },
            ],
        },
    },
    CRMDealGiaiDoan: {
        route: routes.CRMDealGiaiDoan,

        action: {
            view: { visible: true, permission: permissions.CRMDealGiaiDoanXem },
            add: { visible: true, permission: permissions.CRMDealGiaiDoanThem },
            edit: { visible: true, permission: permissions.CRMDealGiaiDoanSua },
            del: { visible: true, permission: permissions.CRMDealGiaiDoanXoa }
        },

        linkHeader: {
            ...IntlFormat.default(Mess_CRMGiaiDoan.List),
            active: true,
            isSetting: true,
        },

        tableList: {
            keyForm: "CRMDealGiaiDoan",
            formModal: ZenLookup.Ma_giai_doan,
            fieldCode: "ma_giai_doan",
            unPagination: false,

            api: {
                url: ApiCRMDealGiaiDoan,
                type: "sql",
            },
            customCols: (data, rowItem, fieldName, infoCol) => {
                let customCell = undefined
                if (fieldName === 'loai') {
                    const loaiGD = optionsLoaiGiaiDoan.find(t => t.value == rowItem["loai"]) || {}
                    customCell = <Table.Cell key={fieldName}>
                        {loaiGD.text || rowItem["loai"]}
                    </Table.Cell>
                }
                return customCell
            },
            columns: [
                {
                    ...IntlFormat.default(Mess_CRMGiaiDoan.Ma), fieldName: "ma_giai_doan",
                    type: "string", filter: "string", sorter: true, editForm: true,
                },
                {
                    ...IntlFormat.default(Mess_CRMGiaiDoan.Ten), fieldName: "ten_giai_doan",
                    type: "string", filter: "string", sorter: true, editForm: true,
                },
                {
                    ...IntlFormat.default(Mess_CRMGiaiDoan.Loai), fieldName: "loai",
                    type: "string", sorter: true, custom: true,
                    filter: "list", listFilter: optionsLoaiGiaiDoan
                },
                {
                    ...IntlFormat.default(Mess_CRMGiaiDoan.Ordinal), fieldName: "ordinal",
                    type: "number", filter: "number", sorter: true, propsCell: { textAlign: "left" }
                },
                {
                    ...IntlFormat.default(Mess_CRMGiaiDoan.GhiChu), fieldName: "ghi_chu",
                    type: "string", filter: "string", sorter: true, textarea: true
                },
                {
                    id: "ksd", defaultMessage: "Không sử dụng", fieldName: "ksd",
                    type: "bool", filter: "", sorter: true
                },
            ],
        },
    },
    CRMLead: {
        route: routes.CRMLead,

        action: {
            view: { visible: true, permission: "" },
            add: { visible: true, permission: "" },
            edit: { visible: true, permission: "" },
            del: { visible: true, permission: "" }
        },

        linkHeader: {
            ...IntlFormat.default(Mess_CRMLead.List),
            active: true,
            isSetting: false,
        },

        tableList: {
            keyForm: "CRMLead",
            formModal: ZenLookup.CRMLead,
            fieldCode: "id",
            unPagination: false,

            api: {
                url: ApiCRMLead,
                type: "sql",
            },
            onBeforeCallApiGet: (where) => {
                const { qfFilter, paramsApi } = where
                const userInfo = auth.getUserInfo()
                return new Promise(resolve => {
                    // là admin get hết
                    if (userInfo.admin.toLowerCase() == "true") {
                        resolve(where)
                        return
                    }

                    // get theo ma_nvns
                    const qf = `a.id IN (SELECT lead_id from crmleadnv where ma_nvns = '${userInfo.hrid}')`
                    if (qfFilter.qf) {
                        qfFilter.qf += ` and (${qf})`
                    } else {
                        qfFilter.qf = qf
                    }
                    resolve({ ...where, qfFilter: qfFilter })
                })
            },
            columns: [
                { ...IntlFormat.default(Mess_CRMLead.DanhXung), fieldName: "danh_xung", type: "string", filter: "string", sorter: true, },
                {
                    ...IntlFormat.default(Mess_CRMLead.LienHe), fieldName: "lien_he", type: "string", filter: "string", sorter: true,
                    link: { route: routes.CRMLeadDetail(), params: "id" }
                },
                { ...IntlFormat.default(Mess_CRMLead.DienThoai), fieldName: "dien_thoai", type: "string", filter: "string", sorter: true, },
                { text: "Email", fieldName: "email", type: "string", filter: "string", sorter: true, },
                { ...IntlFormat.default(Mess_CRMLead.DealTen), fieldName: "deal_ten", type: "string", filter: "string", sorter: true, },
                { ...IntlFormat.default(Mess_CRMLead.ChucDanh), fieldName: "chuc_danh", type: "string", filter: "string", sorter: true, },
                { ...IntlFormat.default(Mess_CRMLead.BoPhan), fieldName: "bo_phan", type: "string", filter: "string", sorter: true, },
                { ...IntlFormat.default(Mess_CRMLead.CtyTen), fieldName: "cty_ten", type: "string", filter: "string", sorter: true, },
                { ...IntlFormat.default(Mess_CRMLead.MaNvnsOwner), fieldName: "ma_nvns_owner", type: "string", filter: "string", sorter: true, },
            ],
        },
    },
}