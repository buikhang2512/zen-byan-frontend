import React from "react";
import { Form } from "semantic-ui-react";
import { ZenField, ZenFieldNumber, ZenFieldSelect, ZenFieldTextArea } from "../../../components/Control/index";
import { FormMode } from "../../../utils";
import { ApiCRMDealGiaiDoan } from "../Api";
import { Mess_CRMGiaiDoan } from "../zLanguage/variable";
import { optionsLoaiGiaiDoan } from "../Component/zConstVariable";
import { IntlFormat } from "../../../utils/intlFormat";
import * as permissions from "../../../constants/permissions";

const FormModal = (props) => {
   const { formik, permission, mode } = props
   return <>
      <Form.Group>
         <ZenField required autoFocus readOnly={!permission || (mode === FormMode.ADD ? false : true)}
            name="ma_giai_doan" props={formik}
            textCase="uppercase" width="10" 
            //restrictChar={["/","\\"]}
            {...IntlFormat.label(Mess_CRMGiaiDoan.Ma)}
            isCode
         />
         <ZenFieldNumber readOnly={!permission}
            name="ordinal" props={formik}
            width="6"
            {...IntlFormat.label(Mess_CRMGiaiDoan.Ordinal)}
         />
      </Form.Group>
      <ZenField required readOnly={!permission}
         name="ten_giai_doan" props={formik}
         {...IntlFormat.label(Mess_CRMGiaiDoan.Ten)}
      />
      <ZenFieldSelect readOnly={!permission} options={optionsLoaiGiaiDoan}
         name="loai" props={formik} search={false}
         {...IntlFormat.label(Mess_CRMGiaiDoan.Loai)}
      />
      <ZenFieldTextArea readOnly={!permission}
         name="ghi_chu" props={formik}
         {...IntlFormat.label(Mess_CRMGiaiDoan.GhiChu)}
      />
   </>
}

export const CRMDealGiaiDoan = {
   FormModal: FormModal,
   api: {
      url: ApiCRMDealGiaiDoan,
   },
   permission: {
      view: permissions.CRMDealGiaiDoanXem,
      add: permissions.CRMDealGiaiDoanThem,
      edit: permissions.CRMDealGiaiDoanSua
   },
   formId: "crmdealgiaidoan-form",
   size: "tiny",
   initItem: {
      ma_giai_doan: "",
      ten_giai_doan: "",
      loai: "",
      ghi_chu: "",
      ordinal: "",
      ksd: false
   },
   formValidation: [
      {
         id: "ma_giai_doan",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
            {
               type: "max",
               params: [8, "Không được vượt quá 8 kí tự"]
            },
         ]
      },
      {
         id: "ten_giai_doan",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
         ]
      },
   ]
}