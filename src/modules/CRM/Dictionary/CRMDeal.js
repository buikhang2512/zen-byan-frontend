import React, { useCallback, useEffect,useState } from "react";
import { Form } from "semantic-ui-react";
import { ZenField, ZenFieldDate, ZenFieldNumber, ZenFieldSelectApi, ZenFieldTextArea, ZenButton } from "../../../components/Control/index";
import { FormMode, zzControlHelper } from "../../../components/Control/zzControlHelper";
import { ZenHelper } from "../../../utils";
import { IntlFormat } from "../../../utils/intlFormat";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { ApiCRMDeal } from "../Api";
import { Mess_CRMDeal } from "../zLanguage/variable";
import * as routes from '../../../constants/routes';
import { getModal } from "../../ComponentInfo/Dictionary/ZenGetModal";
import * as permissions from "../../../constants/permissions";

const FormModal = ({ formik, permission, mode, infoModal }) => {
   const { other } = infoModal
   useEffect(() => {
      if (other && other.id) {
         formik.setFieldValue("object_link", {
            object_id: other.fieldCode === "id" ? other.id : "",
            object_code: other.fieldCode !== "id" ? other.id : "",
            object_type: other.type
         })
      }
   }, [other])

   const handleItemSelected = (option, { name }) => {
      if (name === "ma_uu_tien") {
         formik.setFieldValue("ten_uu_tien", option.ten_uu_tien)
      } else if (name === "ma_giai_doan") {
         formik.setFieldValue("ten_giai_doan", option.ten_giai_doan)
      }
   }

   return <>
      <ZenField required width={8} autoFocus readOnly={!permission}
         name="ma_deal" props={formik}
         label={'crmdeal.madeal'} defaultlabel="Mã cơ hội"
         isCode
      />
      <ZenField readOnly={!permission}
         name="ten_deal" props={formik}
         label={'crmdeal.madeal'} defaultlabel="Tên cơ hội"
      />
      <ZenField readOnly={!permission}
         name="nhu_cau" props={formik}
         label={'crmdeal.nhucau'} defaultlabel="Sản phẩm dịch vụ có nhu cầu"
      />
      <ZenFieldSelectApi readOnly={!permission}
         lookup={ZenLookup.Ma_kh} loadApi
         name="ma_kh" formik={formik}
         label={'crmdeal.ma_kh'} defaultlabel="Khách hàng"
      />
      <Form.Group widths="equal">
         <ZenFieldNumber readOnly={!permission}
            name="gia_tri_dk" props={formik}
            {...IntlFormat.label(Mess_CRMDeal.GiaTriDk)}
         />

         <ZenFieldDate readOnly={!permission}
            name="ngay_dong_dk" props={formik}
            label={'crmdeal.ngay_dong_dk'} defaultlabel="Ngày đóng dự kiến"
         />
      </Form.Group>
      <Form.Group widths="equal">
      <ZenFieldSelectApi readOnly={!permission}
         lookup={ZenLookup.Ma_uu_tien}
         name="ma_uu_tien" formik={formik}
         onItemSelected={handleItemSelected}
         {...IntlFormat.label(Mess_CRMDeal.MaUuTien)}
      />
      <ZenFieldSelectApi readOnly={!permission}
         lookup={ZenLookup.Ma_giai_doan}
         name="ma_giai_doan" formik={formik}
         onItemSelected={handleItemSelected}
         {...IntlFormat.label(Mess_CRMDeal.MaGiaiDoan)}
      />
      </Form.Group>
      <Form.Group widths="equal">
         <ZenFieldSelectApi width={8}
            lookup={ZenLookup.ID_NV}
            name="ma_nvns_owner" formik={formik}
            label={'crmdeal.ma_nvns_owner'} defaultlabel="Nhân viên phụ trách"
         />
      </Form.Group>
      {/* <ZenFieldSelectApi
         lookup={ZenLookup.User}
         name="ma_nvns_owner" formik={formik}
         readOnly={!permission}
         {...IntlFormat.label(Mess_CRMDeal.MaNvnsOwner)}
      /> */}
      <ZenFieldTextArea readOnly={!permission}
         name="ghi_chu" props={formik}
         {...IntlFormat.label(Mess_CRMDeal.GhiChu)}
      />
   </>
}

const customHeader = (props, state) => {
   const { id, header, fieldCode, formMode, currentItem, intl } = props
   const { valueFieldKey } = state
   const txtHeader = !id ? ('Thêm mới cơ hội bán hàng')
       : ('Cơ hội bán hàng' + ": " + id)
   return txtHeader
 }

export const CRMDeal = {
   FormModal: FormModal,
   customHeader,

   saveAndRedirect: {
      linkto : (item) => {
         const id = zzControlHelper.btoaUTF8(item['id'])
         window.location.assign(`#${routes.CRMDealDetail(id)}`)
      }
   },

   api: {
      url: ApiCRMDeal,
   },
   permission: {
      view: permissions.DEALXem,
      add: permissions.DEALThem,
      edit: permissions.DEALSua
   },
   fieldCode: "id",
   formId: "crmdeal-form",
   size: "small",
   initItem: {
      id: "",
      ksd: false,
      ma_deal: "",
      ten_deal: "",
      nhu_cau: "",
      gia_tri_dk: 0,
      ma_uu_tien: "",
      ma_giai_doan: "",
      ngay_dong_dk: ZenHelper.formatDateTime(new Date(), "YYYY-MM-DD"),
      ngay_dong_tt: "",
      ma_nvns_owner: "",
      ma_ly_do_thua: "",
      ghi_chu: "",
      ma_kh: ""
   },
   formValidation: [
      {
         id: "ma_deal",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
         ]
      },
   ]
}