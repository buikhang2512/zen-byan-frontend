import React from "react"
import { Form } from "semantic-ui-react"
import { ZenField, ZenFieldTextArea,ZenFieldNumber } from "../../../components/Control"
import { FormMode } from "../../../components/Control/zzControlHelper"
import { IntlFormat } from "../../../utils/intlFormat"
import { ApiCRMSetupHoatDong } from "../Api"
import { Mess_CRMHoatDong } from "../zLanguage/variable"
import * as permissions from "../../../constants/permissions";

const CRMLoaiHoatDongModal = (props) => {
    const { formik, permission, mode } = props

   return <>
        <Form.Group widths="equal">
            <ZenField required autoFocus readOnly={!permission || (mode === FormMode.ADD ? false : true)}
                name="ma_hoat_dong" props={formik}
                isCode
                {...IntlFormat.label(Mess_CRMHoatDong.Ma)}
            />
            <ZenFieldNumber
            readOnly={!permission}
            label={"PMDmGiaiDoanDa.ordinal"} defaultlabel="Thứ tự hiển thị"
            name="ordinal" props={formik}
         />
        </Form.Group>
            <ZenField required readOnly={!permission}
                name="ten_hoat_dong" props={formik}
                {...IntlFormat.label(Mess_CRMHoatDong.Ten)}
            />
        <Form.Group>
            <ZenFieldTextArea width={16} readOnly={!permission}
               name="ket_qua" props={formik}
               label={'crm.ket_qua'} defaultlabel={'Kết quả của hoạt động (Các kết quả cách nhau dấu , )'}
            />
        </Form.Group>
   </>
}

export const CRMLoaiHoatDong = {
    FormModal: CRMLoaiHoatDongModal,
    api: {
       url: ApiCRMSetupHoatDong,
    },
    permission: {
       view: permissions.CRMLoaiHoatDongXem,
       add: permissions.CRMLoaiHoatDongThem,
       edit: permissions.CRMLoaiHoatDongSua,
    },
    formId: "crmloaihoatdong-form",
    size: "tiny",
    initItem: {
       ma_hoat_dong: "",
       ten_hoat_dong: "",
       ket_qua:"",
       ordinal:0,
       ksd: false
    },
    formValidation: [
       {
          id: "ma_hoat_dong",
          validationType: "string",
          validations: [
             {
                type: "required",
                params: ["Không được bỏ trống trường này"]
             },
          ]
       },
       {
        id: "ten_hoat_dong",
        validationType: "string",
        validations: [
           {
              type: "required",
              params: ["Không được bỏ trống trường này"]
           },
        ]
     },
    ]
 }