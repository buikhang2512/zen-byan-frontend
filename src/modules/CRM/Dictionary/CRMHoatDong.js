import React, { useEffect, useState } from "react";
import { Form } from "semantic-ui-react";
import { ZenFieldDate, ZenFieldSelect, ZenFieldSelectApi, ZenFieldTextArea } from "../../../components/Control";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { ApiCRMHoatDong } from "../Api/index";
import { Mess_CRMHoatDong } from "../zLanguage/variable";
import { IntlFormat } from "../../../utils/intlFormat";
import { ZenHelper } from "../../../utils";

const FormModal = ({ formik, permission, mode, infoModal }) => {
   const { other } = infoModal
   const [optionsKQ, setOptionsKQ] = useState([])
   useEffect(() => {
      if (other.id) {
         formik.setFieldValue("associate", {
            object_id: other.keyData === "id" ? other.id : "",
            object_code: other.keyData !== "id" ? other.id : "",
            object_type: other.type
         })
      }
   }, [other.id])

   const handleItemSelected = (itemHD, { name }) => {
      formik.setFieldValue("ket_qua", "")
      createOptionForKQ(itemHD)
   }

   const handleInitAfter = (options, { name }) => {
      const itemHD = options.find(t => t.value == formik.values.ma_hoat_dong)
      createOptionForKQ(itemHD)
   }

   function createOptionForKQ(itemHD) {
      if (itemHD) {
         const temp = itemHD.ket_qua ? itemHD.ket_qua.split(",").map((text, idx) => {
            return {
               key: idx,
               value: text.trim(),
               text: text.trim()
            }
         }) : []
         setOptionsKQ(temp)
      }
   }

   return <React.Fragment>
      <ZenFieldSelectApi required autoFocus formik={formik}
         readOnly={!permission}
         lookup={ZenLookup.Ma_hoat_dong} name="ma_hoat_dong"
         onItemSelected={handleItemSelected}
         onInitAfter={handleInitAfter}
         {...IntlFormat.label(Mess_CRMHoatDong.Header)}
      />

      <Form.Group widths="equal">
         <ZenFieldSelect readOnly={!permission}
            name="ket_qua" props={formik} options={optionsKQ}
            {...IntlFormat.label(Mess_CRMHoatDong.KetQua)}
         />
         <ZenFieldDate showTime readOnly={!permission}
            name="thoi_gian" props={formik}
            {...IntlFormat.label(Mess_CRMHoatDong.ThoiGian)} />
      </Form.Group>

      <ZenFieldTextArea readOnly={!permission}
         name="noi_dung" props={formik}
         {...IntlFormat.label(Mess_CRMHoatDong.NoiDung)}
      />
   </React.Fragment>
}

export const CRMHoatDong = {
   FormModal: FormModal,
   fieldCode: "id",
   api: {
      url: ApiCRMHoatDong,
   },
   permission: {
      view: "",
      add: "",
      edit: ""
   },
   formId: "crmhoatdong-form",
   size: "tiny",
   isScrolling: false,

   initItem: {
      ma_hoat_dong: "",
      noi_dung: "",
      ket_qua: "",
      thoi_gian: ZenHelper.formatDateTime(new Date(), "YYYY-MM-DD HH:mm"),
      ksd: false
   },
   formValidation: [
      {
         id: "ma_hoat_dong",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
         ]
      },
   ]
}