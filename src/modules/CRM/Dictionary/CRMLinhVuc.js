import React from "react";
import { Form } from "semantic-ui-react";
import { ZenField, ZenFieldTextArea,ZenFieldNumber } from "../../../components/Control/index";
import { FormMode } from "../../../utils";
import { IntlFormat } from "../../../utils/intlFormat";
import { ApiCRMLinhVuc } from "../Api";
import { Mess_CRMLinhVuc } from "../zLanguage/variable";
import * as permissions from "../../../constants/permissions";

const FormModal = (props) => {
   const { formik, permission, mode } = props

   return <React.Fragment>
      <Form.Group widths="equal">
      <ZenField required autoFocus readOnly={!permission || (mode === FormMode.ADD ? false : true)}
         name="ma_linh_vuc" props={formik}
         textCase="uppercase" //restrictChar={["/","\\"]}
         isCode
         {...IntlFormat.label(Mess_CRMLinhVuc.Ma)}
      />
      <ZenFieldNumber
            readOnly={!permission}
            label={"PMDmGiaiDoanDa.ordinal"} defaultlabel="Thứ tự hiển thị"
            name="ordinal" props={formik}
         />
      </Form.Group>
      <ZenField required readOnly={!permission}
         name="ten_linh_vuc" props={formik}
         {...IntlFormat.label(Mess_CRMLinhVuc.Ten)}
      />
      <ZenFieldTextArea readOnly={!permission}
         name="ghi_chu" props={formik}
         {...IntlFormat.label(Mess_CRMLinhVuc.GhiChu)}
      />
   </React.Fragment>
}

const CRMLinhVuc = {
   FormModal: FormModal,
   api: {
      url: ApiCRMLinhVuc,
   },
   permission: {
      view: permissions.CRMLinhVucXem,
      add: permissions.CRMLinhVucThem,
      edit: permissions.CRMLinhVucSua
   },
   formId: "crmlinhvuc-form",
   size: "tiny",
   initItem: {
      ma_linh_vuc: "",
      ten_linh_vuc: "",
      ghi_chu: "",
      ordinal:0,
      ksd: false
   },
   formValidation: [
      {
         id: "ma_linh_vuc",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
            {
               type: "max",
               params: [8, "Không được vượt quá 8 kí tự"]
            },
         ]
      },
      {
         id: "ten_linh_vuc",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
         ]
      },
   ]
}

export { CRMLinhVuc };