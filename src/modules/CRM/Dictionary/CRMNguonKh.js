import React from "react";
import { Form } from "semantic-ui-react";
import { ZenField,ZenFieldNumber } from "../../../components/Control/index";
import { FormMode } from "../../../utils";
import { IntlFormat } from "../../../utils/intlFormat";
import { ApiCRMNguonKh } from "../Api";
import { Mess_CRMNguonKh } from "../zLanguage/variable";
import * as permissions from "../../../constants/permissions";

const FormModal = (props) => {
   const { formik, permission, mode } = props

   return <React.Fragment>
      <Form.Group widths="equal">
      <ZenField required autoFocus readOnly={!permission || (mode === FormMode.ADD ? false : true)}
         name="ma_nguon_kh" props={formik}
         textCase="uppercase" //restrictChar={["/","\\"]}
         isCode
         {...IntlFormat.label(Mess_CRMNguonKh.Ma)}
      />
      <ZenFieldNumber
            readOnly={!permission}
            label={"PMDmGiaiDoanDa.ordinal"} defaultlabel="Thứ tự hiển thị"
            name="ordinal" props={formik}
         />
      </Form.Group>
      <ZenField required readOnly={!permission}
         name="ten_nguon_kh" props={formik}
         {...IntlFormat.label(Mess_CRMNguonKh.Ten)}
      />
   </React.Fragment>
}

const CRMNguonKh = {
   FormModal: FormModal,
   api: {
      url: ApiCRMNguonKh,
   },
   permission: {
      view: permissions.CRMNguonKhXem,
      add: permissions.CRMNguonKhThem,
      edit: permissions.CRMNguonKhSua
   },
   formId: "crmnguonkh-form",
   size: "tiny",
   initItem: {
      ma_nguon_kh: "",
      ten_nguon_kh: "",
      ordinal:0,
      ksd: false
   },
   formValidation: [
      {
         id: "ma_nguon_kh",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
            {
               type: "max",
               params: [8, "Không được vượt quá 8 kí tự"]
            },
         ]
      },
      {
         id: "ten_nguon_kh",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
         ]
      },
   ]
}

export { CRMNguonKh };