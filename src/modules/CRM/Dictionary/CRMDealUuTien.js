import React from "react";
import { Form } from "semantic-ui-react";
import { ZenField, ZenFieldNumber, ZenFieldTextArea } from "../../../components/Control/index";
import { FormMode } from "../../../utils";
import { IntlFormat } from "../../../utils/intlFormat";
import { ApiCRMDealUuTien } from "../Api";
import { Mess_CRMDealUuTien } from "../zLanguage/variable";
import * as permissions from "../../../constants/permissions";

const FormModal = (props) => {
   const { formik, permission, mode } = props
   return <>
      <Form.Group>
         <ZenField required autoFocus readOnly={!permission || (mode === FormMode.ADD ? false : true)}
            name="ma_uu_tien" props={formik}
            textCase="uppercase" width="10" //restrictChar={["/","\\"]}
            {...IntlFormat.label(Mess_CRMDealUuTien.Ma)}
            isCode
         />
         <ZenFieldNumber readOnly={!permission}
            name="ordinal" props={formik}
            width="6"
            {...IntlFormat.label(Mess_CRMDealUuTien.Ordinal)}
         />
      </Form.Group>
      <ZenField required readOnly={!permission}
         name="ten_uu_tien" props={formik}
         {...IntlFormat.label(Mess_CRMDealUuTien.Ten)}
      />
      <ZenFieldTextArea readOnly={!permission}
         name="ghi_chu" props={formik}
         {...IntlFormat.label(Mess_CRMDealUuTien.GhiChu)}
      />
   </>
}

export const CRMDealUuTien = {
   FormModal: FormModal,
   api: {
      url: ApiCRMDealUuTien,
   },
   permission: {
      view: permissions.CRMDealUuTienXem,
      add: permissions.CRMDealUuTienThem,
      edit: permissions.CRMDealUuTienSua
   },
   formId: "crmdealuutien-form",
   size: "tiny",
   initItem: {
      ma_uu_tien: "",
      ten_uu_tien: "",
      ghi_chu: "",
      ordinal: "",
      ksd: false
   },
   formValidation: [
      {
         id: "ma_uu_tien",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
            {
               type: "max",
               params: [8, "Không được vượt quá 8 kí tự"]
            },
         ]
      },
      {
         id: "ten_uu_tien",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
         ]
      },
   ]
}