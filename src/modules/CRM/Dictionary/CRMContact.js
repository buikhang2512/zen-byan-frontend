import React, { useEffect } from "react";
import { ZenField, ZenFieldTextArea } from "../../../components/Control/index";
import { ApiCRMContact } from "../Api/ApiCRMContact";
import { Form } from "semantic-ui-react";
import { ValidError } from "../../../utils/language/variable";
import { IntlFormat } from "../../../utils/intlFormat";

const FormModal = (props) => {
   const { formik, permission, mode } = props

   useEffect(() => {
      const ma_kh = props.infoModal?.other?.ma_kh
      if (ma_kh) {
         formik.setFieldValue("ma_kh", ma_kh)
      }
   }, [])

   return <React.Fragment>
      <ZenField readOnly={!permission}
         label={"CRMContact.ten_lien_he"} defaultlabel="Tên liên hệ"
         name="ten_lien_he" props={formik}
      />

      <Form.Group widths="equal">
         <ZenField readOnly={!permission}
            isIntl={false} label={"Email"}
            name="email" props={formik}
         />
         <ZenField readOnly={!permission}
            label={"CRMContact.dien_thoai"} defaultlabel="Điện thoại"
            name="dien_thoai" props={formik}
         />
      </Form.Group>

      <ZenField readOnly={!permission}
         label={"CRMContact.dia_chi"} defaultlabel="Địa chỉ"
         name="dia_chi" props={formik}
      />

      <ZenField readOnly={!permission}
         label={"CRMContact.chuc_danh"} defaultlabel="Vị trí/chức vụ công việc"
         name="chuc_danh" props={formik}
      />

      <ZenFieldTextArea readOnly={!permission}
         label={"CRMContact.ghi_chu"} defaultlabel="Ghi chú"
         name="ghi_chu" props={formik}
      />
   </React.Fragment>
}

const CRMContact = {
   FormModal: FormModal,
   api: {
      url: ApiCRMContact,
   },
   permission: {
      view: "",
      add: "",
      edit: ""
   },
   formId: "CRMContact-form",
   size: "small",
   isScrolling: false,
   fieldCode: "id",
   initItem: {
      ten_lien_he: "",
      email: "",
      dien_thoai: "",
      ksd: false
   },
   // isScrolling: false, scroll phần content: default True

   formValidation: [
      {
         ...ValidError.formik({ name: "ten_lien_he", type: "string" },
            { type: "required", intl: IntlFormat.default(ValidError.Required) },
         )
      },
   ]
}

export { CRMContact };