import React from "react";
import { Form } from "semantic-ui-react";
import { ZenField, ZenFieldNumber, ZenFieldTextArea } from "../../../components/Control/index";
import { FormMode } from "../../../utils";
import { IntlFormat } from "../../../utils/intlFormat";
import { ApiCRMDealLyDoThua } from "../Api";
import { Mess_CRMDealLyDoThua } from "../zLanguage/variable";

const FormModal = (props) => {
   const { formik, permission, mode } = props
   return <>
      <Form.Group>
         <ZenField required autoFocus readOnly={!permission || (mode === FormMode.ADD ? false : true)}
            name="ma_ly_do" props={formik}
            textCase="uppercase" width="10" //restrictChar={["/","\\"]}
            {...IntlFormat.label(Mess_CRMDealLyDoThua.Ma)}
            isCode
         />
         <ZenFieldNumber readOnly={!permission}
            name="ordinal" props={formik}
            width="6"
            {...IntlFormat.label(Mess_CRMDealLyDoThua.Ordinal)}
         />
      </Form.Group>
      <ZenField required readOnly={!permission}
         name="ten_ly_do" props={formik}
         {...IntlFormat.label(Mess_CRMDealLyDoThua.Ten)}
      />
      <ZenFieldTextArea readOnly={!permission}
         name="ghi_chu" props={formik}
         {...IntlFormat.label(Mess_CRMDealLyDoThua.GhiChu)}
      />
   </>
}

export const CRMDealLyDoThua = {
   FormModal: FormModal,
   api: {
      url: ApiCRMDealLyDoThua,
   },
   permission: {
      view: "",
      add: "",
      edit: ""
   },
   formId: "crmdeallydothua-form",
   size: "tiny",
   initItem: {
      ma_ly_do: "",
      ten_ly_do: "",
      ghi_chu: "",
      ordinal: "",
      ksd: false
   },
   formValidation: [
      {
         id: "ma_ly_do",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
            {
               type: "max",
               params: [8, "Không được vượt quá 8 kí tự"]
            },
         ]
      },
      {
         id: "ten_ly_do",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
         ]
      },
   ]
}