import axios from '../../../Api/axios';

const ExtName = "ArDmKh"

export const ApiArDmKh = {
   get(callback, filter = {}, pagination = {}, params = {}) {
      axios.get(`${ExtName}`, {
         params: {
            qf: filter.qf,
            keyword: filter.keyword,
            sort: filter.sort,
            page: pagination.page,
            pageSize: pagination.pageSize,
            id_nv: params.id_nv
         },
      })
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   getByCode(code, callback) {
      axios.get(`${ExtName}/` + code)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   insert(data, callback) {
      axios.post(`${ExtName}`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   update(data, callback) {
      axios.put(`${ExtName}`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   updatePatch(code, data, callback) {
      axios.patch(`${ExtName}/${code}`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   delete(code, callback) {
      axios.delete(`${ExtName}/` + code)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   getContactByCode(code, callback) {
      axios.get(`${ExtName}/${code}/contact `)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },
   getDeal(code, callback) {
      axios.get(`${ExtName}/${code}/deals`)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },
   getCongViecByKh(ma_kh, callback) {
      axios.get(`${ExtName}/${ma_kh}/congviec`)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   setAccount(data, callback) {
      axios.post(`${ExtName}/${data.ma_kh}/account?user=${data.user}`)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   }
}