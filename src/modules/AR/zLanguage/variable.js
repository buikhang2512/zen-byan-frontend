const prefixARDmKh = "ARDmKh."

export const Mess_ARDmKh = {
   List: {
      id: prefixARDmKh,
      defaultMessage: "Danh sách khách hàng"
   },
   Header: {
      id: prefixARDmKh + "header",
      defaultMessage: "Khách hàng"
   },
   Detail: {
      id: prefixARDmKh + "detail",
      defaultMessage: "Chi tiết khách hàng"
   },
   Ma: {
      id: prefixARDmKh + "ma_kh",
      defaultMessage: "Mã khách hàng"
   },
   Ten: {
      id: prefixARDmKh + "ten_kh",
      defaultMessage: "Tên khách hàng"
   },
   MaSoThue: {
      id: prefixARDmKh + "ma_so_thue",
      defaultMessage: "Mã số thuế"
   },
   DiaChi: {
      id: prefixARDmKh + "dia_chi",
      defaultMessage: "Địa chỉ"
   },
   Tel: {
      id: prefixARDmKh + "tel",
      defaultMessage: "Điện thoại"
   },
   Fax: {
      id: prefixARDmKh + "fax",
      defaultMessage: "Fax"
   },
   Website: {
      id: prefixARDmKh + "home_page",
      defaultMessage: "Website"
   },
   NguoiGD: {
      id: prefixARDmKh + "nguoi_gd",
      defaultMessage: "Người giao dịch"
   },
   Loai: {
      id: prefixARDmKh + "loai",
      defaultMessage: "Loại"
   },
   NhomKh: {
      id: prefixARDmKh + "ma_nhkh",
      defaultMessage: "Nhóm KH"
   },
   MaPlkh1: {
      id: prefixARDmKh + "ma_plkh1",
      defaultMessage: "Mã phân loại KH 1"
   },
   MaPlkh2: {
      id: prefixARDmKh + "ma_plkh2",
      defaultMessage: "Mã phân loại KH 2"
   },
   MaPlkh3: {
      id: prefixARDmKh + "ma_plkh3",
      defaultMessage: "Mã phân loại KH 3"
   },
   HomePage: {
      id: prefixARDmKh + "home_page",
      defaultMessage: "Trang chủ"
   },
   MaHttt: {
      id: prefixARDmKh + "ma_httt",
      defaultMessage: "Mã HTTT"
   },
   MaTt: {
      id: prefixARDmKh + "ma_tt",
      defaultMessage: "ĐK thanh toán"
   },
   GioiHanNo: {
      id: prefixARDmKh + "gh_no",
      defaultMessage: "Giới hạn nợ"
   },
   GhiChu: {
      id: prefixARDmKh + "ghi_chu",
      defaultMessage: "Ghi chú"
   },
   Add: {
      id: prefixARDmKh + "add",
      defaultMessage: "Thêm"
   },
   CreateNew: {
      id: prefixARDmKh + "createNew",
      defaultMessage: "Tạo mới"
   }
}
