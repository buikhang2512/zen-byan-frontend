import { IntlFormat } from "../../../utils/intlFormat";
import { Mess_ARDmKh } from "./variable";

const Vi_VN = {
   ...IntlFormat.setMessageLanguage(Mess_ARDmKh),
}

export default Vi_VN