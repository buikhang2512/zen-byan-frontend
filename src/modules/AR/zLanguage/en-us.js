import { IntlFormat } from "../../../utils/intlFormat"
import { Mess_ARDmKh } from "./variable"

const local = "en"
const En_US = {
   ...IntlFormat.setMessageLanguage(Mess_ARDmKh, local),
}

export default En_US