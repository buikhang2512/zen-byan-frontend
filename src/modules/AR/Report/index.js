import { ARRptBCCN0101 } from "./ARRptBCCN0101"
import { ARRptBCCN0102 } from "./ARRptBCCN0102"
import { ARRptBCCN02 } from "./ARRptBCCN02"
import { ARRptBCCN03 } from "./ARRptBCCN03"

export const AR_Report = {
   ARRptBCCN0101: ARRptBCCN0101,
   ARRptBCCN0102: ARRptBCCN0102,
   ARRptBCCN02: ARRptBCCN02,
   ARRptBCCN03: ARRptBCCN03,
}
