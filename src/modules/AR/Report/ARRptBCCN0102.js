import React from "react";
import * as routes from "../../../constants/routes";
import { Table } from "semantic-ui-react";
import {
  ZenFieldSelectApi,
  FormatDate,
  FormatNumber,
} from "../../../components/Control";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import {
  RptHeader,
  RptTable,
  RptTableCell,
  RptTableRow,
} from "../../../components/Control/zReport";
import { IntlFormat } from "../../../utils/intlFormat";
import { Mess_Report } from "../../ComponentInfo/zLanguage/variable";

const FilterForm = ({ formik }) => {
  const handleItemSelected = (item, { name }) => {
    if (name === "ma_kh") {
      formik.setFieldValue("ten_kh", item.ten_kh);
    } else if (name === "tk") {
      formik.setFieldValue("ten_tk", item.ten_tk);
    }
  };

  return (
    <>
      <ZenFieldSelectApi
        required
        lookup={{
          ...ZenLookup.TK,
          where: "tk_cn = 1",
          onLocalWhere: (items) => {
            return items.filter((t) => t.tk_cn == true);
          },
        }}
        label={"rpt.tk"}
        defaultlabel="Tài khoản"
        name="tk"
        formik={formik}
        onItemSelected={handleItemSelected}
      />

      <ZenFieldSelectApi
        required
        loadApi
        lookup={ZenLookup.Ma_kh}
        label={"ardmkh.header"}
        defaultlabel="Khách hàng"
        name="ma_kh"
        formik={formik}
        onItemSelected={handleItemSelected}
      />

      <ZenFieldSelectApi
        lookup={ZenLookup.Ma_nt}
        label={"rpt.ma_nt"}
        defaultlabel="Ngoại tệ"
        name="ma_nt"
        formik={formik}
      />
    </>
  );
};

const TableForm = ({ data = [], filter = {} }) => {
  return (
    <>
      <RptTable maNt={filter.ma_nt}>
        <Table.Header fullWidth>
          <RptHeader
            maNt={filter.ma_nt}
            header={[
              { text: ["rpt.ngay_ct", "Ngày ct"] },
              { text: ["rpt.so_ct", "Số ct"] },
              { text: ["rpt.dien_giai", "Diễn giải"] },
              { text: ["rpt.tk_du", "TK đ / ư"] },
              { text: ["rpt.ma_vt", "Mã VT"] },
              { text: ["rpt.so_luong", "Số lượng"] },
              { text: ["rpt.gia", "Giá"] },
              { text: ["rpt.tien", "Tiền"] },
              { text: ["rpt.ps_no", "PS nợ"] },
              { text: ["rpt.ps_co", "PS có"] },
              { text: ["rpt.ma_bp", "Mã BP"] },
              { text: ["rpt.ma_hd", "Mã HĐ"] },
              { text: ["rpt.ma_phi", "Mã phí"] },
              { text: ["rpt.ma_spct", "Mã SPCT"] },
              { text: ["rpt.ma_ku", "Mã khế ước"] },
              { text: ["rpt.ma_ct", "Mã ct"] },
            ]}
          />
        </Table.Header>
        <Table.Body>
          {data.length > 0
            ? data.map((item, index) => {
              const isNt =
                filter.ma_nt && filter.ma_nt !== "VND" ? true : false;
              return (
                <RptTableRow
                  key={index}
                  isBold={item.bold}
                  index={index}
                  item={item}
                >
                  <RptTableCell value={item.ngay_ct} />
                  <RptTableCell value={item.so_ct} />
                  <RptTableCell value={item.dien_giai} />
                  <RptTableCell value={item.tk_du} />
                  <RptTableCell value={item.ma_vt} />
                  <RptTableCell value={item.so_luong} />
                  <RptTableCell value={item.gia} />
                  <RptTableCell value={item.tien} />
                  <RptTableCell value={item.ps_no} />
                  <RptTableCell value={item.ps_co} />
                  <RptTableCell value={item.ma_bp} />
                  <RptTableCell value={item.ma_hd} />
                  <RptTableCell value={item.ma_phi} />
                  <RptTableCell value={item.ma_spct} />
                  <RptTableCell value={item.ma_ku} />
                  <RptTableCell value={item.ma_ct} />
                </RptTableRow>
              );
            })
            : undefined}
        </Table.Body>
      </RptTable>
    </>
  );
};

export const ARRptBCCN0102 = {
  FilterForm: FilterForm,
  TableForm: TableForm,
  permission: "06.50.4",
  visible: true, // hiện/ẩn báo cáo
  route: routes.RptARRptBCCN0102,

  period: {
    fromDate: "ngay1",
    toDate: "ngay2",
  },

  linkHeader: {
    id: "ARRptBCCN0102",
    defaultMessage: "Sổ chi tiết công nợ một khách hàng có số lượng",
    active: true,
  },

  info: {
    code: "08.20.14.2",
  },
  initFilter: {
    ngay1: "",
    ngay2: "",
    tk: "131",
    ma_kh: "",
    ma_nt: "",
  },
  columns: [
    {
      ...IntlFormat.default(Mess_Report.NgayCt),
      fieldName: "ngay_ct",
      type: "date",
      sorter: true,
    },
    {
      ...IntlFormat.default(Mess_Report.SoCt),
      fieldName: "so_ct",
      type: "string",
      sorter: true,
    },
    {
      ...IntlFormat.default(Mess_Report.DienGiai),
      fieldName: "dien_giai",
      type: "string",
      sorter: true,
    },
    {
      ...IntlFormat.default(Mess_Report.TkDu),
      fieldName: "tk_du",
      type: "string",
      sorter: true,
    },
    {
      ...IntlFormat.default(Mess_Report.MaVt),
      fieldName: "ma_vt",
      type: "string",
      sorter: true,
    },

    {
      ...IntlFormat.default(Mess_Report.SoLuong),
      fieldName: "so_luong",
      type: "number",
      sorter: true,
    },
    {
      ...IntlFormat.default(Mess_Report.DonGia),
      fieldName: "gia",
      type: "number",
      sorter: true,
    },
    {
      ...IntlFormat.default(Mess_Report.ThanhTien),
      fieldName: "tien",
      type: "string",
      sorter: true,
    },
    {
      ...IntlFormat.default(Mess_Report.MaHd),
      fieldName: "ma_hd",
      type: "string",
      sorter: true,
    },
    {
      ...IntlFormat.default(Mess_Report.MaPhi),
      fieldName: "ma_phi",
      type: "string",
      sorter: true,
    },
    {
      ...IntlFormat.default(Mess_Report.MaSPCT),
      fieldName: "ma_spct",
      type: "string",
      sorter: true,
    },
    {
      ...IntlFormat.default(Mess_Report.MaKu),
      fieldName: "ma_ku",
      type: "string",
      sorter: true,
    },
    {
      ...IntlFormat.default(Mess_Report.MaCt),
      fieldName: "ma_ct",
      type: "string",
      sorter: true,
    },
  ],
  formValidation: [
    {
      id: "ma_kh",
      validationType: "string",
      validations: [
        {
          type: "required",
          params: ["Không được bỏ trống trường này"],
        },
      ],
    },
    {
      id: "tk",
      validationType: "string",
      validations: [
        {
          type: "required",
          params: ["Không được bỏ trống trường này"],
        },
      ],
    },
  ],
};
