import React from "react";
import * as routes from "../../../constants/routes";
import { Table } from "semantic-ui-react";
import {
  ZenFieldSelectApi,
  ZenField,
  FormatNumber,
} from "../../../components/Control";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import {
  RptHeader,
  RptTable,
  RptTableCell,
  RptTableRow,
} from "../../../components/Control/zReport";
import { IntlFormat } from "../../../utils/intlFormat";
import { Mess_Report } from "../../ComponentInfo/zLanguage/variable";

const FilterForm = ({ formik }) => {
  const handleItemSelected = (item, { name }) => {
    if (name === "ma_kh") {
      formik.setFieldValue("ten_kh", item.ten_kh);
    } else if (name === "tk") {
      formik.setFieldValue("ten_tk", item.ten_tk);
    } else if (name === "ma_nhkh") {
      formik.setFieldValue("ten_nhkh", item.ten_nhkh);
    } else if (name === "ma_plvt1") {
      formik.setFieldValue("ten_plvt1", item.ten_plvt);
    } else if (name === "ma_plvt2") {
      formik.setFieldValue("ten_plvt2", item.ten_plvt);
    } else if (name === "ma_plvt3") {
      formik.setFieldValue("ten_plvt3", item.ten_plvt);
    }
  };

  return (
    <>
      <ZenFieldSelectApi
        required
        lookup={{
          ...ZenLookup.TK,
          onLocalWhere: (items) => {
            return items.filter((t) => t.tk_cn == true);
          },
          where: "tk_cn = 1",
        }}
        label={"rpt.tk"}
        defaultlabel="Tài khoản"
        name="tk"
        formik={formik}
        onItemSelected={handleItemSelected}
      />
      <ZenFieldSelectApi
        loadApi
        lookup={ZenLookup.Ma_kh}
        label={"ardmkh.header"}
        defaultlabel="Khách hàng"
        name="ma_kh"
        formik={formik}
        onItemSelected={handleItemSelected}
      />
      <ZenFieldSelectApi
        lookup={ZenLookup.Ma_nhkh}
        label={"rpt.Ma_nhkh"}
        defaultlabel="Nhóm khách hàng"
        name="ma_nhkh"
        formik={formik}
      />
      <ZenFieldSelectApi
        lookup={{
          ...ZenLookup.Ma_plvt,
          onLocalWhere: (items) => {
            return items?.filter(t => t.loai == "1")
          }
        }}
        label={"rpt.Ma_plvt1"} defaultlabel="Phân loại vt 1"
        name="ma_plvt1" formik={formik}
        onItemSelected={handleItemSelected}
      />
      <ZenFieldSelectApi
        lookup={{
          ...ZenLookup.Ma_plvt,
          onLocalWhere: (items) => {
            return items?.filter(t => t.loai == "2")
          }
        }}
        label={"rpt.Ma_plvt2"} defaultlabel="Phân loại vt 2"
        name="ma_plvt2" formik={formik}
        onItemSelected={handleItemSelected}
      />
      <ZenFieldSelectApi
        lookup={{
          ...ZenLookup.Ma_plvt,
          onLocalWhere: (items) => {
            return items?.filter(t => t.loai == "3")
          }
        }}
        label={"rpt.Ma_plvt3"} defaultlabel="Phân loại vt 3"
        name="ma_plvt3" formik={formik}
        onItemSelected={handleItemSelected}
      />
      <ZenField
        label={"rpt.Group1"}
        defaultlabel="Thứ tự PLVT1"
        name="group1"
        props={formik}
      />
      <ZenField
        label={"rpt.Group2"}
        defaultlabel="Thứ tự PLVT2"
        name="group2"
        props={formik}
      />
      <ZenField
        label={"rpt.Group3"}
        defaultlabel="Thứ tự PLVT3"
        name="group3"
        props={formik}
      />
      <ZenFieldSelectApi
        lookup={ZenLookup.Ma_nt}
        label={"rpt.Ma_nt"}
        defaultlabel="Ngoại tệ"
        name="ma_nt"
        formik={formik}
      />
    </>
  );
};

const TableForm = ({ data = [], filter = {} }) => {
  return (
    <>
      <RptTable maNt={filter.ma_nt}>
        <Table.Header fullWidth>
          <RptHeader
            maNt={filter.ma_nt}
            header={[
              { text: ["rpt.ma_kh", "Mã KH"] },
              { text: ["rpt.ten_kh", "Tên KH"] },
              { text: ["rpt.du_no1", "Dư nợ ĐK"] },
              { text: ["rpt.du_co1", "Dư có ĐK"] },
              { text: ["rpt.ps_no", "Ps nợ"] },
              { text: ["rpt.ps_co", "Ps có"] },
              { text: ["rpt.du_no_ck", "Dư nợ CK"] },
              { text: ["rpt.du_co_ck", "Dư có CK"] },
            ]}
          />
        </Table.Header>
        <Table.Body>
          {data.length > 0
            ? data.map((item, index) => {
              const isNt =
                filter.ma_nt && filter.ma_nt !== "VND" ? true : false;
              return (
                <RptTableRow
                  key={index}
                  isBold={item.bold}
                  index={index}
                  item={item}
                >
                  <RptTableCell value={item.ma_kh} />
                  <RptTableCell value={item.ten_kh} />
                  <RptTableCell value={item.du_no1} type="number" />
                  <RptTableCell value={item.du_co1} type="number" />
                  <RptTableCell value={item.ps_no} type="number" />
                  <RptTableCell value={item.ps_co} type="number" />
                  <RptTableCell value={item.du_no_ck} type="number" />
                  <RptTableCell value={item.du_co_ck} type="number" />
                </RptTableRow>
              );
            })
            : undefined}
        </Table.Body>
      </RptTable>
    </>
  );
};

export const ARRptBCCN02 = {
  FilterForm: FilterForm,
  TableForm: TableForm,
  permission: "06.50.2",
  visible: true, // hiện/ẩn báo cáo
  route: routes.RptARRptBCCN02,

  period: {
    fromDate: "ngay1",
    toDate: "ngay2",
  },

  linkHeader: {
    id: "ARRptBCCN02",
    defaultMessage: "Bảng cân đối phát sinh công nợ một tài khoản",
    active: true,
  },

  info: {
    code: "08.20.17.1",
  },
  initFilter: {
    ngay1: "",
    ngay2: "",
    tk: "131",
    ma_kh: "",
    ma_nhkh: "",
    ma_plkh1: "",
    ma_plkh2: "",
    ma_plkh3: "",
    group1: "1",
    group2: "2",
    group3: "3",
    ma_nt: "",
  },
  columns: [
    {
      ...IntlFormat.default(Mess_Report.MaKh),
      fieldName: "ma_kh",
      type: "string",
      sorter: true,
    },
    {
      ...IntlFormat.default(Mess_Report.TenKh),
      fieldName: "ten_kh",
      type: "string",
      sorter: true,
    },
    {
      ...IntlFormat.default(Mess_Report.DuNoDauky),
      fieldName: "du_no1",
      type: "number",
      sorter: true,
    },
    {
      ...IntlFormat.default(Mess_Report.DuCoDauKy),
      fieldName: "du_co1",
      type: "number",
      sorter: true,
    },
    {
      ...IntlFormat.default(Mess_Report.PsNo),
      fieldName: "ps_no",
      type: "number",
      sorter: true,
    },
    {
      ...IntlFormat.default(Mess_Report.PsCo),
      fieldName: "ps_co",
      type: "number",
      sorter: true,
    },
    {
      ...IntlFormat.default(Mess_Report.DuNoCuoiKy),
      fieldName: "du_no_ck",
      type: "number",
      sorter: true,
    },
    {
      ...IntlFormat.default(Mess_Report.DuCoCuoiKy),
      fieldName: "du_co_ck",
      type: "number",
      sorter: true,
    },
  ],

  formValidation: [
    {
      id: "tk",
      validationType: "string",
      validations: [
        {
          type: "required",
          params: ["Không được bỏ trống trường này"],
        },
      ],
    },
  ],
};
