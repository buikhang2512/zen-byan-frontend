import React from "react";
import * as routes from "../../../constants/routes";
import { Table } from "semantic-ui-react";
import {
  ZenFieldSelectApi,
  ZenField,
  FormatNumber,
} from "../../../components/Control";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import {
  RptHeader,
  RptTable,
  RptTableCell,
  RptTableRow,
} from "../../../components/Control/zReport";
import { IntlFormat } from "../../../utils/intlFormat";
import { Mess_Report } from "../../ComponentInfo/zLanguage/variable";

const FilterForm = ({ formik }) => {
  const handleItemSelected = (item, { name }) => {
    if (name === "ma_kh") {
      formik.setFieldValue("ten_kh", item.ten_kh);
    } else if (name === "tk") {
      formik.setFieldValue("ten_tk", item.ten_tk);
    } else if (name === "ma_nhkh") {
      formik.setFieldValue("ten_nhkh", item.ten_nhkh);
    } else if (name === "ma_plkh1") {
      formik.setFieldValue("ten_plkh1", item.ten_plkh);
    } else if (name === "ma_plkh2") {
      formik.setFieldValue("ten_plkh2", item.ten_plkh);
    } else if (name === "ma_plkh3") {
      formik.setFieldValue("ten_plkh3", item.ten_plkh);
    }
  };
  return (
    <>
      <ZenFieldSelectApi
        required
        lookup={{
          ...ZenLookup.TK,
          where: "tk_cn = 1",
          onLocalWhere: (items) => {
            return items.filter((t) => t.tk_cn == true);
          },
        }}
        label={"rpt.tk"}
        defaultlabel="Tài khoản"
        name="tk"
        formik={formik}
        onItemSelected={handleItemSelected}
      />
      <ZenFieldSelectApi
        loadApi
        lookup={ZenLookup.Ma_kh}
        label={"ardmkh.header"}
        defaultlabel="Khách hàng"
        name="ma_kh"
        formik={formik}
        onItemSelected={handleItemSelected}
      />
      <ZenFieldSelectApi
        lookup={ZenLookup.Ma_nhkh}
        label={"rpt.Ma_nhkh"}
        defaultlabel="Nhóm khách hàng"
        name="ma_nhkh"
        formik={formik}
        onItemSelected={handleItemSelected}
      />
      <ZenFieldSelectApi
        lookup={{
          ...ZenLookup.Ma_plkh,
          onLocalWhere: (items) => {
            return items?.filter(t => t.loai == "1") || []
          }
        }}
        label={"rpt.Ma_plkh1"} defaultlabel="Phân loại kh 1"
        name="ma_plkh1" formik={formik}
        onItemSelected={handleItemSelected}
      />
      <ZenFieldSelectApi
        lookup={{
          ...ZenLookup.Ma_plkh,
          onLocalWhere: (items) => {
            return items?.filter(t => t.loai == "2") || []
          }
        }}
        label={"rpt.Ma_plkh2"} defaultlabel="Phân loại kh 2"
        name="ma_plkh2" formik={formik}
        onItemSelected={handleItemSelected}
      />
      <ZenFieldSelectApi
        lookup={{
          ...ZenLookup.Ma_plkh,
          onLocalWhere: (items) => {
            return items?.filter(t => t.loai == "3") || []
          }
        }}
        label={"rpt.Ma_plkh3"} defaultlabel="Phân loại kh 3"
        name="ma_plkh3" formik={formik}
        onItemSelected={handleItemSelected}
      />
      <ZenFieldSelectApi
        lookup={ZenLookup.Ma_nt}
        label={"rpt.ma_nt"}
        defaultlabel="Ngoại tệ"
        name="ma_nt"
        formik={formik}
      />
    </>
  );
};

const TableForm = ({ data = [], filter = {} }) => {
  return (
    <>
      <RptTable maNt={filter.Ma_nt}>
        <Table.Header fullWidth>
          <RptHeader
            maNt={filter.Ma_nt}
            header={[
              { text: ["rpt.ma_kh", "Mã KH"] },
              { text: ["rpt.ten_kh", "Tên KH"] },
              { text: ["rpt.du_no1", "Dư nợ ĐK"] },
              { text: ["rpt.du_co1", "Dư có ĐK"] },
              { text: ["rpt.du_no_ck", "Dư nợ CK"] },
              { text: ["rpt.du_co_ck", "Dư có CK"] },
            ]}
          />
        </Table.Header>
        <Table.Body>
          {data.length > 0
            ? data.map((item, index) => {
              const isNt =
                filter.mM_nt && filter.Ma_nt !== "VND" ? true : false;
              return (
                <RptTableRow
                  key={index}
                  isBold={item.bold}
                  index={index}
                  item={item}
                >
                  <RptTableCell value={item.ma_kh} />
                  <RptTableCell value={item.ten_kh} />
                  <RptTableCell value={item.du_no1} type="number" />
                  <RptTableCell value={item.du_co1} type="number" />
                  <RptTableCell value={item.du_no_ck} type="number" />
                  <RptTableCell value={item.du_co_ck} type="number" />
                </RptTableRow>
              );
            })
            : undefined}
        </Table.Body>
      </RptTable>
    </>
  );
};

export const ARRptBCCN03 = {
  FilterForm: FilterForm,
  TableForm: TableForm,
  permission: "06.50.1",
  visible: true, // hiện/ẩn báo cáo
  route: routes.RptARRptBCCN03,

  period: {
    fromDate: "ngay1",
    toDate: "ngay2",
  },

  linkHeader: {
    id: "ARRptBCCN03",
    defaultMessage: "Tổng hợp số dư công nợ một tài khoản",
    active: true,
  },

  info: {
    code: "08.20.20.1",
  },
  initFilter: {
    ngay1: "",
    ngay2: "",
    tk: "131",
    ma_kh: "",
    ma_nhkh: "",
    ma_plkh1: "",
    ma_plkh2: "",
    ma_plkh3: "",
    ma_nt: "",
  },
  columns: [
    {
      ...IntlFormat.default(Mess_Report.MaKh),
      fieldName: "ma_kh",
      type: "string",
      sorter: true,
    },
    {
      ...IntlFormat.default(Mess_Report.TenKh),
      fieldName: "ten_kh",
      type: "string",
      sorter: true,
    },
    {
      ...IntlFormat.default(Mess_Report.DuNoDauky),
      fieldName: "du_no1",
      type: "number",
      sorter: true,
    },
    {
      ...IntlFormat.default(Mess_Report.DuCoDauKy),
      fieldName: "du_co1",
      type: "number",
      sorter: true,
    },
    {
      ...IntlFormat.default(Mess_Report.DuNoCuoiKy),
      fieldName: "du_no_ck",
      type: "number",
      sorter: true,
    },
    {
      ...IntlFormat.default(Mess_Report.DuCoCuoiKy),
      fieldName: "du_co_ck",
      type: "number",
      sorter: true,
    },
  ],
  formValidation: [
    {
      id: "tk",
      validationType: "string",
      validations: [
        {
          type: "required",
          params: ["Không được bỏ trống trường này"],
        },
      ],
    },
  ],
};
