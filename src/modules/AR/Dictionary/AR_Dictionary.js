import React from "react";
import * as routes from "../../../constants/routes";
import { ApiArDmNhkh, ApiArDmPlkh, ApiArDmKh } from "../Api/index";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { Icon, Table } from "semantic-ui-react";
import { ARDmKhFilter } from "./ARDmKhFilter";
import * as permissions from "../../../constants/permissions";
import { auth, ZenHelper } from "../../../utils";
import { ZenMessageAlert } from "../../../components/Control";
import { GlobalStorage, KeyStorage } from "../../../utils/storage";
import { useIntl } from "react-intl";
import { ApiLookup } from "../../../Api";

function setParamsLookup(lookup) {
   return {
      code_name: lookup.code,
      field_list: lookup.cols,
      where: lookup.where,
      top: lookup.top === null ? 0 : (lookup.top || 20)
   }
}

const sidmloai = GlobalStorage.getByField(KeyStorage.CacheData, 'ma_loai')?.data
let optionMaLoaiKH = []
if (sidmloai) {
   optionMaLoaiKH = ZenHelper.translateListToSelectOptions(sidmloai.filter(x => x.ma_nhom === 'PLKH'), false, 'ma_loai', 'ma', 'ma_loai', [], 'ten')
} else {
   ApiLookup.get(res => {
      if (res.status === 200) {
         optionMaLoaiKH = ZenHelper.translateListToSelectOptions(res.data.data.filter(x => x.ma_nhom === 'PLKH'), false, 'ma_loai', 'ma', 'ma_loai', [], 'ten')
      }
   }, {
      ...setParamsLookup(ZenLookup.SIDmloai)
   })
}


function checkpermislimit(permis) {
   let user = auth.getUserInfo()
   return !permis ? false : user.permisson.includes(permis);
}


const AR_Dictionary = {
   // ArDmNhkh: {
   //    route: routes.ARDmNhkh,
   //    importExcel: true,
   //    action: {
   //       view: { visible: true, permission: "06.24.1" },
   //       add: { visible: true, permission: "06.24.2" },
   //       edit: { visible: true, permission: "06.24.3" },
   //       del: { visible: true, permission: "06.24.4" },
   //    },

   //    linkHeader: {
   //       id: "ardmnhkh",
   //       defaultMessage: "Danh mục nhóm khách hàng",
   //       active: true,
   //       isSetting: false,
   //    },

   //    tableList: {
   //       keyForm: "ArDmNhkh",
   //       formModal: ZenLookup.Ma_nhkh,
   //       fieldCode: "ma_nhkh",
   //       unPagination: false,
   //       duplicate: true,

   //       api: {
   //          url: ApiArDmNhkh,
   //          type: "sql",
   //       },
   //       columns: [
   //          { id: "ardmnhkh.ma_plkh", defaultMessage: "Mã nhóm khách hàng", fieldName: "ma_nhkh", filter: "string", sorter: true, editForm: true },
   //          { id: "ardmnhkh.ten_plkh", defaultMessage: "Tên nhóm khách hàng", fieldName: "ten_nhkh", filter: "string", sorter: true, },
   //          { id: "ksd", defaultMessage: "Không sử dụng", fieldName: "ksd", type: "bool", filter: "", sorter: true, collapsing: true },
   //       ],
   //    },
   // },
   ArDmPlkh: {
      route: routes.ARDmPlkh,
      importExcel: true,
      action: {
         view: { visible: true, permission: permissions.ARDmPlkhXem },
         add: { visible: true, permission: permissions.ARDmPlkhThem },
         edit: { visible: true, permission: permissions.ARDmKhSua },
         del: { visible: true, permission: permissions.ARDmKhXoa },
         dup: { visible: true, permission: "" }
      },

      linkHeader: {
         id: "ardmplkh",
         defaultMessage: "Danh mục phân loại khách hàng",
         active: true,
         isSetting: true,
      },

      tableList: {
         keyForm: "ARDmPlkh",
         showFilterLabel: true,
         formModal: ZenLookup.Ma_plkh,
         fieldCode: "ma_plkh",
         unPagination: false,
         duplicate: true,

         api: {
            url: ApiArDmPlkh,
            type: "sql",
         },
         columns: [
            { id: "ardmplkh.ma_plkh", defaultMessage: "Mã phân loại khách hàng", fieldName: "ma_plkh", filter: "string", sorter: true, editForm: true },
            { id: "ardmplkh.ten_plkh", defaultMessage: "Tên phân loại khách hàng", fieldName: "ten_plkh", filter: "string", sorter: true, },
            { id: "ardmplkh.loai", defaultMessage: "Tên loại", fieldName: "loai", filter: "list", sorter: true, custom: true, listFilter: optionMaLoaiKH },
            { id: "PMDmGiaiDoanDa.ordinal", defaultMessage: "Thứ tự hiển thị", fieldName: "ordinal", filter: "number", sorter: true, },
            { id: "ksd", defaultMessage: "Không sử dụng", fieldName: "ksd", type: "bool", filter: "", sorter: true, collapsing: true },
         ],
         customCols: (data, item, fieldName, { onEdit, ...restCol }) => {
            let customCell = undefined
            if (fieldName === 'loai') {
               const loai = optionMaLoaiKH.find(t => t.value == item["loai"]) || {}
               customCell = <Table.Cell key={fieldName}>
                  {loai.text || item["ten_loai"]}
               </Table.Cell>
            }
            return customCell
         }
      },
   },
   ArDmKh: {
      route: routes.ARDmkh,
      importExcel: true,
      action: {
         view: { visible: true, permission: permissions.ARDmKhXem },
         add: { visible: true, permission: permissions.ARDmKhThem },
         edit: { visible: true, permission: permissions.ARDmKhSua },
         del: { visible: true, permission: permissions.ARDmKhXoa },
      },

      linkHeader: {
         id: "ArDmKh",
         defaultMessage: "Danh mục khách hàng",
         active: true,
         isSetting: false,
      },

      tableList: {
         ContainerTop: ARDmKhFilter,
         showFilterLabel: true,
         propsContainerTop: {
            isOutside: true
         },
         onFilterList: (data, newItem) => {
            return [newItem].concat(data)
         },

         onBeforeAction: (item, mode) => {
            let user = auth.getUserInfo()
            checkpermislimit(permissions.ARDmKhSuaPhuTrach, 'aaaa')
            if (mode === 2) {
               if (auth.checkPermission(permissions.ARDmKhSua) && checkpermislimit(permissions.ARDmKhSuaPhuTrach)) {
                  if (item.ma_nvkd === user.hrid) {
                     return { open: true }
                  } else {
                     ZenMessageAlert.warning("Bạn không phải là người phụ trách khách hàng này")
                     return { open: false }
                  }
               }
               if (auth.checkPermission(permissions.ARDmKhSua) && !checkpermislimit(permissions.ARDmKhSuaPhuTrach)) {
                  return { open: true }
               } else {
                  ZenMessageAlert.warning("Vui lòng liên hệ admin nếu muốn sử dụng tính năng này")
                  return { open: false }
               }
            }
            if (mode === 3) {
               if (auth.checkPermission(permissions.ARDmKhXoa) && checkpermislimit(permissions.ARDmKhXoaPhuTrach)) {
                  if (item.ma_nvkd === user.hrid) {
                     return { open: true }
                  } else {
                     ZenMessageAlert.warning("Bạn không phải là người phụ trách khách hàng này")
                     return { open: false }
                  }
               }
               if (auth.checkPermission(permissions.ARDmKhSua) && !checkpermislimit(permissions.ARDmKhSuaPhuTrach)) {
                  return { open: true }
               } else {
                  ZenMessageAlert.warning("Vui lòng liên hệ admin nếu muốn sử dụng tính năng này")
                  return { open: false }
               }
            }
            if (mode === 7) {
               return { open: true }
            }
         },

         keyForm: "ARDmkh",
         formModal: ZenLookup.Ma_kh,
         fieldCode: "ma_kh",

         api: {
            url: ApiArDmKh,
            type: "sql",
            qf: `iskh = 1`,
         },

         noMoveCols: ["user_account"],
         noResizeCols: ["user_account"],
         columns: [
            { text: "Account", fieldName: "user_account", filter: "", sorter: false, visibleColMove: false, custom: true },
            { id: "ArDmKh.cdate", defaultMessage: "Ngày nhập", fieldName: "cdate", filter: "date", sorter: true },
            {
               id: "ArDmKh.ma_kh", defaultMessage: "Mã khách hàng", fieldName: "ma_kh", filter: "string", sorter: true,
               link: { route: routes.ARDmKhDetail(), params: "ma_kh" }
            },
            { id: "ArDmKh.ten_kh", defaultMessage: "Tên khách hàng", fieldName: "ten_kh", filter: "string", sorter: true },
            { id: "ArDmKh.linh_vuc", defaultMessage: "Lĩnh vực", fieldName: "ten_linh_vuc", filter: "string", sorter: true, },
            { id: "ArDmKh.plkh2", defaultMessage: "Cấp học", fieldName: "ten_plkh2", filter: "string", sorter: true, },
            { id: "ArDmKh.quan", defaultMessage: "Quận/huyện", fieldName: "ten_huyen", filter: "string", sorter: true, },
            { id: "ArDmKh.ma_nhkh", defaultMessage: "Nhóm khách hàng", fieldName: "ten_nhkh", filter: "string", sorter: true, },
            { id: "ArDmKh.ma_nvkd", defaultMessage: "Người phụ trách", fieldName: "ten_nvkd", filter: "string", sorter: true, },
         ],

         customCols: (data, item, fieldName, colInfo) => {
            let customCell = undefined
            if (fieldName === 'user_account') {
               customCell = <Table.Cell key={fieldName} collapsing textAlign="center">
                  {item.user_account && <Icon name="user circle" />}
               </Table.Cell>
            }
            return customCell
         }
      },
   },
   // ArDmNcc: {
   //    route: routes.ARDmNcc,
   //    importExcel: true,
   //    action: {
   //       view: { visible: true, permission: "08.21.1" },
   //       add: { visible: true, permission: "08.21.2" },
   //       edit: { visible: true, permission: "08.21.3" },
   //       del: { visible: true, permission: "08.21.4" },
   //    },

   //    linkHeader: {
   //       id: "ArDmNcc",
   //       defaultMessage: "Danh mục nhà cung cấp",
   //       active: true,
   //       isSetting: false,
   //    },

   //    tableList: {
   //       keyForm: "ARDmNcc",
   //       formModal: ZenLookup.Ma_ncc,
   //       fieldCode: "ma_kh",
   //       unPagination: false,
   //       duplicate: true,

   //       api: {
   //          url: ApiArDmKh,
   //          type: "sql",
   //          qf: `isncc = 1`
   //       },
   //       columns: [
   //          { id: "ArDmKh.ma_ncc", defaultMessage: "Mã NCC", fieldName: "ma_kh", filter: "string", sorter: true, editForm: true },
   //          { id: "ArDmKh.ten_ncc", defaultMessage: "Tên nhà cung cấp", fieldName: "ten_kh", filter: "string", sorter: true, editForm: true },
   //          { id: "ArDmKh.ma_so_thue", defaultMessage: "Mã số thuế", fieldName: "ma_so_thue", filter: "string", sorter: true, },
   //          { id: "ArDmKh.dia_chi", defaultMessage: "Địa chỉ", fieldName: "dia_chi", filter: "string", sorter: true, },
   //          { id: "ArDmKh.tel", defaultMessage: "ĐT", fieldName: "tel", filter: "string", sorter: true, },
   //          { id: "ArDmKh.fax", defaultMessage: "Fax", fieldName: "fax", filter: "string", sorter: true, },
   //          { id: "ArDmKh.email", defaultMessage: "Email", fieldName: "email", filter: "string", sorter: true, },
   //          { id: "ArDmKh.home_page", defaultMessage: "Websites", fieldName: "home_page", filter: "string", sorter: true, },
   //          { id: "ArDmKh.nguoi_gd", defaultMessage: "Người giao dịch", fieldName: "nguoi_gd", filter: "string", sorter: true, },
   //          { id: "ArDmKh.ma_plkh1", defaultMessage: "Mã phân loại KH 1", fieldName: "ma_plkh1", filter: "string", sorter: true, },
   //          { id: "ArDmKh.ma_plkh2", defaultMessage: "Mã phân loại KH 2", fieldName: "ma_plkh2", filter: "string", sorter: true, },
   //          { id: "ArDmKh.ma_plkh3", defaultMessage: "Mã phân loại KH 3", fieldName: "ma_plkh3", filter: "string", sorter: true, },
   //          { id: "ArDmKh.ma_nhkh", defaultMessage: "Mã nhóm KH", fieldName: "ma_nhkh", filter: "string", sorter: true, },
   //          { id: "ksd", defaultMessage: "Không sử dụng", fieldName: "ksd", type: "bool", filter: "", sorter: true, collapsing: true },
   //       ],
   //    },
   // },
}

export default AR_Dictionary