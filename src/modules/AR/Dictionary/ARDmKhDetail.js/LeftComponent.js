import React, { useContext, useEffect, useLayoutEffect, useMemo, useRef, useState } from "react";
import { useIntl } from "react-intl";
import {
    Button, Form, Icon,
    Message, Modal, Segment, Table
} from "semantic-ui-react";
import {
    ZenButton,
    ZenFieldSelectApi, ZenFieldSelectApiMulti,
    ZenLink,
    ZenLoading, ZenMessageAlert, ZenMessageToast, ZenModal, ButtonEdit, ButtonDelete
} from "../../../../components/Control";
import { FormMode, zzControlHelper } from "../../../../components/Control/zzControlHelper";
import { ApiArDmKh } from "../../Api";
import { ApiHrHsNs } from "../../../HrDmOther/Api/ApiHrHsNs";
import { ZenLookup } from "../../../ComponentInfo/Dictionary/ZenLookup";
import UserFormAdd from "../../../User/UserFormAdd"
import { ContextARDmKhDetail } from "./ARDmKhDetail";
import { getModal } from "../../../ComponentInfo/Dictionary/ZenGetModal";
import { ApiCRMContact } from "../../../CRM/Api";
import * as permissions from '../../../../constants/permissions'
import { auth } from "../../../../utils";

const extractStr = zzControlHelper.extractString(["{", "}"]);

function checkpermislimit(permis) {
    let user = auth.getUserInfo()
    return !permis ? false : user.permisson.includes(permis);
}

export const CKhachHangInfo = ({ title, loading, item, viewObject, onSetItem }) => {
    // ************ STATE
    const intl = useIntl()
    const [collapse, setCollapse] = useState(false)
    const [loadingSave, setLoadingSave] = useState("")

    // ************ MEMO
    const memoizedView = useMemo(() => getBasicInfo(), [item]);

    // ************ HANDLE
    const onItemLookup = (option, { api, fieldUpdate, lookup, renderView, arrName }) => {
        if (option[lookup.value] == item[fieldUpdate]) {
            return
        }

        setLoadingSave(fieldUpdate)
        // PATCH
        const patchData = [
            {
                value: option[lookup.value],
                path: `/${fieldUpdate}`,
                op: "replace",
                operationType: 0,
            }
        ]

        api["updatePatch"](item.id, patchData, res => {
            if (res.status >= 200 && res.status <= 204) {
                const newItem = {}, valueRender = renderView.split(",")

                arrName.forEach((itemField, index) => {
                    newItem[itemField] = option[valueRender[index]]
                });
                onSetItem(newItem)
                setLoadingSave()
                ZenMessageToast.success();
            } else {
                setLoadingSave()
                ZenMessageAlert.error(ZenHelper.getResponseError(res))
            }
        })
    };

    // ************ FUNCTION
    function getBasicInfo() {
        if (!item) return []

        return viewObject.map(({ name, text, ...rest }) => {
            const arrName = extractStr(name);
            let value = name;

            for (let index = 0; index < arrName.length; index++) {
                const field = arrName[index];
                let valueFormat = item[field]

                if (rest.type === "date") {
                    valueFormat = item[field] ? intl.formatDate(item[field], {
                        year: 'numeric',
                        month: 'numeric',
                        day: 'numeric',
                    }) : ""
                } else if (rest.type === "number") {
                    valueFormat = item[field] ? intl.formatNumber(item[field], {}) : ""
                }

                value = value.replace(`{${field}}`, valueFormat || "")
            }

            return {
                text: text,
                value: value,
                arrName: arrName,
                ...rest
            }
        })
    }

    function tableUI() {
        return <Table basic='very'>
            <Table.Body>
                {
                    memoizedView.map((t, index) => {
                        const isLoadingBtn = t.edit ? loadingSave === t.edit.fieldUpdate : false
                        let viewValue = t.value
                        // route
                        if (t.route) {
                            viewValue = <ZenLink to={t.route(zzControlHelper.btoaUTF8(item.ma_kh))}>
                                {t.value}
                            </ZenLink>
                        }
                        return <Table.Row key={index}>
                            <Table.Cell textAlign="right" collapsing>{t.text}</Table.Cell>
                            <Table.Cell style={{ fontWeight: "bold", whiteSpace: t.type == "textarea" ? "pre-wrap" : "" }}>
                                {viewValue}
                            </Table.Cell>
                            <Table.Cell textAlign="right" collapsing>
                                {t.edit && t.edit.lookup && <>
                                    <ZenModalLookup trigger={<Icon link
                                        name={isLoadingBtn ? "spinner" : "pencil"}
                                        loading={isLoadingBtn ? true : false} />}

                                        lookup={t.edit.lookup}
                                        value={item[t.edit.fieldUpdate]}
                                        onItemLookup={(option) => onItemLookup(option, { ...t.edit, arrName: t.arrName })}
                                    />
                                </>}
                            </Table.Cell>
                        </Table.Row>
                    })
                }
            </Table.Body>
        </Table>
    }

    return <Segment loading={loading}>
        {title && <div className={"ar-lf-header"}>
            {title}
            <Icon name={collapse ? "plus" : "minus"}
                style={{ float: "right" }} link
                onClick={() => setCollapse(!collapse)}
            />
        </div>}
        {!collapse && tableUI()}
    </Segment>
}

export const CPhuTrachTheoDoi = (props) => {
    const { title, itemKH } = props
    const { setItemKH } = useContext(ContextARDmKhDetail)

    // *************** STATE
    const _isMounted = useRef(true)
    const _Contructor = useRef(true)
    const [followList, setFollowList] = useState();
    const [error, setError] = useState();
    const [loading, setLoading] = useState(false);
    const [stateModal, setStateModal] = useState({ open: false });
    const [collapse, setCollapse] = useState(false);
    const intl = useIntl()

    useEffect(() => {
        if (_Contructor.current === true && itemKH) {
            setLoading(true)
            let strSql = zzControlHelper.replaceAll(itemKH.id_nv_follow, ",", `','`);
            ApiHrHsNs.get(res => {
                if (res.status === 200) {
                    setFollowList(res.data.data)
                } else {
                    // setError(zzControlHelper.getResponseError(res))}
                }
                setLoading(false)
            }, {
                qf: `id_nv IN ('${strSql}')`
            })
            _Contructor.current = false;
        }

        return () => {
            _isMounted.current = false
        }
    }, [itemKH])

    const handleOpenModal = (e) => {
        e.preventDefault();
        let user = auth.getUserInfo()
        if (permissions.ARDmKhSua) {
            if (auth.checkPermission(permissions.ARDmKhSua) && checkpermislimit(permissions.ARDmKhSuaPhuTrach)) {
                if (itemKH.ma_nvkd === user.hrid) {
                    setStateModal({ ...stateModal, open: !stateModal.open })
                } else {
                    ZenMessageAlert.warning("Bạn không phải là người phụ trách khách hàng này")
                }
            }
            if (auth.checkPermission(permissions.ARDmKhSua) && !checkpermislimit(permissions.ARDmKhSuaPhuTrach)) {
                setStateModal({ ...stateModal, open: !stateModal.open })
            }
        } else {
            ZenMessageAlert.warning(
                intl.formatMessage({ id: "component.permission", defaultMessage: "Không có quyền truy cập" })
            )
        }
    }

    const handleSaveModal = (newItemKH) => {
        const id_nv_followStr = newItemKH.arrIdNvFollow?.toString() || ""
        const patchData = [
            {
                value: newItemKH.ma_nvkd,
                path: `/ma_nvkd`,
                op: "replace",
                operationType: 0,
            },
            {
                value: id_nv_followStr,
                path: `/id_nv_follow`,
                op: "replace",
                operationType: 0,
            }
        ]

        ApiArDmKh.updatePatch(newItemKH.ma_kh, patchData, res => {
            if (res.status >= 200 && res.status <= 204) {
                setItemKH({ ...newItemKH, id_nv_follow: id_nv_followStr })
                if (newItemKH.fllowList) { setFollowList(newItemKH.fllowList) }
                setStateModal({ open: false })
                ZenMessageToast.success();
            } else {
                ZenMessageAlert.error(zzControlHelper.getResponseError(res))
            }
        })
    }

    return <>
        <Segment>
            <TitleHeader title={"Người phụ trách/theo dõi"}
                totalRows={followList?.length}
                collapse={collapse}
                onCollapse={() => setCollapse(!collapse)}
            />
            {
                !collapse && <>
                    <ZenLoading loading={loading} inline="centered" />
                    {error && <Message negative list={error} />}

                    {itemKH && !loading && <div style={{ borderBottom: "solid 1px rgba(0, 0, 0, 0.1)", marginBottom: "14px" }}>
                        <p>Phụ trách chính: <span style={{ fontWeight: "bold" }}>{itemKH.ten_nvkd}</span></p>
                        <p>Theo dõi:</p>
                        <ul>
                            {
                                followList && followList.length > 0 ?
                                    followList.map(item => {
                                        return <li key={item.id_nv}>{item.ho_ten}</li>
                                    })
                                    :
                                    itemKH?.nv_follow && itemKH?.nv_follow.length > 0 &&
                                    itemKH?.nv_follow.map(item => {
                                        return <li key={item.id_nv}>{item.ho_ten}</li>
                                    })
                            }
                        </ul>
                    </div>}

                    <BottomContent>
                        <a href="#" onClick={handleOpenModal}>
                            <Icon name="linkify" />
                            Sửa thông tin
                        </a>
                    </BottomContent>
                </>
            }

            {stateModal.open && <ModalPhuTrachTheoDoi
                itemKH={itemKH}
                stateModal={stateModal}
                onOpenModal={handleOpenModal}
                onSaveModal={handleSaveModal}
            />}
        </Segment>
    </>
}

export const CLienHe = (props) => {
    const { itemKH, valueKey } = props
    const [contacts, setContacts] = useState();
    const [error, setError] = useState();
    const [loading, setLoading] = useState(true);
    const [stateModal, setStateModal] = useState({ open: false });
    const [collapse, setCollapse] = useState(false);
    const intl = useIntl()

    useEffect(() => {
        ApiArDmKh.getContactByCode(valueKey, res => {
            if (res.status === 200) {
                setContacts(res.data.data)
            } else {
                setError(zzControlHelper.getResponseError(res))
            }
            loading && setLoading(false)
        })
    }, [])

    const modalContact = useMemo(() => {
        return getModal(ZenLookup.CrmContact)
    }, [])

    const handleOpenModal = (mode, item) => {
        let user = auth.getUserInfo()
        if (mode === FormMode.DEL) {
            if (permissions.ARDmKhSua) {
                if (auth.checkPermission(permissions.ARDmKhSua) && checkpermislimit(permissions.ARDmKhSuaPhuTrach)) {
                    if (itemKH.ma_nvkd === user.hrid) {
                        ZenMessageAlert.question("Bạn có chắc muốn xóa không?")
                            .then(success => {
                                if (success == 1) {
                                    removeContact(item)
                                }
                            })
                    } else {
                        ZenMessageAlert.warning("Bạn không phải là người phụ trách khách hàng này")
                        return
                    }
                }
                if (auth.checkPermission(permissions.ARDmKhSua) && !checkpermislimit(permissions.ARDmKhSuaPhuTrach)) {
                    ZenMessageAlert.question("Bạn có chắc muốn xóa không?")
                        .then(success => {
                            if (success == 1) {
                                removeContact(item)
                            }
                        })
                } else {
                    ZenMessageAlert.warning(
                        intl.formatMessage({ id: "component.permission", defaultMessage: "Không có quyền truy cập" })
                    )
                }
            }
        } else {

            if (mode === FormMode.EDIT) {
                if (permissions.ARDmKhSua) {
                    if (auth.checkPermission(permissions.ARDmKhSua) && checkpermislimit(permissions.ARDmKhSuaPhuTrach)) {
                        if (itemKH.ma_nvkd === user.hrid) {
                            setStateModal({
                                id: item?.id,
                                formMode: mode,
                                open: !stateModal.open,
                            })
                        } else {
                            ZenMessageAlert.warning("Bạn không phải là người phụ trách khách hàng này")
                            return
                        }
                    }
                    if (auth.checkPermission(permissions.ARDmKhSua) && !checkpermislimit(permissions.ARDmKhSuaPhuTrach)) {
                        setStateModal({
                            id: item?.id,
                            formMode: mode,
                            open: !stateModal.open,
                        })
                    } else {
                        ZenMessageAlert.warning(
                            intl.formatMessage({ id: "component.permission", defaultMessage: "Không có quyền truy cập" })
                        )
                    }
                }
            }
            if (mode === FormMode.ADD) {
                if (permissions.ARDmKhSua) {
                    console.log(checkpermislimit(permissions.ARDmKhSuaPhuTrach))
                    if (auth.checkPermission(permissions.ARDmKhSua) && checkpermislimit(permissions.ARDmKhSuaPhuTrach)) {
                        if (itemKH.ma_nvkd === user.hrid) {
                            setStateModal({
                                id: item?.id,
                                formMode: mode,
                                open: !stateModal.open,
                            })
                        } else {
                            ZenMessageAlert.warning("Bạn không phải là người phụ trách khách hàng này")
                            return
                        }
                    }
                    if (auth.checkPermission(permissions.ARDmKhSua) && !checkpermislimit(permissions.ARDmKhSuaPhuTrach)) {
                        setStateModal({
                            id: item?.id,
                            formMode: mode,
                            open: !stateModal.open,
                        })
                    } else {
                        ZenMessageAlert.warning(
                            intl.formatMessage({ id: "component.permission", defaultMessage: "Không có quyền truy cập" })
                        )
                    }
                }
            }
            if (mode == FormMode.NON) {
                setStateModal({
                    id: item?.id,
                    formMode: mode,
                    open: !stateModal.open,
                })
            }
        }
    }

    const handleAfterSaveModal = (newItem, { mode }) => {
        if (mode === FormMode.ADD) {
            setContacts(contacts?.concat(newItem))
        } else if (mode === FormMode.EDIT) {
            setContacts(contacts?.map(item => item.id == newItem.id ? newItem : item))
        }
        setStateModal({ open: !stateModal.open, formMode: FormMode.NON })
    }

    function removeContact(itemContact) {
        ApiCRMContact.delete(itemContact.id, res => {
            if (res.status === 204) {
                setContacts(contacts?.filter(item => item.id != itemContact.id))
                ZenMessageToast.success();
            } else {
                ZenMessageAlert.error(zzControlHelper.getResponseError(res))
            }
        })
    }

    function listDataUI() {
        return contacts?.map((obj, index) => {
            return <div key={obj.id || `c${index}`} style={{ marginBottom: "14px" }}>
                <div className="ar-lf-row ar-select-hover">
                    <div style={{ width: "100%" }}>
                        <div style={{ marginBottom: "7px" }}>
                            <Icon name="user" />
                            <strong>{`${obj.ten_lien_he} ${obj.chuc_danh ? (" - " + obj.chuc_danh) : ""}`}</strong>
                        </div>
                        <div style={{ display: "flex", marginBottom: "7px" }}>
                            <div>
                                <Icon name="mail outline" />
                                {obj.email || "..."}
                            </div>
                            <div style={{ marginLeft: "28px" }}>
                                <Icon name="mobile alternate" />
                                {obj.dien_thoai || "..."}
                            </div>
                        </div>
                        <div style={{ display: "flex" }}>
                            <div>
                                <Icon name="address card outline" />
                                {obj.dia_chi || "..."}
                            </div>
                        </div>
                    </div>
                    <div className="sub-hover" style={{paddingRight: "5px"}}>
                        <Button.Group size="tiny" style={{ float: "right" }}>
                            <ButtonEdit
                                onClick={() => handleOpenModal(FormMode.EDIT, obj)}
                            />
                            <ButtonDelete
                                onClick={() => handleOpenModal(FormMode.DEL, obj)}
                            />
                        </Button.Group>
                        {/* <Icon name="edit" link onClick={() => handleOpenModal(FormMode.EDIT, obj)} style={{ marginBottom: "7px" }} />
                        <Icon name="trash" link onClick={() => handleOpenModal(FormMode.DEL, obj)} /> */}
                    </div>
                </div>
                <div className="ar-hr" />
            </div>
        })
    }

    return <>
        <Segment>
            <TitleHeader title={"Người liên hệ"}
                totalRows={contacts?.length}
                collapse={collapse}
                onCollapse={() => setCollapse(!collapse)}
            />

            {
                !collapse && <>
                    <ZenLoading loading={loading} inline="centered" />
                    {error && <Message negative list={error} />}

                    {listDataUI()}

                    <BottomContent>
                        <a href="#" onClick={(e) => {
                            e.preventDefault();
                            handleOpenModal(FormMode.ADD)
                        }}>
                            <Icon name="linkify" />
                            Tạo liên hệ
                        </a>
                    </BottomContent>

                </>
            }

            {zzControlHelper.openModalComponent({
                ...modalContact,
                ...stateModal,
                other: {
                    ma_kh: valueKey
                }
            },
                {
                    onClose: () => handleOpenModal(FormMode.NON),
                    onAfterSave: handleAfterSaveModal,
                }
            )}
        </Segment>
    </>
}

export const CUserAccount = (props) => {
    const { itemKH } = props
    const { setItemKH } = useContext(ContextARDmKhDetail)
    const [stateModal, setStateModal] = useState({ open: false });
    const [collapse, setCollapse] = useState(false);
    const [openModalAccount, setOpenModalAccount] = useState(false);

    const handleOpenModal = () => {
        setStateModal({ ...stateModal, open: !stateModal.open })
    }

    const handleAfterSaveModal = (newUser) => {
        const patchData = [
            {
                value: newUser.email,
                path: `/user_account`,
                op: "replace",
                operationType: 0,
            },
        ]

        ApiArDmKh.updatePatch(itemKH.ma_kh, patchData, res => {
            if (res.status >= 200 && res.status <= 204) {
                setItemKH({ user_account: newUser.email })
                setStateModal({ open: false })
            } else {
                ZenMessageAlert.error(zzControlHelper.getResponseError(res))
            }
        })
    }

    const handleSetAccount = (data) => {
        ApiArDmKh.setAccount(data, res => {
            if (res.status >= 200 && res.status <= 204) {
                setItemKH({ user_account: data.email })
                setOpenModalAccount(false)
            } else {
                ZenMessageAlert.error(zzControlHelper.getResponseError(res))
            }
        })
    }

    function noAccountUI() {
        return <>
            <p>
                Khách hàng này chưa được cấp tài khoản đăng nhập
            </p>
            <Button
                primary fluid
                size="small"
                content="Tạo tài khoản cho khách hàng"
                icon="user plus"
                onClick={handleOpenModal}
            />
            <Button style={{ marginTop: "5px" }}
                primary fluid
                size="small"
                content="Gán tài khoản đã tồn tại"
                icon="user plus"
                onClick={(e) => setOpenModalAccount(true)}
            />
        </>
    }

    function hasAccountUI() {
        return <>
            <p>
                Tài khoản đăng nhập của khách hàng là:
            </p>
            <p style={{ fontWeight: "bold" }}>
                {itemKH.user_account}
            </p>
        </>
    }

    function renderUI() {
        return <>
            {itemKH.user_account ? hasAccountUI() : noAccountUI()}

            {stateModal.open && <UserFormAdd
                id={itemKH.id}
                initItem={{
                    email: itemKH.email,
                    full_name: itemKH.ten_kh
                }}
                header={"Tạo tài khoản truy cập"}
                open={stateModal.open}
                onAfterSave={handleAfterSaveModal}
                onClose={handleOpenModal}

                createFromDetail={true}
            />}

            {
                openModalAccount && <ModalSetAccount
                    itemKH={itemKH}
                    open={openModalAccount}
                    onClose={() => setOpenModalAccount(false)}
                    onSaveModal={(data) => handleSetAccount(data)}
                />
            }
        </>
    }

    return <Segment>
        <TitleHeader title={"Tài khoản đăng nhập"}
            collapse={collapse}
            onCollapse={() => setCollapse(!collapse)}
        />
        {!collapse && itemKH && renderUI()}
    </Segment>
}

const ModalSetAccount = (props) => {
    const { open, onClose, itemKH, onSaveModal } = props
    const [account, setAccount] = useState({ id: '', email: '' })
    const [loading, setLoading] = useState(false)


    return <>

        <ZenModal
            open={open}
            onClose={onClose}
            size="tiny"
            header="Gán tài khoản đã tồn tại"

            actions={
                <ZenButton btnType="save"
                    onClick={() => onSaveModal({ ma_kh: itemKH.ma_kh, user: account.id, email: account.email })}
                    size="small"
                />
            }
            scrolling={false}
        >
            <Modal.Content>
                <Form>
                    <ZenFieldSelectApi
                        //loadApi
                        lookup={ZenLookup.User}
                        name="user" value={account.id}
                        label={"ardmkh.user"} defaultlabel="User"
                        onChange={(e, i) => {
                            setAccount({ id: i.value, email: i.options.filter(t => t.value === i.value)[0].email })
                        }}
                    />
                </Form>

                <ZenLoading loading={loading} />

            </Modal.Content>
        </ZenModal>
    </>
}

const ModalPhuTrachTheoDoi = (props) => {
    const { itemKH, stateModal, onOpenModal, onSaveModal } = props;
    const [data, setData] = useState({ ma_nvkd: "", arrIdNvFollow: [] })
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        setTimeout(() => {
            setLoading(false)
        }, 300);
        setData({ ...itemKH, arrIdNvFollow: itemKH?.id_nv_follow ? itemKH?.id_nv_follow.split(",") : [] })
    }, [])

    const handleChange = (e, { name, value, options }) => {
        if (name === "ma_nvkd") {
            const option = options.find(t => t.value === value)
            setData({ ...data, [name]: value, ten_nvkd: option?.ho_ten || "" })
        } else if (name === "arrIdNvFollow") {
            const arraySelected = value.map(id_nv => {
                return options.find(t => t.id_nv == id_nv)
            })
            setData({ ...data, [name]: value, fllowList: arraySelected })
        }
    }

    return <>
        <ZenModal
            open={stateModal.open}
            onClose={onOpenModal}
            size="tiny"
            header="Sửa người phụ trách"

            actions={
                <ZenButton btnType="save"
                    onClick={() => onSaveModal(data)}
                    size="small"
                />
            }
            scrolling={false}
        >
            <Modal.Content>
                <Form>
                    <ZenFieldSelectApi
                        //loadApi
                        lookup={ZenLookup.ID_NV}
                        name="ma_nvkd" value={data.ma_nvkd}
                        label={"ardmkh.nguoi_phu_trach"} defaultlabel="Người phụ trách chính"
                        onChange={handleChange}
                    />

                    <ZenFieldSelectApiMulti
                        search={true}
                        lookup={ZenLookup.ID_NV}
                        name="arrIdNvFollow" value={data.arrIdNvFollow || []}
                        label={"ardmkh.nv_follow"} defaultlabel="Người theo dõi"
                        onChange={handleChange}
                    />
                </Form>

                <ZenLoading loading={loading} />

            </Modal.Content>
        </ZenModal>
    </>
}

const TitleHeader = ({ title, totalRows, collapse, onCollapse }) => {
    return <div className={"ar-lf-header"}>
        {title}
        {totalRows > 0 && ` (${totalRows})`}
        <Icon name={collapse ? "plus" : "minus"}
            style={{ float: "right" }} link
            onClick={onCollapse}
        />
    </div>
}

const BottomContent = ({ children }) => {
    return <div style={{ textAlign: "center", fontWeight: "bold" }}>
        {children}
    </div>
}