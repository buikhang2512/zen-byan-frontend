import React from "react";
import { Breadcrumb, Grid } from "semantic-ui-react";
import { ContainerScroll, HeaderLink, Helmet, SegmentHeader, ZenLoading } from "../../../../components/Control";
import { zzControlHelper } from "../../../../components/Control/zzControlHelper";
import * as routes from "../../../../constants/routes";
import { CKhachHangInfo, CLienHe, CPhuTrachTheoDoi, CUserAccount } from "./LeftComponent";
import { ApiArDmKh } from "../../Api/index";
import { ZenHelper } from "../../../../utils/global";
import { RightComponent } from "./RightComponent";
import "./ARDmKhDetail.css"
import { menuBar } from "./zConstVariable";
import * as permissions from './../../../../constants/permissions'
import { auth } from '../../../../utils/index'

const ContextARDmKhDetail = React.createContext();

class ARDmKhDetailComponent extends React.Component {
    constructor(props) {
        super()
        this._isMounted = true
        this.idElementContainer = "ardmkh-container"
        this.maKH = zzControlHelper.atobUTF8(props.match.params.id)

        this.state = {
            error: null,
            loading: true,
            itemKH: null,
        }
    }

    componentDidMount() {
        ApiArDmKh.getByCode(this.maKH, res => {
            if (this._isMounted) {
                if (res.status === 200)
                    var temp = { itemKH: res.data.data }
                else
                    temp = { error: ZenHelper.getResponseError(res) }

                this.setState({
                    loading: false,
                    ...temp
                })
            }
        })
    }

    componentWillUnmount() {
        this._isMounted = false
    }

    render() {
        const { error, itemKH, loading } = this.state

        return <ContextARDmKhDetail.Provider value={{
            setItemKH: (newItem) => this.setState({ itemKH: newItem })
        }}>
            <PageHeader item={itemKH} />
            <ZenLoading loading={loading} dimmerPage />

            <Grid columns={2} id={this.idElementContainer}>
                <Grid.Column width="6">
                    <ContainerScroll isSegment={false}
                        idElementContainer={this.idElementContainer}
                        id={"left-content"}
                    >
                        <CKhachHangInfo item={itemKH} loading={loading}
                            valueKey={this.maKH}
                            title="Thông tin khách hàng"
                            viewObject={[
                                { name: "{ten_kh}", text: "Khách hàng" },
                                { name: "{ma_so_thue}", text: "Mã số thuế" },
                                { name: "{dia_chi}", text: "Địa chỉ" },
                                { name: "{tel}", text: "Điện thoại" },
                                { name: "{email}", text: "Email" },
                                { name: "{home_page}", text: "Website" },
                            ]}
                        />

                        <CLienHe
                            itemKH={itemKH}
                            valueKey={this.maKH}
                        />

                        <CPhuTrachTheoDoi
                            title={"Phụ trách/theo dõi"}
                            itemKH={itemKH}
                            valueKey={this.maKH}
                        />

                        <CUserAccount
                            itemKH={itemKH}
                            valueKey={this.maKH}
                        />
                    </ContainerScroll>
                </Grid.Column>

                <Grid.Column width="10">
                    <RightComponent
                        idElementContainer={this.idElementContainer}
                        itemKH={itemKH}
                        valueKey={this.maKH}
                        type={"MA_KH"}
                        keyData="ma_kh"
                        listHeader={listHeader}
                        menuBar={{
                            ...menuBar,
                            GhiChu: auth.checkPermission(permissions.ARCRMTabGhiChuXem) ? { name: "ghichu", content: "Ghi chú", visible: true } : { visible: false },
                            HoatDong: auth.checkPermission(permissions.ARCRMTabHoatDongXem) ? { name: "hoatdong", content: "Hoạt động", visible: true, } : { visible: false },
                            Deal: auth.checkPermission(permissions.ARCRMTabCoHoiXem) ? { name: "deal", content: "Cơ hội", visible: true, } : { visible: false },
                            BaoGia: auth.checkPermission(permissions.ARCRMTabBaoGiaXem) ? { name: "baogia", content: "Báo Giá", visible: true, } : { visible: false },
                            HopDong: auth.checkPermission(permissions.ARCRMTabHopDongXem) ? { name: "sidmhd", content: "Hợp đồng", visible: true, } : { visible: false },
                            TraoDoi: auth.checkPermission(permissions.ARCRMTabTraoDoiXem) ? { name: "traodoi", content: "Trao Đổi", visible: true, } : { visible: false },
                            CongViec: auth.checkPermission(permissions.ARCRMTabCongViecXem) ? { name: "congviec", content: "Công việc", visible: true, } : { visible: false },
                        }}
                    />
                </Grid.Column>
            </Grid>
        </ContextARDmKhDetail.Provider>
    }
}

const PageHeader = ({ item }) => {
    return <>
        <Helmet idMessage={"ardmkh.detail"}
            defaultMessage={"Chi tiết khách hàng"} />

        <SegmentHeader>
            <HeaderLink listHeader={listHeader}>
                <Breadcrumb.Divider icon='right chevron' />
                <Breadcrumb.Section active>
                    {item?.ten_kh || "Chi tiết"}
                </Breadcrumb.Section>
            </HeaderLink>
        </SegmentHeader>
        <br />
    </>
}

const listHeader = [{
    id: "ardmkh",
    defaultMessage: "Khách hàng",
    route: routes.ARDmkh,
    active: false,
    isSetting: false,
}]

export { ContextARDmKhDetail }
export const ARDmKhDetail = {
    route: routes.ARDmKhDetail(),
    Zenform: ARDmKhDetailComponent,
    action: {
        view: { visible: true, permission: permissions.ARDmKhXem },
    },
}