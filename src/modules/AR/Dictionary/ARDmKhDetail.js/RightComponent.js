import React, { useContext, useEffect, useMemo, useRef, useState } from "react";
import { useHistory } from "react-router-dom";
import {
   Button, Card, Form,
   Menu, Message, Segment, TextArea, Table,
} from "semantic-ui-react";
import {
   ZenMessageAlert, ZenButton, DiscussComment,
   FormatDate, ConfirmDelete, ZenMessageToast,
   ZenFieldSelectApi, ZenField, ZenFormik, ZenLoading, ContainerScroll,
   ButtonDelete, ButtonEdit, ZenFieldTextArea
} from "../../../../components/Control/index";
import { FormMode, zzControlHelper } from "../../../../components/Control/zzControlHelper";
import { ZenHelper } from "../../../../utils";
import { ZenLookup } from "../../../ComponentInfo/Dictionary/ZenLookup";
import { getModal } from "../../../ComponentInfo/Dictionary/ZenGetModal";
import { ApiCRMDeal, ApiCRMHoatDong, ApiCRMNote, ApiSOVchSO0 } from "../../../CRM/Api";
import { IntlFormat } from "../../../../utils/intlFormat";
import { ValidError } from "../../../../utils/language/variable";
import { ApiArDmKh } from "../../Api";
import { ContextARDmKhDetail } from "./ARDmKhDetail";
import { Mess_SIDmHd } from "../../../SI/zLanguage/variable"
import { ApiSIDmHd } from "../../../SI/Api";
import { Link } from "react-router-dom";
import * as routes from "../../../../constants/routes"
import { auth } from "../../../../utils";
import _ from "lodash";
import { CongViecComponent } from "./CongViecComponent";
import { useHeaderList } from "../../../../components/Control/zzCustomHook";
import FormatNumber from "../../../../components/Control/FormatNumber"
import * as permissions from "../../../../constants/permissions";
import { useIntl } from "react-intl";

export const ContextArDmKh = React.createContext({})

export const RightComponent = ({ itemKH, keyData, type, menuBar, idElementContainer, ...rest }) => {

   return <MenuBar idElementContainer={idElementContainer} typeForm={type} valueKey={rest.valueKey} menuBar={menuBar}>
      {
         ({ activeMenu }) => {
            return <>
               {menuBar.KhInfo.visible && menuBar.KhInfo.name === activeMenu && <TabKHInfo itemKH={itemKH} type={type} keyData={keyData} {...rest} />}
               {menuBar.GhiChu.visible && menuBar.GhiChu.name === activeMenu && <TabNote data={itemKH} type={type} keyData={keyData} {...rest} />}
               {menuBar.HoatDong.visible && menuBar.HoatDong.name === activeMenu && <TabHoatDong data={itemKH} type={type} keyData={keyData} {...rest} />}
               {menuBar.Deal.visible && menuBar.Deal.name === activeMenu && <TabCohoi data={itemKH} type={type} keyData={keyData} {...rest} />}
               {menuBar.BaoGia.visible && menuBar.BaoGia.name === activeMenu && <TabBaoGia data={itemKH} type={type} keyData={keyData} {...rest} />}
               {menuBar.HopDong.visible && menuBar.HopDong.name === activeMenu && <TabHopDong data={itemKH} type={type} keyData={keyData} {...rest} />}
               {menuBar.TraoDoi.visible && menuBar.TraoDoi.name === activeMenu && <TabTraoDoi itemKH={itemKH} type={type} keyData={keyData} {...rest} />}
               {menuBar.CongViec.visible && menuBar.CongViec.name === activeMenu && <CongViecComponent data={itemKH} type={type} keyData={keyData} fileCodeName={"CRM"} {...rest} />}
            </>
         }
      }
   </MenuBar>
}

const MenuBar = ({ children, typeForm, valueKey, idElementScroll = "content-right", idElementContainer, menuBar }) => {
   // ************ MEMO
   const memoKey = useMemo(() => typeForm + valueKey, [])
   const memoizeMenu = useMemo(() => getMenu(), [])
   // ************ STATE
   const [loadingForm, setLoadingForm] = useState(true)
   const [activeMenu, setActiveMenu] = useState(memoizeMenu.defaultMenu)

   useEffect(() => {
      disableLoadingMenuBar(activeMenu)
   }, [])

   // ************ HANDLE
   const handleChangeTab = (e, { name }) => {
      if (name !== activeMenu) {
         if (disableLoadingMenuBar(name) === false) {
            setLoadingForm(true)
         }
         setActiveMenu(name)
         memoryStore.set(memoKey, { activeMenu: name })
      }
   }

   // ************ FUNCTION
   function disableLoadingMenuBar(menuName) {
      // các tab ko set loading từ menubar
      if (menuName === menuBar.CongViec.name) {
         setLoadingForm(false)
         return true
      }
      return false
   }

   function getMenu() {
      let defaultMenu;
      const menus = Object.keys(menuBar).map(menu => {
         const itemMenu = menuBar[menu]
         if (itemMenu.default) {
            // default tab theo khai báo
            defaultMenu = itemMenu.name
         }
         return itemMenu
      })
      // get activeMenu in memory
      const tempActiveMenu = memoryStore.get(memoKey)
      defaultMenu = tempActiveMenu?.activeMenu ? tempActiveMenu.activeMenu : defaultMenu
      return {
         menus: menus,
         defaultMenu: defaultMenu
      }
   }

   return <ContextArDmKh.Provider
      value={{
         setLoadingForm: (isLoad) => setLoadingForm(isLoad),
         loadingForm: loadingForm,
      }}
   >
      <Segment>
         <Menu pointing secondary>
            {memoizeMenu.menus.map(menu => {
               const { visible, ...restMenu } = menu
               return visible ? <Menu.Item key={menu.name}
                  {...restMenu}
                  active={activeMenu === menu.name}
                  onClick={handleChangeTab}
               /> : undefined
            })}
         </Menu>

         <ContainerScroll
            id={idElementScroll} isSegment={false}
            idElementContainer={idElementContainer}
            heightSub={28}
         >
            {children({ activeMenu: activeMenu })}
            <ZenLoading loading={loadingForm} inline="centered" />
         </ContainerScroll>
      </Segment>
   </ContextArDmKh.Provider>
}

const TabKHInfo = (props) => {
   const { itemKH, type, keyData } = props;
   const { setLoadingForm, loadingForm } = useContext(ContextArDmKh);
   const { setItemKH } = useContext(ContextARDmKhDetail);
   const [permission, setPermission] = useState(false);
   const [loadingSave, setLoadingSave] = useState(false);
   const [dataKH, setDataKH] = useState();
   const refForm = useRef();
   const intl = useIntl()

   useEffect(() => {
      setLoadingForm(false)
   }, [])

   useEffect(() => {
      setDataKH({ ...itemKH })
   }, [itemKH])

   const memoSiDmLoai = useMemo(() => {
      const listSiDmLoai = ZenHelper.getDataLocal(ZenLookup.SIDmloai.code)
      const data = listSiDmLoai?.data?.filter(t => t.ma_nhom === "PLKH")
      var result = data?.reduce(
         (obj, item) => Object.assign(obj, { [item.ma]: item.ten }), {}
      );
      return result || {}
   }, [])

   function checkpermislimit(permis) {
      let user = auth.getUserInfo()
      return !permis ? false : user.permisson.includes(permis);
   }

   const handleEdit = () => {
      let user = auth.getUserInfo()

      if (auth.checkPermission(permissions.ARDmKhSua) && checkpermislimit(permissions.ARDmKhSuaPhuTrach)) {
         if (itemKH.ma_nvkd === user.hrid) {
            if (permission) {
               refForm.current.handleReset()
            }
            setPermission(!permission)
         } else {
            ZenMessageAlert.warning("Bạn không phải là người phụ trách khách hàng này")
            return
         }
      } else {
         if (auth.checkPermission(permissions.ARDmKhSua) && !checkpermislimit(permissions.ARDmKhSuaPhuTrach)) {
            if (permission) {
               refForm.current.handleReset()
            }
            setPermission(!permission)
         } else {
            ZenMessageAlert.warning(
               intl.formatMessage({ id: "component.permission", defaultMessage: "Không có quyền truy cập" })
            )
            return
         }
      }
   }

   const handleSubmit = (values) => {
      setLoadingSave(true)
      ApiArDmKh.update(values, res => {
         if (res.status === 204) {
            setItemKH({ ...itemKH, ...values })
            ZenMessageToast.success();
         } else {
            ZenMessageAlert.error(zzControlHelper.getResponseError(res))
         }
         setPermission(!permission)
         setLoadingSave(false)
      })
   }

   return <>
      <ZenFormik form={"TabKHInfo"}
         ref={refForm}
         validation={formValidationKH}
         initItem={dataKH}
         onSubmit={handleSubmit}
      >
         {
            formik => {
               return <Form>
                  <Form.Group widths="3">
                     <ZenField autoFocus
                        readOnly
                        label={"ardmkh.ma_kh"} defaultlabel="Mã khách hàng"
                        name="ma_kh"
                        textCase="uppercase"
                        maxLength={20}
                        props={formik} isCode
                     />
                     <ZenField
                        readOnly={!permission}
                        label={"ardmkh.ma_so_thue"} defaultlabel="Mã số thuế"
                        name="ma_so_thue"
                        props={formik} isCode
                     />
                  </Form.Group>

                  <ZenField readOnly={!permission}
                     label={"ardmkh.ten_kh"} defaultlabel="Tên khách hàng"
                     name="ten_kh" props={formik}
                  />

                  <ZenField readOnly={!permission}
                     label={"ardmkh.dia_chi"} defaultlabel="Địa chỉ"
                     name="dia_chi" props={formik}
                  />

                  <Form.Group widths="3">
                     <ZenField readOnly={!permission}
                        label={"ardmkh.email"} defaultlabel="Email"
                        name="email" props={formik}
                     />

                     <ZenField readOnly={!permission}
                        label={"ardmkh.tel"} defaultlabel="Điện thoại"
                        name="tel" props={formik}
                     />

                     <ZenField readOnly={!permission}
                        label={"ardmkh.home_page"} defaultlabel="Website"
                        name="home_page" props={formik}
                     />
                  </Form.Group>

                  <Form.Group widths="3">
                     <ZenFieldSelectApi
                        readOnly={!permission}
                        lookup={ZenLookup.Ma_nguon_kh}
                        label={"ardmkh.nguon_kh"} defaultlabel="Nguồn khách hàng"
                        name="ma_nguon_kh" formik={formik}
                     />

                     <ZenFieldSelectApi
                        readOnly={!permission}
                        lookup={{
                           ...ZenLookup.Ma_plkh,
                           onLocalWhere: (itemsLocal) => {
                              return itemsLocal?.filter(t => t.loai == "1") || []
                           },
                           where: 'loai = 1'
                        }}
                        isIntl={false} label={memoSiDmLoai["1"]}
                        name="ma_plkh1" formik={formik}
                     />
                  </Form.Group>

                  <Form.Group widths="3">
                     <ZenFieldSelectApi
                        readOnly={!permission}
                        lookup={ZenLookup.Ma_linh_vuc}
                        label={"ardmkh.linh_vuc"} defaultlabel="Lĩnh vực/Loại hình"
                        name="ma_linh_vuc" formik={formik}
                     />

                     <ZenFieldSelectApi
                        readOnly={!permission}
                        lookup={{
                           ...ZenLookup.Ma_plkh,
                           onLocalWhere: (itemsLocal) => {
                              return itemsLocal?.filter(t => t.loai == "2") || []
                           },
                           where: 'loai = 2'
                        }}
                        isIntl={false} label={memoSiDmLoai["2"]}
                        name="ma_plkh2" formik={formik}
                     />

                     <ZenFieldSelectApi
                        readOnly={!permission}
                        lookup={{
                           ...ZenLookup.Ma_plkh,
                           onLocalWhere: (itemsLocal) => {
                              return itemsLocal?.filter(t => t.loai == "3") || []
                           },
                           where: 'loai = 3'
                        }}
                        isIntl={false} label={memoSiDmLoai["3"]}
                        name="ma_plkh3" formik={formik}
                     />
                  </Form.Group>

                  <Form.Group widths="3">
                     <ZenFieldSelectApi
                        readOnly={!permission}
                        lookup={ZenLookup.SIDmQuocGia}
                        label={"ardmkh.ma_quoc_gia"} defaultlabel="Mã quốc gia"
                        name="ma_quoc_gia" formik={formik}
                        onItemSelected={(itemSelected, { name }) => {
                           formik.setValues({
                              ...formik.values,
                              [name]: itemSelected.ma_quoc_gia,
                              ma_tinh: "",
                              ma_huyen: ""
                           })
                        }}
                     />
                     <ZenFieldSelectApi
                        readOnly={!permission}
                        lookup={{
                           ...ZenLookup.SIDmTinh,
                           where: `ma_quoc_gia = '${formik.values.ma_quoc_gia || ''}'`,
                           onLocalWhere: (itemsLocal) => {
                              return itemsLocal.filter(t => t.ma_quoc_gia == formik.values.ma_quoc_gia)
                           }
                        }}
                        label={"ardmkh.ma_tinh"} defaultlabel="Mã tỉnh/TP"
                        name="ma_tinh" formik={formik}
                        onItemSelected={(itemSelected, { name }) => {
                           formik.setValues({
                              ...formik.values,
                              [name]: itemSelected.ma_tinh,
                              ma_huyen: ""
                           })
                        }}
                     />
                     <ZenFieldSelectApi
                        readOnly={!permission}
                        lookup={{
                           ...ZenLookup.SIDmHuyen,
                           where: `ma_tinh = '${formik.values.ma_tinh || ''}'`,
                           onLocalWhere: (itemsLocal) => {
                              return itemsLocal.filter(t => t.ma_tinh == formik.values.ma_tinh)
                           }
                        }}
                        label={"ardmkh.ma_huyen"} defaultlabel="Mã quận/huyện"
                        name="ma_huyen" formik={formik}
                     />
                  </Form.Group>

                  <ZenFieldTextArea readOnly={!permission}
                     label={"ardmkh.ghi_chu"} defaultlabel="Ghi chú"
                     name="ghi_chu" props={formik} />
               </Form>
            }
         }
      </ZenFormik>
      <div style={{ paddingTop: "14px" }}>
         {permission ? <>
            <ZenButton btnType="save" permission={permissions.ARDmKhSua}
               size="small"
               loading={loadingSave}
               onClick={() => refForm.current.handleSubmit()}
            />
            <ZenButton btnType="cancel"
               size="small"
               loading={loadingSave}
               onClick={handleEdit}
            />
         </>
            : <ZenButton primary
               permission={permissions.ARDmKhSua}
               size="small"
               icon="edit"
               content={"Sửa thông tin chung"}
               onClick={handleEdit}
            />}
      </div>
   </>
}

const TabNote = ({ data, keyData = "id", type, valueKey }) => {
   const { setLoadingForm, loadingForm } = useContext(ContextArDmKh)
   const _isMounted = useRef(true);
   const [err, setErr] = useState()
   const [items, setItems] = useState([])
   const [comment, setComment] = useState("")
   const [editItem, setEditItem] = useState()

   const memoUserInfo = useMemo(() => {
      return auth.getUserInfo();
   }, [])

   useEffect(() => {
      getItems();
      return () => {
         _isMounted.current = false
      }
   }, [])

   const handleSaveGhiChu = () => {
      if (editItem) {
         updateItem()
      } else {
         insItem()
      }
   }

   const handleAction = (e, item, mode) => {
      e.preventDefault();

      if (mode === FormMode.EDIT) {
         setEditItem({ ...item })
      } else if (mode === FormMode.DEL) {
         delItem(item)
      }
   }

   function insItem() {
      const dataIns = {
         content: comment,
         associate: {
            object_type: type,
            object_id: keyData === "id" ? data[keyData] : "",
            object_code: keyData !== "id" ? data[keyData] : ""
         }
      }
      ApiCRMNote.insert(dataIns, res => {
         if (res.status === 201) {
            setItems([{ ...res.data.data, cuser_name: memoUserInfo.fullName }, ...items])
            setComment("")
         } else {
            ZenMessageAlert.error(ZenHelper.getResponseError(res))
         }
      })
   }

   function updateItem() {
      const patchData = [
         {
            value: editItem.content,
            path: `/content`,
            op: "replace",
            operationType: 0,
         }
      ]

      ApiCRMNote.updatePatch(editItem.id, patchData,
         res => {
            if (res.status === 200) {
               setItems(items.map(item => item.id == editItem.id ? editItem : item))
               setEditItem()
            } else {
               ZenMessageAlert.error(zzControlHelper.getResponseError(res))
            }
         })
   }

   function delItem(item) {
      ZenMessageAlert.question("Bạn có chắc muốn xóa không?")
         .then(success => {
            if (success == "1") {
               ApiCRMNote.delete(item.id, res => {
                  if (res.status === 204) {
                     setItems(items.filter(t => t.id != item.id))
                     ZenMessageToast.success();
                  } else {
                     ZenMessageAlert.error(zzControlHelper.getResponseError(res))
                  }
               })
            }
         })
   }

   function getItems() {
      const params = {
         type: type,
         oid: "",
         ocode: valueKey
      }
      ApiCRMNote.get(params, res => {
         if (_isMounted.current) {
            if (res.status === 200) {
               setItems(res.data.data)
            } else {
               setErr(ZenHelper.getResponseError(res))
            }
            loadingForm && setLoadingForm(false)
         }
      })
   }

   function inputComment() {
      return <Form className="ar-r-note">
         <TextArea name="content" value={comment}
            onChange={(e, { value }) => setComment(value)}
            placeholder="Nhập ghi chú..."
         />
         {
            comment.trim() && <div>
               <ZenButton btnType="save" size="mini" onClick={handleSaveGhiChu} />
               <ZenButton btnType="cancel" size="mini" content="Hủy" onClick={() => setComment("")} />
            </div>
         }
      </Form>
   }

   function inputCommentEdit(item) {
      return <Form>
         <TextArea name="content" value={editItem?.content}
            onChange={(e, { value }) => setEditItem({ ...editItem, content: value })}
            placeholder="Nhập ghi chú..."
         />
         {
            editItem && <div>
               <ZenButton btnType="save" size="mini" onClick={handleSaveGhiChu} />
               <ZenButton btnType="cancel" size="mini" content="Hủy" onClick={() => setEditItem()} />
            </div>
         }
      </Form>
   }

   return <>
      {err && <Message negative list={err} />}
      {inputComment()}

      {items.map((item, idx) => {
         return <Card key={item.id || idx} link fluid
            style={styles.hd_card}
         >
            <Card.Content>
               <p>
                  <span style={{ fontWeight: "bold" }}>Ghi chú bởi:</span>
                  {` ${item.cuser_name || "[Unknown]"} - Lúc: `} <FormatDate value={item.cdate} showTime />
               </p>

               {editItem?.id === item.id ? inputCommentEdit(item)
                  : <p style={{ whiteSpace: "pre-wrap", paddingRight: "100px" }}>
                     <span style={{ fontWeight: "bold" }}>Nội dung: </span>
                     <span>{item.content}</span>
                  </p>}

               {(editItem && editItem.id == item.id) || memoUserInfo?.userId == item.cuser && <p>
                  <a href="3" onClick={(e) => handleAction(e, item, FormMode.EDIT)}>Sửa</a>
                  <a href="3" style={{ paddingLeft: "14px" }}
                     onClick={(e) => handleAction(e, item, FormMode.DEL)}
                  >
                     Xóa
                  </a>
               </p>}
            </Card.Content>
         </Card>
      })}
   </>
}

const TabHoatDong = ({ data, keyData = "id", type, valueKey }) => {
   const { setLoadingForm, loadingForm } = useContext(ContextArDmKh)
   const _isMounted = useRef(true);
   const [err, setErr] = useState()
   const [items, setItems] = useState([])
   const [itemData, setItemData] = useState()
   const [infoModal, setInfoModal] = useState({})

   useEffect(() => {
      const info = getModal(ZenLookup.CRMHoatDong)
      setInfoModal(info)
      getItems()
      return () => {
         _isMounted.current = false
      }
   }, [])

   // ********************** HANDLE
   const handleOpenCloseModal = (formMode, item) => {
      setInfoModal({
         ...infoModal,
         id: item ? item.id : "",
         formMode: formMode,
         open: true,
         other: {
            id: data[keyData],
            keyData: keyData,
            type: type,
         }
      })
   }

   const handleAfterSaveModal = (newItem, { mode }) => {
      if (mode === FormMode.ADD) {
         setItems(items.concat(newItem))
      } else if (mode === FormMode.EDIT) {
         setItems(items.map(t => t.id == newItem.id ? newItem : t))
      }
      // close modal
      setInfoModal({
         ...infoModal,
         id: "",
         formMode: FormMode.NON,
         open: false,
      })
   }

   const handleRemoveItem = () => {
      setItemData({ ...itemData, loadingRemove: true })

      ApiCRMHoatDong.delete(itemData.id, res => {
         if (res.status === 204) {
            setItems(items.filter(t => t.id != itemData.id))
            setItemData()
            ZenMessageToast.success()
         } else {
            setItemData()
            ZenMessageAlert.error(ZenHelper.getResponseError(res))
         }
      })
   }

   // ********************** FUNCTION
   function getItems() {
      const params = {
         type: type,
         oid: "",
         ocode: valueKey
      }

      ApiCRMHoatDong.get(params, res => {
         if (_isMounted.current) {
            if (res.status === 200) {
               setItems(res.data.data)
            } else {
               setErr(ZenHelper.getResponseError(res))
            }
            loadingForm && setLoadingForm(false)
         }
      })
   }

   return <>
      <ZenButton btnType="add" size="tiny" floated="right"
         onClick={() => handleOpenCloseModal(FormMode.ADD)}
      />
      <div style={{ clear: "both" }} />
      {err && <Message list={err} negative header="Error" />}

      {
         items.map((item, idx) => {
            return <Card key={item.id || idx} link fluid
               style={styles.hd_card}
            >
               <Card.Content>
                  <div style={{ float: "right" }}>
                     <ZenButton btnType="delete" visibleContent={false} size="mini"
                        onClick={() => setItemData({ id: item.id })} />
                     <ZenButton btnType="edit" visibleContent={false} size="mini" primary
                        onClick={() => handleOpenCloseModal(FormMode.EDIT, item)}
                     />
                  </div>

                  <div>
                     <span style={{ fontSize: "1.3em", fontWeight: "bold" }}>{item.ten_hoat_dong} {" | "}</span>
                     {`Thực hiện bởi: ${item.cuser_name || "[Unknown]"} - Lúc: `} <FormatDate value={item.thoi_gian} showTime />
                  </div>

                  <Card.Description style={{ color: "black" }}>
                     <p>
                        <strong>
                           Kết quả
                        </strong>
                        {`: ${item.ket_qua}`}
                     </p>

                     <div className="ztb-textarea">
                        <p>
                           <span style={{ fontWeight: "bold" }}>Mô tả: </span>
                           {item.noi_dung}
                        </p>
                     </div>
                  </Card.Description>
               </Card.Content>
            </Card>
         })
      }

      {itemData &&
         <ConfirmDelete open loading={itemData.loadingRemove}
            onCancel={() => setItemData()} onConfirm={handleRemoveItem} />}

      {zzControlHelper.openModalComponent(infoModal,
         {
            onClose: () => handleOpenCloseModal(FormMode.NON),
            onAfterSave: handleAfterSaveModal,
         }
      )}
   </>
}

const TabHopDong = ({ data, keyData = "id", type, valueKey, listHeader }) => {
   const { setLoadingForm, loadingForm } = useContext(ContextArDmKh);
   const _isMounted = useRef(true);
   const [err, setErr] = useState();
   const [items, setItems] = useState([]);
   const [infoModal, setInfoModal] = useState(getModal(ZenLookup.Ma_hd));
   const history = useHistory();
   const [headers, setHeaders] = useHeaderList();

   useEffect(() => {
      getItems();
      return () => {
         _isMounted.current = false
      }
   }, [])

   // ********************** HANDLE
   const handleOpenCloseModal = (formMode, item) => {
      setInfoModal({
         ...infoModal,
         info: {
            ...infoModal.info,
            checkPermission: (permis) => auth.checkPermission(permis),
            initItem: { ...infoModal.info.initItem, [keyData]: valueKey },
         },
         id: item ? item["ma_hd"] : "",
         formMode: formMode,
         open: true,
         other: {
            id: data[keyData],
            keyData: keyData,
            type: type,
         }
      })
   }

   const handleAfterSaveModal = (newItem, { mode }) => {
      // change
      if (mode === FormMode.ADD) {
         setItems(items.concat(newItem))
      } else if (mode === FormMode.EDIT) {
         setItems(items.map(t => t["ma_hd"] === newItem["ma_hd"] ? newItem : t))
      }
      // close modal
      setInfoModal({
         ...infoModal,
         id: "",
         formMode: FormMode.NON,
         open: false,
      })
   }

   const handleDeleteItem = (item) => {
      ZenMessageAlert.question("Bạn có chắc muốn xóa không?")
         .then(success => {
            if (success == 1) {
               ApiSIDmHd.delete(item.ma_hd, res => {
                  if (res.status >= 200 && res.status <= 204) {
                     setItems(items.filter(t => t.ma_hd != item.ma_hd))
                     ZenMessageToast.success()
                  } else {
                     ZenMessageAlert.error(res)
                  }
               })
            }
         })
   }

   // ********************** FUNCTION
   function getItems() {
      ApiSIDmHd.get(res => {
         if (_isMounted.current) {
            if (res.status === 200) {
               setItems(res.data.data)
            } else {
               setErr(ZenHelper.getResponseError(res))
            }
            loadingForm && setLoadingForm(false)
         }
      }, { qf: `a.${[keyData]} = '${valueKey}'` })
   }

   const handleAction = (item, isWithHistory = true) => {
      const listHeaderToBaoGia = listHeader.concat({
         text: data.ten_kh,
         route: routes.ARDmKhDetail(zzControlHelper.btoaUTF8(valueKey)),
         active: false,
      })

      const pathUrl = getUrlChiTiet(item?.ma_hd)

      if (item) {
         listHeaderToBaoGia.push({
            text: `Hợp đồng - ${item.ma_hd}`,
            route: pathUrl.pathname,
            active: true,
         })
      } else {
         listHeaderToBaoGia.push({
            text: "Hợp đồng - Thêm mới",
            route: pathUrl.pathname,
            active: true,
         })
      }

      // set header khi chuyển route
      setHeaders(listHeaderToBaoGia)
      isWithHistory && history.push(pathUrl)
   }

   function renderList() {
      return <Table compact selectable striped
         className="crm-sticky-table"
      >
         <Table.Header>
            <Table.Row>
               <Table.HeaderCell content={IntlFormat.text(Mess_SIDmHd.Ma)} />
               <Table.HeaderCell content={"Ngày ký"} />
               <Table.HeaderCell content={IntlFormat.text(Mess_SIDmHd.Ten)} />
               <Table.HeaderCell content={"Nhóm hợp đồng"} />
               <Table.HeaderCell content={"Nhân viên kinh doanh"} />
               <Table.HeaderCell content="" />
            </Table.Row>
         </Table.Header>
         <Table.Body>
            {

               items.map(item => {
                  return <Table.Row key={item["ma_hd"]}
                     onDoubleClick={() => handleOpenCloseModal(FormMode.VIEW, item)}
                  >
                     <Table.Cell>
                        {/* <ZenLink style={{ fontWeight: "bold" }}
                           to={routes.SIDmHdDetail(zzControlHelper.btoaUTF8(item.ma_hd))}
                        >  
                           {item.ma_hd}
                        </ZenLink> */}
                        <a style={{ fontWeight: "bold" }}
                           href={`#${getUrlChiTiet(item.ma_hd)?.pathname}`}
                           onContextMenu={(e) => handleAction(item, false)}

                           onClick={(e) => {
                              e.preventDefault(e)
                              handleAction(item)
                           }}
                        >
                           {item.ma_hd}
                        </a>
                     </Table.Cell>
                     <Table.Cell content={<FormatDate value={item.ngay_hd} />} />
                     <Table.Cell content={item.ten_hd} />
                     <Table.Cell content={item.ten_nhhd} />
                     <Table.Cell content={item.id_nvkd} />
                     <Table.Cell collapsing>
                        <Button.Group size="tiny" style={{ float: "right" }}>
                           <ButtonEdit
                              onClick={() => handleOpenCloseModal(FormMode.EDIT, item)}
                           />
                           <ButtonDelete
                              onClick={() => handleDeleteItem(item)}
                           />
                        </Button.Group>
                     </Table.Cell>
                  </Table.Row>
               })
            }
         </Table.Body>
      </Table>
   }

   function getUrlChiTiet(sttrec) {
      if (sttrec) {
         return {
            pathname: `${routes.SIDmHdDetail(zzControlHelper.btoaUTF8(sttrec))}`,
         }
      }
      else {
         return {
            pathname: `${routes.SIDmHd}`,
            state: { ma_kh: valueKey }
         }
      }
   }

   return <>
      <ZenButton btnType="add" size="tiny" floated="right"
         onClick={() => handleOpenCloseModal(FormMode.ADD)}
      />
      <div style={{ clear: "both" }} />

      {err && <Message list={err} negative header="Error" />}

      {renderList()}

      {zzControlHelper.openModalComponent(infoModal,
         {
            onClose: () => handleOpenCloseModal(FormMode.NON),
            onAfterSave: handleAfterSaveModal,
         }
      )}
   </>
}

const TabCohoi = ({ data, keyData = "id", type, valueKey, listHeader }) => {
   const { setLoadingForm, loadingForm } = useContext(ContextArDmKh);
   const _isMounted = useRef(true);
   const [err, setErr] = useState();
   const [items, setItems] = useState([]);
   const [infoModal, setInfoModal] = useState(getModal(ZenLookup.CRMDeal));
   const history = useHistory();
   const [headers, setHeaders] = useHeaderList();

   useEffect(() => {
      getItems()

      return () => {
         _isMounted.current = false
      }
   }, [])

   // ********************** HANDLE
   const handleOpenCloseModal = (formMode, item) => {
      setInfoModal({
         ...infoModal,
         info: {
            ...infoModal.info,
            checkPermission: (permis) => auth.checkPermission(permis),
            initItem: { ...infoModal.info.initItem, [keyData]: valueKey },
         },
         id: item ? item.id : "",

         formMode: formMode,
         open: true,
         other: {
            id: data[keyData],
            keyData: keyData,
            type: type,
         }
      })
   }

   const handleAfterSaveModal = (newItem, { mode }) => {
      // change
      if (mode === FormMode.ADD) {
         setItems(items.concat(newItem))
      } else if (mode === FormMode.EDIT) {
         setItems(items.map(t => t["ma_hd"] === newItem["ma_hd"] ? newItem : t))
      }
      // close modal
      setInfoModal({
         ...infoModal,
         id: "",
         formMode: FormMode.NON,
         open: false,
      })
   }

   const handleDeleteItem = (item) => {
      ZenMessageAlert.question("Bạn có chắc muốn xóa không?")
         .then(success => {
            if (success == 1) {

               ApiCRMDeal.delete(item.id, res => {
                  if (res.status >= 200 && res.status <= 204) {
                     setItems(items.filter(t => t.ma_deal != item.ma_deal))
                     ZenMessageToast.success()
                  } else {
                     ZenMessageAlert.error(res)
                  }
               })
            }
         })
   }

   // ********************** FUNCTION
   function getItems() {
      ApiArDmKh.getDeal(valueKey, res => {
         if (_isMounted.current) {
            if (res.status === 200) {
               setItems(res.data.data)
            } else {
               setErr(ZenHelper.getResponseError(res))
            }
            loadingForm && setLoadingForm(false)
         }
      })
   }

   const handleAction = (item, isWithHistory = true) => {
      const listHeaderToBaoGia = listHeader.concat({
         text: data.ten_kh,
         route: routes.ARDmKhDetail(zzControlHelper.btoaUTF8(valueKey)),
         active: false,
      })

      const pathUrl = getUrlChiTiet(item?.id)

      if (item) {
         listHeaderToBaoGia.push({
            text: `Cơ hội - ${item.ma_deal}`,
            route: pathUrl.pathname,
            active: true,
         })
      } else {
         listHeaderToBaoGia.push({
            text: "Cơ hội - Thêm mới",
            route: pathUrl.pathname,
            active: true,
         })
      }

      // set header khi chuyển route
      setHeaders(listHeaderToBaoGia)
      isWithHistory && history.push(pathUrl)
   }

   function renderList() {
      return <Table compact selectable striped
         className="crm-sticky-table"
      >
         <Table.Header>
            <Table.Row>
               <Table.HeaderCell content={"Mã deal"} />
               <Table.HeaderCell content={"Tên deal"} />
               <Table.HeaderCell content={"Sản phẩm/dịch vụ"} />
               <Table.HeaderCell content={"Giai đoạn"} />
               <Table.HeaderCell content={"Giá trị dự kiến"} />
               <Table.HeaderCell content={"Ngày chốt dự kiến"} />
               <Table.HeaderCell content="" />
            </Table.Row>
         </Table.Header>
         <Table.Body>
            {

               items.map((item, idx) => {
                  return <Table.Row key={item.id || `ma_deal${idx}`}
                     onDoubleClick={() => handleOpenCloseModal(FormMode.VIEW, item)}
                  >
                     <Table.Cell>
                        <a style={{ fontWeight: "bold" }}
                           href={`#${getUrlChiTiet(item.id)?.pathname}`}
                           onContextMenu={(e) => handleAction(item, false)}

                           onClick={(e) => {
                              e.preventDefault(e)
                              handleAction(item)
                           }}
                        >
                           {item.ma_deal}
                        </a>
                     </Table.Cell>
                     <Table.Cell content={item.ten_deal} />
                     <Table.Cell content={item.nhu_cau} />
                     <Table.Cell content={item.ma_giai_doan} />
                     <Table.Cell textAlign={'right'}><FormatNumber isZero={false} value={item.gia_tri_dk} /></Table.Cell>
                     <Table.Cell content={<FormatDate value={item.ngay_dong_dk} />} />
                     <Table.Cell collapsing>
                        <Button.Group size="tiny" style={{ float: "right" }}>
                           <ButtonEdit
                              onClick={() => handleOpenCloseModal(FormMode.EDIT, item)}
                           />
                           <ButtonDelete
                              onClick={() => handleDeleteItem(item)} />
                        </Button.Group>
                     </Table.Cell>
                  </Table.Row>
               })
            }
         </Table.Body>
      </Table>
   }

   function getUrlChiTiet(sttrec) {
      if (sttrec) {
         return {
            pathname: `${routes.CRMDealDetail(zzControlHelper.btoaUTF8(sttrec))}`,
         }
      }
      else {
         return {
            pathname: `${routes.CRMDeal}`,
            state: { ma_kh: valueKey }
         }
      }
   }

   return <>
      <ZenButton btnType="add" size="tiny" floated="right"
         onClick={() => handleOpenCloseModal(FormMode.ADD)}
      />
      <div style={{ clear: "both" }} />

      {err && <Message list={err} negative header="Error" />}

      {renderList()}

      {zzControlHelper.openModalComponent(infoModal,
         {
            onClose: () => handleOpenCloseModal(FormMode.NON),
            onAfterSave: handleAfterSaveModal,
         }
      )}
   </>
}

const TabBaoGia = ({ data, keyData = "id", type, valueKey, listHeader }) => {
   const { setLoadingForm, loadingForm } = useContext(ContextArDmKh);
   const _isMounted = useRef(true);
   const [err, setErr] = useState();
   const [items, setItems] = useState([]);
   const [infoModal, setInfoModal] = useState({});
   const history = useHistory();
   const [headers, setHeaders] = useHeaderList();

   useEffect(() => {
      getItems()
      return () => {
         _isMounted.current = false
      }
   }, [])

   // ********************** HANDLE
   const handleOpenCloseModal = (formMode, item) => {
      setInfoModal({
         ...infoModal,
         info: {
            ...infoModal.info,
            checkPermission: (permis) => auth.checkPermission(permis),
            initItem: { ...infoModal.info.initItem, [keyData]: valueKey },
         },
         id: item ? item["ma_hd"] : "",
         formMode: formMode,
         open: true,
         other: {
            id: data[keyData],
            keyData: keyData,
            type: type,
         }
      })
      ZenMessageAlert.warning("Chưa hoàn thiện")
   }

   const handleAfterSaveModal = (newItem, { mode }) => {
      // change
      if (mode === FormMode.ADD) {
         setItems(items.concat(newItem))
      } else if (mode === FormMode.EDIT) {
         setItems(items.map(t => t["ma_hd"] === newItem["ma_hd"] ? newItem : t))
      }
      // close modal
      setInfoModal({
         ...infoModal,
         id: "",
         formMode: FormMode.NON,
         open: false,
      })
   }

   const handleDeleteItem = (item) => {
      ZenMessageAlert.question("Bạn có chắc muốn xóa không?")
         .then(success => {
            if (success == 1) {
               ApiSOVchSO0.delete(item.stt_rec, res => {
                  if (res.status >= 200 && res.status <= 204) {
                     setItems(items.filter(t => t.ma_hd != item.ma_hd))
                     ZenMessageToast.success()
                  } else {
                     ZenMessageAlert.error(res)
                  }
               })
            }
         })
   }

   const handleAction = (item, isWithHistory = true) => {
      const listHeaderToBaoGia = listHeader.concat({
         text: data.ten_kh,
         route: routes.ARDmKhDetail(zzControlHelper.btoaUTF8(valueKey)),
         active: false,
      })

      const pathUrl = getUrlChiTiet(item?.stt_rec)

      if (item) {
         listHeaderToBaoGia.push({
            text: `Báo giá - ${item.so_ct}`,
            route: pathUrl.pathname,
            active: true,
         })
      } else {
         listHeaderToBaoGia.push({
            text: "Báo giá - Thêm mới",
            route: pathUrl.pathname,
            active: true,
         })
      }

      // set header khi chuyển route
      setHeaders(listHeaderToBaoGia)
      isWithHistory && history.push(pathUrl)
   }

   // ********************** FUNCTION
   function getItems() {
      ApiSOVchSO0.get(res => {
         if (_isMounted.current) {
            if (res.status === 200) {
               setItems(res.data.data)
            } else {
               setErr(ZenHelper.getResponseError(res))
            }
            loadingForm && setLoadingForm(false)
         }
      }, { qf: `a.${[keyData]} = '${valueKey}'` })
   }

   function renderList() {
      return <Table compact selectable striped
         className="crm-sticky-table"
      >
         <Table.Header>
            <Table.Row>
               <Table.HeaderCell content={"Số báo giá"} />
               <Table.HeaderCell content={"Ngày"} />
               <Table.HeaderCell content={"Người nhận"} />
               <Table.HeaderCell content={"Ngày hết hiệu lực"} />
               <Table.HeaderCell content={"Nhân viên phụ trách"} />
               <Table.HeaderCell content="" />
            </Table.Row>
         </Table.Header>
         <Table.Body>
            {

               items.map(item => {
                  return <Table.Row key={item["ma_hd"]}>
                     <Table.Cell>
                        <a style={{ fontWeight: "bold" }}
                           href={`#${getUrlChiTiet(item.stt_rec)?.pathname}`}
                           // event right click
                           onContextMenu={(e) => handleAction(item, false)}

                           onClick={(e) => {
                              e.preventDefault(e)
                              handleAction(item)
                           }}
                        >
                           {item.so_ct}
                        </a>
                     </Table.Cell>
                     <Table.Cell content={<FormatDate value={item.ngay_ct} />} />
                     <Table.Cell content={item.nguoi_gd} />
                     <Table.Cell content={<FormatDate value={item.ngay_hh} />} />
                     <Table.Cell content={item.id_nvkd} />
                     <Table.Cell collapsing>
                        <Button.Group size="tiny" style={{ float: "right" }}>
                           <ButtonEdit
                              onClick={() => handleAction(item)}
                           />
                           <ButtonDelete
                              onClick={() => handleDeleteItem(item)}
                           />
                        </Button.Group>
                     </Table.Cell>
                  </Table.Row>
               })
            }
         </Table.Body>
      </Table >
   }

   function getUrlChiTiet(sttrec) {
      if (sttrec) {
         return {
            pathname: `${routes.SOVchSO0Edit(zzControlHelper.btoaUTF8(sttrec))}`,
         }
      } else {
         return {
            pathname: `${routes.SOVchSO0New}`,
            state: { ma_kh: valueKey }
         }
      }
   }

   return <>
      <ZenButton btnType="add" size="tiny" floated="right"
         onClick={() => handleAction()}
      />
      <div style={{ clear: "both" }} />
      {err && <Message list={err} negative header="Error" />}

      {renderList()}

      {zzControlHelper.openModalComponent(infoModal,
         {
            onClose: () => handleOpenCloseModal(FormMode.NON),
            onAfterSave: handleAfterSaveModal,
         }
      )}
   </>
}

const TabTraoDoi = (props) => {
   const { type, valueKey } = props
   const { setLoadingForm, loadingForm } = useContext(ContextArDmKh);

   useEffect(() => {
      setLoadingForm(false)
   }, [])
   return <DiscussComment codeName="CRM" type={type} keyValue={valueKey} />
}

const formValidationKH = [
   {
      ...ValidError.formik({ name: "ma_kh", type: "string" },
         { type: "required", intl: IntlFormat.default(ValidError.Required) },
         { type: "max", params: [20], intl: { ...IntlFormat.default(ValidError.MaxLength) } }
      )
   },
   {
      ...ValidError.formik({ name: "ten_kh", type: "string" },
         { type: "required", intl: IntlFormat.default(ValidError.Required) }
      )
   },
]

const styles = {
   hd_card: {
      borderLeft: "solid 1px rgba(34, 36, 38, 0.1)",
      borderRight: "solid 1px rgba(34, 36, 38, 0.1)",
      borderBottom: "solid 1px rgba(34, 36, 38, 0.1)",
   }
}

/** Ghi nhớ vị trí của tab */
const memoryStore = {
   _data: new Map(),
   get(key) {
      if (!key) {
         return null
      }

      return this._data.get(key) || null
   },
   set(key, data) {
      if (!key) {
         return
      }
      return this._data.set(key, data)
   }
}
