import React, { useEffect, useRef, useState } from "react";
import {
    Button, Dropdown, Form, Icon, Label, Loader, Message, Modal, Table,
} from "semantic-ui-react";

import {
    ZenMessageAlert, ZenButton, ZenFormJsonSchema,
    ZenFormJsonSchemaView, ZenModal, ZenModalLookup,
    FormatDate, ZenMessageToast, InputTextArea,
    ZenLoading, ZenSelectSearch, ZenFileViewer,
    ButtonDelete, ButtonEdit, DiscussComment
} from "../../../../components/Control/index";
import { FormMode, zzControlHelper } from "../../../../components/Control/zzControlHelper";
import { ZenLookup } from "../../../ComponentInfo/Dictionary/ZenLookup";
import { getModal } from "../../../ComponentInfo/Dictionary/ZenGetModal";
import { ApiCRMDeal } from "../../../CRM/Api";
import { ApiArDmKh } from "../../Api";
import _ from "lodash";
import { ApiWMCongViec } from "../../../WM/Api";
import { ApiFileAttachment } from "../../../../Api";
import InputDateIcon from "../../../../components/Control/InputDateIcon";
import { auth } from "../../../../utils";

export const CongViecComponent = ({ data, keyData, valueKey, type, fileCodeName }) => {
    const _isMounted = useRef(true);
    const [err, setErr] = useState()
    const [loading, setLoading] = useState(true)
    const [items, setItems] = useState()
    const [modalInfoCV, setModalInfoCV] = useState(getModal(ZenLookup.WMCongViec));
    const [detailCV, setDetailCV] = useState();

    useEffect(() => {
        const apiGet = getApi(type)
        apiGet(valueKey, res => {
            if (_isMounted) {
                if (res.status === 200) {
                    setItems(res.data.data)
                } else {
                    setErr(zzControlHelper.getResponseError(res))
                }
                setLoading(false)
            }
        })
        return () => {
            _isMounted.current = false
        }
    }, [])

    // ********************** HANDLE
    const handleOpenCloseModal = (formMode, item) => {
        if (!modalInfoCV) {
            ZenMessageAlert.error("Chưa khai báo thông tin modal")
            return
        }
        setModalInfoCV({
            ...modalInfoCV,
            info: {
                ...modalInfoCV.info,
                initItem: { ...modalInfoCV.info.initItem, [keyData]: valueKey },
                checkPermission: (permiss) => auth.checkPermission(permiss),
            },
            id: item ? item.id : "",
            formMode: formMode,
            open: true,
            other: {
                type: type
            }
        })
    }

    const handleAfterSaveModal = (newItem, { mode }) => {
        // change
        ApiWMCongViec.getByCode(newItem.id, res => {
            if (mode === FormMode.ADD) {
                setItems(items.concat(res.data.data))
            } else if (mode === FormMode.EDIT) {
                setItems(items.map(t => t.id === newItem.id ? res.data.data : t))
            }
            // close modal
            setModalInfoCV({
                ...modalInfoCV,
                id: "",
                formMode: FormMode.NON,
                open: false,
            })
        })
    }

    const handleDeleteItem = (item) => {
        ZenMessageAlert.question("Bạn có chắc muốn xóa không?")
            .then(success => {
                if (success == 1) {
                    ApiWMCongViec.delete(item.id, res => {
                        if (res.status >= 200 && res.status <= 204) {
                            setItems(items.filter(t => t.id != item.id))
                            ZenMessageToast.success()
                        } else {
                            ZenMessageAlert.error(res)
                        }
                    })
                }
            })
    }

    const handleCloseCVDetail = (newItem) => {
        if (items.find(item => item.id == newItem.id)) {
            setItems(items.map(item => item.id == newItem.id ? newItem : item)
                .filter(item => item[keyData] === data[keyData]))
        } else {
            setItems(items.concat(newItem))
        }
        setDetailCV()
    }

    // ********************** Chi tiết công việc
    const handleOpenDetailCV = (e, item) => {
        setDetailCV({ open: true, item: item })
    }

    // ********************** FUNCTIONS 
    function renderList() {
        return <Table compact selectable striped
            className="ar-sticky-table"
        >
            <Table.Header>
                <Table.Row>
                    <Table.HeaderCell content="Ngày thực hiện" />
                    <Table.HeaderCell content="Ngày kết thúc" />
                    <Table.HeaderCell content="Tên công việc" />
                    <Table.HeaderCell content="Giai đoạn" />
                    <Table.HeaderCell content="Loại công việc" />
                    <Table.HeaderCell content="Người thực hiện" />
                    <Table.HeaderCell content="" />
                </Table.Row>
            </Table.Header>

            <Table.Body>
                {
                    items?.map((item, index) => {
                        const assaigns_name_split = item.assaigns_name?.split(",")
                        return <Table.Row
                            key={item.id || index}
                            onDoubleClick={() => handleOpenCloseModal(FormMode.VIEW, item)}
                        >
                            <Table.Cell>
                                <FormatDate value={item.ngay_bd} />
                            </Table.Cell>
                            <Table.Cell>
                                <FormatDate value={item.ngay_kt} />
                            </Table.Cell>
                            <Table.Cell>
                                <a style={{ fontWeight: "bold" }}
                                    href={`#`}
                                    onClick={(e) => {
                                        e.preventDefault();
                                        handleOpenDetailCV(e, item);
                                    }}
                                >
                                    {item.ten_cong_viec}
                                </a>
                            </Table.Cell>
                            <Table.Cell>
                                {item.ten_gdoan_cv}
                            </Table.Cell>
                            <Table.Cell>
                                {item.ten_loai_cv}
                            </Table.Cell>
                            <Table.Cell>
                                {
                                    assaigns_name_split?.map(ass => {
                                        return <Label key={ass} content={ass} as="a" />
                                    })
                                }
                            </Table.Cell>
                            <Table.Cell singleLine collapsing>
                                <Button.Group size="small" style={{ float: "right" }}>
                                    <ButtonEdit
                                        onClick={() => handleOpenCloseModal(FormMode.EDIT, item)}
                                    />
                                    <ButtonDelete
                                        onClick={() => handleDeleteItem(item)} />
                                </Button.Group>
                            </Table.Cell>
                        </Table.Row>
                    })
                }
            </Table.Body>
        </Table>
    }


    return <ContextCongViec.Provider>
        <ZenButton btnType="add" size="tiny" floated="right"
            onClick={() => handleOpenCloseModal(FormMode.ADD)}
        />
        <div style={{ clear: "both" }} />

        <ZenLoading loading={loading} inline="centered" />
        {err && <Message list={err} negative header="Error" />}

        {renderList()}

        {detailCV?.open && <ModalCongViecDetail
            open={detailCV.open}
            cvItem={detailCV.item}
            onClose={handleCloseCVDetail}
            modalInfoCV={modalInfoCV}
            keyData={keyData}
            valueKey={valueKey}
            type={type}
            fileCodeName={fileCodeName}
        />}

        {zzControlHelper.openModalComponent(modalInfoCV,
            {
                onClose: () => handleOpenCloseModal(FormMode.NON),
                onAfterSave: handleAfterSaveModal,
            }
        )}
    </ContextCongViec.Provider>
}

export const ModalCongViecDetail = (props) => {
    const { open, cvItem, modalInfoCV, keyData, valueKey, type, onClose, fileCodeName } = props
    const refFormschema = useRef();
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState();
    const [cvs, setCvs] = useState({ id: cvItem.id, ten_cong_viec: "" });
    const [filesAttach, setFilesAttach] = useState();
    const [editContent, setEditContent] = useState({ isEdit: false, content: "" });
    const [listGiaiDoan, setListGiaiDoan] = useState();
    const [openModalCV, setOpenModalCV] = useState({ open: false });
    const [changeNV, setChangeNV] = useState();
    const [formBC, setFormBC] = useState({ loading: false, mode: "VIEW", modeModal: "", currentGDoan: null });

    const [viewFile, setViewFile] = useState(initViewFile);
    const { openView, fileInfo } = viewFile
    const [loadingAddns1, setLoadingAddns1] = useState(false);
    const [loadingAddns2, setLoadingAddns2] = useState(false);

    useEffect(() => {
        loadData();
    }, [])

    const handleChangeCVS = (e, { name, value, option }) => {
        if (cvs[name] === value) return

        let objectCvs = {
            [name]: value
        }
        if (name === "ma_loai_cv") {
            objectCvs.ten_loai_cv = option.ten_loai_cv
            objectCvs.ma_form_bc = option.ma_form_bc 
            objectCvs.phai_bc= option.phai_bc
            objectCvs.schema_form_bc = option.form_schema
        } else if (name === "ma_gdoan_cv") {
            objectCvs.color_gdoan_cv = option.color
            objectCvs.isclose_gdoan_cv = option.isclose?.toString() === "true"
        }
        apiUpdateCV(name, value, objectCvs)
    }

    const handleSelectedFile = async (e) => {
        if (e.target.value) {
            uploadFile(e)
        }
    }

    const handleEditContent = (newEditContent, isSave) => {
        if (isSave) {
            apiUpdateCV("noi_dung", editContent.content)
        }
        setEditContent(newEditContent)
    }

    const handleClickDropdown = (e, { value }) => {
        if (value === "CLONE") {
            cloneCongViec()
        } else {
            setOpenModalCV({
                open: true,
                formMode: FormMode.EDIT,
            })
        }
    }

    const handleNextGiaiDoan = (currentMaGiaiDoan) => {
        const indexCurrentGD = listGiaiDoan.findIndex(t => t.ma_gdoan_cv == currentMaGiaiDoan)
        if (indexCurrentGD > -1 && indexCurrentGD < listGiaiDoan.length - 1) {
            const temp = listGiaiDoan[indexCurrentGD + 1]

            if (temp.isclose?.toString() === "true" &&
                cvs.phai_bc && cvs.schema_form_bc) {
                setFormBC({ loading: false, mode: "EDIT", modeModal: "ADD", currentGDoan: temp })
                return
            }

            apiUpdateCV("ma_gdoan_cv", temp.ma_gdoan_cv, {
                color_gdoan_cv: temp.color,
                isclose_gdoan_cv: temp.isclose?.toString() === "true"
            })
        }
    }

    const handleCloseModal = (formMode, item) => {
        setOpenModalCV({
            open: false,
            formMode: formMode,
        })
    }

    const handleAfterSaveModal = (newItem, { mode }) => {
        // change
        if (mode === FormMode.EDIT) {
            //assignees , followers
            _setCvs({ ...newItem, assignees: newItem.assigneesArr, followers: newItem.followersArr })
        }
        // close modal
        setOpenModalCV({
            formMode: FormMode.NON,
            open: false,
        })
    }

    // ================================ VIEW FILE
    const handleViewFile = (e, fileAtt) => {
        e.preventDefault();
        setViewFile({ openView: true, fileInfo: fileAtt })
    }

    const handleDownloadFile = (e) => {
        e.preventDefault();
        ApiFileAttachment.download(codeName, fileInfo.gid, res => {
            if (res.status >= 200 && res.status <= 204) {
                const url = window.URL.createObjectURL(new Blob([res.data]));

                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download', fileInfo.file_name);
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            } else {
                ZenMessageAlert.error(zzControlHelper.getResponseError(res))
            }
        })
    }

    const handleDeleteFile = (fileAtt) => {
        ZenMessageAlert.question("Bạn có chắc muốn xóa File không?")
            .then(isOK => {
                if (isOK == 1) {
                    ApiFileAttachment.delete(fileCodeName, fileAtt.id, fileAtt.gid,
                        res => {
                            if (res.status >= 200 && res.status <= 204) {
                                setFilesAttach(filesAttach.filter(t => t.id !== fileAtt.id))
                            } else {
                                ZenMessageAlert.error(ZenMessageAlert.error(res))
                            }
                        })
                }
            })
    }

    // ================================= UPDATE THONG TIN CONG VIEC
    const handleAddNS = (dataSelected, { loai }) => {
        const { option, checked } = dataSelected
        //{id_cviec, loai, id_nv}
        const data = {
            id_cviec: cvs.id,
            id_nv: option.value,
            loai: loai
        }

        // là chọn
        if (checked) {
            if (loai == "1") {
                setLoadingAddns1(true)
            } else if (loai == "2") {
                setLoadingAddns2(true)
            }
            ApiWMCongViec.insertNS(data, res => {
                if (res.status >= 200 && res.status <= 204) {
                    const temp = [{
                        id_nv: option.value,
                        ho_ten: option.ho_ten
                    }]
                    if (loai == "1") {
                        _setCvs({ assignees: cvs.assignees?.concat(temp) || temp })
                    } else if (loai == "2") {
                        _setCvs({ followers: cvs.followers?.concat(temp) || temp })
                    }
                } else {
                    ZenMessageAlert.error(zzControlHelper.getResponseError(res))
                }
                setLoadingAddns1(false)
                setLoadingAddns2(false)
            })
        } else {
            handleDeleteNS(data, { loai: loai })
        }
    }

    const handleDeleteNS = (item, { loai }) => {
        // {id_cviec, id_nv, loai}
        const data = {
            id_cviec: cvs.id,
            id_nv: item.id_nv,
            loai: loai
        }
        setChangeNV(item.id_nv)
        ApiWMCongViec.deleteNS(data, res => {
            if (res.status >= 200 && res.status <= 204) {
                if (loai == "1") {
                    _setCvs({ assignees: cvs.assignees?.filter(t => t.id_nv != item.id_nv) })
                } else if (loai == "2") {
                    _setCvs({ followers: cvs.followers?.filter(t => t.id_nv != item.id_nv) })
                }
            } else {
                ZenMessageAlert.error(zzControlHelper.getResponseError(res))
            }
            setChangeNV()
        })
    }

    const handleUpdateCV = (e, date, otherObject) => {
        const { name, value } = date
        if (cvs[name] != value) {
            apiUpdateCV(name, value, otherObject)
        }
    }

    // ================================= SAVE FORM BC
    const handleSaveFormBC = (itemBC) => {
        setFormBC({ ...formBC, loading: true })
        apiUpdateCV("bao_cao", JSON.stringify(itemBC), {},
            ({ sucessed, error }) => {
                if (sucessed) {
                    ZenMessageToast.success();
                    if (formBC.modeModal === "ADD") {
                        // update giao đoạn sau khi lưu form BC
                        let objectCvs = {
                            ma_gdoan_cv: formBC.currentGDoan.value,
                            color_gdoan_cv: formBC.currentGDoan.color,
                            isclose_gdoan_cv: formBC.currentGDoan.isclose?.toString() === "true"
                        }
                        apiUpdateCV("ma_gdoan_cv", objectCvs.ma_gdoan_cv, objectCvs)
                    }
                } else {
                    ZenMessageAlert.error(zzControlHelper.getResponseError(error))
                }
                setFormBC({ loading: false, mode: "VIEW", modeModal: "" })
            })
    }

    const handleCloseFormBCModal = (e) => {
        setFormBC({ loading: false, mode: "VIEW", modeModal: "" })
    }

    const handleChangeBeforeGiaiDoan = (e, { name, value, option }) => {
        // nếu giai đoạn chuyển sang isclose và chưa có báo cáo
        if (option.isclose?.toString() === "true" &&
            cvs.phai_bc && cvs.schema_form_bc) {
            setFormBC({ loading: false, mode: "EDIT", modeModal: "ADD", currentGDoan: option })
            return {
                success: false
            }
        }
        return {
            success: true
        }
    }

    // ================================= FUNCTIONS
    function loadData(idCV) {
        const dataCvs = new Promise((resolve, reject) => {
            ApiWMCongViec.getByCode(idCV || cvItem.id, res => {
                if (res.status === 200) {
                    resolve(res.data.data)
                } else {
                    reject(zzControlHelper.getResponseError(res))
                }
            })
        })

        const dataFileAttach = new Promise((resolve) => {
            ApiFileAttachment.get(fileCodeName, `CONGVIEC_${idCV || cvItem.id}`, res => {
                if (res.status === 200) {
                    resolve(res.data.data)
                } else {
                    resolve(zzControlHelper.getResponseError(res))
                }
            })
        })

        Promise.all([dataCvs, dataFileAttach])
            .then(values => {
                setCvs(values[0])
                setFilesAttach(values[1])
            })
            .catch(err => {
                setError(err)
            })
            .finally(res => {
                setLoading(false)
            })
    }

    function uploadFile(e) {
        const formData = new FormData();
        formData.append("doc_key", `CONGVIEC_${cvs.id}`)
        formData.append("ma_loai", "zzz")
        formData.append("ghi_chu", "")
        console.log(e.target.files.length)

        if (e.target.files && e.target.files.length > 0) {
            for (let file of e.target.files) {
               formData.append("file_upload", file);
            }
         }

        ApiFileAttachment.uploadList(fileCodeName, formData, res => {
            if (res.status >= 200 && res.status <= 204) {
                const temp = filesAttach || []
                setFilesAttach(temp.concat(res.data.data))
            } else {
                ZenMessageAlert.error(zzControlHelper.getResponseError(res))
            }
        })
        e.target.value = ""
    }

    function apiUpdateCV(key, value, objectCvs, callback, otherPatchData = []) {
        let patchData = [
            {
                value: value,
                path: `/${key}`,
                op: "replace",
                operationType: 0,
            }
        ]
        patchData = patchData.concat(otherPatchData)
        ApiWMCongViec.updatePatch(cvs.id, patchData,
            res => {
                if (res.status === 200) {
                    _setCvs({ [key]: value, ...objectCvs })
                    callback && callback({ sucessed: true })
                } else {
                    callback && callback({ sucessed: false, error: res })
                }
            })
    }

    function cloneCongViec() {
        let assignees = cvs.assignees
        if (assignees && Array.isArray(assignees)) {
            assignees = assignees.map(t => t.id_nv).toString();
        }

        let followers = cvs.followers
        if (followers && Array.isArray(followers)) {
            followers = followers.map(t => t.id_nv).toString();
        }

        ApiWMCongViec.insert({ ...cvs, id: null, assignees: assignees, followers: followers },
            res => {
                if (res.status >= 200 && res.status <= 204) {
                    loadData(res.data.data.id)
                    ZenMessageToast.success();
                } else {
                    ZenMessageAlert.error(zzControlHelper.getResponseError(res));
                }
            })
    }

    function _setCvs(newCvs) {
        const assaigns_name = newCvs?.assignees?.map(t => t.ho_ten)?.toString()
        const newItem = { ...cvs, ...newCvs }
        if (assaigns_name) {
            newItem.assaigns_name = assaigns_name
        }
        setCvs({ ...newItem })
    }

    return <>
        <Modal closeOnEscape // closeIcon
            closeOnDimmerClick={false}
            size="fullscreen"
            open={open}
            onClose={() => onClose(cvs)}
        >
            <Modal.Content style={{ minHeight: "70vh", marginTop: "7px" }}>
                <ZenLoading loading={loading} />
                {error && <Message negative list={error} />}

                <div className="ar-cv-grid-container">
                    <div className="item1" style={{ display: "flex" }}>
                        <div style={{ width: "33px" }}>
                            <Icon name="list alternate outline" size="big" />
                        </div>

                        <div style={{ width: "100%" }}>
                            <InputTextArea
                                rows="1" autoResize isBlur
                                className="ar-cv-hover-input"
                                value={cvs.ten_cong_viec} name="ten_cong_viec"
                                onBlur={(e) => {
                                    if (e.target.value !== cvs.ten_cong_viec) {
                                        apiUpdateCV("ten_cong_viec", e.target.value)
                                    }
                                }}
                            />
                        </div>
                    </div>

                    <div className="item2">
                        <GiaiDoanInfo cvs={cvs}
                            getOptionsGiaiDoan={(options) => setListGiaiDoan(options)}
                            onChangeCVS={handleChangeCVS}
                            onClickDropdown={handleClickDropdown}
                            onNextGiaiDoan={handleNextGiaiDoan}
                            onChangeBeforeGiaiDoan={handleChangeBeforeGiaiDoan}
                            onClose={() => onClose(cvs)}
                        />
                    </div>

                    <div className="item3">
                        <CongViecInfo cvs={cvs}
                            onUpdateCV={handleUpdateCV}
                            onAddNS={handleAddNS}
                            onDeleteNS={handleDeleteNS}
                            changeNV={changeNV}
                            loadingAddns1={loadingAddns1}
                            loadingAddns2={loadingAddns2}
                        />
                    </div>

                    <div className="item4">
                        <NoiDungAndFile cvs={cvs}
                            editContent={editContent}
                            filesAttach={filesAttach}
                            onEditContent={handleEditContent}
                            onViewFile={handleViewFile}
                            onSelectedFile={handleSelectedFile}
                            onDeleteFile={handleDeleteFile}
                        />

                        <FormSchema cvs={cvs}
                            formBC={formBC}
                            setFormBC={setFormBC}
                            refFormschema={refFormschema}
                            onSaveFormBC={handleSaveFormBC}
                            onCloseFormBCModal={handleCloseFormBCModal}
                        />
                    </div>

                    {/* <div className="item5">
                        <FormSchema cvs={cvs}
                            formBC={formBC}
                            setFormBC={setFormBC}
                            refFormschema={refFormschema}
                            onSaveFormBC={handleSaveFormBC}
                        />
                    </div> */}

                    <div className="item6">
                        <div>
                            <Icon name="comments outline" size="big" />
                            <span style={{ fontSize: "1.2em", fontWeight: "bold" }}>Thảo luận</span>
                        </div>

                        <DiscussComment codeName={fileCodeName}
                            type={"CONG_VIEC"}
                            keyValue={cvs.id}
                            keyIsId={true}
                        />
                    </div>
                </div>

            </Modal.Content>

            <ZenFileViewer
                fileInfo={fileInfo}
                open={openView}
                onClose={() => setViewFile(initViewFile)}
                onDownload={handleDownloadFile}
            />
        </Modal>

        {zzControlHelper.openModalComponent({
            ...modalInfoCV,
            info: {
                ...modalInfoCV.info,
                initItem: { ...modalInfoCV.info.initItem, [keyData]: valueKey },
            },
            id: cvs ? cvs.id : "",
            other: {
                type: type
            },
            ...openModalCV
        },
            {
                onClose: () => handleCloseModal(FormMode.NON),
                onAfterSave: handleAfterSaveModal,
            }
        )}
    </>
}

const GiaiDoanInfo = ({ cvs, getOptionsGiaiDoan, onNextGiaiDoan, onChangeCVS, onChangeBeforeGiaiDoan, onClickDropdown, onClose }) => {
    return <div style={{ float: "right" }}>
        <ZenSelectSearch
            lookup={ZenLookup.WMDmUuTienCV}
            name="ma_uu_tien"
            value={cvs.ma_uu_tien}
            onChange={onChangeCVS}
            style={{ marginRight: "14px" }}
            isHover
        />

        <ZenSelectSearch
            lookup={ZenLookup.WMDmLoaiCV}
            name="ma_loai_cv"
            value={cvs.ma_loai_cv}
            onChange={onChangeCVS}
            style={{ marginRight: "14px" }}
            isHover
        />

        <Button.Group size="tiny"
            style={{ marginRight: "14px" }}
        >
            <ZenSelectSearch button floating
                lookup={ZenLookup.WMDmGiaiDoanCV}
                name="ma_gdoan_cv"
                value={cvs.ma_gdoan_cv}
                icon={{ style: { display: "none" } }}
                onChange={onChangeCVS}
                onChangeBefore={onChangeBeforeGiaiDoan}
                className="ar-cv-btn-hover"
                style={{
                    borderLeft: "1px solid rgba(34, 36, 38, 0.15)",
                    backgroundColor: cvs.color_gdoan_cv,
                    color: cvs.color_gdoan_cv ? "white" : undefined
                }}
                getOptionsCurrent={getOptionsGiaiDoan}
            />
            <Button icon="caret right" size="tiny"
                className="ar-cv-btn-hover"
                style={{
                    backgroundColor: cvs.color_gdoan_cv,
                    color: cvs.color_gdoan_cv ? "white" : undefined
                }}
                onClick={() => onNextGiaiDoan(cvs.ma_gdoan_cv)}
            />
        </Button.Group>

        <Dropdown floating button className='icon tiny basic' icon="ellipsis horizontal"
            style={{ margin: 0 }}
        >
            <Dropdown.Menu direction="left">
                {/* <Dropdown.Item value="OBJECT" onClick={onClickDropdown}>Đối tượng</Dropdown.Item> */}
                <Dropdown.Item value="CLONE" onClick={onClickDropdown}>Sao chép công việc</Dropdown.Item>
            </Dropdown.Menu>
        </Dropdown>

        <Icon name="cancel" link
            size="cancel"
            onClick={onClose}
            style={{
                position: "absolute",
                right: 5,
                top: 5
            }} />
    </div>
}

const CongViecInfo = ({ cvs = {}, changeNV, onUpdateCV, onAddNS, onDeleteNS, loadingAddns1, loadingAddns2 }) => {
    const onChangeInfo = (item, name) => {
        if (name === "ma_kh")
            onUpdateCV({}, { name: "ma_kh", value: item.ma_kh }, { ten_kh: item.ten_kh })
        else if (name === "ma_du_an") {
            onUpdateCV({}, { name: "ma_du_an", value: item.ma_du_an }, { ten_du_an: item.ten_du_an })
        } else if (name === "ma_deal") {
            onUpdateCV({}, { name: "ma_deal", value: item.ma_deal }, { ten_deal: item.ten_deal })
        }
    }

    return <div style={{ width: "100%", paddingLeft: "33px" }}>
        <div className="ar-cv-span14">
            <span style={{ paddingLeft: "0" }}>
                <strong>Khách hàng: </strong>
                {cvs.ten_kh} {" "}
                <ZenModalLookup
                    trigger={<a><Icon link name="edit" /></a>}
                    lookup={ZenLookup.Ma_kh}
                    value={cvs.ma_kh}
                    onItemLookup={(item) => onChangeInfo(item, "ma_kh")}
                />
            </span>
            <span>
                <strong>Dự án: </strong>
                {cvs.ten_du_an}{" "}
                <ZenModalLookup
                    trigger={<a><Icon link name="edit" /></a>}
                    lookup={ZenLookup.PMDuAn}
                    value={cvs.ma_du_an}
                    onItemLookup={(item) => onChangeInfo(item, "ma_du_an")}
                />
            </span>
            <span>
                <strong>Hạng mục: </strong>
                {cvs.ten_hang_muc}
            </span>
            <span>
                <strong>Cơ hội: </strong>
                {cvs.ten_deal}{" "}
                <ZenModalLookup
                    trigger={<a><Icon link name="edit" /></a>}
                    lookup={ZenLookup.CRMDeal}
                    value={cvs.ma_deal}
                    onItemLookup={(item) => onChangeInfo(item, "ma_deal")}
                />
            </span>
        </div>

        <div className="ar-cv-span14" style={{ padding: "7px 0" }}>
            <strong>Tạo bởi: </strong> {cvs.cuser_name}
            <span style={{ paddingLeft: "28px" }}>
                Lúc: <Icon name="clock outline" /><FormatDate value={cvs.cdate} showTime />
            </span>

            <span>
                <strong>Thời gian: </strong>

                <span>
                    <InputDateIcon header="Bắt đầu"
                        name="ngay_bd" showTime
                        value={cvs.ngay_bd}
                        onClose={onUpdateCV}
                    />
                </span>

                <span style={{ paddingLeft: "28px" }}>
                    <InputDateIcon header="Kết thúc"
                        name="ngay_kt" showTime
                        value={cvs.ngay_kt}
                        onClose={onUpdateCV}
                    />
                </span>
            </span>

        </div>

        <div style={{ display: "flex" }}>
            <div>
                <span style={{ fontWeight: "bold" }}>Thực hiện: </span>
                {cvs?.assignees && Array.isArray(cvs.assignees) && cvs.assignees.map(item => {
                    return <Label key={item.id_nv} size="small" as='a' color="teal">
                        {item.ho_ten}
                        {changeNV == item.id_nv ? <Icon name="spinner" loading />
                            : <Icon name="delete" onClick={(e,) => onDeleteNS(item, { loai: "1" })} />}
                    </Label>
                })}
                {loadingAddns1 && <Icon name="spinner" loading />}
                <ZenSelectSearch
                    lookup={ZenLookup.ID_NV}
                    name="id_nv"
                    icon={{ name: "plus circle", size: "large", color: "teal" }}
                    multiple
                    value={cvs.assignees?.map(t => t.id_nv) || undefined}
                    inputSearch hiddenText required
                    onChange={(e, dataInput) => onAddNS(dataInput, { loai: "1" })}
                />
            </div>

            <div style={{ marginLeft: "28px" }}>
                <span style={{ fontWeight: "bold" }}>Theo dõi: </span>
                {cvs?.followers && Array.isArray(cvs.followers) && cvs.followers.map(item => {
                    return <Label key={item.id_nv} size="small" as='a' color="teal">
                        {item.ho_ten}

                        {changeNV == item.id_nv ? <Icon name="spinner" loading />
                            : <Icon name="delete" onClick={(e,) => onDeleteNS(item, { loai: "2" })} />}
                    </Label>
                })}
                {loadingAddns2 && <Icon name="spinner" loading />}
                <ZenSelectSearch floating
                    lookup={ZenLookup.ID_NV}
                    name="id_nv"
                    icon={{ name: "plus circle", size: "large", color: "teal" }}
                    multiple direction="left"
                    value={cvs.followers?.map(t => t.id_nv) || undefined}
                    inputSearch hiddenText required
                    onChange={(e, dataInput) => onAddNS(dataInput, { loai: "2" })}
                />
            </div>
        </div>
    </div>
}

const NoiDungAndFile = ({ cvs, editContent, onEditContent, filesAttach, onViewFile, onSelectedFile, onDeleteFile }) => {
    const refFileAttach = useRef();

    return <>
        <div>
            <Icon name="content" size="big" />
            <span style={{ fontSize: "1.2em", fontWeight: "bold" }}>Nội dung công việc</span>

            {editContent.isEdit ? <>
                <span style={{ paddingLeft: "14px" }}>
                    <Icon name="save" link onClick={() => onEditContent({ isEdit: false, content: "" }, true)} />
                    {" | "}
                    <Icon name="cancel" link onClick={() => onEditContent({ isEdit: false, content: "" })} />
                </span>
            </>
                : <span style={{ paddingLeft: "14px" }}>
                    <Icon name="edit" link onClick={() => onEditContent({ isEdit: true, content: cvs.noi_dung })} />
                </span>}
        </div>

        <div style={{ paddingLeft: "33px" }}>
            {editContent.isEdit ?
                <Form>
                    <InputTextArea
                        autoResize isBlur
                        value={editContent.content}
                        onBlur={(e, { value }) => onEditContent({ isEdit: true, content: value })}
                    />
                </Form>
                : <div style={{ whiteSpace: "pre-line" }}>{cvs.noi_dung}</div>}
        </div>

        <div style={{ paddingTop: "14px" }}>
            <Icon name="attach" size="big" />
            <span style={{ fontSize: "1.2em", fontWeight: "bold" }}>File kèm theo</span>
        </div>

        <div style={{ paddingLeft: "33px" }}>
            {
                filesAttach?.map(item => {
                    return <div key={item.id}>
                        <a href="#" onClick={(e) => onViewFile(e, item)}>{item.file_name}</a>
                        <Icon link name="cancel" color="red" onClick={() => onDeleteFile(item)} />
                    </div>
                })
            }

            <a href="#" onClick={(e) => {
                e.preventDefault();
                refFileAttach.current.click(e)
            }}>
                <Icon name="plus" />
                Thêm File
            </a>
        </div>

        <input hidden type="file" multiple
            ref={refFileAttach}
            onChange={onSelectedFile} />

    </>
}

const FormSchema = ({ cvs, formBC, setFormBC, refFormschema, onSaveFormBC, onCloseFormBCModal }) => {
    const schemaForm = cvs.schema_form_bc ? JSON.parse(cvs.schema_form_bc) : ""
    const schemaData = cvs.bao_cao ? JSON.parse(cvs.bao_cao) : {}

    return <>
        {schemaForm && cvs.phai_bc && cvs.isclose_gdoan_cv &&
            <>
                <br />
                <ZenFormJsonSchemaView
                    titleForm={<>
                        <Icon name="file alternate outline" size="big" />
                        <span style={{ fontSize: "1.2em", fontWeight: "bold" }}>
                            {schemaForm.title}
                        </span>
                    </>}
                    formSchema={schemaForm}
                    formData={cvs.bao_cao ? schemaData : {}}
                />

                <ZenButton btnType="edit"
                    primary
                    size="small"
                    onClick={(e) => setFormBC({ loading: false, mode: "EDIT", modeModal: "EDIT" })}
                />
            </>}

        {formBC.mode === "EDIT" &&
            <ZenModal
                open size="small"
                onClose={onCloseFormBCModal}

                content={<ZenFormJsonSchema
                    readOnly={formBC.mode === "VIEW"}
                    formSchema={schemaForm}
                    formData={cvs.bao_cao ? schemaData : {}}
                    ref={refFormschema}
                    onSaveForm={onSaveFormBC}
                />}

                actions={<ZenButton btnType="save"
                    size="small"
                    loading={formBC?.loading}

                    onClick={(e) => {
                        const { refZenFormik } = refFormschema.current
                        const { handleSubmit, setTouched, isValid, errors } = refZenFormik.current
                        handleSubmit(e);

                        if (isValid === false) {
                            Object.keys(errors).map(field => {
                                setTouched({ [field]: true }, true)
                            })
                            refFormschema.current.forceUpdate()
                            ZenMessageAlert.error("Vui lòng kiểm tra lại form")
                        }
                    }}
                />}

            ></ZenModal>}
    </>
}

function getApi(type = "") {
    return type.toUpperCase() === "MA_KH" ? ApiArDmKh.getCongViecByKh : ApiCRMDeal.getCongViecByDeal
}
const ContextCongViec = React.createContext();
const initViewFile = { fileInfo: "", openView: false }

// data: item lien kết , vd ardmkh
// keyData: tên trường liên kết, VD ở đây là ma_kh
// valueKey: giá trị của trường liên kết
// type: biến nhận biết công việc thuộc đối tượng nào, ở đây là MA_KH