import React, { useMemo } from "react";
import { ZenField, ZenFieldTextArea, ZenFieldSelectApi } from "../../../components/Control/index";
import { FormMode, ZenHelper } from "../../../utils";
import { ApiArDmKh } from "../Api";
import { Form } from "semantic-ui-react";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { ValidError } from "../../../utils/language/variable";
import { IntlFormat } from "../../../utils/intlFormat";
import * as routes from "../../../constants/routes"
import { zzControlHelper } from "../../../components/Control/zzControlHelper";
import * as permissions from "../../../constants/permissions";

const ArDmKhModal = (props) => {
   const { formik, permission, mode } = props

   const memoSiDmLoai = useMemo(() => {
      const listSiDmLoai = ZenHelper.getDataLocal(ZenLookup.SIDmloai.code)
      const data = listSiDmLoai?.data?.filter(t => t.ma_nhom === "PLKH")
      var result = data?.reduce(
         (obj, item) => Object.assign(obj, { [item.ma]: item.ten }), {}
      );
      return result || {}
   }, [])

   return <React.Fragment>
      <Form.Group widths="equal">
         <ZenField autoFocus readOnly={!permission || (mode === FormMode.ADD ? false : true)}
            label={"ArDmKh.ma_kh"} defaultlabel="Mã khách hàng"
            name="ma_kh" props={formik} textCase="uppercase"
            maxLength={20}
            required isCode
         />
         <ZenField readOnly={!permission}
            label={"ArDmKh.ma_so_thue"} defaultlabel="Mã số thuế"
            name="ma_so_thue" props={formik}
         />
      </Form.Group>

      <ZenField readOnly={!permission} required
         label={"ArDmKh.ten_kh"} defaultlabel="Tên khách hàng"
         name="ten_kh" props={formik}
      />

      <ZenField readOnly={!permission}
         label={"ArDmKh.dia_chi"} defaultlabel="Địa chỉ"
         name="dia_chi" props={formik}
      />

      <Form.Group widths="equal">
         <ZenField readOnly={!permission}
            label={"ArDmKh.email"} defaultlabel="Email"
            name="email" props={formik}
         />

         <ZenField readOnly={!permission}
            label={"ArDmKh.tel"} defaultlabel="Điện thoại"
            name="tel" props={formik}
         />
      </Form.Group>

      <ZenField readOnly={!permission}
         label={"ArDmKh.home_page"} defaultlabel="Website"
         name="home_page" props={formik}
      />

      <Form.Group widths="equal">
         <ZenFieldSelectApi upward
            readOnly={!permission}
            lookup={ZenLookup.Ma_linh_vuc}
            label={"ArDmKh.linh_vuc"} defaultlabel="Lĩnh vực/Loại hình"
            name="ma_linh_vuc" formik={formik}
         />

         <ZenFieldSelectApi upward
            readOnly={!permission}
            lookup={{
               ...ZenLookup.Ma_plkh,
               onLocalWhere: (items) => {
                  return items?.filter(t => t.loai == "1") || []
               },
               where: 'loai = 1'
            }}
            isIntl={false} label={memoSiDmLoai["1"]}
            name="ma_plkh1" formik={formik}
         />
      </Form.Group>

      <Form.Group widths='equal'>
         <ZenFieldSelectApi upward
            readOnly={!permission}
            lookup={{
               ...ZenLookup.Ma_plkh,
               onLocalWhere: (items) => {
                  return items?.filter(t => t.loai == "2") || []
               },
               where: 'loai = 2'
            }}
            isIntl={false} label={memoSiDmLoai["2"]}
            name="ma_plkh2" formik={formik}
         />
         <ZenFieldSelectApi upward
            readOnly={!permission}
            lookup={{
               ...ZenLookup.Ma_plkh,
               onLocalWhere: (items) => {
                  return items?.filter(t => t.loai == "3") || []
               },
               where: 'loai = 3'
            }}
            isIntl={false} label={memoSiDmLoai["3"]}
            name="ma_plkh3" formik={formik}
         />
      </Form.Group>

      <ZenFieldTextArea readOnly={!permission}
         label={"ArDmKh.ghi_chu"} defaultlabel="Ghi chú"
         name="ghi_chu" props={formik}
      />
   </React.Fragment>
}

const ArDmKh = {
   FormModal: ArDmKhModal,
   api: {
      url: ApiArDmKh,
   },
   permission: {
      view: permissions.ARDmKhXem,
      add: permissions.ARDmKhThem,
      edit: permissions.ARDmKhSua,
   },
   saveAndRedirect: {
      linkto : (item) => {
         const id = zzControlHelper.btoaUTF8(item['ma_kh'])
         window.location.assign(`#${routes.ARDmKhDetail(id)}`)
      }
   },
   formId: "ArDmKh-form",
   size: "small",
   isScrolling: false,
   initItem: {
      ma_kh: "",
      ten_kh: "",
      ma_quoc_gia: "VN",
      iskh: true,
      ksd: false
   },
   // isScrolling: false, scroll phần content: default True

   formValidation: [
      {
         ...ValidError.formik({ name: "ma_kh", type: "string" },
            { type: "required", intl: IntlFormat.default(ValidError.Required) },
            { type: "max", params: [20], intl: { ...IntlFormat.default(ValidError.MaxLength) } }
         )
      },
      {
         ...ValidError.formik({ name: "ten_kh", type: "string" },
            { type: "required", intl: IntlFormat.default(ValidError.Required) }
         )
      },
   ]
}

export { ArDmKh };