import React, { useEffect } from "react";
import { ZenField, ZenFieldSelectApi,ZenFieldNumber } from "../../../components/Control/index";
import { FormMode } from "../../../utils";
import { Form } from "semantic-ui-react";
import { ApiArDmPlkh } from "../Api";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import * as permissions from "../../../constants/permissions";

const ARDmPlkhModal = (props) => {
   const { formik, permission, mode } = props

   useEffect(() => {
      if (mode === FormMode.ADD) {
         formik.setFieldValue("loai", "1")
      }
   }, [])

   const handleselectloai = (e,i) => {
      formik.setFieldValue("loai", i.value)
      formik.setFieldValue("ten_loai", i.options.filter( a => a.value == i.value)[0].ten)
   }

   return <React.Fragment>
      <Form.Group widths="equal">
      <ZenField required autoFocus readOnly={!permission || (mode === FormMode.ADD ? false : true)}
         label={"ardmplkh.ma_plkh"} defaultlabel="Mã phân loại khách hàng"
         name="ma_plkh" props={formik} textCase="uppercase"
         maxLength={8} isCode
      />
      <ZenFieldNumber
            readOnly={!permission}
            label={"PMDmGiaiDoanDa.ordinal"} defaultlabel="Thứ tự hiển thị"
            name="ordinal" props={formik}
         />
      </Form.Group>
      <ZenField required readOnly={!permission}
         label={"ardmplkh.ten_plkh"} defaultlabel="Tên phân loại khách hàng"
         name="ten_plkh" props={formik}
      />
      <ZenFieldSelectApi width={"8"}
         required autoFocus readOnly={!permission}
         lookup={{...ZenLookup.SIDmloai,where:"MA_NHOM = 'PLKH'"}}
         label={"ardmplkh.loai"} defaultlabel="Loại"
         name="loai" formik={formik} 
         onChange = { (e,i) => handleselectloai(e,i)}
      />
   </React.Fragment>
}

const ARDmPlkh = {
   FormModal: ARDmPlkhModal,
   api: {
      url: ApiArDmPlkh,
   },
   permission: {
      view: permissions.ARDmPlkhXem,
      add: permissions.ARDmKhThem,
      edit: permissions.ARDmKhSua,
   },
   formId: "ardmplkh-form",
   size: "tiny",
   isScrolling: false,
   initItem: {
      ma_plkh: "",
      ten_plkh: "",
      loai: "",
      ordinal:0,
      ksd: false
   },
   formValidation: [
      {
         id: "ma_plkh",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
            {
               type: "max",
               params: [8, "Không được vượt quá 8 kí tự"]
            },
         ]
      },
      {
         id: "ten_plkh",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
         ]
      },
   ]
}

export { ARDmPlkh };