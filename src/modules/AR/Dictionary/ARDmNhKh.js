import React from "react";
import { Form } from "semantic-ui-react";
import { ZenField ,ZenFieldNumber} from "../../../components/Control/index";
import { FormMode } from "../../../utils";
import { ApiArDmNhkh } from "../Api";
const ARDmNhKhModal = (props) => {
   const { formik, permission, mode } = props

   return <React.Fragment>
      <Form.Group widths="equal">
      <ZenField required autoFocus readOnly={!permission || (mode === FormMode.ADD ? false : true)}
         label={"ardmnhkh.ma_nhkh"} defaultlabel="Mã nhóm khách hàng"
         name="ma_nhkh" props={formik} textCase="uppercase"
         maxLength={8} isCode
      />
      <ZenFieldNumber
            readOnly={!permission}
            label={"PMDmGiaiDoanDa.ordinal"} defaultlabel="Thứ tự hiển thị"
            name="ordinal" props={formik}
         />
      </Form.Group>
      <ZenField required readOnly={!permission}
         label={"ardmnhkh.ten_nhkh"} defaultlabel="Tên nhóm khách hàng"
         name="ten_nhkh" props={formik}
      />
   </React.Fragment>
}

const ARDmNhKh = {
   FormModal: ARDmNhKhModal,
   api: {
      url: ApiArDmNhkh,
   },
   permission: {
      view: "06.24.1",
      add: "06.24.2",
      edit: "06.24.3"
   },
   formId: "ardmnhkh-form",
   size: "tiny",
   initItem: {
      ma_nhkh: "",
      ten_nhkh: "",
      ordinal:0,
      ksd: false
   },
   formValidation: [
      {
         id: "ma_nhkh",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
            {
               type: "max",
               params: [8, "Không được vượt quá 8 kí tự"]
            },
         ]
      },
      {
         id: "ten_nhkh",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
         ]
      },
   ]
}

export { ARDmNhKh };