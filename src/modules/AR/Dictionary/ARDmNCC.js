import React from "react";
import { ZenField, ZenFieldNumber, ZenFieldTextArea, ZenFieldSelectApi, ZenFieldCheckbox } from "../../../components/Control/index";
import { FormMode } from "../../../utils";
import { ApiArDmKh } from "../Api";
import { Form, Tab } from "semantic-ui-react";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { ValidError } from "../../../utils/language/variable";
import { IntlFormat } from "../../../utils/intlFormat";

const ARDmNCCModal = (props) => {
   const { formik, permission, mode } = props

   return <React.Fragment>
      <Form.Group>
         <ZenField width='4' autoFocus readOnly={!permission || (mode === FormMode.ADD ? false : true)}
            label={"ArDmKh.ma_ncc"} defaultlabel="Mã NCC"
            name="ma_kh" props={formik} textCase="uppercase"
            maxLength={20} isCode
         />
         <ZenField width='12' readOnly={!permission}
            label={"ArDmKh.ten_ncc"} defaultlabel="Tên nhà cung cấp"
            name="ten_kh" props={formik}
         />
      </Form.Group>
      <Form.Group>
         <ZenField width='12' readOnly={!permission}
            label={"ArDmKh.dia_chi"} defaultlabel="Địa chỉ"
            name="dia_chi" props={formik}
         />
         <ZenField width='4' readOnly={!permission}
            label={"ArDmKh.ma_so_thue"} defaultlabel="Mã số thuế"
            name="ma_so_thue" props={formik}
         />
      </Form.Group>
      <Form.Group style={{ alignItems: "center" }}>
         <ZenFieldSelectApi width='4'
            readOnly={!permission}
            lookup={{ ...ZenLookup.Ma_nhkh }}
            label={"ArDmKh.ma_nhkh"} defaultlabel="Mã nhóm"
            name="ma_nhkh" formik={formik}
         />
         <ZenFieldCheckbox width='3' readOnly={!permission} style={{ marginTop: "16px" }}
            label={"ardmkh.kh"} defaultlabel="Khách hàng" tabIndex={-1}
            name="iskh" props={formik} />
         <ZenFieldCheckbox width='3' readOnly={!permission} style={{ marginTop: "16px" }}
            label={"ardmkh.ncc"} defaultlabel="Nhà cung cấp" tabIndex={-1}
            name="isncc" props={formik} />
         <ZenFieldCheckbox width='3' readOnly={!permission} style={{ paddingTop: "16px" }}
            label={"ardmkh.nv"} defaultlabel="Nhân viên" tabIndex={-1}
            name="isnv" props={formik} />
         <ZenFieldCheckbox width='3' readOnly={!permission} style={{ marginTop: "16px" }}
            label={"ardmkh.nvkd"} defaultlabel="Nhân viên KD" tabIndex={-1}
            name="isnvkd" props={formik} />
      </Form.Group>
      <Tab panes={panes(formik, permission)} />
   </React.Fragment>
}

const panes = (formik, permission) => [
   {
      menuItem: { key: 'Info', icon: 'info', content: 'Thông tin chung' },
      render: () => <Tab.Pane>{info(formik, permission)}</Tab.Pane>,
   },
]
const info = (formik, permission) => <>
   <Form.Group widths='equal'>
      <ZenField readOnly={!permission}
         label={"ArDmKh.nguoi_gd"} defaultlabel="Người giao dịch"
         name="nguoi_gd" props={formik}
      />
      <ZenField readOnly={!permission}
         label={"ArDmKh.home_page"} defaultlabel="Trang chủ"
         name="home_page" props={formik}
      />
   </Form.Group>
   <Form.Group widths='equal'>
      <ZenField readOnly={!permission}
         label={"ArDmKh.tel"} defaultlabel="Điện thoại"
         name="tel" props={formik}
      />
      <ZenField readOnly={!permission}
         label={"ArDmKh.fax"} defaultlabel="Fax"
         name="fax" props={formik}
      />
      <ZenField readOnly={!permission}
         label={"ArDmKh.email"} defaultlabel="Email"
         name="email" props={formik}
      />
   </Form.Group>

   <Form.Group>
      <ZenFieldSelectApi width="8"
         readOnly={!permission}
         lookup={{
            // ...ZenLookup.Ma_httt_po,
            ...ZenLookup.Ma_httt,
            onLocalWhere: (items) => {
               return items.filter(t => t.moduleid == "PO")
            }
         }}
         label={"ArDmKh.ma_httt_po"} defaultlabel="Mã HTTT"
         name="ma_httt_po" formik={formik}
      />
   </Form.Group>

   <Form.Group widths='equal'>
      <ZenFieldSelectApi
         readOnly={!permission}
         lookup={{
            ...ZenLookup.Ma_plkh,
            onLocalWhere: (items) => {
               return items?.filter(t => t.loai == "1") || []
            },
            where: 'loai = 1'
         }}
         label={"ArDmKh.ma_plkh1"} defaultlabel="Phân loại 1"
         name="ma_plkh1" formik={formik} upward
      />
      <ZenFieldSelectApi
         readOnly={!permission}
         lookup={{
            ...ZenLookup.Ma_plkh,
            onLocalWhere: (items) => {
               return items?.filter(t => t.loai == "2") || []
            },
            where: 'loai = 2'
         }}
         label={"ArDmKh.ma_plkh2"} defaultlabel="Phân loại 2"
         name="ma_plkh2" formik={formik} upward
      />
      <ZenFieldSelectApi
         readOnly={!permission}
         lookup={{
            ...ZenLookup.Ma_plkh,
            onLocalWhere: (items) => {
               return items?.filter(t => t.loai == "3") || []
            },
            where: 'loai = 3'
         }}
         label={"ArDmKh.ma_plkh3"} defaultlabel="Phân loại 3"
         name="ma_plkh3" formik={formik} upward
      />
   </Form.Group>
   <ZenFieldTextArea readOnly={!permission}
      label={"ArDmKh.ghi_chu"} defaultlabel="ghi chú"
      name="ghi_chu" props={formik}
   />
</>

const ARDmNCC = {
   FormModal: ARDmNCCModal,
   api: {
      url: ApiArDmKh,
   },
   permission: {
      view: "08.21.1",
      add: "08.21.2",
      edit: "08.21.3"
   },
   formId: "ArDmKh-form",
   size: "large",
   initItem: {
      ma_kh: "",
      ten_kh: "",
      isncc: true,
      ksd: false
   },
   formValidation: [
      {
         ...ValidError.formik({ name: "ma_kh", type: "string" },
            { type: "required", intl: IntlFormat.default(ValidError.Required) },
            { type: "max", params: [20], intl: { ...IntlFormat.default(ValidError.MaxLength) } }
         )
      },
      {
         ...ValidError.formik({ name: "ten_kh", type: "string" },
            { type: "required", intl: IntlFormat.default(ValidError.Required) }
         )
      },
   ]
}

export { ARDmNCC };