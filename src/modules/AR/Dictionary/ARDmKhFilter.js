import React, { useEffect, useMemo, useRef, useState } from "react";
import { Accordion, Button, Form, Icon } from "semantic-ui-react";
import { ZenFormik, ZenFieldSelect, ZenFieldSelectApi } from "../../../components/Control/index";
import { zzControlHelper } from "../../../components/Control/zzControlHelper";
import { auth } from "../../../utils";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import * as permissions from "../../../constants/permissions"

export const ARDmKhFilter = (props) => {
    const { onLoadData, fieldCode } = props;
    const refForm = useRef();
    const [expand, setExpand] = useState(1);

    const memoSIDmLoai = useMemo(() => {
        const data = zzControlHelper.getDataFromLocal(ZenLookup.SIDmloai)
        return {
            TenPlkh: data?.filter(t => t.ma_nhom === "PLKH")
                ?.reduce((obj, item) => Object.assign(obj, { [item.ma]: item.ten }), {}),
            optionLoaiKh: zzControlHelper.convertToOptionsSemantic(data.filter(t => t.ma_nhom === "LOAI_KH"), true, "ma", "ma", "ten")
        }
    }, [])

    useEffect(() => {
        onLoadData(``);
    }, []);


    const handleSubmit = (item, { name }) => {
        const newItem = { ...refForm.current.values, [name]: item.value }
        const strSql = createStrSql(newItem)
        onLoadData(strSql, newItem);
    }

    function createStrSql(item = {}) {
        let _sql = "";

        if (item.loai) _sql += `AND loai = '${item.loai}'`;
        if (item.id_nv) _sql += `AND cuser = '${item.user_id}'`;
        if (item.ma_nvkd) _sql += `AND ma_nvkd = '${item.ma_nvkd}'`;
        if (item.ma_nguon_kh) _sql += `AND ma_nguon_kh = '${item.ma_nguon_kh}'`;
        if (item.ma_plkh1) _sql += `AND ma_plkh1 = '${item.ma_plkh1}'`;
        if (item.ma_plkh2) _sql += `AND ma_plkh2 = '${item.ma_plkh2}'`;
        if (item.ma_plkh3) _sql += `AND ma_plkh3 = '${item.ma_plkh3}'`;

        return _sql.replace("AND", "")
    }

    return <>
        <Accordion fluid styled>
            <Accordion.Title
                active={expand === 0}
                index={0}
                onClick={(e, { index }) => setExpand(expand === index ? -1 : index)}
            >
                <Icon name="dropdown" />
                Điều kiện lọc
            </Accordion.Title>
            <Accordion.Content active={expand === 0}>
                <ZenFormik form={"ardmkh-filter"}
                    ref={refForm}
                    validation={[]}
                    initItem={initItem}
                    onSubmit={handleSubmit}
                    onReset={(item) => onLoadData("")}
                >
                    {
                        formik => {
                            return <Form size="small">
                                <Form.Group widths="4">
                                    <ZenFieldSelect
                                        name="loai"
                                        options={memoSIDmLoai.optionLoaiKh}
                                        label={"ardmkh.ma"}
                                        defaultlabel="Loại KH"
                                        props={formik}
                                        onSelectedItem={handleSubmit}
                                    />
                                    {auth.checkPermission(permissions.ARDmKhXemAll) && <>
                                        <ZenFieldSelectApi
                                            name="id_nv"
                                            lookup={{
                                                ...ZenLookup.ID_NV,
                                                where: "loai_hsns = 5",
                                                onLocalWhere: (options) => {
                                                    return options.filter(t => t.loai_hsns == "5")
                                                }
                                            }}
                                            label={"ardmkh.cuser"}
                                            defaultlabel="User nhập"
                                            formik={formik}
                                            onItemSelected={handleSubmit}
                                        />
                                        <ZenFieldSelectApi
                                            name="ma_nvkd"
                                            lookup={{
                                                ...ZenLookup.ID_NV,
                                                where: "loai_hsns = 5",
                                                onLocalWhere: (options) => {
                                                    return options.filter(t => t.loai_hsns == "5")
                                                }
                                            }}
                                            label={"ardmkh.ma_nvkd"}
                                            defaultlabel="Người phụ trách"
                                            formik={formik}
                                            onItemSelected={handleSubmit}
                                        />
                                    </>}
                                </Form.Group>
                                <Form.Group widths="4">
                                    <ZenFieldSelectApi
                                        name="ma_nguon_kh"
                                        lookup={ZenLookup.Ma_nguon_kh}
                                        label={"ardmkh.nguon_kh"}
                                        defaultlabel="Nguồn khách hàng"
                                        formik={formik}
                                        onItemSelected={handleSubmit}
                                    />

                                    <ZenFieldSelectApi
                                        lookup={{
                                            ...ZenLookup.Ma_plkh,
                                            onLocalWhere: (itemsLocal) => {
                                                return itemsLocal?.filter(t => t.loai == "1") || []
                                            },
                                            where: 'loai = 1'
                                        }}
                                        isIntl={false} label={memoSIDmLoai.TenPlkh["1"]}
                                        name="ma_plkh1" formik={formik}
                                        onItemSelected={handleSubmit}
                                    />

                                    <ZenFieldSelectApi
                                        lookup={{
                                            ...ZenLookup.Ma_plkh,
                                            onLocalWhere: (itemsLocal) => {
                                                return itemsLocal?.filter(t => t.loai == "2") || []
                                            },
                                            where: 'loai = 2'
                                        }}
                                        isIntl={false} label={memoSIDmLoai.TenPlkh["2"]}
                                        name="ma_plkh2" formik={formik}
                                        onItemSelected={handleSubmit}
                                    />

                                    <ZenFieldSelectApi
                                        lookup={{
                                            ...ZenLookup.Ma_plkh,
                                            onLocalWhere: (itemsLocal) => {
                                                return itemsLocal?.filter(t => t.loai == "3") || []
                                            },
                                            where: 'loai = 3'
                                        }}
                                        isIntl={false} label={memoSIDmLoai.TenPlkh["3"]}
                                        name="ma_plkh3" formik={formik}
                                        onItemSelected={handleSubmit}
                                    />
                                </Form.Group>
                            </Form>
                        }
                    }
                </ZenFormik>

                <Button primary
                    content="Reset"
                    size="small" icon="refresh"
                    onClick={(e) => refForm.current.handleReset(e)}
                />
            </Accordion.Content>
        </Accordion>
    </>
}

const initItem = {
    loai: "",
    ma_nvkd: "",
    cuser: "",
    ma_nguon_kh: "",
    ma_plkh1: "",
    ma_plkh2: "",
    ma_plkh3: "",
}