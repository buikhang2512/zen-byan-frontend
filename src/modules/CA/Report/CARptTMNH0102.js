import React from "react";
import { Table } from "semantic-ui-react";
import { ZenFieldSelectApi } from "../../../components/Control";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import {
  RptHeader,
  RptTable,
  RptTableCell,
  RptTableRow,
} from "../../../components/Control/zReport";
import * as routes from "../../../constants/routes";
import { IntlFormat } from "../../../utils/intlFormat";
import { Mess_Report } from "../../ComponentInfo/zLanguage/variable";

const FilterForm = ({ formik }) => {
  const handleItemSelected = (item, { name }) => {
    if (name === "tk") {
      formik.setFieldValue("ten_tk", item.ten_tk);
    }
  };
  return (
    <>
      <ZenFieldSelectApi
        required
        lookup={ZenLookup.TK}
        label={"rpt.tk"}
        defaultlabel="Tài khoản"
        name="tk"
        formik={formik}
        onItemSelected={handleItemSelected}
      />

      <ZenFieldSelectApi
        lookup={ZenLookup.Ma_nt}
        label={"rpt.ma_nt"}
        defaultlabel="Ngoại tệ"
        name="ma_nt"
        formik={formik}
      />
    </>
  );
};

const TableForm = ({ data = [], filter = {} }) => {
  return (
    <>
      <RptTable maNt={filter.Ma_nt}>
        <Table.Header fullWidth>
          <RptHeader
            maNt={filter.Ma_nt}
            header={[
              { text: ["rpt.ngay_ct", "Ngày ghi sổ"] },
              { text: ["rpt.ngay_lct", "Ngày lập ct"] },
              { text: ["rpt.so_ct", "Số CT"] },
              { text: ["rpt.dien_giai", "Diễn giải"] },
              { text: ["rpt.tk", "TK"] },
              { text: ["rpt.tk_du", "TK Đ/Ứ"] },

              { text: ["rpt.ps_no", "ps nợ"], isNT: true },
              { text: ["rpt.ps_co", "ps có"], isNT: true },
              { text: ["rpt.so_du", "số dư"], isNT: true },
              { text: ["rpt.ma_nt", "Mã NT"], isNT: true, hiddenNT: true },
              { text: ["rpt.ty_gia", "Tỷ  giá"], isNT: true },

              { text: ["rpt.ps_no", "ps nợ"] },
              { text: ["rpt.ps_co", "ps có"] },
              { text: ["rpt.so_du", "số dư"] },
              { text: ["rpt.ma_kh", "Mã KH"] },
              { text: ["rpt.ten_kh", "Tên KH"] },

              { text: ["rpt.ma_bp", "Mã BP"], isNT: true },
              { text: ["rpt.ma_hd", "Mã HĐ"], isNT: true },
              { text: ["rpt.ma_ku", "Mã K/Ứ"], isNT: true },
              { text: ["rpt.ma_spct", "Mã SPCT"], isNT: true },

              { text: ["rpt.ma_ct", "Mã CT"] },
            ]}
          />
        </Table.Header>
        <Table.Body>
          {data.length > 0
            ? data.map((item, index) => {
              const isNt =
                filter.Ma_nt && filter.Ma_nt !== "VND" ? true : false;
              return (
                <RptTableRow
                  key={index}
                  isBold={item.bold}
                  index={index}
                  item={item}
                >
                  <RptTableCell value={item.Ngay_ct} type="date" />
                  <RptTableCell value={item.Ngay_lct} type="date" />
                  <RptTableCell value={item.So_ct} />
                  <RptTableCell value={item.Dien_giai} />
                  <RptTableCell value={item.Tk} />
                  <RptTableCell value={item.tk_du} />

                  {isNt && (
                    <RptTableCell value={item.Ps_no_nt} type="number" />
                  )}
                  {isNt && (
                    <RptTableCell value={item.Ps_co_nt} type="number" />
                  )}
                  {isNt && (
                    <RptTableCell value={item.So_du_nt} type="number" />
                  )}
                  {isNt && <RptTableCell value={item.Ma_nt} />}
                  {isNt && <RptTableCell value={item.Ty_gia} type="number" />}

                  <RptTableCell value={item.Ps_no} type="number" />
                  <RptTableCell value={item.Ps_co} type="number" />
                  <RptTableCell value={item.so_du} type="number" />
                  <RptTableCell value={item.ma_kh} />
                  <RptTableCell value={item.ten_kh} />

                  {isNt && <RptTableCell value={item.ma_bp} />}
                  {isNt && <RptTableCell value={item.ma_hd} />}
                  {isNt && <RptTableCell value={item.ma_ku} />}
                  {isNt && <RptTableCell value={item.ma_spct} />}

                  <RptTableCell value={item.ma_ct} />
                </RptTableRow>
              );
            })
            : undefined}
        </Table.Body>
      </RptTable>
    </>
  );
};

export const CARptTMNH0102 = {
  FilterForm: FilterForm,
  TableForm: TableForm,
  permission: "04.40.2",
  visible: true, // hiện/ẩn báo cáo
  route: routes.RptCARptTMNH0102,

  period: {
    fromDate: "ngay_ct1",
    toDate: "ngay_ct2",
  },

  linkHeader: {
    id: "CARptTMNH0102",
    defaultMessage: "Sổ kế toán chi tiết quỹ tiền mặt",
    active: true,
    isReportCenter: true,
  },

  info: {
    code: "04.20.02.2",
  },
  initFilter: {
    ngay_ct1: "",
    ngay_ct2: "",
    tk: "",
    ma_nt: "",
  },

  columns: [
    {
      ...IntlFormat.default(Mess_Report.NgayGhiSo),
      fieldName: "ngay_ct",
      type: "date",
      sorter: true,
    },
    {
      ...IntlFormat.default(Mess_Report.NgayLct),
      fieldName: "ngay_lct",
      type: "date",
      sorter: true,
    },
    {
      ...IntlFormat.default(Mess_Report.SoCt),
      fieldName: "so_ct",
      type: "string",
      sorter: true,
    },
    {
      ...IntlFormat.default(Mess_Report.DienGiai),
      fieldName: "dien_giai",
      type: "string",
      sorter: true,
    },
    {
      ...IntlFormat.default(Mess_Report.TaiKhoan),
      fieldName: "tk",
      type: "string",
      sorter: true,
    },
    {
      ...IntlFormat.default(Mess_Report.TkDu),
      fieldName: "tk_du",
      type: "string",
      sorter: true,
    },
    {
      ...IntlFormat.default(Mess_Report.Thu),
      fieldName: "ps_no_nt",
      type: "number",
      sorter: true,
      isNT: true,
    },
    {
      ...IntlFormat.default(Mess_Report.Chi),
      fieldName: "ps_co_nt",
      type: "number",
      sorter: true,
      isNT: true,
    },
    {
      ...IntlFormat.default(Mess_Report.Ton),
      fieldName: "so_du_nt",
      type: "number",
      sorter: true,
      isNT: true,
    },
    {
      ...IntlFormat.default(Mess_Report.MaNt),
      fieldName: "ma_nt",
      type: "string",
      sorter: true,
      isNT: true,
      hiddenNT: true,
    },
    {
      ...IntlFormat.default(Mess_Report.TyGia),
      fieldName: "ty_gia",
      type: "number",
      sorter: true,
      isNT: true,
    },
    {
      ...IntlFormat.default(Mess_Report.PsNo),
      fieldName: "ps_no",
      type: "number",
      sorter: true,
      isNT: false,
    },
    {
      ...IntlFormat.default(Mess_Report.PsCo),
      fieldName: "ps_co",
      type: "number",
      sorter: true,
      isNT: false,
    },
    {
      ...IntlFormat.default(Mess_Report.SoDu),
      fieldName: "so_du",
      type: "number",
      sorter: true,
      isNT: false,
    },
    {
      ...IntlFormat.default(Mess_Report.MaKh),
      fieldName: "ma_kh",
      type: "string",
      sorter: true,
    },
    {
      ...IntlFormat.default(Mess_Report.TenKh),
      fieldName: "ten_kh",
      type: "string",
      sorter: true,
    },
    {
      ...IntlFormat.default(Mess_Report.MaBp),
      fieldName: "ma_bp",
      type: "string",
      sorter: true,
    },
    {
      ...IntlFormat.default(Mess_Report.MaHd),
      fieldName: "ma_hd",
      type: "string",
      sorter: true,
    },
    {
      ...IntlFormat.default(Mess_Report.MaKu),
      fieldName: "ma_ku",
      type: "string",
      sorter: true,
    },
    {
      ...IntlFormat.default(Mess_Report.MaSPCT),
      fieldName: "ma_spct",
      type: "string",
      sorter: true,
    },
    {
      ...IntlFormat.default(Mess_Report.MaCt),
      fieldName: "ma_ct",
      type: "string",
      sorter: true,
    },
  ],
  formValidation: [
    {
      id: "tk",
      validationType: "string",
      validations: [
        {
          type: "required",
          params: ["Không được bỏ trống trường này"],
        },
      ],
    },
  ],
};
