import React, { useEffect } from "react";
import { Table, Form } from "semantic-ui-react";
import { ZenFieldSelectApi, ZenDatePeriod } from "../../../components/Control";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import {
  RptHeader,
  RptTable,
  RptTableCell,
  RptTableRow,
} from "../../../components/Control/zReport";
import * as routes from "../../../constants/routes";
import { IntlFormat } from "../../../utils/intlFormat";
import { Mess_Report } from "../../ComponentInfo/zLanguage/variable";
import { useLocation } from "react-router-dom";

const FilterForm = ({ formik }) => {
  const handleItemSelected = (item, { name }) => {
    if (name === "tk") {
      formik.setFieldValue("ten_tk", item.ten_tk);
    }
  };

  const location = useLocation()
  useEffect(() => {
    let tk = new URLSearchParams(location.search).get("tk")
    if (tk) formik.setFieldValue("tk", tk);
  }, [location]);
  
  return (
    <>
      <ZenFieldSelectApi
        required
        lookup={ZenLookup.TK}
        label={"rpt.tk"}
        defaultlabel="Tài khoản"
        name="tk"
        formik={formik}
        onItemSelected={handleItemSelected}
      />

      <ZenFieldSelectApi
        lookup={ZenLookup.Ma_nt}
        label={"rpt.ma_nt"}
        defaultlabel="Ngoại tệ"
        name="ma_nt"
        formik={formik}
      />
    </>
  );
};

const TableForm = ({ data = [], filter = {} }) => {
  return (
    <>
      <RptTable maNt={filter.Ma_nt}>
        <Table.Header fullWidth>
          <RptHeader
            maNt={filter.MaNt}
            header={[
              { text: ["CARptTMNH02.ngay_ct", "Ngày ghi sổ"] },
              { text: ["rpt.so_ct", "Số CT"] },
              { text: ["CARptTMNH02.ngay_lct", "Ngày CT"] },
              { text: ["rpt.dien_giai", "Diễn giải"] },

              { text: ["CARptTMNH02.ps_no_nt", "Thu"], isNT: true },
              { text: ["CARptTMNH02.ps_co_nt", "Chi"], isNT: true },
              { text: ["CARptTMNH02.so_du_nt", "Tồn"], isNT: true },
              { text: ["rpt.ma_nt", "Mã NT"], isNT: true, hiddenNT: true },
              { text: ["rpt.ty_gia", "Tỷ  giá"], isNT: true },

              { text: ["CARptTMNH02.tk_du", "TK Đ/Ư"], isNT: false },
              { text: ["CARptTMNH02.ps_no", "Thu (Gửi vào)"] },
              { text: ["CARptTMNH02.ps_co", "Chi (Rút ra)"] },
              { text: ["CARptTMNH02.so_du", "Tồn"] },
              { text: ["CARptTMNH02.ma_kh", "Mã KH"] },
              { text: ["CARptTMNH02.ten_kh", "Tên KH"] },
              { text: ["rpt.ma_ct", "Mã CT"] },
            ]}
          />
        </Table.Header>
        <Table.Body>
          {data.length > 0
            ? data.map((item, index) => {
              const isNt =
                filter.MaNt && filter.MaNt !== "VND" ? true : false;
              return (
                <RptTableRow
                  key={index}
                  isBold={item.bold}
                  index={index}
                  item={item}
                >
                  <RptTableCell value={item.Ngay_ct} type="date" />
                  <RptTableCell value={item.So_ct} />
                  <RptTableCell value={item.ngay_lct} type="date" />
                  <RptTableCell value={item.Dien_giai} />

                  {isNt && (
                    <RptTableCell value={item.Ps_no_nt} type="number" />
                  )}
                  {isNt && (
                    <RptTableCell value={item.Ps_co_nt} type="number" />
                  )}
                  {isNt && (
                    <RptTableCell value={item.so_du_nt} type="number" />
                  )}
                  {isNt && <RptTableCell value={item.Ma_nt} />}
                  {isNt && <RptTableCell value={item.Ty_gia} type="number" />}

                  <RptTableCell value={item.Tk_du} />
                  <RptTableCell value={item.Ps_no} type="number" />
                  <RptTableCell value={item.Ps_co} type="number" />
                  <RptTableCell value={item.so_du} type="number" />
                  <RptTableCell value={item.Ma_kh} />
                  <RptTableCell value={item.ten_kh} />
                  <RptTableCell value={item.ma_ct} />
                </RptTableRow>
              );
            })
            : undefined}
        </Table.Body>
      </RptTable>
    </>
  );
};

export const CARptTMNH02 = {
  FilterForm: FilterForm,
  TableForm: TableForm,
  permission: "04.40.3",
  visible: true, // hiện/ẩn báo cáo
  route: routes.RptCARptTMNH02,

  period: {
    fromDate: "ngay1",
    toDate: "ngay2",
  },

  linkHeader: {
    id: "CARptTMNH02",
    defaultMessage: "Sổ tiền gửi ngân hàng",
    active: true,
    isReportCenter: true,
  },

  info: {
    code: "04.20.05",
  },
  initFilter: {
    ngay1: "",
    ngay2: "",
    tk: "",
    ma_nt: "",
  },

  columns: [
    {
      ...IntlFormat.default(Mess_Report.NgayGhiSo),
      fieldName: "ngay_ct",
      type: "date",
      sorter: true,
    },
    {
      ...IntlFormat.default(Mess_Report.SoCt),
      fieldName: "so_ct",
      type: "string",
      sorter: true,
    },
    {
      ...IntlFormat.default(Mess_Report.NgayLct),
      fieldName: "ngay_lct",
      type: "date",
      sorter: true,
    },
    {
      ...IntlFormat.default(Mess_Report.DienGiai),
      fieldName: "dien_giai",
      type: "string",
      sorter: true,
    },
    {
      ...IntlFormat.default(Mess_Report.Thu),
      fieldName: "ps_no_nt",
      type: "number",
      sorter: true,
      isNT: true,
    },
    {
      ...IntlFormat.default(Mess_Report.Chi),
      fieldName: "ps_co_nt",
      type: "number",
      sorter: true,
      isNT: true,
    },
    {
      ...IntlFormat.default(Mess_Report.Ton),
      fieldName: "so_du_nt",
      type: "number",
      sorter: true,
      isNT: true,
    },
    {
      ...IntlFormat.default(Mess_Report.MaNt),
      fieldName: "ma_nt",
      type: "string",
      sorter: true,
      isNT: true,
      hiddenNT: true,
    },
    {
      ...IntlFormat.default(Mess_Report.TkDu),
      fieldName: "tk_du",
      type: "number",
      sorter: true,
      isNT: false,
    },
    {
      ...IntlFormat.default(Mess_Report.Thu),
      fieldName: "ps_no",
      type: "number",
      sorter: true,
    },
    {
      ...IntlFormat.default(Mess_Report.Chi),
      fieldName: "ps_co",
      type: "number",
      sorter: true,
    },
    {
      ...IntlFormat.default(Mess_Report.Ton),
      fieldName: "so_du",
      type: "number",
      sorter: true,
    },
    {
      ...IntlFormat.default(Mess_Report.MaKh),
      fieldName: "ma_kh",
      type: "string",
      sorter: true,
    },
    {
      ...IntlFormat.default(Mess_Report.TenKh),
      fieldName: "ten_kh",
      type: "string",
      sorter: true,
    },
    {
      ...IntlFormat.default(Mess_Report.MaCt),
      fieldName: "ma_ct",
      type: "string",
      sorter: true,
    },
  ],
  formValidation: [
    {
      id: "tk",
      validationType: "string",
      validations: [
        {
          type: "required",
          params: ["Không được bỏ trống trường này"],
        },
      ],
    },
  ],
};
