import { CARptTMNH01 } from "./CARptTMNH01"
import { CARptTMNH0102 } from "./CARptTMNH0102"
import { CARptTMNH02 } from "./CARptTMNH02"

export const CA_Report = {
    CARptTMNH01: CARptTMNH01,
    CARptTMNH0102: CARptTMNH0102,
    CARptTMNH02: CARptTMNH02,
}
