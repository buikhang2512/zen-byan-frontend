import axios from '../../../Api/axios';

const ExtName = "ca4"

export const ApiCAVchCA4 = {
   get(callback, filter = {}, pagination = {}) {
      axios.get(`${ExtName}`, {
         params: {
            qf: filter.qf,
            keyword: filter.keyword,
            sort: filter.sort,
            page: pagination.page,
            pageSize: pagination.pageSize
         },
      })
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   getByCode(stt_rec, callback) {
      axios.get(`${ExtName}/` + stt_rec)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   insert(data, callback) {
      axios.post(`${ExtName}`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   update(data, callback) {
      axios.put(`${ExtName}`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   delete(stt_rec, callback) {
      axios.delete(`${ExtName}/` + stt_rec)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   report(stt_rec, q, callback) {
      axios.get(`${ExtName}/${stt_rec}/rpt`, {
         params: {
            codeReport: q.codeReport,
            parameters: q.params,
         },
      })
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },
}