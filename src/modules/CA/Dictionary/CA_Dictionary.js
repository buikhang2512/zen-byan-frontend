import * as routes from "../../../constants/routes";
import { ApiArDmKh } from "../../AR/Api";
import { ApiCaCdKu } from "../Api/ApiCaCdKu";
import { ApiCaDmKu } from "../Api/ApiCaDmKu";
import { ZenHelper } from "../../../utils";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { Select, Dropdown, Form } from "semantic-ui-react";
import React, { useEffect, useMemo, useState } from "react";
import { ZenMessageAlert } from "../../../components/Control";
import { GlobalStorage, KeyStorage } from "../../../utils/storage";
import { FormMode } from "../../../components/Control/zzControlHelper";

const ContainerCaCdKu = (props) => {
  const { onLoadData, fieldCode } = props;
  const [nam, setNam] = useState(ZenHelper.getFiscalYear());
  const listNamTC = useMemo(() => getNamTaiChinh(), []);

  useEffect(() => {
    onLoadData(`nam = ${nam}`);
  }, []);

  useEffect(() => {
    GlobalStorage.set('nam_sodu', nam)
  }, [nam]);

  function getNamTaiChinh() {
    return ZenHelper.getOptYear(ZenHelper.getFiscalYear());
  }

  return (
    <div style={{ padding: "12px" }}>
      <strong>
        Số dư khế ước năm:{" "}
        <Dropdown
          scrolling
          options={listNamTC}
          label={"GLCDTK.nam"}
          name="nam"
          value={nam}
          onChange={(e, { name, value }) => {
            setNam(value);
            onLoadData(`nam = ${value}`, undefined, true);
          }}
          clearable={false}
        />
      </strong>
    </div>
  );
};

const CheckNamTCBeforeAction = (item, mode) => {
  const data = GlobalStorage.getByField(KeyStorage.CacheData, "sidmnamtc")?.data
  const namtc = data.filter(d => d.nam === item.nam)[0]
  if (mode === FormMode.DEL) {
    if (namtc.ngay_ks && namtc.ngay_ks >= namtc.ngay_dntc) {
      ZenMessageAlert.warning(`Năm tài chính ${item.nam} đã khóa số liệu, ngày khóa: ${ZenHelper.formatDateTime(namtc.ngay_ks, 'DD/MM/YYYY')} `)
      return { open: false }
    } else {
      return { open: true }
    }
  }
  if (mode === FormMode.EDIT) {
    if (namtc.ngay_ks && namtc.ngay_ks >= namtc.ngay_dntc) {
      return { open: true, mode: FormMode.VIEW }

    } else {
      return { open: true }
    }
  }
  return { open: true }
}

const CA_Dictionary = {
  CaDmNv: {
    route: routes.CADmNv,
    importExcel: true,
    action: {
      view: { visible: true, permission: "04.21.1" },
      add: { visible: true, permission: "04.21.2" },
      edit: { visible: true, permission: "04.21.3" },
      del: { visible: true, permission: "04.21.4" },
    },

    linkHeader: {
      id: "CADmNv",
      defaultMessage: "Danh mục nhân viên",
      active: true,
      isSetting: false,
    },

    tableList: {
      keyForm: "CADmNv",
      formModal: ZenLookup.CaDmNv,
      fieldCode: "ma_kh",
      unPagination: false,
      duplicate: true,
      showFilterLabel: true,

      api: {
        url: ApiArDmKh,
        type: "sql",
        qf: `(isnv = 1 or isnvkd = 1)`,
      },
      columns: [
        {
          id: "CaDmNv.ma_kh",
          defaultMessage: "Mã nhân viên",
          fieldName: "ma_kh",
          filter: "string",
          sorter: true,
          editForm: true,
        },
        {
          id: "CaDmNv.ten_kh",
          defaultMessage: "Tên nhân viên",
          fieldName: "ten_kh",
          filter: "string",
          sorter: true,
          editForm: true,
        },
        {
          id: "CaDmNv.ma_so_thue",
          defaultMessage: "Mã số thuế",
          fieldName: "ma_so_thue",
          filter: "string",
          sorter: true,
        },
        {
          id: "CaDmNv.dia_chi",
          defaultMessage: "Địa chỉ",
          fieldName: "dia_chi",
          filter: "string",
          sorter: true,
        },
        {
          id: "CaDmNv.tel",
          defaultMessage: "ĐT",
          fieldName: "tel",
          filter: "string",
          sorter: true,
        },
        {
          id: "CaDmNv.fax",
          defaultMessage: "Fax",
          fieldName: "fax",
          filter: "string",
          sorter: true,
        },
        {
          id: "CaDmNv.email",
          defaultMessage: "Email",
          fieldName: "email",
          filter: "string",
          sorter: true,
        },
        {
          id: "CaDmNv.home_page",
          defaultMessage: "Websites",
          fieldName: "home_page",
          filter: "string",
          sorter: true,
        },
        {
          id: "CaDmNv.nguoi_gd",
          defaultMessage: "Người giao dịch",
          fieldName: "nguoi_gd",
          filter: "string",
          sorter: true,
        },
        {
          id: "CaDmNv.ma_plkh1",
          defaultMessage: "Mã phân loại KH 1",
          fieldName: "ma_plkh1",
          filter: "string",
          sorter: true,
        },
        {
          id: "CaDmNv.ma_plkh2",
          defaultMessage: "Mã phân loại KH 2",
          fieldName: "ma_plkh2",
          filter: "string",
          sorter: true,
        },
        {
          id: "CaDmNv.ma_plkh3",
          defaultMessage: "Mã phân loại KH 3",
          fieldName: "ma_plkh3",
          filter: "string",
          sorter: true,
        },
        {
          id: "CaDmNv.ma_nhkh",
          defaultMessage: "Mã nhóm KH",
          fieldName: "ma_nhkh",
          filter: "string",
          sorter: true,
        },
        {
          id: "ksd",
          defaultMessage: "Không sử dụng",
          fieldName: "ksd",
          type: "bool",
          filter: "",
          sorter: true,
          collapsing: true,
        },
      ],
    },
  },

  CaDmKu: {
    route: routes.CADmKu,
    importExcel: true,
    action: {
      view: { visible: true, permission: "" },
      add: { visible: true, permission: "" },
      edit: { visible: true, permission: "" },
      del: { visible: true, permission: "" },
    },

    linkHeader: {
      id: "CaDmKu",
      defaultMessage: "Danh mục khế ước",
      active: true,
      isSetting: false,
    },

    tableList: {
      keyForm: "CaDmKu",
      formModal: ZenLookup.CaDmKu,
      fieldCode: "ma_ku",
      unPagination: false,
      duplicate: true,
      showFilterLabel: true,
      api: {
        url: ApiCaDmKu,
        type: "sql",
        qf: ``,
      },
      columns: [
        {
          id: "CaDmKu.ma_ku",
          defaultMessage: "Mã khế ước",
          fieldName: "ma_ku",
          filter: "string",
          sorter: true,
          editForm: true,
        },
        {
          id: "CaDmKu.ten_ku",
          defaultMessage: "Tên khế ước",
          fieldName: "ten_ku",
          filter: "string",
          sorter: true,
        },
        {
          id: "CaDmKu.so_ku",
          defaultMessage: "Số khế ước",
          fieldName: "so_ku",
          filter: "string",
          sorter: true,
        },
        {
          id: "CaDmKu.ngay_ky",
          defaultMessage: "Ngày ký",
          fieldName: "ngay_ky",
          filter: "string",
          type: "date",
          sorter: true,
        },
        {
          id: "CaDmKu.ngay_vay",
          defaultMessage: "Ngày vay",
          fieldName: "ngay_vay",
          filter: "string",
          type: "date",
          sorter: true,
        },
        {
          id: "CaDmKu.tien_vay",
          defaultMessage: "Tiền vay",
          fieldName: "tien",
          filter: "number",
          type: "number",
          sorter: true,
        },
        {
          id: "CaDmKu.ma_nt",
          defaultMessage: "Mã NT",
          fieldName: "ma_nt",
          filter: "string",
          type: "string",
          sorter: true,
        },
        {
          id: "CaDmKu.ghi_chu",
          defaultMessage: "Ghi chú",
          fieldName: "ghi_chu",
          filter: "string",
          type: "string",
          sorter: true,
        },
      ]
    }
  },

  CaCdKu: {
    route: routes.CACdKu,

    action: {
      view: { visible: true, permission: "" },
      add: { visible: true, permission: "" },
      edit: { visible: true, permission: "" },
      del: { visible: true, permission: "" },
    },

    linkHeader: {
      id: "CaCdKu",
      defaultMessage: "Số dư khế ước",
      active: true,
      isSetting: false,
    },

    tableList: {
      keyForm: "CaCdKu",
      formModal: ZenLookup.CaCdKu,
      fieldCode: "ma_ku",
      unPagination: false,
      changeCode: false,
      multiKey: ["nam", "ma_ku"],
      ContainerTop: ContainerCaCdKu,
      onBeforeAction: CheckNamTCBeforeAction,
      showFilterLabel: true,
      api: {
        url: ApiCaCdKu,
        type: "sql",
        params: ["nam", "ma_ku"],
      },
      columns: [
        {
          id: "CaCdKu.ma_ku",
          defaultMessage: "Mã khế ước",
          fieldName: "ma_ku",
          filter: "string",
          sorter: true,
          editForm: true,
        },
        {
          id: "CaCdKu.ten_ku",
          defaultMessage: "Tên khế ước",
          fieldName: "ten_ku",
          filter: "string",
          sorter: true,
        },
        {
          id: "CaCdKu.tk",
          defaultMessage: "TK",
          fieldName: "tk",
          filter: "string",
          sorter: true,
        },
        {
          id: "CaCdKu.ngay_ku",
          defaultMessage: "Ngày khế ước",
          fieldName: "ngay_ku",
          filter: "date",
          sorter: true,
          type: "date",
        },
        {
          id: "CaCdKu.ngay_tt",
          defaultMessage: "Ngày thanh toán",
          fieldName: "ngay_tt",
          filter: "date",
          sorter: true,
          type: "date",
        },
        {
          id: "CaCdKu.da_vay_nt",
          defaultMessage: "Đã vay NT",
          fieldName: "da_vay_nt",
          filter: "number",
          sorter: true,
          type: "number",
        },
        {
          id: "CaCdKu.da_tt_nt",
          defaultMessage: "Đã TT NT",
          fieldName: "da_tt_nt",
          filter: "number",
          sorter: true,
          type: "number",
        },
        {
          id: "CaCdKu.ma_nt",
          defaultMessage: "Mã NT",
          fieldName: "ma_nt",
          filter: "string",
          sorter: true,
        },
        {
          id: "CaCdKu.da_vay",
          defaultMessage: "Đã vay",
          fieldName: "da_vay",
          filter: "number",
          sorter: true,
          type: "number",
        },
        {
          id: "CaCdKu.da_tt",
          defaultMessage: "Đã thanh toán",
          fieldName: "da_tt",
          filter: "number",
          sorter: true,
          type: "number",
        },
      ],
    },
  },

};


export default CA_Dictionary;
