import React from "react";
import {
  ZenField,
  ZenFieldTextArea,
  ZenFieldSelectApi,
  ZenFieldCheckbox,
  ZenMessageAlert,
} from "../../../components/Control/index";
import { FormMode } from "../../../utils";
import { Form, Tab } from "semantic-ui-react";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { ValidError } from "../../../utils/language/variable";
import { IntlFormat } from "../../../utils/intlFormat";
import { ApiArDmKh } from "../../AR/Api";

const CADmNvModal = (props) => {
  const { formik, permission, mode } = props;

  return (
    <React.Fragment>
      <Form.Group>
        <ZenField
          required
          width="4"
          autoFocus
          readOnly={!permission || (mode === FormMode.ADD ? false : true)}
          label={"cadmnv.ma_kh"}
          defaultlabel="Mã nhân viên"
          name="ma_kh"
          props={formik}
          textCase="uppercase"
          maxLength={20}
        />
        <ZenField
          required
          width="12"
          readOnly={!permission}
          label={"cadmnv.ten_kh"}
          defaultlabel="Tên nhân viên"
          name="ten_kh"
          props={formik}
        />
      </Form.Group>
      <Form.Group>
        <ZenField
          width="12"
          readOnly={!permission}
          label={"ArDmKh.dia_chi"}
          defaultlabel="Địa chỉ"
          name="dia_chi"
          props={formik}
        />
        <ZenField
          width="4"
          readOnly={!permission}
          label={"ArDmKh.ma_so_thue"}
          defaultlabel="Mã số thuế"
          name="ma_so_thue"
          props={formik}
        />
      </Form.Group>
      <Form.Group style={{ alignItems: "center" }}>
        <ZenFieldSelectApi
          width="4"
          readOnly={!permission}
          lookup={{ ...ZenLookup.Ma_nhkh }}
          label={"ArDmKh.ma_nhkh"}
          defaultlabel="Mã nhóm"
          name="ma_nhkh"
          formik={formik}
        />
        <ZenFieldCheckbox
          width="3"
          readOnly={!permission}
          style={{ marginTop: "16px" }}
          label={"ardmkh.kh"}
          defaultlabel="Khách hàng"
          name="iskh"
          props={formik}
          tabIndex={-1}
        />
        <ZenFieldCheckbox
          width="3"
          readOnly={!permission}
          style={{ marginTop: "16px" }}
          label={"ardmkh.ncc"}
          defaultlabel="Nhà cung cấp"
          name="isncc"
          props={formik}
          tabIndex={-1}
        />
        <ZenFieldCheckbox
          width="3"
          readOnly={!permission}
          style={{ paddingTop: "16px" }}
          label={"ardmkh.nv"}
          defaultlabel="Nhân viên"
          name="isnv"
          props={formik}
          tabIndex={-1}
        />
        <ZenFieldCheckbox
          width="3"
          readOnly={!permission}
          style={{ marginTop: "16px" }}
          label={"ardmkh.nvkd"}
          defaultlabel="Nhân viên KD"
          name="isnvkd"
          props={formik}
          tabIndex={-1}
        />
      </Form.Group>
      <Tab panes={panes(formik, permission)} />
    </React.Fragment>
  );
};

const panes = (formik, permission) => [
  {
    menuItem: { key: "Info", icon: "info", content: "Thông tin chung" },
    render: () => <Tab.Pane>{info(formik, permission)}</Tab.Pane>,
  },
];
const info = (formik, permission) => (
  <>
    <Form.Group widths="equal">
      <ZenField
        readOnly={!permission}
        label={"ArDmKh.nguoi_gd"}
        defaultlabel="Người giao dịch"
        name="nguoi_gd"
        props={formik}
      />
      <ZenField
        readOnly={!permission}
        label={"ArDmKh.home_page"}
        defaultlabel="Trang chủ"
        name="home_page"
        props={formik}
      />
    </Form.Group>
    <Form.Group widths="equal">
      <ZenField
        readOnly={!permission}
        label={"ArDmKh.tel"}
        defaultlabel="Điện thoại"
        name="tel"
        props={formik}
      />
      <ZenField
        readOnly={!permission}
        label={"ArDmKh.fax"}
        defaultlabel="Fax"
        name="fax"
        props={formik}
      />
      <ZenField
        readOnly={!permission}
        label={"ArDmKh.email"}
        defaultlabel="Email"
        name="email"
        props={formik}
      />
    </Form.Group>

    <Form.Group>
      <ZenFieldSelectApi
        width="8"
        readOnly={!permission}
        lookup={{
          // ...ZenLookup.Ma_httt_so,
          ...ZenLookup.Ma_httt,
          onLocalWhere: (items) => {
            return items.filter((t) => t.moduleid == "SO");
          },
        }}
        label={"ArDmKh.ma_httt"}
        defaultlabel="Mã HTTT"
        name="ma_httt"
        formik={formik}
      />
    </Form.Group>

    <Form.Group widths="equal">
      <ZenFieldSelectApi
        upward
        readOnly={!permission}
        lookup={{
          ...ZenLookup.Ma_plkh,
          onLocalWhere: (items) => {
            return items?.filter((t) => t.loai == "1") || [];
          },
          where: "loai = 1",
        }}
        label={"ArDmKh.ma_plkh1"}
        defaultlabel="Phân loại 1"
        name="ma_plkh1"
        formik={formik}
      />
      <ZenFieldSelectApi
        upward
        readOnly={!permission}
        lookup={{
          ...ZenLookup.Ma_plkh,
          onLocalWhere: (items) => {
            return items?.filter((t) => t.loai == "2") || [];
          },
          where: "loai = 2",
        }}
        label={"ArDmKh.ma_plkh2"}
        defaultlabel="Phân loại 2"
        name="ma_plkh2"
        formik={formik}
      />
      <ZenFieldSelectApi
        upward
        readOnly={!permission}
        lookup={{
          ...ZenLookup.Ma_plkh,
          onLocalWhere: (items) => {
            return items?.filter((t) => t.loai == "3") || [];
          },
          where: "loai = 3",
        }}
        label={"ArDmKh.ma_plkh3"}
        defaultlabel="Phân loại 3"
        name="ma_plkh3"
        formik={formik}
      />
    </Form.Group>
    <ZenFieldTextArea
      readOnly={!permission}
      label={"ArDmKh.ghi_chu"}
      defaultlabel="ghi chú"
      name="ghi_chu"
      props={formik}
    />
  </>
);

const onBeforeSave = (item) => {
  if (!item.iskh && !item.isncc && !item.isnv) {
    ZenMessageAlert.error("Vui lòng chọn 1 trong 3 nhóm: Khách hàng, Nhà cung cấp hoặc Nhân viên");
  } else {
    return new Promise(resolve => {
      return resolve(item)
    })
  }
}

const CADmNv = {
  FormModal: CADmNvModal,
  onBeforeSave: onBeforeSave,
  header: "Nhân viên",
  api: {
    url: ApiArDmKh,
  },
  permission: {
    view: "04.21.1",
    add: "04.21.2",
    edit: "04.21.3"
  },
  formId: "CaDmNv-form",
  size: "large",
  initItem: {
    ma_kh: "",
    ten_kh: "",
    isnv: true,
    ksd: false
  },
  // isScrolling: false, scroll phần content: default True

  formValidation: [
    {
      ...ValidError.formik(
        { name: "ma_kh", type: "string" },
        { type: "required", intl: IntlFormat.default(ValidError.Required) },
        {
          type: "max",
          intl: IntlFormat.default(ValidError.MaxLength),
          params: [20],
        }
      ),
    },
    {
      ...ValidError.formik(
        { name: "ten_kh", type: "string" },
        { type: "required", intl: IntlFormat.default(ValidError.Required) }
      ),
    },
  ],
};

export { CADmNv };
