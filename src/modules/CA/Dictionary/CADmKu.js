import React from "react";
import {
  ZenField,
  ZenFieldDate,
  ZenFieldSelectApi,
  ZenFieldSelect,
  ZenFieldNumber,
} from "../../../components/Control/index";
import { FormMode, ZenHelper } from "../../../utils";
import { Form, Tab } from "semantic-ui-react";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { ValidError } from "../../../utils/language/variable";
import { IntlFormat } from "../../../utils/intlFormat";
import { ApiCaDmKu } from "../Api/ApiCaDmKu";

const options_th = [
  { key: "0", value: "0", text: "Ngày" },
  { key: "1", value: "1", text: "Tháng" },
  { key: "2", value: "2", text: "Năm" },
];
const options_pp = [
  { key: "1", value: "1", text: "Trả gốc và lãi theo dự nợ giảm dần" },
  { key: "2", value: "2", text: "Trả góp đều hàng tháng theo lãi suất kép" },
  { key: "3", value: "3", text: "Trả góp đều theo lãi xuất đơn" },
  {
    key: "4",
    value: "4",
    text: "Trả góp đều. lãi tính trên dư nợ giảm dần hàng tháng",
  },
];
const options_ttg = [
  { key: "1", value: "1", text: "Hàng tháng" },
  { key: "2", value: "2", text: "Hàng quý" },
  { key: "3", value: "3", text: "Hàng năm" },
  {
    key: "4",
    value: "4",
    text: "Thanh toán 1 lần cuối kỳ",
  },
];
const options_ttl = [
  { key: "1", value: "1", text: "Hàng tháng" },
  { key: "2", value: "2", text: "Hàng quý" },
  { key: "3", value: "3", text: "Hàng năm" },
  {
    key: "4",
    value: "4",
    text: "Thanh toán 1 lần cuối kỳ",
  },
];

const CaDmKuMoDal = (props) => {
  const { formik, permission, mode } = props;
  const handleChangeNgayvay = (date) => {
    const { name, value } = date;
    const { tinh_theo, thoi_han } = formik.values;

    let newNgaydh;
    if (tinh_theo == "0") {
      newNgaydh = ZenHelper.addDays(value, Number(thoi_han));
    } else if (tinh_theo == "1") {
      newNgaydh = ZenHelper.addMonths(value, Number(thoi_han));
    } else if (tinh_theo == "2") {
      newNgaydh = ZenHelper.addYears(value, Number(thoi_han));
    }
    formik.setFieldValue("ngay_dh", newNgaydh);
  };

  const handleChangeThoiHan = (number) => {
    const { floatValue, name } = number;

    const { tinh_theo, ngay_vay } = formik.values;
    let newNgaydh;
    if (tinh_theo == "0") {
      newNgaydh = ZenHelper.addDays(ngay_vay, floatValue);
    } else if (tinh_theo == "1") {
      newNgaydh = ZenHelper.addMonths(ngay_vay, floatValue);
    } else if (tinh_theo == "2") {
      newNgaydh = ZenHelper.addYears(ngay_vay, floatValue);
    }
    formik.setFieldValue("ngay_dh", newNgaydh);
  };

  const handleChangeTinhTheo = (item) => {
    const { value, name } = item;
    const { thoi_han, ngay_vay } = formik.values;
    let newNgaydh;
    if (value == "0") {
      newNgaydh = ZenHelper.addDays(ngay_vay, Number(thoi_han));
    } else if (value == "1") {
      newNgaydh = ZenHelper.addMonths(ngay_vay, Number(thoi_han));
    } else if (value == "2") {
      newNgaydh = ZenHelper.addYears(ngay_vay, Number(thoi_han));
    }
    formik.setFieldValue("ngay_dh", newNgaydh);
  };

  function changeLabel() {
    let dflabel = "Lãi xuất";
    const { tinh_theo } = formik.values;
    if (tinh_theo == "0") {
      dflabel = "Lãi suất/ngày";
    } else if (tinh_theo == "1") {
      dflabel = "Lãi suất/tháng";
    } else if (tinh_theo == "2") {
      dflabel = "Lãi suất/năm";
    }
    return {
      defaultlabel: dflabel,
    };
  }

  return (
    <React.Fragment>
      <Form.Group>
        <ZenField
          width="4"
          autoFocus
          readOnly={!permission || (mode === FormMode.ADD ? false : true)}
          label={"cadmku.ma_ku"}
          defaultlabel="Mã khế ước"
          name="ma_ku"
          props={formik}
          textCase="uppercase"
          maxLength={20}
        />
        <ZenField
          width="12"
          readOnly={!permission}
          label={"cadmku.so_ku"}
          defaultlabel="Số khế ước"
          name="so_ku"
          props={formik}
        />
      </Form.Group>
      <Form.Group>
        <ZenField
          width="16"
          readOnly={!permission}
          label={"cadmku.ten_ku"}
          defaultlabel="Tên khế ước"
          name="ten_ku"
          props={formik}
        />
      </Form.Group>
      <Form.Group>
        <ZenFieldDate
          width="4"
          readOnly={!permission}
          label={"cadmku.ngay_ky"}
          defaultlabel="Ngày ký"
          name="ngay_ky"
          props={formik}
        />
        <ZenFieldDate
          width="4"
          readOnly={!permission}
          label={"cadmku.ngay_vay"}
          defaultlabel="Ngày vay"
          name="ngay_vay"
          onChange={handleChangeNgayvay}
          props={formik}
        />
      </Form.Group>
      <Form.Group>
        <ZenFieldNumber
          width="4"
          readOnly={!permission}
          label={"cadmku.thoi_han"}
          defaultlabel="Thời gian vay"
          name="thoi_han"
          onValueChange={handleChangeThoiHan}
          props={formik}
        />
        <ZenFieldSelect
          width="4"
          readOnly={!permission}
          options={options_th}
          label={"cadmku.tinh_theo"}
          defaultlabel="Tính theo"
          name="tinh_theo"
          onSelectedItem={handleChangeTinhTheo}
          props={formik}
        />
        <ZenFieldDate
          width="4"
          readOnly
          label={"cadmku.ngay_dh"}
          defaultlabel="Ngày đáo hạn"
          name="ngay_dh"
          props={formik}
        />
        <ZenFieldNumber
          decimalScale={2}
          width="4"
          readOnly={!permission}
          label={"cadmku.lai_suat"}
          defaultlabel="Lãi suất"
          {...changeLabel()}
          name="lai_suat"
          props={formik}
        />
      </Form.Group>
      <Form.Group>
        <ZenFieldSelect
          width="8"
          readOnly={!permission}
          options={options_pp}
          label={"cadmku.phuong_phap"}
          defaultlabel="Phương pháp tính lãi"
          name="phuong_phap"
          props={formik}
          clearable={false}
        />
        <ZenFieldSelect
          width="4"
          readOnly={!permission}
          options={options_ttg}
          label={"cadmku.ky_tt_goc"}
          defaultlabel="Kỳ thanh toán gốc"
          name="ky_tt_goc"
          props={formik}
          clearable={false}
        />
        <ZenFieldSelect
          width="4"
          readOnly={!permission}
          options={options_ttl}
          label={"cadmku.ky_tt_lai"}
          defaultlabel="Kỳ thanh toán lãi"
          name="ky_tt_lai"
          props={formik}
          clearable={false}
        />
      </Form.Group>
      <Form.Group>
        <ZenFieldSelectApi
          width="4"
          readOnly={!permission}
          lookup={ZenLookup.Ma_nt}
          label={"cadmku.ma_nt"}
          defaultlabel="Mã NT"
          name="ma_nt"
          formik={formik}
        />
        <ZenFieldNumber
          decimalScale={2}
          width="4"
          readOnly={!permission}
          label={"cadmku.tien_nt"}
          defaultlabel="Tiền khế ước NT"
          name="tien_nt"
          props={formik}
        />
        <ZenFieldNumber
          width="4"
          readOnly={!permission}
          label={"cadmku.tien"}
          defaultlabel="Tiền khế ước"
          name="tien"
          props={formik}
        />
      </Form.Group>
      <Form.Group>
        <ZenFieldSelectApi
          width="4"
          readOnly={!permission}
          lookup={{
            ...ZenLookup.TK,
            onLocalWhere: (items) => {
              return items.filter((t) => t.chi_tiet == true);
            },
          }}
          label={"cadmku.tk_vay"}
          defaultlabel="Tài khoản vay"
          name="tk_vay"
          formik={formik}
        />

        <ZenFieldSelectApi
          width="4"
          readOnly={!permission}
          lookup={{
            ...ZenLookup.TK,
            onLocalWhere: (items) => {
              return items.filter((t) => t.chi_tiet == true);
            },
          }}
          label={"cadmku.tk_cp"}
          defaultlabel="Tài khoản chi phí"
          name="tk_cp"
          formik={formik}
        />

        <ZenFieldSelectApi
          width="8"
          readOnly={!permission}
          lookup={{
            ...ZenLookup.TK,
            onLocalWhere: (items) => {
              return items.filter((t) => t.chi_tiet == true);
            },
          }}
          label={"cadmku.tk_cp_pt"}
          defaultlabel="Tài khoản chi phí phải trả"
          name="tk_cp_pt"
          formik={formik}
        />
      </Form.Group>
      <Form.Group>
        <ZenFieldSelectApi
          width="8"
          readOnly={!permission}
          lookup={{
            ...ZenLookup.Ma_hd,
            onLocalWhere: (items) => {
              return items.filter((t) => t.moduleid == "SO");
            },
          }}
          label={"cadmku.ma_hd"}
          defaultlabel="Mã hợp đồng"
          name="ma_hd"
          formik={formik}
        />

        <ZenFieldSelectApi
          width="8"
          readOnly={!permission}
          lookup={{
            ...ZenLookup.Ma_ngh,
            onLocalWhere: (items) => {
              return items.filter((t) => t.moduleid == "SO");
            },
          }}
          label={"cadmku.ten_hd"}
          defaultlabel="Mã ngân hàng"
          name="ten_hd"
          formik={formik}
        />
      </Form.Group>
      <Form.Group>
        <ZenField
          width="16"
          readOnly={!permission}
          label={"cadmku.ghi_chu"}
          defaultlabel="Ghi chú"
          name="ghi_chu"
          props={formik}
        />
      </Form.Group>
    </React.Fragment>
  );
};

const CaDmKu = {
  FormModal: CaDmKuMoDal,
  header: "Khế ước",
  api: {
    url: ApiCaDmKu,
  },
  permission: {
    view: "",
    add: "",
    edit: "",
  },
  formId: "CaDmKu-form",
  size: "large",
  initItem: {
    ma_ku: "",
    ten_ku: "",
  },
  formValidation: [
    {
      ...ValidError.formik(
        { name: "ma_ku", type: "string" },
        { type: "required", intl: IntlFormat.default(ValidError.Required) },
        {
          type: "max",
          intl: IntlFormat.default(ValidError.MaxLength),
          params: [20],
        }
      ),
    },
    {
      ...ValidError.formik(
        { name: "ten_ku", type: "string" },
        { type: "required", intl: IntlFormat.default(ValidError.Required) }
      ),
    }
  ],
};

export { CaDmKu };
