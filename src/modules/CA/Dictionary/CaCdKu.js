import React, { useEffect } from "react";
import {
  ZenField,
  ZenFieldDate,
  ZenFieldSelectApi,
  ZenFieldSelect,
  ZenFieldNumber,
  ZenMessageAlert,
} from "../../../components/Control/index";
import { Form, Tab } from "semantic-ui-react";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { ValidError } from "../../../utils/language/variable";
import { IntlFormat } from "../../../utils/intlFormat";
import { ApiCaCdKu } from "../Api/ApiCaCdKu";
import { FormMode, ZenHelper } from "../../../utils";
import { GlobalStorage, KeyStorage } from "../../../utils/storage";

const CaCdKuMoDal = (props) => {
  const { formik, permission, mode } = props;
  useEffect(() => {
    if (mode === FormMode.ADD) {
      formik.setFieldValue("nam", GlobalStorage.get('nam_sodu') || ZenHelper.getFiscalYear());
    }
  }, []);

  const handleSelectedNhvt = (item) => {
    formik.setFieldValue("ngay_ku", item.ngay_vay);
    formik.setFieldValue("ma_nt", item.ma_nt);
  }

  return (
    <React.Fragment>
      <Form.Group>
        <ZenField
          required
          width="4"
          readOnly={permission}
          maxLength={4}
          label={"cacdku.nam"}
          defaultlabel="Năm"
          name="nam"
          props={formik}
        />
        <ZenFieldSelectApi
          loadApi
          required
          width="12"
          readOnly={!permission || (mode === FormMode.EDIT ? permission : !permission)}
          lookup={{ ...ZenLookup.CaDmKu }}
          label={"cacdku.ma_ku"}
          textcase="uppercase"
          name="ma_ku"
          defaultlabel="Mã khế ước"
          formik={formik}
          onItemSelected={handleSelectedNhvt}
        />
      </Form.Group>
      <Form.Group>
        <ZenFieldSelectApi
          width="16"
          required
          loadApi
          lookup={{
            ...ZenLookup.TK,
            onLocalWhere: (items) => {
              return items.filter((t) => t.chi_tiet == true);
            },
          }}
          tabIndex={1}
          label={"cacdku.tk"}
          defaultlabel="Mã TK"
          name="tk"
          formik={formik}
          readOnly={!permission}
        />
      </Form.Group>
      <Form.Group>
        <ZenFieldDate
          width="8"
          required
          readOnly={permission}
          label={"cacdku.ngay_ku"}
          defaultlabel="Ngày khế ước"
          name="ngay_ku"
          props={formik}
        />
        <ZenFieldDate
          width="8"
          required
          readOnly={!permission}
          label={"cacdku.ngay_tt"}
          defaultlabel="Ngày thanh toán"
          name="ngay_tt"
          props={formik}
        />
      </Form.Group>
      <Form.Group>
        <ZenFieldSelectApi
          width="8"
          required
          readOnly={permission}
          lookup={ZenLookup.Ma_nt}
          label={"cacdku.ma_nt"}
          defaultlabel="Mã NT"
          name="ma_nt"
          formik={formik}
        />
      </Form.Group>
      <Form.Group>
        {formik.values.ma_nt != "VND" ? <ZenFieldNumber
          width="8"
          readOnly={!permission}
          label={"cacdku.da_vay_nt"}
          defaultlabel={`Tiền đã vay NT  ${formik.values.ma_nt !== "VND" ? formik.values.ma_nt : ""}`}
          name="da_vay_nt"
          props={formik}
        /> : null}
        <ZenFieldNumber
          width="8"
          readOnly={!permission}
          label={"cacdku.da_vay"}
          defaultlabel="Tiền đã vay VND"
          name="da_vay"
          props={formik}
        />
      </Form.Group>
      <Form.Group>
        {formik.values.ma_nt != "VND" ? <ZenFieldNumber
          width="8"
          readOnly={!permission}
          label={"cacdku.da_tt_nt"}
          defaultlabel={`Tiền đã thanh toán NT ${formik.values.ma_nt !== "VND" ? formik.values.ma_nt : ""}`}
          name="da_tt_nt"
          props={formik}
        /> : null}
        <ZenFieldNumber
          width="8"
          readOnly={!permission}
          label={"cacdku.da_tt"}
          defaultlabel="Tiền đã thanh toán VND"
          name="da_tt"
          props={formik}
        />
      </Form.Group>
    </React.Fragment>
  );
};

const onBeforeSave = (item) => {
  const data = GlobalStorage.getByField(KeyStorage.CacheData, "sidmnamtc")?.data
  const namtc = data.filter(d => d.nam === item.nam)[0]
  if (namtc.ngay_ks && namtc.ngay_ks >= namtc.ngay_dntc) {
    ZenMessageAlert.warning(`Năm tài chính ${item.nam} đã khóa số liệu, ngày khóa: ${ZenHelper.formatDateTime(namtc.ngay_ks, 'DD/MM/YYYY')} `)
    return
  } else {
    return item
  }
}

const AddItemFooter = (items) => {
  let nam_sodu = items.nam ? items.nam : null;
  const data = GlobalStorage.getByField(KeyStorage.CacheData, "sidmnamtc")?.data
  const namtc = data.filter(d => d.nam === nam_sodu)[0]
  if (namtc) {
    if (namtc.ngay_ks && namtc.ngay_ks >= namtc.ngay_dntc) {
      return <div style={{ float: "left", padding: "7px 0px" }}>
        <label style={{ color: "red", fontWeight: "bold" }}>Năm tài chính {nam_sodu}, khóa sổ ngày: {ZenHelper.formatDateTime(namtc.ngay_ks, 'DD/MM/YYYY')}</label>
      </div>
    }
  }
  return
}

const CaCdKu = {
  FormModal: CaCdKuMoDal,
  AddItemFooter,
  onBeforeSave,
  header: "Số dư khế ước",
  api: {
    url: ApiCaCdKu,
    params: ["nam", "ma_ku", "ma_nt"],
  },
  permission: {
    view: "",
    add: "",
    edit: "",
  },
  formId: "CaCdKu-form",
  size: "small",
  initItem: {
    ma_ku: "",
    ma_nt: "VND",
    nam: "",
  },
  formValidation: [
    {
      id: "nam",
      validationType: "string",
      validations: [
        {
          type: "required",
          params: ["Không được bỏ trống trường này"]
        },
      ]
    },
    {
      id: "ma_ku",
      validationType: "string",
      validations: [
        {
          type: "required",
          params: ["Không được bỏ trống trường này"]
        },
      ]
    },
    {
      id: "ngay_ku",
      validationType: "date",
      validations: [
        {
          type: "required",
          params: ["Không được bỏ trống trường này"]
        },
      ]
    },
    {
      id: "ngay_tt",
      validationType: "date",
      validations: [
        {
          type: "required",
          params: ["Không được bỏ trống trường này"]
        },
      ]
    },
    {
      id: "tk",
      validationType: "string",
      validations: [
        {
          type: "required",
          params: ["Không được bỏ trống trường này"]
        },
      ]
    },
    {
      id: "ma_nt",
      validationType: "string",
      validations: [
        {
          type: "required",
          params: ["Không được bỏ trống trường này"],
        },
        {
          type: "max",
          params: [3, "Không được vượt quá 3 kí tự"],
        },
      ],
    },
  ],
};

export { CaCdKu };
