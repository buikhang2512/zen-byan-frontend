import { CAVchCA1List, CAVchCA1Edit } from './CAVchCA1/index'
import { CAVchCA2List, CAVchCA2Edit } from './CAVchCA2/index'
import { CAVchCA3List, CAVchCA3Edit } from './CAVchCA3/index'
import { CAVchCA4List, CAVchCA4Edit } from './CAVchCA4/index'

export const CA_Voucher = {
    CA1: CAVchCA1List,
    CA2: CAVchCA2List,
    CA3: CAVchCA3List,
    CA4: CAVchCA4List,
}
export const CA_VoucherEdit = {
    CA1: CAVchCA1Edit,
    CA2: CAVchCA2Edit,
    CA3: CAVchCA3Edit,
    CA4: CAVchCA4Edit,
}