import React, { useRef } from "react";
import { Button, Form, Modal } from "semantic-ui-react";
import {
   ZenButton,
   ZenDatePeriod,
   ZenField,
   ZenFieldSelectApi,
   ZenFormik
} from "../../../../components/Control";
import * as routes from "../../../../constants/routes";
import { ZenHelper } from "../../../../utils/global";
import { ZenLookup } from "../../../ComponentInfo/Dictionary/ZenLookup";
import { ApiCAVchCA1 } from "../../Api/index";
import { GlobalStorage, KeyStorage } from "../../../../utils/storage";
import * as permissions from "../../../../constants/permissions";

const styles = {
    label: {
        marginTop: "0.14285714em",
        marginBottom: "0.14285714em"
    }
}

const FindAdvanced = ({ onClose, onSearch }) => {
   const globalStorage = GlobalStorage.get(KeyStorage.Global) || {};
   const initItem = {
      ngay1: globalStorage.from_date,
      ngay2: globalStorage.to_date,
   }
   const refFormik = useRef();

   const handleSearch = (values) => {
      const _sql = convertToSql(values);
      onSearch(_sql, values);
   };

   const handleChangeDate = ({ startDate, endDate }) => {
      refFormik.current.setValues({
         ...refFormik.current.values,
         ngay1: startDate,
         ngay2: endDate,
      });
   };


   return (
      <>
         <Modal.Header>Tìm kiếm</Modal.Header>
         <Modal.Content>
            <ZenFormik
               ref={refFormik}
               //validation={[]}
               initItem={initItem}
               onSubmit={handleSearch}
            >
               {(formikProps) => {
                  const { values } = formikProps;
                  return (
                     <Form>
                        <ZenDatePeriod
                           onChange={handleChangeDate}
                           value={[values.ngay1, values.ngay2]}
                           textLabel="Từ ngày - đến ngày"
                           defaultPopupYear={ZenHelper.getFiscalYear()}
                        />
                        <Form.Group widths="equal">
                           <ZenField
                              label={"ca1.so_ct1"}
                              defaultlabel="Số chứng từ"
                              name="so_ct1"
                              props={formikProps}
                           />
                           <ZenField
                              label={"ca1.so_ct2"}
                              defaultlabel="Đến số"
                              name="so_ct2"
                              props={formikProps}
                           />
                        </Form.Group>
                        <ZenFieldSelectApi
                           loadApi
                           lookup={{ ...ZenLookup.Ma_kh, format: "{ten_kh}" }}
                           label={"ca1.ma_kh"} defaultlabel="Khách hàng"
                           name="ma_kh" formik={formikProps}
                        />
                        <ZenField
                           label={"ca1.dien_giai"}
                           defaultlabel="Diễn giải"
                           name="dien_giai"
                           props={formikProps}
                        />
                     </Form>
                  );
               }}
            </ZenFormik>
         </Modal.Content>
         <Modal.Actions>
            <ZenButton btnType={"cancel"} size="small" onClick={onClose} />
            <Button
               content="Tìm"
               icon="search"
               size="small"
               primary
               onClick={(e) => refFormik.current.handleSubmit(e)}
            />
         </Modal.Actions>
      </>
   );
};

function convertToSql(item) {
   let _sql = "";
   if (item.ngay1) _sql += ` AND ngay_ct >= '${item.ngay1}'`;
   if (item.ngay2) _sql += ` AND ngay_ct <= '${item.ngay2}'`;
   if (item.so_ct1) _sql += ` AND so_ct >= '${item.so_ct1}'`;
   if (item.ma_kh) _sql += ` AND ma_kh = '${item.ma_kh}'`;
   if (item.so_ct2) _sql += ` AND so_ct <= '${item.so_ct2}'`;
   if (item.dien_giai) _sql += ` AND dien_giai LIKE '%${item.dien_giai}%'`;
   return _sql.replace("AND", "");
}

export const CAVchCA1List = {
   route: routes.CAVchCA1,

   action: {
      view: { visible: true, permission: permissions.CA1Xem },
      add: { visible: true, permission: permissions.CA1Them, link: { route: routes.CAVchCA1New } },
      edit: { visible: true, permission:permissions.CA1Sua, link: { route: routes.CAVchCA1Edit(), params: "stt_rec" } },
      del: { visible: true, permission: permissions.CA1Xoa }
   },

   linkHeader: {
      id: "cavchca1",
      defaultMessage: "Phiếu thu",
      active: true
   },

   tableList: {
      findComponent: {
         FindForm: FindAdvanced,
         convertToSql: convertToSql,
         labels: [
            { name: "ngay1", text: "Từ ngày", type: "date" },
            { name: "ngay2", text: "Đến ngày", type: "date" },
            { name: "ma_kh", text: "Khách hàng", type: "string" },
            { name: "so_ct1", text: "Từ số" },
            { name: "so_ct2", text: "Đến số" },
            { name: "dien_giai", text: "Diễn giải" },
         ],

      },

      unPagination: false,
      fieldCode: "stt_rec",
      ma_ct: 'CA1',

      api: {
         url: ApiCAVchCA1,
         type: "sql",
      },
      columns: [
         { id: "ca1.ngay_ct", defaultMessage: "Ngày CT", fieldName: "ngay_ct", type: "date", filter: "date", sorter: true, },
         { id: "ca1.so_ct", defaultMessage: "Số CT", fieldName: "so_ct", filter: "string", sorter: true, link: { route: routes.CAVchCA1Edit(), params: "stt_rec" }, propsCell: { singleLine: true } },
         { id: "ca1.t_tien", defaultMessage: "Tổng TT", fieldName: "t_tien", type: "number", filter: "number", sorter: true, },
         { id: "ca1.ma_nt", defaultMessage: "Mã NT", fieldName: "ma_nt", filter: "string", sorter: true, },
         { id: "ca1.tk_no", defaultMessage: "Tài khoản thu", fieldName: "ten_tk_no", filter: "string", sorter: true, },
         { id: "ca1.ma_kh", defaultMessage: "Mã KH", fieldName: "ma_kh", filter: "string", sorter: true, },
         { id: "ca1.ten_kh", defaultMessage: "Tên KH", fieldName: "ten_kh", filter: "string", sorter: true, },
         { id: "ca1.dien_giai", defaultMessage: "Diễn giải", fieldName: "dien_giai", filter: "string", sorter: true, },
      ],
   },
}