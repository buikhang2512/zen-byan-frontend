import React, { useContext, useState } from "react";
import { Grid, Table } from "semantic-ui-react";
import { ZenField, ZenFieldDate, ZenFieldSelectApi } from "../../../../components/Control/index";
import { auth, FormMode, ZenHelper } from "../../../../utils";
import { ApiCAVchCA1 } from "../../Api/index";
import {
   ContextVoucher, VoucherHelper,
   RowItemPH, NumberCell, TextCell, DeleteCell, RowHeaderCell, ActionType, SelectCell, TableScroll, TableTotalPH, RowTotalPH, DropDownCell
} from "../../../../components/Control/zVoucher";
import { ZenLookup } from "../../../ComponentInfo/Dictionary/ZenLookup";
import * as routes from "../../../../constants/routes";
import { ConfigBySiDmCt } from "../../../../components/Control/zVoucher/ZenVoucherHelper";
import { GlobalStorage, KeyStorage } from "../../../../utils/storage";
import * as permissions from "../../../../constants/permissions";

const PHForm = ({ formik, permission, modeForm, FieldNT }) => {
   const { ct, onUpdatePHCT } = useContext(ContextVoucher)
   let isReadOnly = VoucherHelper.f_ReadOnly(modeForm, permission)

   function getTyGia(ma_nt, ngay_ct) {
      let date_ct = ngay_ct ? ngay_ct : null
      let newArr = [];
      const getLocalTyGia = GlobalStorage.getByField(KeyStorage.CacheData, 'sidmtgnt').data
      newArr = getLocalTyGia.filter(i => i.ma_nt === ma_nt)
      if (newArr.length) {
         if (date_ct) {
            var findDateClosest = newArr.filter(i => ZenHelper.formatDateTime(i.ngay_tg, 'YYYY-MM-DD') <= date_ct)
            if (!findDateClosest || findDateClosest.length === 0) return 1;
            if (findDateClosest.length < 2) return findDateClosest[0].ty_gia;
            if (findDateClosest.length > 1) return findDateClosest[findDateClosest.length - 1].ty_gia
         } else { return null }
      }
      return null
   }

   function PHCT(ma_nt, ngay_ct) {
      const tygia = ma_nt === "VND" ? 1 : getTyGia(ma_nt, ngay_ct);
      const totalPH = {
         ma_nt: ma_nt,
         ty_gia: tygia,
         ngay_ct: ngay_ct,
         t_tien: 0,
         t_tien_nt: 0
      }
      const newCT = ct.map(item => {
         // tính CT
         if (ma_nt === 'VND') {
            item.ps_co = Math.round(item.ps_co_nt)
            item.ps_co_nt = item.ps_co
         } else {
            item.ps_co = Math.round(item.ps_co_nt * tygia)
         }

         // tính PH
         totalPH.t_tien_nt += VoucherHelper.f_NullToZero(item.ps_co_nt)
         totalPH.t_tien += VoucherHelper.f_NullToZero(item.ps_co)
         return item
      });
      onUpdatePHCT(totalPH, newCT)
   }
   const handleChangeDatect = (e) => {
      const { ma_nt } = formik.values
      PHCT(ma_nt, e.value)
   }

   const handleChangeNT = ({ ma_nt, ty_gia }) => {
      const totalPH = {
         ma_nt: ma_nt,
         ty_gia: ty_gia,
         t_tien: 0,
         t_tien_nt: 0
      }
      const newCT = ct.map(item => {
         // tính CT
         if (ma_nt === 'VND') {
            item.ps_co = Math.round(item.ps_co_nt)
            item.ps_co_nt = item.ps_co
         } else {
            item.ps_co = Math.round(item.ps_co_nt * ty_gia)
         }

         // tính PH
         totalPH.t_tien_nt += VoucherHelper.f_NullToZero(item.ps_co_nt)
         totalPH.t_tien += VoucherHelper.f_NullToZero(item.ps_co)
         return item
      });
      onUpdatePHCT(totalPH, newCT)
   }

   const handleSelectedLookup = (itemSelected, { name }) => {
      if (name === 'ma_kh') {
         formik.setFieldValue('dia_chi', itemSelected.dia_chi)
         formik.setFieldValue('nguoi_gd', itemSelected.nguoi_gd)
         formik.setFieldValue('ten_kh', itemSelected.ten_kh)
      }
      if (name == 'tk_no') {
         console.log(itemSelected)
         formik.setFieldValue('ten_tk_no', itemSelected.ten_tk)
      }
   }

   return <React.Fragment>
      <Grid columns="3">
         <RowItemPH content={[
            {
               width: 6,
               input: <ZenFieldSelectApi required tabIndex={1}
                  loadApi initFromLocal
                  lookup={{ ...ZenLookup.Ma_kh, format: "{ten_kh}" }}
                  label={"ardmkh.header"} defaultlabel="Khách hàng"
                  name="ma_kh" formik={formik}
                  onItemSelected={handleSelectedLookup}
                  readOnly={isReadOnly}
               />
            },
            {
               width: 7,
               input: <ZenField readOnly={isReadOnly} tabIndex={1}
                  label={"ca1.dia_chi"} defaultlabel="Địa chỉ"
                  name="dia_chi" props={formik}
               />
            },
            {
               width: 3,
               input: <ZenField required readOnly={isReadOnly} tabIndex={3}
                  label={"ca1.so_ct"} defaultlabel="Số chứng từ"
                  name="so_ct" props={formik}
               />
            },
         ]} />

         <RowItemPH content={[
            {
               width: 6,
               input: <ZenField readOnly={isReadOnly} tabIndex={1}
                  label={"ca1.nguoi_gd"} defaultlabel="Người giao dịch"
                  name="nguoi_gd" props={formik}
               />
            },
            {
               width: 7,
               input: <ZenField readOnly={isReadOnly} tabIndex={1}
                  label={"ca1.dien_giai"} defaultlabel="Diễn giải"
                  name="dien_giai" props={formik}
                  onBlur={(e) => {
                     if (FormMode.VIEW === modeForm) return

                     const newCT = ct.map(item => {
                        if (item.dien_giai === '') {
                           item.dien_giai = e.target.value
                        }
                        return item
                     })
                     onUpdatePHCT({}, newCT)
                  }}
               />
            },
            {
               width: 3,
               input: <ZenFieldDate required readOnly={isReadOnly} tabIndex={3}
                  label={"ca1.ngay_ct"} defaultlabel="Ngày chứng từ"
                  name="ngay_ct" props={formik}
                  onChange={handleChangeDatect}
               />
            },
         ]} />

         <RowItemPH content={[
            {
               width: 6,
               input: <ZenFieldSelectApi required loadApi
                  lookup={{
                     ...ZenLookup.TK,
                     where: `tk like '11%'`,
                     onLocalWhere: (items) => {
                        return items.filter(t => { return t.tk.indexOf("11") == 0 })
                     }
                  }}
                  tabIndex={1}
                  label={"ca1.tk_no"} defaultlabel="Tài khoản thu"
                  name="tk_no" formik={formik}
                  readOnly={isReadOnly}
                  onItemSelected={handleSelectedLookup}
               />
            },
            { width: 7, input: undefined },
            {
               width: 3, input: <FieldNT formik={formik} onChangeNT={handleChangeNT} tabIndex={3} />
            },
         ]} />
      </Grid>
   </React.Fragment>
}

const CTForm = ({ ph, ct, permission, modeForm }) => {
   const { onChangeCT, errorCT, getItemFormik, itemSiDmCt } = useContext(ContextVoucher)
   let isReadOnly = VoucherHelper.f_ReadOnly(modeForm, permission)

   const switchAction = (propsElement, type, itemCurrent, index) => {
      const { name, value, itemSelected } = propsElement
      const currentPH = getItemFormik().values

      switch (type) {
         case ActionType.TEXT_CHANGE:
            const { lookup } = propsElement
            // set tên nếu là lookup
            // => cần để khởi tạo item cho input lookup, nếu hiển thị nhìu hơn 1 trường mã
            if (lookup) {
               if (name === 'ma_kh') {
                  itemCurrent['ten_kh'] = itemSelected['ten_kh']
               } else if (name === 'tk') {
                  itemCurrent['ten_tk'] = itemSelected['ten_tk']
               }
            }
            // set value
            itemCurrent[name] = value
            onChangeCT(itemCurrent, index, type, {}, propsElement)
            break;

         case ActionType.NUMBER_CHANGE:
            f_calcNumber(propsElement, type, itemCurrent, index)
            break;

         case ActionType.ADD_ROW:
            let newCT = ({
               tk: value, ten_tk: itemSelected.ten_tk,
               ps_co: 0, ps_co_nt: 0,
            })
            if (ct.length === 0) {
               newCT.ma_kh = currentPH.ma_kh
               newCT.dien_giai = currentPH.dien_giai
            }
            onChangeCT(newCT, null, type)
            break;

         case ActionType.DELETE_ROW:
            // xóa dòng CT, tính lại PH
            const totalPH = {
               t_tien: currentPH.t_tien - VoucherHelper.f_NullToZero(itemCurrent.ps_co),
               t_tien_nt: currentPH.t_tien_nt - VoucherHelper.f_NullToZero(itemCurrent.ps_co_nt)
            }
            onChangeCT(itemCurrent, index, type, totalPH)
            break;
         default:
            console.log(type)
            break;
      }
   }

   function f_calcNumber(propsElement, type, itemCurrent, index) {
      const currentPH = getItemFormik().values
      const { name, value } = propsElement

      if (itemCurrent[name] !== value) {
         itemCurrent[name] = value

         if (currentPH.ma_nt !== 'VND') {
            if (name === 'ps_co_nt') {
               itemCurrent['ps_co'] = Math.round(value * currentPH.ty_gia)
            } else if (name === 'ps_co') {
               itemCurrent['ps_co_nt'] = Number((value / currentPH.ty_gia).toFixed(2))
            }
         } else {
            itemCurrent['ps_co_nt'] = value
         }

         // cập nhật PH, CT
         VoucherHelper.f_Timeout(() => {
            const totalPH = {
               t_tien: 0,
               t_tien_nt: 0
            }
            ct.forEach((item, idx) => {
               if (index === idx) {
                  totalPH.t_tien_nt += VoucherHelper.f_NullToZero(itemCurrent.ps_co_nt)
                  totalPH.t_tien += VoucherHelper.f_NullToZero(itemCurrent.ps_co)
               } else {
                  totalPH.t_tien_nt += VoucherHelper.f_NullToZero(item.ps_co_nt)
                  totalPH.t_tien += VoucherHelper.f_NullToZero(item.ps_co)
               }
            })
            onChangeCT(itemCurrent, index, type, totalPH, propsElement)
         })
      }
   }

   return <TableScroll>
      <Table.Header>
         <RowHeaderCell colAction={isReadOnly}
            itemSiDmCt={itemSiDmCt}
            header={[
               { text: ['ca1.tk_co', 'Khoản thu'] },
               { text: ['ca1.ps_co', 'PS có'], maNT: ph.ma_nt, isNT: true },
               { text: ['ca1.ps_co', 'PS có'], maNT: 'VND' },
               { text: ['ardmkh.header', 'Khách hàng'] },
               { text: ['ca1.dien_giai', 'Diễn giải'] },
            ]}
         />
      </Table.Header>
      <Table.Body>
         {
            ct && ct.map((item, index) => {
               const error = errorCT ? errorCT.find(x => x.index === index) : undefined

               return <Table.Row key={item.stt_rec0 ? item.stt_rec0 : ('REC' + index)}>
                  <DropDownCell lookup={{
                     ...ZenLookup.TK,
                     where: 'chi_tiet = 1',
                     onLocalWhere: (items) => {
                        return items.filter(t => t.chi_tiet == true)
                     }
                  }} error={error}
                     name="tk" rowItem={item} index={index}
                     placeholder={['ca1.tk_co', 'Khoản thu']}
                     readOnly={isReadOnly}
                     onChange={switchAction}
                  />

                  {ph.ma_nt !== "VND" && <NumberCell name="ps_co_nt"
                     rowItem={item} index={index} decimalScale={2}
                     placeholder={["ca1.ps_co", "PS có", { ma_nt: ph.ma_nt }]}
                     onChange={switchAction} error={error}
                     readOnly={isReadOnly}
                     // set focus khi thêm mới
                     autoFocus={(ph.ma_nt !== 'VND' && item.autoFocus && ct.length === index + 1) ? true : false}
                  />
                  }

                  <NumberCell name="ps_co"
                     rowItem={item} index={index}
                     placeholder={["ca1.ps_co", "PS có", { ma_nt: 'VND' }]}
                     onChange={switchAction} error={error}
                     readOnly={isReadOnly}
                     // set focus khi thêm mới
                     autoFocus={(ph.ma_nt === 'VND' && item.autoFocus && ct.length === index + 1) ? true : false}
                  />

                  <SelectCell
                     lookup={ZenLookup.Ma_kh}
                     name="ma_kh" rowItem={item} index={index}
                     placeholder={['ardmkh.header', 'Khách hàng']}
                     onChange={switchAction}
                     readOnly={isReadOnly}
                  />

                  <TextCell name="dien_giai"
                     rowItem={item} index={index}
                     placeholder={['ca1.dien_giai', 'Diễn giải']}
                     onChange={switchAction}
                     readOnly={isReadOnly}
                  />

                  <ConfigBySiDmCt
                     rowItem={item} index={index}
                     itemSiDmCt={itemSiDmCt}
                     readOnly={isReadOnly}
                     switchAction={switchAction}
                  />

                  {isReadOnly === false && <DeleteCell collapsing
                     rowItem={item} index={index}
                     onClick={switchAction} />}
               </Table.Row>
            })
         }

         {isReadOnly === false && <Table.Row>
            <SelectCell isAdd clearable={false} name="addRow"
               lookup={{
                  ...ZenLookup.TK,
                  where: 'chi_tiet = 1',
                  onLocalWhere: (items) => {
                     return items.filter(t => t.chi_tiet == true)
                  }
               }}
               placeholder={['ca1.tk_co', 'TK có']}
               onChange={switchAction}
            />
            <Table.Cell colSpan={5} />
         </Table.Row>}
      </Table.Body>
   </TableScroll>
}

const CTFormTotal = ({ ph }) => {
   return <Grid columns="2">
      <Grid.Column width="8" />
      <Grid.Column width="8" textAlign="right">
         <TableTotalPH maNt={ph.ma_nt}>
            <RowTotalPH text="Tổng tiền" value={ph.t_tien} valueNT={ph.t_tien_nt} maNt={ph.ma_nt} />
         </TableTotalPH>
      </Grid.Column>
   </Grid>
}

const onValidCT = (ph, ct, currentRowCT) => {
   const { item, index } = currentRowCT
   const errorField = {}
   if (!item.tk) {
      errorField.tk = ["Tài khoản không được để trống"]
   }

   if (!item.ps_co) {
      errorField.ps_co = ["Giá trị phải lớn hơn 0"]
   }

   if (ph.ma_nt !== 'VND' && !item.ps_co_nt) {
      errorField.ps_co_nt = ["Giá trị phải lớn hơn 0"]
   }
   return Object.keys(errorField).length > 0 ? errorField : undefined
}

export const CAVchCA1Edit = {
   PHForm: PHForm,
   CTForm: CTForm,
   CTFormTotal: CTFormTotal,
   onValidCT: onValidCT,
   ma_ct: 'CA1',
   formId: "CAVchCA1",

   route: {
      add: routes.CAVchCA1New,
      edit: routes.CAVchCA1Edit()
   },
   linkHeader: {
      id: "cavchca1",
      defaultMessage: "Phiếu thu",
      route: routes.CAVchCA1,
      active: false,
   },

   api: {
      url: ApiCAVchCA1,
   },
   action: {
      view: { visible: true, permission: permissions.CA1Xem },
      add: { visible: true, permission: permissions.CA1Them },
      edit: { visible: true, permission: permissions.CA1Sua },
      del: { visible: true, permission: permissions.CA1Xoa }
   },
   initItem: {
      stt_rec: "", so_ct: "", ma_ct: "CA1", ma_nt: "VND", ty_gia: 1,
      ngay_ct: ZenHelper.formatDateTime(new Date(), "YYYY-MM-DD"),
      ngay_lct: ZenHelper.formatDateTime(new Date(), "YYYY-MM-DD"),
      ma_kh: "", trang_thai: "",
      dia_chi: "", nguoi_gd: "", dien_giai: "",
      t_tien: null, t_tien_nt: null,
      tk_no: auth.getCacheStorage() ? auth.getCacheStorage().ma_ct.data[0].tk_no : "",
   },
   formValidation: [
      {
         id: "ngay_ct",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
         ]
      },
      {
         id: "so_ct",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
         ]
      },
      {
         id: "ma_kh",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
         ]
      },
      {
         id: "tk_no",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
         ]
      },
   ]
}
