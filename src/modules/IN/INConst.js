export const typePSCL = [{ key: '0', value: '0', text: 'Không tạo' },
{ key: '1', value: '1', text: 'Các trường hợp số lượng = 0, số tiền # 0' },
{ key: '2', value: '2', text: 'Các trường hợp có phát sinh chênh lệch' }
]

export const typeTinh = [
    { key: '0', text: 'Tính giá trung bình tháng', value: '0', },
    { key: '1', text: 'Tính giá bình quân di động', value: '1', },
    { key: '2', text: 'Tính giá nhập trước xuất trước', value: '2', }
]