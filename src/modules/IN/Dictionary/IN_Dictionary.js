import React, { useEffect, useState } from 'react';
import * as routes from "../../../constants/routes";
import { ApiINDmDvt,ApiINDMVT,ApiINDmNhvt,ApiINDmPlvt} from "../Api/index";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { ZenField, ZenLink } from "../../../components/Control";
import { Table } from "semantic-ui-react";
import * as permissions from "../../../constants/permissions"

const IN_Dictionary = {
   INDmDvt: {
      route: routes.INDmDvt,

      action: {
         view: { visible: true, permission: permissions.INDmDvtXem },
         add: { visible: true, permission: permissions.INDmVtThem },
         edit: { visible: true, permission: permissions.INDmDvtSua },
         del: { visible: true, permission: permissions.INDmDvtXoa },
      },

      linkHeader: {
         id: "indmdvt",
         defaultMessage: "Danh mục đơn vị tính",
         active: true,
         isSetting: true,
      },

      tableList: {
         keyForm: "InDmDvt",
         formModal: ZenLookup.Ma_dvt,
         fieldCode: "dvt",
         unPagination: false,
         duplicate: true,
         showFilterLabel: true,

         hasChecked: true,
         onDeleteBulk: (items, onAfterExec) => {
            setTimeout(() => {
               // items: các item đã chọn
               // onAfterExec(mảng các mã đã chọn: [code1,code2])
               onAfterExec(items.map(t => t.dvt))
            }, 500);
         },
         // onUpdateBulk: (items) => {

         // },

         api: {
            url: ApiINDmDvt,
            type: "sql",
         },
         columns: [
            { id: "indmdvt.dvt", defaultMessage: "Mã đơn vị tính", fieldName: "dvt", type: "string", filter: "string", sorter: true, editForm: true },
            { id: "indmdvt.ten_dvt", defaultMessage: "Tên đơn vị tính", fieldName: "ten_dvt", type: "string", filter: "string", sorter: true, },
            { id: "ksd", defaultMessage: "Không sử dụng", fieldName: "ksd", type: "bool", filter: "", sorter: true, collapsing: true, propsCell: { textAlign: "center" } },
         ],
      },
   },
   INDmPlvt: {
      route: routes.INDmPlvt,
      importExcel: true,
      action: {
         view: { visible: true, permission: permissions.INDmPlVtXem },
         add: { visible: true, permission: permissions.INDmPlVtThem },
         edit: { visible: true, permission: permissions.INDmPlVtSua },
         del: { visible: true, permission: permissions.INDmPlVtXoa },
      },

      linkHeader: {
         id: "indmplvt",
         defaultMessage: "Danh mục phân loại vật tư hàng hóa",
         active: true,
         isSetting: true,
      },

      tableList: {
         keyForm: "INDmPlvt",
         formModal: ZenLookup.Ma_plvt,
         fieldCode: "ma_plvt",
         unPagination: false,
         duplicate: true,
         showFilterLabel: true,

         api: {
            url: ApiINDmPlvt,
            type: "sql",
         },
         columns: [
            { id: "indmdvt.ma_plvt", defaultMessage: "Mã phân loại vật tư", fieldName: "ma_plvt", type: "string", filter: "string", sorter: true, editForm: true },
            { id: "indmdvt.ten_plvt", defaultMessage: "Tên phân loại vật tư", type: "string", fieldName: "ten_plvt", filter: "string", sorter: true, },
            { id: "indmdvt.loai", defaultMessage: "Kiểu", fieldName: "loai", type: "string", filter: "string", sorter: true, },
            { id: "ksd", defaultMessage: "Không sử dụng", fieldName: "ksd", type: "bool", filter: "", sorter: true, collapsing: true, propsCell: { textAlign: "center" } },
         ],
      },
   },
INDmNhvt: {
      route: routes.INDmNhvt,
      importExcel: true,
      action: {
         view: { visible: true, permission: permissions.INDmNhVtXem },
         add: { visible: true, permission: permissions.INDmNhVtThem },
         edit: { visible: true, permission: permissions.INDmNhVtSua },
         del: { visible: true, permission: permissions.INDmNhVtXoa },
      },

      linkHeader: {
         id: "indmnhvt",
         defaultMessage: "Danh mục nhóm vật tư hàng hóa",
         active: true,
         isSetting: true,
      },

      tableList: {
         keyForm: "InDmNhvt",
         formModal: ZenLookup.Ma_nhvt,
         fieldCode: "ma_nhvt",
         unPagination: false,
         duplicate: true,
         showFilterLabel: true,

         api: {
            url: ApiINDmNhvt,
            type: "sql",
         },
         columns: [
            { id: "indmdvt.ma_nhvt", defaultMessage: "Mã nhóm vật tư", fieldName: "ma_nhvt", type: "string", filter: "string", sorter: true, editForm: true },
            { id: "indmdvt.ten_nhvt", defaultMessage: "Tên nhóm vật tư", fieldName: "ten_nhvt", type: "string", filter: "string", sorter: true, custom: true },
            { id: "indmdvt.cap", defaultMessage: "Cấp", fieldName: "cap", type: "number", filter: "number", sorter: true, collapsing: true },
            { id: "indmdvt.nhom_me", defaultMessage: "Nhóm mẹ", fieldName: "nhom_me", type: "string", filter: "string", sorter: true, },
            { id: "indmdvt.tk_vt", defaultMessage: "TK VT", fieldName: "tk_vt", type: "string", filter: "string", sorter: true, },
            { id: "indmdvt.tk_dt", defaultMessage: "TK DT", fieldName: "tk_dt", type: "string", filter: "string", sorter: true, },
            { id: "indmdvt.tk_dtnb", defaultMessage: "TK DTNB", fieldName: "tk_dtnb", type: "string", filter: "string", sorter: true, },
            { id: "indmdvt.tk_gv", defaultMessage: "TK GV", fieldName: "tk_gv", type: "string", filter: "string", sorter: true, },
            { id: "indmdvt.tk_tl", defaultMessage: "TK HBTL", fieldName: "tk_tl", type: "string", filter: "string", sorter: true, },
            { id: "indmdvt.tk_ck", defaultMessage: "TK CK", fieldName: "tk_ck", type: "string", filter: "string", sorter: true, },
            { id: "ksd", defaultMessage: "Không sử dụng", fieldName: "ksd", type: "bool", filter: "", sorter: true, collapsing: true, propsCell: { textAlign: "center" } },
         ],

         customCols: (data, item, fieldName, { onEdit, ...restCol }) => {
            let customCell = undefined
            if (fieldName === 'ten_nhvt') {
               customCell = <Table.Cell key={fieldName} singleLine>
                  <p style={{
                     paddingLeft: `${Number(item.cap - 1) * 14}px`,
                     ...restCol.style,
                  }}
                     permission=""
                  >
                     {item.ten_nhvt}
                  </p>
               </Table.Cell>
            }
            return customCell
         }
      },
   },
INDmVT: {
      route: routes.INDMVT,
      importExcel: true,
      action: {
         view: { visible: true, permission: permissions.INDmVtXem },
         add: { visible: true, permission: permissions.INDmVtThem },
         edit: { visible: true, permission: permissions.INDmVtSua },
         del: { visible: true, permission: permissions.INDmVtXoa },
      },

      linkHeader: {
         id: "indmvt",
         defaultMessage: "Danh mục vật tư hàng hóa",
         active: true,
         isSetting: false,
      },

      tableList: {
         keyForm: "InDmVt",
         formModal: ZenLookup.Ma_vt,
         fieldCode: "ma_vt",
         unPagination: false,
         duplicate: true,
         showFilterLabel: true,

         api: {
            url: ApiINDMVT,
            type: "sql",
         },
         columns: [
            { id: "indmdvt.ma_vt", defaultMessage: "Mã vật tư", fieldName: "ma_vt", type: "string", filter: "string", sorter: true, editForm: true },
            { id: "indmdvt.ten_vt", defaultMessage: "Tên vật tư", fieldName: "ten_vt", type: "string", filter: "string", sorter: true, },
            { id: "indmdvt.dvt", defaultMessage: "ĐVT", fieldName: "dvt", type: "string", filter: "string", sorter: true, collapsing: true },
            { id: "indmdvt.ma_nhvt", defaultMessage: "Nhóm", fieldName: "ma_nhvt", type: "string", filter: "string", sorter: true, },
            { id: "indmdvt.ma_kho", defaultMessage: "Mã kho", fieldName: "ma_kho", type: "string", filter: "string", sorter: true, },
            { id: "indmdvt.ma_vitri", defaultMessage: "Mã vị trí", type: "string", fieldName: "ma_vitri", filter: "string", sorter: true, },
            { id: "indmdvt.tk_vt", defaultMessage: "TK VT", fieldName: "tk_vt", type: "string", filter: "string", sorter: true, },
            { id: "indmdvt.tk_dt", defaultMessage: "TK DT", fieldName: "tk_dt", type: "string", filter: "string", sorter: true, },
            { id: "indmdvt.tk_dtnb", defaultMessage: "TK DTNB", fieldName: "tk_dtnb", type: "string", filter: "string", sorter: true, },
            { id: "indmdvt.tk_gv", defaultMessage: "TK GV", fieldName: "tk_gv", type: "string", filter: "string", sorter: true, },
            { id: "indmdvt.tk_tl", defaultMessage: "TK HBTL", fieldName: "tk_tl", type: "string", filter: "string", sorter: true, },
            { id: "indmdvt.tk_ck", defaultMessage: "TK CK", fieldName: "tk_ck", type: "string", filter: "string", sorter: true, },
            { id: "ksd", defaultMessage: "Không sử dụng", fieldName: "ksd", type: "bool", filter: "", sorter: true, collapsing: true, propsCell: { textAlign: "center" } },
         ],
      },
   },
}

export default IN_Dictionary