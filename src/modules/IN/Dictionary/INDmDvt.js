import React from "react";
import { ZenField } from "../../../components/Control/index";
import { FormMode } from "../../../utils";
import { ApiINDmDvt } from "../Api";
import * as permissions from "../../../constants/permissions"

const INDmDvtModal = (props) => {
   const { formik, permission, mode } = props

   return <React.Fragment>
      <ZenField required autoFocus readOnly={!permission || (mode === FormMode.ADD ? false : true)}
         label={"indmdvt.dvt"} defaultlabel="Mã đơn vị tính"
         name="dvt" props={formik}
         textCase="uppercase" maxLength={8}
         isCode
      />
      <ZenField required readOnly={!permission}
         label={"indmdvt.ten_dvt"} defaultlabel="Tên đơn vị tính"
         name="ten_dvt" props={formik}
      />
   </React.Fragment>
}

const INDmDvt = {
   FormModal: INDmDvtModal,
   api: {
      url: ApiINDmDvt,
   },
   permission: {
      view: permissions.INDmDvtXem,
      add: permissions.INDmDvtThem,
      edit: permissions.INDmDvtSua
   },
   formId: "indmdvt-form",
   size: "tiny",
   initItem: {
      dvt: "",
      ten_dvt: "",
      ksd: false
   },
   formValidation: [
      {
         id: "dvt",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
            {
               type: "max",
               params: [8, "Không được vượt quá 8 kí tự"]
            },
         ]
      },
      {
         id: "ten_dvt",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
         ]
      },
   ]
}

export { INDmDvt };