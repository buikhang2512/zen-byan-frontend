import React, { memo, useEffect, useState } from "react";
import { ZenField, ZenFieldCheckbox, ZenFieldNumber, ZenFieldSelectApi } from "../../../components/Control/index";
import { FormMode } from "../../../utils";
import { Form, Menu, Segment } from "semantic-ui-react";
import { ApiINDMVT } from "../Api";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { optionsLoaiVT, optionsLoaiGiaTon, optionsTKKhoTheo } from './';
import * as permissions from "../../../constants/permissions"

const INDmVTModal = (props) => {
   const { formik, permission, mode } = props;
   const [state, setState] = React.useState({ activeItem: "detail" });
   const [tonKho, setTonKho] = useState(true);
   const { activeItem } = state
   const arr = ["1", "2", "3", "5"];

   const changeTonKho = () => {
      setTonKho(formik.values.sua_kh)
   }

   useEffect(() => {
      const { ton_kho } = formik.values;
      changeTonKho()
      if (tonKho) {
         formik.setFieldValue("ton_kho", ton_kho);
      }
   }, [formik.values.ma_kho])


   const handleItemClick = (e, { name }) => setState({ activeItem: name })

   const handleLoaiChange = (item) => {
      if (arr.indexOf(item.value) != -1) {
         formik.setFieldValue('ton_kho', true);
      } else {
         formik.setFieldValue('ton_kho', tonKho);
      }
   }

   const handleNhomChange = (item, { name }) => {
      formik.setFieldValue('tk_vt', item.tk_vt);
      formik.setFieldValue('tk_dt', item.tk_dt);
      formik.setFieldValue('tk_gv', item.tk_gv);
      formik.setFieldValue('tk_ck', item.tk_ck);
      formik.setFieldValue('tk_tl', item.tk_tl);
   }

   return <React.Fragment>
      <Form.Group>
         <ZenField required autoFocus readOnly={!permission || (mode === FormMode.ADD ? false : true)}
            width='4' textCase="uppercase"
            label={"indmvt.ma_vt"} defaultlabel="Mã vật tư"
            name="ma_vt" props={formik}
            isCode
         />
         <ZenField required readOnly={!permission}
            width='12'
            label={"indmvt.ten_vt"} defaultlabel="Tên vật tư"
            name="ten_vt" props={formik}
         />
      </Form.Group>
      <Form.Group widths='equal'>
         <ZenFieldSelectApi
            lookup={{ ...ZenLookup.Ma_nhvt, cols: 'ma_nhvt,ten_nhvt,tk_vt,tk_dt,tk_gv,tk_ck,tk_tl' }}
            label={"indmvt.ma_nhvt"} defaultlabel="Nhóm"
            name="ma_nhvt" formik={formik}
            readOnly={!permission}
            onItemSelected={handleNhomChange}
         />
         <ZenFieldSelectApi
            required
            lookup={{ ...ZenLookup.Ma_loai_vt, cols: 'id,mo_ta,ton_kho' }}
            label={"indmvt.loai"} defaultlabel="Loại vật tư"
            name="loai" formik={formik}
            readOnly={!permission}
            onItemSelected={handleLoaiChange}
         />
         <Form.Field>
            <label>&nbsp;</label>
            <ZenFieldCheckbox readOnly={!permission || arr.indexOf(formik.values.loai) != -1}
               label={"indmvt.ton_kho"} defaultlabel="Theo dõi tồn kho"
               name="ton_kho" props={formik}
            />
         </Form.Field>
      </Form.Group>

      <TabMenu activeItem={activeItem} handleItemClick={handleItemClick} />
      {activeItem === "detail" && <TabThongTin formik={formik} permission={permission} />}
      {activeItem === "tk" && <TabTaiKhoan formik={formik} permission={permission} />}
   </React.Fragment>
}

const TabMenu = memo(({ activeItem, handleItemClick }) => {
   return <Menu attached='top' tabular>
      <Menu.Item
         name='detail'
         active={activeItem === 'detail'}
         onClick={handleItemClick}
      >
         Thông tin chung
      </Menu.Item>
      <Menu.Item
         name='tk'
         active={activeItem === 'tk'}
         onClick={handleItemClick}
      >
         Tài khoản
      </Menu.Item>
   </Menu>
}, (prev, next) => {
   return prev.activeItem === next.activeItem
})

const TabThongTin = ({ permission, formik }) => {
   const handleItemSelected = (item, { name }) => {
      if (name === "ma_thue") {
         formik.setFieldValue('ts_gtgt', item.ts_gtgt);
      }
   }

   return <Segment>
      <Form.Group widths='equal'>
         <ZenFieldSelectApi
            lookup={{ ...ZenLookup.Ma_dvt }}
            label={"indmvt.dvt"} defaultlabel="ĐVT"
            name="dvt" formik={formik}
            readOnly={!permission}
         />
         <ZenFieldNumber readOnly={!permission}
            label={"indmnhvt.sl_min"} defaultlabel="Tồn tối thiểu"
            name="sl_min" props={formik}
            decimalScale={0}
         />
         <ZenFieldNumber readOnly={!permission}
            label={"indmnhvt.sl_max"} defaultlabel="Tồn tối đa"
            name="sl_max" props={formik}
            decimalScale={0}
         />
      </Form.Group>

      <Form.Group widths='equal'>
         <ZenFieldSelectApi
            lookup={{ ...ZenLookup.Ma_loai_giaton }}
            label={"indmvt.gia_ton"} defaultlabel="Phương pháp tính giá tồn"
            name="gia_ton" formik={formik}
            readOnly={!permission}
         />
         <ZenFieldSelectApi
            lookup={{ ...ZenLookup.Ma_kho }}
            label={"indmvt.ma_kho"} defaultlabel="Mã kho mặc định"
            name="ma_kho" formik={formik}
            readOnly={!permission}
         />
         <ZenFieldSelectApi
            lookup={{ ...ZenLookup.Ma_thue }}
            label={"indmvt.ma_thue"} defaultlabel="Mã thuế"
            name="ma_thue" formik={formik}
            readOnly={!permission}
            onItemSelected={handleItemSelected}
         />
      </Form.Group>
      <Form.Group widths="equal">
         <ZenFieldNumber readOnly={!permission}
            label={"indmnhvt.ts_gtgt"} defaultlabel="Thuế suất thuế GTGT"
            name="ts_gtgt" props={formik}
            decimalScale={0}
         />
         <ZenFieldNumber readOnly={!permission}
            label={"indmnhvt.ts_xk"} defaultlabel="Thuế suất thuế nhập khẩu"
            name="ts_xk" props={formik}
            decimalScale={0}
         />
         <ZenFieldNumber readOnly={!permission}
            label={"indmnhvt.ts_ttdb"} defaultlabel="Thuế suất thuế TTĐB"
            name="ts_ttdb" props={formik}
            decimalScale={0}
         />
      </Form.Group>
      <Form.Group widths="equal">
         <ZenFieldSelectApi upward
            lookup={{
               ...ZenLookup.Ma_plvt,
               onLocalWhere: (items) => {
                  return items?.filter(t => t.loai == "1") || []
               },
               where: 'loai = 1'
            }}
            label={"indmvt.ma_plvt1"} defaultlabel="Mã PLVT 1"
            name="ma_plvt1" formik={formik}
            readOnly={!permission}
         />
         <ZenFieldSelectApi upward
            lookup={{
               ...ZenLookup.Ma_plvt,
               onLocalWhere: (items) => {
                  return items?.filter(t => t.loai == "2") || []
               },
               where: 'loai = 2'
            }}
            label={"indmvt.ma_plvt2"} defaultlabel="Mã PLVT 2"
            name="ma_plvt2" formik={formik}
            readOnly={!permission}
         />
         <ZenFieldSelectApi upward
            lookup={{
               ...ZenLookup.Ma_plvt,
               onLocalWhere: (items) => {
                  return items?.filter(t => t.loai == "3") || []
               },
               where: 'loai = 3'
            }}
            label={"indmvt.ma_plvt3"} defaultlabel="Mã PLVT 3"
            name="ma_plvt3" formik={formik}
            readOnly={!permission}
         />
      </Form.Group>
   </Segment>
}
const TabTaiKhoan = ({ permission, formik }) => {
   return <Segment>
      <Form.Group widths="equal">
         {/* <ZenFieldSelect readOnly={!permission} options={optionsLoaiGiaTon}
label={"indmvt.gia_ton"} defaultlabel="Phương pháp tính giá tồn"
name="gia_ton" props={formik} clearable={false}
/> */}
         <ZenFieldSelectApi loadApi
            lookup={{
               ...ZenLookup.TK,
               onLocalWhere: (items) => {
                  return items.filter(t => t.chi_tiet == true)
               }
            }}
            label={"indmvt.tk_vt"} defaultlabel="TK vật tư"
            name="tk_vt" formik={formik}
            readOnly={!permission}
         />
      </Form.Group>

      <Form.Group widths="equal">
         <ZenFieldSelectApi loadApi upward
            lookup={{
               ...ZenLookup.TK,
               onLocalWhere: (items) => {
                  return items.filter(t => t.chi_tiet == true)
               }
            }}
            label={"indmvt.tk_dt"} defaultlabel="TK doanh thu"
            name="tk_dt" formik={formik}
            readOnly={!permission}
         />
         <ZenFieldSelectApi loadApi upward
            lookup={{
               ...ZenLookup.TK,
               onLocalWhere: (items) => {
                  return items.filter(t => t.chi_tiet == true)
               }
            }}
            label={"indmvt.tk_gv"} defaultlabel="TK giá vốn"
            name="tk_gv" formik={formik}
            readOnly={!permission}
         />
      </Form.Group>
      <Form.Group widths="equal">
         <ZenFieldSelectApi loadApi upward
            lookup={{
               ...ZenLookup.TK,
               onLocalWhere: (items) => {
                  return items.filter(t => t.chi_tiet == true)
               }
            }}
            label={"indmvt.tk_tl"} defaultlabel="TK trả lại"
            name="tk_tl" formik={formik}
            readOnly={!permission}
         />
         <ZenFieldSelectApi loadApi upward
            lookup={{
               ...ZenLookup.TK,
               onLocalWhere: (items) => {
                  return items.filter(t => t.chi_tiet == true)
               }
            }}
            label={"indmvt.tk_ck"} defaultlabel="TK chiết khấu"
            name="tk_ck" formik={formik}
            readOnly={!permission}
         />
      </Form.Group>
   </Segment>
}

const INDmVT = {
   FormModal: INDmVTModal,
   onValidate: (item) => {
      const errors = {};
      if (item.ton_kho) {
         if (item.gia_ton == "") {
            errors.gia_ton = 'Không được bỏ trống trường này';
         }
      }
      return errors
   },
   api: {
      url: ApiINDMVT,
   },
   permission: {
      view: permissions.INDmVtXem,
      add: permissions.INDmDvtThem,
      edit: permissions.INDmDvtSua,
   },
   formId: "INDmVT-form",

   initItem: {
      ma_vt: "", ten_vt: "", loai: "1",
      ma_nhvt: "", dvt: "", gia_ton: "1",
      tk_vt: "", tk_dt: "", tk_dtnb: "",
      tk_gv: "", tk_tl: "", tk_ck: "", ksd: false,
      ton_kho: true
   },
   formValidation: [
      {
         id: "ma_vt",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
            {
               type: "max",
               params: [20, "Không được vượt quá 20 kí tự"]
            },
         ]
      },
      {
         id: "ten_vt",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
         ]
      },
      {
         id: "loai",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
         ]
      },

   ]
}

export { INDmVT };