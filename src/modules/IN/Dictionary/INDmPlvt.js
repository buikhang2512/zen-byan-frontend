import React from "react";
import { ZenField, ZenFieldSelect } from "../../../components/Control/index";
import { FormMode } from "../../../utils";
import { ApiINDmPlvt } from "../Api";
import * as permissions from "../../../constants/permissions"

const optionsLoai = [
   { key: 1, value: "1", text: "1" },
   { key: 2, value: "2", text: "2" },
   { key: 3, value: "3", text: "3" }
]

const INDmPlvtModal = (props) => {
   const { formik, permission, mode } = props
   return <React.Fragment>
      <ZenField required autoFocus readOnly={!permission || (mode === FormMode.ADD ? false : true)}
         label={"indmplvt.ma_plvt"} defaultlabel="Mã phân loại vật tư"
         name="ma_plvt" props={formik} textCase="uppercase"
         isCode
      />
      <ZenField required readOnly={!permission}
         label={"indmplvt.ten_plvt"} defaultlabel="Tên phân loại vật tư"
         name="ten_plvt" props={formik}
      />
      <ZenFieldSelect readOnly={!permission} options={optionsLoai}
         label={"indmplvt.loai"} defaultlabel="Kiểu" width="8"
         name="loai" props={formik} clearable={false}
      />
   </React.Fragment>
}

const INDmPlvt = {
   FormModal: INDmPlvtModal,
   api: {
      url: ApiINDmPlvt,
   },
   permission: {
      view: permissions.INDmPlVtXem,
      add: permissions.INDmPlVtThem,
      edit: permissions.INDmPlVtSua
   },
   formId: "indmplvt-form",
   size: "tiny",
   isScrolling: false,
   initItem: {
      ma_plvt: "",
      ten_plvt: "",
      loai: "1",
      ksd: false
   },
   formValidation: [
      {
         id: "ma_plvt",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
            {
               type: "max",
               params: [8, "Không được vượt quá 8 kí tự"]
            },
         ]
      },
      {
         id: "ten_plvt",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
         ]
      },
   ]
}

export { INDmPlvt };