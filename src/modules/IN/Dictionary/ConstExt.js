export const optionsLoaiVT = [
    { key: 1, value: "1", text: "Hàng hóa" },
    { key: 2, value: "2", text: "Vật tư" },
    { key: 3, value: "3", text: "Thành phẩm" },
    { key: 4, value: "4", text: "Dịch vụ" },
    { key: 5, value: "5", text: "Công cụ dụng cụ" },
    { key: 9, value: "9", text: "Khác" }
]
export const optionsLoaiGiaTon = [
    { key: 1, value: "1", text: "1. Bình quân tháng" },
    { key: 2, value: "2", text: "2. Nhập trước xuất trước" },
    { key: 3, value: "3", text: "3. Bình quân di động" },
    { key: 4, value: "4", text: "4. Đích danh" }
]

export const optionsTKKhoTheo = [
    { key: 1, value: 1, text: "Theo khai báo vật tư" },
    { key: 2, value: 2, text: "Theo kho" },
]