import React from "react";
import { FormatDate, ZenField, ZenFieldCheckbox, ZenFieldNumber, ZenFieldSelectApi } from "../../../components/Control/index";
import { FormMode } from "../../../utils";
import { Form } from "semantic-ui-react";
import { ApiINDmNhvt } from "../Api";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import * as permissions from "../../../constants/permissions"

const INDmNhvtModal = (props) => {
   const { formik, permission, mode } = props

   const handleSelectedNhvt = (itemSelected) => {
      formik.setFieldValue("cap", itemSelected.cap ? itemSelected.cap + 1 : 1)
      formik.setFieldValue("tk_vt", itemSelected.tk_vt)
      formik.setFieldValue("tk_dt", itemSelected.tk_dt)
      formik.setFieldValue("tk_dtnb", itemSelected.tk_dtnb)
      formik.setFieldValue("tk_gv", itemSelected.tk_gv)
      formik.setFieldValue("tk_tl", itemSelected.tk_tl)
      formik.setFieldValue("tk_ck", itemSelected.tk_ck)
   }

   return <React.Fragment>
      <ZenField required autoFocus readOnly={!permission || (mode === FormMode.ADD ? false : true)}
         label={"indmnhvt.ma_nhvt"} defaultlabel="Mã nhóm VT"
         name="ma_nhvt" props={formik} textCase="uppercase"
         isCode
      />
      <ZenField required readOnly={!permission}
         label={"indmnhvt.ten_nhvt"} defaultlabel="Tên nhóm VT"
         name="ten_nhvt" props={formik}
      />

      <Form.Group widths="equal">
         <ZenFieldSelectApi loadApi
            lookup={{ ...ZenLookup.Ma_nhvt, cols: ZenLookup.Ma_nhvt.cols + ',cap' }}
            label={"indmnhvt.nhom_me"} defaultlabel="Nhóm mẹ"
            name="nhom_me" formik={formik}
            readOnly={!permission}
            onItemSelected={handleSelectedNhvt}
         />

         <ZenFieldNumber readOnly={!permission} maxLength={3}
            label={"indmnhvt.cap"} defaultlabel="Cấp"
            name="cap" props={formik}
            decimalScale={0} readOnly
         />
      </Form.Group>

      <Form.Group widths="equal">
         <ZenFieldSelectApi loadApi
            lookup={{
               ...ZenLookup.TK,
               where: "chi_tiet = 1",
               onLocalWhere: (items) => {
                  return items.filter((t) => t.chi_tiet == true);
               },
               formatInput: "{tk_vt}",
            }}
            label={"indmnhvt.tk_vt"} defaultlabel="TK VT"
            name="tk_vt" formik={formik}
            readOnly={!permission}
         />
         <ZenFieldSelectApi loadApi
            lookup={{
               ...ZenLookup.TK, where: "chi_tiet = 1",
               onLocalWhere: (items) => {
                  return items.filter((t) => t.chi_tiet == true);
               },
               formatInput: "{tk_dt}",
            }}
            label={"indmnhvt.tk_dt"} defaultlabel="TK DT"
            name="tk_dt" formik={formik}
            readOnly={!permission}
         />
      </Form.Group>

      <Form.Group widths="equal">
         <ZenFieldSelectApi loadApi upward
            lookup={{
               ...ZenLookup.TK, where: "chi_tiet = 1",
               onLocalWhere: (items) => {
                  return items.filter((t) => t.chi_tiet == true);
               },
               formatInput: "{tk_dtnb}",
            }}
            label={"indmnhvt.tk_dtnb"} defaultlabel="TK DTNB"
            name="tk_dtnb" formik={formik}
            readOnly={!permission}
         />
         <ZenFieldSelectApi loadApi upward
            lookup={{
               ...ZenLookup.TK, where: "chi_tiet = 1",
               onLocalWhere: (items) => {
                  return items.filter((t) => t.chi_tiet == true);
               },
               formatInput: "{tk_gv}",
            }}
            label={"indmnhvt.tk_gv"} defaultlabel="TK GV"
            name="tk_gv" formik={formik}
            readOnly={!permission}
         />
      </Form.Group>

      <Form.Group widths="equal">
         <ZenFieldSelectApi loadApi upward
            lookup={{
               ...ZenLookup.TK, where: "chi_tiet = 1",
               onLocalWhere: (items) => {
                  return items.filter((t) => t.chi_tiet == true);
               },
               formatInput: "{tk_tl}",
            }}
            label={"indmnhvt.tk_tl"} defaultlabel="TK TL"
            name="tk_tl" formik={formik}
            readOnly={!permission}
         />
         <ZenFieldSelectApi loadApi upward
            lookup={{
               ...ZenLookup.TK, where: "chi_tiet = 1",
               onLocalWhere: (items) => {
                  return items.filter((t) => t.chi_tiet == true);
               },
               formatInput: "{tk_vt}",
            }}
            label={"indmnhvt.tk_ck"} defaultlabel="TK CK"
            name="tk_ck" formik={formik}
            readOnly={!permission}
         />
      </Form.Group>

      <ZenFieldCheckbox readOnly={!permission}
         label={"indmnhvt.cong_sl"} defaultlabel="Cộng số lượng"
         name="cong_sl" props={formik} />
   </React.Fragment>
}

const INDmNhvt = {
   FormModal: INDmNhvtModal,
   api: {
      url: ApiINDmNhvt,
   },
   permission: {
      view: permissions.INDmNhVtXem,
      add: permissions.INDmNhVtThem,
      edit: permissions.INDmNhVtSua
   },
   formId: "indmnhvt-form",

   initItem: {
      ma_nhvt: "", ten_nhvt: "",
      nhom_me: "",
      tk_vt: "", tk_dt: "", tk_dtnb: "",
      tk_gv: "", tk_tl: "", tk_ck: "",
      cap: 1,
      cong_sl: false, ksd: false
   },
   formValidation: [
      {
         id: "ma_nhvt",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
            {
               type: "max",
               params: [8, "Không được vượt quá 8 kí tự"]
            },
         ]
      },
      {
         id: "ten_nhvt",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
         ]
      },
   ]
}

export { INDmNhvt };