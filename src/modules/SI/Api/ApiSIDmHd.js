import axios from '../../../Api/axios';

const ExtName = "sidmhd"

export const ApiSIDmHd = {
   get(callback, filter = {}, pagination = {}) {
      axios.get(`${ExtName}`, {
         params: {
            qf: filter.qf,
            keyword: filter.keyword,
            sort: filter.sort,
            page: pagination.page,
            pageSize: pagination.pageSize
         },
      })
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   getByCode(code, callback) {
      axios.get(`${ExtName}/` + code)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   insert(data, callback) {
      axios.post(`${ExtName}`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   update(data, callback) {
      axios.put(`${ExtName}`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   updatePatch(code, data, callback) {
      axios.patch(`${ExtName}/${code}`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   delete(code, callback) {
      axios.delete(`${ExtName}/` + code)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   getHrHsNsByMaHd(ma_hd, callback, params) {
      axios.get(`${ExtName}/${ma_hd}/hrhsns`, {
         params: {
            vai_tro: params.vai_tro
         }
      })
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   insertHrHsNsByMaHd(data, callback) {
      axios.post(`${ExtName}/${data.ma_hd}/hrhsns`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   editHrHsNsByMaHd(data, callback) {
      axios.put(`${ExtName}/${data.ma_hd}/hrhsns`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   deleteHrHsNsByMaHd(ma_hd, ma_nvns, callback) {
      axios.delete(`${ExtName}/${ma_hd}/hrhsns/${ma_nvns}`)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   getthutien(code, callback) {
      axios.get(`${ExtName}/thutien`,{params:{maHd: code}})
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   insertthutien(data, callback) {
      axios.post(`${ExtName}/thutien`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   updatethutien(data, callback) {
      axios.put(`${ExtName}/thutien`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   deletethutien(params, callback) {
      axios.delete(`${ExtName}/thutien`,{params: {stt_rec: params.stt_rec, ma_ct: params.ma_ct}})
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },
}