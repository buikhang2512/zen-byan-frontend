import { IntlFormat } from "../../../utils/intlFormat";
import { Mess_SIDmHuyen, Mess_SIDmLoai, Mess_SIDmQuocGia, Mess_SIDmTinh, Mess_SIDmDanhXung,Mess_SIDmHd } from "./variable";

const Vi_Vn = {
    ...IntlFormat.setMessageLanguage(Mess_SIDmLoai),
    ...IntlFormat.setMessageLanguage(Mess_SIDmQuocGia),
    ...IntlFormat.setMessageLanguage(Mess_SIDmTinh),
    ...IntlFormat.setMessageLanguage(Mess_SIDmHuyen),
    ...IntlFormat.setMessageLanguage(Mess_SIDmDanhXung),
    ...IntlFormat.setMessageLanguage(Mess_SIDmHd),
}

export default Vi_Vn