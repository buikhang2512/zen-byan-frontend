import { IntlFormat } from "../../../utils/intlFormat";
import { Mess_SIDmHuyen, Mess_SIDmLoai, Mess_SIDmQuocGia, Mess_SIDmTinh, Mess_SIDmHd,Mess_SIDmDanhXung } from "./variable";

const local = "en"

const En_US = {
    ...IntlFormat.setMessageLanguage(Mess_SIDmLoai, local),
    ...IntlFormat.setMessageLanguage(Mess_SIDmQuocGia, local),
    ...IntlFormat.setMessageLanguage(Mess_SIDmTinh, local),
    ...IntlFormat.setMessageLanguage(Mess_SIDmHuyen, local),
    ...IntlFormat.setMessageLanguage(Mess_SIDmDanhXung, local),
   ...IntlFormat.setMessageLanguage(Mess_SIDmHd, local),
}

export default En_US