const prefixSIDmloai = "sidmloai."
const prefixSIDmQuocGia = "sidmquocgia."
const prefixSIDmTinh = "sidmtinh."
const prefixSIDmDanhXung = "SIDmDanhXung."
const prefixSIDmHd = "SIDmHd."

export const Mess_SIDmLoai = {
    List: {
        id: prefixSIDmloai,
        defaultMessage: "Danh mục loại",
        en: "Category list"
    },
    Header: {
        id: prefixSIDmloai + "header",
        defaultMessage: "Loại",
        en: "Type"
    },
    Ma_loai: {
        id: prefixSIDmloai + "ma_loai",
        defaultMessage: "Mã loại",
        en: "Type code"
    },
    Ma: {
        id: prefixSIDmloai + "ma",
        defaultMessage: "Mã",
        en: "Code"
    },
    Ten: {
        id: prefixSIDmloai + "ten_loai",
        defaultMessage: "Tên loại",
        en: "Type name"
    },

    Nhom: {
        id: prefixSIDmloai + "nhom",
        defaultMessage: "Nhóm",
        en: "Group"
    }
}

export const Mess_SIDmQuocGia = {
    List: {
        id: prefixSIDmQuocGia,
        defaultMessage: "Danh mục quốc gia",
        en: "Nation list"
    },
    Header: {
        id: prefixSIDmQuocGia + "header",
        defaultMessage: "Quốc gia",
        en: "Nation"
    },
    Ma: {
        id: prefixSIDmQuocGia + "ma_quoc_gia",
        defaultMessage: "Mã quốc gia",
        en: "Nation code"
    },
    Ten: {
        id: prefixSIDmQuocGia + "ten_quoc_gia",
        defaultMessage: "Tên quốc gia",
        en: "Nation name"
    },
}

export const Mess_SIDmTinh = {
    List: {
        id: prefixSIDmTinh,
        defaultMessage: "Danh mục tỉnh/thành phố",
        en: "Province/city list"
    },
    Header: {
        id: prefixSIDmTinh + "header",
        defaultMessage: "Tỉnh/thành phố",
        en: "Province/city"
    },
    Ma: {
        id: prefixSIDmTinh + "ma_tinh",
        defaultMessage: "Mã tỉnh",
        en: "Province/city code"
    },
    Ten: {
        id: prefixSIDmTinh + "ten_tinh",
        defaultMessage: "Tên tỉnh",
        en: "Province/city name"
    },
    Quocgia: {
        id: prefixSIDmTinh + "ma_quoc_gia",
        defaultMessage: "Quốc gia",
        en: "Nation"
    },
    TenQuocgia: {
        id: prefixSIDmTinh + "ten_quoc_gia",
        defaultMessage: "Tên quốc gia",
        en: "Nation name"
    },
}

export const Mess_SIDmHuyen = {
    List: {
        id: prefixSIDmTinh,
        defaultMessage: "Danh mục quận/huyện",
        en: "District list"
    },
    Header: {
        id: prefixSIDmTinh + "header",
        defaultMessage: "Quận/huyện",
        en: "District"
    },
    Ma: {
        id: prefixSIDmTinh + "ma_huyen",
        defaultMessage: "Mã quận/huyện",
        en: "District code"
    },
    Ten: {
        id: prefixSIDmTinh + "ten_huyen",
        defaultMessage: "Tên quận/huyện",
        en: "District name"
    },
    Tinh: {
        id: prefixSIDmTinh + "ma_tinh",
        defaultMessage: "Tỉnh",
        en: "Province/city"
    },
    TenTinh: {
        id: prefixSIDmTinh + "ten_tinh",
        defaultMessage: "Tên tỉnh",
        en: "Province/city name"
    },
}

export const Mess_SIDmDanhXung = {
    List: {
       id: prefixSIDmDanhXung,
       defaultMessage: "Danh sách danh xưng",
       en: "List call name"
    },
    Header: {
       id: prefixSIDmDanhXung + "header",
       defaultMessage: "Danh xưng",
       en: "Call name"
    },
    DanhXung: {
       id: prefixSIDmDanhXung + "danh_xung",
       defaultMessage: "Danh xưng",
       en: "Call name"
    },
 }
 
 export const Mess_SIDmHd = {
    List: {
       id: prefixSIDmHd,
       defaultMessage: "Danh sách hợp đồng",
    },
    Header: {
       id: prefixSIDmHd + "header",
       defaultMessage: "Hợp đồng",
    },
    Detail: {
       id: prefixSIDmHd + "detail",
       defaultMessage: "Chi tiết hợp đồng",
    },
    Ma: {
       id: prefixSIDmHd + "ma_hd",
       defaultMessage: "Mã hợp đồng",
    },
    Ten: {
       id: prefixSIDmHd + "ten_hd",
       defaultMessage: "Tên hợp đồng",
    },
    SoHd: {
       id: prefixSIDmHd + "so_hd",
       defaultMessage: "Số hợp đồng",
    },
    NgayHd: {
       id: prefixSIDmHd + "ngay_hd",
       defaultMessage: "Ngày hợp đồng",
    },
    NgayHh: {
       id: prefixSIDmHd + "ngay_hh",
       defaultMessage: "Ngày hết hạn",
    },
    GiaTriHD: {
       id: prefixSIDmHd + "tien",
       defaultMessage: "Giá trị HĐ",
    },
    GiaTriHD_NT: {
       id: prefixSIDmHd + "tien_nt",
       defaultMessage: "Giá trị HĐ ngoại tệ",
    },
    LoaiHD: {
       id: prefixSIDmHd + "loai",
       defaultMessage: "Loại HĐ",
    },
    NoiDung: {
       id: prefixSIDmHd + "noi_dung",
       defaultMessage: "Nội dung",
    },
    NhomHD: {
       id: prefixSIDmHd + "ma_nhhd",
       defaultMessage: "Nhóm HĐ",
    },
    NgoaiTe: {
       id: prefixSIDmHd + "ma_nt",
       defaultMessage: "Ngoại tệ",
    },
 }