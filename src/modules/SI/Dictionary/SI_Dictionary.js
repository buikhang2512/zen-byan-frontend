import React, { useState, useEffect } from 'react'
import * as routes from "../../../constants/routes";
import { auth, ZenHelper } from "../../../utils";
import { IntlFormat } from "../../../utils/intlFormat";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { ApiSIDmNamTC, ApiSIDmHuyen, ApiSIDmLoai, ApiSIDmQuocGia, ApiSIDmTinh, ApiSIDmNgh, ApiSIDmPhi, ApiSIDmNhhd, ApiSIDmHd, ApiSIDmForm, ApiSIDmLoaiTaiLieu, ApiSIDMCT } from "../Api";
import { Mess_SIDmLoai, Mess_SIDmQuocGia, Mess_SIDmTinh, Mess_SIDmHuyen, Mess_SIDmHd } from "../zLanguage/variable";
import {
  ZenMessageAlert,
  ZenFieldSelectApi,
} from "../../../components/Control";
import { Form, Dropdown, Table } from "semantic-ui-react";
import { ApiModule } from '../../../Api';
import * as permissions from "../../../constants/permissions";
import { GlobalStorage, KeyStorage } from '../../../utils/storage';

const tinh = GlobalStorage.getByField(KeyStorage.CacheData, "ma_tinh")?.data
const huyen = GlobalStorage.getByField(KeyStorage.CacheData, "ma_huyen")?.data
const quocgia = GlobalStorage.getByField(KeyStorage.CacheData, "ma_quoc_gia")?.data
let optionTinh = ZenHelper.translateListToSelectOptions(tinh, false, 'ma_tinh', 'ma_tinh', 'ma_tinh', [], 'ten_tinh')
let optionHuyen = ZenHelper.translateListToSelectOptions(huyen, false, 'ma_huyen', 'ma_huyen', 'ma_huyen', [], 'ten_huyen')
let optionQuocGia = ZenHelper.translateListToSelectOptions(quocgia, false, 'ma_quoc_gia', 'ma_quoc_gia', 'ma_quoc_gia', [], 'ten_quoc_gia')

const sidmloai = GlobalStorage.getByField(KeyStorage.CacheData, 'ma_loai')?.data
let optionNHOMTL = []
if (sidmloai) {
  optionNHOMTL = ZenHelper.translateListToSelectOptions(sidmloai.filter(x => x.ma_nhom === 'NHOM_TAI_LIEU'), false, 'ma_loai', 'ma', 'ma_loai', [], 'ten')
}
let manhhd = GlobalStorage.getByField(KeyStorage.CacheData, "ma_nhhd")?.data
let optionNhhd = ZenHelper.translateListToSelectOptions(manhhd, false, 'ma_nhhd', 'ma_nhhd', 'ma_nhhd', [], 'ten_nhhd')

const ContainerSIDMCT = (props) => {
  const { onLoadData, fieldCode } = props;
  const [modulesOpt, setModulesOpt] = useState("");
  const [module, setModule] = useState("CA");
  const [ma_ct, setMaCt] = useState("");

  useEffect(() => {
    ApiModule.get((res) => {
      if (res.status == 200) {
        setModulesOpt(
          [{ key: "allz", value: "allz", text: "Tất cả" }].concat(
            ZenHelper.convertToSelectOptions(
              res.data.data,
              false,
              "id",
              "id",
              "name",
            )
          ));
      }
    });
    onLoadData(`a.phan_he = '${"CA"}' and a.ma_ct like '${ma_ct}%'`, {
      module: module,
      ma_ct: ma_ct,
    });
  }, []);

  let onFilterChange = (e, { name, value }) => {
    let lmodule = module;
    let lma_ct = ma_ct;
    switch (name) {
      case "module":
        lmodule = value;
        setModule(value);
        break;
      case "ma_ct":
        lma_ct = value;
        setMaCt(value);
        break;
      default:
        break;
    }
    onLoadData(lmodule == 'allz' ? `a.ma_ct like '${lma_ct}%'` : `a.phan_he = '${lmodule}' and a.ma_ct like '${lma_ct}%'`, {
      module: module,
      ma_ct: ma_ct,
    });
  };

  return (
    <Form size="small">
      <div style={{ display: "flex" }}>
        <div style={{ minWidth: "180px", padding: "12px 12px 0px 0px" }}>
          <strong>
            <label>Phân hệ: </label>
            <Dropdown
              name="module"
              value={module}
              label="Phân hệ:"
              options={modulesOpt || []}
              onChange={onFilterChange}
              clearable={false}
            />
          </strong>
        </div>
        <div style={{ width: "100%" }}>
          <ZenFieldSelectApi
            inline
            width="11"
            lookup={{
              ...ZenLookup.Ma_ct,
              onLocalWhere: (items) => {
                return items.filter((t) => {
                  if (module !== 'allz') {
                    return t.phan_he === module
                  }
                  return t
                });
              },
              where: `phan_he like '${module}'`,
            }}
            label={"fadmts.ma_ct"}
            defaultlabel="Mã chứng từ"
            name="ma_ct"
            value={ma_ct}
            onChange={onFilterChange}
          />
        </div>
      </div>
    </Form>
  );
};

export const SI_Dictionary = {
  SIDmLoai: {
    route: routes.SIDmloai,

    action: {
      view: { visible: true, permission: permissions.SIDmLoai },
      add: { visible: false, permission: permissions.SIDmLoai },
      edit: { visible: true, permission: permissions.SIDmLoai },
      del: { visible: false, permission: permissions.SIDmLoai }
    },
    linkHeader: {
      ...IntlFormat.default(Mess_SIDmLoai.List),
      active: true,
      isSetting: true,
    },

    tableList: {
      keyForm: "SIDmLoai",
      formModal: ZenLookup.SIDmloai,
      fieldCode: "ma_loai",
      upPagination: false,
      changeCode: false,
      showFilterLabel: true,

      api: {
        url: ApiSIDmLoai,
        type: "sql",
      },
      columns: [
        {
          ...IntlFormat.default(Mess_SIDmLoai.Nhom),
          fieldName: "ma_nhom", type: "string", filter: "string", sorter: true,
        },
        {
          ...IntlFormat.default(Mess_SIDmLoai.Ma_loai),
          fieldName: "ma_loai", type: "string", filter: "string", sorter: true, editForm: true
        },
        {
          ...IntlFormat.default(Mess_SIDmLoai.Ma),
          fieldName: "ma", type: "string", filter: "string", sorter: true,
        },
        {
          ...IntlFormat.default(Mess_SIDmLoai.Ten),
          fieldName: "ten", type: "string", filter: "string", sorter: true,
        },
        //{ id: "ksd", defaultMessage: "Không sử dụng", fieldName: "ksd", type: "bool", filter: "", sorter: true },
      ],
    }
  },

  SIDmLoaitailieu: {
    route: routes.SIDmLoaitailieu,

    action: {
      view: { visible: true, permission: permissions.SIDmLoaiTaiLieuXem },
      add: { visible: true, permission: permissions.SIDmLoaiTaiLieuThem },
      edit: { visible: true, permission: permissions.SIDmLoaiTaiLieuSua },
      del: { visible: true, permission: permissions.SIDmLoaiTaiLieuXoa }
    },
    linkHeader: {
      id: "sidmloaitailieu",
      defaultMessage: "Danh mục loại tài liệu",
      active: true,
      isSetting: true,
    },

    tableList: {
      keyForm: "SIDmLoaitailieu",
      formModal: ZenLookup.SIDmloaitailieu,
      fieldCode: "ma_loai",
      upPagination: false,
      changeCode: false,
      showFilterLabel: true,

      api: {
        url: ApiSIDmLoaiTaiLieu,
        type: "sql",
      },
      columns: [
        {
          id: "sidmloaitailieu.", defaultMessage: "Mã",
          fieldName: "ma_loai", type: "string", filter: "string", sorter: true,
        },
        {
          id: "sidmloaitailieu.", defaultMessage: "Tên loại tài liệu",
          fieldName: "ten_loai", type: "string", filter: "string", sorter: true, editForm: true
        },
        {
          id: "sidmloaitailieu.", defaultMessage: "Thứ tự hiển thị",
          fieldName: "ordinal", type: "string", filter: "string", sorter: true,
        },
        {
          id: "sidmloaitailieu.", defaultMessage: "Nhóm",
          fieldName: "nhom", type: "string", filter: "list", sorter: true, custom: true, listFilter: optionNHOMTL
        },
        //{ id: "ksd", defaultMessage: "Không sử dụng", fieldName: "ksd", type: "bool", filter: "", sorter: true },
      ],
    }
  },

  SIDmQuocGia: {
    route: routes.SIDmquocgia,

    action: {
      view: { visible: true, permission: permissions.SIDmQuocGiaXem },
      add: { visible: true, permission: permissions.SIDmQuocGiaThem },
      edit: { visible: true, permission: permissions.SIDmQuocGiaSua },
      del: { visible: true, permission: permissions.SIDmQuocGiaXoa }
    },

    linkHeader: {
      ...IntlFormat.default(Mess_SIDmQuocGia.List),
      active: true,
      isSetting: true,
    },

    tableList: {
      keyForm: "SIDmQuocGia",
      formModal: ZenLookup.SIDmQuocGia,
      fieldCode: "ma_quoc_gia",
      upPagination: false,
      showFilterLabel: true,

      api: {
        url: ApiSIDmQuocGia,
        type: "sql",
      },
      columns: [
        {
          ...IntlFormat.default(Mess_SIDmQuocGia.Ma),
          fieldName: "ma_quoc_gia", type: "string", filter: "string", sorter: true, editForm: true
        },
        {
          ...IntlFormat.default(Mess_SIDmQuocGia.Ten),
          fieldName: "ten_quoc_gia", type: "string", filter: "string", sorter: true,
        },
        { id: "PMDmGiaiDoanDa.ordinal", defaultMessage: "Thứ tự hiển thị", fieldName: "ordinal", filter: "number", sorter: true, },
        { id: "ksd", defaultMessage: "Không sử dụng", fieldName: "ksd", type: "bool", filter: "", sorter: true },
      ]
    }
  },

  SIDmTinh: {
    route: routes.SIDmTinh,

    action: {
      view: { visible: true, permission: permissions.SIDmTinhXem },
      add: { visible: true, permission: permissions.SIDmTinhThem },
      edit: { visible: true, permission: permissions.SIDmTinhSua },
      del: { visible: true, permission: permissions.SIDmTinhXoa }
    },

    linkHeader: {
      ...IntlFormat.default(Mess_SIDmTinh.List),
      active: true,
      isSetting: true,
    },

    tableList: {
      keyForm: "SIDMTINH",
      formModal: ZenLookup.SIDmTinh,
      fieldCode: "ma_tinh",
      upPagination: false,
      showFilterLabel: true,

      api: {
        url: ApiSIDmTinh,
        type: "sql",
      },
      columns: [
        {
          ...IntlFormat.default(Mess_SIDmTinh.Ma),
          fieldName: "ma_tinh", type: "string", filter: "string", sorter: true, editForm: true
        },
        {
          ...IntlFormat.default(Mess_SIDmTinh.Ten),
          fieldName: "ten_tinh", type: "string", filter: "string", sorter: true,
        },
        {
          ...IntlFormat.default(Mess_SIDmTinh.TenQuocgia),
          fieldName: "ma_quoc_gia", type: "string", filter: "list", sorter: true, custom: true, listFilter: optionQuocGia,
        },
        { id: "PMDmGiaiDoanDa.ordinal", defaultMessage: "Thứ tự hiển thị", fieldName: "ordinal", filter: "number", sorter: true, },
        { id: "ksd", defaultMessage: "Không sử dụng", fieldName: "ksd", type: "bool", filter: "", sorter: true },
      ],
      customCols: (data, item, fieldName, { onEdit, ...restCol }) => {
        let customCell = undefined
        if (fieldName === 'ma_quoc_gia') {
          const loai = optionQuocGia.find(t => t.value == item["ma_quoc_gia"]) || {}
          customCell = <Table.Cell key={fieldName}>
            {loai.text || item["ten_quoc_gia"]}
          </Table.Cell>
        }
        return customCell
      }
    }
  },

  SIDmHuyen: {
    route: routes.SIDmHuyen,

    action: {
      view: { visible: true, permission: permissions.SIDmHuyenXem },
      add: { visible: true, permission: permissions.SIDmHuyenThem },
      edit: { visible: true, permission: permissions.SIDmHuyenSua },
      del: { visible: true, permission: permissions.SIDmHuyenXoa }
    },

    linkHeader: {
      ...IntlFormat.default(Mess_SIDmHuyen.List),
      active: true,
      isSetting: true,
    },

    tableList: {
      keyForm: "SIDMHuyen",
      formModal: ZenLookup.SIDmHuyen,
      fieldCode: "ma_huyen",
      upPagination: false,
      showFilterLabel: true,

      api: {
        url: ApiSIDmHuyen,
        type: "sql",
      },
      columns: [
        {
          ...IntlFormat.default(Mess_SIDmHuyen.Ma),
          fieldName: "ma_huyen", type: "string", filter: "string", sorter: true, editForm: true
        },
        {
          ...IntlFormat.default(Mess_SIDmHuyen.Ten),
          fieldName: "ten_huyen", type: "string", filter: "string", sorter: true,
        },
        {
          ...IntlFormat.default(Mess_SIDmHuyen.TenTinh),
          fieldName: "ma_tinh", type: "string", filter: "list", sorter: true, custom: true, listFilter: optionTinh
        },
        { id: "PMDmGiaiDoanDa.ordinal", defaultMessage: "Thứ tự hiển thị", fieldName: "ordinal", filter: "number", sorter: true, },
        { id: "ksd", defaultMessage: "Không sử dụng", fieldName: "ksd", type: "bool", filter: "", sorter: true },
      ],
      customCols: (data, item, fieldName, { onEdit, ...restCol }) => {
        let customCell = undefined
        if (fieldName === 'ma_tinh') {
          const loai = optionTinh.find(t => t.value == item["ma_tinh"]) || {}
          customCell = <Table.Cell key={fieldName}>
            {loai.text || item["ten_tinh"]}
          </Table.Cell>
        }
        return customCell
      }
    }
  },
  SiDmNhhd: {
    route: routes.SIDmNhhd,
    importExcel: true,
    action: {
      view: { visible: true, permission: "90.04.1" },
      add: { visible: true, permission: "90.04.2" },
      edit: { visible: true, permission: "90.04.3" },
      del: { visible: true, permission: "90.04.4" },
    },
    linkHeader: {
      id: "sidmnhhd",
      defaultMessage: "Danh mục nhóm hợp đồng",
      active: true,
      isSetting: true,
    },
    tableList: {
      keyForm: "SiDmNhhd",
      formModal: ZenLookup.Ma_nhhd,
      fieldCode: "ma_nhhd",
      unPagination: false,
      duplicate: true,
      showFilterLabel: true,

      api: {
        url: ApiSIDmNhhd,
        type: "sql",
      },
      columns: [
        {
          id: "sidmnhhd.ma_nhhd",
          defaultMessage: "Mã nhóm hợp đồng",
          fieldName: "ma_nhhd",
          type: "string",
          filter: "string",
          sorter: true,
          editForm: true,
        },
        {
          id: "sidmnhhd.ten_nhhd",
          defaultMessage: "Tên nhóm hợp đồng",
          fieldName: "ten_nhhd",
          type: "string",
          filter: "string",
          sorter: true,
        },
        { id: "PMDmGiaiDoanDa.ordinal", defaultMessage: "Thứ tự hiển thị", fieldName: "ordinal", filter: "number", sorter: true, },
        {
          id: "ksd",
          defaultMessage: "Không sử dụng",
          fieldName: "ksd",
          type: "bold",
          filter: "",
          sorter: true,
          collapsing: true,
        },
      ],
    },
  },
  SiDmPhi: {
    route: routes.SIDmPhi,
    importExcel: true,
    action: {
      view: { visible: true, permission: "90.02.1" },
      add: { visible: true, permission: "90.02.2" },
      edit: { visible: true, permission: "90.02.3" },
      del: { visible: true, permission: "90.02.4  " },
      dup: { visible: true, permission: "" },
    },

    linkHeader: {
      id: "sidmphi",
      defaultMessage: "Danh mục phí",
      active: true,
      isSetting: true,
    },

    tableList: {
      keyForm: "SiDmPhi",
      formModal: ZenLookup.Ma_phi,
      fieldCode: "ma_phi",
      unPagination: false,
      duplicate: true,
      showFilterLabel: true,

      api: {
        url: ApiSIDmPhi,
        type: "sql",
      },
      columns: [
        {
          id: "sidmbp.ma_phi",
          defaultMessage: "Mã phí",
          fieldName: "ma_phi",
          type: "string",
          filter: "string",
          sorter: true,
          editForm: true,
        },
        {
          id: "sidmbp.ten_phi",
          defaultMessage: "Tên phí",
          fieldName: "ten_phi",
          type: "string",
          filter: "string",
          sorter: true,
        },
        { id: "PMDmGiaiDoanDa.ordinal", defaultMessage: "Thứ tự hiển thị", fieldName: "ordinal", filter: "number", sorter: true, },
        {
          id: "ksd",
          defaultMessage: "Không sử dụng",
          fieldName: "ksd",
          type: "bool",
          filter: "",
          sorter: true,
          collapsing: true,
        },
      ],
    },
  },
  SiDmNgh: {
    route: routes.SIDmNgh,
    importExcel: true,
    action: {
      view: { visible: true, permission: permissions.SIDmNghXem },
      add: { visible: true, permission: permissions.SIDmNghThem },
      edit: { visible: true, permission: permissions.SIDmNghSua },
      del: { visible: true, permission: permissions.SIDmNghXoa },
    },

    linkHeader: {
      id: "sidmngh",
      defaultMessage: "Danh mục ngân hàng",
      active: true,
      isSetting: true,
    },

    tableList: {
      keyForm: "SiDmNgh",
      formModal: ZenLookup.Ma_ngh,
      fieldCode: "ma_ngh",
      unPagination: false,
      duplicate: true,
      showFilterLabel: true,

      api: {
        url: ApiSIDmNgh,
        type: "sql",
      },
      columns: [
        {
          id: "sidmbp.ma_ngh",
          defaultMessage: "Mã ngân hàng",
          fieldName: "ma_ngh",
          type: "string",
          filter: "string",
          sorter: true,
          editForm: true,
        },
        {
          id: "sidmbp.ten_ngh",
          defaultMessage: "Tên ngân hàng",
          fieldName: "ten_ngh",
          type: "string",
          filter: "string",
          sorter: true,
        },
        { id: "PMDmGiaiDoanDa.ordinal", defaultMessage: "Thứ tự hiển thị", fieldName: "ordinal", filter: "number", sorter: true, },
        {
          id: "sidmbp.ghi_chu",
          defaultMessage: "Ghi chú",
          fieldName: "ghi_chu",
          type: "string",
          filter: "string",
          sorter: true,
        },
        {
          id: "ksd",
          defaultMessage: "Không sử dụng",
          fieldName: "ksd",
          type: "bool",
          filter: "",
          sorter: true,
          collapsing: true,
        },
      ],
    },
  },
  SiDmHd: {
    route: routes.SIDmHd,
    action: {
      view: { visible: true, permission: permissions.SIDmHdXem },
      add: { visible: true, permission: permissions.SIDmHdThem },
      edit: { visible: true, permission: permissions.SIDmHdSua },
      del: { visible: true, permission: permissions.SIDmHdXoa }
    },
    linkHeader: {
      ...IntlFormat.default(Mess_SIDmHd.List),
      active: true,
      isSetting: false,
    },
    tableList: {
      keyForm: "SiDmHd", // khai báo key để lưu format table
      formModal: ZenLookup.Ma_hd,
      fieldCode: "ma_hd",
      unPagination: false,
      showFilterLabel: true,

      api: {
        url: ApiSIDmHd,
        type: "sql",
      },
      // onBeforeCallApiGet: (where) => {
      //   const { qfFilter, paramsApi } = where
      //   const userInfo = auth.getUserInfo()
      //   return new Promise(resolve => {
      //     // là admin get hết
      //     if (userInfo.admin.toLowerCase() == "true") {
      //       resolve(where)
      //       return
      //     }

      //     // get theo ma_nvns
      //     const qf = `a.ma_hd IN (SELECT ma_hd from sidmhdnv where ma_nvns = '${userInfo.hrid}')`
      //     if (qfFilter.qf) {
      //       qfFilter.qf += ` and (${qf})`
      //     } else {
      //       qfFilter.qf = qf
      //     }
      //     resolve({ ...where, qfFilter: qfFilter })
      //   })
      // },
      columns: [
        {
          ...IntlFormat.default(Mess_SIDmHd.Ma), fieldName: "ma_hd", type: "string",
          filter: "string", sorter: true, link: { route: routes.SIDmHdDetail(), params: "ma_hd" }
        },
        { id: "sidmhd.ngay_hd", defaultMessage: "Ngày ký", fieldName: "ngay_hd", type: "date", filter: "date", sorter: true, },
        {
          ...IntlFormat.default(Mess_SIDmHd.Ten), fieldName: "ten_hd", type: "string",
          filter: "string", sorter: true, link: { route: routes.SIDmHdDetail(), params: "ma_hd" }
        },
        { id: "sidmhd.ten_kh", defaultMessage: "Khách hàng", fieldName: "ten_kh", type: "string", filter: "string", sorter: true, },
        { id: "sidmhd.ma_nhhd", defaultMessage: "Nhóm hợp đồng", fieldName: "ma_nhhd", type: "string", filter: "list", sorter: true, custom: true, listFilter: optionNhhd },
        { id: "sidmhd.ten_nvkd", defaultMessage: "Nhân viên kinh doanh", fieldName: "ten_nvkd", type: "string", filter: "string", sorter: true, },
        // { ...IntlFormat.default(Mess_SIDmHd.SoHd), fieldName: "so_hd", type: "string", filter: "string", sorter: true, },
        // { ...IntlFormat.default(Mess_SIDmHd.NgayHh), fieldName: "ngay_hh", type: "date", filter: "date", sorter: true, },
        // { ...IntlFormat.default(Mess_SIDmHd.GiaTriHD), fieldName: "tien", type: "number", filter: "number", sorter: true, },
        // { id: "ksd", defaultMessage: "Không sử dụng", fieldName: "ksd", type: "bool", filter: "", sorter: true, collapsing: true },
      ],
      customCols: (data, item, fieldName, { onEdit, ...restCol }) => {
        let customCell = undefined
        if (fieldName === 'ma_nhhd') {
          const loai = optionNhhd.find(t => t.value == item["ma_nhhd"]) || {}
          customCell = <Table.Cell key={fieldName}>
            {loai.text || item["ten_nhhd"]}
          </Table.Cell>
        }
        return customCell
      }
    },
  },
  SiDmForm: {
    route: routes.sidmform,
    action: {
      view: { visible: true, permission: permissions.SIDmFormXem },
      add: { visible: true, permission: permissions.SIDmFormThem },
      edit: { visible: true, permission: permissions.SIDmBpSua },
      del: { visible: true, permission: permissions.SIDmFormXoa }
    },
    linkHeader: {
      id: "SIDMFORM",
      defaultMessage: "Danh mục biểu mẫu",
      active: true,
      isSetting: true,
    },
    tableList: {
      keyForm: "SiDmform", // khai báo key để lưu format table
      formModal: ZenLookup.SIDmForm,
      fieldCode: "ma_form",
      unPagination: false,

      api: {
        url: ApiSIDmForm,
        type: "sql",
      },
      columns: [
        {
          id: "sidmform.ma_form", defaultMessage: "Mã Form", fieldName: "ma_form", type: "string", filter: "string", sorter: true, editForm: true,
          link: { route: routes.sidmformDetail(), params: "ma_form" }
        },
        { id: "sidmform.ten_form", defaultMessage: "Tên Form", fieldName: "ten_form", type: "string", filter: "string", sorter: true, },
        { id: "sidmform.ghi_chu", defaultMessage: "Ghi chú", fieldName: "ghi_chu", type: "string", filter: "string", sorter: true, },
        { id: "ksd", defaultMessage: "Không sử dụng", fieldName: "ksd", type: "bool", filter: "", sorter: true, collapsing: true },
      ],
    },
  },

  SIDmNamTC: {
    route: routes.SIDmNamTC,
    action: {
      view: { visible: true, permission: permissions.SIDmNamTc },
      add: { visible: true, permission: permissions.SIDmNamTc },
      edit: { visible: true, permission: permissions.SIDmNamTc },
      del: { visible: true, permission: permissions.SIDmNamTc },
    },
    linkHeader: {
      id: "SIDmNamTC",
      defaultMessage: "Danh mục năm tài chính",
      active: true,
      isSetting: true,
    },
    tableList: {
      keyForm: "SiDmNamTC",
      formModal: ZenLookup.Nam_tc,
      fieldCode: "Nam",
      unPagination: false,
      changeCode: false,
      showFilterLabel: true,

      api: {
        url: ApiSIDmNamTC,
        type: "sql",
      },
      columns: [
        {
          id: "sidmbp.nam",
          defaultMessage: "Năm tài chính",
          fieldName: "Nam",
          type: "string",
          filter: "number",
          sorter: true,
          collapsing: true,
          editForm: true,
        },
        {
          id: "sidmbp.ngay_dntc",
          defaultMessage: "Ngày đầu năm",
          fieldName: "Ngay_dntc",
          type: "date",
          filter: "date",
          sorter: true,
        },
        {
          id: "sidmbp.ngay_cntc",
          defaultMessage: "Ngày kết thúc",
          fieldName: "Ngay_cntc",
          type: "date",
          filter: "date",
          sorter: true,
        },
        {
          id: "sidmbp.ngay_ks",
          defaultMessage: "Ngày khóa số liệu",
          fieldName: "ngay_ks",
          type: "date",
          filter: "date",
          sorter: true,
        },
        {
          id: "sidmbp.description",
          defaultMessage: "Mô tả",
          fieldName: "Description",
          type: "string",
          filter: "string",
          sorter: true,
        },
        {
          id: "ksd",
          defaultMessage: "Không sử dụng",
          fieldName: "ksd",
          type: "bool",
          filter: "",
          sorter: true,
          collapsing: true,
        },
      ],
    },
  },
  SIDMCT: {
    route: routes.SIDMCT,

    action: {
      view: { visible: true, permission: permissions.SIDmCt },
      add: { visible: false, permission: permissions.SIDmCt },
      edit: { visible: true, permission: permissions.SIDmCt },
      del: { visible: false, permission: permissions.SIDmCt },
    },

    linkHeader: {
      id: "sidmct",
      defaultMessage: "khai báo màn hình nhập chứng từ",
      active: true,
      isSetting: true,
    },

    tableList: {
      keyForm: "SiDmCt",
      formModal: ZenLookup.Ma_ct,
      ContainerTop: ContainerSIDMCT,
      fieldCode: "ma_ct",
      unPagination: false,
      changeCode: false,
      showFilterLabel: true,
      
      api: {
        url: ApiSIDMCT,
        type: "sql",
      },
      columns: [
        {
          id: "FaKhTs.ma_ct",
          defaultMessage: "Mã CT",
          fieldName: "ma_ct",
          type: "string",
          filter: "string",
          sorter: true,
          editForm: true,
        },
        {
          id: "FaKhTs.ten_ct",
          defaultMessage: "Tên CT",
          fieldName: "ten_ct",
          type: "string",
          filter: "string",
          sorter: true,
          editForm: true,
        },
        {
          id: "FaKhTs.stt_nkc",
          defaultMessage: "STT NKC",
          fieldName: "stt_nkc",
          type: "string",
          filter: "string",
          sorter: true,
        },
        {
          id: "FaKhTs.ma_ct_me",
          defaultMessage: "Mã CT mẹ",
          fieldName: "ma_ct_me",
          type: "string",
          filter: "string",
          sorter: true,
        },
        {
          id: "FaKhTs.phan_he",
          defaultMessage: "Phân hệ",
          fieldName: "phan_he",
          type: "string",
          filter: "string",
          sorter: true,
        },
        {
          id: "FaKhTs.tk_no",
          defaultMessage: "TK nợ",
          fieldName: "tk_no",
          type: "string",
          filter: "string",
          sorter: true,
        },
        {
          id: "FaKhTs.tk_co",
          defaultMessage: "TK có",
          fieldName: "tk_co",
          type: "string",
          filter: "string",
          sorter: true,
        },
        {
          id: "FaKhTs.ma_nt",
          defaultMessage: "Mã NT",
          fieldName: "ma_nt",
          type: "string",
          filter: "string",
          sorter: true,
        },
        {
          id: "FaKhTs.so_lien",
          defaultMessage: "Số liên",
          fieldName: "so_lien",
          type: "string",
          filter: "string",
          sorter: true,
        },
        {
          id: "FaKhTs.stt_ntxt",
          defaultMessage: "STT NXT",
          fieldName: "stt_ntxt",
          type: "string",
          filter: "string",
          sorter: true,
        },
      ],
    },
  },
}