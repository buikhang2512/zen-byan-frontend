import React, { Component } from "react";
import * as routes from "../../../../constants/routes";
import _ from "lodash";

import { zzControlHelper } from "../../../../components/Control/zzControlHelper";
import { ApiSIDmForm } from "../../Api";
import {
    HeaderLink, Helmet, JSONFormSchemaBuilder, SegmentFooter, SegmentHeader, ZenButton,
    ZenMessageAlert, ZenMessageToast
} from "../../../../components/Control";
import { Breadcrumb } from "semantic-ui-react";
import { ZenLookup } from "../../../ComponentInfo/Dictionary/ZenLookup";
import * as permissions from '../../../../constants/permissions'

class SIDmFormDetailComponent extends Component {
    constructor(props) {
        super(props);
        this._isMounted = true
        this.maForm = zzControlHelper.atobUTF8(props.match.params.id)
        this.refJSONSchema = React.createRef();

        this.state = {
            loadingSave: false,
            item: null,
            itemOriginal: null,
            formSchema: undefined,
        }
    }

    componentDidMount() {
        ApiSIDmForm.getByCode(this.maForm, res => {
            if (this._isMounted) {
                if (res.status === 200) {
                    const result = res.data.data
                    const formSchema = JSON.parse(result.form_schema || "{}")

                    var temp = {
                        item: result,
                        itemOriginal: _.cloneDeep(result),
                        formSchema: _.cloneDeep(formSchema),
                    }
                }
                else
                    temp = { error: zzControlHelper.getResponseError(res) }

                this.setState({
                    loading: false,
                    ...temp
                })
            }
        })
    }

    componentWillUnmount() {
        this._isMounted = false
    }

    handleSave = (jsonschema) => {
        this.setState({
            loadingSave: true
        })
        const patchData = [
            {
                value: JSON.stringify(jsonschema),
                path: `/form_schema`,
                op: "replace",
                operationType: 0,
            }
        ]

        ApiSIDmForm.updatePatch(this.maForm, patchData,
            res => {
                if (res.status >= 200 && res.status <= 204) {
                    this.setState(prev => ({
                        formSchema: jsonschema,
                        item: {
                            ...prev.item,
                            formSchema: jsonschema,
                        },
                        itemOriginal: {
                            ...prev.itemOriginal,
                            formSchema: jsonschema,
                        }
                    }))
                    ZenMessageToast.success()
                } else {
                    ZenMessageAlert.error(zzControlHelper.getResponseError(res))
                }
                this.setState({
                    loadingSave: false
                })
            })
    }

    render() {
        const { formSchema, itemOriginal } = this.state
        return <>
            <PageHeader item={itemOriginal} />

            <JSONFormSchemaBuilder
                ref={this.refJSONSchema}
                formSchema={formSchema}
                optionsLookup={optionsLookup}
                onSave={this.handleSave}
            />

            <SegmentFooter style={{ textAlign: "right" }}>
                <ZenButton btnType="refresh" size="small"
                    content="Reset"
                    disabled={this.state.loadingSave}
                    onClick={() => this.refJSONSchema.current.onReset()}
                />
                <ZenButton btnType="save" size="small"
                    permission={permissions.SIDmFormSua}
                    loading={this.state.loadingSave}
                    onClick={() => this.refJSONSchema.current.onSave()}
                />
            </SegmentFooter>
        </>
    }
}

const PageHeader = ({ item }) => {
    return <>
        <Helmet idMessage={"ardmkh.detail"}
            defaultMessage={"Chi tiết khách hàng"} />

        <SegmentHeader>
            <HeaderLink listHeader={[{
                id: "sidmform",
                defaultMessage: "Danh mục biểu mẫu",
                route: routes.sidmform,
                active: false,
                isSetting: false,
            }]}>
                <Breadcrumb.Divider icon='right chevron' />
                <Breadcrumb.Section active>
                    {item?.ma_form || "Thiết lập form"}
                </Breadcrumb.Section>
            </HeaderLink>
        </SegmentHeader>
        <br />
    </>
}

const optionsLookup = [
    {
        key: "1",
        value: ZenLookup.Ma_kh.code,
        text: "Khách hàng"
    },
    {
        key: "2",
        value: ZenLookup.Ma_nguon_kh.code,
        text: "Nguồn khách hàng"
    },
]

export const SIDmFormDetail = {
    route: routes.sidmformDetail(),
    Zenform: SIDmFormDetailComponent,
    action: {
        view: { visible: true, permission: "" },
    },
}