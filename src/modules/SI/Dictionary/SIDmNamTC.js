import React from "react";
import {
  ZenFieldDate,
  ZenFieldNumber,
  ZenFieldTextArea,
} from "../../../components/Control/index";
import { Form } from "semantic-ui-react";
import { FormMode, ZenHelper } from "../../../utils";
import { ApiSIDmNamTC } from "../Api";
import { isNumber } from "lodash";
import * as permissions from "../../../constants/permissions"

const SIDmNamTCModal = (props) => {
  const { formik, permission, mode } = props;
  // didmountcomponent
  React.useEffect(() => {
    if (mode === FormMode.ADD) {
      let cyear = ZenHelper.getFiscalYear();
      let cDays = calc_ngays(cyear);
      formik.setValues({
        ...formik.values,
        nam: cyear,
        ngay_dntc: cDays.ngay_dntc,
        ngay_cntc: cDays.ngay_cntc,
      });
    }
  }, []);

  let calc_ngays = (cyear) => {
    if (!isNumber(cyear)) {
      return {};
    }
    let fdate = ZenHelper.formatDateTime(
      ZenHelper.getfirstDayOfMonth(0, cyear),
      "YYYY-MM-DD"
    );
    let ldate = ZenHelper.formatDateTime(
      ZenHelper.getLastDayOfMonth(11, cyear),
      "YYYY-MM-DD"
    );
    return {
      ngay_dntc: fdate,
      ngay_cntc: ldate,
    };
  };

  return (
    <React.Fragment>
      <Form.Group>
        <ZenFieldNumber
          thousandSeparator={""}
          width="8"
          name="Nam"
          readOnly={!permission || (mode === FormMode.ADD ? false : true)}
          label={"SIDmNamTC.nam"}
          defaultlabel="Năm tài chính"
          props={formik}
        />
        <ZenFieldDate
          width="8"
          readOnly={!permission}
          label="SIDmNamTC.ngay_ks"
          defaultlabel="Ngày khóa số liệu"
          name="ngay_ks"
          props={formik}
        />
      </Form.Group>

      <Form.Group>
        <ZenFieldDate
          width="8"
          readOnly={!permission}
          label="SIDmNamTC.gay_dntc"
          defaultlabel="Ngày đầu năm"
          name="Ngay_dntc"
          props={formik}
          minDate={new Date("01-01-1900")}
          maxDate={new Date("01-01-2079")}
        />
        <ZenFieldDate
          width="8"
          readOnly={!permission}
          label="SIDmNamTC.ngay_cntc"
          defaultlabel="Ngày kết thúc"
          name="Ngay_cntc"
          props={formik}
        />
      </Form.Group>

      <ZenFieldTextArea
        readOnly={!permission}
        label={"SIDmNamTC.description"}
        defaultlabel="Mô tả"
        name="Description"
        props={formik}
      />
    </React.Fragment>
  );
};

const onValidate = (item) => {
  const errors = {}
  const {ngay_ks,ngay_dntc,ngay_cntc,} = item
  const ngayks = parseInt(ngay_ks) > 1900 ?  ngay_ks : null
  const ngaydntc = parseInt(ngay_dntc) > 1900 ? ngay_dntc : null
  const ngaycntc = parseInt(ngay_cntc) > 1900 ? ngay_cntc : null
  if(ngayks){
    if(ngayks < ngaydntc || ngayks > ngaycntc) {
      errors.ngay_ks = 'Ngày khóa số liệu phải nằm trong khoảng ngày đầu năm và ngày kết thúc'
    }
  }
  return errors
}

const SIDmNamTC = {
  onValidate: onValidate,
  FormModal: SIDmNamTCModal,
  api: {
    url: ApiSIDmNamTC,
  },
  permission: {
    view: permissions.SIDmNamTc,
    add:  permissions.SIDmNamTc,
    edit:  permissions.SIDmNamTc,
  },
  formId: "SIDmNamTC-form",
  size: "tiny",
  isScrolling: false,
  initItem: {
    Nam: ZenHelper.getFiscalYear(),
    Ngay_dntc: "",
    Ngay_cntc: "",
    ngay_ks: "",
    Description: "",
    ksd: false,
  },
  formValidation: [
    {
      id: "Nam",
      validationType: "string",
      validations: [
        {
          type: "required",
          params: ["Không được bỏ trống trường này"],
        },
      ],
    },
    {
      id: "Ngay_dntc",
      validationType: "string",
      validations: [
        {
          type: "required",
          params: ["Không được bỏ trống trường này"],
        },
      ],
    },
    {
      id: "Ngay_cntc",
      validationType: "string",
      validations: [
        {
          type: "required",
          params: ["Không được bỏ trống trường này"],
        },
      ],
    },
  ],
};

export { SIDmNamTC };
