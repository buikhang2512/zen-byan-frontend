import React from 'react'
import { Form } from 'semantic-ui-react'
import { ZenField, ZenFieldSelectApi, ZenFieldNumber } from '../../../components/Control'
import { FormMode } from '../../../components/Control/zzControlHelper'
import { IntlFormat } from '../../../utils/intlFormat'
import { ZenLookup } from '../../ComponentInfo/Dictionary/ZenLookup'
import { ApiSIDmTinh } from '../Api'
import { Mess_SIDmTinh } from '../zLanguage/variable'
import * as permissions from './../../../constants/permissions'

const SIDmTinhModal = (props) => {
    const { formik, permission, mode } = props

    return <>
        <Form.Group widths="equal">
        <ZenField required autoFocus readOnly={!permission || (mode === FormMode.ADD ? false : true)}
            name="ma_tinh" props={formik}
            {...IntlFormat.label(Mess_SIDmTinh.Ma)}
            isCode
        />
        <ZenFieldNumber 
         readOnly={!permission}
         label={"PMDmGiaiDoanDa.ordinal"} defaultlabel="Thứ tự hiển thị"
         name="ordinal" props={formik} 
      />
        </Form.Group>
        <ZenField required readOnly={!permission}
            name="ten_tinh" props={formik}
            {...IntlFormat.label(Mess_SIDmTinh.Ten)}
        />
        <ZenFieldSelectApi required readOnly={!permission}
            loadApi
            lookup={ZenLookup.SIDmQuocGia}
            name="ma_quoc_gia" formik={formik}
            {...IntlFormat.label(Mess_SIDmTinh.Quocgia)}
        />
    </>
}

const SIDmTinh = {
    FormModal: SIDmTinhModal,
    api: {
        url: ApiSIDmTinh
    },
    permission: {
        view: permissions.SIDmTinhXem,
        add: permissions.SIDmTinhThem,
        edit: permissions.SIDmTinhSua,
    },
    formId: "sidmtinh-form",
    size: "tiny",
    isScrolling: false,
    initItem: {
        ma_tinh: "",
        ten_tinh: "",
        ma_quoc_gia: "",
        ksd: false,
    },
    formValidation: [
        {
            id: "ma_tinh",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ]
        },
        {
            id: "ten_tinh",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ]
        },
        {
            id: "ma_quoc_gia",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ]
        },
    ]
}
export {SIDmTinh}