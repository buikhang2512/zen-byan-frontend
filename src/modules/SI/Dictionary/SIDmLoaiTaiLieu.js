import React from 'react'
import { ZenField, ZenFieldSelectApi, ZenFieldNumber } from '../../../components/Control'
import { IntlFormat } from '../../../utils/intlFormat'
import { ZenLookup } from '../../ComponentInfo/Dictionary/ZenLookup'
import { ApiSIDmLoaiTaiLieu } from '../Api'
import { FormMode } from '../../../components/Control/zzControlHelper'
import { Form } from 'semantic-ui-react'
import * as permissions from './../../../constants/permissions'

const SIDmLoaiTaiLieuModal = (props) => {
    const { formik, permission, mode } = props

    return <>
        <Form.Group widths="equal">
        <ZenField required autoFocus readOnly={!permission || (mode === FormMode.ADD ? false : true)}
            name="ma_loai" props={formik} isCode
            label="sidmloaitailieu.ma" defaultlabel="Mã loại tài liệu"
        />
        <ZenFieldNumber 
         readOnly={!permission}
         label={"PMDmGiaiDoanDa.ordinal"} defaultlabel="Thứ tự hiển thị"
         name="ordinal" props={formik} 
      />
        </Form.Group>
        <ZenFieldSelectApi required readOnly={!permission}
            lookup={{...ZenLookup.SIDmloai,
                onLocalWhere: (item) => {
                    return item.filter(t => t.ma_nhom == "NHOM_TAI_LIEU") || []
                },
                where: 'ma_nhom = "NHOM_TAI_LIEU"'
            }}
            name="nhom" formik={formik}
            label="sidmloaitailieu.nhom" defaultlabel="Nhóm tài liệu"
        />
        <ZenField required readOnly={!permission}
            name="ten_loai" props={formik}
            label="sidmloaitailieu.ten" defaultlabel="Tên loại"
        />
    </>
}

const SIDmLoaiTaiLieu = {
    FormModal: SIDmLoaiTaiLieuModal,
    api: {
        url: ApiSIDmLoaiTaiLieu
    },
    permission: {
        view: permissions.SIDmLoaiTaiLieuXem,
        add: permissions.SIDmLoaiTaiLieuThem,
        edit: permissions.SIDmLoaiTaiLieuSua,
    },
    formId: "sidmtinh-form",
    size: "tiny",
    initItem: {
        ma_loai: "",
        ten_loai: "",
        ma_nhom: "",
        ordinal: 0,
    },
    formValidation: [
        {
            id: "ma_loai",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ]
        },
        {
            id: "ten_loai",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ]
        },
        {
            id: "nhom",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ]
        },
        {
            id: "ordinal",
            validationType: "number",
            validations: [
                {
                    type: "min",
                    params: [1,"Không được nhỏ hơn 0"]
                },
            ]
        },
    ]
}
export {SIDmLoaiTaiLieu}