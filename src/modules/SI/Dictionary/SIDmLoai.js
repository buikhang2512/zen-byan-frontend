import React from "react"
import { Form } from "semantic-ui-react"
import { ZenField, ZenFieldNumber } from "../../../components/Control"
import { FormMode } from "../../../components/Control/zzControlHelper"
import { IntlFormat } from "../../../utils/intlFormat"
import { ApiSIDmLoai } from "../Api"
import { Mess_SIDmLoai } from "../zLanguage/variable"
import * as permissions from "../../../constants/permissions"

const SIDmLoaiModal = (props) => {
    const { formik, permission, mode } = props
    return  <>
        <Form.Group>
        <ZenField readOnly={true} autoFocus readOnly={!permission || (mode === FormMode.ADD ? false : true)}
            name="ma_nhom" props={formik} isCode
            {...IntlFormat.label(Mess_SIDmLoai.Nhom)}
        />
        <ZenFieldNumber 
         readOnly={!permission}
         label={"PMDmGiaiDoanDa.ordinal"} defaultlabel="Thứ tự hiển thị"
         name="ordinal" props={formik} 
      />
        </Form.Group>
        <ZenField readOnly={true}
            name="ma_loai" props={formik}
            {...IntlFormat.label(Mess_SIDmLoai.Ma)}
        />
        <ZenField readOnly={!permission || (mode === FormMode.EDIT ? false : true)}
            name="ten" props={formik}
            {...IntlFormat.label(Mess_SIDmLoai.Ten)}
        />
    </>
}

const SIDmLoai = {
    FormModal: SIDmLoaiModal,
    api: {
        url: ApiSIDmLoai,
    },

    permission: {
        view: permissions.SIDmLoai,
        add: permissions.SIDmLoai,
        edit: permissions.SIDmLoai
     },

     formId: "sidmloai-form",
     size: "tiny",
     initItem: {
         nhom: "",
         ma_loai: "",
         ten_loai: "",
         ordinal: 0,
     },
     formValidation: [
     ],
}

export { SIDmLoai };