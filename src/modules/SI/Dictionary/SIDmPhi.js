import React from "react";
import { Form } from "semantic-ui-react";
import { ZenField,ZenFieldNumber } from "../../../components/Control/index";
import { FormMode } from "../../../utils";
import { ApiSIDmPhi } from "../Api";

const SIDmPhiModal = (props) => {
  const { formik, permission, mode } = props;

  return (
    <React.Fragment>
      <Form.Group>
      <ZenField
        required
        autoFocus
        readOnly={!permission || (mode === FormMode.ADD ? false : true)}
        label={"sidmphi.ma_phi"}
        defaultlabel="Mã phí"
        name="ma_phi"
        props={formik}
        textCase="uppercase"
        maxLength={20}
        isCode
      />
      <ZenFieldNumber 
         readOnly={!permission}
         label={"PMDmGiaiDoanDa.ordinal"} defaultlabel="Thứ tự hiển thị"
         name="ordinal" props={formik} 
      />
      </Form.Group>
      <ZenField
        required
        readOnly={!permission}
        label={"sidmphi.ten_phi"}
        defaultlabel="Tên phí"
        name="ten_phi"
        props={formik}
      />
    </React.Fragment>
  );
};

const SIDmPhi = {
  FormModal: SIDmPhiModal,
  api: {
    url: ApiSIDmPhi,
  },
  permission: {
    view: "90.02.1",
    add: "90.02.2",
    edit: "90.02.3",
  },
  formId: "sidmphi-form",
  size: "tiny",
  initItem: {
    ma_phi: "",
    ten_phi: "",
    ksd: false,
    ordinal: 0,
  },
  formValidation: [
    {
      id: "ma_phi",
      validationType: "string",
      validations: [
        {
          type: "required",
          params: ["Không được bỏ trống trường này"],
        },
        {
          type: "max",
          params: [20, "Không được vượt quá 20 kí tự"],
        },
      ],
    },
    {
      id: "ten_phi",
      validationType: "string",
      validations: [
        {
          type: "required",
          params: ["Không được bỏ trống trường này"],
        },
      ],
    },
  ],
};

export { SIDmPhi };
