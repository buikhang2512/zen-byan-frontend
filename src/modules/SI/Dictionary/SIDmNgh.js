import React from "react";
import { Form } from "semantic-ui-react";
import { ZenField, ZenFieldNumber } from "../../../components/Control/index";
import { FormMode } from "../../../utils";
import { ApiSIDmNgh } from "../Api";
import * as permissions from "../../../constants/permissions";

const SIDmNghModal = (props) => {
   const { formik, permission, mode } = props

   return <React.Fragment>
      <Form.Group>
      <ZenField required autoFocus readOnly={!permission || (mode === FormMode.ADD ? false : true)}
         label={"sidmngh.ma_ngh"} defaultlabel="Mã ngân hàng"
         name="ma_ngh" props={formik} textCase="uppercase"
         maxLength={20} isCode
      />
      <ZenFieldNumber 
         readOnly={!permission}
         label={"PMDmGiaiDoanDa.ordinal"} defaultlabel="Thứ tự hiển thị"
         name="ordinal" props={formik} 
      />
      </Form.Group>
      <ZenField required readOnly={!permission}
         label={"sidmngh.ten_ngh"} defaultlabel="Tên ngân hàng"
         name="ten_ngh" props={formik}
      />
      <ZenField readOnly={!permission}
         label={"sidmngh.ghi_chu"} defaultlabel="Ghi chú"
         name="ghi_chu" props={formik}
      />
   </React.Fragment>
}

const SIDmNgh = {
   FormModal: SIDmNghModal,
   api: {
      url: ApiSIDmNgh,
   },
   permission: {
      view: permissions.SIDmNghXem,
      add: permissions.SIDmNghThem,
      edit: permissions.SIDmNghSua,
   },
   formId: "sidmngh-form",
   size: "tiny",
   initItem: {
      ma_ngh: "",
      ten_ngh: "",
      ordinal: 0,
      ksd: false
   },
   formValidation: [
      {
         id: "ma_ngh",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
            {
               type: "max",
               params: [20, "Không được vượt quá 20 kí tự"]
            },
         ]
      },
      {
         id: "ten_ngh",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
         ]
      },
   ]
}

export { SIDmNgh };