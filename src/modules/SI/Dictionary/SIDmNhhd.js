import React from "react";
import { Form } from "semantic-ui-react";
import { ZenField,ZenFieldNumber } from "../../../components/Control/index";
import { FormMode } from "../../../utils";
import { ApiSIDmNhhd } from "../Api";

const SIDmNhhdModal = (props) => {
  const { formik, permission, mode } = props;

  return (
    <React.Fragment>
      <Form.Group widths="equal">
      <ZenField
        required
        autoFocus
        readOnly={!permission || (mode === FormMode.ADD ? false : true)}
        label={"sidmnhhd.ma_nhhd"}
        defaultlabel="Mã nhóm hợp đồng"
        name="ma_nhhd"
        props={formik}
        textCase="uppercase"
        maxLength={8}
        isCode
      />
      <ZenFieldNumber
            readOnly={!permission}
            label={"PMDmGiaiDoanDa.ordinal"} defaultlabel="Thứ tự hiển thị"
            name="ordinal" props={formik}
         />
      </Form.Group>
      <ZenField
        required
        readOnly={!permission}
        label={"sidmnhhd.ten_nhhd"}
        defaultlabel="Tên nhóm hợp đồng"
        name="ten_nhhd"
        props={formik}
      />
    </React.Fragment>
  );
};

const SIDmNhhd = {
  FormModal: SIDmNhhdModal,
  api: {
    url: ApiSIDmNhhd,
  },
  permission: {
    view: "90.04.1",
    add: "90.04.2",
    edit: "90.04.3",
  },
  formId: "sidmnhhd-form",
  size: "tiny",
  initItem: {
    ma_nhhd: "",
    ten_nhhd: "",
    ordinal:0,
    ksd: false,
  },
  formValidation: [
    {
      id: "ma_nhhd",
      validationType: "string",
      validations: [
        {
          type: "required",
          params: ["Không được bỏ trống trường này"],
        },
        {
          type: "max",
          params: [8, "Không được vượt quá 8 kí tự"],
        },
      ],
    },
    {
      id: "ten_nhhd",
      validationType: "string",
      validations: [
        {
          type: "required",
          params: ["Không được bỏ trống trường này"],
        },
      ],
    },
  ],
};

export { SIDmNhhd };
