import React, { useEffect } from "react";
import { ZenField, ZenFieldSelectApi, ZenFieldTextArea } from "../../../components/Control/index";
import { FormMode } from "../../../utils";
import { Form } from "semantic-ui-react";
import { ApiSIDmForm } from "../Api";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import * as permissions from "../../../constants/permissions"

const SIDmFormModal = (props) => {
   const { formik, permission, mode } = props

   return <React.Fragment>
       <Form.Group>
        <ZenField width={6} required autoFocus readOnly={!permission || (mode === FormMode.ADD ? false : true)}
            label={"SIDmForm.ma_form"} defaultlabel="Mã form"
            name="ma_form" props={formik}  isCode
        />
        <ZenField width={10} required readOnly={!permission}
            label={"SIDmForm.ten_form"} defaultlabel="Tên form"
            name="ten_form" props={formik}
        />
        </Form.Group>
      <ZenFieldTextArea 
         readOnly={!permission}
         label={"SIDmForm.ghi_chu"} defaultlabel="Mô tả"
         name="ghi_chu" props={formik} 
      />
      <ZenFieldTextArea style={{width:"100%", height: '300px'}}
         readOnly={!permission}
         label={"SIDmForm.form_schema"} defaultlabel="Form Schema (json)"
         name="form_schema" props={formik} 
      />
   </React.Fragment>
}

const SIDmForm = {
   FormModal: SIDmFormModal,
   api: {
      url: ApiSIDmForm,
   },
   permission: {
      view: permissions.SIDmFormXem,
      add: permissions.SIDmBpThem,
      edit: permissions.SIDmBpSua
   },
   formId: "SIDmForm-form",
   size: "tiny",
   isScrolling: false,
   initItem: {
      ma_form: "",
      ten_form: "",
      ghi_chu: "",
      ksd: false
   },
   formValidation: [
      {
         id: "ma_form",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
            // {
            //    type: "max",
            //    params: [8, "Không được vượt quá 8 kí tự"]
            // },
         ]
      },
      {
         id: "ten_form",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
         ]
      },
   ]
}

export { SIDmForm };