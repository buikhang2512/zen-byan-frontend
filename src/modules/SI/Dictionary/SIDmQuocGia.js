import React from "react"
import { Form } from "semantic-ui-react"
import { ZenField , ZenFieldNumber} from "../../../components/Control"
import { IntlFormat } from "../../../utils/intlFormat"
import { ApiSIDmQuocGia } from "../Api"
import { Mess_SIDmQuocGia } from "../zLanguage/variable"
import { FormMode } from '../../../components/Control/zzControlHelper'
import * as permissions from './../../../constants/permissions'

const SIDmQuocGiaModal = (props) => {
    const { formik, permission, mode } = props
    
    return <>
        <Form.Group widths="equal">
        <ZenField width={5} required autoFocus readOnly={!permission || (mode === FormMode.ADD ? false : true)}
            name="ma_quoc_gia" props={formik}
            {...IntlFormat.label(Mess_SIDmQuocGia.Ma)}
            isCode
        />
        <ZenFieldNumber 
         readOnly={!permission}
         label={"PMDmGiaiDoanDa.ordinal"} defaultlabel="Thứ tự hiển thị"
         name="ordinal" props={formik} 
      />
        </Form.Group>
        <Form.Group>
        <ZenField width={16} required readOnly={!permission}
            name="ten_quoc_gia" props={formik}
            {...IntlFormat.label(Mess_SIDmQuocGia.Ten)}
        />
        </Form.Group>
    </>
}

const SIDmQuocGia = {
    FormModal: SIDmQuocGiaModal,
    api: {
        url: ApiSIDmQuocGia,
    },
    permission: {
        view: permissions.SIDmQuocGiaXem,
        add: permissions.SIDmQuocGiaThem,
        edit: permissions.SIDmQuocGiaSua,
    },
    formId: "sidmquocgia-form",
    size: "tiny",
    initItem: {
        ma_quoc_gia: "",
        ten_quoc_gia: "",
        ordinal:0,
        ksd: false
    },
    formValidation: [
        {
           id: "ma_quoc_gia",
           validationType: "string",
           validations: [
              {
                 type: "required",
                 params: ["Không được bỏ trống trường này"]
              },
           ]
        },
        {
           id: "ten_quoc_gia",
           validationType: "string",
           validations: [
              {
                 type: "required",
                 params: ["Không được bỏ trống trường này"]
              },
           ]
        },
     ]
}

export { SIDmQuocGia };