import React, { useEffect, useState } from "react";
import { ZenFieldNumber, ZenFieldSelectApi, ZenField, ZenFieldSelect, ZenFieldCheckbox } from "../../../components/Control/index";
import { ZenHelper } from "../../../utils";
import { Form, Menu, Segment } from "semantic-ui-react";
import { ApiSIDMCT } from "../Api";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import _ from 'lodash';
import { SI_KIEU_TRUNG_SO } from '../SiConst';
import * as permissions from "../../../constants/permissions"
const tabName = {
   KeToan: "ke-toan",
   SoCt: "so-ct",
   TruongPT: "truong-pt"
}

const SIDMCTModal = (props) => {
   const { formik, permission, mode } = props;
   const [state, setState] = useState({ activeTab: tabName.KeToan });
   const { activeTab } = state

   const preViewSoCt = () => {
      let prefix = formik.values['vn_prefix'].trim();
      let suffix = formik.values['vn_suffix'].trim();
      let sequence = formik.values.vn_sequence?.toString().trim() || 0;
      let pattern = formik.values['vn_pattern'].trim();
      let lsequence = sequence ? sequence.length : 0;
      let nwith = ZenHelper.GetNumVl(formik.values['vn_width']);

      if (prefix.includes('{MM}')) {
         prefix = prefix.replace('{MM}', ZenHelper.formatDateTime(new Date(), ("MM")));
      }
      if (prefix.includes('{YY}')) {
         prefix = prefix.replace('{YY}', ZenHelper.formatDateTime(new Date(), ("yyyy")).substring(2));
      }
      if (suffix.includes('{MM}')) {
         suffix = suffix.replace('{MM}', ZenHelper.formatDateTime(new Date(), ("MM")));
      }
      if (suffix.includes('{YY}')) {
         suffix = suffix.replace('{YY}', ZenHelper.formatDateTime(new Date(), ("yyyy")).substring(2));
      }
      if (!nwith) {
         formik.values['vn_width_View'] = sequence;
      } else if (nwith <= lsequence) {
         formik.values['vn_width_View'] = sequence.substring(lsequence - nwith, nwith);
      } else {
         formik.values['vn_width_View'] = (" ".repeat(nwith - lsequence)).split(" ").join(pattern) + sequence;
      }
      formik.values['vn_width_View'] = prefix + formik.values['vn_width_View'] + suffix;
      formik.setFieldValue('vn_width_View', formik.values.vn_width_View)
   }

   const itemChange = (item, func) => {
      formik.values[item.name] = item.value;
      func();
      formik.setFieldValue(item.name, item.value);
   }

   const handleSOCTValueChange = (e, item) => {
      itemChange(item, () => {
         (preViewSoCt());
      });
   }
   const handleSOCTNumValueChange = (item) => {
      item.value = item.floatValue;
      itemChange(item, () => {
         (preViewSoCt());
      });
   }

   return <React.Fragment>
      <Form.Group widths="equal">
         <ZenFieldSelectApi required
            width='5'
            lookup={{ ...ZenLookup.Ma_ct }}
            readOnly={true}
            label={"sidmct.ma_ct"} defaultlabel="Mã CT"
            name="ma_ct" formik={formik}
         />
         <ZenField required autoFocus readOnly={!permission}
            width='11'
            label={"sidmct.ten_ct"} defaultlabel="Tên chứng từ"
            name="ten_ct" props={formik}
         />
      </Form.Group>

      <TabMenu activeTab={activeTab} onChangeTab={(e, { name }) => setState({ ...state, activeTab: name })} />
      <Segment>
         {activeTab === tabName.KeToan && <TabKeToan formik={formik} permission={permission} />}

         {activeTab === tabName.SoCt && <TabSoCT formik={formik} permission={permission}
            handleSOCTValueChange={handleSOCTValueChange}
            handleSOCTNumValueChange={handleSOCTNumValueChange}
         />}

         {activeTab === tabName.TruongPT && <TabTruongPT formik={formik} permission={permission} />}
      </Segment>
   </React.Fragment>
}

const TabMenu = ({ activeTab, onChangeTab }) => {
   return <Menu attached='top' tabular>
      <Menu.Item
         name={tabName.KeToan}
         active={activeTab === tabName.KeToan}
         onClick={onChangeTab}
      >
         Kế toán
      </Menu.Item>
      <Menu.Item
         name={tabName.SoCt}
         active={activeTab === tabName.SoCt}
         onClick={onChangeTab}
      >
         Số chứng từ
      </Menu.Item>
      <Menu.Item
         name={tabName.TruongPT}
         active={activeTab === tabName.TruongPT}
         onClick={onChangeTab}
      >
         Trường phân tích
      </Menu.Item>
   </Menu>
}

const TabKeToan = ({ formik, permission }) => {
   return <>
      <ZenFieldSelectApi loadApi
         lookup={ZenLookup.TK}
         readOnly={!permission}
         label={"sidmct.tk_no"} defaultlabel="Tài khoản nợ mặc định"
         name="tk_no" formik={formik}
      />
      <ZenFieldSelectApi loadApi
         lookup={ZenLookup.TK}
         readOnly={!permission}
         label={"sidmct.tk_co"} defaultlabel="Tài khoản có mặc định"
         name="tk_co" formik={formik}
      />
      <ZenFieldNumber readOnly={!permission}
         label={"sidmct.stt_nkc"} defaultlabel="Thứ tự sắp xếp in sổ nhật ký chung"
         name="stt_nkc" props={formik}
      />
      <ZenFieldNumber readOnly={!permission}
         label={"sidmct.stt_ntxt"} defaultlabel="Thứ tự tính giá NTXT"
         name="stt_ntxt" props={formik}
      />

   </>
}

const TabSoCT = ({ formik, permission,
   handleSOCTNumValueChange, handleSOCTValueChange }) => {
   return <>
      <ZenFieldSelectApi required
         lookup={{ ...ZenLookup.Ma_ct }}
         readOnly={!permission}
         label={"sidmct.ma_ct_me"} defaultlabel="Mã CT mẹ"
         name="ma_ct_me" formik={formik}
      />
      <ZenFieldSelect required
         options={SI_KIEU_TRUNG_SO}
         readOnly={!permission}
         label={"sidmct.kieu_trung_so_ct"} defaultlabel="Kiểu trùng số"
         name="kieu_trung_so_ct" props={formik}
      />

      <Form.Group widths="equal" className="min-width-field">
         <ZenField readOnly={!permission}
            label={"sidmct.vn_prefix"} defaultlabel="Phần đầu số CT"
            name="vn_prefix" props={formik}
            onChange={handleSOCTValueChange}
         />
         <ZenField readOnly={!permission}
            label={"sidmct.vn_pattern"} defaultlabel="Ký tự mẫu"
            name="vn_pattern" props={formik}
            onChange={handleSOCTValueChange}
         />
         <ZenField readOnly={!permission}
            label={"sidmct.vn_suffix"} defaultlabel="Phần sau số CT"
            name="vn_suffix" props={formik}
            onChange={handleSOCTValueChange}
         />
      </Form.Group>

      <Form.Group widths="equal" className="min-width-field">
         <ZenFieldNumber readOnly={!permission}
            label={"sidmct.vn_width"} defaultlabel="Độ rộng phần tự tăng"
            name="vn_width" props={formik}
            decimalScale={0}
            onValueChange={handleSOCTNumValueChange}
         />
         <ZenFieldNumber readOnly={!permission}
            label={"sidmct.vn_sequence"} defaultlabel="Ví dụ phần tự tăng"
            name="vn_sequence" props={formik}
            decimalScale={0}
            onValueChange={handleSOCTNumValueChange}
         />
         <ZenField readOnly={true}
            label={"sidmct.vn_width_View"} defaultlabel="Hiển thị"
            name="vn_width_View" props={formik}
         />
      </Form.Group>
   </>
}

const TabTruongPT = ({ formik, permission }) => {
   return <>
      <ZenFieldCheckbox props={formik}
         name="vv" readOnly={!permission}
         label={"sidmct.vv"} defaultlabel="Sử dụng trường Mã vụ việc/hợp đồng"
         tabIndex={-1}
      />
      <ZenFieldCheckbox props={formik}
         name="phi" readOnly={!permission}
         label={"sidmct.phi"} defaultlabel="Sử dụng trường Mã phí"
         tabIndex={-1}
      />
      <ZenFieldCheckbox props={formik}
         name="bp" readOnly={!permission}
         label={"sidmct.bp"} defaultlabel="Sử dụng trường Mã bộ phận"
         tabIndex={-1}
      />
      <ZenFieldCheckbox props={formik}
         name="spct" readOnly={!permission}
         label={"sidmct.spct"} defaultlabel="Sử dụng trường Mã sản phẩm"
         tabIndex={-1}
      />
   </>
}

const SIDMCT = {
   FormModal: SIDMCTModal,
   api: {
      url: ApiSIDMCT,
   },
   permission: {
      view: permissions.SIDmCt,
      add: permissions.SIDmCt,
      edit: permissions.SIDmCt,
      hiddenKsd: ["edit"]
   },
   formId: "SIDMCT-form",
   size: "small",
   initItem: {
      ma_ct: "",
      ten_ct: ""
   },
   isScrolling: false,
   formValidation: [
      {
         id: "ma_ct",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
            {
               type: "max",
               params: [20, "Không được vượt quá 8 kí tự"]
            },
         ]
      },
      {
         id: "ten_ct",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
         ]
      },
      {
         id: "vn_width",
         validationType: "number",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
            {
               type: "max",
               params: [12, "Không được vượt quá 12"]
            },
            {
               type: "min",
               params: [1, "Không được vượt quá 12"]
            },
         ]
      },
      {
         id: "vn_pattern",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
            {
               type: "max",
               params: [1, "Không được vượt quá 1 kí tự"]
            },
         ]
      },
   ],
   onAfterLoadData: (item) => {
      return new Promise((resolve, reject) => {
         let prefix = item['vn_prefix'].trim();
         let suffix = item['vn_suffix'].trim();
         let sequence = item.vn_sequence?.toString().trim() || 0;
         let pattern = item['vn_pattern'].trim();
         let lsequence = sequence.length;
         let nwith = ZenHelper.GetNumVl(item['vn_width']);

         if (prefix.includes('{MM}')) {
            prefix = prefix.replace('{MM}', ZenHelper.formatDateTime(new Date(), ("MM")));
         }
         if (prefix.includes('{YY}')) {
            prefix = prefix.replace('{YY}', ZenHelper.formatDateTime(new Date(), ("yyyy")).substring(2));
         }
         if (suffix.includes('{MM}')) {
            suffix = suffix.replace('{MM}', ZenHelper.formatDateTime(new Date(), ("MM")));
         }
         if (suffix.includes('{YY}')) {
            suffix = suffix.replace('{YY}', ZenHelper.formatDateTime(new Date(), ("yyyy")).substring(2));
         }
         if (!nwith) {
            item['vn_width_View'] = sequence;
         } else if (nwith <= lsequence) {
            item['vn_width_View'] = sequence.substring(lsequence - nwith, nwith);
         } else {
            item['vn_width_View'] = (" ".repeat(nwith - lsequence)).split(" ").join(pattern) + sequence;
         }
         item['vn_width_View'] = prefix + item['vn_width_View'] + suffix;
         resolve(item);
      })
   }
}

export { SIDMCT };