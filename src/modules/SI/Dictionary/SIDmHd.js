import React, { useState } from "react";
import {
   ZenField, ZenFieldSelectApi, ZenFieldDate,
   ZenFieldNumber, ZenFieldTextArea
} from "../../../components/Control/index";
import { Form, Menu, Table, TableCell, Grid } from "semantic-ui-react";
import { FormMode, ZenHelper } from "../../../utils";
import { ApiSIDmHd } from "../Api";
import { ZenLookup } from "../../ComponentInfo/Dictionary/ZenLookup";
import { IntlFormat } from "../../../utils/intlFormat";
import { Mess_SIDmHd } from "../zLanguage/variable";
import { Mess_ARDmKh } from "../../AR/zLanguage/variable";
import {
   VoucherHelper,
   NumberCell, TextCell, DeleteCell, RowHeaderCell,
   ActionType, TableScroll, TableTotalPH, RowTotalPH
} from "../../../components/Control/zVoucher";
import * as routes from './../../../constants/routes'
import { zzControlHelper } from "../../../components/Control/zzControlHelper";
import * as permissions from "../../../constants/permissions";

const optionsLoai = [
   { key: 1, value: "1", text: "Bán" },
   { key: 2, value: "2", text: "Mua" }
]

const tabName = {
   HD: "hong-dong",
   HH: "hang-hoa",
}

const SIDmHdModal = (props) => {
   const { formik, permission, mode } = props
   const [state, setState] = useState({ activeTab: tabName.HD });
   const { activeTab } = state
   // didmountcomponent
   React.useEffect(() => {
      if (mode === FormMode.ADD) {
         let today = ZenHelper.formatDateTime(new Date(), "YYYY-MM-DD")
         formik.setValues({
            ...formik.values,
            ngay_hd: today, ngay_hh: today, loai: "1"
         })
      }
   }, []);

   const handleItemSelected = (option, { name }) => {
      if (name === "id_nvkd") {
         formik.setFieldValue("ten_nvkd", option.ho_ten)
      } else if (name === "ma_kh") {
         formik.setFieldValue("ten_kh", option.ten_kh)
      } else if (name === "ma_nhhd") {
         formik.setFieldValue("ten_nhhd", option.ten_nhhd)
      }
   }

   return <React.Fragment>
      <TabMenu activeTab={activeTab} onChangeTab={(e, { name }) => setState({ ...state, activeTab: name })} />
      {activeTab === tabName.HD && <><Form.Group>
         <ZenField required autoFocus readOnly={!permission || (mode === FormMode.ADD ? false : true)}
            name="ma_hd" props={formik} textCase="uppercase"
            width="7" isCode
            {...IntlFormat.label(Mess_SIDmHd.Ma)}
         />
         <ZenField required readOnly={!permission}
            name="so_hd" props={formik}
            width="6"
            {...IntlFormat.label(Mess_SIDmHd.SoHd)}
         />
         <ZenFieldSelectApi readOnly={!permission}
            lookup={{
               ...ZenLookup.SIDmloai, format: `{ten}`,
               onLocalWhere: (items) => {
                  return items.filter(t => t.ma_nhom === 'HOP_DONG') || []
               },
               where: "MA_NHOM: 'HOP_DONG'"
            }}
            name="loai" formik={formik}
            width="3"
            {...IntlFormat.label(Mess_SIDmHd.LoaiHD)}
         />
      </Form.Group>
         <Form.Group>
            <ZenField required readOnly={!permission}
               name="ten_hd" props={formik}
               width="16"
               {...IntlFormat.label(Mess_SIDmHd.Ten)}
            />

         </Form.Group>
         <Form.Group widths="equal">
            <ZenFieldSelectApi loadApi width={16}
               lookup={ZenLookup.Ma_kh}
               name="ma_kh" formik={formik}
               readOnly={!permission}
               onItemSelected={handleItemSelected}
               {...IntlFormat.label(Mess_ARDmKh.Header)}
            />
         </Form.Group>
         <Form.Group widths="equal">
            <ZenFieldSelectApi loadApi
               lookup={ZenLookup.Ma_nhhd}
               name="ma_nhhd" formik={formik}
               readOnly={!permission}
               {...IntlFormat.label(Mess_SIDmHd.NhomHD)}
               onItemSelected={handleItemSelected}
            />
            <ZenFieldSelectApi loadApi
               lookup={ZenLookup.ID_NV}
               name="id_nvkd" formik={formik}
               readOnly={!permission}
               label={"SiDmHd.nvkh"} defaultlabel="Nhân viên kinh doanh"
               onItemSelected={handleItemSelected}
            />
         </Form.Group>
         <Form.Group >
            <ZenFieldDate width="8" readOnly={!permission}
               name="ngay_hd" props={formik}
               {...IntlFormat.label(Mess_SIDmHd.NgayHd)}
            />
            <ZenFieldDate width="8" readOnly={!permission}
               name="ngay_hl" props={formik}
               label={"SiDmHd.ngay_hl"} defaultlabel="Ngày hiệu lực"
            />
            <ZenFieldDate width="8" readOnly={!permission}
               name="ngay_hh" props={formik}
               {...IntlFormat.label(Mess_SIDmHd.NgayHh)}
            />
         </Form.Group>

         <Form.Group widths="equal">
            <ZenFieldSelectApi
               lookup={ZenLookup.Ma_nt}
               name="ma_nt" formik={formik}
               readOnly={!permission}
               {...IntlFormat.label(Mess_SIDmHd.NgoaiTe)}
            />
            <ZenFieldNumber name="tien" readOnly={!permission}
               decimalScale={0} props={formik}
               label={"SiDmHd.tien"} defaultlabel="Giá trị"
            />
            <ZenFieldNumber name="tien_nt" readOnly={!permission}
               decimalScale={2} props={formik}
               label={"SiDmHd.tien_nt"} defaultlabel="Giá trị NT"
            />
         </Form.Group>

         <ZenFieldTextArea readOnly={!permission}
            name="noi_dung" props={formik}
            {...IntlFormat.label(Mess_SIDmHd.NoiDung)}
         /></>}
      {activeTab === tabName.HH && <TabHH formik={formik} permission={permission}
      // handleSOCTValueChange={handleSOCTValueChange}
      // handleSOCTNumValueChange={handleSOCTNumValueChange}
      />}
   </React.Fragment>
}

const TabMenu = ({ activeTab, onChangeTab }) => {
   return <Menu pointing secondary>
      <Menu.Item
         name={tabName.HD}
         active={activeTab === tabName.HD}
         onClick={onChangeTab}
      >
         Thông tin
      </Menu.Item>
      <Menu.Item
         name={tabName.HH}
         active={activeTab === tabName.HH}
         onClick={onChangeTab}
      >
         Hàng hóa/dịch vụ
      </Menu.Item>
   </Menu>
}

const TabHH = ({ formik, permission, handleSOCTNumValueChange, handleSOCTValueChange }) => {

   const switchAction = (propsElement, type, itemCurrent, index) => {
      const { name, value, itemSelected } = propsElement;
      const { cts } = formik.values;

      switch (type) {
         case ActionType.TEXT_CHANGE:
            const { lookup } = propsElement;
            // set value
            itemCurrent[name] = value;
            let calcPH = {};

            if (lookup) {
               if (name === "ma_vt") {
                  itemCurrent["ten_vt"] = itemSelected["ten_vt"];
                  itemCurrent["dvt"] = itemSelected["dvt"];
               }
            }
            cts[index] = { ...itemCurrent };
            formik.setFieldValue('cts', cts)
            break;

         case ActionType.NUMBER_CHANGE:
            f_calcNumber(propsElement, type, itemCurrent, index);
            break;

         case ActionType.ADD_ROW:
            let newCT = {
               ma_vt: value,
               ten_vt: itemSelected.ten_vt,
               dvt: itemSelected.dvt,
               tk_vt: itemSelected.tk_vt,
               ts_gtgt: itemSelected.ts_gtgt,

               so_luong: 0,
               gia: 0,
               gia2: 0,
               gia_nt: 0,
               tien: 0,
               tien2: 0,
               tien_nt: 0,
               thue_gtgt_nt: 0,
               thue_gtgt: 0,
            };
            if (itemSelected.ma_kho) {
               newCT.ma_kho = itemSelected.ma_kho;
            }

            cts.push(newCT)
            formik.setFieldValue('cts', cts)
            break;

         case ActionType.DELETE_ROW:
            // xóa dòng CT, tính lại PH
            let ctDel = cts.filter((item, idx) => idx !== index);
            const totalPH = {
               t_so_luong: VoucherHelper.f_SumArray(ctDel, "so_luong"),
               t_tien0: VoucherHelper.f_SumArray(ctDel, "tien0"),
               t_tien_nt0: VoucherHelper.f_SumArray(ctDel, "tien_nt0"),
               t_tien: VoucherHelper.f_SumArray(ctDel, "tien"),
               t_tien_nt: VoucherHelper.f_SumArray(ctDel, "tien_nt"),
               t_cp: VoucherHelper.f_SumArray(ctDel, "cp"),
               t_cp_nt: VoucherHelper.f_SumArray(ctDel, "cp_nt"),
               t_tt: VoucherHelper.f_SumArray(ctDel, "tien"),
               t_tt_nt: VoucherHelper.f_SumArray(ctDel, "tien_nt"),
            };
            const newcts = cts.filter((t, indx) => indx !== index)
            //formik.setFieldValue('cts', newcts)
            formik.setValues({
               ...formik.values,
               ...totalPH,
               cts: newcts,
            })
            break;
         default:
            console.log(type);
            break;
      }
   };

   function f_calcNumber(propsElement, type, itemCurrent, index) {
      const { cts } = formik.values
      const { name, value } = propsElement
      if (name === 'so_luong') {
         itemCurrent.so_luong = value ? value : 0
         changeSoLuong(itemCurrent)
      } else if (name === 'gia2') {
         itemCurrent.gia2 = value ? value : 0
         changeGia2(itemCurrent)
      } else if (name === 'gia_nt2') {
         changeGia_nt2(itemCurrent)
      } else if (name === 'tien2') {
         changeTien2(itemCurrent)
      } else if (name === 'tien_nt2') {
         changeTien_nt2(itemCurrent)
      } else if (name === 'ts_gtgt' || name === "ma_vt") {
         itemCurrent.ts_gtgt = value ? value : 0
         changeMa_thue(itemCurrent)
      } else if (name === 'thue_gtgt') {
         changeThue_gtgt(itemCurrent)
      } else if (name === 'thue_gtgt_nt') {
         changeThue_gtgt_nt(itemCurrent)
      } else if (name === 'tl_ck') {
         changeTl_ck(itemCurrent)
      } else if (name === 'tien_ck') {
         changeTien_ck(itemCurrent)
      } else if (name === 'tien_ck_nt') {
         changeTien_ck_nt(itemCurrent)
      } else if (name === 'gia') {
         changeGia(itemCurrent, value)
      } else if (name === 'gia_nt') {
         changeGia_nt(itemCurrent)
      }
      //cập nhật PH, CT
      VoucherHelper.f_Timeout(() => {
         const totalPH = calcTotalPH(itemCurrent, index)
         cts[index] = { ...itemCurrent };
         //formik.setFieldValue('cts',cts);
         formik.setValues({
            ...formik.values,
            ...totalPH,
            cts: cts,
         })
      })
   }

   // =============================== VALUE CHANGE
   function changeSoLuong(item) {
      f_calcTien2(item)
      f_calcTien_Ck(item)
      f_calcThue(item)
      f_calcTT(item)
      f_calcTien(item) // tiền vốn
   }
   function changeGia2(item) {
      changeSoLuong(item)
   }
   // function changeGia_nt2(item) {
   //    item.gia2 = Math.round(item.gia_nt2 * ph.ty_gia)
   //    changeSoLuong(item)
   // }
   function changeTien2(item) {
      item.tien_nt2 = item.tien2
      f_calcThue(item)
      f_calcTien_Ck(item)
      f_calcTT(item)
   }

   function changeTl_ck(item) {
      f_calcTien_Ck(item)
      f_calcThue(item)
      f_calcTT(item)
   }
   function changeTien_ck(item) {
      item.tien_ck_nt = item.tien_ck

      f_calcThue(item)
      f_calcTT(item)
   }

   function changeMa_thue(item) {
      f_calcThue(item)
      f_calcTT(item)
   }
   function changeThue_gtgt(item) {
      //item.thue_gtgt = Math.round(item.thue_gtgt / ph.ty_gia, ph.ma_nt)
      f_calcTT(item)
   }

   function changeGia(item) {
      item.gia_nt = item.gia

      changeSoLuong(item)
   }

   // ==================================== tính toán
   function f_calcTien2(item) {
      item.tien2 = Math.round(item.so_luong * item.gia2)

   }
   function f_calcTien_Ck(item) {
      item.tien_ck = item.tien_ck_nt

   }
   function f_calcThue(item) {
      item.thue_gtgt = item.tien2 * item.ts_gtgt / 100
   }
   function f_calcTT(item) {
      item.tt = item.tien_nt2 + item.thue_gtgt_nt - item.tien_ck_nt
   }
   function f_calcTien(item) {
      item.tien = item.tien_nt

   }
   function calcTotalPH(itemCurrent, index) {
      const { cts } = formik.values
      // tạo CT mới
      const newCT = cts.map((item, idx) => idx === index ? itemCurrent : item)
      // tính tổng PH
      const totalPH = {
         t_so_luong: VoucherHelper.f_SumArray(newCT, 'so_luong'),
         t_tien2: VoucherHelper.f_SumArray(newCT, 'tien2'),
         t_tien_nt2: VoucherHelper.f_SumArray(newCT, 'tien_nt2'),
         t_thue: VoucherHelper.f_SumArray(newCT, 'thue_gtgt'),
         t_thue_nt: VoucherHelper.f_SumArray(newCT, 'thue_gtgt_nt'),
         t_ck: VoucherHelper.f_SumArray(newCT, 'tien_ck'),
         t_ck_nt: VoucherHelper.f_SumArray(newCT, 'tien_ck_nt'),
         t_tt: 0,
         //t_tt_nt: 0
      }

      totalPH.t_tt = totalPH.t_tien2 + totalPH.t_thue - totalPH.t_ck
      //totalPH.t_tt_nt = totalPH.t_tien_nt2 + totalPH.t_thue_nt - totalPH.t_ck_nt
      return totalPH
   }

   const onChangeVT = (propsElement, item, index) => {
      const { cts } = formik.values;
      const { options, name, value } = propsElement
      let option;
      if (propsElement.option) {
         option = propsElement.option
      } else {
         option = options.filter(t => t.ma_vt == value)[0]
      }
      item[name] = value;
      item["ten_vt"] = option["ten_vt"];
      item["dvt"] = option["dvt"];
      item["ts_gtgt"] = option["ts_gtgt"]
      cts[index] = { ...item };
      formik.setFieldValue('cts', cts)
   }

   const onChangetext = (propsElement, item, index) => {
      const { cts } = formik.values;
      const { name, value } = propsElement
      item[name] = value;
      cts[index] = { ...item };
      formik.setFieldValue('cts', cts)
   }

   const addRow = (propsElement) => {
      const { cts } = formik.values
      const { options, name, value } = propsElement
      let option;
      if (propsElement.option) {
         option = propsElement.option
      } else {
         option = options.filter(t => t.ma_vt == value)[0]
      }
      let newCT = ({
         ma_hd: formik.values.ma_hd,
         ma_vt: value,
         ten_vt: option.ten_vt,
         dvt: option.dvt,
         ten_dvt: "",
         so_luong: 0,
         gia2: 0,
         tien2: 0,
         ts_gtgt: option.ts_gtgt,
         thue: 0,
         tt: 0,
         ghi_chu: ""
      })
      if (cts) {
         cts.push(newCT)
         formik.setFieldValue('cts', cts)
      } else {
         formik.setFieldValue('cts', [newCT])
      }
   }

   return <>
      <div style={{ minHeight: "490px" }}>
         <TableScroll>
            <Table.Header>
               <RowHeaderCell colAction={!permission}
                  header={[
                     { text: ['so3.ma_vt', 'Mã HH/DV'], },
                     { text: ['so3.ten_vt', 'Tên HH/DV'] },
                     { text: ['so3.dvt', 'ĐVT'] },
                     { text: ['so3.so_luong', 'Số lượng'] },
                     { text: ['so3.gia_nt2', 'Đơn giá'] },
                     { text: ['so3.tien_nt2', 'Thành tiền'] },
                     { text: ['so3.ts_gtgt', '% VAT'] },
                     { text: ['so3.ghi_chu', 'Ghi chú'] },
                  ]}
               />
            </Table.Header>
            <Table.Body>
               {formik.values.cts && formik.values.cts.map((item, index) => {

                  return <Table.Row key={index}>
                     <Table.Cell>
                        <ZenFieldSelectApi
                           rowItem={item} index={index}
                           style={{ margin: '0', width: "200px" }}
                           readOnly={!permission}
                           lookup={{ ...ZenLookup.Ma_vt }}
                           name='ma_vt'
                           clearable
                           value={item.ma_vt} index={index}
                           onChange={(e, propsElement) => onChangeVT(propsElement, item, index)}
                        />
                     </Table.Cell>

                     <TextCell widthCell={1}
                        name="ten_vt" rowItem={item} index={index}
                        placeholder={['so0.ma_vt', 'Tên hàng hóa dịch vụ']}
                        readOnly={!permission}
                        onChange={switchAction}
                     //autoFocus={(item.autoFocus && .length === index + 1) ? true : false}
                     />
                     <TableCell>
                        <ZenFieldSelectApi
                           rowItem={item} index={index}
                           style={{ margin: '0', width: "200px" }}
                           readOnly={!permission}
                           lookup={{ ...ZenLookup.Ma_dvt }}
                           name='dvt'
                           clearable
                           value={item.dvt} index={index}
                           onChange={(e, propsElement) => onChangetext(propsElement, item, index)}
                        />
                     </TableCell>

                     <NumberCell name="so_luong"
                        rowItem={item} index={index}
                        placeholder={["so3.so_luong", "Số lượng"]}
                        onChange={switchAction}
                        readOnly={!permission}
                     />

                     <NumberCell name="gia2"
                        rowItem={item} index={index}
                        placeholder={["so3.gia", "Giá", { ma_nt: 'VND' }]}
                        onChange={switchAction}
                        readOnly={!permission}
                     />

                     <NumberCell name="tien2"
                        rowItem={item} index={index}
                        placeholder={["so3.tien", "thành tiền", { ma_nt: 'VND' }]}
                        onChange={switchAction}
                        readOnly={!permission}
                     />
                     <TableCell>
                        <ZenFieldSelectApi
                           rowItem={item} index={index}
                           readOnly={!permission}
                           lookup={{ ...ZenLookup.Ma_thue }}
                           style={{ margin: '0', width: "100px" }}
                           name='ts_gtgt'
                           clearable
                           value={item.ts_gtgt} index={index}
                           onChange={(e, propsElement) => switchAction(propsElement, 'number_change', item, index)}
                        />
                     </TableCell>
                     <TextCell name="ghi_chu"
                        rowItem={item} index={index}
                        placeholder={["so3.ghi_chu", "Ghi chú", { ma_nt: 'VND' }]}
                        onChange={switchAction}
                        readOnly={!permission}
                     />
                     {/* <NumberCell name="thue_gtgt"
                        rowItem={item} index={index}
                        placeholder={["so3.thue_gtgt", "VAT VND", { ma_nt: 'VND' }]}
                        onChange={switchAction}
                        readOnly={!permission}
                     /> */}

                     {!permission === false && <DeleteCell collapsing
                        rowItem={item} index={index}
                        onClick={switchAction}
                     />}
                  </Table.Row>
               })
               }

               {!permission === false && <Table.Row>
                  <Table.Cell>
                     <ZenFieldSelectApi name="addRow"
                        lookup={ZenLookup.Ma_vt}
                        placeholder={'Mã VT'}
                        onChange={(e, propsElement) => addRow(propsElement)}
                        value=""
                     />
                  </Table.Cell>
                  <Table.Cell colSpan={12} />
                  <Table.Cell colSpan={24} />
               </Table.Row>}
            </Table.Body>
         </TableScroll>
         <div style={{
            paddingTop: "30px"
         }}>
            <Grid columns="2">
               <Grid.Column width="8" />
               <Grid.Column width="8" textAlign="right">
                  <TableTotalPH maNt={formik.values.ma_nt}>
                     <RowTotalPH text="Tổng số lượng" value={formik.values.t_so_luong} maNt={formik.values.ma_nt} oneCol />
                     <RowTotalPH text="Tổng tiền" value={formik.values.t_tien2} valueNT={formik.values.t_tien_nt2} maNt={formik.values.ma_nt} />
                     <RowTotalPH text="Thuế GTGT" value={formik.values.t_thue} valueNT={formik.values.t_thue_nt} maNt={formik.values.ma_nt} />
                     <RowTotalPH text="Tổng thanh toán" value={formik.values.t_tt} valueNT={formik.values.t_tt_nt} maNt={formik.values.ma_nt} />
                  </TableTotalPH>
               </Grid.Column>
            </Grid>
         </div>
      </div>
   </>
}



const SIDmHd = {
   FormModal: SIDmHdModal,

   saveAndRedirect: {
      linkto: (item) => {
         const id = zzControlHelper.btoaUTF8(item['ma_hd'])
         window.location.assign(`#${routes.SIDmHdDetail(id)}`)
      }
   },

   api: {
      url: ApiSIDmHd,
   },
   permission: {
      view: permissions.SIDmHdXem,
      add: permissions.SIDmHdThem,
      edit: permissions.SIDmHdSua,
   },
   formId: "sidmhd-form",
   fieldCode: "ma_hd",
   size: "large",
   initItem: {
      ma_hd: "", ten_hd: "", so_hd: "", noi_dung: "",
      ngay_hd: "", ngay_hh: "", ngay_hl: "",
      tien: "", tien_nt: "",
      loai: "", ma_nhhd: "", ma_kh: "", ma_nt: "VND",
      ordinal: 0,
      cts: [],
      ksd: false
   },
   formValidation: [
      {
         id: "ma_hd",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
            {
               type: "max",
               params: [20, "Không được vượt quá 20 kí tự"]
            },
         ]
      },
      {
         id: "ten_hd",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
            {
               type: "max",
               params: [255, "Không được vượt quá 255 kí tự"]
            },
         ]
      },
      {
         id: "so_hd",
         validationType: "string",
         validations: [
            {
               type: "required",
               params: ["Không được bỏ trống trường này"]
            },
            {
               type: "max",
               params: [50, "Không được vượt quá 50 kí tự"]
            },
         ]
      },
   ]
}

export { SIDmHd };