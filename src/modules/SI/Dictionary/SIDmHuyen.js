import React from 'react'
import { ZenField, ZenFieldSelectApi, ZenFieldNumber } from '../../../components/Control'
import { IntlFormat } from '../../../utils/intlFormat'
import { ZenLookup } from '../../ComponentInfo/Dictionary/ZenLookup'
import { ApiSIDmHuyen } from '../Api'
import { Mess_SIDmHuyen } from '../zLanguage/variable'
import { FormMode } from '../../../components/Control/zzControlHelper'
import { Form } from 'semantic-ui-react'
import * as permissions from './../../../constants/permissions'

const SIDmHuyenModal = (props) => {
    const { formik, permission, mode } = props

    return <>
        <Form.Group widths="equal">
        <ZenField required autoFocus readOnly={!permission || (mode === FormMode.ADD ? false : true)}
            name="ma_huyen" props={formik} isCode
            {...IntlFormat.label(Mess_SIDmHuyen.Ma)}
        />
        <ZenFieldNumber 
         readOnly={!permission}
         label={"PMDmGiaiDoanDa.ordinal"} defaultlabel="Thứ tự hiển thị"
         name="ordinal" props={formik} 
      />
        </Form.Group>
        <ZenField required readOnly={!permission}
            name="ten_huyen" props={formik}
            {...IntlFormat.label(Mess_SIDmHuyen.Ten)}
        />
        <ZenFieldSelectApi required readOnly={!permission}
            loadApi
            lookup={ZenLookup.SIDmTinh}
            name="ma_tinh" formik={formik}
            {...IntlFormat.label(Mess_SIDmHuyen.Tinh)}
        />
    </>
}

const SIDmHuyen = {
    FormModal: SIDmHuyenModal,
    api: {
        url: ApiSIDmHuyen
    },
    permission: {
        view: permissions.SIDmHuyenXem,
        add: permissions.SIDmHuyenThem,
        edit: permissions.SIDmHuyenSua,
    },
    formId: "sidmtinh-form",
    size: "tiny",
    isScrolling: false,
    initItem: {
        ma_huyen: "",
        ten_huyen: "",
        ma_tinh: "",
        ordinal: 0,
        ksd: false,
    },
    formValidation: [
        {
            id: "ma_huyen",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ]
        },
        {
            id: "ten_huyen",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ]
        },
        {
            id: "ma_tinh",
            validationType: "string",
            validations: [
                {
                    type: "required",
                    params: ["Không được bỏ trống trường này"]
                },
            ]
        },
    ]
}
export {SIDmHuyen}