export const Home = "/home";
/**
 * Route for Account modules
 */
export const Login = "/login";
export const Logout = "/account/logout";
export const UserProfile = "/account/profile";
export const ChangePassword = "/account/change-password";
export const NotPermission = "/permission-error";
export const SystemInfo = "/system-info";

export const Register = "/register";
export const RegisterTks = "/thanks-for-register";
export const ForgotPassword = "/forgot-password";
export const ForgotPasswordTks = "/reset-password-success";
export const Activate = (id) => {
  return id ? `/register-confirm/${id}` : "/register-confirm/:id";
};

export const Settings = "/settings";
export const Role = "/settings/roles";
export const RoleInfo = (id) => {
  return id ? `/settings/roles/${id}` : "/settings/roles/:id";
};
export const RoleInfoDashlet = (id) => {
  return id ? `/settings/roles/${id}/dashlet` : "/settings/roles/:id/dashlet";
};
export const User = "/settings/users";
export const Permission = "/permissions";
export const SetupModule = "/settings/setup-module";

export const ReportModule = "/reports";
export const PostMessage = "/posts"
export const PostMessageNew = "/posts/new"
export const PostMessageDetail = (id) => { return id ? `/post/${id}` : "/post/:id" };
export const EODmLoaiPost = "/eodmloaipost"

export const Company = "/company";
/**
 **************** SI
 **/
export const SysDashLet = "/sys-dashlet"
export const SysDashBoardTemplate = "/sys-dashboardtemplate"
export const SysDashBoardTemplateCustom = (id) => { return id ? `/sys-dashboardtemplate/${id}` : `/sys-dashboardtemplate/:id`};

export const MailSetting = "/settings/mail"
export const SIDmHuyen = `/settings/sidmhuyen`
export const SIDmTinh = `/settings/sidmtinh`
export const SIDmloai = `/settings/sidmloai`
export const SIDmquocgia = `/settings/sidmquocgia`
export const SIDmBp = "/settings/sidmbp";
export const SIDmHd = "/sidmhd";
export const SIDmLoaitailieu = "/sidmloaitailieu";
export const SIDmHd2 = "/sidmhd2";
export const SIDmNhhd = "/settings/sidmnhhd";
export const SIDmNhhd2 = "/sidmnhhd2";
export const SIDmPhi = "/settings/sidmphi";
export const SIDmNgh = "/sidmngh";
export const SIDmHtttPO = "/SIDmHtttPO";
export const SIDmHtttSO = "/SIDmHtttSO";
export const SIDmNamTC = "/settings/SIDmNamTC";
export const SIDMCT = "/settings/sidmct";
export const SIDMNT = "/settings/sidmnt";
export const SIDMTGNT = "/settings/sidmtgnt";

export const SIDmDanhXung = "/dm/SIDmDanhXung";
export const SIDmHdDetail = (id) => { return id ? `/sidmhd/${id}` : "/sidmhd/:id" };

export const sidmform = '/sidmform';
export const sidmformDetail = (id) => { return id ? `/sidmform/${id}` : "/sidmform/:id" };
/**
 **************** CA
**/
export const CADmNv = "/cadmnv"
export const CADmKu = "/cadmku";
export const CACdKu = "/cacdku";

export const CAVchCA1 = "/ca1"
export const CAVchCA1New = "/ca1/new"
export const CAVchCA1Edit = (id) => { return id ? `/ca1=${id}` : "/ca1=:id" };
export const CAVchCA2 = "/ca2"
export const CAVchCA2New = "/ca2/new"
export const CAVchCA2Edit = (id) => { return id ? `/ca2=${id}` : "/ca2=:id" };
export const CAVchCA3 = "/ca3"
export const CAVchCA3New = "/ca3/new"
export const CAVchCA3Edit = (id) => { return id ? `/ca3=${id}` : "/ca3=:id" };
export const CAVchCA4 = "/ca4"
export const CAVchCA4New = "/ca4/new"
export const CAVchCA4Edit = (id) => { return id ? `/ca4=${id}` : "/ca4=:id" };

export const RptCARptTMNH01 = "/rpt/CARptTMNH01" // Sổ quỹ tiền mặt
export const RptCARptTMNH0102 = "/rpt/CARptTMNH0102" // Sổ kế toán chi tiết quỹ tiền mặt
export const RptCARptTMNH02 = "/rpt/CARptTMNH02" // Sổ tiển gửi ngân hàng

/**
 **************** CO
**/

export const CoDmSpct = `/codmctrinh`

/**
 **************** CRM
**/

export const CRMNguonKh = `/settings/crmnguonkh`
export const CRMLinhVuc = `/settings/CRMLinhVuc`
export const CRMLoaiHoatDong = `/settings/crmloaihoatdong`
export const CRMDealStatus = "/CRMDeakStatus"
export const CRMDealUuTien = "/CRMDealUuTien"
export const CRMDealGiaiDoan = "/CRMDealGiaiDoan"
export const CRMDealLyDoThua = "/CRMDealLyDoThua"

export const CRMDeal = "/deals"
export const CRMDealDetail = (id) => { return id ? `/deal/${id}` : "/deal/:id" }

export const CRMLead = "/leads"
export const CRMLeadDetail = (id) => { return id ? `/lead/${id}` : "/lead/:id" }

/**
 **************** AR
 **/
export const ARDmNhkh = "/ardmnhkh";
export const ARDmPlkh = "/settings/ardmplkh";
export const ARDmkh = "/ardmkh";
export const ARDmKhDetail = (id) => { return id ? `/ardmkh/${id}` : "/ardmkh/:id" }
export const ARDmNcc = "/ardmncc";
export const ARCDKH = "/settings/arcdkh";

export const RptARRptBCCN0101 = "/rpt/ARRptBCCN0101"; // Sổ chi tiết công nợ 1 khách hàng
export const RptARRptBCCN0102 = "/rpt/ARRptBCCN0102";
export const RptARRptBCCN02 = "/rpt/ARRptBCCN02";
export const RptARRptBCCN03 = "/rpt/ARRptBCCN03";

/**
 **************** PM
**/
export const PMDMNhDa = '/pmdmnhomda';
export const PMDMGiaiDoanDa = '/pmdmgiaidoanda';
export const PMDuAn = '/pmduan';
export const PMDuAnDetail = (id) => { return id ? `/pmduan/${id}` : "/pmduan/:id" }
/**
 **************** PO
**/
export const PODmCp = "/podmcp"
export const POVchPO3 = "/po3"
export const POVchPO3New = "/po3/new"
export const POVchPO3Edit = (id) => { return id ? `/po3=${id}` : "/po3=:id" };
export const POVchPO4 = "/po4"
export const POVchPO4New = "/po4/new"
export const POVchPO4Edit = (id) => { return id ? `/po4=${id}` : "/po4=:id" };
export const POVchPO5 = "/po5"
export const POVchPO5New = "/po5/new"
export const POVchPO5Edit = (id) => { return id ? `/po5=${id}` : "/po5=:id" };
export const POVchPO7 = "/po7"
export const POVchPO7New = "/po7/new"
export const POVchPO7Edit = (id) => { return id ? `/po7=${id}` : "/po7=:id" };
export const POVchPO1 = "/po1"
export const POVchPO1New = "/po1/new"
export const POVchPO1Edit = (id) => { return id ? `/po1=${id}` : "/po1=:id" };
export const PORptBK01 = "/rpt/PORptBK01"
export const PORptBK02 = "/rpt/PORptBK02"
export const PORptTH01 = "/rpt/PORptTH01"
/**
 **************** SO
**/
export const SODmTs = "/settings/sodmts"
export const SoDmNVKD = "/sodmnvkd"
export const SoDmGiaBan = "/sodmgiaban"
export const SoDmMhd = "/sodmmhd"

export const SOVchSO0 = "/so0"
export const SOVchSO0New = "/so0/new"
export const SOVchSO0Edit = (id) => { return id ? `/so0=${id}` : "/so0=:id" };
export const SOVchSO1 = "/so1"
export const SOVchSO1New = "/so1/new"
export const SOVchSO1Edit = (id) => { return id ? `/so1=${id}` : "/so1=:id" };
export const SOVchSO3 = "/so3"
export const SOVchSO3New = "/so3/new"
export const SOVchSO3Edit = (id) => { return id ? `/so3=${id}` : "/so3=:id" };
export const SOVchSO4 = "/so4"
export const SOVchSO4New = "/so4/new"
export const SOVchSO4Edit = (id) => { return id ? `/so4=${id}` : "/so4=:id" };
export const SORptBK01 = "/rpt/SORptBK01"
export const SORptBK02 = "/rpt/SORptBK02"
export const SORptTH01 = "/rpt/SORptTH01"
export const SORptBCPT0401 = "/rpt/SORptBCPT0401"
export const SORptBCPT0402 = "/rpt/SORptBCPT0402"
export const SORptBCPT06 = "/rpt/SORptBCPT06"
export const SORptLaiLo = "/rpt/SORptLaiLo"
/**
 **************** IN
 **/
export const INDmNhvt = "/indmnhvt";
export const INDmPlvt = "/indmplvt";
export const INDmDvt = "/settings/indmdvt";
export const INDmKho = "/indmkho";
export const INDmLo = "/indmlo";
export const INDmViTri = "/indmvitri";
export const INCDVT = "/settings/incdvt";
export const INDMVT = "/indmvt";
export const INCHUYENTONKHO = "/dm/incdvt";
export const INTINHGIATB = "/tinhgiatb";

export const INVchIN1 = "/in1";
export const INVchIN1New = "/in1/new";
export const INVchIN1Edit = (id) => {
  return id ? `/in1=${id}` : "/in1=:id";
};
export const INVchIN2 = "/in2";
export const INVchIN2New = "/in2/new";
export const INVchIN2Edit = (id) => {
  return id ? `/in2=${id}` : "/in2=:id";
};
export const INVchIN3 = "/in3";
export const INVchIN3New = "/in3/new";
export const INVchIN3Edit = (id) => {
  return id ? `/in3=${id}` : "/in3=:id";
};
export const INVchIN5 = "/in5";
export const INVchIN5New = "/in5/new";
export const INVchIN5Edit = (id) => {
  return id ? `/in5=${id}` : "/in5=:id";
};
export const INVchIN9 = "/in9"
export const INVchIN9New = "/in9/new"
export const INVchIN9Edit = (id) => { return id ? `/in9=${id}` : "/in9=:id" };

export const INRptCTVT0101 = "/rpt/INRptCTVT0101";
export const INRptCTVT0102 = "/rpt/INRptCTVT0102";

export const INRptCD0101 = "/rpt/INRptCD0101";
export const INRptCD0102 = "/rpt/INRptCD0102";
export const INRptCD0103 = "/rpt/INRptCD0103";
export const INRptCD0104 = "/rpt/INRptCD0104";

export const INRptCD0201 = "/rpt/INRptCD0201";
export const INRptCD0202 = "/rpt/INRptCD0202";
export const INRptCD0203 = "/rpt/INRptCD0203";

export const INRptBKN04 = "/rpt/INRptBKN04";
export const INRptBKX04 = "/rpt/INRptBKX04";

export const INRptTHN0201 = "/rpt/INRptTHN0201";
export const INRptTHN0202 = "/rpt/INRptTHN0202";
export const INRptTHX0201 = "/rpt/INRptTHX0201";
export const INRptTHX0202 = "/rpt/INRptTHX0202";
/**
 **************** FA
 **/
export const FADMNV = "/fadmnv";
export const FADMNHTS = "/fadmnhts";
export const FADMLDTG = "/fadmldtg";
export const FADMBPSD = "/fadmbpsd";
export const FADMTS = "/fadmts";
export const FADGTS = "/fadgts";
export const FAGiamTS = "/fagiamts";
export const FaDungKh = "/fadungkh";
export const FaKhTs = "/fakhts";
export const FATinhKH = "/tinhkhauhao";
export const FAXoaPBKH = "/xoapbkhauhao";
export const FAPBKH = "/pbkhauhao";
export const FADMCC = "/fadmcc";
export const FABHCC = "/fabhcc";
export const FAXoaPhanBoCC = "/xoaphanbocc";
export const FAPhanBoCC = "/phanbocc";

export const FARptBCTS04 = "/rpt/FARptBCTS04";
export const FARptBCTS05 = "/rpt/FARptBCTS05";
export const FARptBCTS06 = "/rpt/FARptBCTS06";
export const FARptBCTS07 = "/rpt/FARptBCTS07";
export const FARptBCTS08 = "/rpt/FARptBCTS08";
export const FARptBCTS09 = "/rpt/FARptBCTS09";
/**
 **************** GL
 **/
export const GlDmTk = "/settings/GlDmTk";
export const GlDmKc = "/GlDmKc";
export const GLCDTK = "/settings/glcdtk";

export const GLVchGL1 = "/gl1";
export const GLVchGL1New = "/gl1/new";
export const GLVchGL1Edit = (id) => {
  return id ? `/gl1=${id}` : "/gl1=:id";
};

export const GLRptBK01 = "/rpt/GLRptBK01";
export const GLRptTH01 = "/rpt/GLRptTH01";
export const GLRptNKC02 = "/rpt/GLRptNKC02";
export const GLRptNKC03C = "/rpt/GLRptNKC03C";
export const GLRptNKC03T = "/rpt/GLRptNKC03T";
export const GLRptNKC04 = "/rpt/GLRptNKC04";
export const GLRptNKC05 = "/rpt/GLRptNKC05";
export const GLRptNKC06 = "/rpt/GLRptNKC06";
export const GLRptNKC07 = "/rpt/GLRptNKC07";
export const GLRptNKC01All01 = "/rpt/GLRptNKC01All01";
export const GLKETCHUYENTUDONG = "/ketchuyentudong";
export const GLPHANBOTUDONG = "/pbchiphitudong";
export const CODMPB = "/codmpb";
export const GLCHUYENSODUTK = "/settings/transferglcdtk";
export const GLRptBCTC011 = "/rpt/GLRptBCTC011";
export const GLRptBCTC012 = "/rpt/GLRptBCTC012";
export const GLRptBCTC021 = "/rpt/GLRptBCTC021";
export const GLRptBCTC022 = "/rpt/GLRptBCTC022";
export const GLRptBCTC023 = "/rpt/GLRptBCTC023";
export const GLMAUBCTC02 = (mau) => {
  return mau ? `/setting/bctc02=${mau}` : "/setting/bctc02=:mau";
};
export const CODmBOM = "/codmbom"
export const CODMCPTT = "/codmcptt";
export const INCDFIFO = "/incdfifo"
export const TAOUT = "/taout";
export const TAIN = "/tain";
export const CODmSP = "/codmsp"
export const CODmNhSP = "/codmnhsp"

/**
 **************** HRDmOther
 **/

export const HrDmOther = `/settings/hrdmkhac`
export const HrDmCctc = `/settings/hrdmcctc`
export const HrHsNs = `/hrhsns`
export const HrHsNsGV = '/hrhsns-gv'
export const HrHsNsNV = '/hrhsns-nv'
export const HrHsNsGVCV = '/hrhsns-gvcv'
export const HrHsNs_detail = (id) => {
  return id ? `/hrhsns/${id}` : "/hrhsns/:id";
};

export const HrLichDay = `/hrlichday`
export const HrLichDayWeek = `/hrlichdayw`
export const HrLichDayMonth = `/hrlichdaym`
export const HrLichDayGv = `/hrlichdaygv`

export const HrDmNgayNghi = '/settings/hrdmngaynghi'

export const HrRptGiayToHetHan = '/hrrpt-giaytohethan'
export const HrRptSinhNhat = '/hrrpt-sinhnhat'

export const HrDmLyDoNghi = '/hrdmlydonghi'

/**
 **************** HRDmOther
 **/
export const WMDmLoaiCV = '/wmdmloaicv'
export const WMDmGiaiDoanCV = '/wmdmgiaidoancv'
export const WMDmUuTienCV = '/wmdmuutiencv'
export const WMCongViec = '/wmcongviec'
export const WMCongViecList = '/wmcongvieclist'

export const HrQueryHsNs = `/hrqueryhsns`;

export const HrChamCongGv = `/hrchamconggv`;
export const HrChamCong = `/hrchamcong`;

/**
 **************** PA
 **/

export const PADmKyLuong = "/padmkyluong";
export const PAVchPA1 = "/pa1"
export const PAVchPA1New = "/pa1/new"
export const PAVchPA1Edit = (id) => { return id ? `/pa1=${id}` : "/pa1=:id" };
export const PADieuChinh = "/padieuchinh";
export const PAKyHieuChamCong = "/pakyhieuchamcong";
export const PADmLoaiBangLuong = "/padmloaibangluong";
export const PADmLoaiBangLuongDetail = (id) => { return id ? `/padmloaibangluong/${id}` : `/padmloaibangluong/:id` };
export const PABangLuong = "/pabangluong";
export const PABangLuongDetail = (id) => { return id ? `/pabangluong/${id}` : `/pabangluong/:id`};
export const PAHangSo = "/pahangso"
export const PARptTHChotCong = "/PARptThChotCong"
export const PARptThChotCongdrilldown = "/PARptThChotCongdrilldown"
/**
 **************** ZEN
 **/
export const ZenLogs = `/zenlogs`

export const Menus = `/menu`
