export const Xem = "";
export const Them = "";
export const Sua = "";
export const Xoa = "";
export const Duyet = "";
export const XemAll = "";

//00 - Tham số hệ thống
export const ARDmKhXem = "06.21.1";
export const ARDmKhThem = "06.21.2";
export const ARDmKhSua = "06.21.3";
export const ARDmKhXoa = "06.21.4";
export const ARDmKhXemAll = "06.21.6";
export const ARDmKhSuaPhuTrach = "06.21.8";
export const ARDmKhXoaPhuTrach = "06.21.9";

export const ARDmPlkhXem = "06.23.1";
export const ARDmPlkhThem = "06.23.2";
export const ARDmPlkhSua = "06.23.3";
export const ARDmPlkhXoa = "06.23.4";

export const ARDmNhkhXem = "06.24.1";
export const ARDmNhkhThem = "06.24.1";
export const ARDmNhkhSua = "06.24.1";
export const ARDmNhkhXoa = "06.24.1";

export const SIDmHdXem = "90.03.1";
export const SIDmHdThem = "90.03.2";
export const SIDmHdSua = "90.03.3";
export const SIDmHdXoa = "90.03.4";
export const SIDmHdXemAll = "90.03.6";
export const SIDmHdDTTT = "90.03.a";

export const EODmLoaiPostXem = "93.13.1";
export const EODmLoaiPostThem = "93.13.2";
export const EODmLoaiPostSua = "93.13.3";
export const EODmLoaiPostXoa = "93.13.4";

export const PostMessageXem = "45.01.1";
export const PostMessageThem = "45.01.2";
export const PostMessageSua = "45.01.3";
export const PostMessageXoa = "45.01.4";
export const PostMessageXemAll = "45.0.6";

export const WMCongViecXem = "45.03.1";
export const WMCongViecThem = "45.03.2";
export const WMCongViecSua = "45.03.3";
export const WMCongViecXoa = "45.03.4";
export const WMCongViecXemAll = "45.03.6";
export const WMCongViecDuyet = "45.03.7";

export const DashBoardTemplate = "92.01.7"

export const SIDmBpXem = "90.01.1";
export const SIDmBpThem = "90.01.2";
export const SIDmBpSua = "90.01.3";
export const SIDmBpXoa = "90.01.4";

export const SIDmPhiXem = "90.02.1";
export const SIDmPhiThem = "90.02.2";
export const SIDmPhiSua = "90.02.3";
export const SIDmPhiXoa = "90.02.4";

export const SIDmNhhdXem = "90.04.1";
export const SIDmNhhdThem = "90.04.2";
export const SIDmNhhdSua = "90.04.3";
export const SIDmNhhdXoa = "90.04.4";

export const SIDmNtXem = "90.05.1";
export const SIDmNtThem = "90.05.2";
export const SIDmNtSua = "90.05.3";
export const SIDmNtXoa = "90.05.4";

export const SIDmTgNtDuyet = "90.05.5";

export const SIDmNghXem = "04.22.1";
export const SIDmNghThem = "04.22.2";
export const SIDmNghSua = "04.22.3";
export const SIDmNghXoa = "04.22.4";

export const SIDmQuocGiaXem = "90.31.1";
export const SIDmQuocGiaThem = "90.31.2";
export const SIDmQuocGiaSua = "90.31.3";
export const SIDmQuocGiaXoa = "90.31.4";

export const SIDmTinhXem = "90.32.1";
export const SIDmTinhThem = "90.32.2";
export const SIDmTinhSua = "90.32.3";
export const SIDmTinhXoa = "90.32.4";

export const SIDmHuyenXem = "90.33.1";
export const SIDmHuyenThem = "90.33.2";
export const SIDmHuyenSua = "90.33.3";
export const SIDmHuyenXoa = "90.33.4";

export const SIDmLoaiTaiLieuXem = "90.35.1";
export const SIDmLoaiTaiLieuThem = "90.35.2";
export const SIDmLoaiTaiLieuSua = "90.35.3";
export const SIDmLoaiTaiLieuXoa = "90.35.4";

export const DEALXem = "40.05.1";
export const DEALThem = "40.05.2";
export const DEALSua = "40.05.3";
export const DEALXoa = "40.05.4";
export const DEALXemAll = "40.05.6";

export const CRMLinhVucXem = "92.31.1";
export const CRMLinhVucThem = "92.31.2";
export const CRMLinhVucSua = "92.31.3";
export const CRMLinhVucXoa = "92.31.4";

export const CRMNguonKhXem = "92.33.1";
export const CRMNguonKhThem = "92.33.2";
export const CRMNguonKhSua = "92.33.3";
export const CRMNguonKhXoa = "92.33.4";

export const CRMLoaiHoatDongXem = "92.35.1";
export const CRMLoaiHoatDongThem = "92.35.2";
export const CRMLoaiHoatDongSua = "92.35.3";
export const CRMLoaiHoatDongXoa = "92.35.4";

export const CRMDealUuTienXem = "92.37.1";
export const CRMDealUuTienThem = "92.37.2";
export const CRMDealUuTienSua = "92.37.3";
export const CRMDealUuTienXoa = "92.37.4";

export const CRMDealGiaiDoanXem = "92.39.1";
export const CRMDealGiaiDoanThem = "92.39.2";
export const CRMDealGiaiDoanSua = "92.39.3";
export const CRMDealGiaiDoanXoa = "92.39.4";

// TAB

// Đối tượng khách hàng
export const ARCRMTabGhiChuXem = "06.21.a";
export const ARCRMTabHoatDongXem = "06.21.b";
export const ARCRMTabCoHoiXem = "06.21.c";
export const ARCRMTabBaoGiaXem = "06.21.d";
export const ARCRMTabHopDongXem = "06.21.e";
export const ARCRMTabTraoDoiXem = "06.21.f";
export const ARCRMTabCongViecXem = "06.21.g";

// Đối tượng hợp đồng
export const SICRMTabGhiChuXem = "90.03.b";
export const SICRMTabHoatDongXem = "90.03.c";
export const SICRMTabFileAttachXem = "90.03.d";

// Đối tượng cơ hội
export const CRMDealTabGhiChuXem = "40.05.a";
export const CRMDealTabHoatDongXem = "40.05.b";
export const CRMDealTabTraoDoiXem = "40.05.c";
export const CRMDealTabFileAttachXem = "40.05.d";
export const SIDMHDTabNhanSu = "40.05.e";

export const CA1Xem = "04.01.1";
export const CA1Them = "04.01.2";
export const CA1Sua = "04.01.3";
export const CA1Xoa = "04.01.4";

export const CA2Xem = "04.02.1";
export const CA2Them = "04.02.2";
export const CA2Sua = "04.02.3";
export const CA2Xoa = "04.02.4";

export const SO0Xem = "40.07.1";
export const SO0Them = "40.07.2";
export const SO0Sua = "40.07.3";
export const SO0Xoa = "40.07.4";
export const SO0XemAll = "40.07.6";

export const SO3Xem = "06.01.1";
export const SO3Them = "06.01.2";
export const SO3Sua = "06.01.3";
export const SO3Xoa = "06.01.4";

export const PA1Xem = "43.01.1";
export const PA1Them = "43.01.2";
export const PA1Sua = "43.01.3";
export const PA1Xoa = "43.01.4";
export const PA1ChoChotCong = "43.01.5";

export const PMDuAnXem = "50.01.1";
export const PMDuAnThem = "50.01.2";
export const PMDuAnSua = "50.01.3";
export const PMDuAnXoa = "50.01.4";
export const PMDuAnXemAll = "50.01.6";

export const HrHsNsXem = "41.01.1";
export const HrHsNsThem = "41.01.2";
export const HrHsNsSua = "41.01.3";
export const HrHsNsXoa = "41.01.4";
export const HrHsNsGiayTo = "41.01.a";
export const HrHsNsHdld = "41.01.b";
export const HrHsNsPhuCap = "41.01.c";

export const HRQueryHsNs = "41.03";
export const HRHsNsGV = "41.05";
export const HRHsNsNV = "41.07";
export const HRHsNsGVCV = "41.09";

export const HrLichDayXem = "42.01.1";
export const HrLichDayThem = "42.01.2";
export const HrLichDaySua = "42.01.3";
export const HrLichDayXoa = "42.01.4";
export const HrLichDayXemAll = "42.01.6";
export const HrLichDayChamCong = "42.01.a";

export const HrChamCongGVXem= "43.02.1"; //Thực hiện tổng hợp công giáo viên

export const HrChamCongXem = "43.03.1"; //Thực hiện chấm công nhân viên

export const HRDmKhacXem = "93.01.1";
export const HRDmKhacThem = "93.01.2";
export const HRDmKhacSua = "93.01.3";
export const HRDmKhacXoa = "93.01.4";

export const HRDmCctcXem = "93.03.1";
export const HRDmCctcThem = "93.03.2";
export const HRDmCctcSua = "93.03.3";
export const HRDmCctcXoa = "93.03.4";

export const PADmKyLuongXem = "93.05.1";
export const PADmKyLuongThem = "93.05.2";
export const PADmKyLuongSua = "93.05.3";
export const PADmKyLuongXoa = "93.05.4";

export const HrDmNgayNghiXem = "93.07.1";
export const HrDmNgayNghiThem = "93.07.2";
export const HrDmNgayNghiSua = "93.07.3";
export const HrDmNgayNghiXoa = "93.07.4";

export const HrDmLyDoNghiXem = "93.12.1";
export const HrDmLyDoNghiThem = "93.12.2";
export const HrDmLyDoNghiSua = "93.12.3";
export const HrDmLyDoNghiXoa = "93.12.4";

export const PAKyHieuChamCongXem = "93.09.1";
export const PAKyHieuChamCongThem = "93.09.2";
export const PAKyHieuChamCongSua = "93.09.3";
export const PAKyHieuChamCongXoa = "93.09.4";

export const PADmLoaiBangLuongXem = "93.11.1";
export const PADmLoaiBangLuongThem = "93.11.2";
export const PADmLoaiBangLuongSua = "93.11.3";
export const PADmLoaiBangLuongXoa = "93.11.4";

export const PABangLuongTaoVaTinhLuong = "43.04.1"; // Tạo và tính lương nhân viên
export const PABangLuongGVTaoVaTinhLuong = "43.04.1"; // Tạo và tính lương giáo viên

export const PADieuChinhXem = "43.05.1";
export const PADieuChinhThem = "43.05.2";
export const PADieuChinhSua = "43.05.3";
export const PADieuChinhXoa = "43.05.4";

export const PAHangSoXem = "43.06.1";
export const PAHangSoThem = "43.06.2";
export const PAHangSoSua = "43.06.3";
export const PAHangSoXoa = "43.06.4";

export const PMDmGiaiDoanDaXem = "94.01.1";
export const PMDmGiaiDoanDaThem = "94.01.2";
export const PMDmGiaiDoanDaSua = "94.01.3";
export const PMDmGiaiDoanDaXoa = "94.01.4";

export const WMDmGiaiDoanCvXem = "94.03.1";
export const WMDmGiaiDoanCvThem = "94.03.2";
export const WMDmGiaiDoanCvSua = "94.03.3";
export const WMDmGiaiDoanCvXoa = "94.03.4";

export const WMDmLoaiCvXem = "94.05.1";
export const WMDmLoaiCvThem = "94.05.2";
export const WMDmLoaiCvSua = "94.05.3";
export const WMDmLoaiCvXoa = "94.05.4";

export const PMDmNhomDaXem = "94.07.1";
export const PMDmNhomDaThem = "94.07.2";
export const PMDmNhomDaSua = "94.07.3";
export const PMDmNhomDaXoa = "94.07.4";

export const WMDmUuTienCvXem = "94.09.1";
export const WMDmUuTienCvThem = "94.09.2";
export const WMDmUuTienCvSua = "94.09.3";
export const WMDmUuTienCvXoa = "94.09.4";

export const INDmDvtXem = "10.25.1";
export const INDmDvtThem = "10.25.2";
export const INDmDvtSua = "10.25.3";
export const INDmDvtXoa = "10.25.4";

export const INDmVtXem = "10.21.1";
export const INDmVtThem = "10.21.2";
export const INDmVtSua = "10.21.3";
export const INDmVtXoa = "10.21.4";

export const INDmPlVtXem = "10.23.1";
export const INDmPlVtThem = "10.23.2";
export const INDmPlVtSua = "10.23.3";
export const INDmPlVtXoa = "10.23.4";

export const INDmNhVtXem = "10.24.1";
export const INDmNhVtThem = "10.24.2";
export const INDmNhVtSua = "10.24.3";
export const INDmNhVtXoa = "10.24.4";

export const SODmTsXem = "90.07.1";
export const SODmTsThem = "90.07.2";
export const SODmTsSua = "90.07.3";
export const SODmTsXoa = "90.07.4";

export const SIDmNamTc = "92.01.1";
export const SIDmCt = "92.01.2";
export const SIDmLoai = "92.01.5";
export const Setup_Module = "92.01.3";
export const Company = "92.01.4";
export const Mail = "92.01.6";

export const SIDmFormXem = "92.15.1";
export const SIDmFormThem = "92.15.2";
export const SIDmFormSua = "92.15.3";
export const SIDmFormXoa = "92.15.4";

export const GlDmTkXem = "02.21.1";
export const GlDmTkThem = "02.21.2";
export const GlDmTkSua = "02.21.3";
export const GlDmTkXoa = "02.21.4";

export const UserXem = "92.12.1";
export const UserThem = "92.12.2";
export const UserSua = "92.12.3";
export const UserXoa = "92.12.4";

export const RoleXem = "92.11.1";
export const RoleThem = "92.11.2";
export const RoleSua = "92.11.3";
export const RoleXoa = "92.11.4";

export const Preferences = "00.05.1";

