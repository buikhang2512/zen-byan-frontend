import axios from './axios';

const ExtName = "configuration"

export const ApiConfig = {
   getMaGd(callback) {
      axios.get(`${ExtName}/magd`)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   getMaGdByCT(ma_ct, callback) {
      axios.get(`${ExtName}/magd/` + ma_ct)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   getTrangThaiByCT(ma_ct, callback) {
      axios.get(`${ExtName}/trangthai/` + ma_ct)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   getLoaiVT(callback) {
      axios.get(`${ExtName}/loaivt`)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   getLoaiGiaTon(callback) {
      axios.get(`${ExtName}/loaigiaton`)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   getTenNguonVon(callback) {
      axios.get(`${ExtName}/tennguonvon`)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },
   getFaSetup(cols, callback) {
      axios.get(`${ExtName}/fasetup/${cols}`)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   getSttRec(ma_ct, callback) {
      axios.get(`${ExtName}/sttrec/${ma_ct}`)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   getModuleSetupMeta(module, callback) {
      axios.get(`${ExtName}/setupmeta/${module}`)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   getModuleSetupByName(module, name, callback) {
      axios.get(`${ExtName}/setup/${module}/${name}`)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   getModuleSetup(module, callback) {
      axios.get(`${ExtName}/setup/${module}`)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   /**
    * data :{
    *    var_name: tên trường
    *    var_value: giá trị
    * }
    */
   updModuleSetup(module, data, callback) {
      axios.put(`${ExtName}/setup/${module}`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },
   changeCode(data, callback) {
      axios.put(`${ExtName}/change_code`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   getModuleForSetup(callback) {
      axios.get(`${ExtName}/setup/list-modules`)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },
}
