import axios, { axiosMultiFormData } from './axios';

const ExtName = "file_attachment"

export const ApiFileAttachment = {
   get(codeName, docKey, callback) {
      axios.get(`${ExtName}`, {
         params: {
            codeName: codeName,
            docKey: docKey
         }
      })
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   download(codeName, gid, callback) {
      axios.get(`${ExtName}/${gid}/download`, {
         params: {
            codeName: codeName
         },
         responseType: 'blob',
      })
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   upload(codeName, fileAttachment, callback) {
      axiosMultiFormData.post(`${ExtName}/${codeName}/upload`, fileAttachment)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   uploadList(codeName, fileAttachment, callback) {
      axiosMultiFormData.post(`${ExtName}/${codeName}/uploadList`, fileAttachment)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },



   getAttachmentInfo(codeName, callback) {
      axios.get(`${ExtName}/${codeName}/attachmentinfo`)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   delete(codeName, id, gid, callback) {
      axios.delete(`${ExtName}/${codeName}/${id}&${gid}`)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   uploadEditor(fileupload, callback) {
      console.log(fileupload,'adasdsd')
      axiosMultiFormData.post(`upload/editor`, fileupload)
         .then(res => {
            console.log(res)
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   }
}