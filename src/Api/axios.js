import axios from 'axios';
import { ZenApp } from '../utils/global';
import { default as auth } from '../utils/auth';

const instance = axios.create({
   baseURL: ZenApp.baseUrlAPI,
   transformRequest: [function (data, headers) {
      configHeaders(headers, "application/json")
      return JSON.stringify(data)
   }],
});

const axiosBase = axios.create({
   baseURL: ZenApp.baseUrl,
   transformRequest: [function (data, headers) {
      configHeaders(headers, "application/json")
      return JSON.stringify(data)
   }],
});

const axiosMultiFormData = axios.create({
   baseURL: ZenApp.baseUrlAPI,
   transformRequest: [function (data, headers) {
      configHeaders(headers, "multipart/form-data")
      return data
   }]
});

instance.interceptors.response.use(
   response => response,
   f_TokenRefreshInterceptor
)
axiosBase.interceptors.response.use(
   response => response,
   f_TokenRefreshInterceptor
)
axiosMultiFormData.interceptors.response.use(
   response => response,
   f_TokenRefreshInterceptor
)

// ================== resfresh token
let isRefreshing = false,
   subscribers = [];
const routeRefreshToken = "account/refreshtoken"

let failedQueue = [];

const processQueue = (error, token = null) => {
   failedQueue.forEach(prom => {
      if (error) {
         prom.reject(error);
      } else {
         prom.resolve(token);
      }
   })

   failedQueue = [];
}

async function f_TokenRefreshInterceptor(err) {

   const originalRequest = err.config,
      status = err.response.status,
      oldRefreshToken = auth.getToken();

   if (err.response.status === 401 && !originalRequest._retry && oldRefreshToken) {

      if (isRefreshing) {
         return new Promise(function (resolve, reject) {
            failedQueue.push({ resolve, reject })
         }).then(token => {
            originalRequest.headers['Authorization'] = 'Bearer ' + token;
            if (originalRequest.data && (typeof originalRequest.data) === "string"
               && (originalRequest.data instanceof FormData) === false) {
               originalRequest.data = JSON.parse(originalRequest.data)
            }
            return axios(originalRequest);
         }).catch(err => {
            return Promise.reject(err);
         })
      }

      originalRequest._retry = true;
      isRefreshing = true;

      return new Promise(function (resolve, reject) {
         instance.post(routeRefreshToken, { token: oldRefreshToken, tenant: auth.getTenantId() })
            .then(res => {
               if (res.status === 200) {
                  const newToken = res.data.data.access_token
                  auth.setToken(newToken, true);
                  auth.saveCurrentSession();
                  axios.defaults.headers.common['Authorization'] = 'Bearer ' + newToken;
                  if (originalRequest.data && (typeof originalRequest.data) === "string"
                     && (originalRequest.data instanceof FormData) === false) {
                     originalRequest.data = JSON.parse(originalRequest.data)
                  }
                  processQueue(null, newToken);
                  resolve(axios(originalRequest));
               }
            })
            .catch((err) => {
               processQueue(err, null);
               reject(err);
               auth.clearToken()
               auth.clearTenantInfo()
               location.reload();
            })
            .finally(() => { isRefreshing = false })
      })
   }

   return Promise.reject(err);
}


// ================== resfresh token old
// function f_refreshToken(err) {
//    const originalRequest = err.config,
//       status = err.response.status,
//       oldRefreshToken = auth.getToken();

//    if (status === 401 && !originalRequest._retry && oldRefreshToken) {
//       if (!isRefreshing) {
//          isRefreshing = true
//          originalRequest._retry = true

//          instance.post(routeRefreshToken, { token: oldRefreshToken, tenant: auth.getTenantId() })
//             .then(res => {
//                if (res.status === 200) {
//                   const newToken = res.data.data.access_token
//                   // set token
//                   auth.setToken(newToken, true);
//                   auth.saveCurrentSession();
//                   originalRequest.headers["Authorization"] = 'Bearer ' + newToken;

//                   // thực hiện danh sách request đã lưu sau khi lấy token mới
//                   onAccessTokenFetched(newToken)
//                } else {

//                   auth.clearToken()
//                   auth.clearTenantInfo()
//                   location.reload();
//                }
//             }).then(res => {
//                isRefreshing = false
//             })
//       }

//       // lưu các request tiếp theo vào subscribers
//       return new Promise((resolve) => {
//          addSubscriber(access_token => {
//             originalRequest.headers["Authorization"] = 'Bearer ' + access_token
//             if (originalRequest.data && (typeof originalRequest.data) === "string"
//                && (originalRequest.data instanceof FormData) === false) {
//                originalRequest.data = JSON.parse(originalRequest.data)
//             }
//             resolve(axios(originalRequest))
//          })
//       })
//    }
//    return Promise.reject(err);
// }

function onAccessTokenFetched(access_token) {
   subscribers = subscribers.filter(callback => callback(access_token))
}

function addSubscriber(callback) {
   subscribers.push(callback)
}

function configHeaders(headers, contentType = "application/json") {
   headers["Authorization"] = "bearer " + auth.getToken();
   headers["TenantId"] = auth.getTenantId();
   headers["Access-Control-Allow-Credentials"] = true;
   headers["Access-Control-Allow-Origin"] = "*";
   headers["Content-Type"] = contentType;
}

export { axiosBase, axiosMultiFormData }
export default instance;
