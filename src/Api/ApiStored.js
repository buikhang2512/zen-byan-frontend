import { axios } from "./index";

function getColsLookup(params) {
  let cols = params.cols ? params.cols : "";
  if (!cols.includes(params.value)) {
    cols += `,${params.value}`;
  }
  if (!cols.includes(params.text)) {
    cols += `,${params.text}`;
  }
  return cols;
}

const lookupDictionary = (params) => {
  return {
    sqlCommand: "asLookup",
    parameters: {
      pCodeName: params.code,
      pFieldList: getColsLookup(params),
      pTableName: params.tableName,
      pWhere: params.where,
      pTop: params.top || 0,
      pLastModDate: null,
    },
  };
};

export const ApiStored = {
  get(stored, callback) {
    axios
      .post(`zenapi`, stored)
      .then((res) => {
        callback(res);
      })
      .catch((err) => {
        callback(err);
      });
  },

  lookupDictionary(params, callback) {
    axios
      .post(`zenapi`, lookupDictionary(params))
      .then((res) => {
        callback(res);
      })
      .catch((err) => {
        callback(err);
      });
  },
};
