import axios from "./axios";

export const ApiMenu = {
  get(callback, query) {
    axios
      .get(`menus`, {
        params: {
          qf: query,
        },
      })
      .then((res) => {
        callback(res);
      })
      .catch((err) => {
        callback(err);
      });
  },

  getNav(callback) {
    axios
      .get(`menus/nav`)
      .then((res) => {
        callback(res);
      })
      .catch((err) => {
        callback(err);
      });
  },

  getReport(callback, query = undefined, pagination = {}) {
    axios
      .get(`menus/reports`, {
        params: {
          qf: query,
          page: pagination.page,
          pageSize: pagination.pageSize,
        },
      })
      .then((res) => {
        callback(res);
      })
      .catch((err) => {
        callback(err);
      });
  },
};
