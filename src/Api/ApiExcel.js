import axios from 'axios';
import auth from '../utils/auth';

const instance = axios.create({
   baseURL: process.env.API_URL,
   transformRequest: [function (data, headers) {
      headers['Authorization'] = 'bearer ' + auth.getToken();
      headers["TenantId"] = auth.getTenantId();
      headers['Access-Control-Allow-Credentials'] = true
      headers['Access-Control-Allow-Origin'] = '*'
      headers['Content-Type'] = 'multipart/form-data'
      return data
   }],
   headers: {
      'Access-Control-Allow-Origin': '*',
      'Content-Type': 'multipart/form-data'
   }
});

export const ApiExcel = {
   import(form, callback) {
      instance.post(`excels/import`, form)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   importTrans(form, callback) {
      instance.post(`excels/import/trans`, form)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   export(code, callback) {
      instance.post(`excels/export`, code, {
         responseType: 'blob'
      })
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },
}
