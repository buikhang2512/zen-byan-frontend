import { axios } from ".";

const ExtName = "resource-version"

export const ApiResourceVersion = {
    get(callback) {
        axios.get(`${ExtName}`)
            .then(res => {
                callback(res)
            })
            .catch(err => {
                callback(err)
            });
    },
}