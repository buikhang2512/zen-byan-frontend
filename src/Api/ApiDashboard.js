import axios from './axios';

const ExtName = "sys-dashboard"

const ExtNameChart = "dashboard"

export const ApiDashboard = {
   getByUser(callback) {
      axios.get(`${ExtName}?app=web`)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },
   insert(data, callback) {
      axios.post(`${ExtName}`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },
   getDasletByUser(callback) {
      axios.get(`${ExtName}/available_lets`)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },
   getDashlet(id, params, callback) {
      axios.post(`${ExtNameChart}/${id}`, { ...params })
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },
   bienDongSoSu(params, callback) {
      axios.post(`${ExtNameChart}/biendongsodu`, { ...params })
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },
   phanTichChiPhi(params, callback) {
      axios.post(`${ExtNameChart}/chi_phi_theo_km`, { ...params })
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },
   dongTien(params, callback) {
      axios.post(`${ExtNameChart}/dongtien`, { ...params })
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },
   lichSuDoanhThu(params, callback) {
      axios.post(`${ExtNameChart}/lic_su_doanh_thu`, { ...params })
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },
   doanhThuChiPhi(params, callback) {
      axios.post(`${ExtNameChart}/doanh_thu`, { ...params })
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },
   tongHopYeuCau(params, callback) {
      axios.post(`${ExtNameChart}/tong_hop_yeu_cau`, { ...params })
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },
   noPhaiTra(params, callback) {
      axios.post(`${ExtNameChart}/topcongnophaitra`, { ...params })
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },
   noPhaiThu(params, callback) {
      axios.post(`${ExtNameChart}/cong_no_phai_thu`, { ...params })
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },
   topNVKD(params, callback) {
      axios.post(`${ExtNameChart}/top_nvkd`, { ...params })
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   reset(params, callback) {
      axios.post(`${ExtName}/reset?app=${params.app}`)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   }
}
