import axios from './axios';

const ExtName = "sys-dashletrole"

export const ApiRoleDashlet = {

   getListRole(roleid, callback) {
      axios.get(`${ExtName}/` +  roleid,
      )
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   insert(data, callback) {
      axios.post(`${ExtName}`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

}