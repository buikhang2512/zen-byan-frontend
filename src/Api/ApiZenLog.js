import axios from './axios';

const ExtName = "ZenLog"

export const ApiZenLog = {
    logError(data, callback) {
        axios.post(`${ExtName}`, data)
            .then(res => {
                callback(res)
            })
            .catch(err => {
                callback(err)
            });
    },
}