import axios, { axiosMultiFormData } from './axios';

const ExtName = "posts"

export const ApiPostMessage = {
   get(callback, filter = {}, pagination = {}) {
      axios.get(`${ExtName}`, {
         params: {
            qf: filter.qf,
            keyword: filter.keyword,
            sort: filter.sort,
            page: pagination.page,
            pageSize: pagination.pageSize
         },
      })
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   getByCode(code, callback) {
      axios.get(`${ExtName}/` + code)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   insert(data, callback) {
      axiosMultiFormData.post(`${ExtName}`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   update(data, callback) {
      axiosMultiFormData.put(`${ExtName}`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   updatePatch(code, data, callback) {
      axios.patch(`${ExtName}/${code}`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   delete(code, callback) {
      axios.delete(`${ExtName}/` + code)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   // ***************************** Participant
   getParticipants(idPost, callback) {
      axios.get(`${ExtName}/${idPost}/participants`)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   insertParticipants(idPost, data, callback) {
      axios.post(`${ExtName}/${idPost}/participants`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   deleteParticipants(idPost, idParticipant, callback) {
      axios.delete(`${ExtName}/${idPost}/participants/${idParticipant}`)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   // ***************************** Activity
   getActivities(idPost, callback) {
      axios.get(`${ExtName}/${idPost}/activities`)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   insertActivities(idPost, data, callback) {
      axios.post(`${ExtName}/${idPost}/activities`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   updateActivities(idPost, data, callback) {
      axios.put(`${ExtName}/${idPost}/activities`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   deleteActivities(idPost, idActivity, callback) {
      axios.delete(`${ExtName}/${idPost}/activities/${idActivity}`)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   seenPosts(code, callback) {
      axios.post(`${ExtName}/${code}/seen`)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   votePost(code, data, callback) {
      axios.post(`${ExtName}/${code}/vote?poll_id=${data.id}`)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   publishPost(code, callback) {
      axios.post(`${ExtName}/${code}/publish`)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   }

}