import axios, { axiosMultiFormData } from './axios';

const ExtName = "excels"

export const ApiImportExcel = {
    download(codeName, callback) {
        axios.get(`${ExtName}/template`, {
            params: {
                codeName: codeName
            },
            responseType: 'blob',
        })
            .then(res => {
                callback(res)
            })
            .catch(err => {
                callback(err)
            });
    },

    import(data, callback) {
        // data:{code: string, FileExcel: IFormFile}
        axiosMultiFormData.post(`${ExtName}/import/dictionary`, data)
            .then(res => {
                callback(res)
            })
            .catch(err => {
                callback(err)
            });
    },
}