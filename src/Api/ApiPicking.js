import axios from './axios';

// headers: {
//    'Content-Type': 'text/html',
//    'Authorization': 'bearer ' + auth.getToken()
// }

export const ApiPicking = {
   get(callback, query = undefined) {
      axios.get(`pickings`, {
         params: {
            qf: query
         },
      })
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   getReport(q, callback) {
      axios.get(`reportviewer`, {
         params: {
            nameReport: q.nameReport,
            parameters: q.params,
            fromDate: q.fromDate,
            todate: q.toDate
         },
      })
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   getReportTest(q, callback) {
      axios.get(`reportviewer/test`, {
         params: {
            nameReport: q.nameReport,
            parameters: q.params
         },
      })
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },
}