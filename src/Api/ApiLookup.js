import { axios } from ".";

const ExtName = "lookup"

export const ApiLookup = {
   get(callback, filter = {}, pagination = {}) {
      axios.get(`${ExtName}/${filter.code_name}`, {
         params: {
            field_list: filter.field_list,   // thêm trường trả về, default field_list trong Lookupinfo
            where: filter.where,             // điều kiện lookup
            keyword: filter.keyword,         // search theo keyword
            sort: filter.sort,
            top: filter.top,
            language: filter.language,

            page: pagination.page,
            pageSize: pagination.pageSize
         },
      })
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   getMulti(strCode, callback) {
      axios.get(`${ExtName}`, {
         params: {
            strCode: strCode
         },
      })
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   getLookupInfo(code_name, language, callback) {
      axios.get(`${ExtName}/${code_name}/info`, {
         params: {
            language: language
         },
      })
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   }
}