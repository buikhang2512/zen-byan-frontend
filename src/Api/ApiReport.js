import axios from "./axios";

const urlPrinted = "zenreport";
const urlInfo = "report_info";

export const ApiReport = {
  // ********************* report_info
  getReportInfoByCode(code, callback) {
    axios
      .get(`${urlInfo}/code/` + code)
      .then((res) => {
        callback(res);
      })
      .catch((err) => {
        callback(err);
      });
  },

  getReportInfoByGroup(group, callback) {
    axios
      .get(`${urlInfo}/group/` + group)
      .then((res) => {
        callback(res);
      })
      .catch((err) => {
        callback(err);
      });
  },

  // Sử dụng để xuất file báo cáo FastReport trong ReportForm.js
  getReportViewer(q, callback) {
    axios
      .post(`${urlPrinted}/viewer/` + q.code64, {
        //nameReport: q.nameReport,
        procParams: { ...q.params },
        sort: q.sort
      }, {
        responseType: "blob"
      })
      .then((res) => {
        callback(res);
      })
      .catch((err) => {
        callback(err);
      });
  },

  /**
   * Sử dụng để get dữ liệu của Báo cáo trong ReportForm.js
   * code64: base64 code của report
   * params: json
   * pagination: {page, pageSize}
   */
  getDataReport(infoReport, callback) {
    const { code64, params, pagination } = infoReport;
    axios
      .post(`${urlPrinted}/` + code64, { ...params, ...pagination })
      .then((res) => {
        callback(res);
      })
      .catch((err) => {
        callback(err);
      });
  },

  // Sử dụng xuất báo cáo trong ZenVoucherReport.js
  getReportCT(q, callback) {
    q.params.prefix = "";
    var base64 = btoa(q.codeReport);
    axios
      .post(`${urlPrinted}/viewer/${base64}`, {
        procParams: { ...q.params },
        rpt_type: "CT",
      }, {
        responseType: "blob"
      })
      .then((res) => {
        callback(res);
      })
      .catch((err) => {
        callback(err);
      });
  },
  getDrillDown(code,callback) {
    axios
    .get(`${urlInfo}/${code}/drilldowns`)
    .then((res) => {
      callback(res);
    })
    .catch((err) => {
      callback(err);
    });
  },

  // Sử dụng để xuất Excel đối với các báo cáo trong ReportForm.js
  exportExcel(q, callback) {
    axios
      .post(`${urlPrinted}/export/` + q.code64, {
        procParams: { ...q.params },
        columns: q.columns
      },
        {
          responseType: 'blob',
        }
      )
      .then((res) => {
        callback(res);
      })
      .catch((err) => {
        callback(err);
      });
  },

  // Sử dụng để gưi mail file báo cáo FastReport cho khách hàng
  getReportSendMail(q, callback) {
    q.params.prefix = "";
    var base64 = btoa(q.codeReport);
    axios
      .post(`${urlPrinted}/send_mail/${base64}`, {
        procParams: { ...q.params },
        rpt_type: "CT",
      }, {
        responseType: "blob"
      })
      .then((res) => {
        callback(res);
      })
      .catch((err) => {
        callback(err);
      });
  },
};
