import axios from './axios';

export const ApiEmail = {
   get(callback, query = undefined, pagination = {}) {
      axios.get(`emails`, {
         params: {
            qf: query,
            page: pagination.page,
            pageSize: pagination.pageSize
         },
      })
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   getByID(id, callback) {
      axios.get(`emails/` + id,
      )
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   sendMailTest(provider, mail, callback) {
      axios.get(`emails/test`, {
         params: {
            provider: provider,
            fromMail: mail.fromMail,
            subject: mail.subject,
            body: mail.body
         }
      }
      )
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   insert(data, callback) {
      axios.post(`emails`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   update(data, callback) {
      axios.put(`emails`, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },

   delete(id, callback) {
      axios.delete(`emails/` + id)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   }
}