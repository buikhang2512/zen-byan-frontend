import axios from './axios';
const ExtName = "setting"

export const ApiSettingGrid = {
   get(callback) {
      axios.get(`${ExtName}/grid`)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },
   getByCode(gridKey, callback) {
      axios.get(`${ExtName}/grid/` + gridKey)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },
   update(gridKey, data, callback) {
      axios.put(`${ExtName}/grid/` + gridKey, data)
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },
}