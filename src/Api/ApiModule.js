import { axios } from 'Api'

export const ApiModule = {
   get(callback, query = undefined, pagination = {}) {
      axios.get(`modules`, {
         params: {
            qf: query,
            page: pagination.page,
            pageSize: pagination.pageSize
         },
      })
         .then(res => {
            callback(res)
         })
         .catch(err => {
            callback(err)
         });
   },
}