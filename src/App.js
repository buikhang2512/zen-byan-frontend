import React from 'react'
import { Route, Switch, HashRouter as Router } from 'react-router-dom'
import 'styling/semantic.less'
import { Home, Login, Register, RegisterTks, ForgotPassword, ForgotPasswordTks, ActivatedForm } from './modules'
import * as routes from './constants/routes';

const App = () => (
  <Router>
    <Switch>
      <Route path={routes.Login} component={Login} />
      <Route path={routes.Register} component={Register} />
      <Route path={routes.RegisterTks} component={RegisterTks} />
      <Route path={routes.ForgotPassword} component={ForgotPassword} />
      <Route path={routes.ForgotPasswordTks} component={ForgotPasswordTks} />
      <Route path={routes.Activate()} component={ActivatedForm} />

      <Route path="/" component={Home} />
    </Switch>
  </Router>
)

export default App
