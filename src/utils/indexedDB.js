const DB_NAME = "ZenBook"

const gldmtkData = [
   { ma_cty: "001", tk: "111", ten_tk: "Tiền mặt", ma_nt: 'VND' },
   { ma_cty: "001", tk: "112", ten_tk: "Tiền gửi ngân hàng", ma_nt: 'VND' },
   { ma_cty: "001", tk: "131", ten_tk: "Phải thu của khách hàng", ma_nt: 'VND' },
   { ma_cty: "001", tk: "141", ten_tk: "Tiền mặt", ma_nt: 'VND' },
];
var req;

export const myIndexedDb = {
   checkIndexedDB() {
      // kiểm tra browser có hỗ trợ ko
      return window.indexedDB ? true : false
   },

   openDb(dbName = DB_NAME, dataObject = { gldmtk: { data: gldmtkData, keyPath: ['ma_cty', 'tk'] } }) {
      req = indexedDB.open(dbName);
      f_openResult(req)

      req.onupgradeneeded = function (e) {
         let db = e.target.result
         // Create an objectStore for database
         var stores = Object.keys(dataObject).map(function (key) {
            return { storeName: key, data: dataObject[key] };
         });

         stores.forEach(el => {
            var store = db.createObjectStore(el.storeName, { keyPath: el.data.keyPath });
            store.createIndex("ma_cty,tk", ["ma_cty", "tk"]);
            store.createIndex("ma_cty", ["ma_cty"]);

            el.data.data.forEach(function (item) {
               store.add(item);
            })
         });
      };
   },

   /*
     muốn thêm stored vào DB,
     openDB với version lớn hơn version hiện tại
  */
   addStore(version, nameStore = 'gldmtk', data = gldmtkData, keyPath = 'tk') {
      req.result.close()
      req = indexedDB.open(DB_NAME, version);
      f_openResult(req)

      req.onupgradeneeded = function (e) {
         var oldVersion = e.oldVersion;
         let db = e.target.result
         // get list store
         var stores = db.objectStoreNames;

         // check store exists
         if (!stores.contains(nameStore)) {
            // Create an objectStore for this database
            var store = db.createObjectStore(nameStore, { keyPath: keyPath });
            data.forEach(function (tk) {
               store.add(tk);
            });
         }
      };
   },

   getAll(tableName, callback) {
      var res = f_read(tableName).index('ma_cty').getAll(['001'])
      f_result(res, callback)
   },
   getByKey(tableName, key, callback) {
      var res = f_read(tableName).get(['001', key])
      f_result(res, callback)
   },

   insert(tableName, data = []) {
      data.forEach(el => {
         f_read(tableName).add(el)
      })
   },
   /**
    * 
    * @param {*} tableName 
    * @param {*} keyName : tên trường khóa 9
    * @param {*} item : item update
    */
   update(tableName, keyName, item) {
      // nếu ko tồn tại Item, method Put sẽ tự insert
      // => nên kiểm trả item trước khi put
      getByKey(tableName, item[keyName], data => {
         if (data) {
            f_read(tableName).put(item)
         }
      })
   },
   delete(tableName, key) {
      f_read(tableName).delete(['001', key]);
   }
}

function f_read(tableName) {
   return req.result.transaction(tableName, 'readwrite').objectStore(tableName)
}

function f_openResult(req) {
   req.onsuccess = function (evt) {
      console.log("openDb DONE");
   };
   req.onerror = function (evt) {
      console.error("openDb:", evt.target.errorCode);
   };
}

function f_result(res, callback) {
   res.onsuccess = (e) => {
      var cursor = e.target.result
      if (Array.isArray(cursor)) {
         callback(cursor)
      } else {
         callback(cursor)
      }
   }
   res.onerror = (e) => {
      callback(e)
   }
}
