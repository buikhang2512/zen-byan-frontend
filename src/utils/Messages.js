import { ComponentEN, ComponentVI } from '../components/language/index';
import { RoleEN, RoleVI } from '../modules/Role/language/index';
import { PermissionEN, PermissionVI } from '../modules/Permission/language/index';
import { UserEN, UserVI } from '../modules/User/language/index';
import { SystemInfoEN, SystemInfoVI } from '../modules/SystemInfo/language/index';
import { ValidEN, ValidVI } from '../utils/language/index';

import { Account_EN, Account_VI } from '../modules/Account/zLanguage/index';
import { DashBoard_EN, DashBoard_VI } from '../modules/Dashboard/zLanguage/index';
import { Setting_EN, Setting_VI } from '../modules/Setting/zLanguage/index';
import { ComponentInfo_EN, ComponentInfo_VI } from '../modules/ComponentInfo/zLanguage/index';
import { Company_EN, Company_VI } from '../modules/Company/zLanguage/index';

const vi_VN = {
    ...Account_VI, ...Company_VI,
    ...ComponentVI, ...RoleVI, ...PermissionVI, ...UserVI, ...DashBoard_VI, ...SystemInfoVI, ...ValidVI, ...Setting_VI,
    ...ComponentInfo_VI,
}
const en_US = {
    ...Account_EN, ...Company_EN,
    ...ComponentEN, ...RoleEN, ...PermissionEN, ...UserEN, ...DashBoard_EN, ...SystemInfoEN, ...ValidEN, ...Setting_EN,
    ...ComponentInfo_EN,
}

export const MessageLanguage = {
    vi_VN: {
        key: "vi-VN",
        locale: "vi-VN",
        defaultLocale: "vi-VN",
        messages: vi_VN,
    },
    en_US: {
        key: "en-US",
        locale: "en-US",
        defaultLocale: "en-US",
        messages: en_US,
    }
}

export const MESSAGES = {
    Required: "valid.required",
    ValidImageFormat: "valid.image_format",
    ValidCodeExists: "valid.code_exists",
    ValidEmail: "valid.email",
    ValidEmailExists: "valid.email_exists",
    ValidPassConfirm: "valid.pass_confirm",

    CPControlAdd: "component.add",
    CPControlEdit: "component.edit",
    CPControlDuplicate: "component.duplicate",
    CPControlCancel: "component.cancel",
    CPControlClose: "component.close",
    CPControlDeleteQS: "component.delete_question",
    CPControlDelOK: "component.delete_ok",
    CPControlDelAll: "component.delete_all",
    CPControlSucces: "component.success",
    CPControlError: "component.error",
    CPControlInfo: "component.info",
    CPControlNotifi: "component.notifi",
    CPControlWarning: "component.warning",

    GeneralHomePage: "general.homepage",
    GeneralList: "general.list",
    GeneralCreated_by: "general.created_by",
    GeneralCreated_at: "general.created_at",
    GeneralModified_by: "general.modified_by",
    GeneralModified_at: "general.modified_at",
    GeneralIsInactive: "general.isinactive",
    GeneralActive: "general.active",
    GeneralzID: "general.zid",
    GeneralEmail: "general.email",
    GeneralTel: "general.tel",
    GeneralFax: "general.fax",
    GeneralPeriod: "general.period",
    GeneralFromDate: "general.fromDate",
    GeneralToDate: "general.toDate",
    GeneralDate: "general.date",
    GeneralDateMoment: "general.dateMoment",
    GeneralChose: "general.chose",
    GeneralChoseAll: "general.chose_all"
}