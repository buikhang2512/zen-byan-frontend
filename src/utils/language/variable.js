const prefixValidForm = "valid."

export const ValidError = {
   formik: validFormik,

   Required: {
      id: prefixValidForm + "required",
      defaultMessage: "Không được bỏ trống trường này",
      en: "Required",
      fr: "",
      ch: ""
   },
   EmailFormat: {
      id: prefixValidForm + "email_format",
      defaultMessage: "Email không đúng định dạng",
      en: "Invalid email address"
   },
   EmailExists: {
      id: prefixValidForm + "email_exists",
      defaultMessage: "Email không tồn tại",
      en: ""
   },
   CodeExists: {
      id: prefixValidForm + "code_exists",
      defaultMessage: "Mã bị trùng. Vui lòng nhập mã khác",
      en: ""
   },
   PasswordOld: {
      id: prefixValidForm + "PasswordOld",
      defaultMessage: "Mật khẩu cũ không đúng",
      en: ""
   },
   Password: {
      id: prefixValidForm + "pass_confirm",
      defaultMessage: "Mật khẩu không đúng",
      en: ""
   },
   PasswordRe: {
      id: prefixValidForm + "PasswordRe",
      defaultMessage: "Mật khẩu xác nhận không đúng",
      en: ""
   },
   MaxLength: {
      id: prefixValidForm + "MaxLength",
      defaultMessage: "Tối đa {value0} kí tự",
      en: "Max length {value0}"
   },
   MinLength: {
      id: prefixValidForm + "MinLength",
      defaultMessage: "Ít nhất {value0} kí tự",
      en: "Min length {value0}"
   }
}

/**
* Valid formik form multi language
* @param {object} {name: field name, type: data type}
* @param {object} arguments { type: Method name, params: [] params of method, intl: object by react-intl }
*/
function validFormik({ name, type }) {
   let validations = [], i = 1;
   for (i; i < arguments.length; i++) {
      validations.push(arguments[i])
   }
   return {
      id: name,
      validationType: type,
      validations: validations,
   }
}

// Ví dụ: xem trong ARDMKH ZenBook
// ValidError.formik({ name: "ma_kh", type: "string" },
//    set message:
//    { type: "required", params: ["Không được bỏ trống trường này"] },
//    { type: "max", params: [20, "Tối đa 20 kí tự"] },

//    multi language:
//    { type: "required", intl: {id:"test", defaultMessage:"Không được bỏ trống trường này"} },
//    { type: "min", params: [3], intl: {id:"test", defaultMessage:"PHải có ít nhất {value0} kí tự"} },
//    { type: "max", params: [20], intl: { ...IntlFormat.default(ValidError.MaxLength) }  },
// )
