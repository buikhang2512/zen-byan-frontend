import { IntlFormat, LanguageFormat } from "../intlFormat";
import { ValidError } from "./variable";

// vi
const ValidVI = {
   ...IntlFormat.setMessageLanguage(ValidError),
}

// en
const ValidEN = {
   ...IntlFormat.setMessageLanguage(ValidError, LanguageFormat.en),
}

export { ValidVI, ValidEN }