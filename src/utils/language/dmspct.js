const prefixCoDmSpct = 'CoDmSpct.';

export const Mess_CoDmSpct = {
    Header:{
        id: prefixCoDmSpct + 'header',
        defaultMessage: "Công trình",
        en: "construction"
    },
    Ma: {
        id: prefixCoDmSpct + 'ma_spct',
        defaultMessage: "Mã công trình",
        en: "Building code"
    },
    Ten: {
        id: prefixCoDmSpct + 'ten_spct',
        defaultMessage: " Tên công trình",
        en: ""
    },
    Nhom: {
        id: prefixCoDmSpct + 'ma_nhspct',
        defaultMessage: "Mã Nhóm SPCT",
        en: "The name of construction"
    },
    Dvt: {
        id: prefixCoDmSpct + 'dvt',
        defaultMessage: "Đơn vị tính",
        en: "Unit"
    },
    Ngay_kc: {
        id: prefixCoDmSpct + 'ngay_kc',
        defaultMessage: "Ngày khởi công",
        en: "Commencement date"
    },
    Ngay_kt: {
        id: prefixCoDmSpct + 'ngay_kt',
        defaultMessage: "Ngày kết thúc",
        en: "End date"
    },
    Du_toan: {
        id: prefixCoDmSpct + 'du_toan',
        defaultMessage: "Dự toán",
        en: "Estimates"
    },
    Du_toan_NT: {
        id: prefixCoDmSpct + 'du_toan_nt',
        defaultMessage: "Dự toán NT",
        en: ""
    },
    Dv_tc: {
        id: prefixCoDmSpct + 'dv_tc',
        defaultMessage: "Đơn vị thi công",
        en: "Construction unit"
    },
    Ghi_chu: {
        id: prefixCoDmSpct + 'ghi_chu',
        defaultMessage: "Ghi chú",
        en: "Note"
    },
}