import React from "react";
import moment from "moment/moment";
import memoize from "memoize-one";
import { isEmpty } from "lodash";
import { ZenMessageAlert, ZenMessageToast } from "../components/Control";
import { GlobalStorage, KeyStorage } from "./storage";

const parse = JSON.parse;
const stringify = JSON.stringify;

function getBaseUrl(url) {
  var pathArray = url.split("/");
  var protocol = pathArray[0];
  var host = pathArray[2];
  return protocol + "//" + host + "/";
}
const messageIntl = memoize((id, defaultText, values) => {
  return ZenHelper.intl
    ? ZenHelper.intl.formatMessage(
      {
        id: id,
        defaultMessage: defaultText || id,
      },
      values ? { ...values } : undefined
    )
    : defaultText || id;
});

export const FormMode = {
  NON: -1,
  VIEW: 0,
  ADD: 1,
  EDIT: 2,
  DEL: 3,
  PRINT: 4,
  DUPLICATE: 5,
  SEARCH: 6,
};

export const ZenApp = {
  //Cấu hình đường dẫn backend tại scripts/start.js hoặc scripts/build.js
  baseUrlAPI: process.env.API_URL,
  baseUrl: getBaseUrl(process.env.API_URL),
  signalRUrl: process.env.SIGNALR_URL,
  ZENAPP_NAME: process.env.ZENAPP_NAME,
  ZENAPP_FULL_NAME: process.env.ZENAPP_FULL_NAME,
  ZENAPP_COLOR_ACCENT: "#178600", //'#21BA45'
};

export const ZenHelper = {
  fileInfo: {
    img: "(JPG|JPEG|PNG|GIF)",
    pdf: "PDF",
    doc: "DOC|DOCX",
    lengthExt: 4,
  },

  intl: null,

  SetIntl(intl) {
    this.intl = { ...intl };
  },

  GetNumVl(value) {
    if (value) {
      return value;
    } else {
      return 0;
    }
  },
  GetMessage(id, defaultMessage, values) {
    return messageIntl(id, defaultMessage, values);
  },
  highlightText(text, higlight) {
    //Split on higlight term and include term into parts, ignore case
    let parts = text.split(new RegExp(`(${higlight})`, "gi"));
    return (
      <span>
        {" "}
        {parts.map((part, i) => (
          <span
            key={i}
            style={
              part.toLowerCase() === higlight.toLowerCase()
                ? { fontWeight: "bold", color: "blue" }
                : {}
            }
          >
            {part}
          </span>
        ))}{" "}
      </span>
    );
  },

  isValidDate(d) {
    return (
      d &&
      !isNaN(d) &&
      new Date(d).toString() !== "Invalid Date" &&
      Object.prototype.toString.call(d) === "[object Date]"
    );
  },

  formatNumber(amount, decimalCount) {
    if (amount > 0 && decimalCount && decimalCount === 0) {
      return amount
        .toFixed(decimalCount)
        .replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
    } else {
      return amount ? (isNaN(amount) ? 0 : Number(amount)) : 0;
    }
  },

  formatDateTime(date, formatString = "DD/MM/YYYY") {
    if (!date) return "";

    return moment(date).format(formatString);
  },

  daysInMonth(year, month) {
    return new Date(year, month + 1, 0).getDate();
  },

  addDays(date, days) {
    // date should YYYY-MM-DD
    var newDate = new Date(date);
    newDate.setDate(newDate.getDate() + days);
    return newDate;
  },

  addMonths(date, months) {
    var newDate = new Date(date);
    newDate.setMonth(newDate.getMonth() + months);
    return newDate;
  },

  addYears(date, years) {
    var newDate = new Date(date);
    newDate.setFullYear(newDate.getFullYear() + years);
    return newDate;
  },

  translateListToSelectOptions: function (
    items,
    addAll = false,
    key = "id",
    value = "id",
    text = "name",
    cols = [],
    showFields = ""
  ) {
    if (items) {
      var arr = items.map((x) => {
        let temp = {};
        if (showFields) {
          let str = ""; //x[text]
          showFields.split(",").forEach((t) => {
            str += " - " + x[t];
          });
          str = str.replace(" - ", "");
          temp = { key: x[key], value: x[value], text: str };
          if (text) {
            temp[text] = x[text];
          }
        } else {
          temp = { key: x[key], value: x[value], text: x[text] };
        }

        if (cols) {
          cols.forEach((col) => {
            temp[col] = x[col];
          });
        }

        return temp;
      });

      if (addAll) return [{ key: "", value: "", text: "-Select-" }, ...arr];
      else return arr;
    } else {
      if (addAll) return [{ key: "", value: "", text: "-Select-" }];
      else return [];
    }
  },

  convertToSelectOptions(
    items,
    addAll = false,
    key,
    value,
    text = "",
    cols = []
  ) {
    if (items) {
      var listOption = items.map((x) => {
        let option = {},
          strText = "";
        const display = text.split(",") || [];

        display.forEach((col) => {
          strText += " - " + x[col];
          option[col] = x[col];
        });

        strText = strText.replace(" - ", "");
        option = { ...option, key: x[key], value: x[value], text: strText };

        if (cols.length > 0) {
          cols.forEach((col) => {
            if (!option.hasOwnProperty(col)) {
              if (typeof x[col] === "boolean") {
                option[col] = x[col] ? "true" : "false";
              } else {
                option[col] = x[col];
              }
            }
          });
        }

        return option;
      });

      if (addAll)
        return [{ key: "", value: "", text: "-Select-" }, ...listOption];
      else return listOption;
    } else {
      if (addAll) return [{ key: "", value: "", text: "-Select-" }];
      else return [];
    }
  },

  selectedImage(e) {
    return new Promise((resolve) => {
      if (e.target.files.length > 0) {
        let reader = new FileReader();
        let file = e.target.files[0];
        var image = {};

        image.file_name = file.name;
        image.file_size = file.size;
        image.file_type = file.type;

        if (!this.checkMimeTypeImage(e)) {
          ZenMessageToast.error(
            ZenHelper.GetMessage(
              "error.image_format",
              "File upload sai định dạng. (png, jpeg, gif)"
            )
          );
          resolve(undefined);
        } else {
          reader.onloadend = () => {
            image.file_base64 = reader.result;
            resolve(image);
          };
          reader.readAsDataURL(file);
        }
      }
    });
  },

  checkMimeTypeImage(event) {
    //getting file object
    let files = event.target.files;
    //define message container
    let err = "";
    // list allow mime type
    const types = ["image/png", "image/jpeg", "image/gif"];
    // loop access array
    for (var x = 0; x < files.length; x++) {
      // compare file type find doesn't matach
      if (types.every((type) => files[x].type !== type)) {
        // create error message and assign to container
        err += files[x].type + " is not a supported format\n";
      }
    }

    if (err !== "") {
      // if message not same old that mean has error
      event.target.value = null; // discard selected file
      return false;
    }
    return true;
  },

  getfirstDayOfMonth(month, year) {
    if (month > 12 || month < 0) {
      return null;
    }
    return new Date(year, month, 1);
  },

  getLastDayOfMonth(month, year) {
    if (month > 12 || month < 0) {
      return null;
    }
    return new Date(year, month + 1, 0, 23, 59);
  },

  getfirstDayOfCurrentMonth() {
    var date = new Date();
    var month = date.getMonth(),
      year = date.getFullYear();
    return this.getfirstDayOfMonth(month, year);
  },

  getLastDayOfCurrentMonth() {
    var date = new Date();
    var month = date.getMonth(),
      year = date.getFullYear();
    return this.getLastDayOfMonth(month, year);
  },

  getFirstDayOfFiscalMonth(year, month) {
    if (month > 12 || month < 0) {
      return null
    }
    const data = GlobalStorage.getByField(KeyStorage.CacheData, "sidmnamtc")?.data
    const namtc = data.filter(d => d.nam === year)[0]
    const fdate = new Date(namtc.ngay_dntc)
    const fday = fdate.getDate()
    const fMonth = fdate.getMonth()
    const fYear = fdate.getFullYear()
    return this.formatDateTime(new Date(fYear, fMonth + month - 1, fday), "YYYY-MM-DD")
  },

  getLastDayOfFiscalMonth(year, month) {
    if (month > 12 || month < 0) {
      return null
    }
    const data = GlobalStorage.getByField(KeyStorage.CacheData, "sidmnamtc")?.data
    const namtc = data.filter(d => d.nam === year)[0]
    const fdate = new Date(namtc.ngay_dntc)
    const fday = fdate.getDate()
    const fMonth = fdate.getMonth()
    const fYear = fdate.getFullYear()
    return this.formatDateTime(new Date(fYear, fMonth + month, fday - 1), "YYYY-MM-DD")
  },

  getNamTC(date) {
    const dDate = new Date(date)
    const listNamTc = GlobalStorage.getByField(KeyStorage.CacheData, "sidmnamtc")?.data

    return listNamTc.find(item => {
      if (item.ngay_dntc && item.ngay_cntc
        && dDate >= new Date(item.ngay_dntc)
        && dDate <= new Date(item.ngay_cntc)
      ) {
        return true
      }
    })
  },

  checkNgayKs(date) {
    // kiểm tra đã khóa sổ: dùng trong core
    const dDate = new Date(date)
    const namTC = ZenHelper.getNamTC(date)

    if (namTC?.ngay_ks && dDate <= new Date(namTC.ngay_ks)) {
      return namTC // đã khóa số liệu
    }
    return
  },

  checkNgayKsAuto(date) {
    // kiểm tra ngày khóa sổ: dùng cho các tính năng tự động
    // get ngày đầu tháng tc
    const dDate = new Date(date)
    const dFirstMonthTC = ZenHelper.getFirstDayOfFiscalMonth(dDate.getFullYear(), dDate.getMonth())
    // get năm tc
    const namTC = ZenHelper.getNamTC(date)
    const dNgayKs = namTC ? new Date(namTC.ngay_ks) : ""
    if (dNgayKs && dDate
      && dDate <= dNgayKs
    ) {
      ZenMessageAlert.error("Đã khóa số liệu đến ngày " + ZenHelper.formatDateTime(dNgayKs))
      return true
    }
    return false
  },

  getResponseError(res = {}) {
    if (res.response && res.response.data) {
      const isSuccess = res.response.data.hasOwnProperty("Success")
        ? res.response.data.Success
        : res.response.data.success;
      const ErrorMessage = res.response.data.hasOwnProperty("Messages")
        ? res.response.data.Messages
        : res.response.data.messages;
      if (isSuccess === false) {
        return ErrorMessage;
      }
      return ["" + res];
    } else {
      return ["" + res];
    }
  },

  removeAccents(str) {
    // bỏ dấu trong chuỗi
    return str
      .normalize("NFD")
      .replace(/[\u0300-\u036f]/g, "")
      .replace(/đ/g, "d")
      .replace(/Đ/g, "D");
  },

  createElementId(code = "") {
    const doc = document.getElementById(code);
    if (!code || doc) {
      return code + new Date().getTime().toString();
    }
    return code;
  },

  f_OpenNewWindow(url, isNewTab = false) {
    if (url) {
      if (isNewTab) {
        // open new tab
        window.open(url, "_blank")
      } else {
        // open window
        var width = (screen.width / 2) + 200,  // width new window = 1/2 screen
          height = (screen.height / 2) + 200, // height new window = 1/2 screen
          left = width - (width / 2) - 300,   // set left screen center
          top = height - (height / 2) - 300   // set top screen center
        window.open(url, "_blank", `width=${width},height=${height},top=${top},left=${left}`)
      }

    }
  },

  extractString([beg, end]) {
    const matcher = new RegExp(`${beg}(.*?)${end}`, 'gm');
    const normalise = (str) => str.slice(beg.length, end.length * -1);
    return function (str) {
      return str.match(matcher).map(normalise);
    }
  },

  getElmHeight(element, list = []) {
    // list = ['height','margin-top','margin-bottom','border-top',
    // 'border-bottom', 'padding-top','padding-bottom']
    const style = window.getComputedStyle(element)
    return list
      .map(k => parseInt(style.getPropertyValue(k), 10))
      .reduce((prev, cur) => prev + cur)
  },
  getTyGiaNt(ngay, ma_nt) {
    const ngay1 = new Date(ZenHelper.formatDateTime(ngay, "YYYY-MM-DD 23:59:00"))
    const data = GlobalStorage.getByField(KeyStorage.CacheData, "sidmtgnt")?.data

    const result = data?.filter(t => {
      const ngay_tg = new Date(t.ngay_tg)
      return t.ma_nt === ma_nt && ngay_tg <= ngay1
    }).sort((a, b) => {
      return new Date(b.ngay_tg) - new Date(a.ngay_tg);
    })

    return result?.length > 0 ? result[0] : null
  },

  // ====================================== cache/storage/session
  /**
  * get cache
  */

  addCache(key, value) {
    this.setAppStorage(value, key, true);
  },

  getCached(key) {
    let value = null;

    value = this.getAppStorage(key);
    return value;
  },
  /**
   * Clear all app storage
   */
  clearAllAppStorage() {
    if (localStorage) {
      localStorage.clear();
    }
    if (sessionStorage) {
      sessionStorage.clear();
    }
  },

  clearAllSessionStorage() {
    if (sessionStorage) {
      sessionStorage.clear();
    }
  },

  /**
   * Remove an item from the used storage
   * @param  {String} key [description]
   */
  clearAppStorage(key) {
    if (localStorage && localStorage.getItem(key)) {
      return localStorage.removeItem(key);
    }

    if (sessionStorage && sessionStorage.getItem(key)) {
      return sessionStorage.removeItem(key);
    }

    return null;
  },

  /**
   * Returns data from storage
   * @param  {String} key Item to get from the storage
   * @return {String|Object}     Data from the storage
   */
  getAppStorage(key) {
    if (localStorage && localStorage.getItem(key)) {
      return parse(localStorage.getItem(key)) || null;
    }

    if (sessionStorage && sessionStorage.getItem(key)) {
      return parse(sessionStorage.getItem(key)) || null;
    }

    return null;
  },

  /**
   * Set data in storage
   * @param {String|Object}  value    The data to store
   * @param {String}  key
   * @param {Boolean} isLocalStorage  Defines if we need to store in localStorage or sessionStorage
   */
  setAppStorage(value, key, isLocalStorage) {
    if (isEmpty(value)) {
      return null;
    }

    if (isLocalStorage && localStorage) {
      return localStorage.setItem(key, stringify(value));
    }

    if (sessionStorage) {
      return sessionStorage.setItem(key, stringify(value));
    }

    return null;
  },
  getFiscalYear() {
    return (
      GlobalStorage.getByField(KeyStorage.Global, "financial_year") ||
      new Date().getFullYear()
    );
  },
  getOptYear(y, step = 5, yStart = 2015) {
    // get nam tai 9
    const result = ZenHelper.getDataLocal("sidmnamtc")?.data || [];
    if (!result || result.length === 0) {
      if (y > 1900) {
        for (let index = yStart; index <= step + y; index++) {
          result.push({ key: index, value: index, text: index });
        }
        return result;
      }
    }

    return result.map((t) => ({
      key: t.nam,
      value: t.nam,
      text: t.nam?.toString(),
    }));
  },
  getOptMonth(fMonth = 1, lMonth = 12) {
    if (fMonth < 1 || fMonth > 12 || fMonth > lMonth) {
      fMonth = 1;
    }
    if (lMonth < 1 || lMonth > 12 || fMonth > lMonth) {
      lMonth = 12;
    }
    let result = [];
    for (let index = fMonth; index <= lMonth; index++) {
      result.push({ key: index, value: index, text: index });
    }
    return result;
  },
  genValidNumFormik(id, type, vldTions = []) {
    let vls = [];
    if (vldTions.length > 0) {
      vls = vldTions;
    } else {
      switch (type) {
        case "string":
        case "date":
          vls.push({
            type: "required",
            params: ["Không được bỏ trống trường này"],
          });
          break;
        case "number":
          vls.push(
            {
              type: "typeError",
              params: ["Không được bỏ trống trường này"],
            },
            {
              type: "moreThan",
              params: [0, "Giá trị phải lớn hơn 0"],
            }
          );
          break;
        default:
          break;
      }
    }
    let kq = {
      id: id,
      validationType: type,
      validations: vls,
    };
    return kq;
  },
  getDataLocal(code) {
    const tables = GlobalStorage.get(KeyStorage.CacheData);
    return tables && tables[code.toLowerCase()];
  },
};
