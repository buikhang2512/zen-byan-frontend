import React, { useContext, useEffect, useState } from 'react'
import { Dropdown } from "semantic-ui-react";
import { ZenHelper, ZenApp } from "./global";
import { GlobalStorage, KeyStorage } from "./storage";
import auth from "./auth";
import _ from "lodash";
import * as routes from "../constants/routes";
import { ZenMessageAlert } from '../components/Control';
import { ApiUser } from '../Api';
import { ApiAccount } from '../modules/Account/Api/ApiAccount';
import { default as LoginSelectTenant } from '../modules/Account/Login/LoginSelectTenant';
import { useHistory } from 'react-router';
import { ZenLookup } from '../modules/ComponentInfo/Dictionary/ZenLookup';
import { AppContext } from '../AppContext';

const FinancialYear = () => {
  const [listYear, setListYear] = useState([])
  const [year, setYear] = useState(new Date().getFullYear())
  const [open, setOpen] = useState(false)

  useEffect(() => {
    initNamTC()
  }, [ZenHelper.getDataLocal(ZenLookup.Nam_tc.code)?.version])

  useEffect(() => {
    const yearCur = new Date().getFullYear()
    setYear(ZenHelper.getFiscalYear() || yearCur)
  }, [])

  function initNamTC() {
    const data = ZenHelper.getDataLocal(ZenLookup.Nam_tc.code)?.data
    if (data) {
      const listNamTC = ZenHelper.convertToSelectOptions(data,
        false, 'nam', 'nam', 'nam', ['ngay_dntc', 'ngay_cntc'])

      setListYear(listNamTC)
    }
  }

  const onClick = (e, { value }, option) => {
    if (value === year) {
      return
    }

    ZenMessageAlert.question("Sau khi thay đổi năm làm việc, hệ thống sẽ điều hướng về trang chủ. Nếu có dữ liệu chưa lưu, vui lòng lưu lại dữ liệu trước khi thay đổi. Bạn chắc chắn thay đổi năm làm việc không?")
      .then(res => {
        if (res === 1) {
          setYear(value)
          // set storage
          const global = GlobalStorage.setByField(KeyStorage.Global, 'financial_year', value)
          if (global) {
            const fromDate = new Date(global.from_date).setFullYear(value)
            const toDate = new Date(global.to_date).setFullYear(value)
            GlobalStorage.set(KeyStorage.Global,
              {
                ...global,
                from_date: ZenHelper.formatDateTime(fromDate, 'YYYY-MM-DD'),
                to_date: ZenHelper.formatDateTime(toDate, 'YYYY-MM-DD 23:59:00'),
                ngay_dntc: option.ngay_dntc,
                ngay_cntc: option.ngay_cntc,
              })
          }
          // route to home
          location.assign("/#")
          location.reload()
        }
      })
    setOpen(false)
  }

  return <Dropdown text={`Năm ${year}`} item
    onClose={() => setOpen(false)}
    onOpen={() => setOpen(true)}
    openOnFocus={false}
    open={open}
  >
    <Dropdown.Menu>
      <Dropdown.Header icon='tags' content='Năm tài chính' />
      <Dropdown.Menu scrolling>
        {listYear.filter(x => open).map((option) => (
          <Dropdown.Item key={option.value} onClick={(e, element) => onClick(e, element, option)}
            active={option.value === year}
            icon={option.value === year ? { name: "check", color: "green" } : undefined}
            value={option.value} text={option.text} />
        ))}
      </Dropdown.Menu>
    </Dropdown.Menu>
  </Dropdown>
}

const SelectTenant = () => {
  const [listTenant, setListTenant] = useState([])
  const [tenant, setTenant] = useState({})
  const [open, setOpen] = useState(false)
  const history = useHistory();
  const { actions, store } = useContext(AppContext);

  useEffect(() => {
    ApiAccount.getTenantsByUser(res => {
      if (res.status === 200) {
        //identifier
        setListTenant(res.data.data)
        const tenantCurrent = res.data.data.find(t => t.identifier == auth.getTenantId())
        setTenant(tenantCurrent)

        // lưu vào global context, để update tên tenant khi update thông tin công ty
        actions.setTenantName(tenantCurrent.name)
      }
    })
  }, [])

  useEffect(() => {
    if (store.tenantName) {
      setTenant({ ...tenant, name: store.tenantName })
    }
  }, [store.tenantName])

  const handleOpenCloseTenant = () => setOpen(!open)

  const handleSelectedTenant = (item) => {
    if (tenant.identifier === item.identifier) {
      handleOpenCloseTenant()
      return
    }
    // khi chuyển đơn vị, clear cache của đơn vị cũ
    GlobalStorage.clear(KeyStorage.CacheData)

    const params = {
      tenant: item.identifier,
      token: auth.getToken()
    }
    ApiAccount.refreshToken(params, res => {
      if (res.status === 200) {
        const newToken = res.data.data.access_token
        // set token
        auth.setToken(newToken, true);
        auth.saveCurrentSession();
        // route to home
        location.assign("/#")
        location.reload()

        setTenant(item)
      } else {
        const temp = ZenHelper.getResponseError(res) || []
        ZenMessageAlert.error(temp)
      }
    })
  }

  const handleCompanyInfo = () => {
    history.push(routes.Company)
  }

  return <>
    <Dropdown text={tenant.name} item
      openOnFocus={false}
    >
      <Dropdown.Menu>
        <Dropdown.Header icon='tags' content='Công ty' />
        <Dropdown.Menu scrolling>
          <Dropdown.Item key={"companyInfo"} name={"companyInfo"}
            content="Thông tin"
            onClick={handleCompanyInfo}
          />
          <Dropdown.Item key={"changeTenant"} name={"changeTenant"}
            content="Chuyển đơn vị"
            onClick={handleOpenCloseTenant}
          />
        </Dropdown.Menu>
      </Dropdown.Menu>
    </Dropdown>

    <LoginSelectTenant open={open}
      tenants={listTenant}
      tenantCurrent={tenant}
      onClose={handleOpenCloseTenant}
      onSelected={handleSelectedTenant}
    />
  </>
}

const navs = {
  GetleftItems(menu, module) {
    var strmn = "menu";
    var menuInStorage = ZenHelper.getAppStorage(strmn);

    if (menuInStorage) {
      return menuInStorage;
    }
    var mn = [];
    _.forEach(module, (item) => {
      if (item.id != ZenApp.SETTING_MODULE) {
        var curmenu = _.filter(menu, (x) => {
          return item.id == x.module_id && x.isHide == false;
        });
        if (curmenu.length > 0) {
          mn.push({
            text: item.name,
            key: item.id,
            dropdowns: curmenu.map((i) => {
              return { content: i.text, to: i.url, href: i.url, key: i.id };
            }),
          });
        }
      }
    });
    if (mn.length > 0) {
      ZenHelper.setAppStorage(mn, strmn);
    }
    return mn;
  },

  getUserNav() {
    var user = auth.getUserInfo();
    var fullName = user ? user.fullName : "Unknow";

    return {
      text: fullName,
      direction: "left",
      key: "user_logined",
      dropdowns: [
        {
          //as: "Link",
          content: "Bảo trì dữ liệu",
          //to: routes.Profile,
          icon: "server",
          key: "maintenance-clearcache",
        },
        {
          content: "Đổi mật khẩu",
          icon: "key",
          key: "account.change-password",
        },
        {
          content: "Đăng xuất",
          icon: "sign-out",
          key: "account.sign-out",
        },
      ],
    };
  },

  rightItems() {
    let rightMenus = [];
    var info = {
      as: "Link",
      content: "",
      to: routes.SystemInfo,
      icon: "info",
      key: "info",
    };
    var support = {
      as: "Link",
      content: "",
      to: "https://zentech.vn/books/help/index.html",
      icon: "help circle",
      target: "_blank",
      rel: "noopener noreferrer",
      key: "support",
      is_external: true
    };
    var setting = {
      as: "Link",
      content: "",
      to: routes.Settings,
      icon: "settings",
      key: "settings",
    };

    // rightMenus.push({ component: <SelectTenant key="tenants" /> });
    rightMenus.push({ component: <FinancialYear key="financial" /> });

    rightMenus.push(setting);

    if (auth.getToken() !== null) {
      rightMenus.push(navs.getUserNav());
    }

    // rightMenus.push(info);
    //rightMenus.push(support);
    return rightMenus;
  },
};

export default navs;
