import memoize from 'memoize-one';

const IntlFormat = {
   intl: {},
   setIntl(intl) {
      this.intl = { ...intl };
   },

   text(objValue) {
      return messageIntl(objValue.id, objValue.defaultMessage, objValue.values)
   },

   label(objValue, isPlaceholder = false) {
      if (isPlaceholder) {
         return {
            placeholder: objValue.id,
            defaultplaceholder: objValue.defaultMessage
         }
      } else {
         return {
            label: objValue.id,
            defaultlabel: objValue.defaultMessage
         }
      }
   },

   default(objValue) {
      return {
         id: objValue.id,
         defaultMessage: objValue.defaultMessage
      }
   },

   setMessageLanguage(objMessList, keyObj = "defaultMessage") {
      let result = {}
      Object.keys(objMessList).map(objValue => {
         result[objMessList[objValue].id] = objMessList[objValue][keyObj]
      })
      return result
   },
}

const messageIntl = memoize((id, defaultText, values) => {
   return IntlFormat.intl.formatMessage ?
      IntlFormat.intl.formatMessage({
         id: id,
         defaultMessage: defaultText || id
      }, values ? { ...values } : undefined)

      : (defaultText || id)
})

const LanguageFormat = {
   vi: "vi",
   en: "en",
   ch: "ch"
}

export { IntlFormat, LanguageFormat }