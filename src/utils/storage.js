const parse = JSON.parse;
const stringify = JSON.stringify;

/**
 * key app: Global = {
 *    language: "",
 *    financial_year: "",
 *    from_date: "",
 *    to_date: "",
 *    ngay_dntc:"",
 *    ngay_cntc:"",
 * }
 * key riêng theo từng module: in,so,po,...
 */
export const KeyStorage = {
   Global: "global",
   CacheData: "cacheData",
   IN: "in",
   SO: "so"
}
export const GlobalStorage = {
   checkDataLookup(infoLookup) {
      if (!infoLookup.localData) return

      const temp = this.get(infoLookup.localData)
      if (!temp || !temp[infoLookup.code.toLowerCase()]) {
         return false
      }
      return true
   },

   get(key, isLocalStorage = true) {
      if (!key) {
         return
      }
      if (isLocalStorage && localStorage && localStorage.getItem(key)) {
         return parse(localStorage.getItem(key));
      }
      return
   },

   getByField(key, field, isLocalStorage = true) {
      if (!field) {
         return
      }
      const item = GlobalStorage.get(key, isLocalStorage)
      return item ? item[field] : undefined
   },

   set(key, value, isLocalStorage = true) {
      if (!key) {
         return
      }
      if (isLocalStorage && localStorage) {
         localStorage.setItem(key, stringify(value));
         return value
      }
      return
   },

   setByField(key, field, value, isLocalStorage = true) {
      if (!field) {
         return
      }
      const item = GlobalStorage.get(key, isLocalStorage)
      if (item) {
         item[field] = value ? value : ""
         GlobalStorage.set(key, item, isLocalStorage);
         return item
      }
      return
   },

   clear(key, isLocalStorage = true) {
      if (!key) {
         return
      }

      if (isLocalStorage && localStorage && localStorage.getItem(key)) {
         return localStorage.removeItem(key);
      }
      return
   },
}