import { ZenHelper } from "./global";
import axios from '../Api/axios';
import { GlobalStorage, KeyStorage } from "./storage";

const TOKEN_KEY = 'access_token';
const USER_INFO = 'userInfo';
const TENANT_INFO_KEY = "tenantInfo";
const CACHE = 'cacheData';

const auth = {

   CheckExpireSession() {
      var decoded = this.getUserInfo();
      if (!decoded) {
         return;
      }
      var date = new Date(0);
      date.setUTCSeconds(decoded.exp - 1);
      if ((date.valueOf() < (new Date().valueOf()))) {
         this.clearToken();
         this.clearUserInfo();
      }

   },

   //User Info Storage
   setUserInfo(value = '', isLocalStorage = false, userInfo = USER_INFO) {
      return ZenHelper.setAppStorage(value, userInfo, isLocalStorage);
   },
   getUserInfo(userInfo = USER_INFO) {
      return ZenHelper.getAppStorage(userInfo);
   },
   clearUserInfo(userInfo = USER_INFO) {
      return ZenHelper.clearAppStorage(userInfo);
   },
   getTenantId() {
      var userInfo = this.getUserInfo();
      if (userInfo)
         return userInfo.tenantid
      return ""
   },

   getCacheStorage(cacheData = CACHE) {
      return ZenHelper.getAppStorage(cacheData);
   },

   //Tenant Info Storage
   setTenantInfo(value = '') {
      return ZenHelper.setAppStorage(value, TENANT_INFO_KEY, true);
   },
   getTenantInfo() {
      return ZenHelper.getAppStorage(TENANT_INFO_KEY);
   },
   clearTenantInfo() {
      return ZenHelper.clearAppStorage(TENANT_INFO_KEY);
   },

   //Access Token
   setToken(value = '', isLocalStorage = false, tokenKey = TOKEN_KEY) {
      return ZenHelper.setAppStorage(value, tokenKey, isLocalStorage);
   },
   getToken(tokenKey = TOKEN_KEY) {
      var token = ZenHelper.getAppStorage(tokenKey);

      return token;
   },
   clearToken(tokenKey = TOKEN_KEY) {
      this.clearCurrentSession();

      return ZenHelper.clearAppStorage(tokenKey);
   },
   parseToken(token) {
      var base64Url = token.split('.')[1];

      return JSON.parse(decodeURIComponent(escape(window.atob(base64Url))));
      //var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
      //return JSON.parse(window.atob(base64))
   },

   logout(e) {
      this.clearToken();
      GlobalStorage.clear(KeyStorage.CacheData)
      GlobalStorage.clear(KeyStorage.Global)
   },

   isLoggedIn() {
      return auth.getToken() !== null;
   },

   checkLogin() {
      return auth.getToken() !== null;
   },

   saveCurrentSession() {
      const obj = this.parseToken(this.getToken());
      const userInfo = {
         "userId": obj.jti,
         "fullName": obj.sub,
         "hrid": obj.hrid,
         "tenantid": obj.tenantid,
         "admin": obj.Admin,
         "permisson": obj.Permission,
         "exp": obj.exp
      };
      this.setUserInfo(userInfo, true);
   },

   checkPermission(permisson) {
      var user = this.getUserInfo()
      if(!user) return false
      if (user.admin === "True") {
         return true;
      } else {
         return !permisson ? false : user.permisson.includes(permisson);
      }
   },

   clearCurrentSession() {
      this.clearUserInfo();
      ZenHelper.clearAllSessionStorage();
   }
};

export default auth;
