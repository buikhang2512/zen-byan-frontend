import React, { useEffect, useReducer, useState } from "react";
import * as signalR from '@aspnet/signalr'
import { ZenApp, ZenHelper } from "./utils/global";
import { IntlFormat } from "./utils/intlFormat";
import { IntlProvider, createIntl, createIntlCache } from "react-intl";
import { MessageLanguage } from "./utils/Messages";
import { ZenResponsiveProvider } from "./components/Control/ZenResponsive";
import auth from "./utils/auth";
import { cacheData } from "./cacheData";
const cache = createIntlCache()
const AppContext = React.createContext();

class AppProviderWrapper extends React.Component {
   constructor(props) {
      super();
      this.hubConnection = null;
      this.num_of_tryconnect = 0;
      this.initSignalRConnection(false)
      this.setInit(MessageLanguage.vi_VN);

      this.state = {
         language: MessageLanguage.vi_VN,
      };
   }

   setInit(language) {
      //Global intl
      var intl = createIntl(language, cache);
      ZenHelper.SetIntl(intl);
      IntlFormat.setIntl(intl)
   }

   onChangeLanguage = (locale = "vi-VN") => {
      let language = { ...this.state.language }
      if (locale === "en-US") {
         language = { ...language, ...MessageLanguage.en_US }
      }

      this.setInit(language);
      this.setState({ language: language });
   }

   // **************************************** HUBS ****************************************\\
   initSignalRConnection = (isLogin = true) => {
      if (auth.getToken() !== null) {
         this.hubConnection = new signalR.HubConnectionBuilder()
            .withUrl(ZenApp.baseUrl + `zenbook-hubs?tenantid=${auth.getTenantId()}`)
            .build()

         this.listenerChangedNotify()
         this.startSignalR(isLogin)
      }
   }

   startSignalR = (startFromLogin) => {
      this.num_of_tryconnect++;
      this.hubConnection.start()
         .then((con) => {
            console.log("connected")
            cacheData({ tenant: auth.getTenantId() }, startFromLogin)
         })
         .catch((err) => {
            console.log('Error while stating connection signalR: ', err)
         })
   }

   disconnectSignalR = () => {
      this.hubConnection.stop()
         .then(() => {
            console.log("disconnected")
         })
         .catch((err) => {
            console.log('Error while disconnect signalR: ', err)
         })
   }

   listenerChangedNotify = () => {
      this.hubConnection.on("InvalidateCache", (notify) => {
         console.log("ChangedNotifyListener", notify)
         cacheData(notify)
      })
   }

   render() {
      const { children } = this.props;
      const { language } = this.state;

      return (
         <GlobalStore
            changeLaguage={this.onChangeLanguage}
            initSignalRConnection={this.initSignalRConnection}
            disconnectSignalR={this.disconnectSignalR}
         >
            <ZenResponsiveProvider>
               <IntlProvider {...language}>
                  {children}
               </IntlProvider>
            </ZenResponsiveProvider>
         </GlobalStore>
      );
   }
}

const initGlobal = {
   menus: [],
   menuSys: [],
   tenantName: "",
}

const fetchReducer = (state, action) => {
   switch (action.type) {
      case "LOGOUT":
         // remove localstore
         auth.logout();
         // disconet hubs
         action.disconnectHubs()
         return initState(initGlobal);
      case "MENUS":
         return {
            ...state,
            menus: action.menus
         };
      case "MENU_SYS":
         return {
            ...state,
            menuSys: action.menuSys
         };
      case "TENANT_NAME":
         return {
            ...state,
            tenantName: action.tenantName
         };
      default:
         throw new Error();
   }
};

function initState(initItem) {
   return { ...initItem };
}

const GlobalStore = (props) => {
   const { changeLaguage, initSignalRConnection, disconnectSignalR } = props
   const [state, dispatch] = useReducer(fetchReducer, initGlobal, initState);

   function actions() {
      return {
         changeLaguage: changeLaguage,
         startHubs: initSignalRConnection,
         logout: () => dispatch({ type: "LOGOUT", disconnectHubs: disconnectSignalR }),
         setMenu: (data) => dispatch({ type: "MENUS", menus: data }),
         setMenuSys: (data) => dispatch({ type: "MENU_SYS", menuSys: data }),
         setTenantName: (name) => dispatch({ type: "TENANT_NAME", tenantName: name }),
      }
   }

   return (
      <AppContext.Provider value={{ store: state, actions: actions() }}>
         {props.children}
      </AppContext.Provider>
   );
}

export { AppContext, AppProviderWrapper }