import PropTypes from "prop-types";
import React, { useEffect, useState } from "react";
import { Button, Icon, Menu, Sidebar } from "semantic-ui-react";
import * as styles from "./NavbarMobile.less";
import { ZenApp } from "../../utils";
import { Link } from "react-router-dom";
import _ from "lodash";
import { zNavbarGeneral } from "./zNavbarGeneral";

const NavbarMobile = ({
  children,
  navItems,
  leftItems,
  rightItems,
}) => {
  const [openSidebar, setOpenSidebar] = useState(false)
  const [modalForm, setModalForm] = useState({ action: "", component: null })

  useEffect(() => {
    // move sidebarMenu to body html
    // Cố định sidebar, ko bị kéo theo khi scroll
    const sidebarMenu = document.getElementById("sidebar-menu")
    document.body.appendChild(sidebarMenu)
    return () => {
      document.body.removeChild(sidebarMenu)
    }
  }, [])

  const handleToggleSidebar = () => setOpenSidebar(!openSidebar)

  const handleOpenComponent = (keyAction) => {
    const component = keyAction ? zNavbarGeneral.getComponent(keyAction) : null;
    setModalForm({ action: keyAction, component: component });
  }

  return <>
    <Menu fixed="top" size="tiny" inverted
      borderless
      color={"teal"}>
      <Menu.Item to="/" as={Link} style={{ padding: "0px 20px" }}>
        {ZenApp.ZENAPP_FULL_NAME}
      </Menu.Item>

      <Menu.Menu position="right">
        <Menu.Item onClick={handleToggleSidebar}>
          <Icon name="sidebar" />
        </Menu.Item>
      </Menu.Menu>
    </Menu>

    <Sidebar.Pushable>
      <Sidebar id={'sidebar-menu'}
        as={Menu} vertical
        inverted
        animation="overlay"
        visible={openSidebar}
      >
        <Menu icon="labeled" className={styles["nav-home"]}
          inverted vertical>
          <Menu.Item as={Link} to="/" className={styles.sub}
            onClick={handleToggleSidebar}
          >
            <Icon name='home' />
            Home
          </Menu.Item>
        </Menu>

        <SideBarMenuNew navItems={navItems} onToggle={handleToggleSidebar} />
        <div className={styles.logout}>
          {
            rightItems.map(rightItem => {
              if (rightItem.dropdowns) {
                return rightItem.dropdowns.map(dropdown => {
                  return <React.Fragment key={dropdown.key}>
                    <Button {...dropdown}
                      name={dropdown.key}
                      fluid inverted color="grey"
                      className={styles.btn}
                      onClick={() => handleOpenComponent(dropdown.key)}
                      style={{ marginBottom: "7px" }}
                    />
                  </React.Fragment>
                })
              }
            })
          }
        </div>
      </Sidebar>

      <Sidebar.Pusher
        dimmed={openSidebar}
        onClick={openSidebar ? handleToggleSidebar : undefined}
        className={styles.pusher}
      >
        {children}
      </Sidebar.Pusher>
    </Sidebar.Pushable>

    {
      zNavbarGeneral.openComponent(modalForm.component,
        {
          open: modalForm.action ? true : false,
          onClose: () => handleOpenComponent()
        })
    }
  </>
};

const SideBarMenuNew = ({ navItems, onToggle }) => {
  const [activeSubMenu, SetActiveSubMenu] = useState([])

  const handleOpenSubMenu = (menuItems, lvl) => {
    if (lvl == 0 && activeSubMenu.includes(menuItems.menu_id)) {
      // Click menu active
      SetActiveSubMenu([])
    } else if (lvl == 0 && activeSubMenu.includes(menuItems.menu_id) === false) {
      // Click menu khác
      SetActiveSubMenu([].concat(menuItems.menu_id))
    } else if (lvl > 0 && activeSubMenu.includes(menuItems.menu_id)) {
      // Click submenu active
      SetActiveSubMenu(activeSubMenu.filter(x => x != menuItems.menu_id))
    } else if (lvl > 0 && activeSubMenu.includes(menuItems.menu_id) === false) {
      // Click submenu khác
      SetActiveSubMenu(activeSubMenu.concat(menuItems.menu_id))
    }
  }
  return <>
    {
      navItems &&
      navItems.length > 0 &&
      navItems.map((navModuleInfo, index) =>
        renderMenu(navModuleInfo, index, handleOpenSubMenu, activeSubMenu, 0, onToggle)
      )
    }
  </>
}

function renderMenu(navModuleInfo, index, handleOpenSubMenu, activeSubMenu, lvl = 0, onToggle) {
  return <React.Fragment key={index}>
    <Menu.Item style={{ paddingLeft: (lvl * 14 + 14) + "px" }}
      onClick={navModuleInfo.url ? onToggle : (e, element) => handleOpenSubMenu(navModuleInfo, lvl)}
      as={navModuleInfo.url ? Link : undefined}
      to={navModuleInfo.url}
    >
      {navModuleInfo.childs && navModuleInfo.childs.length > 0 ?
        <Icon name={activeSubMenu.includes(navModuleInfo.menu_id) ? "caret down" : "caret right"} />
        : navModuleInfo.icon && <Icon name={navModuleInfo.icon} />}

      {navModuleInfo.name}
    </Menu.Item>

    {navModuleInfo.childs && navModuleInfo.childs.length > 0 &&
      activeSubMenu.includes(navModuleInfo.menu_id) &&
      navModuleInfo.childs.map((navItem, idxChild) => {
        return renderMenu(navItem, idxChild, handleOpenSubMenu, activeSubMenu, lvl + 1, onToggle)
      })}
  </React.Fragment>
}

export default NavbarMobile;
NavbarMobile.propTypes = {
  children: PropTypes.node,
  leftItems: PropTypes.arrayOf(PropTypes.object),
  rightItems: PropTypes.arrayOf(PropTypes.object),
};