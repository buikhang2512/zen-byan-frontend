import PropTypes from "prop-types";
import React, { Component } from "react";
import { Container } from "semantic-ui-react";
import { ZenResponsive } from "../Control/ZenResponsive";
import NavbarDesktop from "./NavbarDesktop";
import NavbarMobile from "./NavbarMobile";
import * as styles from "./Navbar.less";
import { ErrorBoundary } from "../Control";

export default class NavBar extends Component {
  static propTypes = {
    children: PropTypes.node,
    navItems: PropTypes.arrayOf(PropTypes.object),
    leftItems: PropTypes.arrayOf(PropTypes.object),
    rightItems: PropTypes.arrayOf(PropTypes.object),
  };

  render() {
    const { children, modules, navItems, leftItems, rightItems } = this.props;

    return (
      <>
        <ZenResponsive at="mobile">
          <NavbarMobile
            navItems={navItems}
            leftItems={leftItems}
            rightItems={rightItems}
          >
            <Container fluid className={styles.containerMobile}>
              <ErrorBoundary>
                {children}
              </ErrorBoundary>
            </Container>
          </NavbarMobile>
        </ZenResponsive>

        <ZenResponsive greaterThan={"mobile"}>
          <NavbarDesktop
            modules={modules}
            navItems={navItems}
            leftItems={leftItems}
            rightItems={rightItems}
          />

          <Container fluid className={styles.container}>
            <ErrorBoundary>
              {children}
            </ErrorBoundary>
          </Container>
        </ZenResponsive>
      </>
    );
  }
}
