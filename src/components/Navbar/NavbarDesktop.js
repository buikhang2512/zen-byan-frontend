import _ from "lodash";
import PropTypes from "prop-types";
import React from "react";
import { Link } from "react-router-dom";
import { Menu, Dropdown, Icon, Image } from "semantic-ui-react";
import { zNavbarGeneral } from "./zNavbarGeneral";

class NavbarDesktop extends React.PureComponent {
  constructor(props) {
    super();

    this.state = {
      component: undefined,
      action: "",
    };
  }

  handleAction = (action) => {
    let component = action ? zNavbarGeneral.getComponent(action) : null;
    this.setState({ action: action, component: component });
  };

  render() {
    const { modules, navItems, leftItems, rightItems } = this.props;
    const { action, component } = this.state;
    return (
      <React.Fragment>
        {/* Global Top Bar */}
        <Menu fixed="top" borderless style={{ height: "50px", zIndex: "102" }}>
          <Menu.Item to="/" as={Link} style={{ padding: "0px 20px" }}>
            <Image src='/static/media/logo.png' style={{ maxHeight: "40px" }} />
            {/* {ZenApp.ZENAPP_FULL_NAME} */}
          </Menu.Item>

          {/* <Menu.Item style={{ padding: "0px 20px" }}>
            <Search
              size="mini"
            // loading={isLoading}
            // onResultSelect={this.handleResultSelect}
            // onSearchChange={_.debounce(this.handleSearchChange, 500, {
            //   leading: true,
            // })}
            // results={results}
            // value={value}
            // {...this.props}
            />
          </Menu.Item> */}

          {/* ==================MENU RIGHT======================== */}
          <Menu.Menu position="right">
            {_.map(rightItems, (item, index1) => {
              if (item.component) {
                return item.component;
              } else if (typeof item.dropdowns !== "undefined") {
                const ret = (
                  <Dropdown key={item.key || index1} item text={item.text}>
                    <Dropdown.Menu
                      // className={styles.brand_dropdown_menu}
                      direction={item.direction}
                    >
                      {_.map(item.dropdowns, (drdItem, index2) => {
                        const dItem =
                          drdItem.to !== undefined ? (
                            <Dropdown.Item
                              key={drdItem.key || index2}
                              as={Link}
                            >
                              <Icon
                                name={drdItem.icon}
                              // className={styles.brand_dropdown_menu_item}
                              />
                              <label
                              // className={styles.brand_dropdown_menu_item}
                              >
                                {drdItem.content}
                              </label>
                            </Dropdown.Item>
                          ) : (
                            <Dropdown.Item
                              key={drdItem.key || index2}
                              onClick={() => this.handleAction(drdItem.key)}
                            >
                              <Icon
                                name={drdItem.icon}
                              // className={styles.brand_dropdown_menu_item}
                              />
                              <label
                              // className={styles.brand_dropdown_menu_item}
                              >
                                {drdItem.content}
                              </label>
                            </Dropdown.Item>
                          );
                        return dItem;
                      })}
                    </Dropdown.Menu>
                  </Dropdown>
                );
                return ret;
              } else {
                const { is_external, ...restItem } = item
                let mItem = <Menu.Item {...restItem} />;

                if (restItem.to !== undefined) {
                  if (is_external) {
                    mItem = <Menu.Item {...restItem} href={restItem.to} as={"a"} />;
                  } else {
                    mItem = <Menu.Item {...restItem} as={Link} />;
                  }
                }
                // const mItem =
                //   item.to !== undefined ? (
                //       <Menu.Item key={item.key} {...item} as={Link} />
                //   ) : (
                //     <Menu.Item key={item.key} {...item} />
                //   );
                return mItem;
              }
            })}
          </Menu.Menu>
        </Menu>

        {/* NavBar */}
        <Menu
          fixed="top"
          inverted
          borderless
          color={"teal"}
          style={{ top: "50px" }}
        >
          {navItems &&
            navItems.length > 0 &&
            navItems.map((navModuleInfo, index3) => {
              var navModuleItem;
              if (parseInt(navModuleInfo.menu_id) < 90) {
                if (navModuleInfo.childs.length === 0 && navModuleInfo.url) {
                  navModuleItem = (
                    <Menu.Item
                      key={navModuleInfo.id || index3}
                      as={Link}
                      to={navModuleInfo.url}
                    >
                      {navModuleInfo.name}
                    </Menu.Item>
                  );
                } else {
                  //Nếu NavModule có childs item thì tạo dropdown
                  navModuleItem = navModuleInfo.childs?.length > 0 ? (
                    <Dropdown
                      key={navModuleInfo.id || index3}
                      item
                      text={navModuleInfo.name}
                    >
                      <Dropdown.Menu>
                        {navModuleInfo.childs.map((groupInfo, index4) => {
                          var groupItem;
                          //group không có childs thì tạo item luôn
                          if (groupInfo.childs.length === 0) {
                            groupItem = (
                              <Dropdown.Item
                                key={groupInfo.id || index4}
                                as={Link}
                                to={groupInfo.url}
                              >
                                {groupInfo.name}
                              </Dropdown.Item>
                            );
                          } else {
                            //Nếu group có child thì tạo Dropdown
                            groupItem = (
                              <Dropdown
                                key={groupInfo.id || index4}
                                item
                                text={groupInfo.name}
                              >
                                <Dropdown.Menu>
                                  {groupInfo.childs.map((menuInfo, index5) => {
                                    return (
                                      <Dropdown.Item
                                        key={menuInfo.id || index5}
                                        as={Link}
                                        to={menuInfo.url}
                                      >
                                        {menuInfo.name}
                                      </Dropdown.Item>
                                    );
                                  })}
                                </Dropdown.Menu>
                              </Dropdown>
                            );
                          }

                          return groupItem;
                        })}
                      </Dropdown.Menu>
                    </Dropdown>
                  ) : undefined;
                }
              }

              return navModuleItem;
            })}
          {/* ==================MENU LEFT======================== 
          {modules &&
            modules.length > 0 &&
            modules.map((mod) => {
              // thêm hr giữa báo cáo
              let isAddHr = false;
              var moduleItem;
              if (mod.url !== undefined) {
                moduleItem = (
                  <Menu.Item key={mod.id} as={Link} to={mod.url}>
                    {mod.name}
                  </Menu.Item>
                );
              } else {
                moduleItem = (
                  <Dropdown key={mod.id} item text={mod.name}>
                    <Dropdown.Menu>
                      {leftItems
                        .filter((x) => x.module_id === mod.id)
                        .map((item) => {
                          var menuItem = [
                            <Dropdown.Item
                              key={item.permission_id}
                              as={Link}
                              to={item.url}
                            >
                              {item.name}
                            </Dropdown.Item>,
                          ];

                          // add HR giữa báo cáo
                          if (!isAddHr && item.url.includes("rpt")) {
                            menuItem.unshift(
                              <Dropdown.Divider key={"hr-divider"} />
                            );
                            isAddHr = true;
                          }
                          return menuItem;
                        })}
                    </Dropdown.Menu>
                  </Dropdown>
                );
              }

              return moduleItem;
            })}
            */}
        </Menu>

        {zNavbarGeneral.openComponent(component, {
          open: action ? true : false,
          onClose: () => this.handleAction(),
        })}
      </React.Fragment>
    );
  }
}

NavbarDesktop.propTypes = {
  leftItems: PropTypes.arrayOf(PropTypes.object),
  rightItems: PropTypes.arrayOf(PropTypes.object),
};

export default NavbarDesktop;
