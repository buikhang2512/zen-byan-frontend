import React from "react";
import { ChangePassword, LogOutModal, MaintenaceClearCache } from "../../modules/index";

const zNavbarGeneral = {
   getComponent: getComponent,
   openComponent: openComponent
}

function getComponent(action) {
   let component;
   if (action === actionRight.ChangePass) {
      component = ChangePassword;
   } else if (action === actionRight.Logout) {
      component = LogOutModal;
   } else if (action === actionRight.ClearCache) {
      component = MaintenaceClearCache
   }
   return component;
}

function openComponent(Component, props) {
   if (Component) {
      return <Component {...props} />;
   }
}

const actionRight = {
   // tương ứng với key khai báo bên ultis/navs
   ChangePass: "account.change-password",
   Logout: "account.sign-out",
   ClearCache: "maintenance-clearcache",

};

export { zNavbarGeneral }