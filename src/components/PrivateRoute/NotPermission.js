import React from "react";
import { Grid, Icon } from "semantic-ui-react";

const NotFound = (props) => {
    return <Grid
        textAlign="center"
        style={{
            height: "25vh",
            backgroundSize: "cover",
            backgroundPosition: "left top",
            backgroundRepeat: "no-repeat",
        }}
        verticalAlign="middle"
    >
        <Grid.Column style={{ maxWidth: 600 }}>
            {/* <p className="pr-fontS-3em" style={{ margin: 0 }}>
                Bạn không có quyền truy cập trang này
            </p> */}

            <p className="pr-fontS-1em" style={{color:"#005c66", textAlign: "left", marginTop: "14px" }}>
                Vui lòng liên hệ admin nếu bạn cần sử dụng tính năng này
            </p>
        </Grid.Column>
    </Grid>
};

export default NotFound;
