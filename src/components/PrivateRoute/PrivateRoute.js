import React from "react";
import { Redirect, Route } from "react-router-dom";
import { auth } from "../../utils";
import * as routes from '../../constants/routes'
import "./PrivateRoute.css"
/**
 * notPermission = true => ko cần check permission
 */

const PrivateRoute = ({ component: Component, notPermission, permission: requirePermission, form, initProps, ...rest }) => {
  var hasPermis = auth.checkPermission(requirePermission);

  // if (requirePermission) {
  //   hasPermis = auth.checkPermission(requirePermission)
  // }

  if (notPermission || hasPermis) {
    return (
      <Route {...rest} render={props => form ? <Component initProps={initProps} {...props} zenform={form} />
        : <Component initProps={initProps} {...props} />} />
    );
  } else {
    return (
      <Redirect to={{ pathname: routes.NotPermission }} />
    );
  }
};

export default PrivateRoute;
