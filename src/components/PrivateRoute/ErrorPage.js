import React from "react";
import { Grid, Icon } from "semantic-ui-react";

const ErrorPage = (props) => {
    return <Grid
        textAlign="center"
        style={{
            height: "25vh",
            backgroundSize: "cover",
            backgroundPosition: "left top",
            backgroundRepeat: "no-repeat",
        }}
        verticalAlign="middle"
    >
        <Grid.Column style={{ maxWidth: 450 }}>
            <p className="pr-fontS-3em" style={{ margin: 0 }}>
                <Icon name="warning circle" />
                ERROR
            </p>

            <p className="pr-fontS-1em" style={{ textAlign: "left" }}>
                Có lỗi khi thực hiện chức năng này, vui lòng thử lại hoặc sử dụng các chức năng khác
            </p>
        </Grid.Column>
    </Grid>
};

export default ErrorPage;
