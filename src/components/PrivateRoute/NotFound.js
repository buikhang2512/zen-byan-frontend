import React from "react";
import { Grid } from "semantic-ui-react";

const NotFound = (props) => {
    return <Grid
        textAlign="center"
        style={{
            height: "25vh",
            backgroundSize: "cover",
            backgroundPosition: "left top",
            backgroundRepeat: "no-repeat",
        }}
        verticalAlign="middle"
    >
        <Grid.Column style={{ maxWidth: 500 }}>
            <p className="pr-fontS-3em" style={{ margin: 0 }}>
                404
            </p>
            <p className="pr-fontS-3em" style={{ margin: 0 }}>
                PAGE NOT FOUND
            </p>

            <p className="pr-fontS-1em" style={{ textAlign: "left", marginTop: "14px" }}>
                Trang bạn truy cập không tìm thấy, vui lòng thử trang khác.
            </p>
        </Grid.Column>
    </Grid>
};

export default NotFound;
