import React from 'react'
import ReactDOM from 'react-dom'
import { AppContainer } from 'react-hot-loader'
import App from './App'
import { AppProviderWrapper } from './AppContext';

const render = (Component) => {
  ReactDOM.render(
    <AppProviderWrapper>
      <AppContainer>
        <Component />
      </AppContainer>
    </AppProviderWrapper>,
    document.getElementById('root'),
  )
}

render(App)
if (process.env.NODE_ENV === 'development' && module.hot) {
  module.hot.accept('./App', () => { render(App) })
}
